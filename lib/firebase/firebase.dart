import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;

class FirebaseAuthorization {
  static FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;
  static FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  static late CollectionReference collectionReference;
  static firebase_storage.FirebaseStorage storage =
      firebase_storage.FirebaseStorage.instance;
}
