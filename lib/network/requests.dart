import 'package:AwesomeSkills/network/base_response.dart';
import 'package:AwesomeSkills/network/end_points.dart';
import 'package:AwesomeSkills/network/logging.dart';
import 'package:AwesomeSkills/utils/utils.dart';
import 'package:dio/dio.dart';
import 'package:get/get.dart' hide Response, FormData;

class DioClient {
  static const String _baseURL = EndPoints.baseURL;
  static final DioClient _singleton = DioClient._internal();

  factory DioClient() {
    return _singleton;
  }

  DioClient._internal();

  final Dio _dio = Dio(
    BaseOptions(
      baseUrl: _baseURL,
      connectTimeout: 21000,
      receiveTimeout: 20000,
    ),
  )..interceptors.addAll([Logging()]);

  Future<BaseResponse> getRequest({
    required String endPoint,
    Map<String, dynamic>? queryParams,
    Map<String, dynamic>? headers,
  }) async {
    try {
      Utils.showProgressBar();
      Response response = await _dio.get(
        endPoint,
        queryParameters: queryParams,
        options: Options(headers: headers),
      );
      BaseResponse baseResponse = parseResponse(response);
      return baseResponse;
    } on DioError catch (e) {
      return handleException(e);
    }
  }

  Future<BaseResponse> postRequest({
    required String endPoint,
    Map<String, dynamic>? body,
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic>? headers,
  }) async {
    try {
      Utils.showProgressBar();
      Response response = await _dio.post(
        _baseURL + endPoint,
        data: body,
        queryParameters: queryParameters,
        options: Options(headers: headers),
      );
      BaseResponse baseResponse = parseResponse(response);
      return baseResponse;
    } on DioError catch (e) {
      return handleException(e);
    }
  }

  Future<BaseResponse> postFormRequest({
    required String endPoint,
    FormData? body,
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic>? headers,
  }) async {
    try {
      Utils.showProgressBar();
      Response response = await _dio.post(
        endPoint,
        data: body,
        queryParameters: queryParameters,
        options: Options(headers: headers),
      );
      BaseResponse baseResponse = parseResponse(response);
      return baseResponse;
    } on DioError catch (e) {
      return handleException(e);
    }
  }

  Future<BaseResponse> putRequest({
    required String endPoint,
    Map<String, dynamic>? body,
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic>? headers,
  }) async {
    try {
      Utils.showProgressBar();
      Response response = await _dio.put(
        _baseURL + endPoint,
        data: body,
        queryParameters: queryParameters,
        options: Options(headers: headers),
      );
      BaseResponse? baseResponse = parseResponse(response);
      return baseResponse;
    } on DioError catch (e) {
      return handleException(e);
    }
  }

  Future<BaseResponse> deleteRequest({
    String? path,
    required String endPoint,
    Map<String, dynamic>? queryParameters,
    Map<String, dynamic>? headers,
    Map<String, dynamic>? body,
  }) async {
    try {
      Utils.showProgressBar();
      Response response = await _dio.delete(
        _baseURL + endPoint,
        queryParameters: queryParameters,
        options: Options(headers: headers),
      );
      BaseResponse? baseResponse = parseResponse(response);
      return baseResponse;
    } on DioError catch (e) {
      return handleException(e);
    }
  }

  BaseResponse parseResponse(Response response) {
    Utils.dismissProgressBar();
    if (response.statusCode == 200 && !response.data['error']) {
      print(response.data['error']);
      BaseResponse baseResponse = BaseResponse.fromJson(response.data);
      return baseResponse;
    } else {
      showMessage(message: response.data['message'], isError: true);
      return BaseResponse(
        error: true,
        message: response.data['message'],
      );
    }
  }

  void showMessage({required String message, required bool isError}) {
    if (message.toLowerCase().contains('DioError')) {
      message = 'Something went Wrong';
    }
    if (isError) {
      Get.showSnackbar(Utils.errorSnackBar(message: message));
    } else {
      Get.showSnackbar(Utils.successSnackBar(message: message));
    }
  }

  BaseResponse handleException(DioError e) {
    Utils.dismissProgressBar();
    showMessage(message: e.response?.data['message'] ?? e.error, isError: true);
    return BaseResponse(
      error: true,
      message: e.response?.statusMessage ?? e.error,
      statusCode: e.response?.statusCode,
    );
  }
}
