class EndPoints {
  // ------ base Url ----------//
  static const baseURL = 'http://51.68.167.212:5000/';
  // -------- post Url For personal  ------ //
  static const signUpUrl = 'user/sign-up';
  static const signInUrl = 'user/sign-in';

  // ----- Part of  Post Url  (ad Need Help Screen EnPoints) ---- //

  // -------- Post Url For personal  ------ //
  static const postAdNeedHelpUrl = 'user/add-needhelpads';
  static const postAdCanHelpUrl = 'user/add-canhelpads';

  static const getCanHelpCardUrl = 'posts/getall-canhelpad';
  static const getNeedHelpCardUrl = 'posts/getall-needhelpad';
  static const getFeatureJobUrl = 'posts/getall-featuredjob';

  // ----- Part of  get Url For personal  (ad Need Help Screen EnPoints) ---- //
  static const addNeedHelpAdsUrl = 'add-needhelpads';
  static const postsGetCategoryUrl = 'posts/get-category';
  static const getServiceUrl = 'posts/get-services';
  static const getTagsAdNeedHelpUrl = 'posts/get-tags';
  static const getWorkingDurationUrl = 'posts/get-duration';
  static const getJobPriceUrl = 'posts/get-pricetype';
  static const getJobTypeRepairUrl = 'posts/get-jobtype';
  static const getChooseLabourUrl = 'posts/get-resource';
  static const getWorkplaceUrl = 'posts/get-workplace';

// ----- analysis get apis url ---- //
  static const getAnalysisUrl = 'user/get-analytics';

  // ----- user related  apis url ---- //
  static const getUserInfo = 'user/user/info';
  static const resetPassword = 'user/reset-password';

  // ----- Skill related apis url ---- //
  static const postDesignationUrl = 'user/designation';
  static const postAddSkillUrl = 'user/add-skills';
  static const postAddLanguage = 'user/add-language';
  static const postAddCertificate = 'user/add-certificate';
  static const deleteSkillUrl = 'user/delete-skills/';
  static const deleteLanguageUrl = 'user/delete-language/';
  static const deleteCertificateUrl = 'user/delete-certificate/';

  // ----- review related apis url ---- //
  static const postReviewUrl = 'user/add-review';
  static const getMyReviewUrl = 'user/get-review';

  // ----- user related apis url ---- //
  static const userprofilePostUrl = 'user/add-profile';

// ----- message related apis url ---- //
  static const messageUploadFile = "media";

// ----- job assigning related apis url ---- //
  static const jobAssignUrl = "user/user/assign-job";
  static const getFirebaseUserUrl = "list-users";

// ----- my  task related apis url ---- //
  static const myInProgressTaskUrl = "user/user/get-job";
}
