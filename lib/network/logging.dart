import 'package:AwesomeSkills/local/db/local_storage.dart';
import 'package:AwesomeSkills/network/logger_messaging.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';

class Logging extends Interceptor {
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    options.headers.addAll(
      {
        'content-Type': 'application/json',
        'connection': 'Keep-Alive',
        'Authorization': HiveHelper.getAuthToken()
      },
    );
    if (kDebugMode) {
      LoggerMessage.logInfo(
          msg:
              ' Token : ${HiveHelper.getAuthToken()}\n REQUEST[${options.method}]\n => BASEURL : ${options.baseUrl + options.path}\n => PATH: ${options.path}\n BODY => ${options.data.toString()}');
    }
    return handler.next(options);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    if (kDebugMode) {
      LoggerMessage.logSuccess(
          msg:
              'RESPONSE[${response.statusCode}] => PATH: ${response.requestOptions.path} => DATA : ${response.data}');
    }

    return handler.next(response);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    if (kDebugMode) {
      LoggerMessage.logError(
          msg:
              'STATUS_CODE[${err.response?.statusCode}] => PATH: ${err.requestOptions.path} \n STATUS_MESSAGE[${err.response?.statusMessage}]');
    }
    return handler.next(err);
  }
}
