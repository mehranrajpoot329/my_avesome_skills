/// error : false
/// message : "user successfully created and saved in database"
/// userInfo : {"uid":"w5Q1Q8xLxHfGUMrZx1HdvUWzIhJ3","displayName":"demo1 ","email":"demo12@gmail.com"}

class Base {
  Base({
    bool? error,
    String? message,
    UserInfo? userInfo,
  }) {
    _error = error;
    _message = message;
    _userInfo = userInfo;
  }

  Base.fromJson(dynamic json) {
    _error = json['error'];
    _message = json['message'];
    _userInfo =
        json['userInfo'] != null ? UserInfo.fromJson(json['userInfo']) : null;
  }
  bool? _error;
  String? _message;
  UserInfo? _userInfo;
  Base copyWith({
    bool? error,
    String? message,
    UserInfo? userInfo,
  }) =>
      Base(
        error: error ?? _error,
        message: message ?? _message,
        userInfo: userInfo ?? _userInfo,
      );
  bool? get error => _error;
  String? get message => _message;
  UserInfo? get userInfo => _userInfo;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['error'] = _error;
    map['message'] = _message;
    if (_userInfo != null) {
      map['userInfo'] = _userInfo?.toJson();
    }
    return map;
  }
}

/// uid : "w5Q1Q8xLxHfGUMrZx1HdvUWzIhJ3"
/// displayName : "demo1 "
/// email : "demo12@gmail.com"

class UserInfo {
  UserInfo({
    String? uid,
    String? displayName,
    String? email,
  }) {
    _uid = uid;
    _displayName = displayName;
    _email = email;
  }

  UserInfo.fromJson(dynamic json) {
    _uid = json['uid'];
    _displayName = json['displayName'];
    _email = json['email'];
  }
  String? _uid;
  String? _displayName;
  String? _email;
  UserInfo copyWith({
    String? uid,
    String? displayName,
    String? email,
  }) =>
      UserInfo(
        uid: uid ?? _uid,
        displayName: displayName ?? _displayName,
        email: email ?? _email,
      );
  String? get uid => _uid;
  String? get displayName => _displayName;
  String? get email => _email;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['uid'] = _uid;
    map['displayName'] = _displayName;
    map['email'] = _email;
    return map;
  }
}
