// import 'dart:convert';
// import 'package:get/get.dart';
// import 'package:dio/dio.dart' as dioApi;
// import 'package:AwesomeSkills/local/db/local_storage.dart';
// import 'package:AwesomeSkills/network/end_points.dart';
//
// class BaseApiConnection extends GetConnect {
//   static BaseApiConnection instance = BaseApiConnection();
//
//   Future<dynamic>? postRequest(
//       {required String endPoint,
//       Map<String, dynamic>? body,
//       Map<String, dynamic>? queryParms}) async {
//     try {
//       String url = EndPoints.baseURL;
//
//       String? token = await SharedPreferenceHelper.instance.getString('Token');
//       String? corporateID = await SharedPreferenceHelper.instance
//           .getString(SharedPreferenceHelper.corporateIdKey);
//
//       Map<String, String> apiHeaders = {
//         'access-token': token ?? '',
//         if (corporateID != null) 'id-Corporate': corporateID
//       };
//
//       var options = dioApi.BaseOptions(
//         baseUrl: url,
//         connectTimeout: 60 * 1000,
//         method: 'POST',
//         receiveTimeout: 60 * 1000,
//         headers: apiHeaders,
//       );
//       dioApi.Dio dio = dioApi.Dio(options);
//
//       var result = await dio
//           .post(endPoint, data: jsonEncode(body), queryParameters: queryParms)
//           .onError((error, stackTrace) {
//         throw error.toString();
//       });
//
//       print(result);
//
//       if (result.data['message'].contains('token has expired') == true) {
//         // await updateToken();
//         token = await SharedPreferenceHelper.instance
//             .getString(SharedPreferenceHelper.tokenKey);
//
//         dio.options.headers = {
//           'access-token': token,
//           if (corporateID != null) 'id-Corporate': corporateID
//         };
//         var result = await dio
//             .post(endPoint, data: jsonEncode(body), queryParameters: queryParms)
//             .onError((error, stackTrace) {
//           throw error.toString();
//         });
//         return result;
//       } else {
//         return result;
//       }
//     } catch (e) {
//       print(e.toString());
//     }
//   }
//
//   // updateToken() async {
//   //   var firebaseAuth = FirebaseAuth.instance;
//   //   if (FirebaseAuth.instance.currentUser != null) {
//   //     String? token = await firebaseAuth.currentUser?.getIdToken(true);
//   //     await SharedPreferenceHelper.instance
//   //         .storeString(SharedPreferenceHelper.tokenKey, token ?? '');
//   //   }
//   // }
// }
