enum DashboardPersonalDrawerEnum {
  myTaskPersonal,
  myAdsPersonal,
  mySkillsPersonal,
  myQuotesPersonal,
  analyticsPersonal,
  messagePersonal,
  reviewsPersonal,
  favouritesPersonal,
  settingsPersonal,
  profilePersonal,
  alertNotificationsPersonal,
  securityPasswordPersonal,
  socialMediaPersonal,
  myPlanPersonal,
  paymentMethodPersonal
}
