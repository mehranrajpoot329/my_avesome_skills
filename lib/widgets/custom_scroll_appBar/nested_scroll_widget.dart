import 'package:AwesomeSkills/local/db/local_storage.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/widgets/custom_bottom_appBar_navi/custom_bottom_appBar_navi.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/custom_app_bar_controller.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget_controller.dart';
import 'package:AwesomeSkills/widgets/drawer/personal/drawer_profile_overview_personal_controller.dart';
import 'package:sizer/sizer.dart';

import '../../constants/app_colors.dart';

class NestedScrollWidget extends StatefulWidget {
  static final customAppBar = Get.put(CustomAppBarController());
  Widget body, floatingActionButton;

  NestedScrollWidget({
    required this.body,
    required this.floatingActionButton,
  });

  @override
  State<NestedScrollWidget> createState() => _NestedScrollWidgetState();
}

class _NestedScrollWidgetState extends State<NestedScrollWidget> {
  final appBarController = Get.put(ProfileOverviewControllerPersonal());

  final nestedScrollController = Get.put(NestedScrollWidgetController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawerEdgeDragWidth: 0.0,
      floatingActionButton: widget.floatingActionButton,
      bottomNavigationBar: CustomNavBarWidget(),
      drawer: GetBuilder<ProfileOverviewControllerPersonal>(
        builder: (_) {
          return appBarController.switchDrawer();
        },
      ),
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxScrolled) {
          return <Widget>[
            NestedScrollWidget.customAppBar.createSilverAppBar1(context),
            HiveHelper.isLoggedIn()
                ? NestedScrollWidget.customAppBar.createSilverAppBar4(context)
                : NestedScrollWidget.customAppBar.createSilverAppBar2(),
            NestedScrollWidget.customAppBar.createSilverAppBar3(context),
            HiveHelper.isLoggedIn()
                ? NestedScrollWidget.customAppBar.createSilverAppBar5(context)
                : NestedScrollWidget.customAppBar.createSilverAppBar4(context)
          ];
        },
        body: widget.body,
      ),
    );
  }
}
