import 'package:AwesomeSkills/constants/app_fonts.dart';
import 'package:AwesomeSkills/enums/dashboard_personal_drawer_enum.dart';
import 'package:AwesomeSkills/local/db/local_storage.dart';
import 'package:AwesomeSkills/routes/app_pages.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/bottom_sheet_widget.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';

import '../../constants/app_strings.dart';

class NestedScrollWidgetController extends GetxController {
  var currentPage = DashboardPersonalDrawerEnum.myTaskPersonal;
  int currentIndex = 9;

  List<BottomNavyBarItem> bottomNavyBarItem(BuildContext context) {
    return [
      BottomNavyBarItem(
        icon: GestureDetector(
          onTap: () => Get.toNamed(AppPage.landingScreen),
          child: SvgPicture.asset(
            'assets/images/post_icon/home_icon.svg',
            height: 18.sp,
            width: 18.sp,
          ),
        ),
        title: Text(
          AppStrings.home,
          style: TextStyle(
              color: Colors.white,
              fontSize: 10.sp,
              fontFamily: AppFonts.poppins,
              fontWeight: FontWeight.w200),
        ),
        activeColor: Colors.white,
      ),
      BottomNavyBarItem(
        icon: GestureDetector(
          onTap: () =>
              Get.toNamed(AppPage.canHelpScreenBusinessWithOutBottomNav),
          child: SvgPicture.asset(
            'assets/images/post_icon/hand_shake.svg',
            height: 18.sp,
            width: 18.sp,
          ),
        ),
        title: const Text(
          AppStrings.canHelp,
          style: TextStyle(
              color: Colors.white,
              fontFamily: AppFonts.poppins,
              fontWeight: FontWeight.w200),
        ),
        activeColor: Colors.white,
      ),
      BottomNavyBarItem(
        icon: GestureDetector(
          onTap: () => Get.toNamed(AppPage.needHelpScreenWithOutBottomNav),
          child: SvgPicture.asset(
            'assets/images/post_icon/question_icon.svg',
            height: 18.sp,
            width: 18.sp,
          ),
        ),
        title: Text(
          AppStrings.needHelp,
          style: TextStyle(
              color: Colors.white,
              fontSize: 10.sp,
              fontFamily: AppFonts.poppins,
              fontWeight: FontWeight.w200),
        ),
        activeColor: Colors.white,
      ),
      BottomNavyBarItem(
        icon: GestureDetector(
          onTap: () => HiveHelper.isLoggedIn()
              ? BottomSheetAdScreen.isLoginedInBottomSheet(
                  context: context,
                  needHelpTap: () =>
                      Get.toNamed(AppPage.adScreenNeedHelpPersonal),
                  canHelpTap: () =>
                      Get.toNamed(AppPage.adScreenCanNeedHelpPersonal))
              : BottomSheetAdScreen.isLoginedOutBottomSheet(context),
          child: SvgPicture.asset(
            'assets/images/post_icon/post_icon.svg',
            height: 18.sp,
            width: 18.sp,
          ),
        ),
        title: Text(
          AppStrings.postAd,
          style: TextStyle(
              fontSize: 10.sp,
              color: Colors.white,
              fontFamily: AppFonts.poppins,
              fontWeight: FontWeight.w200),
        ),
        activeColor: Colors.white,
      ),
    ];
  }
}
