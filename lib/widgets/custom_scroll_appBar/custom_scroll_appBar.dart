import 'package:AwesomeSkills/Modules/Personal/landing_screen/landing_screen_controller.dart';
import 'package:AwesomeSkills/local/db/local_storage.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/routes/app_pages.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/custom_app_bar_controller.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/bottom_sheet_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:AwesomeSkills/widgets/drawer/personal/drawer_profile_overview_personal_controller.dart';
import 'package:sizer/sizer.dart';

class CustomAppBarScreen extends StatefulWidget {
  @override
  State<CustomAppBarScreen> createState() => _CustomAppBarScreenState();
}

class _CustomAppBarScreenState extends State<CustomAppBarScreen> {
  final appBarController = Get.put(ProfileOverviewControllerPersonal());

  final customAppBarController = Get.put(CustomAppBarController());

  late PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawerEdgeDragWidth: 0.0,
        drawer: HiveHelper.isLoggedIn()
            ? Obx(() => appBarController.switchDrawer())
            : null,
        floatingActionButton: Stack(
          clipBehavior: Clip.none,
          children: [
            Positioned(
                right: 0.w,
                bottom: -2.h,
                child: WidgetMethod.parentWidgetFloating(context))
          ],
        ),
        bottomNavigationBar: Padding(
          padding: const EdgeInsets.all(8.0),
          child: BottomNavyBar(
            curve: Curves.bounceInOut,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            containerHeight: 8.h,
            backgroundColor: AppColors.lightBlueColor,
            selectedIndex: customAppBarController.currentIndex,
            showElevation: true, // use this to remove appBar's elevation
            onItemSelected: (index) => setState(() {
              customAppBarController.currentIndex = index;
            }),
            items: customAppBarController.bottomNavyBarItem(context),
          ),
        ),
        body: GetBuilder<CustomAppBarController>(builder: (_) {
          return NestedScrollView(
            headerSliverBuilder: (BuildContext context, bool innerBoxScrolled) {
              return <Widget>[
                customAppBarController.createSilverAppBar1(context),
                HiveHelper.isLoggedIn()
                    ? customAppBarController.createSilverAppBar4(context)
                    : customAppBarController.createSilverAppBar2(),
                customAppBarController.createSilverAppBar3(context),
                HiveHelper.isLoggedIn()
                    ? customAppBarController.createSilverAppBar5(context)
                    : customAppBarController.createSilverAppBar4(context)
              ];
            },
            body: customAppBarController
                .screens[customAppBarController.currentIndex],
          );
        }));
  }
}
