import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../constants/app_colors.dart';
import '../constants/app_fonts.dart';

class CustomDropDown extends StatefulWidget {
  final double height, width;
  final List<String> items;
  final String hint;
  final Function(String?) selectedValue;
  const CustomDropDown(
      {Key? key,
      required this.items,
      required this.height,
      required this.width,
      required this.hint,
      required this.selectedValue})
      : super(key: key);

  @override
  State<CustomDropDown> createState() => _CustomDropDownState();
}

class _CustomDropDownState extends State<CustomDropDown> {
  String? selectedValue;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 1.w),
      child: Container(
        height: widget.height,
        width: widget.width,
        padding: EdgeInsets.symmetric(horizontal: 4.w),
        decoration: BoxDecoration(
          color: AppColors.lightBlue6Color,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Center(
          child: DropdownButton<String>(
            value: selectedValue,
            borderRadius: BorderRadius.circular(10),
            icon: const Icon(Icons.arrow_drop_down),
            iconSize: 3.h,
            isExpanded: true,
            isDense: true,
            underline: const SizedBox(),
            style: TextStyle(
                color: AppColors.textBlack,
                fontFamily: AppFonts.poppins,
                fontSize: 10.sp),
            hint: FittedBox(
              child: Text(
                widget.hint,
                style: TextStyle(
                    color: AppColors.textBlack,
                    fontFamily: AppFonts.poppins,
                    fontSize: 12.sp),
                textAlign: TextAlign.center,
              ),
            ),
            onChanged: (newValue) {
              selectedValue = newValue;
              widget.selectedValue(newValue);
              setState(() {});
            },
            items: widget.items.map<DropdownMenuItem<String>>(
              (String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(
                    value,
                    style: TextStyle(
                        color: AppColors.textBlack,
                        fontFamily: AppFonts.poppins,
                        fontWeight: FontWeight.w500,
                        fontSize: 12.sp),
                    textAlign: TextAlign.center,
                  ),
                );
              },
            ).toList(),
          ),
        ),
      ),
    );
  }
}
