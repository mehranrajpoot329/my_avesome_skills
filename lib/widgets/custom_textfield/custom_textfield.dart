import 'package:AwesomeSkills/Modules/login_signup/signup_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:sizer/sizer.dart';

class CustomTextFormField extends StatelessWidget {
  double height, width;
  BorderRadius borderRadius;
  final controllerSignUp = Get.put(SignUpController());
  TextStyle hintTextStyle;
  TextEditingController controller;
  final String? Function(String?)? validator;
  final List<TextInputFormatter>? inputFormatters;
  final String name;
  Color color;
  String hintText;

  CustomTextFormField(
      {required this.hintText,
      required this.name,
      required this.height,
      required this.width,
      required this.borderRadius,
      required this.hintTextStyle,
      required this.color,
      this.inputFormatters,
      required this.validator,
      required this.controller});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      decoration: BoxDecoration(
        color: color,
        borderRadius: borderRadius,
      ),
      child: FormBuilderTextField(
        name: name,
        inputFormatters: inputFormatters,
        validator: validator,
        controller: controller,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.only(
            left: 6.w,
          ),
          border: InputBorder.none,
          hintText: hintText,
          hintStyle: hintTextStyle,
          errorStyle: const TextStyle(color: Colors.red, fontSize: 10),
        ),
      ),
    );
  }
}

class CustomTextFormFieldSuffix extends StatelessWidget {
  Function onTap;
  double height, width;
  BorderRadius borderRadius;
  final controllerSignUp = Get.put(SignUpController());
  TextStyle hintTextStyle;
  TextEditingController controller;
  final String? Function(String?)? validator;
  final List<TextInputFormatter>? inputFormatters;
  final String name;
  RxBool obscureText = false.obs;
  Color color;
  String hintText;

  CustomTextFormFieldSuffix(
      {required this.hintText,
      required this.name,
      required this.onTap,
      required this.height,
      required this.width,
      required this.borderRadius,
      required this.hintTextStyle,
      required this.color,
      required this.obscureText,
      this.inputFormatters,
      required this.validator,
      required this.controller});

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Container(
        padding: EdgeInsets.only(
          left: 6.w,
          right: 2.w,
        ),
        height: height,
        width: width,
        decoration: BoxDecoration(
          color: color,
          borderRadius: borderRadius,
        ),
        child: Row(
          children: [
            Expanded(
              child: FormBuilderTextField(
                textAlign: TextAlign.justify,
                name: name,
                inputFormatters: inputFormatters,
                validator: validator,
                obscureText: obscureText.value,
                controller: controller,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.only(top: 0.5.h),
                  border: InputBorder.none,
                  hintText: hintText,
                  hintStyle: hintTextStyle,
                  errorStyle: const TextStyle(color: Colors.red, fontSize: 10),
                ),
              ),
            ),
            GestureDetector(
              onTap: () => onTap(),
              child: Icon(
                obscureText.value ? Icons.visibility_off : Icons.visibility,
                color: AppColors.iconVisibleColor,
              ),
            )
          ],
        ),
      ),
    );
  }
}
