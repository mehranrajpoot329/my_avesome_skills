import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/constants/app_colors.dart';
import 'package:AwesomeSkills/constants/app_fonts.dart';
import 'package:AwesomeSkills/constants/app_strings.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/custom_app_bar_controller.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/bottom_sheet_widget.dart';
import 'package:sizer/sizer.dart';

import '../../routes/app_pages.dart';

class CustomNavBarWidget extends StatefulWidget {
  @override
  State<CustomNavBarWidget> createState() => _CustomNavBarWidgetState();
}

class _CustomNavBarWidgetState extends State<CustomNavBarWidget> {
  int currentIndex = 8;

  final controller = Get.put(CustomAppBarController());

  @override
  Widget build(BuildContext context) {
    List iconIndex = [
      'assets/images/post_icon/home_icon.svg',
      'assets/images/post_icon/hand_shake.svg',
      'assets/images/post_icon/question_icon.svg',
      //  'assets/images/post_icon/quote_icon.svg',
      'assets/images/post_icon/post_icon.svg',
    ];
    List iconName = [
      AppStrings.home,
      AppStrings.canHelp,
      AppStrings.needHelp,
      //     AppStrings.getQuotes,
      AppStrings.postAnAds,
    ];
    return Container(
      margin: const EdgeInsets.all(8),
      //  padding: EdgeInsets.only(left: 8.w, right: 8.w),
      decoration: BoxDecoration(
          color: AppColors.lightBlueColor,
          borderRadius: BorderRadius.circular(10)),
      child: SizedBox(
        width: double.infinity,
        height: 8.h,
        child: Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
          InkWell(
            onTap: () {
              setState(() {
                currentIndex = 0;
                Get.toNamed(AppPage.landingScreen);
              });
            },
            child: currentIndex == 0
                ? Container(
                    height: 5.2.h,
                    width: 30.w,
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.2),
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: [
                          SvgPicture.asset(
                            iconIndex[0],
                            height: 18.sp,
                            width: 18.sp,
                          ),
                          SizedBox(
                            width: 2.w,
                          ),
                          const Text(
                            AppStrings.home,
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: AppFonts.poppins,
                                fontWeight: FontWeight.w200),
                          )
                        ],
                      ),
                    ))
                : SvgPicture.asset(
                    iconIndex[0],
                    height: 20.sp,
                    width: 20.sp,
                  ),
          ),
          GestureDetector(
            onTap: () {
              setState(() {
                currentIndex = 1;
                Get.toNamed(AppPage.canHelpScreenWithOutBottomNav);
              });
            },
            child: currentIndex == 1
                ? Container(
                    height: 5.2.h,
                    width: 35.w,
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.2),
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: [
                          SvgPicture.asset(
                            iconIndex[1],
                            height: 18.sp,
                            width: 18.sp,
                          ),
                          SizedBox(
                            width: 2.w,
                          ),
                          const Text(
                            AppStrings.canHelp,
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: AppFonts.poppins,
                                fontWeight: FontWeight.w200),
                          )
                        ],
                      ),
                    ))
                : SvgPicture.asset(
                    iconIndex[1],
                    height: 20.sp,
                    width: 20.sp,
                  ),
          ),
          GestureDetector(
            onTap: () {
              setState(() {
                currentIndex = 2;
                Get.back();
                Get.toNamed(AppPage.needHelpScreenWithOutBottomNav);
              });
            },
            child: currentIndex == 2
                ? Container(
                    height: 5.2.h,
                    width: 40.w,
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.2),
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: [
                          SvgPicture.asset(
                            iconIndex[2],
                            height: 18.sp,
                            width: 18.sp,
                          ),
                          SizedBox(
                            width: 2.w,
                          ),
                          const Text(
                            AppStrings.iNeedHelp,
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: AppFonts.poppins,
                                fontWeight: FontWeight.w200),
                          )
                        ],
                      ),
                    ))
                : SvgPicture.asset(
                    iconIndex[2],
                    height: 20.sp,
                    width: 20.sp,
                  ),
          ),
          // GestureDetector(
          //   onTap: () {
          //     setState(() {
          //       currentIndex = 3;
          //       Get.toNamed(AppPage.getQuotesScreen);
          //       currentIndex = 8;
          //     });
          //   },
          //   child: currentIndex == 3
          //       ? Container(
          //           height: 5.2.h,
          //           width: 32.w,
          //           decoration: BoxDecoration(
          //             color: Colors.white.withOpacity(0.2),
          //             borderRadius: BorderRadius.circular(7),
          //           ),
          //           child: Padding(
          //             padding: const EdgeInsets.all(8.0),
          //             child: Row(
          //               children: [
          //                 SvgPicture.asset(
          //                   iconIndex[3],
          //                 ),
          //                 SizedBox(
          //                   width: 2.w,
          //                 ),
          //                 const Text(
          //                   AppStrings.getQuotes,
          //                   style: TextStyle(
          //                       color: Colors.white,
          //                       fontFamily: AppFonts.poppins,
          //                       fontWeight: FontWeight.w200),
          //                 ),
          //               ],
          //             ),
          //           ))
          //       : SvgPicture.asset(
          //           iconIndex[3],
          //         ),
          // ),
          GestureDetector(
            onTap: () {
              setState(() {
                Get.back();
                currentIndex = 3;
                BottomSheetAdScreen.isLoginedInBottomSheet(
                    context: context,
                    needHelpTap: () =>
                        Get.toNamed(AppPage.adScreenNeedHelpPersonal),
                    canHelpTap: () =>
                        Get.toNamed(AppPage.adScreenCanNeedHelpPersonal));
              });
            },
            child: currentIndex == 3
                ? Container(
                    height: 5.2.h,
                    width: 33.w,
                    decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.2),
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 8),
                      child: Row(
                        children: [
                          SvgPicture.asset(
                            iconIndex[3],
                            height: 18.sp,
                            width: 18.sp,
                          ),
                          SizedBox(
                            width: 2.w,
                          ),
                          const Text(
                            AppStrings.postAd,
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: AppFonts.poppins,
                                fontWeight: FontWeight.w200),
                          )
                        ],
                      ),
                    ))
                : SvgPicture.asset(
                    iconIndex[3],
                    height: 18.sp,
                    width: 18.sp,
                  ),
          )
        ]),
      ),
    );
  }
}
