import 'package:AwesomeSkills/constants/app_fonts.dart';
import 'package:AwesomeSkills/local/db/local_storage.dart';
import 'package:AwesomeSkills/routes/app_pages.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/bottom_sheet_widget.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';

import '../../constants/app_colors.dart';
import '../../constants/app_strings.dart';

class CustomBottomAppBarController extends GetxController {
  var selectedIcon = '';

  selectedIconAction(selectedIcon) {
    selectedIcon = selectedIcon;
    update();
  }

  switchHighlight(selectedIcon) {
    if (selectedIcon == selectedIcon) {
      return AppColors.buttonBlueColor;
    } else {
      return AppColors.buttonGreyColor;
    }
  }

  switchHighlightColor(selectedIcon) {
    if (selectedIcon == selectedIcon) {
      return Colors.white;
    } else {
      return AppColors.blueColor;
    }
  }
}
