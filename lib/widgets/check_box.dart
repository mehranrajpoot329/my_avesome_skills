import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:flutter/material.dart';

class LabeledCheckbox extends StatelessWidget {
  const LabeledCheckbox({
    Key? key,
    required this.label,
    required this.contentPadding,
    required this.value,
    required this.onTap,
    required this.activeColor,
    required this.labelTextStyle,
    this.gap = 4.0,
  }) : super(key: key);

  final String label;
  final TextStyle labelTextStyle;
  final EdgeInsets contentPadding;
  final bool value;
  final Function onTap;
  final Color activeColor;
  final double gap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onTap(!value),
      child: Padding(
        padding: contentPadding,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Checkbox(
              value: value,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(3)),
              side: const BorderSide(color: AppColors.blueColor, width: 1.5),
              checkColor: Colors.white,
              activeColor: AppColors.blueColor,
              onChanged: (val) => onTap(val),
            ),
            SizedBox(
              width: gap,
            ), // you can control gap between checkbox and label with this field
            Flexible(
              child: Text(label, style: labelTextStyle),
            ),
          ],
        ),
      ),
    );
  }
}
