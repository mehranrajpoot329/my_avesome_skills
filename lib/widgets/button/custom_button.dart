import 'package:flutter/material.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';

class CustomButton extends StatelessWidget {
  double height;
  double width;
  String buttonName;
  Color buttonColor;
  Function onTap;
  TextStyle textStyle;
  BorderRadius borderRadius;

  CustomButton(
      {required this.buttonName,
      required this.width,
      required this.height,
      required this.buttonColor,
      required this.textStyle,
      required this.borderRadius,
      required this.onTap});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      decoration: BoxDecoration(color: buttonColor, borderRadius: borderRadius),
      child: MaterialButton(
        onPressed: () => onTap(),
        child: FittedBox(
          child: Center(
            child: Text(
              buttonName,
              style: textStyle,
            ),
          ),
        ),
      ),
    );
  }
}
