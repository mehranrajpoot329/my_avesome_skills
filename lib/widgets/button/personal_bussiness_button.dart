import 'package:AwesomeSkills/Modules/login_signup/signup_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';

import 'package:AwesomeSkills/constants/app_fonts.dart';
import 'package:AwesomeSkills/routes/app_pages.dart';
import 'package:sizer/sizer.dart';

class PersonalBusinessButton extends StatelessWidget {
  final controller = Get.put(SignUpController());

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        GetBuilder<SignUpController>(
          builder: (_) {
            return GestureDetector(
              onTap: () {
                controller.selectedAccount(AppStrings.personalAccount);
                controller.pageViewController.animateToPage(0,
                    duration: Duration(seconds: 1), curve: Curves.easeIn);
              },
              child: Container(
                height: 6.h,
                width: MediaQuery.of(context).size.width * 0.44,
                decoration: BoxDecoration(
                  color: controller.switchHighlight(AppStrings.personalAccount),
                  //    color: ,
                  borderRadius: BorderRadius.circular(8.0),
                ),
                child: Center(
                    child: Text(AppStrings.personalAccount,
                        style: TextStyle(
                            fontFamily: AppFonts.poppins,
                            fontSize: 12,
                            color: controller.switchHighlightColor(
                                AppStrings.personalAccount)))

                    // customButtonTextStyle,
                    ),
              ),
            );
          },
        ),
        GetBuilder<SignUpController>(
          builder: (_) {
            return GestureDetector(
              onTap: () {
                controller.isBusinessAccountSelection;
                controller.selectedAccount(AppStrings.businessAccount);
                controller.pageViewController.animateToPage(1,
                    duration: Duration(seconds: 1), curve: Curves.easeIn);
              },
              child: Container(
                height: 6.h,
                width: MediaQuery.of(context).size.width * 0.44,
                decoration: BoxDecoration(
                  color: controller.switchHighlight(AppStrings.businessAccount),
                  borderRadius: BorderRadius.circular(8.0),
                ),
                child: Center(
                  child: Text(
                    AppStrings.businessAccount,
                    style: TextStyle(
                      fontFamily: AppFonts.poppins,
                      fontSize: 12,
                      color: controller
                          .switchHighlightColor(AppStrings.businessAccount),
                    ),

                    // customButtonTextStyle,
                  ),
                ),
              ),
            );
          },
        ),
      ],
    );
  }
}
