import 'package:flutter/material.dart';

class CustomIconButton extends StatelessWidget {
  double height;
  double width;
  String buttonName;
  Color buttonColor;
  Function onTap;
  TextStyle textStyle;
  BorderRadius borderRadius;

  CustomIconButton(
      {required this.buttonName,
      required this.width,
      required this.height,
      required this.buttonColor,
      required this.textStyle,
      required this.borderRadius,
      required this.onTap});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      decoration: BoxDecoration(color: buttonColor, borderRadius: borderRadius),
      child: MaterialButton(
        onPressed: () => onTap(),
        child: Center(
          child: Row(
            children: [
              Container(
                height: 20,
                width: 20,
                decoration: BoxDecoration(
                  color: Colors.white70,
                  shape: BoxShape.circle,
                ),
                child: Center(
                    child: Icon(
                  Icons.add,
                  size: 15,
                  color: Colors.white,
                )),
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                buttonName,
                style: textStyle,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
