import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/routes/app_pages.dart';

import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:sizer/sizer.dart';

import '../../Constants/app_strings.dart';
import '../../Constants/text_style.dart';
import '../../constants/app_colors.dart';

class SavePreviewPostButton extends StatelessWidget {
  Function saveOnTap, previewAdOnTap, postAdOnTap;

  SavePreviewPostButton(
      {required this.saveOnTap,
      required this.previewAdOnTap,
      required this.postAdOnTap});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Expanded(
          child: CustomButton(
              buttonName: AppStrings.saveForLater,
              width: 33.w,
              height: 5.h,
              buttonColor: AppColors.lightBlue2Color,
              textStyle: black14Color,
              borderRadius: BorderRadius.circular(10),
              onTap: () => saveOnTap()),
        ),
        SizedBox(
          width: 1.w,
        ),
        Expanded(
          child: CustomButton(
              buttonName: AppStrings.previewAd,
              width: 30.w,
              height: 5.h,
              buttonColor: AppColors.indigoColor,
              textStyle: white14Color,
              borderRadius: BorderRadius.circular(10),
              onTap: () => previewAdOnTap()),
        ),
        SizedBox(
          width: 1.w,
        ),
        Expanded(
            child: CustomButton(
                buttonName: AppStrings.postAd,
                width: 30.w,
                height: 5.h,
                buttonColor: AppColors.green10Color,
                textStyle: white14Color,
                borderRadius: BorderRadius.circular(10),
                onTap: () => postAdOnTap())),
      ],
    );
  }
}
