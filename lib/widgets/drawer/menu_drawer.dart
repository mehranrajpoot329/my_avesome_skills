import 'package:AwesomeSkills/Modules/Business/custom_scroll_appBar_business/custom_app_bar_business_controller.dart';
import 'package:AwesomeSkills/Modules/Personal/contact_us/contact_us_controller.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/landing_screen_controller.dart';
import 'package:AwesomeSkills/local/db/local_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/search_controller.dart';
import 'package:AwesomeSkills/constants/app_colors.dart';
import 'package:AwesomeSkills/routes/app_pages.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/custom_app_bar_controller.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/bottom_sheet_widget.dart';
import 'package:sizer/sizer.dart';

class MenuDrawer extends StatelessWidget {
  MenuDrawer({Key? key}) : super(key: key);
  static final customAppBar = Get.put(CustomAppBarController());
  final contactController = Get.put(ContactUsController());
  final landingScreenController = Get.put(LandingScreenController());
  final customAppBarBusinessController =
      Get.put(CustomAppBarBusinessController());
  String selectedItem = AppStrings.personalAccount;

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      icon: const Icon(
        Icons.notes,
        color: Colors.black,
      ),
      itemBuilder: (context) => [
        PopupMenuItem(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GestureDetector(
              onTap: () => Get.toNamed(AppPage.needHelpScreenWithOutBottomNav),
              child: BuildWidgetDrawer(
                textName: AppStrings.needHelp,
              ),
            ),
            const Divider(),
            GestureDetector(
              onTap: () => Get.toNamed(AppPage.canHelpScreenWithOutBottomNav),
              child: BuildWidgetDrawer(
                textName: AppStrings.canHelpWith,
              ),
            ),
            const Divider(),
            BuildWidgetDrawer(
              textName: AppStrings.pricing,
            ),
            const Divider(),
            InkWell(
              onTap: () {
                Get.back();
                HiveHelper.isLoggedIn()
                    ? Get.toNamed(AppPage.listYourBusiness)
                    : BottomSheetAdScreen.isLoginedOutBottomSheet(context);
              },
              child: BuildWidgetDrawer(
                textName: AppStrings.listYourBusiness,
              ),
            ),
            const Divider(),
            BuildWidgetDrawer(
              textName: AppStrings.aboutUs,
            ),
            const Divider(),
            BuildWidgetDrawer(
              textName: AppStrings.howItWorks,
            ),
            const Divider(),
            BuildWidgetDrawer(
              textName: AppStrings.myCommunity,
            ),
            const Divider(),
            GestureDetector(
              onTap: () {
                HiveHelper.isLoggedIn() ||
                        customAppBarBusinessController.isLogined
                    ? Get.toNamed(AppPage.contactUsScreen)
                    : BottomSheetAdScreen.isLoginedOutBottomSheet(context);
              },
              child: BuildWidgetDrawer(
                textName: AppStrings.contactUs,
              ),
            ),
            const Divider(),
            GestureDetector(
              onTap: () {
                Get.back();
                HiveHelper.saveUserLogout(isLoggedOut: true);
                HiveHelper.saveUserLogin(isLoggedIn: false);
                Get.toNamed(AppPage.loginScreen);
              },
              child: BuildWidgetDrawer(
                textName: AppStrings.logOut,
              ),
            ),
          ],
        ))
      ],
    );
  }
}

class BuildWidgetDrawer extends StatelessWidget {
  String textName;

  BuildWidgetDrawer({
    required this.textName,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 0.5.h, left: 3.w, right: 2.w),
      child: Text(
        textName,
        style: blackBold2Color,
      ),
    );
  }
}

class SearchMenu extends StatelessWidget {
  final searchController = Get.put(SearchController());

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
        icon: Icon(
          Icons.add_road_outlined,
          color: Colors.black,
          size: 18.sp,
        ),
        itemBuilder: (context) => [
              PopupMenuItem(
                child: AnimatedContainer(
                  height: 250,
                  width: Get.width,
                  duration: const Duration(seconds: 3),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          bottomRight: Radius.circular(30),
                          bottomLeft: Radius.circular(30))),
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Row(children: [
                          const Expanded(
                            child: Text(
                              AppStrings.iNeedHelp,
                              style: grey14Color,
                            ),
                          ),
                          Obx(
                            () => Checkbox(
                              value: searchController.needHelp.value,
                              shape: const CircleBorder(),
                              side: const BorderSide(
                                  color: AppColors.grey4Color, width: 1.5),
                              checkColor: Colors.white,
                              activeColor: AppColors.green3Color,
                              onChanged: (value) =>
                                  searchController.needHelp.value =
                                      value ?? !searchController.needHelp.value,
                            ),
                          ),
                        ]),
                        Row(children: [
                          const Expanded(
                            child: Text(
                              AppStrings.canHelpWith,
                              style: grey14Color,
                            ),
                          ),
                          Obx(
                            () => Checkbox(
                              value: searchController.canNeedHelp.value,
                              shape: const CircleBorder(),
                              side: const BorderSide(
                                  color: AppColors.grey4Color, width: 1.5),
                              checkColor: Colors.white,
                              activeColor: AppColors.green3Color,
                              onChanged: (value) => searchController
                                      .canNeedHelp.value =
                                  value ?? !searchController.canNeedHelp.value,
                            ),
                          ),
                        ]),
                        Row(children: [
                          const Expanded(
                            child: Text(
                              AppStrings.useCurrentLocation,
                              style: grey14Color,
                            ),
                          ),
                          Obx(
                            () => Checkbox(
                              value: searchController.useCurrentLocation.value,
                              shape: const CircleBorder(),
                              side: const BorderSide(
                                  color: AppColors.grey4Color, width: 1.5),
                              checkColor: Colors.white,
                              activeColor: AppColors.green3Color,
                              onChanged: (value) =>
                                  searchController.useCurrentLocation.value =
                                      value ??
                                          !searchController
                                              .useCurrentLocation.value,
                            ),
                          ),
                        ]),
                        Row(
                          children: [
                            Text(
                              AppStrings.distance,
                              style: grey14Color,
                            ),
                            Expanded(
                              child: GetBuilder<SearchController>(builder: (_) {
                                return Slider(
                                    thumbColor: AppColors.lightBlueColor,
                                    activeColor: AppColors.lightBlueColor,
                                    inactiveColor: AppColors.lightBlueColor
                                        .withOpacity(0.2),
                                    max: 100,
                                    min: 0,
                                    divisions: 20,
                                    label:
                                        "${searchController.distance.round().toString()} Km",
                                    value: searchController.distance,
                                    onChanged: (newValue) => searchController
                                        .changeDistance(newValue));
                              }),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Text(
                              AppStrings.priceRange,
                              style: grey14Color,
                            ),
                            Expanded(
                              child: GetBuilder<SearchController>(builder: (_) {
                                return SliderTheme(
                                  data: SliderThemeData(trackHeight: 0.2.h),
                                  child: RangeSlider(
                                      activeColor: AppColors.lightBlueColor,
                                      inactiveColor: AppColors.lightBlueColor
                                          .withOpacity(0.2),
                                      max: 100,
                                      min: 0,
                                      divisions: 20,
                                      labels: RangeLabels(
                                        "\$${searchController.priceRange.start.round().toString()}",
                                        "\$${searchController.priceRange.end.round().toString()}",
                                      ),
                                      values: searchController.priceRange,
                                      onChanged: (newValue) => searchController
                                          .changePriceRange(newValue)),
                                );
                              }),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 2.h,
                        ),
                      ],
                    ),
                  ),
                ),
              )
            ]);
  }
}
