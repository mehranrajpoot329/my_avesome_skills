import 'package:AwesomeSkills/constants/app_colors.dart';
import 'package:AwesomeSkills/widgets/drawer/business/drawer_business_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';

import '../../../Constants/app_strings.dart';
import '../../../Constants/text_style.dart';
import '../../../constants/app_fonts.dart';
import '../../../routes/app_pages.dart';
import 'drawer_profile_over_view_business_controller.dart';

class UserInfoDrawerBusiness extends StatelessWidget {
  final drawerController = Get.put(DrawerBusinessController());
  final controller = Get.put(DrawerProfileOverViewBusinessController());

  @override
  Widget build(BuildContext context) {
    return Drawer(
      width: 66.w,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            buildHeader(context),
            SizedBox(
              height: 1.h,
            ),
            buildBody(context)
          ],
        ),
      ),
    );
  }

  Widget buildHeader(BuildContext context) {
    return Container(
      height: 31.h,
      decoration: BoxDecoration(
          color: AppColors.lightBlueColor,
          borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30)),
          boxShadow: [
            BoxShadow(
                blurRadius: 4.0,
                spreadRadius: 2.0,
                color: Colors.grey.withOpacity(0.2))
          ]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 5.h),
          InkWell(
            onTap: () {
              controller.selectedAccount();
              Get.toNamed(AppPage.profileOverViewBusiness);
            },
            child: Container(
              height: 90,
              width: 90,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(
                      color: Colors.white, width: 1, style: BorderStyle.solid),
                  image: const DecorationImage(
                      image: AssetImage('assets/images/business_profile.png'),
                      fit: BoxFit.cover)),
              child: Stack(
                clipBehavior: Clip.none,
                children: [
                  Positioned(
                    bottom: 0.0,
                    right: -1.0,
                    child: SvgPicture.asset(
                        'assets/images/post_icon/company_rate.svg'),
                    height: 20.sp,
                    width: 20.sp,
                  )
                ],
              ),
            ),
          ),
          SizedBox(
            height: 2.h,
          ),
          Text(
            AppStrings.astria,
            style: montserratTextStyle,
          ),
          SizedBox(
            height: 1.h,
          ),
          Text(
            AppStrings.memberSince,
            style: montserrat400TextStyle,
          ),
        ],
      ),
    );
  }

  Widget buildBody(BuildContext context) {
    return GetBuilder<DrawerBusinessController>(
      builder: (_) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            buildRowWidget(
                index: 0,
                onTap: () {
                  Get.back();
                  Get.toNamed(AppPage.myTaskBusinessAccount);
                  drawerController.selectedItem(0);
                }),
            const Divider(),
            buildRowWidget(
                index: 1,
                onTap: () {
                  Get.back();
                  Get.toNamed(AppPage.myAdsBusinessScreen);
                  drawerController.selectedItem(1);
                }),
            const Divider(),
            buildRowWidget(
                index: 2,
                onTap: () {
                  Get.back();
                  Get.toNamed(AppPage.mySkillsBusiness);
                  drawerController.selectedItem(2);
                }),
            const Divider(),
            buildRowWidget(
                index: 3,
                onTap: () {
                  Get.back();
                  Get.toNamed(AppPage.myQuotesBusiness);
                  drawerController.selectedItem(3);
                }),
            const Divider(),
            buildRowWidget(
                index: 4,
                onTap: () {
                  Get.back();
                  Get.toNamed(AppPage.myAnalyticsBusiness);
                  drawerController.selectedItem(4);
                }),
            const Divider(),
            buildRowWidget(
                index: 5,
                onTap: () {
                  Get.back();
                  Get.toNamed(AppPage.myMessageBusiness);
                  drawerController.selectedItem(5);
                }),
            const Divider(),
            buildRowWidget(
                index: 6,
                onTap: () {
                  Get.back();
                  Get.toNamed(AppPage.myReviewsBusiness);
                  drawerController.selectedItem(6);
                }),
            const Divider(),
            buildRowWidget(
                index: 7,
                onTap: () {
                  Get.back();
                  Get.toNamed(AppPage.myFavouritesBusiness);
                  drawerController.selectedItem(7);
                }),
            const Divider(),
            Theme(
              data:
                  Theme.of(context).copyWith(dividerColor: Colors.transparent),
              child: ListTileTheme(
                contentPadding: EdgeInsets.all(0),
                dense: true,
                horizontalTitleGap: 0.0,
                minLeadingWidth: 0,
                child: ExpansionTile(
                  tilePadding: EdgeInsets.only(right: 15.w),
                  childrenPadding: EdgeInsets.zero,
                  iconColor: AppColors.indigoColor,
                  backgroundColor: drawerController.selectedItemIndex == 8
                      ? AppColors.lightBlueColor
                      : Colors.transparent,
                  title: GestureDetector(
                    onTap: () {
                      Get.back();
                      Get.toNamed(AppPage.mySettingsBusiness);
                    },
                    child: Padding(
                      padding: EdgeInsets.only(left: 8.w),
                      child: Row(
                        children: [
                          SvgPicture.asset(
                            drawerController.selectedItemIndex == 8
                                ? drawerController
                                    .drawerItemSelectedImageList[8]
                                : drawerController.drawerItemImageList[8],
                          ),
                          SizedBox(
                            width: 8.w,
                          ),
                          Text(drawerController.item[8],
                              style: TextStyle(
                                fontFamily: AppFonts.montserrat,
                                fontSize: 12.sp,
                                fontWeight: FontWeight.w500,
                                color: drawerController.selectedItemIndex == 8
                                    ? Colors.white
                                    : Colors.black,
                              )),
                        ],
                      ),
                    ),
                  ),
                  children: [
                    Divider(
                      color: Colors.grey.withOpacity(0.4),
                    ),
                    buildExpansionSettings(
                        index: 9,
                        onTap: () {
                          Get.back();
                          Get.toNamed(AppPage.companyDetailsSettingBusiness);
                          drawerController.selectedItem(9);
                        }),
                    Divider(
                      color: Colors.grey.withOpacity(0.4),
                    ),
                    buildExpansionSettings(
                        index: 10,
                        onTap: () {
                          Get.back();
                          Get.toNamed(AppPage.aboutUsBusiness);
                          drawerController.selectedItem(10);
                        }),
                    Divider(
                      color: Colors.grey.withOpacity(0.4),
                    ),
                    buildExpansionSettings(
                        index: 11,
                        onTap: () {
                          Get.back();
                          Get.toNamed(AppPage.serviceBusiness);
                          drawerController.selectedItem(11);
                        }),
                    Divider(
                      color: Colors.grey.withOpacity(0.4),
                    ),
                    buildExpansionSettings(
                        index: 12,
                        onTap: () {
                          Get.back();
                          Get.toNamed(AppPage.companyLifeBusiness);
                          drawerController.selectedItem(12);
                        }),
                    Divider(
                      color: Colors.grey.withOpacity(0.4),
                    ),
                    buildExpansionSettings(
                        index: 13,
                        onTap: () {
                          Get.back();
                          Get.toNamed(AppPage.alertNotificationBusiness);
                          drawerController.selectedItem(13);
                        }),
                    Divider(
                      color: Colors.grey.withOpacity(0.4),
                    ),
                    buildExpansionSettings(
                        index: 14,
                        onTap: () {
                          Get.back();
                          Get.toNamed(AppPage.securityPasswordSettingBusiness);
                          drawerController.selectedItem(14);
                        }),
                    Divider(
                      color: Colors.grey.withOpacity(0.4),
                    ),
                    buildExpansionSettings(
                        index: 15,
                        onTap: () {
                          Get.back();
                          Get.toNamed(AppPage.receiveLeadingSettingBusiness);
                          drawerController.selectedItem(15);
                        }),
                    Divider(
                      color: Colors.grey.withOpacity(0.4),
                    ),
                    buildExpansionSettings(
                        index: 16,
                        onTap: () {
                          Get.back();
                          Get.toNamed(AppPage.socialMediaSettingBusiness);
                          drawerController.selectedItem(16);
                        }),
                    Divider(
                      color: Colors.grey.withOpacity(0.4),
                    ),
                    buildExpansionSettings(
                        index: 17,
                        onTap: () {
                          Get.back();
                          Get.toNamed(AppPage.freePlanSettingsBusiness);
                          drawerController.selectedItem(17);
                        }),
                    Divider(
                      color: Colors.grey.withOpacity(0.4),
                    ),
                    buildExpansionSettings(
                        index: 18,
                        onTap: () {
                          Get.back();
                          Get.toNamed(AppPage.paymentMethodSettingsBusiness);
                          drawerController.selectedItem(18);
                        }),
                    Divider(
                      color: Colors.grey.withOpacity(0.4),
                    ),
                  ],
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  Widget buildRowWidget({required int index, required Function onTap}) {
    return Container(
      padding: EdgeInsets.only(
        top: 1.h,
      ),
      width: Get.width,
      height: 32,
      decoration: BoxDecoration(
          color: drawerController.selectedItemIndex == index
              ? AppColors.lightBlue4Color
              : Colors.transparent),
      child: Column(
        children: [
          GestureDetector(
            onTap: () {
              onTap();
            },
            child: SizedBox(
              width: 50.w,
              child: Row(
                children: [
                  SvgPicture.asset(
                    drawerController.selectedItemIndex == index
                        ? drawerController.drawerItemSelectedImageList[index]
                        : drawerController.drawerItemImageList[index],
                  ),
                  SizedBox(
                    width: 8.w,
                  ),
                  Text(drawerController.item[index],
                      style: TextStyle(
                        fontFamily: AppFonts.montserrat,
                        fontSize: 12.sp,
                        fontWeight: FontWeight.w500,
                        color: drawerController.selectedItemIndex == index
                            ? Colors.white
                            : Colors.black,
                      )),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildExpansionSettings({required int index, required Function onTap}) {
    return Container(
      padding: EdgeInsets.only(top: 1.h, bottom: 0.2.h),
      width: Get.width,
      height: 32,
      decoration: BoxDecoration(
          color: drawerController.selectedItemIndex == index
              ? AppColors.lightBlue4Color
              : Colors.transparent),
      child: Padding(
        padding: EdgeInsets.only(left: 7.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            GestureDetector(
              onTap: () {
                onTap();
              },
              child: SizedBox(
                width: 50.w,
                child: Row(
                  children: [
                    Expanded(
                      child: drawerController.selectedItemIndex == index
                          ? SvgPicture.asset(
                              drawerController
                                  .drawerItemSelectedImageList[index],
                              color: Colors.white,
                            )
                          : SvgPicture.asset(
                              drawerController.drawerItemImageList[index],
                            ),
                    ),
                    SizedBox(
                      width: 4.w,
                    ),
                    Expanded(
                      flex: 10,
                      child: Text(drawerController.item[index],
                          style: TextStyle(
                            fontFamily: AppFonts.montserrat,
                            fontSize: 12.sp,
                            fontWeight: FontWeight.w500,
                            color: drawerController.selectedItemIndex == index
                                ? Colors.white
                                : Colors.black,
                          )),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
