import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../constants/app_colors.dart';
import '../../../constants/app_strings.dart';

class DrawerBusinessController extends GetxController {
  var selectedItemIndex;

  Color drawerItemColor = AppColors.black;
  Color drawerSelectedItemColor = Colors.blue;

  final item = [
    AppStrings.myTask,
    AppStrings.myAds,
    AppStrings.mySkills,
    AppStrings.myQuotes,
    AppStrings.analytics,
    AppStrings.message,
    AppStrings.reviews,
    AppStrings.favourite,
    AppStrings.settings,
    AppStrings.companyDetails,
    AppStrings.aboutUs,
    AppStrings.services,
    AppStrings.companyLife,
    AppStrings.alertNotifications,
    AppStrings.security,
    AppStrings.receivingLeads,
    AppStrings.socialMedia,
    AppStrings.myPlan,
    AppStrings.paymentMethod,
  ];

  final drawerItemImageList = [
    'assets/images/drawer_icon/my_task.svg',
    'assets/images/drawer_icon/my_ads.svg',
    'assets/images/drawer_icon/my_skill.svg',
    'assets/images/drawer_icon/my_quote.svg',
    'assets/images/drawer_icon/analytics.svg',
    'assets/images/drawer_icon/message.svg',
    'assets/images/drawer_icon/review.svg',
    'assets/images/drawer_icon/favourite.svg',
    'assets/images/drawer_icon/settings.svg',
    'assets/images/drawer_icon/company_details.svg',
    'assets/images/drawer_icon/about_us.svg',
    'assets/images/drawer_icon/services.svg',
    'assets/images/drawer_icon/company_life.svg',
    'assets/images/drawer_icon/alert_notification.svg',
    'assets/images/drawer_icon/lock.svg',
    'assets/images/drawer_icon/receiving.svg',
    'assets/images/drawer_icon/social_media.svg',
    'assets/images/drawer_icon/my_plan.svg',
    'assets/images/drawer_icon/payment_method.svg',
  ];
  final drawerItemSelectedImageList = [
    'assets/images/drawer_icon/my_task_white.svg',
    'assets/images/drawer_icon/myAds_white.svg',
    'assets/images/drawer_icon/my_skill_white.svg',
    'assets/images/drawer_icon/my_quotes_white.svg',
    'assets/images/drawer_icon/analytics_white.svg',
    'assets/images/drawer_icon/message_white.svg',
    'assets/images/drawer_icon/review_white.svg',
    'assets/images/drawer_icon/favourite_white.svg',
    'assets/images/drawer_icon/settings_white.svg',
    'assets/images/drawer_icon/company_details.svg',
    'assets/images/drawer_icon/about_us.svg',
    'assets/images/drawer_icon/services.svg',
    'assets/images/drawer_icon/company_life.svg',
    'assets/images/drawer_icon/alert_notification.svg',
    'assets/images/drawer_icon/lock.svg',
    'assets/images/drawer_icon/receiving.svg',
    'assets/images/drawer_icon/social_media.svg',
    'assets/images/drawer_icon/my_plan.svg',
    'assets/images/drawer_icon/payment_method.svg',
  ];

  selectedItem(int index) {
    selectedItemIndex = index;
    update();
  }
}
