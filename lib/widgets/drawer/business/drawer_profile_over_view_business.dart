import 'package:AwesomeSkills/Modules/Business/custom_scroll_appBar_business/custom_app_bar_business_controller.dart';
import 'package:AwesomeSkills/widgets/drawer/business/drawer_profile_over_view_business_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';

import '../../../Constants/text_style.dart';
import '../../../constants/app_colors.dart';
import '../../../constants/app_strings.dart';
import '../../../routes/app_pages.dart';
import '../../button/custom_button.dart';
import '../../custom_widget_app/rating_star.dart';

class DrawerProfileOverViewBusiness extends StatelessWidget {
  DrawerProfileOverViewBusiness({Key? key}) : super(key: key);

  final controller = Get.put(DrawerProfileOverViewBusinessController());
  final customAppBarBusinessController =
      Get.put(CustomAppBarBusinessController());

  @override
  Widget build(BuildContext context) {
    return Drawer(
      width: 65.w,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            buildHeader(context),
            SizedBox(
              height: 2.h,
            ),
            buildBody(context),
            SizedBox(
              height: 3.h,
            ),
          ],
        ),
      ),
    );
  }

  Widget buildHeader(BuildContext context) {
    return Container(
      height: 27.5.h,
      decoration: BoxDecoration(color: AppColors.indigoColor, boxShadow: [
        BoxShadow(
            blurRadius: 4.0,
            spreadRadius: 2.0,
            color: Colors.grey.withOpacity(0.2))
      ]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 5.h),
          InkWell(
            onTap: () {
              controller.selectedAccountBack();
              Get.back();
            },
            child: Container(
              height: 90,
              width: 90,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(
                      color: Colors.white, width: 1, style: BorderStyle.solid),
                  image: const DecorationImage(
                      image: AssetImage('assets/images/business_profile.png'),
                      fit: BoxFit.cover)),
              child: Stack(
                clipBehavior: Clip.none,
                children: [
                  Positioned(
                    bottom: 0.0,
                    right: -1.0,
                    child: SvgPicture.asset(
                        'assets/images/post_icon/company_rate.svg'),
                    height: 20.sp,
                    width: 20.sp,
                  )
                ],
              ),
            ),
          ),
          SizedBox(
            height: 2.h,
          ),
          Text(
            AppStrings.company,
            style: montserratTextStyle,
          ),
          SizedBox(
            height: 1.h,
          ),
          Padding(
            padding: EdgeInsets.only(left: 2.w),
            child: SizedBox(
              width: 35.w,
              child: RatingStar(
                  isNumberRating: true,
                  ratingNumber: '23',
                  textStyle: customButtonTextStyle),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildBody(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        bodyText(
            textName: AppStrings.responseRate, value: AppStrings.ninetyEight),
        bodyText(
            textName: AppStrings.responseTime, value: AppStrings.thirtyMin),
        bodyText(textName: AppStrings.jobSuccess, value: AppStrings.ninetyNine),
        SizedBox(
          height: 2.h,
        ),
        detailText(textName: AppStrings.bundallQLD),
        detailText(textName: AppStrings.phoneNumber),
        detailText(textName: AppStrings.telephone),
        detailText(textName: AppStrings.emailSpam),
        SizedBox(
          height: 1.h,
        ),
        detailText(textName: AppStrings.memberSince),
        detailText(textName: AppStrings.qbcc),
        detailText(textName: AppStrings.abn),
        detailText(textName: AppStrings.viewABNDetails),
        Align(
          alignment: Alignment.topCenter,
          child: CustomButton(
            buttonName: AppStrings.getQuotes,
            width: 50.w,
            height: 6.h,
            buttonColor: AppColors.lightBlue2Color,
            textStyle: black14Color,
            borderRadius: BorderRadius.circular(10),
            onTap: () => Get.toNamed(AppPage.getQuotesScreen),
          ),
        ),
        SizedBox(
          height: 2.h,
        ),
        Align(
          alignment: Alignment.topCenter,
          child: CustomButton(
              buttonName: AppStrings.message,
              width: 50.w,
              height: 6.h,
              buttonColor: AppColors.blueColor,
              textStyle: customButtonTextStyle,
              borderRadius: BorderRadius.circular(10),
              onTap: () {
                Get.toNamed(AppPage.canHelpMessageScreenBusiness);
              }),
        ),
        SizedBox(
          height: 2.h,
        ),
        Align(
          alignment: Alignment.topCenter,
          child: CustomButton(
            buttonName: AppStrings.call,
            width: 50.w,
            height: 6.h,
            buttonColor: AppColors.green5Color,
            textStyle: black14Color,
            borderRadius: BorderRadius.circular(10),
            onTap: () {},
          ),
        ),
      ],
    );
  }

  Widget bodyText({required String textName, required String value}) {
    return Padding(
      padding: EdgeInsets.only(
        left: 5.w,
      ),
      child: Column(
        children: [
          Row(
            children: [
              Text(
                textName,
                style: black14Color,
              ),
              SizedBox(
                width: 1.w,
              ),
              Text(
                ':',
                style: black14Color,
              ),
              SizedBox(
                width: 1.w,
              ),
              Text(
                value,
                style: blue12ColorTextStyle,
              ),
            ],
          ),
          SizedBox(
            height: 1.h,
          ),
        ],
      ),
    );
  }

  Widget detailText({required String textName}) {
    return Padding(
      padding: EdgeInsets.only(
        left: 5.w,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            textName,
            style: black14Color,
          ),
          SizedBox(
            height: 1.h,
          ),
        ],
      ),
    );
  }
}
