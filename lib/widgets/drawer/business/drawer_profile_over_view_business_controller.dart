import 'package:get/get.dart';
import 'package:AwesomeSkills/widgets/drawer/business/drawer_profile_over_view_business.dart';
import 'package:AwesomeSkills/widgets/drawer/business/user_info_drawer_business.dart';

class DrawerProfileOverViewBusinessController extends GetxController {
  RxBool selectItem = false.obs;
  selectedAccount() {
    selectItem.value = true;
  }

  selectedAccountBack() {
    selectItem.value = false;
  }

  switchDrawer() {
    if (selectItem.isTrue) {
      return DrawerProfileOverViewBusiness();
    } else {
      return UserInfoDrawerBusiness();
    }
  }
}
