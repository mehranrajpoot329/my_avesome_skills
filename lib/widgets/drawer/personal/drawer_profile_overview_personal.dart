import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/routes/app_pages.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:AwesomeSkills/widgets/drawer/personal/drawer_profile_overview_personal_controller.dart';
import 'package:sizer/sizer.dart';

import '../../../Constants/app_colors.dart';
import '../../../Constants/app_strings.dart';
import '../../../Constants/text_style.dart';
import '../../custom_widget_app/rating_star.dart';

class DrawerProfileOverViewPersonal extends StatelessWidget {
  final controller = Get.put(ProfileOverviewControllerPersonal());

  @override
  Widget build(BuildContext context) {
    return Drawer(
      width: 65.w,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            buildHeader(context),
            SizedBox(
              height: 2.h,
            ),
            buildBody(context),
            SizedBox(
              height: 3.h,
            ),
          ],
        ),
      ),
    );
  }

  Widget buildHeader(BuildContext context) {
    return Container(
      height: 31.5.h,
      decoration: BoxDecoration(
          color: AppColors.lightBlueColor,
          borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30)),
          boxShadow: [
            BoxShadow(
                blurRadius: 4.0,
                spreadRadius: 2.0,
                color: Colors.grey.withOpacity(0.2))
          ]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 5.h),
          InkWell(
            onTap: () => controller.selectedAccountBack(),
            child: Container(
              height: 90,
              width: 90,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(
                      color: Colors.white, width: 1, style: BorderStyle.solid),
                  image: const DecorationImage(
                      image: AssetImage('assets/images/profile_pic2.png'),
                      fit: BoxFit.cover)),
            ),
          ),
          SizedBox(
            height: 2.h,
          ),
          Text(
            AppStrings.astria,
            style: montserratTextStyle,
          ),
          SizedBox(
            height: 1.h,
          ),
          Text(
            AppStrings.memberSince,
            style: montserrat400TextStyle,
          ),
          SizedBox(
            height: 1.h,
          ),
          Padding(
            padding: EdgeInsets.only(left: 2.w),
            child: SizedBox(
              width: 35.w,
              child: RatingStar(
                textStyle: customButtonTextStyle,
                isNumberRating: true,
                ratingNumber: '23',
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildBody(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        bodyText(
            textName: AppStrings.responseRate, value: AppStrings.ninetyEight),
        bodyText(
            textName: AppStrings.responseTime, value: AppStrings.thirtyMin),
        bodyText(textName: AppStrings.jobSuccess, value: AppStrings.ninetyNine),
        bodyText(
            textName: AppStrings.recommendations, value: AppStrings.zeroFour),
        Padding(
          padding: EdgeInsets.only(
            left: 5.w,
          ),
          child: Text(
            AppStrings.featuredSkill,
            style: lightBlue16ColorTextStyle,
          ),
        ),
        SizedBox(
          height: 2.h,
        ),
        bodyText(
          textName: AppStrings.helpMoving,
          value: AppStrings.fortyOne,
        ),
        bodyText(
          textName: AppStrings.homeRepairs,
          value: AppStrings.thirtySeven,
        ),
        bodyText(
          textName: AppStrings.furnitureAssembly,
          value: AppStrings.thirtyFive,
        ),
        SizedBox(
          height: 2.h,
        ),
        detailText(textName: AppStrings.bundallQLD),
        detailText(textName: AppStrings.phoneNumber),
        detailText(textName: AppStrings.telephone),
        detailText(textName: AppStrings.emailSpam),
        SizedBox(
          height: 1.h,
        ),
        detailText(textName: AppStrings.memberSince),
        detailText(textName: AppStrings.qbcc),
        detailText(textName: AppStrings.abn),
        detailText(textName: AppStrings.viewABNDetails),
        Align(
          alignment: Alignment.topCenter,
          child: CustomButton(
            buttonName: AppStrings.call,
            width: 50.w,
            height: 6.h,
            buttonColor: AppColors.green5Color,
            textStyle: black14Color,
            borderRadius: BorderRadius.circular(10),
            onTap: () {},
          ),
        ),
        SizedBox(
          height: 3.h,
        ),
        Align(
          alignment: Alignment.topCenter,
          child: CustomButton(
            buttonName: AppStrings.message,
            width: 50.w,
            height: 6.h,
            buttonColor: AppColors.blueColor,
            textStyle: customButtonTextStyle,
            borderRadius: BorderRadius.circular(10),
            onTap: () => Get.toNamed(AppPage.canHelpMessageScreen),
          ),
        ),
      ],
    );
  }

  Widget bodyText({required String textName, required String value}) {
    return Padding(
      padding: EdgeInsets.only(
        left: 5.w,
      ),
      child: Column(
        children: [
          Row(
            children: [
              Text(
                textName,
                style: black14Color,
              ),
              SizedBox(
                width: 1.w,
              ),
              Text(
                ':',
                style: black14Color,
              ),
              SizedBox(
                width: 1.w,
              ),
              Text(
                value,
                style: blue12ColorTextStyle,
              ),
            ],
          ),
          SizedBox(
            height: 1.h,
          ),
        ],
      ),
    );
  }

  Widget detailText({required String textName}) {
    return Padding(
      padding: EdgeInsets.only(
        left: 5.w,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            textName,
            style: black14Color,
          ),
          SizedBox(
            height: 1.h,
          ),
        ],
      ),
    );
  }
}
