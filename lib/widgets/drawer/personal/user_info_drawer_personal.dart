import 'package:AwesomeSkills/local/db/local_storage.dart';
import 'package:AwesomeSkills/models/user_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/constants/app_fonts.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/routes/app_pages.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget_controller.dart';
import 'package:AwesomeSkills/widgets/drawer/personal/drawer__personal_controller.dart';
import 'package:AwesomeSkills/widgets/drawer/personal/drawer_profile_overview_personal_controller.dart';

import 'package:sizer/sizer.dart';

class UserInfoDrawerPersonal extends StatefulWidget {
  @override
  State<UserInfoDrawerPersonal> createState() => _UserInfoDrawerPersonalState();
}

class _UserInfoDrawerPersonalState extends State<UserInfoDrawerPersonal> {
  final controller = Get.put(DrawerPersonalController());
  final customController = Get.put(ProfileOverviewControllerPersonal());
  final nestedScrollController = Get.put(NestedScrollWidgetController());

  @override
  Widget build(BuildContext context) {
    return Drawer(
      width: 66.w,
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          buildHeader(context),
          SizedBox(
            height: 1.h,
          ),
          buildBody(context)
        ],
      ),
    );
  }

  Widget buildHeader(BuildContext context) {
    // UserModel user = HiveHelper.getUser();
    return Container(
      height: 31.h,
      decoration: BoxDecoration(
          color: AppColors.lightBlueColor,
          borderRadius: const BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30)),
          boxShadow: [
            BoxShadow(
                blurRadius: 4.0,
                spreadRadius: 2.0,
                color: Colors.grey.withOpacity(0.2))
          ]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 5.h),
          InkWell(
            onTap: () {
              customController.selectedAccount();
              Get.toNamed(AppPage.profileOverViewPersonal);
            },
            child: Container(
              height: 90,
              width: 90,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(
                      color: Colors.white, width: 1, style: BorderStyle.solid),
                  image: const DecorationImage(
                      image: AssetImage('assets/images/profile_pic2.png'),
                      fit: BoxFit.cover)),
            ),
          ),
          SizedBox(
            height: 2.h,
          ),
          Text(
            AppStrings.astria,
            style: montserratTextStyle,
          ),
          SizedBox(
            height: 1.h,
          ),
          Text(
            AppStrings.memberSince,
            style: montserrat400TextStyle,
          ),
        ],
      ),
    );
  }

  Widget buildBody(BuildContext context) {
    return GetBuilder<DrawerPersonalController>(
      builder: (_) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            buildRowWidget(
                index: 0,
                onTap: () {
                  controller.selectedItem(0);
                  Get.toNamed(AppPage.myTask);
                }),
            Divider(
              color: Colors.grey.withOpacity(0.4),
            ),
            buildRowWidget(
                index: 1,
                onTap: () {
                  controller.selectedItem(1);
                  Get.toNamed(AppPage.myAds);
                }),
            Divider(
              color: Colors.grey.withOpacity(0.4),
            ),
            buildRowWidget(
                index: 2,
                onTap: () {
                  controller.selectedItem(2);
                  Get.toNamed(AppPage.mySkills);
                }),
            Divider(
              color: Colors.grey.withOpacity(0.4),
            ),
            buildRowWidget(
                index: 3,
                onTap: () {
                  controller.selectedItem(3);
                  Get.toNamed(AppPage.myAnalytics);
                }),
            Divider(
              color: Colors.grey.withOpacity(0.4),
            ),
            buildRowWidget(
                index: 4,
                onTap: () {
                  controller.selectedItem(4);
                  Get.toNamed(AppPage.myMessage);
                }),
            Divider(
              color: Colors.grey.withOpacity(0.4),
            ),
            buildRowWidget(
                index: 5,
                onTap: () {
                  controller.selectedItem(5);
                  Get.toNamed(AppPage.myReviews);
                }),
            Divider(
              color: Colors.grey.withOpacity(0.4),
            ),
            buildRowWidget(
                index: 6,
                onTap: () {
                  controller.selectedItem(6);
                  Get.toNamed(AppPage.myFavourites);
                }),
            Divider(
              color: Colors.grey.withOpacity(0.4),
            ),
            Theme(
              data:
                  Theme.of(context).copyWith(dividerColor: Colors.transparent),
              child: ListTileTheme(
                contentPadding: EdgeInsets.all(0),
                dense: true,
                horizontalTitleGap: 0.0,
                minLeadingWidth: 0,
                child: ExpansionTile(
                  tilePadding: EdgeInsets.only(right: 15.w),
                  childrenPadding: EdgeInsets.zero,
                  iconColor: AppColors.indigoColor,
                  backgroundColor: controller.selectedItemIndex == 7
                      ? AppColors.lightBlueColor
                      : Colors.transparent,
                  title: GestureDetector(
                    onTap: () {
                      Get.toNamed(AppPage.mySettings);
                    },
                    child: Padding(
                      padding: EdgeInsets.only(left: 8.w),
                      child: Row(
                        children: [
                          SvgPicture.asset(
                            controller.selectedItemIndex == 7
                                ? controller.drawerItemSelectedImageList[7]
                                : controller.drawerItemImageList[7],
                          ),
                          SizedBox(
                            width: 8.w,
                          ),
                          Text(controller.item[7],
                              style: TextStyle(
                                fontFamily: AppFonts.montserrat,
                                fontSize: 12.sp,
                                fontWeight: FontWeight.w500,
                                color: controller.selectedItemIndex == 8
                                    ? Colors.white
                                    : Colors.black,
                              )),
                        ],
                      ),
                    ),
                  ),
                  children: [
                    Divider(
                      color: Colors.grey.withOpacity(0.4),
                    ),
                    buildExpansionSettings(
                        index: 8,
                        onTap: () {
                          controller.selectedItem(8);
                          Get.toNamed(AppPage.myProfileSetting);
                        }),
                    Divider(
                      color: Colors.grey.withOpacity(0.4),
                    ),
                    buildExpansionSettings(
                        index: 9,
                        onTap: () {
                          controller.selectedItem(9);
                          Get.toNamed(AppPage.alertNotificationSetting);
                        }),
                    Divider(
                      color: Colors.grey.withOpacity(0.4),
                    ),
                    buildExpansionSettings(
                        index: 10,
                        onTap: () {
                          controller.selectedItem(10);
                          Get.toNamed(AppPage.securityPasswordSetting);
                        }),
                    Divider(
                      color: Colors.grey.withOpacity(0.4),
                    ),
                    buildExpansionSettings(
                        index: 11,
                        onTap: () {
                          controller.selectedItem(11);
                          Get.toNamed(AppPage.socialMediaSetting);
                        }),
                    Divider(
                      color: Colors.grey.withOpacity(0.4),
                    ),
                    buildExpansionSettings(
                        index: 12,
                        onTap: () {
                          controller.selectedItem(12);
                          Get.toNamed(AppPage.freePlanSettings);
                        }),
                    Divider(
                      color: Colors.grey.withOpacity(0.4),
                    ),
                    buildExpansionSettings(
                        index: 13,
                        onTap: () {
                          controller.selectedItem(13);
                          Get.toNamed(AppPage.paymentMethodSettings);
                        }),
                  ],
                ),
              ),
            ),
            Divider(
              color: Colors.grey.withOpacity(0.4),
            ),
            GetBuilder(
              init: DrawerPersonalController(),
              builder: (_) {
                return buildRowWidget(
                    index: 14,
                    onTap: () {
                      controller.logoutUser();
                      controller.selectedItem(14);
                    });
              },
            ),
            SizedBox(
              height: 7.h,
            ),
          ],
        );
      },
    );
  }

  Widget buildRowWidget({required int index, required Function onTap}) {
    return Container(
        padding: EdgeInsets.only(
          top: 1.h,
        ),
        width: Get.width,
        height: 32,
        decoration: BoxDecoration(
          color: controller.selectedItemIndex == index
              ? AppColors.lightBlue4Color
              : Colors.transparent,
        ),
        child: Column(children: [
          InkWell(
            onTap: () {
              Get.back();
              onTap();
            },
            child: SizedBox(
              width: 50.w,
              child: Row(
                children: [
                  SvgPicture.asset(
                    controller.selectedItemIndex == index
                        ? controller.drawerItemSelectedImageList[index]
                        : controller.drawerItemImageList[index],
                  ),
                  SizedBox(
                    width: 8.w,
                  ),
                  Text(controller.item[index],
                      style: TextStyle(
                        fontFamily: AppFonts.montserrat,
                        fontSize: 12.sp,
                        fontWeight: FontWeight.w500,
                        color: controller.selectedItemIndex == index
                            ? Colors.white
                            : Colors.black,
                      )),
                ],
              ),
            ),
          ),
        ]));
  }

  Widget buildExpansionSettings({required int index, required Function onTap}) {
    return Container(
      padding: EdgeInsets.only(top: 1.h, bottom: 0.2.h),
      width: Get.width,
      height: 32,
      decoration: BoxDecoration(
          color: controller.selectedItemIndex == index
              ? AppColors.lightBlue4Color
              : Colors.transparent),
      child: Padding(
        padding: EdgeInsets.only(left: 7.w),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            GestureDetector(
              onTap: () {
                Get.back();
                onTap();
              },
              child: SizedBox(
                width: 50.w,
                child: Row(
                  children: [
                    Expanded(
                      child: SvgPicture.asset(
                        controller.selectedItemIndex == index
                            ? controller.drawerItemSelectedImageList[index]
                            : controller.drawerItemImageList[index],
                      ),
                    ),
                    SizedBox(
                      width: 4.w,
                    ),
                    Expanded(
                      flex: 10,
                      child: Text(controller.item[index],
                          style: TextStyle(
                            fontFamily: AppFonts.montserrat,
                            fontSize: 12.sp,
                            fontWeight: FontWeight.w500,
                            color: controller.selectedItemIndex == index
                                ? Colors.white
                                : Colors.black,
                          )),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
