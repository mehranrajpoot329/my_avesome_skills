import 'package:AwesomeSkills/constants/app_colors.dart';
import 'package:AwesomeSkills/local/db/local_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/routes/app_pages.dart';

import '../../../constants/app_strings.dart';

class DrawerPersonalController extends GetxController {
  var selectedItemIndex;
  bool isDrawerOpen = false;
  double xOffset = 0;
  double scaleFector = 1;
  double yOffset = 0;
  double drawerBorderRadius = 0;

  Color drawerItemColor = AppColors.black;
  Color drawerSelectedItemColor = Colors.blue;

  final item = [
    AppStrings.myTask,
    AppStrings.myAds,
    AppStrings.mySkills,
    AppStrings.analytics,
    AppStrings.message,
    AppStrings.reviews,
    AppStrings.favourite,
    AppStrings.settings,
    AppStrings.profileSettings,
    AppStrings.alertNotifications,
    AppStrings.securityPassword,
    AppStrings.socialMedia,
    AppStrings.myPlan,
    AppStrings.paymentMethod,
    AppStrings.logOut,
  ];

  final drawerItemImageList = [
    'assets/images/drawer_icon/my_task.svg',
    'assets/images/drawer_icon/my_ads.svg',
    'assets/images/drawer_icon/my_skill.svg',
    'assets/images/drawer_icon/analytics.svg',
    'assets/images/drawer_icon/message.svg',
    'assets/images/drawer_icon/review.svg',
    'assets/images/drawer_icon/favourite.svg',
    'assets/images/drawer_icon/settings.svg',
    'assets/images/drawer_icon/profile_icon.svg',
    'assets/images/drawer_icon/alert_notification.svg',
    'assets/images/drawer_icon/lock.svg',
    'assets/images/drawer_icon/social_media.svg',
    'assets/images/drawer_icon/my_plan.svg',
    'assets/images/drawer_icon/payment_method.svg',
    'assets/images/drawer_icon/logout.svg',
  ];
  final drawerItemSelectedImageList = [
    'assets/images/drawer_icon/my_task_white.svg',
    'assets/images/drawer_icon/myAds_white.svg',
    'assets/images/drawer_icon/my_skill_white.svg',
    'assets/images/drawer_icon/analytics_white.svg',
    'assets/images/drawer_icon/message_white.svg',
    'assets/images/drawer_icon/review_white.svg',
    'assets/images/drawer_icon/favourite_white.svg',
    'assets/images/drawer_icon/settings_white.svg',
    'assets/images/drawer_icon/profile_icon_white.svg',
    'assets/images/drawer_icon/alert_notification_white.svg',
    'assets/images/drawer_icon/lock_white.svg',
    'assets/images/drawer_icon/social_media_white.svg',
    'assets/images/drawer_icon/my_plan_white.svg',
    'assets/images/drawer_icon/payment_method_white.svg',
    'assets/images/drawer_icon/logout.svg',
  ];

  selectedItem(int index) {
    selectedItemIndex = index;
    update();
  }

  void logoutUser() {
    HiveHelper.saveUserLogout(isLoggedOut: true);
    HiveHelper.saveUserLogin(isLoggedIn: false);
    if (isDrawerOpen) {
      Get.offAllNamed(AppPage.loginScreen);
    }
    update();
  }
}
