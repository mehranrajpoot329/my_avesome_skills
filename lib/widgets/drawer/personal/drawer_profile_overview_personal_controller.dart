import 'package:get/get.dart';
import 'package:AwesomeSkills/widgets/drawer/personal/drawer_profile_overview_personal.dart';
import 'package:AwesomeSkills/widgets/drawer/personal/user_info_drawer_personal.dart';

class ProfileOverviewControllerPersonal extends GetxController {
  RxBool selectItem = false.obs;
  selectedAccount() {
    selectItem.value = true;
    update();
  }

  selectedAccountBack() {
    selectItem.value = false;
    update();
  }

  switchDrawer() {
    if (selectItem.isTrue) {
      return DrawerProfileOverViewPersonal();
    } else {
      return UserInfoDrawerPersonal();
    }
  }
}
