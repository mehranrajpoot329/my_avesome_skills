import 'package:flutter/material.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';

class TransportWidget extends StatelessWidget {
  const TransportWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: const [
        Text(
          AppStrings.walk,
          style: roboto16TextStyle,
        ),
        Text(
          AppStrings.bike,
          style: roboto16TextStyle,
        ),
        Text(
          AppStrings.motor,
          style: roboto16TextStyle,
        ),
        Text(
          AppStrings.bikeScooter,
          style: roboto16TextStyle,
        ),
        Text(
          AppStrings.car,
          style: roboto16TextStyle,
        ),
        Text(
          AppStrings.truck,
          style: roboto16TextStyle,
        ),
      ],
    );
  }
}
