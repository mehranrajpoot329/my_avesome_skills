import 'package:AwesomeSkills/constants/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sizer/sizer.dart';

import '../../Constants/app_strings.dart';
import '../../Constants/text_style.dart';

class BusinessSettingsCard extends StatelessWidget {
  String imagePath, textName;
  bool isComplete;
  Function onTap;
  BusinessSettingsCard(
      {required this.imagePath,
      required this.textName,
      required this.onTap,
      required this.isComplete});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onTap(),
      child: Container(
        padding: EdgeInsets.only(top: 3.h, left: 8.w, right: 2.w),
        height: 18.h,
        width: 18.h,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
            border: Border.all(
                color: Colors.grey, width: 0.5, style: BorderStyle.solid),
            boxShadow: [
              BoxShadow(
                  offset: const Offset(0, 5),
                  color: Colors.grey.withOpacity(0.2),
                  spreadRadius: 1.0,
                  blurRadius: 1.0),
            ]),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(right: 1.w),
              child: Align(
                alignment: Alignment.topRight,
                child: isComplete
                    ? SvgPicture.asset(
                        'assets/images/post_icon/check_icon.svg',
                        height: 13.sp,
                        width: 13.sp,
                      )
                    : const Text(
                        AppStrings.inComplete,
                        style: redColorTextStyle,
                      ),
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SvgPicture.asset(
                  imagePath,
                  height: 15.sp,
                  width: 15.sp,
                ),
                SizedBox(
                  width: 3.w,
                ),
                Expanded(
                  child: Text(
                    textName,
                    style: black14Color,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class BusinessSettingsCompanyDetailsCard extends StatelessWidget {
  String imagePath, textName;
  bool isComplete;
  Function onTap;
  BusinessSettingsCompanyDetailsCard(
      {required this.imagePath,
      required this.textName,
      required this.onTap,
      required this.isComplete});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onTap(),
      child: Container(
        padding: EdgeInsets.only(top: 3.h, left: 10.w, right: 2.w),
        height: 18.h,
        width: 18.h,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
            border: Border.all(
                color: Colors.grey, width: 0.5, style: BorderStyle.solid),
            boxShadow: [
              BoxShadow(
                  offset: const Offset(0, 5),
                  color: Colors.grey.withOpacity(0.2),
                  spreadRadius: 1.0,
                  blurRadius: 1.0),
            ]),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(right: 1.w),
              child: Align(
                alignment: Alignment.topRight,
                child: isComplete
                    ? SvgPicture.asset(
                        'assets/images/post_icon/check_icon.svg',
                        height: 13.sp,
                        width: 13.sp,
                      )
                    : const Text(
                        AppStrings.inComplete,
                        style: redColorTextStyle,
                      ),
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  imagePath,
                  height: 20.sp,
                  width: 20.sp,
                ),
                SizedBox(
                  width: 2.w,
                ),
                Expanded(
                  child: Text(
                    textName,
                    style: black14Color,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class FreeMyPlanBusiness extends StatelessWidget {
  Function onTap;

  FreeMyPlanBusiness({required this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onTap(),
      child: Container(
          height: 18.h,
          width: 18.h,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
              border: Border.all(
                  color: Colors.grey, width: 0.5, style: BorderStyle.solid),
              boxShadow: [
                BoxShadow(
                    offset: const Offset(0, 5),
                    color: Colors.grey.withOpacity(0.2),
                    spreadRadius: 1.0,
                    blurRadius: 1.0),
              ]),
          child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            const Text(
              AppStrings.free,
              style: greenColorArialTextStyle,
            ),
            const SizedBox(
              width: 4,
            ),
            Text(
              AppStrings.myPlan,
              style: blackBold16Color,
            )
          ])),
    );
  }
}

class PaymentMethodCardBusiness extends StatelessWidget {
  Function onTap;

  PaymentMethodCardBusiness({required this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onTap(),
      child: Container(
          padding: EdgeInsets.only(top: 3.h, left: 4.w, right: 2.w),
          height: 18.h,
          width: 18.h,
          decoration: BoxDecoration(
              color: AppColors.blue16Color,
              borderRadius: BorderRadius.circular(15),
              border: Border.all(
                  color: Colors.grey, width: 0.5, style: BorderStyle.solid),
              boxShadow: [
                BoxShadow(
                    offset: const Offset(0, 5),
                    color: Colors.grey.withOpacity(0.2),
                    spreadRadius: 1.0,
                    blurRadius: 1.0),
              ]),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  children: [
                    SvgPicture.asset(
                      'assets/images/post_icon/american_express_white_icon.svg',
                      height: 13.sp,
                      width: 13.sp,
                    ),
                    SizedBox(
                      width: 2.w,
                    ),
                    const FittedBox(
                      child: Text(
                        AppStrings.paymentMethod,
                        style: white11Color,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 2.h,
                ),
                const Text(
                  AppStrings.creditPlusDebitCard,
                  style: white600MulishTextStyle,
                ),
              ])),
    );
  }
}

class SocialMediaBusinessCard extends StatelessWidget {
  bool isComplete;
  Function onTap;

  SocialMediaBusinessCard({required this.onTap, required this.isComplete});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onTap(),
      child: Container(
        padding: EdgeInsets.only(
          top: 3.h,
          left: 4.w,
        ),
        height: 18.h,
        width: 18.h,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
            boxShadow: [
              BoxShadow(
                  offset: const Offset(0, 5),
                  color: Colors.grey.withOpacity(0.2),
                  spreadRadius: 1.0,
                  blurRadius: 1.0),
            ]),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 5.0),
              child: Align(
                alignment: Alignment.topRight,
                child: isComplete
                    ? SvgPicture.asset(
                        'assets/images/post_icon/check_icon.svg',
                        height: 13.sp,
                        width: 13.sp,
                      )
                    : const Text(
                        AppStrings.inComplete,
                        style: redColorTextStyle,
                      ),
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            FittedBox(
              child: Text(
                AppStrings.socialMedia,
                style: black14Color,
              ),
            ),
            SizedBox(
              height: 1.h,
            ),
            Row(
              children: [
                Expanded(
                  child: SvgPicture.asset(
                    'assets/images/post_icon/facebook_icon.svg',
                    height: 13.sp,
                    width: 13.sp,
                  ),
                ),
                SizedBox(
                  width: 2.w,
                ),
                Expanded(
                  child: SvgPicture.asset(
                    'assets/images/post_icon/twitter_icon.svg',
                    height: 13.sp,
                    width: 13.sp,
                  ),
                ),
                SizedBox(
                  width: 2.w,
                ),
                Expanded(
                  child: SvgPicture.asset(
                    'assets/images/post_icon/insta_black_icon.svg',
                    height: 13.sp,
                    width: 13.sp,
                  ),
                ),
                SizedBox(
                  width: 2.w,
                ),
                Expanded(
                  child: SvgPicture.asset(
                    'assets/images/post_icon/youtube_icon.svg',
                    height: 13.sp,
                    width: 13.sp,
                  ),
                ),
                SizedBox(
                  width: 2.w,
                ),
                Expanded(
                  child: SvgPicture.asset(
                    'assets/images/post_icon/google_green_icon.svg',
                    height: 13.sp,
                    width: 13.sp,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 2.h,
            ),
          ],
        ),
      ),
    );
    ;
  }
}
