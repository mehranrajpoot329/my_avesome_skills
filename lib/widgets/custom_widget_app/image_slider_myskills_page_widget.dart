import 'package:AwesomeSkills/Modules/Personal/drawers_screens/my_skills/my_skills_controller.dart';
import 'package:flutter/material.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';

class ImageSliderMySkillPageWidget extends StatefulWidget {
  @override
  State<ImageSliderMySkillPageWidget> createState() =>
      _ImageSliderMySkillPageWidgetState();
}

class _ImageSliderMySkillPageWidgetState
    extends State<ImageSliderMySkillPageWidget> {
  final skillController = Get.put(MySkillsController());

  int photoIndex = 0;
  void _previousImage() {
    setState(() {
      photoIndex = photoIndex > 0 ? photoIndex - 1 : 0;
    });
  }

  void _nextImage() {
    setState(() {
      photoIndex =
          photoIndex < skillController.getUserInfo.value.projects!.length - 1
              ? photoIndex + 1
              : photoIndex;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => skillController.getUserInfo.value.projects!.length == 0
          ? Center(child: Text("No Project"))
          : Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    photoIndex > 0
                        ? GestureDetector(
                            onTap: () {
                              _previousImage();
                            },
                            child: Container(
                              height: 7.h,
                              width: 7.w,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  shape: BoxShape.circle,
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.2),
                                      spreadRadius: 4.0,
                                      blurRadius: 2.0,
                                    )
                                  ]),
                              child: const Center(
                                child: Icon(
                                  Icons.arrow_back_ios_new,
                                  color: Colors.black,
                                  size: 17,
                                ),
                              ),
                            ),
                          )
                        : Container(),
                    SizedBox(
                      width: 7.w,
                    ),
                    Container(
                      height: 20.h,
                      width: 50.w,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          image: DecorationImage(
                              image: NetworkImage(skillController.getUserInfo
                                      .value.projects![photoIndex].imageUrl ??
                                  ""),
                              fit: BoxFit.cover)),
                    ),
                    SizedBox(
                      width: 7.w,
                    ),
                    photoIndex <
                            skillController.getUserInfo.value.projects!.length -
                                1
                        ? GestureDetector(
                            onTap: () {
                              _nextImage();
                            },
                            child: Container(
                              height: 7.h,
                              width: 7.w,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  shape: BoxShape.circle,
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.2),
                                      spreadRadius: 4.0,
                                      blurRadius: 2.0,
                                    )
                                  ]),
                              child: const Center(
                                child: Icon(
                                  Icons.arrow_forward_ios_outlined,
                                  color: Colors.black,
                                  size: 17,
                                ),
                              ),
                            ),
                          )
                        : Container(),
                  ],
                ),
                SizedBox(
                  height: 2.h,
                ),
                Align(
                    alignment: Alignment.center,
                    child: Text(
                      skillController.getUserInfo.value.projects![photoIndex]
                              .projectTitle ??
                          "",
                      style: roboto500TextStyle,
                    )),
              ],
            ),
    );
  }
}
