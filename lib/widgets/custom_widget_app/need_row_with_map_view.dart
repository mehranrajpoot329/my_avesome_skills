import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:sizer/sizer.dart';

class NeedHelpRowWithMap extends StatelessWidget {
  String textName, textName1;
  bool isText = true;

  NeedHelpRowWithMap(
      {required this.textName, this.textName1 = '', required this.isText});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              textName,
              style: blackBoldFourteenTextStyle,
            ),
            SizedBox(
              width: 33.w,
              child: Row(
                children: [
                  Image.asset('assets/images/post_icon/location.png'),
                  SizedBox(
                    width: 1.w,
                  ),
                  CustomButton(
                    buttonName: AppStrings.mapView,
                    width: 24.w,
                    height: 3.h,
                    buttonColor: AppColors.circleLightBlueColor,
                    textStyle: customButtonLightTextStyle,
                    borderRadius: BorderRadius.circular(20),
                    onTap: () {},
                  ),
                ],
              ),
            )
          ],
        ),
        isText
            ? SizedBox(
                height: 0.5.h,
              )
            : Container(),
        isText
            ? Row(
                children: [
                  Text(
                    AppStrings.helpJobUnder,
                    style: blackBold16Color,
                  ),
                  SizedBox(
                    width: 0.5.w,
                  ),
                  Text(
                    AppStrings.dollarTwoFifty,
                    style: blackBoldTextStyle,
                  ),
                  SizedBox(
                    width: 0.5.w,
                  ),
                  Text(
                    AppStrings.areFreeToPost,
                    style: blackBold16Color,
                  ),
                ],
              )
            : Text(''),
      ],
    );
  }
}
