import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import '../../Constants/app_colors.dart';
import '../../Constants/app_strings.dart';
import '../../Constants/text_style.dart';
import '../../routes/app_pages.dart';

class BottomSheetAdScreen extends StatelessWidget {
  const BottomSheetAdScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container();
  }

  static isLoginedInBottomSheet(
      {required BuildContext context,
      required Function needHelpTap,
      required Function canHelpTap}) {
    return showModalBottomSheet(
        backgroundColor: Colors.transparent,
        context: context,
        builder: (context) => Container(
              height: 200,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    AppStrings.whatAdWould,
                    style: blackBold16Color,
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 2.w, right: 2.w),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        InkWell(
                          onTap: () => needHelpTap(),
                          child: Container(
                            padding: EdgeInsets.only(
                                left: 2.w, right: 2.w, top: 1.h),
                            width: 45.w,
                            height: 15.h,
                            decoration: BoxDecoration(
                              color: AppColors.blue7Color,
                              borderRadius: BorderRadius.circular(5),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  AppStrings.needHelp,
                                  style: blackBold16Color,
                                ),
                                SizedBox(
                                  height: 1.h,
                                ),
                                Text(
                                  AppStrings.ifYouAreStuck,
                                  style: blue12ColorTextStyle,
                                )
                              ],
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () => canHelpTap(),
                          child: Container(
                            padding: EdgeInsets.only(
                                left: 2.w, right: 2.w, top: 1.h),
                            width: 45.w,
                            height: 15.h,
                            decoration: BoxDecoration(
                              color: AppColors.blue7Color,
                              borderRadius: BorderRadius.circular(5),
                            ),
                            child: Column(
                              children: [
                                Text(
                                  AppStrings.canHelpWith,
                                  style: blackBold16Color,
                                ),
                                SizedBox(
                                  height: 1.h,
                                ),
                                Text(
                                  AppStrings.hereYouCanPost,
                                  style: blue12ColorTextStyle,
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ));
  }

  static isLoginedOutBottomSheet(BuildContext context) {
    return showModalBottomSheet(
        backgroundColor: Colors.transparent,
        context: context,
        builder: (context) => Container(
              padding: EdgeInsets.only(top: 1.h),
              height: 150,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(10),
                    topLeft: Radius.circular(10)),
                color: Colors.white,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    AppStrings.toPosAndAd,
                    style: blackBold16Color,
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 2.w, right: 2.w),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        InkWell(
                          onTap: () => Get.toNamed(AppPage.signUpScreen),
                          child: Container(
                            padding: EdgeInsets.only(
                                left: 2.w, right: 2.w, top: 1.h),
                            width: 45.w,
                            height: 8.h,
                            decoration: BoxDecoration(
                              color: AppColors.blue7Color,
                              borderRadius: BorderRadius.circular(5),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  AppStrings.createAccount,
                                  style: blackBold16Color,
                                ),
                              ],
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () => Get.toNamed(AppPage.loginScreen),
                          child: Container(
                            padding: EdgeInsets.only(
                                left: 2.w, right: 2.w, top: 1.h),
                            width: 45.w,
                            height: 8.h,
                            decoration: BoxDecoration(
                              color: AppColors.lightBlueColor,
                              borderRadius: BorderRadius.circular(5),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  AppStrings.signIn,
                                  style: whiteBoldTwelveColor,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ));
  }
}
