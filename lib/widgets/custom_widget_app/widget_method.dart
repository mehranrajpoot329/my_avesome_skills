import 'package:AwesomeSkills/local/db/local_storage.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/custom_app_bar_controller.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/bottom_sheet_widget.dart';
import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';

import '../../Constants/app_colors.dart';
import '../../Constants/app_strings.dart';
import '../../Constants/text_style.dart';

class WidgetMethod {
  static Widget floatingActionButton(BuildContext context) {
    return Stack(clipBehavior: Clip.none, children: [
      Positioned(
          right: 0.w,
          bottom: -2.h,
          child: GestureDetector(
            onTap: () => bottomSheet(context),
            child: FittedBox(
              child: SvgPicture.asset(
                'assets/images/micMan.svg',
                height: 23.sp,
                width: 23.sp,
              ),
            ),
          )),
    ]);
  }

  static Widget parentWidgetFloating(BuildContext context) {
    final customAppBarController = Get.put(CustomAppBarController());
    return GestureDetector(
      onTap: () => HiveHelper.isLoggedIn()
          ? bottomSheet(context)
          : BottomSheetAdScreen.isLoginedOutBottomSheet(context),
      child: FittedBox(
        child: SvgPicture.asset(
          'assets/images/micMan.svg',
          height: 23.sp,
          width: 23.sp,
        ),
      ),
    );
  }

  static bottomSheet(BuildContext context) {
    return showModalBottomSheet(
        backgroundColor: Colors.transparent,
        context: context,
        builder: (context) => Container(
              height: 200,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  CustomButton(
                      buttonName: AppStrings.getHelpToUse,
                      width: MediaQuery.of(context).size.width * 0.8,
                      height: 50,
                      buttonColor: AppColors.blue11Color,
                      textStyle: interWhiteTextStyle,
                      borderRadius: BorderRadius.circular(10),
                      onTap: () {}),
                  const SizedBox(
                    height: 30,
                  ),
                  CustomButton(
                      buttonName: AppStrings.suggestIdeaToImprove,
                      width: MediaQuery.of(context).size.width * 0.8,
                      height: 50,
                      buttonColor: AppColors.blue11Color,
                      textStyle: interWhiteTextStyle,
                      borderRadius: BorderRadius.circular(10),
                      onTap: () {}),
                ],
              ),
            ));
  }
}
