import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/search_controller.dart';
import 'package:AwesomeSkills/widgets/check_box.dart';
import 'package:sizer/sizer.dart';

import '../../constants/app_colors.dart';
import '../../constants/app_strings.dart';
import '../../constants/text_style.dart';

// class CustomSearchDialogFilter extends StatelessWidget {
//   static final controller = Get.put(SearchController());
//
//   @override
//   Widget build(BuildContext context) {
//     return Dialog(
//       shape: RoundedRectangleBorder(
//         borderRadius: BorderRadius.circular(10),
//       ),
//       elevation: 0,
//       backgroundColor: Colors.transparent,
//       child: buildDialog(),
//     );
//   }
//
//   static buildDialog() {
//     return Get.dialog(
//       Column(
//         mainAxisSize: MainAxisSize.min,
//         mainAxisAlignment: MainAxisAlignment.center,
//         children: [
//           Stack(
//             alignment: Alignment.topCenter,
//             children: [
//               Padding(
//                 padding: EdgeInsets.only(top: 20.h),
//                 child: Dialog(
//                   shape: RoundedRectangleBorder(
//                       borderRadius: BorderRadius.circular(16)),
//                   child: Padding(
//                     padding: EdgeInsets.only(left: 5.w, right: 5.w),
//                     child: Column(
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: [
//                         SizedBox(
//                           height: 3.h,
//                         ),
//                         Row(children: [
//                           Expanded(
//                             child: Text(
//                               AppStrings.iNeedHelp,
//                               style: grey14Color,
//                             ),
//                           ),
//                           Obx(
//                             () => Checkbox(
//                               value: controller.needHelp.value,
//                               shape: CircleBorder(),
//                               side: BorderSide(
//                                   color: AppColors.grey4Color, width: 1.5),
//                               checkColor: Colors.white,
//                               activeColor: AppColors.green3Color,
//                               onChanged: (value) => controller.needHelp.value =
//                                   value ?? !controller.needHelp.value,
//                             ),
//                           ),
//                         ]),
//                         Row(children: [
//                           Expanded(
//                             child: Text(
//                               AppStrings.canHelpWith,
//                               style: grey14Color,
//                             ),
//                           ),
//                           Obx(
//                             () => Checkbox(
//                               value: controller.canNeedHelp.value,
//                               shape: CircleBorder(),
//                               side: BorderSide(
//                                   color: AppColors.grey4Color, width: 1.5),
//                               checkColor: Colors.white,
//                               activeColor: AppColors.green3Color,
//                               onChanged: (value) =>
//                                   controller.canNeedHelp.value =
//                                       value ?? !controller.canNeedHelp.value,
//                             ),
//                           ),
//                         ]),
//                         Row(children: [
//                           Expanded(
//                             child: Text(
//                               AppStrings.useCurrentLocation,
//                               style: grey14Color,
//                             ),
//                           ),
//                           Obx(
//                             () => Checkbox(
//                               value: controller.useCurrentLocation.value,
//                               shape: CircleBorder(),
//                               side: BorderSide(
//                                   color: AppColors.grey4Color, width: 1.5),
//                               checkColor: Colors.white,
//                               activeColor: AppColors.green3Color,
//                               onChanged: (value) => controller
//                                       .useCurrentLocation.value =
//                                   value ?? !controller.useCurrentLocation.value,
//                             ),
//                           ),
//                         ]),
//                         Row(
//                           children: [
//                             Expanded(
//                               child: Text(
//                                 AppStrings.distance,
//                                 style: grey14Color,
//                               ),
//                             ),
//                             GetBuilder<SearchController>(builder: (_) {
//                               return Slider(
//                                   thumbColor: AppColors.lightBlueColor,
//                                   activeColor: AppColors.lightBlueColor,
//                                   inactiveColor:
//                                       AppColors.lightBlueColor.withOpacity(0.2),
//                                   max: 10,
//                                   min: 3,
//                                   value: controller.distance,
//                                   onChanged: (newValue) =>
//                                       controller.changeDistance(newValue));
//                             }),
//                           ],
//                         ),
//                         Row(
//                           children: [
//                             Expanded(
//                               child: Text(
//                                 AppStrings.priceRange,
//                                 style: grey14Color,
//                               ),
//                             ),
//                             GetBuilder<SearchController>(builder: (_) {
//                               return Slider(
//                                   thumbColor: AppColors.lightBlueColor,
//                                   activeColor: AppColors.lightBlueColor,
//                                   inactiveColor:
//                                       AppColors.lightBlueColor.withOpacity(0.2),
//                                   max: 10,
//                                   min: 3,
//                                   values: controller.priceRange,
//                                   onChanged: (newValue) =>
//                                       controller.changePriceRange(newValue));
//                             }),
//                           ],
//                         ),
//                         SizedBox(
//                           height: 2.h,
//                         ),
//                         Row(
//                           mainAxisAlignment: MainAxisAlignment.center,
//                           children: [
//                             TextButton(
//                                 onPressed: () => Get.back(),
//                                 child: const Text(
//                                   AppStrings.apply,
//                                   style: lightBlue16ColorTextStyle,
//                                 )),
//                             SizedBox(
//                               width: 10.w,
//                             ),
//                             TextButton(
//                                 onPressed: () => Get.back(),
//                                 child: const Text(
//                                   AppStrings.cancel,
//                                   style: grey16Color,
//                                 ))
//                           ],
//                         ),
//                         SizedBox(
//                           height: 3.h,
//                         ),
//                       ],
//                     ),
//                   ),
//                 ),
//               ),
//             ],
//           ),
//         ],
//       ),
//     );
//   }
//
//   static Widget buildRowWidget() {
//     return Row(
//       children: [
//         Text(
//           AppStrings.iNeedHelp,
//           style: grey9Color,
//         ),
//       ],
//     );
//   }
// }
