import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sizer/sizer.dart';

import '../../Constants/text_style.dart';

class RatingStar extends StatelessWidget {
  bool isNumberRating;
  String ratingNumber;
  TextStyle textStyle;

  RatingStar(
      {required this.ratingNumber,
      required this.textStyle,
      required this.isNumberRating});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        SvgPicture.asset(
          'assets/images/post_icon/star.svg',
          height: 12.sp,
          width: 15.sp,
        ),
        SvgPicture.asset(
          'assets/images/post_icon/star.svg',
          height: 12.sp,
          width: 15.sp,
        ),
        SvgPicture.asset(
          'assets/images/post_icon/star.svg',
          height: 12.sp,
          width: 15.sp,
        ),
        SvgPicture.asset(
          'assets/images/post_icon/star.svg',
          height: 12.sp,
          width: 15.sp,
        ),
        SvgPicture.asset(
          'assets/images/post_icon/star.svg',
          height: 12.sp,
          width: 15.sp,
        ),
        SizedBox(
          width: 1.w,
        ),
        isNumberRating
            ? Text(
                '($ratingNumber)',
                style: textStyle,
              )
            : Container()
      ],
    );
  }
}
