import 'dart:developer';

import 'package:AwesomeSkills/constants/app_strings.dart';
import 'package:AwesomeSkills/models/user_model.dart';
import 'package:hive_flutter/adapters.dart';

class HiveHelper {
  Box get appBox => _appBox;

  static const myAwesomeSkills = 'MyAwesomeSkills';

  static final HiveHelper _singleton = HiveHelper._internal();

  factory HiveHelper() {
    return _singleton;
  }

  HiveHelper._internal();

  static late Box _appBox;

  static initHiveHelper() async {
    await Hive.initFlutter();

    ///to register Hive adapter
    Hive.registerAdapter(
      UserAdapter(),
    );
    // Open the peopleBox
    _appBox = await Hive.openBox(myAwesomeSkills);
  }

  static saveFcmToken({String? fcmToken}) {
    if (fcmToken?.isNotEmpty ?? false) {
      _appBox.put(AppStrings.fcmToken, fcmToken ?? '');
    }
  }

  static String getFcmToken() {
    return _appBox.get(AppStrings.fcmToken) ?? '';
  }

  static setAuthToken(String authToken) {
    log('Login Token : $authToken');
    _appBox.put(AppStrings.authToken,
        authToken.isNotEmpty ? 'MyAwesomeSkillToken $authToken' : '');
  }

  static String getAuthToken() {
    return (_appBox.get(AppStrings.authToken) != null)
        ? _appBox.get(AppStrings.authToken)
        : 'MyAwesomeSkillToken';
  }

  static isLoggedIn() {
    return _appBox.get(AppStrings.isLoggedIn) ?? false;
  }

  static isLoggedOut() {
    return _appBox.get(AppStrings.isLoggedOut) ?? false;
  }

  static saveUserLogout({required bool isLoggedOut}) async {
    await _appBox.put(AppStrings.isLoggedOut, isLoggedOut);
  }

  static isLoggedOutIsUserSignedUp() {
    return _appBox.get(AppStrings.isUserSignedUp) ?? false;
  }

  static saveUserLogin({required bool isLoggedIn}) async {
    await _appBox.put(AppStrings.isLoggedIn, isLoggedIn);
  }

  static saveUserSignedUp({required bool isUserSignedUp}) async {
    await _appBox.put(AppStrings.isUserSignedUp, isUserSignedUp);
  }

  static isNew() {
    return _appBox.get(AppStrings.isNew) ?? true;
  }

  static saveIsNew({required bool isNew}) async {
    await _appBox.put(AppStrings.isNew, isNew);
  }

  static saveLoggIn({bool isLoggedIn = false}) async {
    await _appBox.put(AppStrings.isLoggedIn, isLoggedIn);
  }

  static saveUser(User user) async {
    await _appBox.put(AppStrings.user, user);
  }

  static User getUser() {
    return _appBox.get(AppStrings.user, defaultValue: null);
  }

  static clearBox() {
    _appBox.clear();
  }

  static closeAllBoxes() {
    Hive.close();
  }
}
