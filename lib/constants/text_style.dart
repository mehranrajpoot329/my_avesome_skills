import 'package:flutter/material.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/constants/app_fonts.dart';
import 'package:sizer/sizer.dart';

const projectNameTextStyle = TextStyle(
    color: AppColors.blueColor,
    fontSize: 32,
    fontFamily: AppFonts.poppins,
    fontWeight: FontWeight.w600);

const createAccountTextStyle = TextStyle(
    fontFamily: AppFonts.poppins,
    color: AppColors.blueColor,
    fontSize: 16,
    fontWeight: FontWeight.w600);

final lightBlue500TextStyle = TextStyle(
    color: AppColors.lightBlueColor,
    fontSize: 14.sp,
    fontWeight: FontWeight.w400,
    fontFamily: AppFonts.poppins);

const indigoColorTextStyle = TextStyle(
    fontFamily: AppFonts.poppins,
    color: AppColors.indigoColor,
    fontSize: 10,
    fontWeight: FontWeight.w500);

const indigo14ColorTextStyle = TextStyle(
    fontFamily: AppFonts.poppins,
    color: AppColors.indigoColor,
    fontSize: 14,
    fontWeight: FontWeight.w600);

final indigoSixteenColorTextStyle = TextStyle(
    fontFamily: AppFonts.poppins,
    color: AppColors.indigoColor,
    fontSize: 13.5.sp,
    fontWeight: FontWeight.w600);

final indigoColor20TextStyle = TextStyle(
    fontFamily: AppFonts.poppins,
    color: AppColors.indigoColor,
    fontSize: 12.sp,
    fontWeight: FontWeight.w400);

const indigoColor12TextStyle = TextStyle(
    fontFamily: AppFonts.poppins,
    color: AppColors.indigoColor,
    fontSize: 12,
    fontWeight: FontWeight.w500);

const orangeColorTextStyle = TextStyle(
    fontFamily: AppFonts.poppins,
    color: AppColors.orangeColor,
    fontSize: 24,
    fontWeight: FontWeight.w500);

const orangeColor600TextStyle = TextStyle(
    fontFamily: AppFonts.poppins,
    color: AppColors.orangeColor,
    fontSize: 24,
    fontWeight: FontWeight.w600);

final orange600ColorTextStyle = TextStyle(
    fontFamily: AppFonts.poppins,
    color: AppColors.orangeColor,
    fontSize: 15.sp,
    fontWeight: FontWeight.w600);

const orange500ColorTextStyle = TextStyle(
    fontFamily: AppFonts.poppins,
    color: AppColors.orange2Color,
    fontSize: 14,
    fontWeight: FontWeight.w500);

const orange16ColorTextStyle = TextStyle(
    fontFamily: AppFonts.poppins,
    color: AppColors.orangeColor,
    fontSize: 16,
    fontWeight: FontWeight.w400);

const orangeColor18TextStyle = TextStyle(
    fontFamily: AppFonts.poppins,
    color: AppColors.orangeColor,
    fontSize: 18,
    fontWeight: FontWeight.w600);

const greenColor18TextStyle = TextStyle(
    fontFamily: AppFonts.poppins,
    color: AppColors.green7Color,
    fontSize: 18,
    fontWeight: FontWeight.w600);

const greenColor16TextStyle = TextStyle(
    fontFamily: AppFonts.poppins,
    color: AppColors.green10Color,
    fontSize: 16,
    fontWeight: FontWeight.w600);

const blue600ColorTextStyle = TextStyle(
    fontFamily: AppFonts.poppins,
    color: AppColors.lightBlueColor,
    fontSize: 24,
    fontWeight: FontWeight.w400);

const lightBlue32ColorTextStyle = TextStyle(
    fontFamily: AppFonts.poppins,
    color: AppColors.lightBlueColor,
    fontSize: 32,
    fontWeight: FontWeight.w400);

const lightBlue16ColorTextStyle = TextStyle(
    fontFamily: AppFonts.poppins,
    color: AppColors.lightBlueColor,
    fontSize: 16,
    fontWeight: FontWeight.w500);

const blue22ColorTextStyle = TextStyle(
    fontFamily: AppFonts.poppins,
    color: AppColors.lightBlueColor,
    fontSize: 22,
    fontWeight: FontWeight.w400);

const blue500ColorTextStyle = TextStyle(
    fontFamily: AppFonts.poppins,
    color: AppColors.lightBlueColor,
    fontSize: 24,
    fontWeight: FontWeight.w500);

const blue20ColorTextStyle = TextStyle(
    fontFamily: AppFonts.poppins,
    color: AppColors.blue20Color,
    fontSize: 16,
    fontWeight: FontWeight.w400);

const poppins36ColorTextStyle = TextStyle(
    fontFamily: AppFonts.poppins,
    color: AppColors.lightBlueColor,
    fontSize: 36,
    fontWeight: FontWeight.w500);

const blue16ColorTextStyle = TextStyle(
    fontFamily: AppFonts.poppins,
    color: AppColors.blue12Color,
    fontSize: 16,
    fontWeight: FontWeight.w600);

final blue12ColorTextStyle = TextStyle(
    fontFamily: AppFonts.poppins,
    color: AppColors.lightBlueColor,
    fontSize: 10.sp,
    fontWeight: FontWeight.w500);

const blue400ColorTextStyle = TextStyle(
    fontFamily: AppFonts.poppins,
    color: AppColors.blue14Color,
    fontSize: 16,
    fontWeight: FontWeight.w400);

const blue18ColorTextStyle = TextStyle(
    fontFamily: AppFonts.poppins,
    color: AppColors.blue18Color,
    fontSize: 16,
    fontWeight: FontWeight.w400);

const blue1ColorTextStyle = TextStyle(
    fontFamily: AppFonts.poppins,
    color: AppColors.blue1Color,
    fontSize: 16,
    fontWeight: FontWeight.w400);

const white28ColorTextStyle = TextStyle(
    fontFamily: AppFonts.poppins,
    color: Colors.white,
    fontSize: 28,
    fontWeight: FontWeight.w400);

const white16ColorTextStyle = TextStyle(
    fontFamily: AppFonts.poppins,
    color: Colors.white,
    fontSize: 16,
    fontWeight: FontWeight.w600);

const whiteFourteenColorTextStyle = TextStyle(
    fontFamily: AppFonts.poppins,
    color: Colors.white,
    fontSize: 14,
    fontWeight: FontWeight.bold);

final customButtonTextStyle = TextStyle(
    fontFamily: AppFonts.poppins,
    fontSize: 10.sp,
    color: Colors.white,
    fontWeight: FontWeight.w500);

final customButtonSixTextStyle = TextStyle(
  fontFamily: AppFonts.poppins,
  fontSize: 6.sp,
  color: Colors.white,
);

const white10TextStyle = TextStyle(
  fontFamily: AppFonts.poppins,
  fontSize: 10,
  fontWeight: FontWeight.w600,
  color: Colors.white,
);

final customButtonWhiteTextStyle = TextStyle(
  fontFamily: AppFonts.poppins,
  fontSize: 12.sp,
  fontWeight: FontWeight.bold,
  color: Colors.white,
);
final whiteBoldTwelveColor = TextStyle(
  color: Colors.white,
  fontSize: 14.sp,
  fontWeight: FontWeight.w600,
  fontFamily: AppFonts.poppins,
);

const white22TextStyle = TextStyle(
  fontFamily: AppFonts.poppins,
  fontSize: 22,
  fontWeight: FontWeight.w400,
  color: Colors.white,
);
const buttonBlueTextStyle = TextStyle(
  color: Colors.white,
  fontSize: 16,
  fontWeight: FontWeight.w400,
  fontFamily: AppFonts.poppins,
);

final white14Color = TextStyle(
  color: Colors.white,
  fontSize: 8.sp,
  fontWeight: FontWeight.w400,
  fontFamily: AppFonts.poppins,
);

const white11Color = TextStyle(
  color: Colors.white,
  fontSize: 10,
  fontWeight: FontWeight.w800,
  fontFamily: AppFonts.poppins,
);

const customButtonBlueTextStyle = TextStyle(
  fontFamily: AppFonts.poppins,
  fontSize: 12,
  color: AppColors.blueColor,
);

const blueColorBoldTextStyle = TextStyle(
  fontFamily: AppFonts.poppins,
  fontWeight: FontWeight.bold,
  fontSize: 14,
  color: AppColors.blueColor,
);

final customButtonLightTextStyle = TextStyle(
  fontFamily: AppFonts.poppins,
  fontSize: 10.sp,
  color: AppColors.lightBlueColor,
);

const customLightBlue3TextStyle = TextStyle(
  fontFamily: AppFonts.poppins,
  fontWeight: FontWeight.w600,
  fontSize: 16,
  color: AppColors.blueColor,
);

final labelTextStyle = TextStyle(
  color: Color.fromRGBO(77, 89, 89, 1),
  fontSize: 10.sp,
  fontWeight: FontWeight.w500,
  fontFamily: AppFonts.poppins,
);

const hintTextStyle = TextStyle(
  color: Color.fromRGBO(0, 69, 138, 0.4),
  fontSize: 14,
  fontWeight: FontWeight.w400,
  fontFamily: AppFonts.poppins,
);

const blueTextStyle = TextStyle(
  color: AppColors.blueColor,
  fontSize: 14,
  fontWeight: FontWeight.w400,
  fontFamily: AppFonts.poppins,
);

const grey18Color = TextStyle(
  color: Colors.grey,
  fontSize: 18,
  fontWeight: FontWeight.w500,
  fontFamily: AppFonts.poppins,
);

final greyTextColor = TextStyle(
  color: Colors.grey,
  fontSize: 10.sp,
  fontWeight: FontWeight.w500,
  fontFamily: AppFonts.poppins,
);

const grey16Color = TextStyle(
  color: AppColors.grey6Color,
  fontSize: 16,
  fontWeight: FontWeight.w400,
  fontFamily: AppFonts.poppins,
);

final grey9Color = TextStyle(
  color: AppColors.grey6Color,
  fontSize: 8.sp,
  fontWeight: FontWeight.w400,
  fontFamily: AppFonts.poppins,
);

const grey24Color = TextStyle(
  color: Colors.grey,
  fontSize: 24,
  fontWeight: FontWeight.w400,
  fontFamily: AppFonts.poppins,
);

const grey14Color = TextStyle(
  color: AppColors.grey5Color,
  fontSize: 14,
  fontWeight: FontWeight.w400,
  fontFamily: AppFonts.poppins,
);

const blue16TextStyle = TextStyle(
  color: AppColors.blueColor,
  fontSize: 16,
  fontWeight: FontWeight.w400,
  fontFamily: AppFonts.poppins,
);

const logoTextStyle = TextStyle(
    color: AppColors.lightBlueColor,
    fontSize: 18,
    fontFamily: AppFonts.poppins,
    fontWeight: FontWeight.bold);

final blueBoldTextStyle = TextStyle(
    color: AppColors.lightBlueColor,
    fontSize: 12.sp,
    fontFamily: AppFonts.poppins,
    fontWeight: FontWeight.bold);

const lightBlueTextStyle = TextStyle(
    color: AppColors.lightBlueColor,
    fontSize: 15,
    fontFamily: AppFonts.poppins,
    fontWeight: FontWeight.bold);

const lightBlue20TextStyle = TextStyle(
  fontSize: 20,
  color: AppColors.lightBlueColor,
  fontWeight: FontWeight.w500,
  fontFamily: AppFonts.poppins,
);

const lightBlueTwentyEightTextStyle = TextStyle(
  fontSize: 28,
  color: AppColors.lightBlueColor,
  fontWeight: FontWeight.w500,
  fontFamily: AppFonts.poppins,
);

const lightBlueColorTextStyle = TextStyle(
  fontSize: 24,
  color: AppColors.lightBlueColor,
  fontWeight: FontWeight.w500,
  fontFamily: AppFonts.poppins,
);

const greenColorTextStyle = TextStyle(
  color: AppColors.green12Color,
  fontSize: 16,
  fontWeight: FontWeight.w400,
  fontFamily: AppFonts.poppins,
);

final greenTextStyle = TextStyle(
  color: AppColors.green15Color,
  fontSize: 12.sp,
  fontWeight: FontWeight.bold,
  fontFamily: AppFonts.poppins,
);

final greenColor15TextStyle = TextStyle(
  color: AppColors.green14Color,
  fontSize: 14.sp,
  fontWeight: FontWeight.w500,
  fontFamily: AppFonts.poppins,
);
final greenColor800TextStyle = TextStyle(
  color: AppColors.green14Color,
  fontSize: 14.sp,
  fontWeight: FontWeight.w800,
  fontFamily: AppFonts.poppins,
);

const greenColor24TextStyle = TextStyle(
  color: AppColors.greenColor,
  fontSize: 24,
  fontWeight: FontWeight.w700,
  fontFamily: AppFonts.poppins,
);

const green40ColorTextStyle = TextStyle(
  color: AppColors.green14Color,
  fontSize: 40,
  fontWeight: FontWeight.w700,
  fontFamily: AppFonts.poppins,
);

const greenBold16Color = TextStyle(
  color: AppColors.green6Color,
  fontSize: 16,
  fontWeight: FontWeight.w400,
  fontFamily: AppFonts.poppins,
);

const redColorTextStyle = TextStyle(
  color: AppColors.red3Color,
  fontSize: 10,
  fontWeight: FontWeight.w300,
  fontFamily: AppFonts.poppins,
);

final redColorTenTextStyle = TextStyle(
  color: AppColors.red3Color,
  fontSize: 10.sp,
  fontWeight: FontWeight.w300,
  fontFamily: AppFonts.poppins,
);

final redColor32TextStyle = TextStyle(
  color: AppColors.redAccent2Color,
  fontSize: 18.sp,
  fontWeight: FontWeight.w500,
  fontFamily: AppFonts.poppins,
);

final red4ColorTextStyle = TextStyle(
  color: AppColors.red2Color,
  fontSize: 14.sp,
  fontWeight: FontWeight.w400,
  fontFamily: AppFonts.poppins,
);

final blackSixHundredWeightColor = TextStyle(
  color: Colors.black,
  fontSize: 10.sp,
  fontWeight: FontWeight.w500,
  fontFamily: AppFonts.poppins,
);

const black400Color = TextStyle(
  color: Colors.black,
  fontSize: 8,
  fontWeight: FontWeight.w400,
  fontFamily: AppFonts.poppins,
);

final blackEightColor = TextStyle(
  color: Colors.black,
  fontSize: 8.sp,
  fontFamily: AppFonts.poppins,
);

const blackFiveColor = TextStyle(
  color: Colors.black,
  fontSize: 5,
  fontWeight: FontWeight.w400,
  fontFamily: AppFonts.poppins,
);

const whiteFiveColor = TextStyle(
  color: Colors.white,
  fontSize: 6,
  fontWeight: FontWeight.w400,
  fontFamily: AppFonts.poppins,
);

final blackTwelveColor = TextStyle(
  color: Colors.black,
  fontSize: 10.sp,
  fontWeight: FontWeight.bold,
  fontFamily: AppFonts.poppins,
);

final blackTenColor = TextStyle(
  color: Colors.black,
  fontSize: 6.sp,
  fontWeight: FontWeight.bold,
  fontFamily: AppFonts.poppins,
);

final black14Color = TextStyle(
  color: Colors.black,
  fontSize: 10.sp,
  fontWeight: FontWeight.w500,
  fontFamily: AppFonts.poppins,
);

const black500Color = TextStyle(
  color: Colors.black,
  fontSize: 14,
  fontWeight: FontWeight.w500,
  fontFamily: AppFonts.poppins,
);

final black12Color = TextStyle(
  color: Colors.black.withOpacity(0.4),
  fontSize: 12,
  fontWeight: FontWeight.w300,
  fontFamily: AppFonts.poppins,
);

final black8Color = TextStyle(
  color: Colors.black.withOpacity(0.4),
  fontSize: 8.sp,
  fontWeight: FontWeight.w300,
  fontFamily: AppFonts.poppins,
);

const black18Color = TextStyle(
  color: Colors.black,
  fontSize: 18,
  fontWeight: FontWeight.w500,
  fontFamily: AppFonts.poppins,
);

final blackBold2Color = TextStyle(
  color: Colors.black,
  fontSize: 12.sp,
  fontWeight: FontWeight.w400,
  fontFamily: AppFonts.poppins,
);

const blackBold600Color = TextStyle(
  color: Colors.black,
  fontSize: 16,
  fontWeight: FontWeight.w600,
  fontFamily: AppFonts.poppins,
);

const black5002Color = TextStyle(
  color: Colors.black,
  fontSize: 12,
  fontWeight: FontWeight.w500,
  fontFamily: AppFonts.poppins,
);

final blackBold16Color = TextStyle(
  color: Colors.black,
  fontSize: 12.sp,
  fontWeight: FontWeight.w400,
  fontFamily: AppFonts.poppins,
);

final blackBoldPoppinsColor = TextStyle(
  color: Colors.black,
  fontSize: 12.sp,
  fontWeight: FontWeight.w500,
  fontFamily: AppFonts.poppins,
);

const black9PoppinsColor = TextStyle(
  color: Colors.black,
  fontSize: 10,
  fontWeight: FontWeight.w400,
  fontFamily: AppFonts.poppins,
);

const black12PoppinsColor = TextStyle(
  color: Colors.black,
  fontSize: 12,
  fontWeight: FontWeight.w400,
  fontFamily: AppFonts.poppins,
);

const greenBoldPoppinsColor = TextStyle(
  color: AppColors.green3Color,
  fontSize: 14,
  fontWeight: FontWeight.w700,
  fontFamily: AppFonts.poppins,
);

final blackBoldTextStyle = TextStyle(
  color: Colors.black,
  fontSize: 12.sp,
  fontWeight: FontWeight.bold,
  fontFamily: AppFonts.poppins,
);

final blackBoldFourteenTextStyle = TextStyle(
  color: Colors.black,
  fontSize: 14.5.sp,
  fontWeight: FontWeight.bold,
  fontFamily: AppFonts.poppins,
);

const black600TextStyle = TextStyle(
  color: Colors.black,
  fontSize: 20,
  fontWeight: FontWeight.w600,
  fontFamily: AppFonts.poppins,
);

final yellow2ColorTextStyle = TextStyle(
  color: AppColors.yellow2Color,
  fontSize: 12.sp,
  fontWeight: FontWeight.w500,
  fontFamily: AppFonts.poppins,
);

final black500TextStyle = TextStyle(
    color: Colors.black,
    fontSize: 12.sp,
    fontWeight: FontWeight.w600,
    fontFamily: AppFonts.poppins);

final blackFourteenSpTextStyle = TextStyle(
    color: Colors.black,
    fontSize: 14.sp,
    fontWeight: FontWeight.w500,
    fontFamily: AppFonts.poppins);

const black24TextStyle = TextStyle(
  color: Colors.black,
  fontSize: 24,
  fontWeight: FontWeight.w600,
  fontFamily: AppFonts.poppins,
);

const blackPoppins300TextStyle = TextStyle(
  color: Colors.black,
  fontSize: 14,
  fontWeight: FontWeight.w300,
  fontFamily: AppFonts.poppins,
);

const yellowPoppins300TextStyle = TextStyle(
  color: AppColors.yellow3Color,
  fontSize: 12,
  fontWeight: FontWeight.w400,
  fontFamily: AppFonts.poppins,
);

final gFATextStyle = TextStyle(
    color: Colors.grey[700],
    fontSize: 14,
    fontFamily: AppFonts.lato,
    fontWeight: FontWeight.w700);

final greyTextStyle = TextStyle(
    color: Colors.grey[700],
    fontSize: 9.sp,
    fontFamily: AppFonts.lato,
    fontWeight: FontWeight.w700);

final interBlackTextStyle = TextStyle(
    color: AppColors.grey2Color,
    fontSize: 11.sp,
    fontWeight: FontWeight.w600,
    fontFamily: AppFonts.inter);

final interBlackFiveHundredTextStyle = TextStyle(
    color: AppColors.grey2Color,
    fontSize: 10.sp,
    fontWeight: FontWeight.w500,
    fontFamily: AppFonts.inter);

final interBlack16TextStyle = TextStyle(
    color: AppColors.grey2Color,
    fontSize: 12.sp,
    fontWeight: FontWeight.w600,
    fontFamily: AppFonts.inter);

const inter16TextStyle = TextStyle(
    color: AppColors.indigoColor,
    fontSize: 16,
    fontWeight: FontWeight.w400,
    fontFamily: AppFonts.inter);

const inter14TextStyle = TextStyle(
    color: AppColors.indigoColor,
    fontSize: 14,
    fontWeight: FontWeight.w400,
    fontFamily: AppFonts.inter);

final interBlueTextStyle = TextStyle(
    fontSize: 11.sp,
    color: AppColors.indigoColor,
    fontWeight: FontWeight.w400,
    fontFamily: AppFonts.inter);

final interBlueThirteenTextStyle = TextStyle(
    fontSize: 12.sp,
    color: AppColors.indigoColor,
    fontWeight: FontWeight.w600,
    fontFamily: AppFonts.inter);

final inter11TextStyle = TextStyle(
    fontSize: 8.sp,
    color: Colors.white,
    fontWeight: FontWeight.w500,
    fontFamily: AppFonts.inter);

const interBlack2TextStyle = TextStyle(
    fontSize: 14, fontWeight: FontWeight.w600, fontFamily: AppFonts.inter);

const interWhite14TextStyle = TextStyle(
    fontSize: 14,
    color: Colors.white,
    fontWeight: FontWeight.w500,
    fontFamily: AppFonts.inter);

final interGreenTextStyle = TextStyle(
    fontSize: 20.sp,
    color: AppColors.greenColor,
    fontWeight: FontWeight.w700,
    fontFamily: AppFonts.inter);

const interWhiteTextStyle = TextStyle(
    fontSize: 12,
    color: Colors.white,
    fontWeight: FontWeight.w400,
    fontFamily: AppFonts.inter);

final interLightBlueTextStyle = TextStyle(
    fontSize: 9.sp,
    color: AppColors.lightBlueColor,
    fontWeight: FontWeight.w400,
    fontFamily: AppFonts.inter);

final interGreen15TextStyle = TextStyle(
    fontSize: 12.sp,
    color: AppColors.green4Color,
    fontWeight: FontWeight.w700,
    fontFamily: AppFonts.inter);

final blackMulishTextStyle = TextStyle(
    color: Colors.black,
    fontSize: 10.sp,
    fontWeight: FontWeight.w600,
    fontFamily: AppFonts.mulish);

const blue24MulishTextStyle = TextStyle(
    color: AppColors.indigoColor,
    fontSize: 24,
    fontWeight: FontWeight.bold,
    fontFamily: AppFonts.mulish);

final blackMulish300TextStyle = TextStyle(
    color: Colors.black,
    fontSize: 10.sp,
    fontWeight: FontWeight.w300,
    fontFamily: AppFonts.mulish);

const blackMulish10TextStyle = TextStyle(
    color: Colors.black,
    fontSize: 10,
    fontWeight: FontWeight.w600,
    fontFamily: AppFonts.mulish);

TextStyle blackMulish14TextStyle = TextStyle(
    color: Colors.black,
    fontSize: 10.sp,
    fontWeight: FontWeight.w300,
    fontFamily: AppFonts.mulish);

const black12MulishTextStyle = TextStyle(
    color: Colors.black,
    fontSize: 13,
    fontWeight: FontWeight.w700,
    fontFamily: AppFonts.mulish);

const black400MulishTextStyle = TextStyle(
    color: Colors.black,
    fontSize: 18,
    fontWeight: FontWeight.w400,
    fontFamily: AppFonts.mulish);

const black18MulishTextStyle = TextStyle(
    color: Colors.black,
    fontSize: 18,
    fontWeight: FontWeight.w700,
    fontFamily: AppFonts.mulish);
const blackMulish1TextStyle = TextStyle(
    color: Colors.black,
    fontSize: 15,
    fontWeight: FontWeight.w600,
    fontFamily: AppFonts.mulish);

const black14Mulish1TextStyle = TextStyle(
    color: Colors.black,
    fontSize: 14,
    fontWeight: FontWeight.w400,
    fontFamily: AppFonts.mulish);

const grey13MulishTextStyle = TextStyle(
    color: Colors.grey,
    fontSize: 13,
    fontWeight: FontWeight.w700,
    fontFamily: AppFonts.mulish);

const grey10MulishTextStyle = TextStyle(
    color: Colors.grey,
    fontSize: 10,
    fontWeight: FontWeight.w700,
    fontFamily: AppFonts.mulish);

const grey12MulishTextStyle = TextStyle(
    color: AppColors.grey4Color,
    fontSize: 12,
    fontWeight: FontWeight.w400,
    fontFamily: AppFonts.mulish);

final blue12MulishTextStyle = TextStyle(
    color: AppColors.blue4Color,
    fontSize: 10.sp,
    fontWeight: FontWeight.w700,
    fontFamily: AppFonts.mulish);

final blue18MulishTextStyle = TextStyle(
    color: AppColors.blue3Color,
    fontSize: 15.sp,
    fontWeight: FontWeight.w700,
    fontFamily: AppFonts.mulish);

const blue700MulishTextStyle = TextStyle(
    color: AppColors.lightBlueColor,
    fontSize: 18,
    fontWeight: FontWeight.w700,
    fontFamily: AppFonts.mulish);

const blue600MulishTextStyle = TextStyle(
    color: AppColors.lightBlueColor,
    fontSize: 12,
    fontWeight: FontWeight.w600,
    fontFamily: AppFonts.mulish);

final greenMulishTextStyle = TextStyle(
    color: AppColors.green2Color,
    fontSize: 10.sp,
    fontWeight: FontWeight.bold,
    fontFamily: AppFonts.mulish);

final greenThreeMulishTextStyle = TextStyle(
    color: AppColors.green3Color,
    fontSize: 10.sp,
    fontWeight: FontWeight.bold,
    fontFamily: AppFonts.mulish);

const whiteMulishTextStyle = TextStyle(
    color: Colors.white,
    fontSize: 14,
    fontWeight: FontWeight.w600,
    fontFamily: AppFonts.mulish);

const white12MulishTextStyle = TextStyle(
    color: Colors.white,
    fontSize: 12,
    fontWeight: FontWeight.w600,
    fontFamily: AppFonts.mulish);

const white700MulishTextStyle = TextStyle(
    color: Colors.white,
    fontSize: 16,
    fontWeight: FontWeight.w700,
    fontFamily: AppFonts.mulish);

const white600MulishTextStyle = TextStyle(
    color: Colors.white,
    fontSize: 10,
    fontWeight: FontWeight.w600,
    fontFamily: AppFonts.mulish);

final blueMulish1TextStyle = TextStyle(
    color: AppColors.blue8Color,
    fontSize: 12.sp,
    fontWeight: FontWeight.w700,
    fontFamily: AppFonts.mulish);

const greenMulish1TextStyle = TextStyle(
    color: AppColors.greenColor,
    fontWeight: FontWeight.w700,
    fontFamily: AppFonts.mulish);

const blueMulish2TextStyle = TextStyle(
    color: AppColors.blue4Color,
    fontSize: 16,
    fontWeight: FontWeight.w700,
    fontFamily: AppFonts.mulish);

const blueMulish3TextStyle = TextStyle(
    color: AppColors.lightBlueColor,
    fontSize: 16,
    fontWeight: FontWeight.w500,
    fontFamily: AppFonts.mulish);

const blue16MulishTextStyle = TextStyle(
    color: AppColors.lightBlueColor,
    fontSize: 16,
    fontWeight: FontWeight.w600,
    fontFamily: AppFonts.mulish);

final montserratTextStyle = TextStyle(
  fontFamily: AppFonts.montserrat,
  fontSize: 12.sp,
  fontWeight: FontWeight.w600,
  color: Colors.black,
);

final montserrat400TextStyle = TextStyle(
  fontFamily: AppFonts.montserrat,
  fontSize: 10.sp,
  fontWeight: FontWeight.w400,
  color: Colors.black,
);

const montserrat500TextStyle = TextStyle(
  fontFamily: AppFonts.montserrat,
  fontSize: 16,
  fontWeight: FontWeight.w500,
  color: Colors.black,
);
const montserrat20TextStyle = TextStyle(
  fontFamily: AppFonts.montserrat,
  fontSize: 20,
  fontWeight: FontWeight.w500,
  color: Colors.black,
);

const roboto16TextStyle = TextStyle(
  fontFamily: AppFonts.roboto,
  fontSize: 16,
  fontWeight: FontWeight.w600,
  color: AppColors.black,
);

const roboto500TextStyle = TextStyle(
  fontFamily: AppFonts.roboto,
  fontSize: 16,
  fontWeight: FontWeight.w500,
  color: Colors.black,
);

const greenColorArialTextStyle = TextStyle(
  fontFamily: AppFonts.arial,
  fontSize: 24,
  fontWeight: FontWeight.w400,
  color: AppColors.green13Color,
);
