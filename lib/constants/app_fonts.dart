class AppFonts {
  static const String poppins = 'poppins';
  static const String lato = 'Lato';
  static const String inter = 'inter';
  static const String mulish = 'mulish';
  static const String montserrat = 'montserrat';
  static const String roboto = 'roboto';
  static const String arial = 'arial';
}
