class AppStrings {
// --------- LogIn Screen and LogIn Form Text is Here --------- //

  static const String skip = 'Skip'; // Login Screen And Sign Up Screen
  static const String myAwesomeSkill =
      'My AwesomeSkills'; // Login Screen And Sign Up Screen
  static const String createFreeAccount =
      'Create your free account'; // Login Screen And Sign Up Screen
  static const String emailAddress = 'Email Address';
  static const String howItWork = 'How it Works';
  static const String emailTextAddress =
      'Joe@doe.com'; // Login Screen And Sign Up Screen
  static const String password = 'Password'; // Login Screen And Sign Up Screen
  static const String textPassword =
      'Johny~/4567'; // Login Screen And Sign Up Screen
  static const String forgotPassword = 'Forgot password?';
  static const String logIn = 'Log In';
  static const String orLoginWith = 'Or login with';
  static const String google = 'Google';
  static const String facebook = 'Facebook';
  static const String apple = 'Apple';
  static const String logOut = 'Log Out';

// --------- SignUp Screen and  SignUo Form Text is Here --------- //

  static const String personalAccount = 'PERSONAL ACCOUNT';
  static const String personal = 'personal';
  static const String businessAccount = 'BUSINESS ACCOUNT';
  static const String fullName = 'Full Name';
  static const String nameText = 'Joe Doe';
  static const String email = 'Email';
  static const String businessUrl = 'Business URL';
  static const String businessHintText = 'Business.com/';
  static const String moreCharacterUse =
      'Use 8 or more character with a mix of letters,';
  static const String numberAndSymbol = 'numbers & symbols';
  static const String confirmPassword = 'Confirm Password';
  static const String acceptTermCondition =
      'Please accept the terms and condition of seing the application and data protection';
  static const String signUp = 'Signup';

// --------- Landing Screen --------- //
  static const String logoText = 'Logo';
  static const String logInLandingScreen = 'Log in';
  static const String signUpLandingScreen = 'Sign up';

// ----- Bottom Navigation ------//
  static const String home = 'Home';
  static const String canHelp = 'I Can Help';
  static const String needHelp = 'I Need Help With';
  static const String getQuotes = 'Get quotes';
  static const String getQuotesSmall = 'Get quotes';
  static const String pastJob = 'Past Jobs';
  static const String createAccount = 'Create an account';
  static const String signIn = 'Sign In';
  static const String whatAdWould = 'What Ad would you like to post?';
  static const String toPosAndAd = 'To post and ad please do the following:';
  static const String ifYouAreStuck =
      'If you are stuck with a job or a project,'
      'here you can post to get help';

  static const String hereYouCanPost = 'Here you can post your skills,'
      'or services so that others can hire you';

  // ----- PostAdsHelpCard  ------//

  static const String iNeedHelp = 'I need help with';
  static const String iCanHelp = 'I can help with';
  static const String listYourBusinessSmall = 'List your business';

// ----- CustomScrollAppBar  ------//
  static const String findLocal = 'Find Local';
  static const String professionalForText = 'Professional For';
  static const String prettyMuchText = 'Pretty Much Anything';
  static const String whatOnYourToDo = "What's on  your to-do list";
  static const String search = 'Search';
  static const String allAdsUpTo = 'All Ads up to \$250 are ';
  static const String toPost = 'to post.';
  static const String noCommissionTaken = ' No Commission taken at all.';
  static const String overDollarTwoFiftyItIsOnly =
      'Over \$250 it is only \$9 per Ad';
  static const String monthlyPlansStartingFrom =
      'Monthly Plans start from \$19 and';

// ---------- Landing Screen Body -------- //

// Part Drawer ------- //

  static const String listYourBusiness = 'List your Business';
  static const String ourPlans = 'Our Plans';
  static const String aboutUs = 'About Us';
  static const String howItWorks = 'How it works';
  static const String myCommunity = 'My Community';
  static const String contactUs = 'Contact Us';

  // Contact Us  Strings ------- //
  static const String sendMessage = 'Send Message';

  static const String canHelpWith = 'I Can Help With';
  static const String useCurrentLocation = 'Use current location';
  static const String distance = 'Distance';
  static const String priceRange = 'Price range';
  static const String apply = 'Apply';
  static const String postAndAd = 'Post and ad';

  static const String fromItsMedievalOrigins =
      'From its medieval origins the digital easily '
      'build waiting rooms'
      'readable english desktop publishing';
  static const String postAds = 'Post and ad';
  static const String pricing = 'Pricing';
  static const String ourFee = 'Our Fees';

  static const String featured = 'Featured';
  static const String jobs = 'Jobs';
  static const String ofTheDay = 'Of The Day';
  static const String explore = 'Explore';

  // ----  Drawer profile OverView ---- //
  static const String thirtyMin = '30min';
  static const String recommendations = 'Recommendations';
  static const String zeroFour = '04';
  static const String telephone = '0755-5642';
  static const String qbcc = 'QBCC 56854';
  static const String viewABNDetails = 'View ABN details';

  // ----   Profile OverView Personal ---- //
  static const String welcomeTpSpacePencil = 'Welcome To Space Pencil';
  static const String oneFortyTwoLikes = '142 likes';
  static const String zeroNineJobsInProgress = '09 jobs in progress';
  static const String tenFinishedJob = '10 Finished Job';
  static const String director = 'Director';
  static const String product = 'Product';
  static const String projects = 'Projects';

// -----  Part 1 Landing Screen Feature Card Screen ------ //

  static const String housePaint = 'House Painting';
  static const String southPort = 'SouthPort';
  static const String loremIpsumText =
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. '
      'Quis phasellus leo euismod fringilla. A metus, tellus '
      'euismod tincidunt nulla.';
  static const int thirtyDollar = 30;
  static const String onePointFiveText = '1.5/mo';
  static const String getPaid = 'Get Paid';
  static const String asap = 'ASAP';
  static const String frame = 'Frame';
  static const String mapView = 'MAP VIEW';
  static const String helpJobUnder = 'All help jobs under';
  static const String dollarTwoFifty = '\$250';
  static const String areFreeToPost = 'are free to post';
  static const String aFernandez = 'A.Fernandez';
  static const String zero = '0';
  static const String $300 = '\$300';
  static const String jobValue = 'JOB VALUE';
  static const String southPortQLD = 'Southport, 4216 QLD';
  static const String getHelpToday = 'Get Help Today !!';
  static const String viewMore = 'VIEW MORE';
  static const String viewAll = 'VIEW ALL';
  static const String handyman = 'Handyman Services';
  static const String minorHome = 'Minor Home Repair';
  static const String hireAPainter = 'Hire A Painter';
  static const String lightInstallation = 'Light Installation';
  static const String alex = 'Alex S.';
  static const String sixtyFour = '64';

  static const String featuredSkill = 'Featured Skills';
  static const String helpMoving = 'Helping Moving';
  static const String homeRepairs = 'Home Repairs';
  static const String furnitureAssembly = 'Furniture Assembly';
  static const String fortyOne = '\$41.30/hr';
  static const String thirtySeven = '\$37.64/hr';
  static const String thirtyFive = '\$35.29/hr';
  static const String jobSuccessPercentage = '0 % JOB SUCCESS';

// ---------- Sign In Drawer Part -------- //

  static const String astria = 'Astria';
  static const String memberSince = 'Member Since 2022';

// --------- Need Help Card Details --------- //
  static const String jobDescription = 'Job description';
  static const String jobId = 'Job id : 898692';

  static const String suppliesAndResource =
      'I have supplies and resources available for helping you'
      ' with your move ins, move outs and move aroun. I have supplies and '
      'resources available for helping you with your move ins, move outs'
      ' and move aroun...';
  static const String needToBeDone = 'What needs to be done';
  static const String message = 'Message';
  static const String call = 'Call';
  static const String acceptJob = 'Accept Job';
  static const String iAmInterested = 'I am Interested';
  static const String getHelpToUse = 'Get Help to Use this App';
  static const String suggestIdeaToImprove =
      'Suggest Ideas to improve this App';

// --------- Can Help Card  Details --------- //
  static const String responseRate = 'Response Rate';
  static const String ninetyEight = '98%';
  static const String responseTime = 'Response Time';
  static const String withInHalfHour = 'Within half an hour';
  static const String jobSuccess = 'Job Success     ';
  static const String ninetyNine = '99.99%';
  static const String oneFortyTwo = ' 142';
  static const String rightPersonJob = ' I’m the right person for the job:';
  static const String abn = 'ABN      12 597 034';
  static const String mobileNumber = '0433 987987';
  static const String emailSpam = 'spamflo@yahoo.com.au';
  static const String webMyClean = 'www.mycleanlady.com';

// --------- My Task Screen --------- //
  static const String myTask = 'My Task';
  static const String helloAgain = 'Hello again';
  static const String youHave = 'You have ';
  static const String five = '5';
  static const String taskToFinish = 'tasks to finish.';
  static const String toStart = 'To Start';
  static const String releaseProject = 'Release Project > Stage 1';
  static const String updateContractorAgreement =
      'Update contractor agreement consectetuer adipiscing elit.';
  static const String inProgress = 'In Progress';
  static const String completed = 'Completed';

// --------- My Ads Screen --------- //
  static const String myAds = "My Ad's";
  static const String live = "Live";
  static const String paused = "Paused";
  static const String expired = "Expired";
  static const String postAnAds = "Post An Ad";
  static const String post = "Post";
  static const String anAd = "An Ad";
  static const String pause = "Pause";
  static const String delete = "Delete";
  static const String edit = "Edit";
  static const String play = "Play";
  static const String repost = "Repost";
  static const String areYouSureDelete = "Are you sure you want to delete?";
  static const String areYouSurePause = "Are you sure you want to Pause?";
  static const String deviceDeleteCard =
      "This will delete the Card from your device.";
  static const String devicePauseCard =
      "This will Pause the Card from your device.";
  static const String cancel = "cancel";

// --------- My Skill Screen --------- //
  static const String mySkills = "My Skills";
  static const String webDesigner = "Web Designer";
  static const String myAwesomeSkillText = 'myAwesome Skills';
  static const String editSkill = 'edit';
  static const String top3Skills =
      'Your top 3 skills will be displayed in your AD';
  static const String webDesign = 'Web design';
  static const String twentyFive = '\$25/hr';
  static const String python = 'Python';
  static const String forty = '\$40/hr';
  static const String uIDesign = 'UI design';
  static const String thirty = '\$30/hr';
  static const String addMoreSkills = 'Add More Skills';
  static const String nameOfSkills = 'Name of skills';
  static const String enterYourPrice = 'Enter your price';

  static const String create = 'create';
  static const String language = 'Languages';
  static const String addWhatOtherLanguage = 'Add Other Languages';
  static const String tradeCertificate = 'Trade Certificate';
  static const String addMore = 'Add More ';
  static const String transport = 'Transport ';
  static const String walk = 'Walk ';
  static const String bike = 'Bike ';
  static const String motor = 'Motor ';
  static const String bikeScooter = 'Bike/Scooter ';
  static const String car = 'Car ';
  static const String truck = 'Truck ';
  static const String myWorksProject = 'My Works/Projects';
  static const String rollandCampaing = 'Rolland Campaing';
  static const String appleDesign = 'Apple New Design';
  static const String studioArt = 'Studio of ART';
  static const String visualArt = 'Visual ART';

// --------- My Quotes Screen --------- //
  static const String myQuotes = "My Quotes";
  static const String tomLatham = "Tom Latham";
  static const String hashZeroOne = "#014523";
  static const String quotesPaused = 'Quotes Paused';
  static const String restart = 'Restart';
  static const String houseMoving = "House Moving";
  static const String toHelpWithMoving =
      "To help with the moving it will cost you \$350";
  static const String eightHundred = "\$800";
  static const String twentyFiveJanuary = "25 January 2022";

// --------- My analytic_business Screen --------- //
  static const String analytics = "Analytics";
  static const String totalVisit = "1495 Total visits";
  static const String plusTwentyFive = "+25% ";
  static const String newLike = "142 New Likes";
  static const String statistics = "Statistics";
  static const String weekly = "Weekly";
  static const String last30Day = "Last 30 Days";
  static const String last60Day = "Last 60 Days";
  static const String visits = "Visits";
  static const String thirtyTwo = "32";
  static const String newJobs = "New Jobs";
  static const String jobInProgress = "Jobs in progress";
  static const String finishedJobs = "Finished jobs";
  static const String recentReviews = "Recent reviews";

// --------- My Message Screen --------- //
  static const String overAllMessage = "Overall Messages";
  static const String twoThreeDoubleNine = "2399";
  static const String thisMonth = "this month";
  static const String sent = "Sent";
  static const String twoThreeNine = "239";
  static const String receivedMessage = "Received Message";
  static const String pleaseYourMessage =
      "Please note that all your messages will be held for 60 days, After that they will automatically deleted";
  static const String active = "Active";
  static const String archive = "Archived";
  static const String rejected = "Rejected";
  static const String rupertLandstroom = "rupert_landstroom";
  static const String onlineNow = "Online now";
  static const String threeHourAgo = "3 hour ago";
  static const String seven = "7";
  static const String address = "Address";
  static const String jobValueSmall = 'Job Value';
  static const String jobDetail = 'Job Details';
  static const String iNeedPaimter =
      'I need a paimter to paint my 3 bed room home. '
      'The cellings are standard 2.4m high. Need it done by 30 March, no day '
      'dreamers that think that they can paint, must be experienced.';
  static const String tenTwenty = '10:20am';
  static const String helloHowMayI = 'Hellow, how may i help you';
  static const String yesterday = 'Yesterday';
  static const String typeYourMessage = 'Type your message here...';
  static const String reply = 'Reply';
  static const String rejectedJob = 'Reject Job';

// --------- My Review Screen --------- //
  static const String reviews = "Reviews";
  static const String myReview = "My Reviews";
  static const String leaveFeedback =
      "Leave feedback of your experience with a user and their listing and help us build a smarter community.\n"
      "The feedback you leave will help us create a trust factor and over all rating for a user.\n"
      "Please try to resolve issues with the user before leaving feedback and avoid in using rude and abusive comments.";
  static const String pending = "Pending";
  static const String writeReview = "Write Review";
  static const String projectToReview = "Project to Review";
  static const String twoMarch2022 = "2 MARCH,2022";
  static const String easyToWork =
      "Easy to work with, followed instructions correctly,";
  static const String waitingOnReview = 'Waiting on Review';
  static const String writeAReview = 'Write A Review';
  static const String yourRatings = 'Your Ratings';
  static const String yourComment = 'Your Comment';
  static const String maximumLength = 'Maximum length is 500 words';
  static const String honesty = 'Honesty';
  static const String workQuality = 'Work quality';
  static const String technicalSkill = 'Technical skill';
  static const String punctuality = 'Punctuality';
  static const String communication = 'Communication';
  static const String typeCharacterYouSee =
      'Type the character you see in the picture';
  static const String tripleSevenTripleNine = '777999212';
  static const String iAccept =
      'I accept terms and conditions and privacy policy.';
  static const String postYourReview = 'Post your review now';
  static const String webDesignSmall = 'Web designer';

// --------- My Favourite Screen --------- //
  static const String favourite = "Favourites";
  static const String savedFavourite = "saved favourites.";

// --------- My Settings Screen --------- //
  static const String settings = "Settings";
  static const String helloAgainAstria = 'Hello again Astria';
  static const String twenty = '20%';
  static const String profileCompletion = 'Profile Completion';
  static const String profileSettings = 'Profile Settings';
  static const String inComplete = 'Incomplete';
  static const String alertNotification = 'Alerts/Notification';
  static const String securityPassword = 'Security/Password';
  static const String socialMedia = 'Social Media';
  static const String free = 'FREE';
  static const String myPlan = 'My Plan';
  static const String postedThreeOfFourAds = 'Posted 3 of 4 ads';
  static const String creditPlusDebitCard = 'CREDIT + DEBIT CARDS';
  static const String paymentMethod = 'Payment Method';
  static const String upgradeBusiness = 'Upgrade to Business Account';

// --------- My Profile Settings Screen --------- //
  static const String profilePicture = 'Profile Picture';
  static const String optional = '(optional)';
  static const String uploadOrChange = 'Upload or change user profile picture';
  static const String uploadPhotoForPastJob =
      'Upload photos from past jobs/projects';
  static const String dragAndDrop = 'Drag and Drop picture here';
  static const String or = 'or';
  static const String clickToBrowse = 'Click to Browse';
  static const String aboutMe = 'About Me';
  static const String star = '*';
  static const String portfolio = 'Portfolio';
  static const String uploadPhoto = 'Upload photos from past jobs/projects.';
  static const String resume = 'Resume';
  static const String uploadYourResume = 'Upload your resume in a PDF format.';
  static const String education = 'Education';
  static const String whatLevelOfEducation =
      'What level of education have you reached.';
  static const String awards = 'Awards';
  static const String listYourLifeAchievement = 'List your life achievements.';
  static const String contact = 'Contact';
  static const String insertYourContact = 'Insert your contact details.';
  static const String mobile = 'Mobile';
  static const String homePhone = 'Home phone';
  static const String emailDash = 'e-mail';
  static const String website = 'Website';
  static const String save = 'Save';

// --------- Alert Notification Settings Screen --------- //
  static const String alertNotifications = 'Alerts/Notifications';
  static const String quotesPlusReview = 'Quotes + Reviews';
  static const String emailNotificationSettings =
      'Email Notifications Settings';
  static const String howWouldYouLike =
      'How would you like to receive notifications?';
  static const String getNotified =
      'Get notified for new posted jobs within your area.';
  static const String emailName = 'fbelbe@yahoo.com.au';
  static const String sms = 'SMS';
  static const String phoneNumber = '0433 871732';
  static const String jobAlert = 'Job Alerts';
  static const String addKeywords = 'Add keywords';
  static const String chooseKMSRadius = 'Choose kms radius';
  static const String extraNotification = 'Extra Notifications';
  static const String chooseExtraNotifications =
      'Choose extra notifications you would like to receive';
  static const String forBusiness = 'For Business';
  static const String securityPasswordLogIn = 'Security/Password/Log in';
  static const String yourSecurity = 'Your Security';
  static const String secureYourAccount =
      'Secure your account using the options below.';
  static const String telephoneNumber = 'Telephone Number';
  static const String youHaveNotAssociated =
      'You have not associated a phone number with '
      'this account. Adding a phone number adds additional security by enabling'
      ' two factor authentication, preventing unauthorized access to your account.';
  static const String addMobileNumber = 'Add mobile number';
  static const String confirmCode = 'Confirm code that was sent to your mobile';
  static const String addPhone = 'Add Phone';
  static const String fingerPrintSignIn = 'Fingerprint Sign In';
  static const String useFingerPrint =
      'Use your fingerprint to recognize and verify your account log in. ';
  static const String faceRecognition = 'Face Recognition Sign In';
  static const String useYourFace =
      'Use your face to verify your account log in. ';
  static const String twoFactorAuthentication = 'Two-Factor Authentication';
  static const String byEnablingTwo =
      'By enabling two factor authentication it will'
      'prevent unauthorized access to your account ';
  static const String optionalBracket = '(optional)';
  static const String addressVerification = 'Address Verification';
  static const String verifyYourPhysical =
      'Verify your physical address which can assist '
      'int the recovery of your account if compromised.'
      ' the address by google maps or australia post.';
  static const String resetPassword = 'Reset Password';
  static const String securityQuestion = 'Security Question';
  static const String byAddingSecurity =
      'By adding a security question, we can identify '
      'you based on your answer if you ever lost access to your account or email';
  static const String selectSecurityQuestion = 'Select a security questions';
  static const String yourAnswer = 'Your answer';
  static const String typeYourQuestion = 'Type your question';
  static const String oldPassword = 'Old Password';
  static const String typeYourAnswer = 'Type your answer';
  static const String set = 'Set';

// --------- Social Media Settings Screen --------- //
  static const String manageYourLinked = 'Manage Your Linked';
  static const String socialAccount = 'Social Accounts';
  static const String twitter = 'Twitter';
  static const String instagram = 'Instagram';
  static const String linkedIn = 'LinkedIn';

// --------- Free Plan Settings Screen --------- //
  static const String yourAreCurrently = 'You are currently on this plan.';
  static const String upgrade = 'Upgrade';
  static const String invoices = 'Invoices';
  static const String viewAllInvoices = 'View all invoices';
  static const String downloadInvoices = 'Download invoices';
  static const String billingInformation = 'Billing Information';
  static const String masterCardEnding = 'Mastercard Ending in 4620';
  static const String sevenSlashTwenty = '07/20';
  static const String visaEnding = 'Visa Ending in 1198';
  static const String americanExpressEnding = 'American Express Ending in 0385';
  static const String yourChosenPayment = 'Your chosen payment methods';
  static const String masterCardNumber = '**** **** **** 1282';
  static const String addOnTen = 'Added on  10/03/17';

// --------- Ad Can help Screen  --------- //
  static const String ad = 'Ad';
  static const String getNoticed = 'Get Noticed';
  static const String standardAd = 'Standard Ad';
  static const String freeCapital = 'FREE';
  static const String featuredAd = 'Featured Ad';
  static const String tenDay = '\$10 for 10days';
  static const String jobTitle = 'Job tittle';
  static const String useTitleAndFair =
      'Use a title and a fair price to attract more buyers.';
  static const String eightyCharacter = '80 Characters left';
  static const String beAsDescriptive = 'Be as descriptive as possible. Provide'
      'samples and explain what you will and'
      'will not do';
  static const String extraInstruction = 'Extra instructions';
  static const String whichCategoryIsThe =
      'Which category is the best for this job';
  static const String tags = 'Tags';
  static const String uiUX = 'UI/UX';
  static const String design = 'Design';
  static const String app = 'App';
  static const String whichSubRub = 'Which suburb is the job in?';
  static const String jobPrice = 'Job price';
  static const String whenDoYouNeed = 'When do you need the work to start?';
  static const String nextFewDays = 'Next few days';
  static const String week = "Week";
  static const String iAmFlexible = "I am Flexible";
  static const String totalPriceDollar = 'Total price in \$';
  static const String hourlyRate = 'Hourly rate in \$';
  static const String fixedPrice = 'Fixed price';
  static const String workPlace = 'Workplace';
  static const String toBeCompleted = 'To be completed in person';
  static const String canBeCompleted = 'Can be completed online';
  static const String uploadImageOptional = 'Upload images (optional)';
  static const String youtubeVideoLinks = 'Youtube video links';
  static const String tenTenDay = '\$10(10 days)';
  static const String add = 'Add';
  static const String saveForLater = 'Save for later';
  static const String submit = 'Submit';
  static const String previewAd = 'Preview AD';
  static const String postAd = 'Post AD';

// --------- Ad Can help Screen  --------- //
  static const String industry = 'Industry';
  static const String title = 'Title';
  static const String selectIndustries =
      'Select the industries you want to receive leads for';
  static const String featuredSkillsHourly = 'Featured Skills Hourly Rates';
  static const String typeYourSkills = 'Type your skills';
  static const String dollarFortyFive = '\$45hr';
  static const String helpRepair = 'Help Repair';
  static const String forAnExtra = 'For an Extra';
  static const String iWill = 'I will';
  static const String traveling = 'Traveling(km radius)';
  static const String howManyKMYouAre =
      'How many KM are you willing to travel for a job';
  static const String iamTheRightPerson = "I'm the right person for the job:";
  static const String mySkill = 'My skill : ';
  static const String myEducation = 'My education : ';
  static const String postJobs = 'Post jobs : ';
  static const String uploadImages = 'Upload images';

// --------- Get Quotes Screen  --------- //
  static const String description = 'Description';
  static const String tellUsMoreAboutThe = 'Tell Us More About The Job';
  static const String whereIsTheJob = 'Where is the job located?';
  static const String enterPostCodeOrSubRub = 'Enter postcode or subrub';
  static const String whenDoYouNeeTheWork =
      'When do you need the work to start?';
  static const String whatTypeOfJObs = 'What type of jobs this?';
  static const String chooseCategory = ''
      'Choose a Category ';

  static const String isThisRepairOrInstallation =
      'Is this a repair or installation job?';
  static const String quotesToIncludeMaterial =
      'Do you want the quote to include materials, labour or both?';
  static const String pleaseProvideSizeArea =
      'Please provide job size area (m2)';
  static const String enterANumber = 'Enter a number';
  static const String canTradesManVisitTheSite =
      'Can tradesman visit the site to give quotes?';
  static const String yourName = 'Your name';
  static const String bestNumberToContactYou = 'Best Number to contact you';

// --------- Extend Get Quotes Screen  --------- //
  static const String staceyPetCare = "Stacey's Pet Care";
  static const String jobQuotesDollar = "Job Quote : \$65";
  static const String hereIsYourQuotes = "Here is your quote for : ";
  static const String petGrooming = "Pet grooming";
  static const String hiDiane = 'HI Diane,';
  static const String tOComeOutAnd = "To come out and give your dog a full "
      "wash you are looking at approx \$65."
      "this does not include nail clipping or other extras that you nay require."
      "Please go to our website and see the full options that are available for your"
      "pet.Looking forward to hear from you your paradise point pet grooming florin.";
  static const String bundallQLD = "Bundall QLD";

  // ----  Personal Account Text ---- //
  static const String createPersonalAccount = 'Create Personal Account';
  static const String profileUserName = 'Profile/UserName';
  static const String dateOfBirth = 'Date of Birth';

  static const String finish = 'Finish';

  // ----  Notification Screen Text ---- //
  static const String notifications = 'Notifications';
  static const String yourOrderIsPlaced = 'Your Order is Placed';
  static const String viewMoreCapital = 'View More';
  static const String dummyText =
      'Lorem Ipsum Is Simply Dummy Text Of The Printing...';
  // ----  Drawer profile OverView Business ---- //
  static const String company = 'Company';

// Business Account Text ---- //
  static const String inSeasonPTYLTD = "In Season PTY LTD";
  static const String joinedIn2007 = "Joined in 2007";
  static const String adminFlorinBelbe = "Admin : Florin Belbe";
  static const String addMoreProjects = "ADD MORE PROJECTS";
  static const String companyDetails = "Company Details";
  static const String services = "Services";
  static const String servicesSmall = "services";
  static const String companyLife = "Company Life";
  static const String receivingLeads = "Receiving Leads";
  static const String security = "Security";
  static const String ceo = "CEO";
  static const String stateManager = "State Manager";
  static const String customerHelp = "Customer help";
  static const String administrator = "Administrator";
  static const String companyAddress =
      "PO.BOX 932 SURFERS PDS, 4217 QLD Primary Billing Address";

  static const String fbelbeSpaflo = "fbelbe@spaflo.com.auPrimary";
  static const String phoneNumberText = "Phone Number";
  static const String certificateAndLicense = "Certificates/License";
  static const String writeDownYour = "Write down your list of";
  static const String thatYouIntend = "that you intend to advertise.";
  static const String addFingerPrint = "Add Fingerprint";
  static const String addFaceRecognition = "Add Face Recognition";
  static const String addAddress = "Add Address";

  static const String pauseAllIncomingQuotes = "Pause all incoming Quotes";
  static const String oneDashThreeDays = "1-3days";

  // ----  List Your Business ---- //
  static const String createBusinessAccount = 'Create Business Account';
  static const String applicantDetails = 'Applicant Details';
  static const String positionInTheCompany = 'Position in the company';
  static const String ownerDirectorCeo = 'Owner, Director, CEO';
  static const String userNameYouWillNeed =
      'Username (you  will need it to make any changes to the account)';
  static const String passwordYouWillNeed =
      'Password (you  will need it to make any changes to the account)';
  static const String OR = 'OR';
  static const String createManage = 'Create/Manage Admin Account';
  static const String meetTheTeam = 'Meet the Team';
  static const String companyBusinessDetail = 'Company/Business Details';
  static const String doYouHaveOrRequire =
      'Do you have or require a license for your industry?';
  static const String yes = 'Yes';
  static const String no = 'No';
  static const String license = 'License';
  static const String certificate = 'Certificates';
  static const String tagLine = 'Tagline';
  static const String video = 'Video';
  static const String manageYourLinkedSocialAccount =
      'Manage your Linked Social Accounts';
  static const String yourAreOnThe = 'Your are on the';
  static const String freePlan = 'FREE PLAN';
  static const String searchByBusiness =
      ' Search by Business/Trading Name or ABN';
  static const String abnText = 'ABN';
  static const String tradingName = 'Trading Name';
  static const String thisIsTheNameTo =
      'This is the name to be displayed for customers to find you';
  static const String subrub = 'Subrub';
  static const String ashmore = 'Ashmore';
  static const String noPoBoxAccepted = 'No PO BOX Accepted';
  static const String state = 'State';
  static const String QLD = 'QLD';
  static const String postCode = 'Post Code';
  static const String landLine = 'Landline';
  static const String link = 'Link';
  static const String brieflyDescribeYourCompany =
      'Briefly describe your company';
  static const String aLinkToAVideoOf = 'A link to a video of your company';
  static const String brieflyDescribeWhatYourCompanyDoes =
      'Briefly describe what your company does';
  static const String howManyKM =
      'How many KM are you willing to travel for a job?';
  static const String zeroKilometer = '0km';
  static const String hundredKilometer = '100km';
  static const String sixtyFiveKilometer = '65km';
  static const String category = 'Category';
  static const String subCategory = 'SubCategory';
  static const String subSubCategory = 'Sub SubCategory';
  static const String youCanHaveTwoSources =
      'You can have two sources of your reviews from other websites';
  static const String ebay = 'Ebay';
  static const String other = 'Other';
  static const String addPaymentMethod = 'Add Payment Method';
  static const String resetPasswordSmall = 'Reset Password';
  static const String createPassword = 'Create Password';
  static const String newPassword = 'New Password';
  static const String change = 'Change';
  static const String saver = 'SAVER';
  static const String freeToPost = '\$-\$250 FREE TO POST ALL ADS';
  static const String contactAnyRegistered = 'CONTACT ANY REGISTERED MEMBER';
  static const String canBrowseAll = 'CAN BROWSE ALL ADS';
  static const String thisPlanHasNo = 'THIS PLAN HAS NO EXPIRING DATE';
  static const String nine = '9';
  static const String nineteen = '19';
  static const String twentyNine = '29';
  static const String perMonth = 'PER MONTH';
  static const String trader = 'TRADIES';
  static const String casual = 'CASUAL';
  static const String yourAllWill = 'YOUR AD WILL RUN FOR 28 DAYS';
  static const String giveUnlimitedQuotes = 'GIVE UNLIMITED QUOTES';
  static const String receiveUnlimitedQuotes = 'RECEIVE UNLIMITED QUOTES';
  static const String freeToPostUpTo = 'FREE TO POST UP TO 4 ADS OVER \$250';
  static const String recommended = 'RECOMMENDED';
  static const String business = 'BUSINESS';
  static const String freeToPostUpToEight =
      'FREE TO POST UP TO 8 ADS OVER \$200';
  static const String fullBusinessProfilePage = 'FULL BUSINESS PROFILE PAGE';
  static const String upgradeCapital = 'UPGRADE';
  static const String selected = 'SELECTED';

  // ----  local Storage ---- //

  static const String isLoggedIn = 'isLoggedIn';
  static const String isLoggedOut = 'isLoggedOut';
  static const String isUserSignedUp = 'isUserSignedUp';
  static const String authToken = 'authToken';
  static const String fcmToken = 'fcmToken';
  static const String isNew = 'isNew';
  static const String user = 'user';
  static const String firebaseCredentails = 'firebaseCredentails';
}
