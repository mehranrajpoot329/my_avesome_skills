import 'package:AwesomeSkills/Modules/Business/landing_screen_business/Need_Help_business/need_help_message_screen_business.dart';
import 'package:AwesomeSkills/Modules/Business/landing_screen_business/Can_Help_business/can_help_message_screen_business.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/Can_Help/can_help_message_screen.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/Need_Help/need_help_message_screen.dart';
import 'package:AwesomeSkills/Modules/login_signup/login_screen.dart';
import 'package:AwesomeSkills/Modules/login_signup/signup_screen.dart';
import 'package:AwesomeSkills/Modules/splash_screen.dart';
import 'package:AwesomeSkills/local/db/local_storage.dart';
import 'package:AwesomeSkills/routes/app_pages.dart';
import 'package:AwesomeSkills/utils/utils.dart';
import 'package:get/get.dart';
import '../Modules/Business/ad_Screen_business/ad_screen_can_help_business.dart';
import '../Modules/Business/ad_Screen_business/ad_screen_need_help_business.dart';
import '../Modules/Business/custom_scroll_appBar_business/custom_scroll_appBar_business.dart';
import '../Modules/Business/drawers_screens_business/analytic_business/analytics_business_screen.dart';
import '../Modules/Business/drawers_screens_business/favourites_business/favourite_business_screen.dart';
import '../Modules/Business/drawers_screens_business/messages_business/message_business_screen.dart';
import '../Modules/Business/drawers_screens_business/messages_business/messgae_typing_screen_business.dart';
import '../Modules/Business/drawers_screens_business/my_ads_business/my_ads_business.dart';
import '../Modules/Business/drawers_screens_business/my_quotes_business/my_quotes_business_screen.dart';
import '../Modules/Business/drawers_screens_business/my_skills_business/my_skill_business.dart';
import '../Modules/Business/drawers_screens_business/my_task_business/my_task_completed_business/my_task_completed_business.dart';
import '../Modules/Business/drawers_screens_business/my_task_business/my_task_inprogress_business/my_task_inprogress_business.dart';
import '../Modules/Business/drawers_screens_business/my_task_business/my_task_to_start_business/my_task_to_start_business.dart';
import '../Modules/Business/drawers_screens_business/my_task_business/my_tasks_business.dart';
import '../Modules/Business/drawers_screens_business/review_business/reviews_business_screen.dart';
import '../Modules/Business/drawers_screens_business/settings_business/about_us_business/about_us_business.dart';
import '../Modules/Business/drawers_screens_business/settings_business/alert_notification_settings_business/alert_notfication_setting_business.dart';
import '../Modules/Business/drawers_screens_business/settings_business/company_details_business/company_details_business.dart';
import '../Modules/Business/drawers_screens_business/settings_business/company_life_business/company_life_business.dart';
import '../Modules/Business/drawers_screens_business/settings_business/free_plan_settings_business/free_plan_screen_business.dart';
import '../Modules/Business/drawers_screens_business/settings_business/payment_method_screen/payment_method_screen_business.dart';
import '../Modules/Business/drawers_screens_business/settings_business/receive_leading_business/receive_leading_business.dart';
import '../Modules/Business/drawers_screens_business/settings_business/security_password_settings_business/security_password_setting_business.dart';
import '../Modules/Business/drawers_screens_business/settings_business/service_business/service_business.dart';
import '../Modules/Business/drawers_screens_business/settings_business/settings_business_screen.dart';
import '../Modules/Business/drawers_screens_business/settings_business/social_media_settings_business/social_media_screen_business.dart';
import '../Modules/Business/get_quotes/extend_quotes_screen.dart';
import '../Modules/Business/get_quotes/get_quotes_screen.dart';
import '../Modules/Business/landing_screen_business/Can_Help_business/can_help_card_detail_business.dart';
import '../Modules/Business/landing_screen_business/Can_Help_business/can_help_screen_business.dart';
import '../Modules/Business/landing_screen_business/Can_Help_business/can_help_screen_with_out_bottom_nav_business.dart';
import '../Modules/Business/landing_screen_business/Need_Help_business/need_help_card_detail_business.dart';
import '../Modules/Business/landing_screen_business/Need_Help_business/need_help_screen_business.dart';
import '../Modules/Business/landing_screen_business/Need_Help_business/need_help_screen_with_out_bottom_nav_business.dart';
import '../Modules/Business/profile_over_view_business/profile_over_view_business.dart';
import '../Modules/Personal/ad_Screen/ad_screeb_can_help_personal/ad_screen_can_help_personal.dart';
import '../Modules/Personal/ad_Screen/ad_personal_need_help_personal/ad_screen_need_help_personal.dart';
import '../Modules/Personal/contact_us/contact_us_screen.dart';
import '../Modules/Personal/drawers_screens/Analytics/analytics.dart';
import '../Modules/Personal/drawers_screens/favourites/favourite_screen.dart';
import '../Modules/Personal/drawers_screens/messages/message_screen.dart';
import '../Modules/Personal/drawers_screens/messages/message_typing_screen.dart';
import '../Modules/Personal/drawers_screens/my_ads/my_ads.dart';
import '../Modules/Personal/drawers_screens/my_quotes/my_quotes_screen.dart';
import '../Modules/Personal/drawers_screens/my_skills/my_skill.dart';
import '../Modules/Personal/drawers_screens/my_task/my_task_completed/my_task_completed.dart';
import '../Modules/Personal/drawers_screens/my_task/my_task_inprogress/my_task_inprogress.dart';
import '../Modules/Personal/drawers_screens/my_task/my_task_to_start/my_task_to_start.dart';
import '../Modules/Personal/drawers_screens/my_task/my_tasks.dart';
import '../Modules/Personal/drawers_screens/review/reviews_screen.dart';
import '../Modules/Personal/drawers_screens/settings/alert_notification_settings/alert_notfication_setting_screen.dart';
import '../Modules/Personal/drawers_screens/settings/free_plan_settings/free_plan_screen.dart';
import '../Modules/Personal/drawers_screens/settings/my_profile_setting/my_profile_setting.dart';
import '../Modules/Personal/drawers_screens/settings/payment_method_screen/payment_method_screen.dart';
import '../Modules/Personal/drawers_screens/settings/security_password_settings/security_password_setting_screen.dart';
import '../Modules/Personal/drawers_screens/settings/settings_screen.dart';
import '../Modules/Personal/drawers_screens/settings/social_media_settings/social_media_screen.dart';
import '../Modules/Personal/landing_screen/Can_Help/can_help_card_detail.dart';
import '../Modules/Personal/landing_screen/Can_Help/can_help_screen.dart';
import '../Modules/Personal/landing_screen/Can_Help/can_help_screen_with_out_bottom_nav.dart';
import '../Modules/Personal/landing_screen/Need_Help/need_help_card_detail.dart';
import '../Modules/Personal/landing_screen/Need_Help/need_help_screen.dart';
import '../Modules/Personal/landing_screen/Need_Help/need_help_screen_with_out_bottom_nav.dart';
import '../Modules/Personal/list_your_business/list_your_business.dart';
import '../Modules/Personal/personal_info_for_post_ad/personal_info_for_post_ad.dart';
import '../Modules/Personal/profile_over_view_personal/profile_over_view_personal.dart';
import '../widgets/custom_scroll_appBar/custom_scroll_appBar.dart';

class AppRoutes {
  static getInitialRoute() {
    return AppPage.splashScreen;
  }

  static void moveToNext() {
    bool isNew = HiveHelper.isNew();
    bool isAlreadyLoggedIn = HiveHelper.isLoggedIn();
    if (isNew) {
      Utils.showProgressBar();
      Future.delayed(3.seconds, () => Get.offAllNamed(AppPage.landingScreen));
      return;
    } else if (isAlreadyLoggedIn) {
      Utils.showProgressBar();
      Future.delayed(3.seconds, () => Get.offAllNamed(AppPage.landingScreen));
      return;
    } else if (HiveHelper.isLoggedOut() || !isAlreadyLoggedIn) {
      Future.delayed(3.seconds, () => Get.offAllNamed(AppPage.loginScreen));
    }
  }

  static getPagesPersonal() {
    return [
      //  --- PERSONAL ACCOUNT ROUTES  --- //

      GetPage(name: AppPage.splashScreen, page: () => SplashScreen()),

      GetPage(name: AppPage.landingScreen, page: () => CustomAppBarScreen()),

      GetPage(name: AppPage.canHelpScreen, page: () => CanHelpScreen()),
      GetPage(
          name: AppPage.canHelpScreenWithOutBottomNav,
          page: () => CanHelpScreenWithOutBottomNav()),

      GetPage(name: AppPage.needHelpScreen, page: () => NeedHelpScreen()),

      GetPage(
          name: AppPage.needHelpMessageScreen,
          page: () => NeedHelpMessagingScreen()),

      GetPage(
          name: AppPage.canHelpMessageScreen,
          page: () => CanHelpMessagingScreen()),
      GetPage(
          name: AppPage.needHelpScreenWithOutBottomNav,
          page: () => NeedHelpScreenWithOutBottomNav()),

      GetPage(
        name: AppPage.adScreenCanNeedHelpPersonal,
        page: () => AdScreenCanHelpPersonal(),
      ),

      GetPage(
          name: AppPage.personalInfoForPostAd,
          page: () => const PersonalInfoForPostAd()),

      GetPage(
        name: AppPage.loginScreen,
        page: () => LoginScreen(),
      ),
      GetPage(
        name: AppPage.signUpScreen,
        page: () => const SignUpScreen(),
      ),

      GetPage(
        name: AppPage.needHelpCardDetailScreen,
        page: () => NeedHelpCardDetailScreen(),
      ),
      GetPage(
        name: AppPage.listYourBusiness,
        page: () => ListYourBusiness(),
      ),
      GetPage(
        name: AppPage.contactUsScreen,
        page: () => ContactUsScreen(),
      ),

      GetPage(name: AppPage.canHelpCardScreen, page: () => CanHelpScreen()),

      GetPage(
        name: AppPage.canHelpCardDetailScreen,
        page: () => CanHelpCardDetail(),
      ),
      GetPage(
        name: AppPage.profileOverViewPersonal,
        page: () => ProfileOverViewPersonal(),
      ),
      GetPage(
        name: AppPage.myTask,
        page: () => MyTaskScreen(),
      ),
      GetPage(
        name: AppPage.myTaskToStart,
        page: () => MyTaskToStart(),
      ),
      GetPage(
        name: AppPage.myTaskInProgress,
        page: () => MyTaskInProgress(),
      ),
      GetPage(
        name: AppPage.myTaskCompleted,
        page: () => MyTaskCompleted(),
      ),
      GetPage(
        name: AppPage.myAds,
        page: () => MyAdsScreen(),
      ),
      GetPage(
        name: AppPage.mySkills,
        page: () => MySkillsScreen(),
      ),
      GetPage(
        name: AppPage.myQuotes,
        page: () => MyQuotesScreen(),
      ),
      GetPage(
        name: AppPage.myAnalytics,
        page: () => AnalyticsScreen(),
      ),
      GetPage(
        name: AppPage.myMessage,
        page: () => MessageScreen(),
      ),
      GetPage(
        name: AppPage.myMessageTyping,
        page: () => MessageTypingScreen(),
      ),
      GetPage(
        name: AppPage.myReviews,
        page: () => ReviewScreen(),
      ),
      GetPage(
        name: AppPage.myFavourites,
        page: () => const FavouriteScreen(),
      ),
      GetPage(
        name: AppPage.mySettings,
        page: () => const SettingScreen(),
      ),
      GetPage(
        name: AppPage.myProfileSetting,
        page: () => MyProfileSetting(),
      ),
      GetPage(
        name: AppPage.alertNotificationSetting,
        page: () => const AlertNotificationSetting(),
      ),
      GetPage(
        name: AppPage.securityPasswordSetting,
        page: () => SecurityPasswordSetting(),
      ),

      GetPage(
        name: AppPage.socialMediaSetting,
        page: () => SocialMediaScreen(),
      ),
      GetPage(
        name: AppPage.freePlanSettings,
        page: () => const FreePlanScreen(),
      ),

      GetPage(
        name: AppPage.paymentMethodSettings,
        page: () => const PaymentMethodScreen(),
      ),

      GetPage(
        name: AppPage.adScreenNeedHelpPersonal,
        page: () => AdScreenNeedHelpPerssonal(),
      ),

      ///  --- BUSINESS ACCOUNT ROUTES  --- ///

      GetPage(
        name: AppPage.landingScreenBusiness,
        page: () => CustomAppBarBusinessScreen(),
      ),

      GetPage(
        name: AppPage.canHelpScreenBusiness,
        page: () => CanHelpBusinessScreen(),
      ),

      GetPage(
        name: AppPage.canHelpCardBusinessDetailScreen,
        page: () => CanHelpCardBusinessDetail(),
      ),
      GetPage(
        name: AppPage.needHelpScreenBusiness,
        page: () => NeedHelpBusinessScreen(),
      ),

      GetPage(
          name: AppPage.canHelpMessageScreenBusiness,
          page: () => CanHelpMessagingScreenBusiness()),
      GetPage(
          name: AppPage.needHelpMessageScreenBusiness,
          page: () => NeedHelpMessagingScreenBusiness()),

      GetPage(
        name: AppPage.needHelpScreenBusinessWithOutBottomNav,
        page: () => NeedHelpScreenBusinessWithOutBottomNav(),
      ),
      GetPage(
        name: AppPage.canHelpScreenBusinessWithOutBottomNav,
        page: () => CanHelpScreenBusinessWithOutBottomNav(),
      ),
      GetPage(
        name: AppPage.needHelpCardBusinessDetailScreen,
        page: () => NeedHelpCardDetailBusinessScreen(),
      ),

      GetPage(
        name: AppPage.adScreenNeedHelpBusiness,
        page: () => AdScreenNeedHelpBusiness(),
      ),
      GetPage(
        name: AppPage.adScreenCanNeedHelpBusiness,
        page: () => AdScreenCanHelpBusiness(),
      ),

      GetPage(
        name: AppPage.profileOverViewBusiness,
        page: () => ProfileOverViewBusiness(),
      ),

      GetPage(
        name: AppPage.myTaskBusinessAccount,
        page: () => const MyTaskScreenBusiness(),
      ),

      GetPage(
        name: AppPage.myTaskToStartBusiness,
        page: () => MyTaskToStartBusiness(),
      ),
      GetPage(
        name: AppPage.myTaskInProgressBusiness,
        page: () => MyTaskInProgressBusiness(),
      ),
      GetPage(
        name: AppPage.myTaskCompletedBusiness,
        page: () => MyTaskCompletedBusiness(),
      ),

      GetPage(
        name: AppPage.myAdsBusinessScreen,
        page: () => MyAdsScreenBusiness(),
      ),
      GetPage(
        name: AppPage.mySkillsBusiness,
        page: () => MySkillsScreenBusiness(),
      ),
      GetPage(
        name: AppPage.myQuotesBusiness,
        page: () => MyQuotesScreenBusiness(),
      ),
      GetPage(
        name: AppPage.myAnalyticsBusiness,
        page: () => AnalyticsScreenBusiness(),
      ),
      GetPage(
        name: AppPage.myMessageBusiness,
        page: () => MessageScreenBusiness(),
      ),
      GetPage(
        name: AppPage.myMessageBusinessTyping,
        page: () => MessageTypingScreenBusiness(),
      ),
      GetPage(
        name: AppPage.myReviewsBusiness,
        page: () => ReviewScreenBusiness(),
      ),
      GetPage(
        name: AppPage.myFavouritesBusiness,
        page: () => const FavouriteScreenBusiness(),
      ),
      GetPage(
        name: AppPage.mySettingsBusiness,
        page: () => const SettingScreenBusiness(),
      ),
      GetPage(
        name: AppPage.companyDetailsSettingBusiness,
        page: () => const CompanyDetailsScreenBusiness(),
      ),
      GetPage(
        name: AppPage.aboutUsBusiness,
        page: () => const AboutUsBusiness(),
      ),
      GetPage(
        name: AppPage.serviceBusiness,
        page: () => ServiceBusiness(),
      ),
      GetPage(
        name: AppPage.receiveLeadingSettingBusiness,
        page: () => ReceiveLeadingBusiness(),
      ),
      GetPage(
        name: AppPage.companyLifeBusiness,
        page: () => const CompanyLifeBusiness(),
      ),
      GetPage(
        name: AppPage.alertNotificationBusiness,
        page: () => const AlertNotificationSettingBusiness(),
      ),
      GetPage(
        name: AppPage.securityPasswordSettingBusiness,
        page: () => SecurityPasswordSettingBusiness(),
      ),
      GetPage(
        name: AppPage.socialMediaSettingBusiness,
        page: () => const SocialMediaScreenBusiness(),
      ),
      GetPage(
        name: AppPage.freePlanSettingsBusiness,
        page: () => const FreePlanScreenBusiness(),
      ),
      GetPage(
        name: AppPage.paymentMethodSettingsBusiness,
        page: () => PaymentMethodScreenBusiness(),
      ),

      GetPage(
        name: AppPage.getQuotesScreen,
        page: () => GetQuotesScreen(),
      ),
      GetPage(
        name: AppPage.extendQuotesScreen,
        page: () => ExtendQuotesScreen(),
      ),
    ];
  }

  static to(String route, {Map<String, dynamic>? arguments}) =>
      Get.toNamed(route, arguments: arguments);

  static offAllTo(String route, {Map<String, dynamic>? arguments}) =>
      Get.offAllNamed(route, arguments: arguments);

  static offTo(String route, {Map<String, dynamic>? arguments}) =>
      Get.offNamed(route, arguments: arguments);

  static back() => Get.back();
}
