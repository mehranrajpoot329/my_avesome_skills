class AppPage {
  //  --- PERSONAL ACCOUNT LINKS  --- //
  static const String splashScreen = '/splashScreen';
  static const String canHelpScreen = '/canHelpScreen';
  static const String canHelpMessageScreen = '/canHelpMessageScreen';
  static const String canHelpScreenWithOutBottomNav =
      '/canHelpScreenWithOutBottomNav';
  static const String needHelpScreen = '/needHelpScreen';
  static const String needHelpScreenWithOutBottomNav =
      '/needHelpScreenWithOutBottomNav';
  static const String needHelpMessageScreen = '/needHelpMessageScreen';
  static const String adScreenNeedHelpPersonal = '/adScreenNeedHelp';
  static const String adScreenCanNeedHelpPersonal = '/adScreenCanNeedHelp';
  static const String adScreenCanNeedHelpPersonalWithOutBottomNav =
      '/adScreenCanNeedHelpWithOutBottomNav';
  static const String getQuotesScreen = '/GetQuotesScreen';
  static const String extendQuotesScreen = '/ExtendQuotesScreen';
  static const String personalInfoForPostAd = '/personalInfoForPostAd';
  static const String landingScreen = '/landingScreen';

  ///      Menu-Drawer-Pages  ///
  static const String contactUsScreen = '/contactUsScreen';

  static const String loginScreen = '/loginScreen';
  static const String signUpScreen = '/signUpScreen';
  static const String needHelpCardDetailScreen = '/needHelpCardDetailScreen';
  static const String listYourBusiness = '/listYourBusiness';
  static const String canHelpCardScreen = '/canHelpCardScreen';
  static const String canHelpCardDetailScreen = '/canHelpCardDetailScreen';
  static const String profileOverViewPersonal = '/profileOverViewPersonal';
  static const String myTask = '/myTaskScreen';
  static const String myTaskToStart = '/myTaskToStart';
  static const String myTaskInProgress = '/myTaskInProgress';
  static const String myTaskCompleted = '/myTaskCompleted';
  static const String myAds = '/myAdsScreen';
  static const String mySkills = '/mySkillsScreen';
  static const String myQuotes = '/myQuotesScreen';
  static const String myAnalytics = '/myAnalyticsScreen';
  static const String myMessage = '/myMessageScreen';
  static const String myMessageTyping = '/myMessageTypingScreen';
  static const String myReviews = '/myReviewsScreen';
  static const String myFavourites = '/myFavouritesScreen';
  static const String mySettings = '/mySettingsScreen';
  static const String myProfileSetting = '/myProfileSetting';
  static const String alertNotificationSetting = '/AlertNotificationSetting';
  static const String securityPasswordSetting = '/securityPasswordSetting';
  static const String socialMediaSetting = '/socialMediaSetting';
  static const String freePlanSettings = '/freePlanSetting';
  static const String paymentMethodSettings = '/paymentMethodSetting';

  /// ------------ Business pages ------------ ///
  static const String landingScreenBusiness = '/landingScreenBusiness';
  static const String canHelpScreenBusiness = '/canHelpScreenBusiness';
  static const String needHelpScreenBusiness = '/needHelpScreenBusiness';
  static const String canHelpMessageScreenBusiness =
      '/canHelpMessageScreenBusiness';
  static const String needHelpMessageScreenBusiness =
      '/needHelpMessageScreenBusiness';
  static const String canHelpCardBusinessDetailScreen =
      '/canHelpCardBusinessDetailScreen';
  static const String needHelpCardBusinessDetailScreen =
      '/needHelpCardBusinessDetailScreen';

  static const String needHelpScreenBusinessWithOutBottomNav =
      '/needHelpScreenBusinessWithOutBottomNav';

  static const String canHelpScreenBusinessWithOutBottomNav =
      '/canHelpScreenBusinessWithOutBottomNav';

  static const String adScreenNeedHelpBusiness =
      '/adScreenNeedHelpBusinessScreen';
  static const String adScreenCanNeedHelpBusiness =
      '/adScreenCanNeedHelpBusinessScreen';

  static const String myTaskBusinessAccount = '/myTaskBusinessScreen';

  static const String myTaskToStartBusiness = '/myTaskToStartBusiness';
  static const String myTaskInProgressBusiness = '/myTaskInProgressBusiness';
  static const String myTaskCompletedBusiness = '/myTaskCompletedBusiness';

  static const String mySkillsBusiness = '/mySkillsBusinessScreen';
  static const String myAdsBusinessScreen = '/myAdsBusinessScreen';
  static const String myQuotesBusiness = '/myQuotesBusinessScreen';
  static const String myAnalyticsBusiness = '/myAnalyticsBusinessScreen';
  static const String myMessageBusiness = '/myMessageBusinessScreen';
  static const String myMessageBusinessTyping =
      '/myMessageTypingBusinessScreen';
  static const String myReviewsBusiness = '/myReviewsBusinessScreen';
  static const String myFavouritesBusiness = '/myFavouritesBusinessScreen';
  static const String mySettingsBusiness = '/mySettingsBusinessScreen';
  static const String companyDetailsSettingBusiness =
      '/companyDetailsBusinessSetting';
  static const String aboutUsBusiness = '/aboutUsBusinessSetting';
  static const String serviceBusiness = '/serviceBusinessSetting';
  static const String companyLifeBusiness = '/companyLifeBusinessSetting';
  static const String alertNotificationBusiness =
      '/alertNotificationBusinessSetting';
  static const String securityPasswordSettingBusiness =
      '/securityPasswordBusinessSetting';
  static const String receiveLeadingSettingBusiness =
      '/receiveLeadingBusinessSetting';
  static const String socialMediaSettingBusiness =
      '/socialMediaBusinessSetting';
  static const String freePlanSettingsBusiness = '/freePlanBusinessSetting';
  static const String paymentMethodSettingsBusiness =
      '/paymentMethodBusinessSetting';
  static const String profileOverViewBusiness = '/profileOverViewBusiness';
}
