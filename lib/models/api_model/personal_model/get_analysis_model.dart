import 'dart:convert';

/// error : false
/// data : {"needHelpAllLikes":0,"canHelpAllLikes":0,"canHelpAllVisit":0,"needHelpAllVisits":20,"weeklyNewJob":1,"twoMonthNewJob":1,"monthNewJob":1,"yearNewJob":1,"weeklyVisits":20,"monthlyVisits":20,"twoMonthVisits":20,"yearVisit":20,"weeklyProgressJob":2,"monthlyProgressJob":2,"twoMonthProgressJob":2,"yearProgressJob":2,"weeklyCompletedJob":0,"monthlyCompletedJob":0,"twoMonthCompletedJob":0,"YearCompletedJob":0,"weeklyReview":0,"monthlyReview":0,"twoMonthReview":0,"yearReview":0,"likePercentage":0,"visitPercentage":100}
/// status : 200

GetAnalysisModel getAnalysisModelFromJson(String str) =>
    GetAnalysisModel.fromJson(json.decode(str));
String getAnalysisModelToJson(GetAnalysisModel data) =>
    json.encode(data.toJson());

class GetAnalysisModel {
  GetAnalysisModel({
    bool? error,
    Data? data,
    num? status,
  }) {
    _error = error;
    _data = data;
    _status = status;
  }

  GetAnalysisModel.fromJson(dynamic json) {
    _error = json['error'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
    _status = json['status'];
  }
  bool? _error;
  Data? _data;
  num? _status;
  GetAnalysisModel copyWith({
    bool? error,
    Data? data,
    num? status,
  }) =>
      GetAnalysisModel(
        error: error ?? _error,
        data: data ?? _data,
        status: status ?? _status,
      );
  bool? get error => _error;
  Data? get data => _data;
  num? get status => _status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['error'] = _error;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    map['status'] = _status;
    return map;
  }
}

/// needHelpAllLikes : 0
/// canHelpAllLikes : 0
/// canHelpAllVisit : 0
/// needHelpAllVisits : 20
/// weeklyNewJob : 1
/// twoMonthNewJob : 1
/// monthNewJob : 1
/// yearNewJob : 1
/// weeklyVisits : 20
/// monthlyVisits : 20
/// twoMonthVisits : 20
/// yearVisit : 20
/// weeklyProgressJob : 2
/// monthlyProgressJob : 2
/// twoMonthProgressJob : 2
/// yearProgressJob : 2
/// weeklyCompletedJob : 0
/// monthlyCompletedJob : 0
/// twoMonthCompletedJob : 0
/// YearCompletedJob : 0
/// weeklyReview : 0
/// monthlyReview : 0
/// twoMonthReview : 0
/// yearReview : 0
/// likePercentage : 0
/// visitPercentage : 100

Data dataFromJson(String str) => Data.fromJson(json.decode(str));
String dataToJson(Data data) => json.encode(data.toJson());

class Data {
  Data({
    num? needHelpAllLikes,
    num? canHelpAllLikes,
    num? canHelpAllVisit,
    num? needHelpAllVisits,
    num? weeklyNewJob,
    num? twoMonthNewJob,
    num? monthNewJob,
    num? yearNewJob,
    num? weeklyVisits,
    num? monthlyVisits,
    num? twoMonthVisits,
    num? yearVisit,
    num? weeklyProgressJob,
    num? monthlyProgressJob,
    num? twoMonthProgressJob,
    num? yearProgressJob,
    num? weeklyCompletedJob,
    num? monthlyCompletedJob,
    num? twoMonthCompletedJob,
    num? yearCompletedJob,
    num? weeklyReview,
    num? monthlyReview,
    num? twoMonthReview,
    num? yearReview,
    num? likePercentage,
    num? visitPercentage,
  }) {
    _needHelpAllLikes = needHelpAllLikes;
    _canHelpAllLikes = canHelpAllLikes;
    _canHelpAllVisit = canHelpAllVisit;
    _needHelpAllVisits = needHelpAllVisits;
    _weeklyNewJob = weeklyNewJob;
    _twoMonthNewJob = twoMonthNewJob;
    _monthNewJob = monthNewJob;
    _yearNewJob = yearNewJob;
    _weeklyVisits = weeklyVisits;
    _monthlyVisits = monthlyVisits;
    _twoMonthVisits = twoMonthVisits;
    _yearVisit = yearVisit;
    _weeklyProgressJob = weeklyProgressJob;
    _monthlyProgressJob = monthlyProgressJob;
    _twoMonthProgressJob = twoMonthProgressJob;
    _yearProgressJob = yearProgressJob;
    _weeklyCompletedJob = weeklyCompletedJob;
    _monthlyCompletedJob = monthlyCompletedJob;
    _twoMonthCompletedJob = twoMonthCompletedJob;
    _yearCompletedJob = yearCompletedJob;
    _weeklyReview = weeklyReview;
    _monthlyReview = monthlyReview;
    _twoMonthReview = twoMonthReview;
    _yearReview = yearReview;
    _likePercentage = likePercentage;
    _visitPercentage = visitPercentage;
  }

  Data.fromJson(dynamic json) {
    _needHelpAllLikes = json['needHelpAllLikes'];
    _canHelpAllLikes = json['canHelpAllLikes'];
    _canHelpAllVisit = json['canHelpAllVisit'];
    _needHelpAllVisits = json['needHelpAllVisits'];
    _weeklyNewJob = json['weeklyNewJob'];
    _twoMonthNewJob = json['twoMonthNewJob'];
    _monthNewJob = json['monthNewJob'];
    _yearNewJob = json['yearNewJob'];
    _weeklyVisits = json['weeklyVisits'];
    _monthlyVisits = json['monthlyVisits'];
    _twoMonthVisits = json['twoMonthVisits'];
    _yearVisit = json['yearVisit'];
    _weeklyProgressJob = json['weeklyProgressJob'];
    _monthlyProgressJob = json['monthlyProgressJob'];
    _twoMonthProgressJob = json['twoMonthProgressJob'];
    _yearProgressJob = json['yearProgressJob'];
    _weeklyCompletedJob = json['weeklyCompletedJob'];
    _monthlyCompletedJob = json['monthlyCompletedJob'];
    _twoMonthCompletedJob = json['twoMonthCompletedJob'];
    _yearCompletedJob = json['YearCompletedJob'];
    _weeklyReview = json['weeklyReview'];
    _monthlyReview = json['monthlyReview'];
    _twoMonthReview = json['twoMonthReview'];
    _yearReview = json['yearReview'];
    _likePercentage = json['likePercentage'];
    _visitPercentage = json['visitPercentage'];
  }
  num? _needHelpAllLikes;
  num? _canHelpAllLikes;
  num? _canHelpAllVisit;
  num? _needHelpAllVisits;
  num? _weeklyNewJob;
  num? _twoMonthNewJob;
  num? _monthNewJob;
  num? _yearNewJob;
  num? _weeklyVisits;
  num? _monthlyVisits;
  num? _twoMonthVisits;
  num? _yearVisit;
  num? _weeklyProgressJob;
  num? _monthlyProgressJob;
  num? _twoMonthProgressJob;
  num? _yearProgressJob;
  num? _weeklyCompletedJob;
  num? _monthlyCompletedJob;
  num? _twoMonthCompletedJob;
  num? _yearCompletedJob;
  num? _weeklyReview;
  num? _monthlyReview;
  num? _twoMonthReview;
  num? _yearReview;
  num? _likePercentage;
  num? _visitPercentage;
  Data copyWith({
    num? needHelpAllLikes,
    num? canHelpAllLikes,
    num? canHelpAllVisit,
    num? needHelpAllVisits,
    num? weeklyNewJob,
    num? twoMonthNewJob,
    num? monthNewJob,
    num? yearNewJob,
    num? weeklyVisits,
    num? monthlyVisits,
    num? twoMonthVisits,
    num? yearVisit,
    num? weeklyProgressJob,
    num? monthlyProgressJob,
    num? twoMonthProgressJob,
    num? yearProgressJob,
    num? weeklyCompletedJob,
    num? monthlyCompletedJob,
    num? twoMonthCompletedJob,
    num? yearCompletedJob,
    num? weeklyReview,
    num? monthlyReview,
    num? twoMonthReview,
    num? yearReview,
    num? likePercentage,
    num? visitPercentage,
  }) =>
      Data(
        needHelpAllLikes: needHelpAllLikes ?? _needHelpAllLikes,
        canHelpAllLikes: canHelpAllLikes ?? _canHelpAllLikes,
        canHelpAllVisit: canHelpAllVisit ?? _canHelpAllVisit,
        needHelpAllVisits: needHelpAllVisits ?? _needHelpAllVisits,
        weeklyNewJob: weeklyNewJob ?? _weeklyNewJob,
        twoMonthNewJob: twoMonthNewJob ?? _twoMonthNewJob,
        monthNewJob: monthNewJob ?? _monthNewJob,
        yearNewJob: yearNewJob ?? _yearNewJob,
        weeklyVisits: weeklyVisits ?? _weeklyVisits,
        monthlyVisits: monthlyVisits ?? _monthlyVisits,
        twoMonthVisits: twoMonthVisits ?? _twoMonthVisits,
        yearVisit: yearVisit ?? _yearVisit,
        weeklyProgressJob: weeklyProgressJob ?? _weeklyProgressJob,
        monthlyProgressJob: monthlyProgressJob ?? _monthlyProgressJob,
        twoMonthProgressJob: twoMonthProgressJob ?? _twoMonthProgressJob,
        yearProgressJob: yearProgressJob ?? _yearProgressJob,
        weeklyCompletedJob: weeklyCompletedJob ?? _weeklyCompletedJob,
        monthlyCompletedJob: monthlyCompletedJob ?? _monthlyCompletedJob,
        twoMonthCompletedJob: twoMonthCompletedJob ?? _twoMonthCompletedJob,
        yearCompletedJob: yearCompletedJob ?? _yearCompletedJob,
        weeklyReview: weeklyReview ?? _weeklyReview,
        monthlyReview: monthlyReview ?? _monthlyReview,
        twoMonthReview: twoMonthReview ?? _twoMonthReview,
        yearReview: yearReview ?? _yearReview,
        likePercentage: likePercentage ?? _likePercentage,
        visitPercentage: visitPercentage ?? _visitPercentage,
      );
  num? get needHelpAllLikes => _needHelpAllLikes;
  num? get canHelpAllLikes => _canHelpAllLikes;
  num? get canHelpAllVisit => _canHelpAllVisit;
  num? get needHelpAllVisits => _needHelpAllVisits;
  num? get weeklyNewJob => _weeklyNewJob;
  num? get twoMonthNewJob => _twoMonthNewJob;
  num? get monthNewJob => _monthNewJob;
  num? get yearNewJob => _yearNewJob;
  num? get weeklyVisits => _weeklyVisits;
  num? get monthlyVisits => _monthlyVisits;
  num? get twoMonthVisits => _twoMonthVisits;
  num? get yearVisit => _yearVisit;
  num? get weeklyProgressJob => _weeklyProgressJob;
  num? get monthlyProgressJob => _monthlyProgressJob;
  num? get twoMonthProgressJob => _twoMonthProgressJob;
  num? get yearProgressJob => _yearProgressJob;
  num? get weeklyCompletedJob => _weeklyCompletedJob;
  num? get monthlyCompletedJob => _monthlyCompletedJob;
  num? get twoMonthCompletedJob => _twoMonthCompletedJob;
  num? get yearCompletedJob => _yearCompletedJob;
  num? get weeklyReview => _weeklyReview;
  num? get monthlyReview => _monthlyReview;
  num? get twoMonthReview => _twoMonthReview;
  num? get yearReview => _yearReview;
  num? get likePercentage => _likePercentage;
  num? get visitPercentage => _visitPercentage;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['needHelpAllLikes'] = _needHelpAllLikes;
    map['canHelpAllLikes'] = _canHelpAllLikes;
    map['canHelpAllVisit'] = _canHelpAllVisit;
    map['needHelpAllVisits'] = _needHelpAllVisits;
    map['weeklyNewJob'] = _weeklyNewJob;
    map['twoMonthNewJob'] = _twoMonthNewJob;
    map['monthNewJob'] = _monthNewJob;
    map['yearNewJob'] = _yearNewJob;
    map['weeklyVisits'] = _weeklyVisits;
    map['monthlyVisits'] = _monthlyVisits;
    map['twoMonthVisits'] = _twoMonthVisits;
    map['yearVisit'] = _yearVisit;
    map['weeklyProgressJob'] = _weeklyProgressJob;
    map['monthlyProgressJob'] = _monthlyProgressJob;
    map['twoMonthProgressJob'] = _twoMonthProgressJob;
    map['yearProgressJob'] = _yearProgressJob;
    map['weeklyCompletedJob'] = _weeklyCompletedJob;
    map['monthlyCompletedJob'] = _monthlyCompletedJob;
    map['twoMonthCompletedJob'] = _twoMonthCompletedJob;
    map['YearCompletedJob'] = _yearCompletedJob;
    map['weeklyReview'] = _weeklyReview;
    map['monthlyReview'] = _monthlyReview;
    map['twoMonthReview'] = _twoMonthReview;
    map['yearReview'] = _yearReview;
    map['likePercentage'] = _likePercentage;
    map['visitPercentage'] = _visitPercentage;
    return map;
  }
}
