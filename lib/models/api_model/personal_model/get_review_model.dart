import 'dart:convert';

/// error : false
/// data : [{"id":1,"rating":"3","comments":"hey there","honesty":"33.333","work_quality":"33.333","technical_skill":"33.333","punctuality":"33.333","communication_skill":"33.333","job":"2","createdAt":"2022-11-16T12:44:51.000Z","updatedAt":"2022-11-16T12:44:51.000Z","userId":17}]
/// status : 200

GetReviewModel getReviewModelFromJson(String str) =>
    GetReviewModel.fromJson(json.decode(str));
String getReviewModelToJson(GetReviewModel data) => json.encode(data.toJson());

class GetReviewModel {
  GetReviewModel({
    bool? error,
    List<Data>? data,
    num? status,
  }) {
    _error = error;
    _data = data;
    _status = status;
  }

  GetReviewModel.fromJson(dynamic json) {
    _error = json['error'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data?.add(Data.fromJson(v));
      });
    }
    _status = json['status'];
  }
  bool? _error;
  List<Data>? _data;
  num? _status;
  GetReviewModel copyWith({
    bool? error,
    List<Data>? data,
    num? status,
  }) =>
      GetReviewModel(
        error: error ?? _error,
        data: data ?? _data,
        status: status ?? _status,
      );
  bool? get error => _error;
  List<Data>? get data => _data;
  num? get status => _status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['error'] = _error;
    if (_data != null) {
      map['data'] = _data?.map((v) => v.toJson()).toList();
    }
    map['status'] = _status;
    return map;
  }
}

/// id : 1
/// rating : "3"
/// comments : "hey there"
/// honesty : "33.333"
/// work_quality : "33.333"
/// technical_skill : "33.333"
/// punctuality : "33.333"
/// communication_skill : "33.333"
/// job : "2"
/// createdAt : "2022-11-16T12:44:51.000Z"
/// updatedAt : "2022-11-16T12:44:51.000Z"
/// userId : 17

Data dataFromJson(String str) => Data.fromJson(json.decode(str));
String dataToJson(Data data) => json.encode(data.toJson());

class Data {
  Data({
    num? id,
    String? rating,
    String? comments,
    String? honesty,
    String? workQuality,
    String? technicalSkill,
    String? punctuality,
    String? communicationSkill,
    String? job,
    String? createdAt,
    String? updatedAt,
    num? userId,
  }) {
    _id = id;
    _rating = rating;
    _comments = comments;
    _honesty = honesty;
    _workQuality = workQuality;
    _technicalSkill = technicalSkill;
    _punctuality = punctuality;
    _communicationSkill = communicationSkill;
    _job = job;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _userId = userId;
  }

  Data.fromJson(dynamic json) {
    _id = json['id'];
    _rating = json['rating'];
    _comments = json['comments'];
    _honesty = json['honesty'];
    _workQuality = json['work_quality'];
    _technicalSkill = json['technical_skill'];
    _punctuality = json['punctuality'];
    _communicationSkill = json['communication_skill'];
    _job = json['job'];
    _createdAt = json['createdAt'];
    _updatedAt = json['updatedAt'];
    _userId = json['userId'];
  }
  num? _id;
  String? _rating;
  String? _comments;
  String? _honesty;
  String? _workQuality;
  String? _technicalSkill;
  String? _punctuality;
  String? _communicationSkill;
  String? _job;
  String? _createdAt;
  String? _updatedAt;
  num? _userId;
  Data copyWith({
    num? id,
    String? rating,
    String? comments,
    String? honesty,
    String? workQuality,
    String? technicalSkill,
    String? punctuality,
    String? communicationSkill,
    String? job,
    String? createdAt,
    String? updatedAt,
    num? userId,
  }) =>
      Data(
        id: id ?? _id,
        rating: rating ?? _rating,
        comments: comments ?? _comments,
        honesty: honesty ?? _honesty,
        workQuality: workQuality ?? _workQuality,
        technicalSkill: technicalSkill ?? _technicalSkill,
        punctuality: punctuality ?? _punctuality,
        communicationSkill: communicationSkill ?? _communicationSkill,
        job: job ?? _job,
        createdAt: createdAt ?? _createdAt,
        updatedAt: updatedAt ?? _updatedAt,
        userId: userId ?? _userId,
      );
  num? get id => _id;
  String? get rating => _rating;
  String? get comments => _comments;
  String? get honesty => _honesty;
  String? get workQuality => _workQuality;
  String? get technicalSkill => _technicalSkill;
  String? get punctuality => _punctuality;
  String? get communicationSkill => _communicationSkill;
  String? get job => _job;
  String? get createdAt => _createdAt;
  String? get updatedAt => _updatedAt;
  num? get userId => _userId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['rating'] = _rating;
    map['comments'] = _comments;
    map['honesty'] = _honesty;
    map['work_quality'] = _workQuality;
    map['technical_skill'] = _technicalSkill;
    map['punctuality'] = _punctuality;
    map['communication_skill'] = _communicationSkill;
    map['job'] = _job;
    map['createdAt'] = _createdAt;
    map['updatedAt'] = _updatedAt;
    map['userId'] = _userId;
    return map;
  }
}
