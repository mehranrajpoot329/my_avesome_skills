import 'dart:convert';

/// error : false
/// data : {"count":1,"rows":[{"id":1,"project_name":"painting","project_id":"35","project_user":"e8jPcelZuMUi00SrflAL8vBKXHw1","stage":null,"status":null,"mark":null,"createdAt":"2022-12-14T14:17:00.000Z","updatedAt":"2022-12-14T14:17:00.000Z","userId":20}]}
/// status : 200

MyTaskModel myTaskModelFromJson(String str) =>
    MyTaskModel.fromJson(json.decode(str));
String myTaskModelToJson(MyTaskModel data) => json.encode(data.toJson());

class MyTaskModel {
  MyTaskModel({
    bool? error,
    Data? data,
    num? status,
  }) {
    _error = error;
    _data = data;
    _status = status;
  }

  MyTaskModel.fromJson(dynamic json) {
    _error = json['error'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
    _status = json['status'];
  }
  bool? _error;
  Data? _data;
  num? _status;
  MyTaskModel copyWith({
    bool? error,
    Data? data,
    num? status,
  }) =>
      MyTaskModel(
        error: error ?? _error,
        data: data ?? _data,
        status: status ?? _status,
      );
  bool? get error => _error;
  Data? get data => _data;
  num? get status => _status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['error'] = _error;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    map['status'] = _status;
    return map;
  }
}

/// count : 1
/// rows : [{"id":1,"project_name":"painting","project_id":"35","project_user":"e8jPcelZuMUi00SrflAL8vBKXHw1","stage":null,"status":null,"mark":null,"createdAt":"2022-12-14T14:17:00.000Z","updatedAt":"2022-12-14T14:17:00.000Z","userId":20}]

Data dataFromJson(String str) => Data.fromJson(json.decode(str));
String dataToJson(Data data) => json.encode(data.toJson());

class Data {
  Data({
    num? count,
    List<Rows>? rows,
  }) {
    _count = count;
    _rows = rows;
  }

  Data.fromJson(dynamic json) {
    _count = json['count'];
    if (json['rows'] != null) {
      _rows = [];
      json['rows'].forEach((v) {
        _rows?.add(Rows.fromJson(v));
      });
    }
  }
  num? _count;
  List<Rows>? _rows;
  Data copyWith({
    num? count,
    List<Rows>? rows,
  }) =>
      Data(
        count: count ?? _count,
        rows: rows ?? _rows,
      );
  num? get count => _count;
  List<Rows>? get rows => _rows;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['count'] = _count;
    if (_rows != null) {
      map['rows'] = _rows?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// id : 1
/// project_name : "painting"
/// project_id : "35"
/// project_user : "e8jPcelZuMUi00SrflAL8vBKXHw1"
/// stage : null
/// status : null
/// mark : null
/// createdAt : "2022-12-14T14:17:00.000Z"
/// updatedAt : "2022-12-14T14:17:00.000Z"
/// userId : 20

Rows rowsFromJson(String str) => Rows.fromJson(json.decode(str));
String rowsToJson(Rows data) => json.encode(data.toJson());

class Rows {
  Rows({
    num? id,
    String? projectName,
    String? projectId,
    String? projectUser,
    dynamic stage,
    dynamic status,
    dynamic mark,
    String? createdAt,
    String? updatedAt,
    num? userId,
  }) {
    _id = id;
    _projectName = projectName;
    _projectId = projectId;
    _projectUser = projectUser;
    _stage = stage;
    _status = status;
    _mark = mark;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _userId = userId;
  }

  Rows.fromJson(dynamic json) {
    _id = json['id'];
    _projectName = json['project_name'];
    _projectId = json['project_id'];
    _projectUser = json['project_user'];
    _stage = json['stage'];
    _status = json['status'];
    _mark = json['mark'];
    _createdAt = json['createdAt'];
    _updatedAt = json['updatedAt'];
    _userId = json['userId'];
  }
  num? _id;
  String? _projectName;
  String? _projectId;
  String? _projectUser;
  dynamic _stage;
  dynamic _status;
  dynamic _mark;
  String? _createdAt;
  String? _updatedAt;
  num? _userId;
  Rows copyWith({
    num? id,
    String? projectName,
    String? projectId,
    String? projectUser,
    dynamic stage,
    dynamic status,
    dynamic mark,
    String? createdAt,
    String? updatedAt,
    num? userId,
  }) =>
      Rows(
        id: id ?? _id,
        projectName: projectName ?? _projectName,
        projectId: projectId ?? _projectId,
        projectUser: projectUser ?? _projectUser,
        stage: stage ?? _stage,
        status: status ?? _status,
        mark: mark ?? _mark,
        createdAt: createdAt ?? _createdAt,
        updatedAt: updatedAt ?? _updatedAt,
        userId: userId ?? _userId,
      );
  num? get id => _id;
  String? get projectName => _projectName;
  String? get projectId => _projectId;
  String? get projectUser => _projectUser;
  dynamic get stage => _stage;
  dynamic get status => _status;
  dynamic get mark => _mark;
  String? get createdAt => _createdAt;
  String? get updatedAt => _updatedAt;
  num? get userId => _userId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['project_name'] = _projectName;
    map['project_id'] = _projectId;
    map['project_user'] = _projectUser;
    map['stage'] = _stage;
    map['status'] = _status;
    map['mark'] = _mark;
    map['createdAt'] = _createdAt;
    map['updatedAt'] = _updatedAt;
    map['userId'] = _userId;
    return map;
  }
}
