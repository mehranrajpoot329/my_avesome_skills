import 'dart:convert';

/// error : false
/// data : {"totalItems":2,"row":[{"id":85,"service_name":"COMPUTER IT","createdAt":"2022-10-14T12:25:52.000Z","updatedAt":"2022-10-14T12:25:52.000Z","IndustryId":1},{"id":86,"service_name":"WEB","createdAt":"2022-10-14T12:25:52.000Z","updatedAt":"2022-10-14T12:25:52.000Z","IndustryId":1}]}
/// status : 200

ServiceModel serviceModelFromJson(String str) =>
    ServiceModel.fromJson(json.decode(str));
String serviceModelToJson(ServiceModel data) => json.encode(data.toJson());

class ServiceModel {
  ServiceModel({
    bool? error,
    Data? data,
    num? status,
  }) {
    _error = error;
    _data = data;
    _status = status;
  }

  ServiceModel.fromJson(dynamic json) {
    _error = json['error'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
    _status = json['status'];
  }
  bool? _error;
  Data? _data;
  num? _status;
  ServiceModel copyWith({
    bool? error,
    Data? data,
    num? status,
  }) =>
      ServiceModel(
        error: error ?? _error,
        data: data ?? _data,
        status: status ?? _status,
      );
  bool? get error => _error;
  Data? get data => _data;
  num? get status => _status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['error'] = _error;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    map['status'] = _status;
    return map;
  }
}

/// totalItems : 2
/// row : [{"id":85,"service_name":"COMPUTER IT","createdAt":"2022-10-14T12:25:52.000Z","updatedAt":"2022-10-14T12:25:52.000Z","IndustryId":1},{"id":86,"service_name":"WEB","createdAt":"2022-10-14T12:25:52.000Z","updatedAt":"2022-10-14T12:25:52.000Z","IndustryId":1}]

Data dataFromJson(String str) => Data.fromJson(json.decode(str));
String dataToJson(Data data) => json.encode(data.toJson());

class Data {
  Data({
    num? totalItems,
    List<Row>? row,
  }) {
    _totalItems = totalItems;
    _row = row;
  }

  Data.fromJson(dynamic json) {
    _totalItems = json['totalItems'];
    if (json['row'] != null) {
      _row = [];
      json['row'].forEach((v) {
        _row?.add(Row.fromJson(v));
      });
    }
  }
  num? _totalItems;
  List<Row>? _row;
  Data copyWith({
    num? totalItems,
    List<Row>? row,
  }) =>
      Data(
        totalItems: totalItems ?? _totalItems,
        row: row ?? _row,
      );
  num? get totalItems => _totalItems;
  List<Row>? get row => _row;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['totalItems'] = _totalItems;
    if (_row != null) {
      map['row'] = _row?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// id : 85
/// service_name : "COMPUTER IT"
/// createdAt : "2022-10-14T12:25:52.000Z"
/// updatedAt : "2022-10-14T12:25:52.000Z"
/// IndustryId : 1

Row rowFromJson(String str) => Row.fromJson(json.decode(str));
String rowToJson(Row data) => json.encode(data.toJson());

class Row {
  Row({
    num? id,
    String? serviceName,
    String? createdAt,
    String? updatedAt,
    num? industryId,
  }) {
    _id = id;
    _serviceName = serviceName;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _industryId = industryId;
  }

  Row.fromJson(dynamic json) {
    _id = json['id'];
    _serviceName = json['service_name'];
    _createdAt = json['createdAt'];
    _updatedAt = json['updatedAt'];
    _industryId = json['IndustryId'];
  }
  num? _id;
  String? _serviceName;
  String? _createdAt;
  String? _updatedAt;
  num? _industryId;
  Row copyWith({
    num? id,
    String? serviceName,
    String? createdAt,
    String? updatedAt,
    num? industryId,
  }) =>
      Row(
        id: id ?? _id,
        serviceName: serviceName ?? _serviceName,
        createdAt: createdAt ?? _createdAt,
        updatedAt: updatedAt ?? _updatedAt,
        industryId: industryId ?? _industryId,
      );
  num? get id => _id;
  String? get serviceName => _serviceName;
  String? get createdAt => _createdAt;
  String? get updatedAt => _updatedAt;
  num? get industryId => _industryId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['service_name'] = _serviceName;
    map['createdAt'] = _createdAt;
    map['updatedAt'] = _updatedAt;
    map['IndustryId'] = _industryId;
    return map;
  }
}
