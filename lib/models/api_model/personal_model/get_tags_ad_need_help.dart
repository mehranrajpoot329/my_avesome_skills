import 'dart:convert';

/// error : false
/// data : {"result":[{"id":1,"title":"UI/UX","description":null,"createdAt":"2022-10-07T12:00:02.000Z","updatedAt":"2022-10-07T12:00:02.000Z"},{"id":2,"title":"Design","description":null,"createdAt":"2022-10-07T12:00:02.000Z","updatedAt":"2022-10-07T12:00:02.000Z"},{"id":3,"title":"Website","description":null,"createdAt":"2022-10-07T12:00:02.000Z","updatedAt":"2022-10-07T12:00:02.000Z"},{"id":4,"title":"App","description":null,"createdAt":"2022-10-07T12:00:02.000Z","updatedAt":"2022-10-07T12:00:02.000Z"},{"id":5,"title":"Mobile App","description":null,"createdAt":"2022-10-07T12:00:02.000Z","updatedAt":"2022-10-07T12:00:02.000Z"}]}
/// status : 200

GetTagsAdNeedHelp getTagsAdNeedHelpFromJson(String str) =>
    GetTagsAdNeedHelp.fromJson(json.decode(str));
String getTagsAdNeedHelpToJson(GetTagsAdNeedHelp data) =>
    json.encode(data.toJson());

class GetTagsAdNeedHelp {
  GetTagsAdNeedHelp({
    bool? error,
    Data? data,
    num? status,
  }) {
    _error = error;
    _data = data;
    _status = status;
  }

  GetTagsAdNeedHelp.fromJson(dynamic json) {
    _error = json['error'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
    _status = json['status'];
  }
  bool? _error;
  Data? _data;
  num? _status;
  GetTagsAdNeedHelp copyWith({
    bool? error,
    Data? data,
    num? status,
  }) =>
      GetTagsAdNeedHelp(
        error: error ?? _error,
        data: data ?? _data,
        status: status ?? _status,
      );
  bool? get error => _error;
  Data? get data => _data;
  num? get status => _status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['error'] = _error;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    map['status'] = _status;
    return map;
  }
}

/// result : [{"id":1,"title":"UI/UX","description":null,"createdAt":"2022-10-07T12:00:02.000Z","updatedAt":"2022-10-07T12:00:02.000Z"},{"id":2,"title":"Design","description":null,"createdAt":"2022-10-07T12:00:02.000Z","updatedAt":"2022-10-07T12:00:02.000Z"},{"id":3,"title":"Website","description":null,"createdAt":"2022-10-07T12:00:02.000Z","updatedAt":"2022-10-07T12:00:02.000Z"},{"id":4,"title":"App","description":null,"createdAt":"2022-10-07T12:00:02.000Z","updatedAt":"2022-10-07T12:00:02.000Z"},{"id":5,"title":"Mobile App","description":null,"createdAt":"2022-10-07T12:00:02.000Z","updatedAt":"2022-10-07T12:00:02.000Z"}]

Data dataFromJson(String str) => Data.fromJson(json.decode(str));
String dataToJson(Data data) => json.encode(data.toJson());

class Data {
  Data({
    List<Result>? result,
  }) {
    _result = result;
  }

  Data.fromJson(dynamic json) {
    if (json['result'] != null) {
      _result = [];
      json['result'].forEach((v) {
        _result?.add(Result.fromJson(v));
      });
    }
  }
  List<Result>? _result;
  Data copyWith({
    List<Result>? result,
  }) =>
      Data(
        result: result ?? _result,
      );
  List<Result>? get result => _result;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (_result != null) {
      map['result'] = _result?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// id : 1
/// title : "UI/UX"
/// description : null
/// createdAt : "2022-10-07T12:00:02.000Z"
/// updatedAt : "2022-10-07T12:00:02.000Z"

Result resultFromJson(String str) => Result.fromJson(json.decode(str));
String resultToJson(Result data) => json.encode(data.toJson());

class Result {
  Result({
    num? id,
    String? title,
    dynamic description,
    String? createdAt,
    String? updatedAt,
  }) {
    _id = id;
    _title = title;
    _description = description;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
  }

  Result.fromJson(dynamic json) {
    _id = json['id'];
    _title = json['title'];
    _description = json['description'];
    _createdAt = json['createdAt'];
    _updatedAt = json['updatedAt'];
  }
  num? _id;
  String? _title;
  dynamic _description;
  String? _createdAt;
  String? _updatedAt;
  Result copyWith({
    num? id,
    String? title,
    dynamic description,
    String? createdAt,
    String? updatedAt,
  }) =>
      Result(
        id: id ?? _id,
        title: title ?? _title,
        description: description ?? _description,
        createdAt: createdAt ?? _createdAt,
        updatedAt: updatedAt ?? _updatedAt,
      );
  num? get id => _id;
  String? get title => _title;
  dynamic get description => _description;
  String? get createdAt => _createdAt;
  String? get updatedAt => _updatedAt;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['title'] = _title;
    map['description'] = _description;
    map['createdAt'] = _createdAt;
    map['updatedAt'] = _updatedAt;
    return map;
  }
}
