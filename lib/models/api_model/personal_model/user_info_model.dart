import 'dart:convert';

/// error : false
/// data : {"id":17,"f_uid":"INiG4KZEKPRJY5b6vQzj3pqmZWk2","first_name":"","last_name":"","email":"admin1@admin.com","phoneNumber":null,"photoURL":"","bussinessURL":"","accountType":"1","trade_name":null,"createdAt":"2022-11-15T09:21:02.000Z","updatedAt":"2022-11-16T06:46:57.000Z","address":{"id":5,"streetAddress":"ksjdl","city":null,"state":"1","country":null,"suburb":null,"postal_code":"567890","email":"admin@admin.com","phone":"","home_phone":null,"website":null,"createdAt":"2022-11-16T06:40:44.000Z","updatedAt":"2022-11-16T06:46:57.000Z","userId":17},"aboutUser":null,"awards":null,"education":null,"certificates":[{"id":10,"certificates":"certificates","createdAt":"2022-11-21T09:31:26.000Z","updatedAt":"2022-11-21T09:31:26.000Z","userId":17}],"languages":[{"id":8,"languages":"english","createdAt":"2022-11-21T08:53:34.000Z","updatedAt":"2022-11-21T08:53:34.000Z","userId":17}],"skills":[{"id":8,"skill":"adfk;ajsdf","hour_rate":"23","createdAt":"2022-11-21T08:38:05.000Z","updatedAt":"2022-11-21T08:38:05.000Z","userId":17},{"id":9,"skill":"labour","hour_rate":"25/hr","createdAt":"2022-11-21T08:47:25.000Z","updatedAt":"2022-11-21T08:47:25.000Z","userId":17}],"projects":[{"id":10,"project_title":"","image_url":"public/images/projects/2022-11-21T09:49:49.003Z.png","project_link":"","createdAt":"2022-11-21T09:49:49.000Z","updatedAt":"2022-11-21T09:49:49.000Z","userId":17}]}
/// status : 200

UserInfoModel userInfoModelFromJson(String str) =>
    UserInfoModel.fromJson(json.decode(str));
String userInfoModelToJson(UserInfoModel data) => json.encode(data.toJson());

class UserInfoModel {
  UserInfoModel({
    bool? error,
    Data? data,
    num? status,
  }) {
    _error = error;
    _data = data;
    _status = status;
  }

  UserInfoModel.fromJson(dynamic json) {
    _error = json['error'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
    _status = json['status'];
  }
  bool? _error;
  Data? _data;
  num? _status;
  UserInfoModel copyWith({
    bool? error,
    Data? data,
    num? status,
  }) =>
      UserInfoModel(
        error: error ?? _error,
        data: data ?? _data,
        status: status ?? _status,
      );
  bool? get error => _error;
  Data? get data => _data;
  num? get status => _status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['error'] = _error;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    map['status'] = _status;
    return map;
  }
}

Data dataFromJson(String str) => Data.fromJson(json.decode(str));
String dataToJson(Data data) => json.encode(data.toJson());

class Data {
  Data({
    num? id,
    String? fUid,
    String? firstName,
    String? lastName,
    String? email,
    dynamic phoneNumber,
    String? photoURL,
    String? bussinessURL,
    String? designation,
    String? accountType,
    dynamic tradeName,
    String? createdAt,
    String? updatedAt,
    Address? address,
    dynamic aboutUser,
    dynamic awards,
    dynamic education,
    List<Certificates>? certificates,
    List<Languages>? languages,
    List<Skills>? skills,
    List<Projects>? projects,
  }) {
    _id = id;
    _fUid = fUid;
    _firstName = firstName;
    _lastName = lastName;
    _email = email;
    _phoneNumber = phoneNumber;
    _photoURL = photoURL;
    _bussinessURL = bussinessURL;
    _designation = designation;
    _accountType = accountType;
    _tradeName = tradeName;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _address = address;
    _aboutUser = aboutUser;
    _awards = awards;
    _education = education;
    _certificates = certificates;
    _languages = languages;
    _skills = skills;
    _projects = projects;
  }

  Data.fromJson(dynamic json) {
    _id = json['id'];
    _fUid = json['f_uid'];
    _firstName = json['first_name'];
    _lastName = json['last_name'];
    _email = json['email'];
    _phoneNumber = json['phoneNumber'];
    _photoURL = json['photoURL'];
    _bussinessURL = json['bussinessURL'];
    _designation = json['designation'];
    _accountType = json['accountType'];
    _tradeName = json['trade_name'];
    _createdAt = json['createdAt'];
    _updatedAt = json['updatedAt'];
    _address =
        json['address'] != null ? Address.fromJson(json['address']) : null;
    _aboutUser = json['aboutUser'];
    _awards = json['awards'];
    _education = json['education'];
    if (json['certificates'] != null) {
      _certificates = [];
      json['certificates'].forEach((v) {
        _certificates?.add(Certificates.fromJson(v));
      });
    }
    if (json['languages'] != null) {
      _languages = [];
      json['languages'].forEach((v) {
        _languages?.add(Languages.fromJson(v));
      });
    }
    if (json['skills'] != null) {
      _skills = [];
      json['skills'].forEach((v) {
        _skills?.add(Skills.fromJson(v));
      });
    }
    if (json['projects'] != null) {
      _projects = [];
      json['projects'].forEach((v) {
        _projects?.add(Projects.fromJson(v));
      });
    }
  }
  num? _id;
  String? _fUid;
  String? _firstName;
  String? _lastName;
  String? _email;
  dynamic _phoneNumber;
  String? _photoURL;
  String? _bussinessURL;
  String? _designation;
  String? _accountType;
  dynamic _tradeName;
  String? _createdAt;
  String? _updatedAt;
  Address? _address;
  dynamic _aboutUser;
  dynamic _awards;
  dynamic _education;
  List<Certificates>? _certificates;
  List<Languages>? _languages;
  List<Skills>? _skills;
  List<Projects>? _projects;
  Data copyWith({
    num? id,
    String? fUid,
    String? firstName,
    String? lastName,
    String? email,
    dynamic phoneNumber,
    String? photoURL,
    String? bussinessURL,
    String? designation,
    String? accountType,
    dynamic tradeName,
    String? createdAt,
    String? updatedAt,
    Address? address,
    dynamic aboutUser,
    dynamic awards,
    dynamic education,
    List<Certificates>? certificates,
    List<Languages>? languages,
    List<Skills>? skills,
    List<Projects>? projects,
  }) =>
      Data(
        id: id ?? _id,
        fUid: fUid ?? _fUid,
        firstName: firstName ?? _firstName,
        lastName: lastName ?? _lastName,
        email: email ?? _email,
        phoneNumber: phoneNumber ?? _phoneNumber,
        photoURL: photoURL ?? _photoURL,
        bussinessURL: bussinessURL ?? _bussinessURL,
        designation: designation ?? _designation,
        accountType: accountType ?? _accountType,
        tradeName: tradeName ?? _tradeName,
        createdAt: createdAt ?? _createdAt,
        updatedAt: updatedAt ?? _updatedAt,
        address: address ?? _address,
        aboutUser: aboutUser ?? _aboutUser,
        awards: awards ?? _awards,
        education: education ?? _education,
        certificates: certificates ?? _certificates,
        languages: languages ?? _languages,
        skills: skills ?? _skills,
        projects: projects ?? _projects,
      );
  num? get id => _id;
  String? get fUid => _fUid;
  String? get firstName => _firstName;
  String? get lastName => _lastName;
  String? get email => _email;
  dynamic get phoneNumber => _phoneNumber;
  String? get photoURL => _photoURL;
  String? get bussinessURL => _bussinessURL;
  String? get designation => _designation;
  String? get accountType => _accountType;
  dynamic get tradeName => _tradeName;
  String? get createdAt => _createdAt;
  String? get updatedAt => _updatedAt;
  Address? get address => _address;
  dynamic get aboutUser => _aboutUser;
  dynamic get awards => _awards;
  dynamic get education => _education;
  List<Certificates>? get certificates => _certificates;
  List<Languages>? get languages => _languages;
  List<Skills>? get skills => _skills;
  List<Projects>? get projects => _projects;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['f_uid'] = _fUid;
    map['first_name'] = _firstName;
    map['last_name'] = _lastName;
    map['email'] = _email;
    map['phoneNumber'] = _phoneNumber;
    map['photoURL'] = _photoURL;
    map['bussinessURL'] = _bussinessURL;
    map['designation'] = _designation;
    map['accountType'] = _accountType;
    map['trade_name'] = _tradeName;
    map['createdAt'] = _createdAt;
    map['updatedAt'] = _updatedAt;
    if (_address != null) {
      map['address'] = _address?.toJson();
    }
    map['aboutUser'] = _aboutUser;
    map['awards'] = _awards;
    map['education'] = _education;
    if (_certificates != null) {
      map['certificates'] = _certificates?.map((v) => v.toJson()).toList();
    }
    if (_languages != null) {
      map['languages'] = _languages?.map((v) => v.toJson()).toList();
    }
    if (_skills != null) {
      map['skills'] = _skills?.map((v) => v.toJson()).toList();
    }
    if (_projects != null) {
      map['projects'] = _projects?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// id : 10
/// project_title : ""
/// image_url : "public/images/projects/2022-11-21T09:49:49.003Z.png"
/// project_link : ""
/// createdAt : "2022-11-21T09:49:49.000Z"
/// updatedAt : "2022-11-21T09:49:49.000Z"
/// userId : 17

Projects projectsFromJson(String str) => Projects.fromJson(json.decode(str));
String projectsToJson(Projects data) => json.encode(data.toJson());

class Projects {
  Projects({
    num? id,
    String? projectTitle,
    String? imageUrl,
    String? projectLink,
    String? createdAt,
    String? updatedAt,
    num? userId,
  }) {
    _id = id;
    _projectTitle = projectTitle;
    _imageUrl = imageUrl;
    _projectLink = projectLink;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _userId = userId;
  }

  Projects.fromJson(dynamic json) {
    _id = json['id'];
    _projectTitle = json['project_title'];
    _imageUrl = json['image_url'];
    _projectLink = json['project_link'];
    _createdAt = json['createdAt'];
    _updatedAt = json['updatedAt'];
    _userId = json['userId'];
  }
  num? _id;
  String? _projectTitle;
  String? _imageUrl;
  String? _projectLink;
  String? _createdAt;
  String? _updatedAt;
  num? _userId;
  Projects copyWith({
    num? id,
    String? projectTitle,
    String? imageUrl,
    String? projectLink,
    String? createdAt,
    String? updatedAt,
    num? userId,
  }) =>
      Projects(
        id: id ?? _id,
        projectTitle: projectTitle ?? _projectTitle,
        imageUrl: imageUrl ?? _imageUrl,
        projectLink: projectLink ?? _projectLink,
        createdAt: createdAt ?? _createdAt,
        updatedAt: updatedAt ?? _updatedAt,
        userId: userId ?? _userId,
      );
  num? get id => _id;
  String? get projectTitle => _projectTitle;
  String? get imageUrl => _imageUrl;
  String? get projectLink => _projectLink;
  String? get createdAt => _createdAt;
  String? get updatedAt => _updatedAt;
  num? get userId => _userId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['project_title'] = _projectTitle;
    map['image_url'] = _imageUrl;
    map['project_link'] = _projectLink;
    map['createdAt'] = _createdAt;
    map['updatedAt'] = _updatedAt;
    map['userId'] = _userId;
    return map;
  }
}

/// id : 8
/// skill : "adfk;ajsdf"
/// hour_rate : "23"
/// createdAt : "2022-11-21T08:38:05.000Z"
/// updatedAt : "2022-11-21T08:38:05.000Z"
/// userId : 17

Skills skillsFromJson(String str) => Skills.fromJson(json.decode(str));
String skillsToJson(Skills data) => json.encode(data.toJson());

class Skills {
  Skills({
    num? id,
    String? skill,
    String? hourRate,
    String? createdAt,
    String? updatedAt,
    num? userId,
  }) {
    _id = id;
    _skill = skill;
    _hourRate = hourRate;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _userId = userId;
  }

  Skills.fromJson(dynamic json) {
    _id = json['id'];
    _skill = json['skill'];
    _hourRate = json['hour_rate'];
    _createdAt = json['createdAt'];
    _updatedAt = json['updatedAt'];
    _userId = json['userId'];
  }
  num? _id;
  String? _skill;
  String? _hourRate;
  String? _createdAt;
  String? _updatedAt;
  num? _userId;
  Skills copyWith({
    num? id,
    String? skill,
    String? hourRate,
    String? createdAt,
    String? updatedAt,
    num? userId,
  }) =>
      Skills(
        id: id ?? _id,
        skill: skill ?? _skill,
        hourRate: hourRate ?? _hourRate,
        createdAt: createdAt ?? _createdAt,
        updatedAt: updatedAt ?? _updatedAt,
        userId: userId ?? _userId,
      );
  num? get id => _id;
  String? get skill => _skill;
  String? get hourRate => _hourRate;
  String? get createdAt => _createdAt;
  String? get updatedAt => _updatedAt;
  num? get userId => _userId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['skill'] = _skill;
    map['hour_rate'] = _hourRate;
    map['createdAt'] = _createdAt;
    map['updatedAt'] = _updatedAt;
    map['userId'] = _userId;
    return map;
  }
}

/// id : 8
/// languages : "english"
/// createdAt : "2022-11-21T08:53:34.000Z"
/// updatedAt : "2022-11-21T08:53:34.000Z"
/// userId : 17

Languages languagesFromJson(String str) => Languages.fromJson(json.decode(str));
String languagesToJson(Languages data) => json.encode(data.toJson());

class Languages {
  Languages({
    num? id,
    String? languages,
    String? createdAt,
    String? updatedAt,
    num? userId,
  }) {
    _id = id;
    _languages = languages;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _userId = userId;
  }

  Languages.fromJson(dynamic json) {
    _id = json['id'];
    _languages = json['languages'];
    _createdAt = json['createdAt'];
    _updatedAt = json['updatedAt'];
    _userId = json['userId'];
  }
  num? _id;
  String? _languages;
  String? _createdAt;
  String? _updatedAt;
  num? _userId;
  Languages copyWith({
    num? id,
    String? languages,
    String? createdAt,
    String? updatedAt,
    num? userId,
  }) =>
      Languages(
        id: id ?? _id,
        languages: languages ?? _languages,
        createdAt: createdAt ?? _createdAt,
        updatedAt: updatedAt ?? _updatedAt,
        userId: userId ?? _userId,
      );
  num? get id => _id;
  String? get languages => _languages;
  String? get createdAt => _createdAt;
  String? get updatedAt => _updatedAt;
  num? get userId => _userId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['languages'] = _languages;
    map['createdAt'] = _createdAt;
    map['updatedAt'] = _updatedAt;
    map['userId'] = _userId;
    return map;
  }
}

/// id : 10
/// certificates : "certificates"
/// createdAt : "2022-11-21T09:31:26.000Z"
/// updatedAt : "2022-11-21T09:31:26.000Z"
/// userId : 17

Certificates certificatesFromJson(String str) =>
    Certificates.fromJson(json.decode(str));
String certificatesToJson(Certificates data) => json.encode(data.toJson());

class Certificates {
  Certificates({
    num? id,
    String? certificates,
    String? createdAt,
    String? updatedAt,
    num? userId,
  }) {
    _id = id;
    _certificates = certificates;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _userId = userId;
  }

  Certificates.fromJson(dynamic json) {
    _id = json['id'];
    _certificates = json['certificates'];
    _createdAt = json['createdAt'];
    _updatedAt = json['updatedAt'];
    _userId = json['userId'];
  }
  num? _id;
  String? _certificates;
  String? _createdAt;
  String? _updatedAt;
  num? _userId;
  Certificates copyWith({
    num? id,
    String? certificates,
    String? createdAt,
    String? updatedAt,
    num? userId,
  }) =>
      Certificates(
        id: id ?? _id,
        certificates: certificates ?? _certificates,
        createdAt: createdAt ?? _createdAt,
        updatedAt: updatedAt ?? _updatedAt,
        userId: userId ?? _userId,
      );
  num? get id => _id;
  String? get certificates => _certificates;
  String? get createdAt => _createdAt;
  String? get updatedAt => _updatedAt;
  num? get userId => _userId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['certificates'] = _certificates;
    map['createdAt'] = _createdAt;
    map['updatedAt'] = _updatedAt;
    map['userId'] = _userId;
    return map;
  }
}

/// id : 5
/// streetAddress : "ksjdl"
/// city : null
/// state : "1"
/// country : null
/// suburb : null
/// postal_code : "567890"
/// email : "admin@admin.com"
/// phone : ""
/// home_phone : null
/// website : null
/// createdAt : "2022-11-16T06:40:44.000Z"
/// updatedAt : "2022-11-16T06:46:57.000Z"
/// userId : 17

Address addressFromJson(String str) => Address.fromJson(json.decode(str));
String addressToJson(Address data) => json.encode(data.toJson());

class Address {
  Address({
    num? id,
    String? streetAddress,
    dynamic city,
    String? state,
    dynamic country,
    dynamic suburb,
    String? postalCode,
    String? email,
    String? phone,
    dynamic homePhone,
    dynamic website,
    String? createdAt,
    String? updatedAt,
    num? userId,
  }) {
    _id = id;
    _streetAddress = streetAddress;
    _city = city;
    _state = state;
    _country = country;
    _suburb = suburb;
    _postalCode = postalCode;
    _email = email;
    _phone = phone;
    _homePhone = homePhone;
    _website = website;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _userId = userId;
  }

  Address.fromJson(dynamic json) {
    _id = json['id'];
    _streetAddress = json['streetAddress'];
    _city = json['city'];
    _state = json['state'];
    _country = json['country'];
    _suburb = json['suburb'];
    _postalCode = json['postal_code'];
    _email = json['email'];
    _phone = json['phone'];
    _homePhone = json['home_phone'];
    _website = json['website'];
    _createdAt = json['createdAt'];
    _updatedAt = json['updatedAt'];
    _userId = json['userId'];
  }
  num? _id;
  String? _streetAddress;
  dynamic _city;
  String? _state;
  dynamic _country;
  dynamic _suburb;
  String? _postalCode;
  String? _email;
  String? _phone;
  dynamic _homePhone;
  dynamic _website;
  String? _createdAt;
  String? _updatedAt;
  num? _userId;
  Address copyWith({
    num? id,
    String? streetAddress,
    dynamic city,
    String? state,
    dynamic country,
    dynamic suburb,
    String? postalCode,
    String? email,
    String? phone,
    dynamic homePhone,
    dynamic website,
    String? createdAt,
    String? updatedAt,
    num? userId,
  }) =>
      Address(
        id: id ?? _id,
        streetAddress: streetAddress ?? _streetAddress,
        city: city ?? _city,
        state: state ?? _state,
        country: country ?? _country,
        suburb: suburb ?? _suburb,
        postalCode: postalCode ?? _postalCode,
        email: email ?? _email,
        phone: phone ?? _phone,
        homePhone: homePhone ?? _homePhone,
        website: website ?? _website,
        createdAt: createdAt ?? _createdAt,
        updatedAt: updatedAt ?? _updatedAt,
        userId: userId ?? _userId,
      );
  num? get id => _id;
  String? get streetAddress => _streetAddress;
  dynamic get city => _city;
  String? get state => _state;
  dynamic get country => _country;
  dynamic get suburb => _suburb;
  String? get postalCode => _postalCode;
  String? get email => _email;
  String? get phone => _phone;
  dynamic get homePhone => _homePhone;
  dynamic get website => _website;
  String? get createdAt => _createdAt;
  String? get updatedAt => _updatedAt;
  num? get userId => _userId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['streetAddress'] = _streetAddress;
    map['city'] = _city;
    map['state'] = _state;
    map['country'] = _country;
    map['suburb'] = _suburb;
    map['postal_code'] = _postalCode;
    map['email'] = _email;
    map['phone'] = _phone;
    map['home_phone'] = _homePhone;
    map['website'] = _website;
    map['createdAt'] = _createdAt;
    map['updatedAt'] = _updatedAt;
    map['userId'] = _userId;
    return map;
  }
}
