class GetCategoryApisModel {
  GetCategoryApisModel({
    bool? error,
    Data? data,
    num? status,
  }) {
    _error = error;
    _data = data;
    _status = status;
  }

  GetCategoryApisModel.fromJson(Map<String, dynamic> json) {
    _error = json['error'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
    _status = json['status'];
  }
  bool? _error;
  Data? _data;
  num? _status;
  GetCategoryApisModel copyWith({
    bool? error,
    Data? data,
    num? status,
  }) =>
      GetCategoryApisModel(
        error: error ?? _error,
        data: data ?? _data,
        status: status ?? _status,
      );
  bool? get error => _error;
  Data? get data => _data;
  num? get status => _status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['error'] = _error;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    map['status'] = _status;
    return map;
  }
}

class Data {
  Data({
    num? totalItems,
    List<Row>? row,
  }) {
    _totalItems = totalItems;
    _row = row;
  }

  Data.fromJson(dynamic json) {
    _totalItems = json['totalItems'];
    if (json['row'] != null) {
      _row = [];
      json['row'].forEach((v) {
        _row?.add(Row.fromJson(v));
      });
    }
  }
  num? _totalItems;
  List<Row>? _row;
  Data copyWith({
    num? totalItems,
    List<Row>? row,
  }) =>
      Data(
        totalItems: totalItems ?? _totalItems,
        row: row ?? _row,
      );
  num? get totalItems => _totalItems;
  List<Row>? get row => _row;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['totalItems'] = _totalItems;
    if (_row != null) {
      map['row'] = _row?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class Row {
  Row({
    num? id,
    String? category,
    String? createdAt,
    String? updatedAt,
    List<Services>? services,
  }) {
    _id = id;
    _category = category;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _services = services;
  }

  Row.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _category = json['category'];
    _createdAt = json['createdAt'];
    _updatedAt = json['updatedAt'];
    if (json['services'] != null) {
      _services = [];
      json['services'].forEach((v) {
        _services?.add(Services.fromJson(v));
      });
    }
  }
  num? _id;
  String? _category;
  String? _createdAt;
  String? _updatedAt;
  List<Services>? _services;
  Row copyWith({
    num? id,
    String? category,
    String? createdAt,
    String? updatedAt,
    List<Services>? services,
  }) =>
      Row(
        id: id ?? _id,
        category: category ?? _category,
        createdAt: createdAt ?? _createdAt,
        updatedAt: updatedAt ?? _updatedAt,
        services: services ?? _services,
      );
  num? get id => _id;
  String? get category => _category;
  String? get createdAt => _createdAt;
  String? get updatedAt => _updatedAt;
  List<Services>? get services => _services;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['category'] = _category;
    map['createdAt'] = _createdAt;
    map['updatedAt'] = _updatedAt;
    if (_services != null) {
      map['services'] = _services?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

class Services {
  Services({
    num? id,
    String? serviceName,
    String? createdAt,
    String? updatedAt,
    num? industryId,
    List<Subservices>? subservices,
  }) {
    _id = id;
    _serviceName = serviceName;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _industryId = industryId;
    _subservices = subservices;
  }

  Services.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _serviceName = json['service_name'];
    _createdAt = json['createdAt'];
    _updatedAt = json['updatedAt'];
    _industryId = json['IndustryId'];
    if (json['subservices'] != null) {
      _subservices = [];
      json['subservices'].forEach((v) {
        _subservices?.add(Subservices.fromJson(v));
      });
    }
  }
  num? _id;
  String? _serviceName;
  String? _createdAt;
  String? _updatedAt;
  num? _industryId;
  List<Subservices>? _subservices;
  Services copyWith({
    num? id,
    String? serviceName,
    String? createdAt,
    String? updatedAt,
    num? industryId,
    List<Subservices>? subservices,
  }) =>
      Services(
        id: id ?? _id,
        serviceName: serviceName ?? _serviceName,
        createdAt: createdAt ?? _createdAt,
        updatedAt: updatedAt ?? _updatedAt,
        industryId: industryId ?? _industryId,
        subservices: subservices ?? _subservices,
      );
  num? get id => _id;
  String? get serviceName => _serviceName;
  String? get createdAt => _createdAt;
  String? get updatedAt => _updatedAt;
  num? get industryId => _industryId;
  List<Subservices>? get subservices => _subservices;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['service_name'] = _serviceName;
    map['createdAt'] = _createdAt;
    map['updatedAt'] = _updatedAt;
    map['IndustryId'] = _industryId;
    if (_subservices != null) {
      map['subservices'] = _subservices?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// id : 1090
/// subservice_name : "Android App Development"
/// createdAt : "2022-10-14T12:25:57.000Z"
/// updatedAt : "2022-10-14T12:25:57.000Z"
/// serviceId : 85

class Subservices {
  Subservices({
    num? id,
    String? subserviceName,
    String? createdAt,
    String? updatedAt,
    num? serviceId,
  }) {
    _id = id;
    _subserviceName = subserviceName;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _serviceId = serviceId;
  }

  Subservices.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _subserviceName = json['subservice_name'];
    _createdAt = json['createdAt'];
    _updatedAt = json['updatedAt'];
    _serviceId = json['serviceId'];
  }
  num? _id;
  String? _subserviceName;
  String? _createdAt;
  String? _updatedAt;
  num? _serviceId;
  Subservices copyWith({
    num? id,
    String? subserviceName,
    String? createdAt,
    String? updatedAt,
    num? serviceId,
  }) =>
      Subservices(
        id: id ?? _id,
        subserviceName: subserviceName ?? _subserviceName,
        createdAt: createdAt ?? _createdAt,
        updatedAt: updatedAt ?? _updatedAt,
        serviceId: serviceId ?? _serviceId,
      );
  num? get id => _id;
  String? get subserviceName => _subserviceName;
  String? get createdAt => _createdAt;
  String? get updatedAt => _updatedAt;
  num? get serviceId => _serviceId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['subservice_name'] = _subserviceName;
    map['createdAt'] = _createdAt;
    map['updatedAt'] = _updatedAt;
    map['serviceId'] = _serviceId;
    return map;
  }
}
