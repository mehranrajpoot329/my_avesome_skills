class AdNeedHelpPersonalPostModel {
  String? jobTitle,
      jobDescription,
      instruction,
      category,
      tags,
      subrub,
      startWork,
      jobPrice,
      repairInstallation,
      resources,
      area,
      tradeManVisit,
      workPlace,
      //   images,
      fixedPrice;

  AdNeedHelpPersonalPostModel({
    this.jobTitle,
    this.jobDescription,
    this.instruction,
    this.category,
    this.tags,
    this.subrub,
    this.startWork,
    this.jobPrice,
    this.repairInstallation,
    this.fixedPrice,
    this.resources,
    this.area,
    this.tradeManVisit,
    this.workPlace,
    //   this.images,
  });

  AdNeedHelpPersonalPostModel.fromJson(Map<String, dynamic> json) {
    jobTitle = json['job_title'].toString();
    jobDescription = json['job_description'].toString();
    instruction = json['instruction'].toString();
    category = json['category'].toString();
    tags = json['tags'].toString();
    startWork = json['starting_time'].toString();
    jobPrice = json['job_price'].toString();
    repairInstallation = json['token'].toString();
    fixedPrice = json['fixed_price'].toString();
    resources = json['resource'].toString();
    area = json['area'].toString();
    tradeManVisit = json['isTradeMan_visit'].toString();
    workPlace = json['workplace'].toString();
    // images = json['images'].toString();
  }
}
