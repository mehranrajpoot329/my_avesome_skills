import 'dart:convert';

/// error : false
/// data : {"totalItems":5,"row":[{"id":50,"job_title":"House Painting","job_description":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type a","instruction":"","category":"2","tags":"3,","starting_time":"3","job_price":"","fixed_price":"true","workplace":"2","image_url":"public/images/needhelpAd/2022-11-21T05:57:49.222Z.jpeg,public/images/needhelpAd/2022-11-21T05:57:49.224Z.jpeg,public/images/needhelpAd/2022-11-21T05:57:49.226Z.jpeg,public/images/needhelpAd/2022-11-21T05:57:49.341Z.jpeg","youtube_link":"","featured":"false","status":"119","isLike":null,"visits":null,"service":"14","subservice":"","resource":"","area":"12","isTradeMan_visit":"","progress_level":null,"createdAt":"2022-11-21T05:57:49.000Z","updatedAt":"2022-11-21T05:57:49.000Z","userId":38},null]}
/// status : 200

GetFeatureJobCardModel getFeatureJobCardModelFromJson(String str) =>
    GetFeatureJobCardModel.fromJson(json.decode(str));
String getFeatureJobCardModelToJson(GetFeatureJobCardModel data) =>
    json.encode(data.toJson());

class GetFeatureJobCardModel {
  GetFeatureJobCardModel({
    bool? error,
    Data? data,
    num? status,
  }) {
    _error = error;
    _data = data;
    _status = status;
  }

  GetFeatureJobCardModel.fromJson(dynamic json) {
    _error = json['error'];
    _data = Data.fromJson(json['data']);
    _status = json['status'];
  }
  bool? _error;
  Data? _data;
  num? _status;
  GetFeatureJobCardModel copyWith({
    bool? error,
    Data? data,
    num? status,
  }) =>
      GetFeatureJobCardModel(
        error: error ?? _error,
        data: data ?? _data,
        status: status ?? _status,
      );
  bool? get error => _error;
  Data? get data => _data;
  num? get status => _status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['error'] = _error;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    map['status'] = _status;
    return map;
  }
}

/// totalItems : 5
/// row : [{"id":50,"job_title":"House Painting","job_description":"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type a","instruction":"","category":"2","tags":"3,","starting_time":"3","job_price":"","fixed_price":"true","workplace":"2","image_url":"public/images/needhelpAd/2022-11-21T05:57:49.222Z.jpeg,public/images/needhelpAd/2022-11-21T05:57:49.224Z.jpeg,public/images/needhelpAd/2022-11-21T05:57:49.226Z.jpeg,public/images/needhelpAd/2022-11-21T05:57:49.341Z.jpeg","youtube_link":"","featured":"false","status":"119","isLike":null,"visits":null,"service":"14","subservice":"","resource":"","area":"12","isTradeMan_visit":"","progress_level":null,"createdAt":"2022-11-21T05:57:49.000Z","updatedAt":"2022-11-21T05:57:49.000Z","userId":38},null]

Data dataFromJson(String str) => Data.fromJson(json.decode(str));
String dataToJson(Data data) => json.encode(data.toJson());

class Data {
  Data({
    num? totalItems,
    List<Row>? row,
  }) {
    _totalItems = totalItems;
    _row = row;
  }

  Data.fromJson(dynamic json) {
    _totalItems = json['totalItems'];
    if (json['row'] != null) {
      _row = [];
      json['row'].forEach((v) {
        _row?.add(Row.fromJson(v));
      });
    }
  }
  num? _totalItems;
  List<Row>? _row;
  Data copyWith({
    num? totalItems,
    List<Row>? row,
  }) =>
      Data(
        totalItems: totalItems ?? _totalItems,
        row: row ?? _row,
      );
  num? get totalItems => _totalItems;
  List<Row>? get row => _row;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['totalItems'] = _totalItems;
    if (_row != null) {
      map['row'] = _row?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// id : 50
/// job_title : "House Painting"
/// job_description : "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type a"
/// instruction : ""
/// category : "2"
/// tags : "3,"
/// starting_time : "3"
/// job_price : ""
/// fixed_price : "true"
/// workplace : "2"
/// image_url : "public/images/needhelpAd/2022-11-21T05:57:49.222Z.jpeg,public/images/needhelpAd/2022-11-21T05:57:49.224Z.jpeg,public/images/needhelpAd/2022-11-21T05:57:49.226Z.jpeg,public/images/needhelpAd/2022-11-21T05:57:49.341Z.jpeg"
/// youtube_link : ""
/// featured : "false"
/// status : "119"
/// isLike : null
/// visits : null
/// service : "14"
/// subservice : ""
/// resource : ""
/// area : "12"
/// isTradeMan_visit : ""
/// progress_level : null
/// createdAt : "2022-11-21T05:57:49.000Z"
/// updatedAt : "2022-11-21T05:57:49.000Z"
/// userId : 38

Row rowFromJson(String str) => Row.fromJson(json.decode(str));
String rowToJson(Row data) => json.encode(data.toJson());

class Row {
  Row({
    num? id,
    String? jobTitle,
    String? jobDescription,
    String? instruction,
    String? category,
    String? tags,
    String? startingTime,
    String? jobPrice,
    String? fixedPrice,
    String? workplace,
    String? imageUrl,
    String? youtubeLink,
    String? featured,
    String? status,
    dynamic isLike,
    dynamic visits,
    String? service,
    String? subservice,
    String? resource,
    String? area,
    String? isTradeManVisit,
    dynamic progressLevel,
    String? createdAt,
    String? updatedAt,
    num? userId,
  }) {
    _id = id;
    _jobTitle = jobTitle;
    _jobDescription = jobDescription;
    _instruction = instruction;
    _category = category;
    _tags = tags;
    _startingTime = startingTime;
    _jobPrice = jobPrice;
    _fixedPrice = fixedPrice;
    _workplace = workplace;
    _imageUrl = imageUrl;
    _youtubeLink = youtubeLink;
    _featured = featured;
    _status = status;
    _isLike = isLike;
    _visits = visits;
    _service = service;
    _subservice = subservice;
    _resource = resource;
    _area = area;
    _isTradeManVisit = isTradeManVisit;
    _progressLevel = progressLevel;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _userId = userId;
  }

  Row.fromJson(dynamic json) {
    _id = json['id'];
    _jobTitle = json['job_title'];
    _jobDescription = json['job_description'];
    _instruction = json['instruction'];
    _category = json['category'];
    _tags = json['tags'];
    _startingTime = json['starting_time'];
    _jobPrice = json['job_price'];
    _fixedPrice = json['fixed_price'];
    _workplace = json['workplace'];
    _imageUrl = json['image_url'];
    _youtubeLink = json['youtube_link'];
    _featured = json['featured'];
    _status = json['status'];
    _isLike = json['isLike'];
    _visits = json['visits'];
    _service = json['service'];
    _subservice = json['subservice'];
    _resource = json['resource'];
    _area = json['area'];
    _isTradeManVisit = json['isTradeMan_visit'];
    _progressLevel = json['progress_level'];
    _createdAt = json['createdAt'];
    _updatedAt = json['updatedAt'];
    _userId = json['userId'];
  }
  num? _id;
  String? _jobTitle;
  String? _jobDescription;
  String? _instruction;
  String? _category;
  String? _tags;
  String? _startingTime;
  String? _jobPrice;
  String? _fixedPrice;
  String? _workplace;
  String? _imageUrl;
  String? _youtubeLink;
  String? _featured;
  String? _status;
  dynamic _isLike;
  dynamic _visits;
  String? _service;
  String? _subservice;
  String? _resource;
  String? _area;
  String? _isTradeManVisit;
  dynamic _progressLevel;
  String? _createdAt;
  String? _updatedAt;
  num? _userId;
  Row copyWith({
    num? id,
    String? jobTitle,
    String? jobDescription,
    String? instruction,
    String? category,
    String? tags,
    String? startingTime,
    String? jobPrice,
    String? fixedPrice,
    String? workplace,
    String? imageUrl,
    String? youtubeLink,
    String? featured,
    String? status,
    dynamic isLike,
    dynamic visits,
    String? service,
    String? subservice,
    String? resource,
    String? area,
    String? isTradeManVisit,
    dynamic progressLevel,
    String? createdAt,
    String? updatedAt,
    num? userId,
  }) =>
      Row(
        id: id ?? _id,
        jobTitle: jobTitle ?? _jobTitle,
        jobDescription: jobDescription ?? _jobDescription,
        instruction: instruction ?? _instruction,
        category: category ?? _category,
        tags: tags ?? _tags,
        startingTime: startingTime ?? _startingTime,
        jobPrice: jobPrice ?? _jobPrice,
        fixedPrice: fixedPrice ?? _fixedPrice,
        workplace: workplace ?? _workplace,
        imageUrl: imageUrl ?? _imageUrl,
        youtubeLink: youtubeLink ?? _youtubeLink,
        featured: featured ?? _featured,
        status: status ?? _status,
        isLike: isLike ?? _isLike,
        visits: visits ?? _visits,
        service: service ?? _service,
        subservice: subservice ?? _subservice,
        resource: resource ?? _resource,
        area: area ?? _area,
        isTradeManVisit: isTradeManVisit ?? _isTradeManVisit,
        progressLevel: progressLevel ?? _progressLevel,
        createdAt: createdAt ?? _createdAt,
        updatedAt: updatedAt ?? _updatedAt,
        userId: userId ?? _userId,
      );
  num? get id => _id;
  String? get jobTitle => _jobTitle;
  String? get jobDescription => _jobDescription;
  String? get instruction => _instruction;
  String? get category => _category;
  String? get tags => _tags;
  String? get startingTime => _startingTime;
  String? get jobPrice => _jobPrice;
  String? get fixedPrice => _fixedPrice;
  String? get workplace => _workplace;
  String? get imageUrl => _imageUrl;
  String? get youtubeLink => _youtubeLink;
  String? get featured => _featured;
  String? get status => _status;
  dynamic get isLike => _isLike;
  dynamic get visits => _visits;
  String? get service => _service;
  String? get subservice => _subservice;
  String? get resource => _resource;
  String? get area => _area;
  String? get isTradeManVisit => _isTradeManVisit;
  dynamic get progressLevel => _progressLevel;
  String? get createdAt => _createdAt;
  String? get updatedAt => _updatedAt;
  num? get userId => _userId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['job_title'] = _jobTitle;
    map['job_description'] = _jobDescription;
    map['instruction'] = _instruction;
    map['category'] = _category;
    map['tags'] = _tags;
    map['starting_time'] = _startingTime;
    map['job_price'] = _jobPrice;
    map['fixed_price'] = _fixedPrice;
    map['workplace'] = _workplace;
    map['image_url'] = _imageUrl;
    map['youtube_link'] = _youtubeLink;
    map['featured'] = _featured;
    map['status'] = _status;
    map['isLike'] = _isLike;
    map['visits'] = _visits;
    map['service'] = _service;
    map['subservice'] = _subservice;
    map['resource'] = _resource;
    map['area'] = _area;
    map['isTradeMan_visit'] = _isTradeManVisit;
    map['progress_level'] = _progressLevel;
    map['createdAt'] = _createdAt;
    map['updatedAt'] = _updatedAt;
    map['userId'] = _userId;
    return map;
  }
}
