import 'dart:convert';

/// error : false
/// data : {"totalItems":3,"totalPage":1,"row":[{"id":11,"title":"COMPUTER IT","description":"The Multan Cricket Stadium is a multi-purpose stadium in Multan, Punjab, Pakistan, owned by the Pakistan Cricket Board. The stadium is located off Vehari Road, in the suburbs of Multan. It has a capac","images":null,"category":{"category":"IT"},"services":[{"service_name":"COMPUTER IT"}],"featured_skill":"My Skill:40,My Skill 2:35,My Skill 3:20","extra":"undefined,undefined","status":{"status":"Active"},"isLike":null,"visits":null,"traveling_distance":"62","progress_level":{"title":"Pending"},"createdAt":"2022-12-09T10:14:48.000Z","updatedAt":"2022-12-09T10:15:42.000Z","userData":{"id":38,"f_uid":"PocVvIVMiCQVW4Sa36pYJQGzuXt1","first_name":"Sallah","last_name":"","email":"imsallah@gmail.com","phoneNumber":null,"photoURL":"public/images/profile/2022-11-24T05:51:03.016Z.jpeg","bussinessURL":"","accountType":"1","trade_name":null,"createdAt":"2022-11-07T14:09:35.000Z","updatedAt":"2022-12-07T09:23:02.000Z","address":{"id":1,"streetAddress":"","city":null,"state":null,"country":null,"suburb":null,"postal_code":null,"email":"","phone":"","home_phone":"","website":"","createdAt":"2022-11-10T11:56:15.000Z","updatedAt":"2022-11-24T05:51:03.000Z","userId":38}},"expended":"0"},null]}
/// status : 200

GetCanHelpModel getCanHelpModelFromJson(String str) =>
    GetCanHelpModel.fromJson(json.decode(str));
String getCanHelpModelToJson(GetCanHelpModel data) =>
    json.encode(data.toJson());

class GetCanHelpModel {
  GetCanHelpModel({
    bool? error,
    Data? data,
    num? status,
  }) {
    _error = error;
    _data = data;
    _status = status;
  }

  GetCanHelpModel.fromJson(dynamic json) {
    _error = json['error'];
    _data = json['data'] != null ? Data.fromJson(json['data']) : null;
    _status = json['status'];
  }
  bool? _error;
  Data? _data;
  num? _status;
  GetCanHelpModel copyWith({
    bool? error,
    Data? data,
    num? status,
  }) =>
      GetCanHelpModel(
        error: error ?? _error,
        data: data ?? _data,
        status: status ?? _status,
      );
  bool? get error => _error;
  Data? get data => _data;
  num? get status => _status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['error'] = _error;
    if (_data != null) {
      map['data'] = _data?.toJson();
    }
    map['status'] = _status;
    return map;
  }
}

/// totalItems : 3
/// totalPage : 1
/// row : [{"id":11,"title":"COMPUTER IT","description":"The Multan Cricket Stadium is a multi-purpose stadium in Multan, Punjab, Pakistan, owned by the Pakistan Cricket Board. The stadium is located off Vehari Road, in the suburbs of Multan. It has a capac","images":null,"category":{"category":"IT"},"services":[{"service_name":"COMPUTER IT"}],"featured_skill":"My Skill:40,My Skill 2:35,My Skill 3:20","extra":"undefined,undefined","status":{"status":"Active"},"isLike":null,"visits":null,"traveling_distance":"62","progress_level":{"title":"Pending"},"createdAt":"2022-12-09T10:14:48.000Z","updatedAt":"2022-12-09T10:15:42.000Z","userData":{"id":38,"f_uid":"PocVvIVMiCQVW4Sa36pYJQGzuXt1","first_name":"Sallah","last_name":"","email":"imsallah@gmail.com","phoneNumber":null,"photoURL":"public/images/profile/2022-11-24T05:51:03.016Z.jpeg","bussinessURL":"","accountType":"1","trade_name":null,"createdAt":"2022-11-07T14:09:35.000Z","updatedAt":"2022-12-07T09:23:02.000Z","address":{"id":1,"streetAddress":"","city":null,"state":null,"country":null,"suburb":null,"postal_code":null,"email":"","phone":"","home_phone":"","website":"","createdAt":"2022-11-10T11:56:15.000Z","updatedAt":"2022-11-24T05:51:03.000Z","userId":38}},"expended":"0"},null]

Data dataFromJson(String str) => Data.fromJson(json.decode(str));
String dataToJson(Data data) => json.encode(data.toJson());

class Data {
  Data({
    num? totalItems,
    num? totalPage,
    List<Row>? row,
  }) {
    _totalItems = totalItems;
    _totalPage = totalPage;
    _row = row;
  }

  Data.fromJson(dynamic json) {
    _totalItems = json['totalItems'];
    _totalPage = json['totalPage'];
    if (json['row'] != null) {
      _row = [];
      json['row'].forEach((v) {
        _row?.add(Row.fromJson(v));
      });
    }
  }
  num? _totalItems;
  num? _totalPage;
  List<Row>? _row;
  Data copyWith({
    num? totalItems,
    num? totalPage,
    List<Row>? row,
  }) =>
      Data(
        totalItems: totalItems ?? _totalItems,
        totalPage: totalPage ?? _totalPage,
        row: row ?? _row,
      );
  num? get totalItems => _totalItems;
  num? get totalPage => _totalPage;
  List<Row>? get row => _row;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['totalItems'] = _totalItems;
    map['totalPage'] = _totalPage;
    if (_row != null) {
      map['row'] = _row?.map((v) => v.toJson()).toList();
    }
    return map;
  }
}

/// id : 11
/// title : "COMPUTER IT"
/// description : "The Multan Cricket Stadium is a multi-purpose stadium in Multan, Punjab, Pakistan, owned by the Pakistan Cricket Board. The stadium is located off Vehari Road, in the suburbs of Multan. It has a capac"
/// images : null
/// category : {"category":"IT"}
/// services : [{"service_name":"COMPUTER IT"}]
/// featured_skill : "My Skill:40,My Skill 2:35,My Skill 3:20"
/// extra : "undefined,undefined"
/// status : {"status":"Active"}
/// isLike : null
/// visits : null
/// traveling_distance : "62"
/// progress_level : {"title":"Pending"}
/// createdAt : "2022-12-09T10:14:48.000Z"
/// updatedAt : "2022-12-09T10:15:42.000Z"
/// userData : {"id":38,"f_uid":"PocVvIVMiCQVW4Sa36pYJQGzuXt1","first_name":"Sallah","last_name":"","email":"imsallah@gmail.com","phoneNumber":null,"photoURL":"public/images/profile/2022-11-24T05:51:03.016Z.jpeg","bussinessURL":"","accountType":"1","trade_name":null,"createdAt":"2022-11-07T14:09:35.000Z","updatedAt":"2022-12-07T09:23:02.000Z","address":{"id":1,"streetAddress":"","city":null,"state":null,"country":null,"suburb":null,"postal_code":null,"email":"","phone":"","home_phone":"","website":"","createdAt":"2022-11-10T11:56:15.000Z","updatedAt":"2022-11-24T05:51:03.000Z","userId":38}}
/// expended : "0"

Row rowFromJson(String str) => Row.fromJson(json.decode(str));
String rowToJson(Row data) => json.encode(data.toJson());

class Row {
  Row({
    num? id,
    String? title,
    String? description,
    dynamic images,
    Category? category,
    List<Services>? services,
    String? featuredSkill,
    String? extra,
    Status? status,
    dynamic isLike,
    dynamic visits,
    String? travelingDistance,
    ProgressLevel? progressLevel,
    String? createdAt,
    String? updatedAt,
    UserData? userData,
    String? expended,
  }) {
    _id = id;
    _title = title;
    _description = description;
    _images = images;
    _category = category;
    _services = services;
    _featuredSkill = featuredSkill;
    _extra = extra;
    _status = status;
    _isLike = isLike;
    _visits = visits;
    _travelingDistance = travelingDistance;
    _progressLevel = progressLevel;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _userData = userData;
    _expended = expended;
  }

  Row.fromJson(dynamic json) {
    _id = json['id'];
    _title = json['title'];
    _description = json['description'];
    _images = json['images'];
    _category =
        json['category'] != null ? Category.fromJson(json['category']) : null;
    if (json['services'] != null) {
      _services = [];
      json['services'].forEach((v) {
        _services?.add(Services.fromJson(v));
      });
    }
    _featuredSkill = json['featured_skill'];
    _extra = json['extra'];
    _status = json['status'] != null ? Status.fromJson(json['status']) : null;
    _isLike = json['isLike'];
    _visits = json['visits'];
    _travelingDistance = json['traveling_distance'];
    _progressLevel = json['progress_level'] != null
        ? ProgressLevel.fromJson(json['progress_level'])
        : null;
    _createdAt = json['createdAt'];
    _updatedAt = json['updatedAt'];
    _userData =
        json['userData'] != null ? UserData.fromJson(json['userData']) : null;
    _expended = json['expended'];
  }
  num? _id;
  String? _title;
  String? _description;
  dynamic _images;
  Category? _category;
  List<Services>? _services;
  String? _featuredSkill;
  String? _extra;
  Status? _status;
  dynamic _isLike;
  dynamic _visits;
  String? _travelingDistance;
  ProgressLevel? _progressLevel;
  String? _createdAt;
  String? _updatedAt;
  UserData? _userData;
  String? _expended;
  Row copyWith({
    num? id,
    String? title,
    String? description,
    dynamic images,
    Category? category,
    List<Services>? services,
    String? featuredSkill,
    String? extra,
    Status? status,
    dynamic isLike,
    dynamic visits,
    String? travelingDistance,
    ProgressLevel? progressLevel,
    String? createdAt,
    String? updatedAt,
    UserData? userData,
    String? expended,
  }) =>
      Row(
        id: id ?? _id,
        title: title ?? _title,
        description: description ?? _description,
        images: images ?? _images,
        category: category ?? _category,
        services: services ?? _services,
        featuredSkill: featuredSkill ?? _featuredSkill,
        extra: extra ?? _extra,
        status: status ?? _status,
        isLike: isLike ?? _isLike,
        visits: visits ?? _visits,
        travelingDistance: travelingDistance ?? _travelingDistance,
        progressLevel: progressLevel ?? _progressLevel,
        createdAt: createdAt ?? _createdAt,
        updatedAt: updatedAt ?? _updatedAt,
        userData: userData ?? _userData,
        expended: expended ?? _expended,
      );
  num? get id => _id;
  String? get title => _title;
  String? get description => _description;
  dynamic get images => _images;
  Category? get category => _category;
  List<Services>? get services => _services;
  String? get featuredSkill => _featuredSkill;
  String? get extra => _extra;
  Status? get status => _status;
  dynamic get isLike => _isLike;
  dynamic get visits => _visits;
  String? get travelingDistance => _travelingDistance;
  ProgressLevel? get progressLevel => _progressLevel;
  String? get createdAt => _createdAt;
  String? get updatedAt => _updatedAt;
  UserData? get userData => _userData;
  String? get expended => _expended;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['title'] = _title;
    map['description'] = _description;
    map['images'] = _images;
    if (_category != null) {
      map['category'] = _category?.toJson();
    }
    if (_services != null) {
      map['services'] = _services?.map((v) => v.toJson()).toList();
    }
    map['featured_skill'] = _featuredSkill;
    map['extra'] = _extra;
    if (_status != null) {
      map['status'] = _status?.toJson();
    }
    map['isLike'] = _isLike;
    map['visits'] = _visits;
    map['traveling_distance'] = _travelingDistance;
    if (_progressLevel != null) {
      map['progress_level'] = _progressLevel?.toJson();
    }
    map['createdAt'] = _createdAt;
    map['updatedAt'] = _updatedAt;
    if (_userData != null) {
      map['userData'] = _userData?.toJson();
    }
    map['expended'] = _expended;
    return map;
  }
}

/// id : 38
/// f_uid : "PocVvIVMiCQVW4Sa36pYJQGzuXt1"
/// first_name : "Sallah"
/// last_name : ""
/// email : "imsallah@gmail.com"
/// phoneNumber : null
/// photoURL : "public/images/profile/2022-11-24T05:51:03.016Z.jpeg"
/// bussinessURL : ""
/// accountType : "1"
/// trade_name : null
/// createdAt : "2022-11-07T14:09:35.000Z"
/// updatedAt : "2022-12-07T09:23:02.000Z"
/// address : {"id":1,"streetAddress":"","city":null,"state":null,"country":null,"suburb":null,"postal_code":null,"email":"","phone":"","home_phone":"","website":"","createdAt":"2022-11-10T11:56:15.000Z","updatedAt":"2022-11-24T05:51:03.000Z","userId":38}

UserData userDataFromJson(String str) => UserData.fromJson(json.decode(str));
String userDataToJson(UserData data) => json.encode(data.toJson());

class UserData {
  UserData({
    num? id,
    String? fUid,
    String? firstName,
    String? lastName,
    String? email,
    dynamic phoneNumber,
    String? photoURL,
    String? bussinessURL,
    String? accountType,
    dynamic tradeName,
    String? createdAt,
    String? updatedAt,
    Address? address,
  }) {
    _id = id;
    _fUid = fUid;
    _firstName = firstName;
    _lastName = lastName;
    _email = email;
    _phoneNumber = phoneNumber;
    _photoURL = photoURL;
    _bussinessURL = bussinessURL;
    _accountType = accountType;
    _tradeName = tradeName;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _address = address;
  }

  UserData.fromJson(dynamic json) {
    _id = json['id'];
    _fUid = json['f_uid'];
    _firstName = json['first_name'];
    _lastName = json['last_name'];
    _email = json['email'];
    _phoneNumber = json['phoneNumber'];
    _photoURL = json['photoURL'];
    _bussinessURL = json['bussinessURL'];
    _accountType = json['accountType'];
    _tradeName = json['trade_name'];
    _createdAt = json['createdAt'];
    _updatedAt = json['updatedAt'];
    _address =
        json['address'] != null ? Address.fromJson(json['address']) : null;
  }
  num? _id;
  String? _fUid;
  String? _firstName;
  String? _lastName;
  String? _email;
  dynamic _phoneNumber;
  String? _photoURL;
  String? _bussinessURL;
  String? _accountType;
  dynamic _tradeName;
  String? _createdAt;
  String? _updatedAt;
  Address? _address;
  UserData copyWith({
    num? id,
    String? fUid,
    String? firstName,
    String? lastName,
    String? email,
    dynamic phoneNumber,
    String? photoURL,
    String? bussinessURL,
    String? accountType,
    dynamic tradeName,
    String? createdAt,
    String? updatedAt,
    Address? address,
  }) =>
      UserData(
        id: id ?? _id,
        fUid: fUid ?? _fUid,
        firstName: firstName ?? _firstName,
        lastName: lastName ?? _lastName,
        email: email ?? _email,
        phoneNumber: phoneNumber ?? _phoneNumber,
        photoURL: photoURL ?? _photoURL,
        bussinessURL: bussinessURL ?? _bussinessURL,
        accountType: accountType ?? _accountType,
        tradeName: tradeName ?? _tradeName,
        createdAt: createdAt ?? _createdAt,
        updatedAt: updatedAt ?? _updatedAt,
        address: address ?? _address,
      );
  num? get id => _id;
  String? get fUid => _fUid;
  String? get firstName => _firstName;
  String? get lastName => _lastName;
  String? get email => _email;
  dynamic get phoneNumber => _phoneNumber;
  String? get photoURL => _photoURL;
  String? get bussinessURL => _bussinessURL;
  String? get accountType => _accountType;
  dynamic get tradeName => _tradeName;
  String? get createdAt => _createdAt;
  String? get updatedAt => _updatedAt;
  Address? get address => _address;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['f_uid'] = _fUid;
    map['first_name'] = _firstName;
    map['last_name'] = _lastName;
    map['email'] = _email;
    map['phoneNumber'] = _phoneNumber;
    map['photoURL'] = _photoURL;
    map['bussinessURL'] = _bussinessURL;
    map['accountType'] = _accountType;
    map['trade_name'] = _tradeName;
    map['createdAt'] = _createdAt;
    map['updatedAt'] = _updatedAt;
    if (_address != null) {
      map['address'] = _address?.toJson();
    }
    return map;
  }
}

/// id : 1
/// streetAddress : ""
/// city : null
/// state : null
/// country : null
/// suburb : null
/// postal_code : null
/// email : ""
/// phone : ""
/// home_phone : ""
/// website : ""
/// createdAt : "2022-11-10T11:56:15.000Z"
/// updatedAt : "2022-11-24T05:51:03.000Z"
/// userId : 38

Address addressFromJson(String str) => Address.fromJson(json.decode(str));
String addressToJson(Address data) => json.encode(data.toJson());

class Address {
  Address({
    num? id,
    String? streetAddress,
    dynamic city,
    dynamic state,
    dynamic country,
    dynamic suburb,
    dynamic postalCode,
    String? email,
    String? phone,
    String? homePhone,
    String? website,
    String? createdAt,
    String? updatedAt,
    num? userId,
  }) {
    _id = id;
    _streetAddress = streetAddress;
    _city = city;
    _state = state;
    _country = country;
    _suburb = suburb;
    _postalCode = postalCode;
    _email = email;
    _phone = phone;
    _homePhone = homePhone;
    _website = website;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _userId = userId;
  }

  Address.fromJson(dynamic json) {
    _id = json['id'];
    _streetAddress = json['streetAddress'];
    _city = json['city'];
    _state = json['state'];
    _country = json['country'];
    _suburb = json['suburb'];
    _postalCode = json['postal_code'];
    _email = json['email'];
    _phone = json['phone'];
    _homePhone = json['home_phone'];
    _website = json['website'];
    _createdAt = json['createdAt'];
    _updatedAt = json['updatedAt'];
    _userId = json['userId'];
  }
  num? _id;
  String? _streetAddress;
  dynamic _city;
  dynamic _state;
  dynamic _country;
  dynamic _suburb;
  dynamic _postalCode;
  String? _email;
  String? _phone;
  String? _homePhone;
  String? _website;
  String? _createdAt;
  String? _updatedAt;
  num? _userId;
  Address copyWith({
    num? id,
    String? streetAddress,
    dynamic city,
    dynamic state,
    dynamic country,
    dynamic suburb,
    dynamic postalCode,
    String? email,
    String? phone,
    String? homePhone,
    String? website,
    String? createdAt,
    String? updatedAt,
    num? userId,
  }) =>
      Address(
        id: id ?? _id,
        streetAddress: streetAddress ?? _streetAddress,
        city: city ?? _city,
        state: state ?? _state,
        country: country ?? _country,
        suburb: suburb ?? _suburb,
        postalCode: postalCode ?? _postalCode,
        email: email ?? _email,
        phone: phone ?? _phone,
        homePhone: homePhone ?? _homePhone,
        website: website ?? _website,
        createdAt: createdAt ?? _createdAt,
        updatedAt: updatedAt ?? _updatedAt,
        userId: userId ?? _userId,
      );
  num? get id => _id;
  String? get streetAddress => _streetAddress;
  dynamic get city => _city;
  dynamic get state => _state;
  dynamic get country => _country;
  dynamic get suburb => _suburb;
  dynamic get postalCode => _postalCode;
  String? get email => _email;
  String? get phone => _phone;
  String? get homePhone => _homePhone;
  String? get website => _website;
  String? get createdAt => _createdAt;
  String? get updatedAt => _updatedAt;
  num? get userId => _userId;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['streetAddress'] = _streetAddress;
    map['city'] = _city;
    map['state'] = _state;
    map['country'] = _country;
    map['suburb'] = _suburb;
    map['postal_code'] = _postalCode;
    map['email'] = _email;
    map['phone'] = _phone;
    map['home_phone'] = _homePhone;
    map['website'] = _website;
    map['createdAt'] = _createdAt;
    map['updatedAt'] = _updatedAt;
    map['userId'] = _userId;
    return map;
  }
}

/// title : "Pending"

ProgressLevel progressLevelFromJson(String str) =>
    ProgressLevel.fromJson(json.decode(str));
String progressLevelToJson(ProgressLevel data) => json.encode(data.toJson());

class ProgressLevel {
  ProgressLevel({
    String? title,
  }) {
    _title = title;
  }

  ProgressLevel.fromJson(dynamic json) {
    _title = json['title'];
  }
  String? _title;
  ProgressLevel copyWith({
    String? title,
  }) =>
      ProgressLevel(
        title: title ?? _title,
      );
  String? get title => _title;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['title'] = _title;
    return map;
  }
}

/// status : "Active"

Status statusFromJson(String str) => Status.fromJson(json.decode(str));
String statusToJson(Status data) => json.encode(data.toJson());

class Status {
  Status({
    String? status,
  }) {
    _status = status;
  }

  Status.fromJson(dynamic json) {
    _status = json['status'];
  }
  String? _status;
  Status copyWith({
    String? status,
  }) =>
      Status(
        status: status ?? _status,
      );
  String? get status => _status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    return map;
  }
}

/// service_name : "COMPUTER IT"

Services servicesFromJson(String str) => Services.fromJson(json.decode(str));
String servicesToJson(Services data) => json.encode(data.toJson());

class Services {
  Services({
    String? serviceName,
  }) {
    _serviceName = serviceName;
  }

  Services.fromJson(dynamic json) {
    _serviceName = json['service_name'];
  }
  String? _serviceName;
  Services copyWith({
    String? serviceName,
  }) =>
      Services(
        serviceName: serviceName ?? _serviceName,
      );
  String? get serviceName => _serviceName;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['service_name'] = _serviceName;
    return map;
  }
}

/// category : "IT"

Category categoryFromJson(String str) => Category.fromJson(json.decode(str));
String categoryToJson(Category data) => json.encode(data.toJson());

class Category {
  Category({
    String? category,
  }) {
    _category = category;
  }

  Category.fromJson(dynamic json) {
    _category = json['category'];
  }
  String? _category;
  Category copyWith({
    String? category,
  }) =>
      Category(
        category: category ?? _category,
      );
  String? get category => _category;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['category'] = _category;
    return map;
  }
}
