import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';

MessageModel messageModelFromJson(String str) =>
    MessageModel.fromJson(json.decode(str));

String messageModelToJson(MessageModel messageModel) =>
    json.encode(messageModel.toJson());

class MessageModel {
  String? documentId;
  Timestamp? date;
  bool? isCompany;

  MessageModel({
    this.isCompany,
    this.date,
    this.documentId,
  });

  MessageModel.fromSnapshot(
      QueryDocumentSnapshot<Map<String, dynamic>> snapshot)
      : isCompany = snapshot["isCompany"],
        date = snapshot["date"],
        documentId = snapshot["id"];

  factory MessageModel.fromJson(Map<String, dynamic> json) => MessageModel(
        isCompany: json["isCompany"],
        date: json["date"],
        documentId: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "isCompany": isCompany,
        "date": date,
        "id": documentId,
      };
}
