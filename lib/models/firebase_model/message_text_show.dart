import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';

MessageTextShowModel messageTextShowFromJson(String str) =>
    MessageTextShowModel.fromJson(json.decode(str));

String messageTextShowToJson(MessageTextShowModel messageModel) =>
    json.encode(messageModel.toJson());

class MessageTextShowModel {
  Timestamp? date;
  String? fileUrl, message, type, uid;

  MessageTextShowModel({
    this.date,
    this.fileUrl,
    this.message,
    this.type,
    this.uid,
  });

  MessageTextShowModel.fromSnapshot(
      QueryDocumentSnapshot<Map<String, dynamic>> snapshot)
      : date = snapshot["date"],
        fileUrl = snapshot["fileUrl"],
        message = snapshot["message"],
        type = snapshot["type"],
        uid = snapshot["uid"];

  factory MessageTextShowModel.fromJson(Map<String, dynamic> json) =>
      MessageTextShowModel(
        date: json["date"],
        fileUrl: json["fileUrl"],
        message: json["message"],
        type: json["type"],
        uid: json["uid"],
      );

  Map<String, dynamic> toJson() => {
        "date": date,
        "fileUrl": fileUrl,
        "message": message,
        "type": type,
        "uid": uid,
      };
}
