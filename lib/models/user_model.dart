import 'package:hive/hive.dart';

part 'user_model.g.dart';

@HiveType(typeId: 0)
class User extends HiveObject {
  @HiveField(0)
  int? id;
  @HiveField(1)
  String? firstName;
  @HiveField(2)
  String? email;
  @HiveField(3)
  String? imageUrl;
  @HiveField(4)
  String? token;
  @HiveField(5)
  String? firebaseUId;
  @HiveField(6)
  String? phoneNumber;
  @HiveField(7)
  String? businessURL;
  @HiveField(8)
  String? accountType;
  @HiveField(9)
  String? tradeName;
  @HiveField(10)
  String? designation;
  @HiveField(11)
  String? createdAt;
  @HiveField(12)
  String? updatedAt;

  User({
    this.id,
    this.firstName,
    this.email,
    this.imageUrl,
    this.token,
    this.firebaseUId,
    this.phoneNumber,
    this.businessURL,
    this.accountType,
    this.tradeName,
    this.designation,
    this.createdAt,
    this.updatedAt,
  });

  User.fromJson(Map<String, dynamic> json) {
    id = json['id']?.toInt();
    firstName = json['first_name']?.toString();
    email = json['email']?.toString();
    imageUrl = json['photoURL']?.toString();
    token = json['token']?.toString();
    firebaseUId = json['f_uid']?.toString();
    phoneNumber = json['phoneNumber']?.toString();
    businessURL = json['bussinessURL']?.toString();
    accountType = json['accountType']?.toString();
    tradeName = json['trade_name']?.toString();
    designation = json['designation']?.toString();
    createdAt = json['createdAt']?.toString();
    updatedAt = json['updatedAt']?.toString();
  }
}
