// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_model.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class UserAdapter extends TypeAdapter<User> {
  @override
  final int typeId = 0;

  @override
  User read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return User(
      id: fields[0] as int?,
      firstName: fields[1] as String?,
      email: fields[2] as String?,
      imageUrl: fields[3] as String?,
      token: fields[4] as String?,
      firebaseUId: fields[5] as String?,
      phoneNumber: fields[6] as String?,
      businessURL: fields[7] as String?,
      accountType: fields[8] as String?,
      tradeName: fields[9] as String?,
      designation: fields[10] as String?,
      createdAt: fields[11] as String?,
      updatedAt: fields[12] as String?,
    );
  }

  @override
  void write(BinaryWriter writer, User obj) {
    writer
      ..writeByte(13)
      ..writeByte(0)
      ..write(obj.id)
      ..writeByte(1)
      ..write(obj.firstName)
      ..writeByte(2)
      ..write(obj.email)
      ..writeByte(3)
      ..write(obj.imageUrl)
      ..writeByte(4)
      ..write(obj.token)
      ..writeByte(5)
      ..write(obj.firebaseUId)
      ..writeByte(6)
      ..write(obj.phoneNumber)
      ..writeByte(7)
      ..write(obj.businessURL)
      ..writeByte(8)
      ..write(obj.accountType)
      ..writeByte(9)
      ..write(obj.tradeName)
      ..writeByte(10)
      ..write(obj.designation)
      ..writeByte(11)
      ..write(obj.createdAt)
      ..writeByte(12)
      ..write(obj.updatedAt);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is UserAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
