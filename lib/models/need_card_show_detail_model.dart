class NeedCardShowDetailModel {
  NeedCardShowDetailModel({
    num? id,
    String? title,
    String? description,
    String? instruction,
    String? isFeatured,
    String? images,
    Category? category,
    Services? services,
    List<Tags>? tags,
    StartingTime? startingTime,
    String? jobPrice,
    Workplace? workplace,
    Status? status,
    Resource? resource,
    String? isTradeManVisit,
    dynamic isLike,
    dynamic visits,
    ProgressLevel? progressLevel,
    String? area,
    String? createdAt,
    String? updatedAt,
    String? expended,
    UserData? userData,
  });

  num? id;
  String? title;
  String? description;
  String? instruction;
  String? isFeatured;
  String? images;
  Category? category;
  Services? services;
  List<Tags>? tags;
  StartingTime? startingTime;
  String? jobPrice;
  Workplace? workplace;
  Status? status;
  Resource? resource;
  String? isTradeManVisit;
  dynamic isLike;
  dynamic visits;
  ProgressLevel? progressLevel;
  String? area;
  String? createdAt;
  String? updatedAt;
  String? expended;
  UserData? userData;
}

class UserData {
  UserData({
    num? id,
    String? fUid,
    String? firstName,
    String? lastName,
    String? email,
    dynamic phoneNumber,
    String? photoURL,
    String? bussinessURL,
    String? accountType,
    dynamic tradeName,
    String? createdAt,
    String? updatedAt,
    Address? address,
  });
  num? id;
  String? fUid;
  String? firstName;
  String? lastName;
  String? email;
  dynamic phoneNumber;
  String? photoURL;
  String? bussinessURL;
  String? accountType;
  dynamic tradeName;
  String? createdAt;
  String? updatedAt;
  Address? address;
}

class Address {
  Address({
    num? id,
    String? streetAddress,
    dynamic city,
    dynamic state,
    dynamic country,
    dynamic suburb,
    dynamic postalCode,
    String? email,
    String? phone,
    String? homePhone,
    String? website,
    String? createdAt,
    String? updatedAt,
    num? userId,
  });
  num? id;
  String? streetAddress;
  dynamic city;
  dynamic state;
  dynamic country;
  dynamic suburb;
  dynamic postalCode;
  String? email;
  String? phone;
  String? homePhone;
  String? website;
  String? createdAt;
  String? updatedAt;
  num? userId;
}

class ProgressLevel {
  ProgressLevel({
    String? title,
  });
  String? title;
}

class Resource {
  Resource({
    String? resource,
  });
  String? resource;
}

class Status {
  Status({
    String? status,
  });
  String? status;
}

class Workplace {
  Workplace({
    String? environment,
  });
  String? environment;
}

class StartingTime {
  StartingTime({
    String? duration,
  });
  String? duration;
}

class Tags {
  Tags({
    String? title,
  });
  String? title;
}

class Services {
  Services({
    String? serviceName,
  });
  String? serviceName;
}

class Category {
  Category({
    String? category,
  });
  String? category;
}
