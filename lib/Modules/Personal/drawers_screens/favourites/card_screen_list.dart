import 'package:flutter/material.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/favourites/favourite_card.dart';

class CardScreenList extends StatelessWidget {
  const CardScreenList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: 4,
        itemBuilder: (context, int index) {
          return FavouriteCard(
            image: 'assets/images/profile_alex.png',
            personName: AppStrings.tomLatham,
            field: AppStrings.webDesignSmall,
            detail: AppStrings.easyToWork,
            date: AppStrings.twoMarch2022,
          );
        });
  }
}
