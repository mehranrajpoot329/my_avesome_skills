import 'package:flutter/material.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/rating_star.dart';
import 'package:sizer/sizer.dart';

class FavouriteCard extends StatelessWidget {
  String image, personName, field, detail, date;

  FavouriteCard({
    required this.image,
    required this.personName,
    required this.field,
    required this.detail,
    required this.date,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
            height: 25.h,
            width: 90.w,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(15),
                boxShadow: [
                  BoxShadow(
                      offset: Offset(0, 3),
                      color: Colors.grey.withOpacity(0.2),
                      spreadRadius: 3.0,
                      blurRadius: 3.0)
                ]),
            child: Stack(children: [
              Positioned(
                  top: 2.h,
                  left: 5.w,
                  child: Column(
                    children: [
                      Container(
                        height: 10.h,
                        width: 10.h,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage(image), fit: BoxFit.cover)),
                      ),
                      SizedBox(
                        height: 2.h,
                      ),
                      Text(
                        personName,
                        style: black12MulishTextStyle,
                      ),
                      SizedBox(
                        height: 1.h,
                      ),
                      Text(
                        field,
                        style: grey10MulishTextStyle,
                      )
                    ],
                  )),
              const Positioned(
                  top: 15,
                  right: 20,
                  child: Icon(
                    Icons.favorite,
                    color: AppColors.red2Color,
                  )),
              Positioned(
                  top: 6.h,
                  left: 30.w,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      RatingStar(
                        textStyle: greyTextColor,
                        isNumberRating: true,
                        ratingNumber: '52',
                      ),
                      SizedBox(
                        height: 1.h,
                      ),
                      SizedBox(
                        width: 200,
                        child: Text(
                          detail,
                          style: blackBoldPoppinsColor,
                        ),
                      )
                    ],
                  )),
              Positioned(
                  bottom: 2.h,
                  right: 3.w,
                  child: Text(
                    date,
                    style: blackBoldPoppinsColor,
                  )),
            ])),
        const SizedBox(
          height: 20.0,
        ),
      ],
    );
  }
}
