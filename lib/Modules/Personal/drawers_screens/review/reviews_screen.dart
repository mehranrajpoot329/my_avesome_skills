import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/review/my_review/my_reviews_tabBar.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/review/review_controller.dart';

import 'package:AwesomeSkills/widgets/custom_bottom_appBar_navi/custom_bottom_appBar_navi.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:sizer/sizer.dart';

import 'pending/pending_tabBar.dart';
import 'write_review/write_review_tabBar.dart';

class ReviewScreen extends StatelessWidget {
  final reviewController = Get.put(ReviewController());

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: NestedScrollWidget(
        floatingActionButton: WidgetMethod.floatingActionButton(context),
        body: Padding(
            padding: EdgeInsets.only(left: 3.w, right: 2.w, top: 2.h),
            child: Column(children: [
              Row(
                children: [
                  GestureDetector(
                    onTap: () => Get.back(),
                    child: Icon(
                      Icons.arrow_back_ios_new,
                      color: Colors.black,
                      size: 3.h,
                    ),
                  ),
                  SizedBox(
                    width: 5.w,
                  ),
                  const Text(
                    AppStrings.reviews,
                    style: montserrat20TextStyle,
                  ),
                ],
              ),
              SizedBox(
                height: 1.h,
              ),
              Obx(
                () => Visibility(
                  visible: reviewController.isReviewMessageVisible.value,
                  child: Padding(
                    padding: EdgeInsets.only(left: 10.w),
                    child: const Text(
                      AppStrings.leaveFeedback,
                      style: grey14Color,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
              Container(
                height: 16.h,
                padding: EdgeInsets.only(left: 2.w, right: 2.w, top: 0.5.h),
                decoration: BoxDecoration(
                  color: AppColors.lightGreen2Color,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left: 3.w),
                          child: Text(
                            AppStrings.reviews,
                            style: orange600ColorTextStyle,
                          ),
                        ),
                        IconButton(
                          onPressed: () => reviewController.showReviewMessage(),
                          icon: Icon(
                            Icons.info_rounded,
                            size: 4.h,
                          ),
                          color: AppColors.lightBlueColor,
                        )
                      ],
                    ),
                    const TabBar(
                        isScrollable: true,
                        indicatorSize: TabBarIndicatorSize.label,
                        indicatorColor: AppColors.lightBlueColor,
                        labelColor: Colors.black,
                        labelStyle: TextStyle(fontWeight: FontWeight.bold),
                        unselectedLabelStyle: TextStyle(
                            fontWeight: FontWeight.w400, color: Colors.black),
                        tabs: [
                          Tab(
                            text: AppStrings.myReview,
                          ),
                          Tab(
                            text: AppStrings.pending,
                          ),
                          Tab(
                            text: AppStrings.writeReview,
                          ),
                        ]),
                  ],
                ),
              ),
              Expanded(
                child: TabBarView(
                    physics: NeverScrollableScrollPhysics(),
                    children: [
                      MyReviewTabBar(),
                      const PendingTabBar(),
                      WriteReviewTaBar(),
                    ]),
              ),
            ])),
      ),
    );
  }
}
