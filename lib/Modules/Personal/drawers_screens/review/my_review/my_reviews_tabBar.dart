import 'package:AwesomeSkills/Modules/Personal/drawers_screens/review/review_controller.dart';
import 'package:AwesomeSkills/local/db/local_storage.dart';
import 'package:flutter/material.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/review/my_review/my_review_card.dart';
import 'package:get/get.dart';

class MyReviewTabBar extends StatelessWidget {
  final reviewController = Get.put(ReviewController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => reviewController.getReviewDataList.isEmpty
          ? const Center(child: CircularProgressIndicator())
          : ListView.builder(
              itemCount: reviewController.getReviewDataList.length,
              itemBuilder: (context, int index) {
                String dateSplit =
                    reviewController.getReviewDataList[index].createdAt ?? "";
                var listOfData = dateSplit.split("T");
                return ReviewCard(
                  image: reviewController.getUserInfoData.value.photoURL ?? "",
                  personName:
                      ("${reviewController.getUserInfoData.value.firstName}${reviewController.getUserInfoData.value.firstName}"),
                  field: AppStrings.webDesigner,
                  detail:
                      reviewController.getReviewDataList[index].comments ?? "",
                  date: listOfData[0],
                );
              }),
    );
  }
}
