import 'package:AwesomeSkills/Modules/Personal/drawers_screens/review/review_controller.dart';
import 'package:flutter/material.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/rating_star.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';

class ReviewCard extends StatelessWidget {
  final reviewController = Get.put(ReviewController());

  String image, personName, field, detail, date;

  ReviewCard({
    required this.image,
    required this.personName,
    required this.field,
    required this.detail,
    required this.date,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
            height: 25.h,
            width: 90.w,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(15),
                boxShadow: [
                  BoxShadow(
                      offset: Offset(0, 5),
                      color: Colors.grey.withOpacity(0.2),
                      spreadRadius: 3.0,
                      blurRadius: 3.0)
                ]),
            child: Stack(children: [
              Positioned(
                  top: 2.h,
                  left: 5.w,
                  child: Column(
                    children: [
                      reviewController.getUserInfoData.value == ""
                          ? Container(
                              height: 10.h,
                              width: 10.h,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: NetworkImage(image),
                                      fit: BoxFit.cover)),
                            )
                          : Container(
                              height: 10.h,
                              width: 10.h,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: AppColors.grey2Color.withOpacity(0.2)),
                              child: Center(
                                child: Icon(
                                  Icons.person,
                                  color: Colors.grey,
                                  size: 8.h,
                                ),
                              ),
                            ),
                      SizedBox(
                        height: 2.h,
                      ),
                      Text(
                        personName,
                        style: black12MulishTextStyle,
                      ),
                      SizedBox(
                        height: 1.h,
                      ),
                      Text(
                        field,
                        style: grey10MulishTextStyle,
                      )
                    ],
                  )),
              Positioned(
                  top: 2.h,
                  right: 3.w,
                  child: Icon(
                    Icons.check_circle,
                    color: AppColors.lightBlueColor,
                  )),
              Positioned(
                  top: 6.h,
                  left: 30.w,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      RatingStar(
                        textStyle: greyTextColor,
                        isNumberRating: true,
                        ratingNumber: '52',
                      ),
                      SizedBox(
                        height: 1.h,
                      ),
                      SizedBox(
                        width: 50.w,
                        child: Text(
                          detail,
                          style: blackMulishTextStyle,
                        ),
                      )
                    ],
                  )),
              Positioned(
                  bottom: 2.h,
                  right: 3.w,
                  child: Text(
                    date,
                    style: blackBoldPoppinsColor,
                  )),
            ])),
        const SizedBox(
          height: 20.0,
        ),
      ],
    );
  }
}
