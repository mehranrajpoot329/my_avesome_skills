import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/review/review_controller.dart';
import 'package:sizer/sizer.dart';

class SliderInfoCard extends StatefulWidget {
  @override
  State<SliderInfoCard> createState() => _SliderInfoCardState();
}

class _SliderInfoCardState extends State<SliderInfoCard> {
  final controller = Get.put(ReviewController());

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
          padding: EdgeInsets.symmetric(vertical: 2.h, horizontal: 2.w),
          height: 60.h,
          width: 90.w,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15.0),
              boxShadow: [
                BoxShadow(
                    offset: Offset(0, 5),
                    color: Colors.grey.withOpacity(0.2),
                    spreadRadius: 2.0,
                    blurRadius: 4.0)
              ]),
          child: Column(
            children: [
              buildSlider(
                  label: controller.honestValue.round().toString(),
                  textName: AppStrings.honesty,
                  sliderValue: controller.honestValue,
                  onChanged: (double newValue) {
                    setState(() {
                      controller.honestValue = newValue.round();
                      print(controller.honestValue);
                    });
                  }),
              buildSlider(
                  label: controller.workValue.round().toString(),
                  textName: AppStrings.workQuality,
                  sliderValue: controller.workValue,
                  onChanged: (double newValue) {
                    setState(() {
                      controller.workValue = newValue.round();
                    });
                  }),
              buildSlider(
                  label: controller.technicalSkillValue.round().toString(),
                  textName: AppStrings.technicalSkill,
                  sliderValue: controller.technicalSkillValue,
                  onChanged: (double newValue) {
                    setState(() {
                      controller.technicalSkillValue = newValue.round();
                    });
                  }),
              buildSlider(
                  label: controller.punctualValue.round().toString(),
                  textName: AppStrings.punctuality,
                  sliderValue: controller.punctualValue,
                  onChanged: (double newValue) {
                    setState(() {
                      controller.punctualValue = newValue.round();
                    });
                  }),
              buildSlider(
                  label: controller.communicationValue.round().toString(),
                  textName: AppStrings.communication,
                  sliderValue: controller.communicationValue,
                  onChanged: (double newValue) {
                    setState(() {
                      controller.communicationValue = newValue.round();
                    });
                  }),
            ],
          )),
    );
  }

  Widget buildSlider(
      {required String textName,
      required int sliderValue,
      required String label,
      required Function onChanged}) {
    return Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
      Text(
        textName,
        style: blackBold16Color,
      ),
      SliderTheme(
        data: const SliderThemeData(
          trackHeight: 4,
        ),
        child: Slider(
            value: sliderValue.toDouble(),
            min: 0.0,
            max: 5.0,
            divisions: 5,
            activeColor: AppColors.lightBlueColor,
            inactiveColor: Colors.blue.withOpacity(0.2),
            label: label,
            onChanged: (double newValue) => onChanged(newValue)),
      ),
      SizedBox(
        height: 0.5.h,
      ),
    ]);
  }
}
