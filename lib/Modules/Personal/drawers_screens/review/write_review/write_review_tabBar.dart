import 'dart:developer';

import 'package:AwesomeSkills/Modules/Personal/drawers_screens/review/write_review/write_review_controller.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/review/write_review/rating_card.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/review/review_controller.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/review/write_review/slider_info_card.dart';

import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:AwesomeSkills/widgets/drop_down.dart';
import 'package:sizer/sizer.dart';

class WriteReviewTaBar extends StatefulWidget {
  @override
  State<WriteReviewTaBar> createState() => _WriteReviewTaBarState();
}

class _WriteReviewTaBarState extends State<WriteReviewTaBar> {
  final controller = Get.put(ReviewController());
  final writeReviewController = Get.put(WriteReviewController());

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 2.w),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 1.h,
            ),
            const Text(
              AppStrings.writeAReview,
              style: blue600ColorTextStyle,
            ),
            SizedBox(
              height: 1.h,
            ),
            CustomDropDown(
              items: [
                'Project to Review',
                'Project to Review 1',
                'Project to Review 2',
                'Project to Review 3',
              ],
              height: 7.h,
              width: Get.width,
              hint: AppStrings.projectToReview,
              selectedValue: (value) {
                controller.projectReview.value = value!;
                log(value.toString());
              },
            ),
            SizedBox(
              height: 2.h,
            ),
            RatingCommentCard(),
            SizedBox(
              height: 2.h,
            ),
            SliderInfoCard(),
            SizedBox(
              height: 2.h,
            ),
            const FittedBox(
              child: Text(
                AppStrings.typeCharacterYouSee,
                style: lightBlueTextStyle,
              ),
            ),
            SizedBox(
              height: 1.h,
            ),
            Row(
              children: [
                Container(
                  height: 43,
                  width: 124,
                  decoration: BoxDecoration(
                    color: AppColors.lightBlueColor.withOpacity(0.1),
                    borderRadius: BorderRadius.circular(2),
                  ),
                  child: const Center(
                    child: Text(
                      AppStrings.tripleSevenTripleNine,
                      style: lightBlueTextStyle,
                    ),
                  ),
                ),
                SizedBox(
                  width: 2.w,
                ),
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  height: 43,
                  width: 124,
                  decoration: BoxDecoration(
                    color: AppColors.lightBlueColor.withOpacity(0.1),
                    borderRadius: BorderRadius.circular(2),
                  ),
                  child: TextFormField(
                    controller: controller.codeTypingTextEditingController,
                    decoration: const InputDecoration(border: InputBorder.none),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 2.h,
            ),
            SizedBox(
              width: 100.w,
              child: Row(
                children: [
                  Checkbox(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(3)),
                    side:
                        const BorderSide(color: AppColors.blueColor, width: 2),
                    checkColor: Colors.white,
                    activeColor: AppColors.blueColor,
                    value: controller.valueFirst,
                    onChanged: (bool? value) {
                      setState(() {
                        controller.valueFirst = value!;
                      });
                    },
                  ),
                  const Expanded(
                    child: Text(
                      AppStrings.iAccept,
                      style: TextStyle(
                        fontFamily: 'poppins',
                        fontSize: 12,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            CustomButton(
                buttonName: AppStrings.postYourReview,
                width: 50.w,
                height: 5.h,
                buttonColor: AppColors.lightBlueColor,
                textStyle: customButtonWhiteTextStyle,
                borderRadius: BorderRadius.circular(2),
                onTap: () => writeReviewController.reviewPost()),
            SizedBox(
              height: 2.h,
            )
          ],
        ),
      ),
    );
  }
}
