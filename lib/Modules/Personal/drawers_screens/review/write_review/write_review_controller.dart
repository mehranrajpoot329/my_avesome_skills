import 'package:AwesomeSkills/Modules/Personal/drawers_screens/review/review_controller.dart';
import 'package:AwesomeSkills/network/base_response.dart';
import 'package:AwesomeSkills/network/end_points.dart';
import 'package:AwesomeSkills/network/requests.dart';
import 'package:AwesomeSkills/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';

class WriteReviewController extends GetxController {
  TextEditingController commentTextEditingController = TextEditingController();

  final reviewController = Get.put(ReviewController());

  final reviewFormKey = GlobalKey<FormBuilderState>();
  double _ratingStar = 0;

  ratingFunction(rating) {
    _ratingStar = rating;
    print(_ratingStar);
    update();
  }

  Future<void> reviewPost() async {
    try {
      if (reviewFormKey.currentState!.saveAndValidate()) {
        var formData = ({
          "job": "1",
          "rating": _ratingStar,
          'comments': commentTextEditingController.text.toString(),
          "honesty": reviewController.honestValue,
          "work_quality": reviewController.workValue,
          "technical_skill": reviewController.technicalSkillValue,
          "punctuality": reviewController.punctualValue,
          "communication_skill": reviewController.communicationValue,
        });
        BaseResponse response = await DioClient().postRequest(
          endPoint: EndPoints.postReviewUrl,
          body: formData,
        );
        if (response.error == false) {
          Utils.errorFlutterToast(msg: response.data);
        } else {
          Utils.errorFlutterToast(msg: response.message.toString());
        }
      }
    } catch (e) {
      Utils.errorSnackBar(message: e.toString());
    }
  }
}
