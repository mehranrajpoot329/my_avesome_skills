import 'package:flutter/material.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/review/pending/pending_card.dart';

class PendingTabBar extends StatelessWidget {
  const PendingTabBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: 10,
        itemBuilder: (context, int index) {
          return PendingCard(
            image: 'assets/images/profile_alex.png',
            personName: AppStrings.tomLatham,
            field: AppStrings.webDesignSmall,
            waiting: AppStrings.waitingOnReview,
            date: AppStrings.twoMarch2022,
          );
        });
  }
}
