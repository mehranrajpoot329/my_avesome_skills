import 'dart:developer';

import 'package:AwesomeSkills/local/db/local_storage.dart';
import 'package:AwesomeSkills/network/base_response.dart';
import 'package:AwesomeSkills/network/end_points.dart';
import 'package:AwesomeSkills/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../models/api_model/personal_model/get_review_model.dart'
    as get_review_model;
import '../../../../models/api_model/personal_model/user_info_model.dart'
    as get_user_info_model;
import '../../../../network/requests.dart';

class ReviewController extends GetxController {
  TextEditingController commentTextEditingController = TextEditingController();
  TextEditingController codeTypingTextEditingController =
      TextEditingController();
  RxBool isReviewMessageVisible = false.obs;
  RxString projectReview = ''.obs;

  RxList<get_review_model.Data> getReviewDataList =
      <get_review_model.Data>[].obs;
  Rx<get_user_info_model.Data> getUserInfoData = get_user_info_model.Data().obs;

  bool valueFirst = false;
  int honestValue = 3;
  int workValue = 3;
  int technicalSkillValue = 3;
  int punctualValue = 3;
  int communicationValue = 3;

  void showReviewMessage() {
    isReviewMessageVisible.value = !isReviewMessageVisible.value;
    update();
  }

  @override
  onInit() {
    super.onInit();
    Future.delayed(20.milliseconds, () {
      getMyReviewData();
    });
  }

  Future<void> getMyReviewData() async {
    try {
      Utils.showProgressBar();
      BaseResponse response = await DioClient().getRequest(
          endPoint: EndPoints.getMyReviewUrl,
          headers: {"token": HiveHelper.getAuthToken()});

      if (response.error == false) {
        print(HiveHelper.getAuthToken());

        for (var item in response.data) {
          getReviewDataList.add(get_review_model.Data.fromJson(item));
          Utils.dismissProgressBar();
        }
      } else {
        Utils.errorFlutterToast(msg: response.message.toString());
      }
    } catch (e) {
      Utils.dismissProgressBar();
      log('$e');
      Utils.errorFlutterToast(msg: e.toString());
    }
  }

  Future<void> getCanHelp() async {
    try {
      Utils.showProgressBar();
      BaseResponse response = await DioClient().getRequest(
          endPoint: EndPoints.getUserInfo,
          headers: {"token": HiveHelper.getAuthToken()});

      if (response.error == false) {
        get_user_info_model.Data baseResponse =
            get_user_info_model.Data.fromJson(response.data);
        getUserInfoData.value = baseResponse;
      } else {
        Utils.errorFlutterToast(msg: response.message.toString());
      }
      Utils.dismissProgressBar();
    } catch (e) {
      Utils.dismissProgressBar();
      log('$e');
      Utils.errorFlutterToast(msg: e.toString());
    }
  }
}
