import 'package:flutter/material.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/messages/build_card_widget.dart';

class RejectedTabBarScreen extends StatelessWidget {
  const RejectedTabBarScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15.0),
      child: Scrollbar(
        trackVisibility: true,
        thickness: 10,
        radius: const Radius.circular(10),
        child: ListView.builder(
          scrollDirection: Axis.vertical,
          padding: const EdgeInsets.only(right: 20),
          itemCount: 10,
          itemBuilder: (context, int index) {
            return BuildCardWidget(
                imagePath: 'assets/images/profile_messeger.png',
                textName: AppStrings.rupertLandstroom,
                postTimingAgo: AppStrings.threeHourAgo);
          },
        ),
      ),
    );
  }
}
