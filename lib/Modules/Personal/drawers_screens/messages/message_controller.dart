import 'package:AwesomeSkills/Modules/Personal/drawers_screens/messages/message_typing_screen.dart';
import 'package:AwesomeSkills/constants/firebase_strings.dart';
import 'package:AwesomeSkills/firebase/firebase.dart';
import 'package:AwesomeSkills/local/db/local_storage.dart';
import 'package:AwesomeSkills/models/firebase_model/get_user_firebase_model.dart';
import 'package:AwesomeSkills/models/firebase_model/message_model.dart';
import 'package:AwesomeSkills/models/firebase_model/message_text_show.dart';
import 'package:AwesomeSkills/network/base_response.dart';
import 'package:AwesomeSkills/network/end_points.dart';
import 'package:AwesomeSkills/network/requests.dart';
import 'package:AwesomeSkills/utils/utils.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart' hide FormData, MultipartFile;
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:http/http.dart' as http;
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

import '../../../../routes/app_pages.dart';

class MessageController extends GetxController {
  TextEditingController messageTextEditingController = TextEditingController();

  RxList<MessageModel> messageList = <MessageModel>[].obs;
  RxList<MessageTextShowModel> messageTextShowList =
      <MessageTextShowModel>[].obs;
  RxList<Data> getFirebaseUser = <Data>[].obs;

  bool isScrollable = false;

  CroppedFile? messagePicUpload;

  Future<void> selectImage(ImageSource selectedSource) async {
    messagePicUpload = await Utils.getImage(source: selectedSource);
    update();
  }

  @override
  void onInit() {
    FirebaseAuthorization.collectionReference = FirebaseAuthorization
        .firebaseFirestore
        .collection(FirebaseStrings.chatCollection);
    //   typeMessage();
    super.onInit();
  }

  Future<void>? createChannel(
    String userId,
  ) {
    var docId = FirebaseAuthorization.firebaseFirestore
        .collection(FirebaseStrings.chatCollection)
        .doc();
    FirebaseAuthorization.collectionReference.doc(docId.id).set({
      "id": docId.id,
      "isCompany": true,
      "date": DateTime.now(),
      "uid": {
        "0": FirebaseAuthorization.firebaseAuth.currentUser!.uid,
        "1": userId,
      }
    }).then((value) {
      print("-------- ${docId.id} ----------}");
      jobAddData(uid: docId.id);
      Get.toNamed(AppPage.needHelpMessageScreen);
    }).onError((error, stackTrace) {
      Utils.errorSnackBar(message: error.toString());
    });
  }

  Future<void>? typeMessage() async {
    var query = FirebaseAuthorization.collectionReference.where("uid",
        arrayContains: FirebaseAuthorization.firebaseAuth.currentUser!.uid);
    query.snapshots().listen((value) {
      for (var element in value.docs) {
        print("--------- $element ---------------");
        messageList.add(MessageModel(
          isCompany: element["isCompany"],
          date: element['date'],
          documentId: element["id"],
        ));
      }
    }).onError((error, stackTrace) {
      Utils.errorSnackBar(message: error.toString());
    });
  }

  Future<void>? getMessageFunction({String? uid}) {
    FirebaseAuthorization.collectionReference
        .doc(uid!)
        .collection("messages")
        .orderBy("date")
        .snapshots()
        .listen((value) {
      print(FirebaseAuthorization.firebaseAuth.currentUser!.uid);
      messageTextShowList.clear();
      print(value.docs);
      for (var element in value.docs) {
        print(value.docs);
        print(element.id);
        messageTextShowList.add(MessageTextShowModel(
          message: element['message'],
          //  date: element['date'],
          type: element['type'],
          uid: element['uid'],
          fileUrl: element['fileUrl'],
        ));
      }
      Get.toNamed(AppPage.myMessageTyping);
    });
    return null;
  }

  Future<void> uploadMessageFile() async {
    try {
      var headers = {'Content-Type': 'application/json'};

      var request = http.MultipartRequest(
          'POST', Uri.parse(EndPoints.baseURL + EndPoints.messageUploadFile));

      request.files.add(
          await http.MultipartFile.fromPath('file', messagePicUpload!.path));

      http.StreamedResponse response = await request.send();
      request.headers.addAll(headers);

      if (response.statusCode == 201) {
        print(await response.stream.bytesToString());
      } else {
        print(response.reasonPhrase);
      }
    } catch (e) {
      print(e.toString());
    }

    // // var formData = ({"file": await messagePicUpload!.path});
    // FormData formData = FormData.fromMap({
    //   "file": (messagePicUpload != null)
    //       ? await MultipartFile.fromFile(messagePicUpload!.path,
    //           filename: messagePicUpload!.path)
    //       : ''
    // });
    //
    // BaseResponse response = await DioClient().postFormRequest(
    //   endPoint: EndPoints.messageUploadFile,
    //   body: formData,
    // );
    // if (response.error == false) {
    // } else {
    //   BaseResponse baseResponse = BaseResponse.fromJson(response.data);
    //   print(response.data);
    //   Utils.errorFlutterToast(msg: baseResponse.message.toString());
    // }
  }

  Future<void> postMessage({String? uid}) async {
    // var query = FirebaseAuthorization.collectionReference.where("uid",
    //     arrayContains: FirebaseAuthorization.firebaseAuth.currentUser!.uid);
    // query.get().then((value) {
    //   for (var element in value.docs) {
    //     uid = element.id;
    //   }
    FirebaseAuthorization.firebaseFirestore
        .collection(FirebaseStrings.chatCollection)
        .doc(uid)
        .collection(FirebaseStrings.messages)
        .doc()
        .set({
      "message": messageTextEditingController.text.toString(),
      "date": DateTime.now(),
      "fileUrl": "is null",
      "type": 'type is.....',
      "uid": FirebaseAuthorization.firebaseAuth.currentUser!.uid,
    }).then((value) {
      messageTextEditingController.clear();
    });
  }

  Future<void> jobAddData({String? uid}) async {
    // var query = FirebaseAuthorization.collectionReference.where("uid",
    //     arrayContains: FirebaseAuthorization.firebaseAuth.currentUser!.uid);
    // query.get().then((value) {
    //   for (var element in value.docs) {
    //     uid = element.id;
    //   }
    FirebaseAuthorization.firebaseFirestore
        .collection(FirebaseStrings.chatCollection)
        .doc(uid)
        .collection(FirebaseStrings.jobs)
        .doc()
        .set({
      "isAccepted": false,
      "date": DateTime.now(),
      "jobType": "is null",
      "message": 'type is.....',
      "postBy": FirebaseAuthorization.firebaseAuth.currentUser!.uid,
      "postId": FirebaseAuthorization.firebaseAuth.currentUser!.uid,
      "requestedBy": FirebaseAuthorization.firebaseAuth.currentUser!.uid,
    }).then((value) {
      postMessage(uid: uid);
      messageTextEditingController.clear();
    });
  }

  Future<void> getFirebaseUserData() async {
    try {
      Utils.showProgressBar();
      BaseResponse response = await DioClient().getRequest(
        endPoint: EndPoints.getFirebaseUserUrl,
      );

      if (response.error == false) {
        for (var element in response.data) {
          getFirebaseUser.add(Data.fromJson(element));
        }
        //  Get.to(() => MessageTypingScreen(), arguments: getFirebaseUser[0].uid);
      } else {
        Utils.errorFlutterToast(msg: response.message.toString());
      }
      Utils.dismissProgressBar();
    } catch (e) {
      Utils.dismissProgressBar();
      Utils.errorFlutterToast(msg: e.toString());
    }
  }

  Future<void> acceptJobProposal() async {
    var formData = {
      'project_name': "painting",
      'project_id': "1",
      'project_user': "hello text",
    };

    BaseResponse response = await DioClient().postRequest(
        endPoint: EndPoints.jobAssignUrl,
        body: formData,
        headers: {"Authorization": "Bearer ${HiveHelper.getAuthToken()}"});
    if (response.statusCode == 200) {
      print(response.data);
      Utils.successSnackBar(message: response.data.toString());
    } else {
      Utils.errorFlutterToast(msg: response.message.toString());
    }
  }
}
