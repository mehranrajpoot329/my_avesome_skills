import 'dart:io';

import 'package:AwesomeSkills/firebase/firebase.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/messages/message_controller.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:AwesomeSkills/widgets/custom_bottom_appBar_navi/custom_bottom_appBar_navi.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sizer/sizer.dart';

import '../../../../constants/app_fonts.dart';

class MessageTypingScreen extends StatelessWidget {
  final controller = Get.put(MessageController());

  Future<ImageSource?> pickImage() async {
    ImageSource? selectedSource = await Get.defaultDialog(
      title: 'Select From',
      content: Column(
        children: [
          GestureDetector(
            onTap: () {
              Get.back(result: ImageSource.gallery);
            },
            child: const ListTile(
              leading: Icon(Icons.image),
              title: Text(
                'Gallery',
                style: TextStyle(
                    color: AppColors.textBlack, fontFamily: AppFonts.inter),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              Get.back(result: ImageSource.camera);
            },
            child: const ListTile(
              leading: Icon(Icons.photo_camera),
              title: Text(
                'Camera',
                style: TextStyle(
                  color: AppColors.textBlack,
                  fontFamily: AppFonts.inter,
                ),
              ),
            ),
          ),
        ],
      ),
    );
    return selectedSource;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: CustomNavBarWidget(),
      appBar: AppBar(
        leading: IconButton(
          onPressed: () => Get.back(),
          icon: Icon(
            Icons.arrow_back_ios,
            color: Colors.black,
          ),
        ),
        title: Row(
          children: [
            Container(
              padding: const EdgeInsets.only(top: 20),
              height: 90,
              width: 80,
              decoration: BoxDecoration(
                  color: AppColors.brownColor,
                  boxShadow: [
                    BoxShadow(
                        blurRadius: 2.0,
                        spreadRadius: 1.0,
                        color: Colors.grey.withOpacity(0.2))
                  ],
                  image: DecorationImage(
                      image: AssetImage("assets/images/profile_messeger.png"),
                      fit: BoxFit.contain)),
            ),
            SizedBox(
              width: 3.w,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  AppStrings.rupertLandstroom,
                  style: blackSixHundredWeightColor,
                ),
                SizedBox(
                  height: 1.h,
                ),
                Text(
                  AppStrings.onlineNow,
                  style: grey9Color,
                ),
              ],
            )
          ],
        ),
        backgroundColor: Colors.white,
        elevation: 10,
        toolbarHeight: 15.h,
      ),
      floatingActionButton: WidgetMethod.floatingActionButton(context),
      body: Padding(
        padding: EdgeInsets.only(
          left: 2.w,
          right: 2.w,
        ),
        child: ListView(
          children: [
            SizedBox(
              height: 3.h,
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 2.w),
              child: Container(
                padding: EdgeInsets.only(left: 4.w, right: 4.w, top: 2.h),
                height: 115.h,
                width: 100.w,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.transparent,
                    border: Border.all(
                        color: Colors.grey,
                        width: 1,
                        style: BorderStyle.solid)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(
                          AppStrings.housePaint,
                          style: blue600ColorTextStyle,
                        ),
                        Icon(
                          Icons.favorite,
                          color: Colors.red,
                          size: 3.h,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    Row(
                      children: [
                        SvgPicture.asset(
                          'assets/images/post_icon/calender_tick_icon.svg',
                          height: 3.h,
                        ),
                        SizedBox(
                          width: 1.w,
                        ),
                        const Text(
                          AppStrings.asap,
                          style: black14Mulish1TextStyle,
                        ),
                        SizedBox(
                          width: 2.w,
                        ),
                        SvgPicture.asset(
                          'assets/images/post_icon/visible_grey.svg',
                          height: 2.h,
                        ),
                        SizedBox(
                          width: 2.w,
                        ),
                        const Text(
                          AppStrings.five,
                          style: black14Mulish1TextStyle,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    const Text(
                      AppStrings.address,
                      style: blue700MulishTextStyle,
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    const Text(
                      AppStrings.southPort,
                      style: black400MulishTextStyle,
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    const Text(
                      AppStrings.jobValueSmall,
                      style: blue700MulishTextStyle,
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    const Text(
                      AppStrings.eightHundred,
                      style: greenMulish1TextStyle,
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    const Text(
                      AppStrings.jobDetail,
                      style: black18MulishTextStyle,
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    const Text(
                      AppStrings.iNeedPaimter,
                      style: black14Mulish1TextStyle,
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    Obx(() => Expanded(
                        child: ListView.builder(
                            itemCount: controller.messageTextShowList.length,
                            itemBuilder: (BuildContext context, int index) {
                              // DateTime d = controller
                              //     .messageTextShowList[index].date!
                              //     .toDate();
                              return Column(
                                children: [
                                  FirebaseAuthorization
                                              .firebaseAuth.currentUser!.uid !=
                                          controller
                                              .messageTextShowList[index].uid
                                      ? Column(
                                          children: [
                                            Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.end,
                                              children: [
                                                Container(
                                                  height: 5.h,
                                                  width: 5.h,
                                                  decoration:
                                                      const BoxDecoration(
                                                          shape:
                                                              BoxShape.circle,
                                                          image:
                                                              DecorationImage(
                                                                  image:
                                                                      AssetImage(
                                                                    'assets/images/profile_messager2.png',
                                                                  ),
                                                                  fit: BoxFit
                                                                      .cover)),
                                                ),
                                                SizedBox(
                                                  width: 1.w,
                                                ),
                                                const Text(
                                                  AppStrings.tenTwenty,
                                                  style: grey12MulishTextStyle,
                                                )
                                              ],
                                            ),
                                            Padding(
                                              padding:
                                                  EdgeInsets.only(left: 10.w),
                                              child: Container(
                                                padding: EdgeInsets.only(
                                                    left: 3.w, top: 1.h),
                                                height: 5.h,
                                                width: 70.w,
                                                decoration: const BoxDecoration(
                                                  color:
                                                      AppColors.whiteGreyColor,
                                                  borderRadius:
                                                      BorderRadius.only(
                                                          topRight:
                                                              Radius.circular(
                                                                  10),
                                                          topLeft:
                                                              Radius.circular(
                                                                  10),
                                                          bottomLeft:
                                                              Radius.circular(
                                                                  10)),
                                                ),
                                                child: Text(
                                                  controller
                                                          .messageTextShowList[
                                                              index]
                                                          .message ??
                                                      "",
                                                  style:
                                                      blackMulish300TextStyle,
                                                ),
                                              ),
                                            ),
                                          ],
                                        )
                                      : Column(
                                          children: [
                                            Padding(
                                              padding:
                                                  EdgeInsets.only(left: 10.w),
                                              child: Container(
                                                padding: EdgeInsets.only(
                                                    left: 2.w, top: 1.h),
                                                height: 5.h,
                                                width: 70.w,
                                                decoration: const BoxDecoration(
                                                  color: AppColors.blue15Color,
                                                  borderRadius:
                                                      BorderRadius.only(
                                                          topRight:
                                                              Radius.circular(
                                                                  10),
                                                          bottomRight:
                                                              Radius.circular(
                                                                  10),
                                                          bottomLeft:
                                                              Radius.circular(
                                                                  10)),
                                                ),
                                                child: Text(
                                                  controller
                                                          .messageTextShowList[
                                                              index]
                                                          .message ??
                                                      "",
                                                  style:
                                                      blackMulish300TextStyle,
                                                ),
                                              ),
                                            ),
                                            SizedBox(
                                              height: 0.5.h,
                                            ),
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.end,
                                              children: [
                                                const Text(
                                                  AppStrings.tenTwenty,
                                                  style: grey12MulishTextStyle,
                                                ),
                                                SizedBox(
                                                  width: 1.w,
                                                ),
                                                Container(
                                                  height: 5.h,
                                                  width: 5.h,
                                                  decoration:
                                                      const BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    image: DecorationImage(
                                                        image: AssetImage(
                                                            'assets/images/profile_messager3.png'),
                                                        fit: BoxFit.cover),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                  SizedBox(
                                    height: 2.h,
                                  ),
                                  Row(
                                    children: [
                                      const Expanded(flex: 1, child: Divider()),
                                      SizedBox(
                                        width: 2.w,
                                      ),
                                      const Expanded(
                                        flex: 0,
                                        child: Text(
                                          AppStrings.yesterday,
                                          style: grey12MulishTextStyle,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 2.w,
                                      ),
                                      Expanded(flex: 1, child: Divider()),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 1.h,
                                  ),
                                ],
                              );
                            }))),
                    SizedBox(
                      height: 2.h,
                    ),
                    const Divider(),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Expanded(
                          flex: 2,
                          child: TextFormField(
                            controller: controller.messageTextEditingController,
                            decoration: const InputDecoration(
                                border: InputBorder.none,
                                hintText: AppStrings.typeYourMessage,
                                hintStyle: grey12MulishTextStyle),
                          ),
                        ),
                        Expanded(
                            child: CustomButton(
                                buttonName: AppStrings.reply,
                                width: 30.w,
                                height: 3.h,
                                buttonColor: AppColors.lightBlueColor,
                                textStyle: white12MulishTextStyle,
                                borderRadius: BorderRadius.circular(4),
                                onTap: () {
                                  controller.postMessage();
                                })),
                      ],
                    ),
                    GetBuilder(
                      init: MessageController(),
                      builder: (_) {
                        return controller.messagePicUpload != null
                            ? Container(
                                height: 10.h,
                                width: 20.w,
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                  image: FileImage(
                                    File(controller.messagePicUpload!.path),
                                  ),
                                )),
                              )
                            : Container();
                      },
                    ),
                    SizedBox(
                      height: 3.h,
                    ),
                    Row(
                      children: [
                        GestureDetector(
                          onTap: () async {
                            ImageSource? imageSource = await pickImage();
                            if (imageSource == null) return;
                            controller.selectImage(imageSource);
                          },
                          child: SvgPicture.asset(
                            'assets/images/post_icon/gallery_icon.svg',
                            height: 3.h,
                          ),
                        ),
                        SizedBox(
                          width: 1.w,
                        ),
                        SvgPicture.asset(
                          'assets/images/post_icon/video_icon.svg',
                          height: 3.h,
                        ),
                        SizedBox(
                          width: 1.w,
                        ),
                        SvgPicture.asset(
                          'assets/images/post_icon/pdf_icon.svg',
                          height: 3.h,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 6.h,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CustomButton(
                            buttonName: AppStrings.rejectedJob,
                            width: 30.w,
                            height: 4.h,
                            buttonColor: AppColors.red2Color,
                            textStyle: blackMulishTextStyle,
                            borderRadius: BorderRadius.circular(5),
                            onTap: () {}),
                        const SizedBox(
                          width: 10,
                        ),
                        CustomButton(
                            buttonName: AppStrings.acceptJob,
                            width: 30.w,
                            height: 4.h,
                            buttonColor: AppColors.green12Color,
                            textStyle: blackMulishTextStyle,
                            borderRadius: BorderRadius.circular(5),
                            onTap: () => controller.acceptJobProposal()),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 3.h,
            )
          ],
        ),
      ),
    );
  }
}
