import 'package:AwesomeSkills/firebase/firebase.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/messages/active_tabBar_screen.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/messages/archived_tabBar_screen.dart';
import 'package:sizer/sizer.dart';

import 'rejected_archive_screen.dart';

class MessageTabBarWidgetScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 2.w),
      child: Container(
        padding: EdgeInsets.only(bottom: 2.h),
        height: 50.h,
        width: 100.w,
        decoration: BoxDecoration(
            color: Colors.white70,
            borderRadius: BorderRadius.circular(20),
            boxShadow: [
              BoxShadow(
                  offset: Offset(0, 5),
                  blurRadius: 4.0,
                  spreadRadius: 2.0,
                  color: Colors.grey.withOpacity(0.1)),
              BoxShadow(
                  offset: Offset(-5, 0),
                  blurRadius: 4.0,
                  spreadRadius: 2.0,
                  color: Colors.grey.withOpacity(0.1)),
              BoxShadow(
                  offset: Offset(5, 0),
                  blurRadius: 4.0,
                  spreadRadius: 2.0,
                  color: Colors.grey.withOpacity(0.1))
            ]),
        child: Column(
          children: [
            Container(
              height: 10.h,
              width: 100.w,
              decoration: const BoxDecoration(
                  color: AppColors.lightGreen2Color,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20))),
              child: const TabBar(
                indicatorWeight: 3,
                indicatorSize: TabBarIndicatorSize.label,
                labelColor: Colors.black,
                indicatorColor: AppColors.orange2Color,
                unselectedLabelStyle:
                    TextStyle(fontWeight: FontWeight.w400, color: Colors.grey),
                labelStyle: TextStyle(fontWeight: FontWeight.bold),
                tabs: [
                  Tab(
                    text: AppStrings.active,
                  ),
                  Tab(
                    text: AppStrings.archive,
                  ),
                  Tab(
                    text: AppStrings.rejected,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            Expanded(
              child: Scrollbar(
                child: TabBarView(
                    physics: NeverScrollableScrollPhysics(),
                    children: [
                      ActiveTabBarScreen(),
                      ArchivedTabBarScreen(),
                      RejectedTabBarScreen()
                    ]),
              ),
            )
          ],
        ),
      ),
    );
  }
}
