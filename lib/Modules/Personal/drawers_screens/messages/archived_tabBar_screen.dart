import 'package:flutter/material.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/messages/build_card_widget.dart';
import 'package:sizer/sizer.dart';

class ArchivedTabBarScreen extends StatelessWidget {
  const ArchivedTabBarScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15.0),
      child: Scrollbar(
        trackVisibility: true,
        thickness: 10,
        radius: Radius.circular(10),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(left: 1.w, right: 1.w),
              width: 120.w,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(
                      color: Colors.grey.withOpacity(0.2),
                      style: BorderStyle.solid,
                      width: 2)),
              child: Text(
                AppStrings.pleaseYourMessage,
                style: redColorTenTextStyle,
              ),
            ),
            Expanded(
              child: ListView.builder(
                scrollDirection: Axis.vertical,
                padding: const EdgeInsets.only(right: 20),
                itemCount: 10,
                itemBuilder: (context, int index) {
                  return BuildCardWidget(
                      imagePath: 'assets/images/profile_messeger.png',
                      textName: AppStrings.rupertLandstroom,
                      postTimingAgo: AppStrings.threeHourAgo);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
