import 'package:AwesomeSkills/Modules/Personal/drawers_screens/messages/message_controller.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/messages/message_typing_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/messages/build_card_widget.dart';
import 'package:AwesomeSkills/routes/app_pages.dart';

class ActiveTabBarScreen extends StatelessWidget {
  final messageController = Get.put(MessageController());

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0),
      child: Scrollbar(
        trackVisibility: true,
        thickness: 10,
        radius: const Radius.circular(10),
        child: Obx(
          () => ListView.builder(
            scrollDirection: Axis.vertical,
            padding: const EdgeInsets.only(right: 20),
            itemCount: messageController.messageList.length,
            itemBuilder: (context, int index) {
              DateTime d = messageController.messageList[index].date!.toDate();
              return GestureDetector(
                onTap: () {
                  messageController.getMessageFunction(
                      uid: messageController.messageList[index].documentId
                          .toString());
                },
                child: BuildCardWidget(
                  imagePath: 'assets/images/profile_messeger.png',
                  textName: "$d",
                  postTimingAgo:
                      messageController.messageList[index].documentId ?? "",
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
