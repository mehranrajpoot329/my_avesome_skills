import 'package:AwesomeSkills/network/end_points.dart';
import 'package:AwesomeSkills/network/requests.dart';
import 'package:AwesomeSkills/utils/utils.dart';
import 'package:get/get.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:AwesomeSkills/models/api_model/personal_model/my_task_model.dart';

import '../../../../../network/base_response.dart';

class MyTaskInProgressController extends GetxController {
  final itemController = ItemScrollController();
  RxBool isCardIndex = false.obs;

  RxInt itemCount = 20.obs;
  Rx<Data> taskInProgressCardData = Data().obs;

  Future scrollItemUp() async {
    itemController.jumpTo(index: 0);
    isCardIndex.value = false;
  }

  Future scrollItemDown() async {
    itemController.jumpTo(index: 20);
    isCardIndex.value = true;
  }

  Future<void> getProgressMyTask() async {
    try {
      Utils.showProgressBar();
      BaseResponse response = await DioClient().getRequest(
        endPoint: EndPoints.myInProgressTaskUrl,
      );

      if (response.error == false) {
        taskInProgressCardData.value = Data.fromJson(response.data);
        Utils.dismissProgressBar();
      } else {
        Utils.errorFlutterToast(msg: response.message.toString());
      }
      Utils.dismissProgressBar();
    } catch (e) {
      Utils.dismissProgressBar();
      Utils.errorFlutterToast(msg: e.toString());
    }
  }
}
