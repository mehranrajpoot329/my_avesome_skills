import 'package:AwesomeSkills/Modules/Personal/drawers_screens/my_task/my_task_inprogress/my_task_inprogress_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/my_task/my_task_to_start/my_task_to_start.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:sizer/sizer.dart';

import '../../../../../Constants/app_colors.dart';
import '../../../../../Constants/app_strings.dart';
import '../../../../../Constants/text_style.dart';
import '../../../../../widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import '../../../../../widgets/custom_widget_app/widget_method.dart';

class MyTaskInProgress extends StatelessWidget {
  final taskInProgress = Get.put(MyTaskInProgressController());

  @override
  Widget build(BuildContext context) {
    return NestedScrollWidget(
      floatingActionButton: Stack(
        clipBehavior: Clip.none,
        children: [
          Positioned(
            right: 0.w,
            bottom: -2.h,
            child: Obx(
              () => Row(
                children: [
                  taskInProgress.isCardIndex.value
                      ? GestureDetector(
                          onTap: () => taskInProgress.scrollItemUp(),
                          child: CircleAvatar(
                            radius: 20,
                            backgroundColor: AppColors.iconVisibleColor,
                            child: Icon(
                              Icons.arrow_upward_rounded,
                              color: Colors.white,
                            ),
                          ))
                      : GestureDetector(
                          onTap: () => taskInProgress.scrollItemDown(),
                          child: CircleAvatar(
                            radius: 20,
                            backgroundColor: AppColors.iconVisibleColor,
                            child: Icon(
                              Icons.arrow_downward_rounded,
                              color: Colors.white,
                            ),
                          )),
                  SizedBox(
                    width: 70.w,
                  ),
                  GestureDetector(
                    onTap: () => WidgetMethod.bottomSheet(context),
                    child: FittedBox(
                      child: SvgPicture.asset(
                        'assets/images/micMan.svg',
                        height: 23.sp,
                        width: 23.sp,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
      body: Padding(
        padding: EdgeInsets.only(left: 3.w, right: 2.w),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          SizedBox(
            height: 1.h,
          ),
          Row(
            children: [
              GestureDetector(
                onTap: () => Get.back(),
                child: Icon(
                  Icons.arrow_back_ios_new,
                  color: Colors.black,
                  size: 3.h,
                ),
              ),
              SizedBox(
                width: 5.w,
              ),
              const Text(
                AppStrings.myTask,
                style: montserrat20TextStyle,
              ),
            ],
          ),
          const FittedBox(
            child: Text(
              AppStrings.helloAgain,
              style: poppins36ColorTextStyle,
            ),
          ),
          Row(
            children: [
              Text(
                AppStrings.youHave,
                style: blackBold16Color,
              ),
              Obx(
                () => Text(
                  taskInProgress.taskInProgressCardData.value.count.toString(),
                  style: greenBold16Color,
                ),
              ),
              SizedBox(
                width: 1.w,
              ),
              Text(
                AppStrings.taskToFinish,
                style: blackBold16Color,
              ),
            ],
          ),
          SizedBox(
            height: 2.h,
          ),
          const Align(
            alignment: Alignment.center,
            child: Text(
              AppStrings.inProgress,
              style: orangeColor18TextStyle,
            ),
          ),
          Expanded(
            child: ScrollablePositionedList.separated(
              itemScrollController: taskInProgress.itemController,
              itemCount:
                  taskInProgress.taskInProgressCardData.value.rows!.length,
              itemBuilder: (BuildContext context, int index) {
                return taskInProgress.taskInProgressCardData.value == null
                    ? Center(
                        child: Text("Data  not found"),
                      )
                    : BuildMyTaskToStartCard(
                        isCompleted: false,
                        cardColor: AppColors.lightYellowColor,
                        textName: taskInProgress.taskInProgressCardData.value ==
                                null
                            ? ""
                            : taskInProgress.taskInProgressCardData.value
                                .rows![index].projectName
                                .toString(),
                        image: taskInProgress.taskInProgressCardData.value ==
                                null
                            ? ""
                            : "",
                        textDetail:
                            taskInProgress.taskInProgressCardData.value == null
                                ? ""
                                : taskInProgress.taskInProgressCardData.value
                                        .rows![0].projectUser ??
                                    "",
                        textDetailColor: AppColors.redColor,
                        flagImage: 'assets/images/post_icon/red_flag.svg');
              },
              separatorBuilder: (BuildContext context, int index) {
                return SizedBox(
                  height: 2.h,
                );
              },
            ),
          ),
        ]),
      ),
    );
  }
}
