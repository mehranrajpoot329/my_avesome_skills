import 'package:AwesomeSkills/Modules/Personal/drawers_screens/my_task/my_task_to_start/my_task_to_start_controller.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/Need_Help/need_help_controller.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/landing_screen_controller.dart';
import 'package:AwesomeSkills/local/db/local_storage.dart';
import 'package:AwesomeSkills/models/user_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/my_task/my_task_list_card.dart';
import 'package:AwesomeSkills/routes/app_pages.dart';
import 'package:AwesomeSkills/widgets/custom_bottom_appBar_navi/custom_bottom_appBar_navi.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:AwesomeSkills/widgets/drawer/personal/drawer_profile_overview_personal_controller.dart';

import 'package:sizer/sizer.dart';

import 'my_task_inprogress/my_task_inprogress_controller.dart';

class MyTaskScreen extends StatelessWidget {
  final appBarController = Get.put(ProfileOverviewControllerPersonal());
  final myTaskInProgress = Get.put(MyTaskInProgressController());
  final myTaskToStart = Get.put(MyTaskToStartController());

  @override
  Widget build(BuildContext context) {
    return NestedScrollWidget(
      floatingActionButton: WidgetMethod.floatingActionButton(context),
      body: Padding(
        padding: EdgeInsets.only(left: 3.w, right: 2.w),
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            SizedBox(
              height: 1.h,
            ),
            Row(
              children: [
                GestureDetector(
                  onTap: () => Get.back(),
                  child: Icon(
                    Icons.arrow_back_ios_new,
                    color: Colors.black,
                    size: 3.h,
                  ),
                ),
                SizedBox(
                  width: 5.w,
                ),
                const Text(
                  AppStrings.myTask,
                  style: montserrat20TextStyle,
                ),
              ],
            ),
            Row(
              children: [
                Text(
                  AppStrings.helloAgain,
                  style: poppins36ColorTextStyle,
                ),
                SizedBox(
                  width: 5.w,
                ),
                SizedBox(
                  width: 40.w,
                  child: Text(
                    myTaskToStart.getUserInfo.value.firstName ?? "",
                    overflow: TextOverflow.ellipsis,
                    style: poppins36ColorTextStyle,
                  ),
                ),
              ],
            ),
            Row(
              children: [
                Text(
                  AppStrings.youHave,
                  style: blackBold16Color,
                ),
                Obx(
                  () => Text(
                    myTaskToStart.taskInProgressCardCountData.value.count !=
                                0 &&
                            myTaskToStart
                                    .taskInProgressCardCountData.value.count !=
                                null
                        ? "${myTaskToStart.taskInProgressCardCountData.value.count}"
                        : "0",
                    style: greenBold16Color,
                  ),
                ),
                SizedBox(
                  width: 1.w,
                ),
                Text(
                  AppStrings.taskToFinish,
                  style: blackBold16Color,
                ),
              ],
            ),
            SizedBox(
              height: 2.h,
            ),
            const Align(
              alignment: Alignment.center,
              child: Text(
                AppStrings.toStart,
                style: logoTextStyle,
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            GestureDetector(
              onTap: () => Get.toNamed(AppPage.myTaskToStart),
              child: Obx(() => myTaskToStart.myTaskList.isEmpty
                  ? Center(
                      child: Text(
                        "No Task is Avaiable in To Start",
                        style: blackSixHundredWeightColor,
                      ),
                    )
                  : MyTaskListCard(
                      isCompleted: false,
                      cardColor: AppColors.blue13Color,
                      textName: myTaskToStart
                              .myTaskList.value[0].userData!.firstName ??
                          "",
                      image: "",
                      textDetail: "",
                      //  myTaskToStart.myTaskList.value[0].description ?? "",
                      textDetailColor: AppColors.lightBlueColor,
                      flagImage: 'assets/images/post_icon/yellow_flag.svg',
                    )),
            ),
            SizedBox(
              height: 9.h,
            ),
            const Align(
              alignment: Alignment.center,
              child: Text(
                AppStrings.inProgress,
                style: orangeColor18TextStyle,
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            GestureDetector(
              onTap: () => Get.toNamed(AppPage.myTaskInProgress),
              child: Obx(
                () => myTaskToStart.taskInProgressCardData.isNotEmpty
                    ? MyTaskListCard(
                        isCompleted: false,
                        cardColor: AppColors.lightYellowColor,
                        textName: myTaskInProgress.taskInProgressCardData.value
                                .rows![0].projectName ??
                            "",
                        image: "",
                        textDetail: myTaskInProgress.taskInProgressCardData
                                .value.rows![0].projectName ??
                            "",
                        textDetailColor: AppColors.redColor,
                        flagImage: 'assets/images/post_icon/red_flag.svg')
                    : const Center(
                        child: Text("No Task Avaiable in Progress"),
                      ),
              ),
            ),
            SizedBox(
              height: 9.h,
            ),
            const Align(
              alignment: Alignment.center,
              child: Text(
                AppStrings.completed,
                style: greenColor18TextStyle,
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            GestureDetector(
              onTap: () => Get.toNamed(AppPage.myTaskCompleted),
              child: MyTaskListCard(
                  isCompleted: true,
                  cardColor: AppColors.green8Color,
                  textName: AppStrings.releaseProject,
                  image: 'assets/images/profile_task_complete.png',
                  textDetail: AppStrings.updateContractorAgreement,
                  textDetailColor: AppColors.green7Color,
                  flagImage: 'assets/images/post_icon/red_flag.svg'),
            ),
            SizedBox(
              height: 10.h,
            ),
          ],
        ),
      ),
    );
  }
}
