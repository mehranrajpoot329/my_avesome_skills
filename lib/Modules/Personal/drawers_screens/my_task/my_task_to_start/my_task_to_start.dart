import 'package:AwesomeSkills/Modules/Personal/drawers_screens/my_task/my_task_to_start/my_task_to_start_controller.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/Need_Help/need_help_controller.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:sizer/sizer.dart';
import '../../../../../Constants/app_strings.dart';
import '../../../../../Constants/text_style.dart';

class MyTaskToStart extends StatelessWidget {
  final taskStartController = Get.put(MyTaskToStartController());

  @override
  Widget build(BuildContext context) {
    return NestedScrollWidget(
      floatingActionButton: Stack(
        clipBehavior: Clip.none,
        children: [
          Positioned(
            right: 0.w,
            bottom: -2.h,
            child: Obx(
              () => Row(
                children: [
                  taskStartController.isCardIndex.value
                      ? GestureDetector(
                          onTap: () => taskStartController.scrollItemUp(),
                          child: const CircleAvatar(
                            radius: 20,
                            backgroundColor: AppColors.iconVisibleColor,
                            child: Icon(
                              Icons.arrow_upward_rounded,
                              color: Colors.white,
                            ),
                          ))
                      : GestureDetector(
                          onTap: () => taskStartController.scrollItemDown(),
                          child: const CircleAvatar(
                            radius: 20,
                            backgroundColor: AppColors.iconVisibleColor,
                            child: Icon(
                              Icons.arrow_downward_rounded,
                              color: Colors.white,
                            ),
                          )),
                  SizedBox(
                    width: 70.w,
                  ),
                  GestureDetector(
                    onTap: () => WidgetMethod.bottomSheet(context),
                    child: FittedBox(
                      child: SvgPicture.asset(
                        'assets/images/micMan.svg',
                        height: 23.sp,
                        width: 23.sp,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
      body: Padding(
        padding: EdgeInsets.only(left: 3.w, right: 2.w),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          SizedBox(
            height: 1.h,
          ),
          Row(
            children: [
              GestureDetector(
                onTap: () => Get.back(),
                child: Icon(
                  Icons.arrow_back_ios_new,
                  color: Colors.black,
                  size: 3.h,
                ),
              ),
              SizedBox(
                width: 5.w,
              ),
              const Text(
                AppStrings.myTask,
                style: montserrat20TextStyle,
              ),
            ],
          ),
          const FittedBox(
            child: Text(
              AppStrings.helloAgain,
              style: poppins36ColorTextStyle,
            ),
          ),
          Row(
            children: [
              Text(
                AppStrings.youHave,
                style: blackBold16Color,
              ),
              Obx(
                () => Text(
                  "",

                  //   taskStartController.taskInProgressCardData.value.count != 0 &&
                  //           taskStartController
                  //                   .taskInProgressCardData.value.count !=
                  //               null
                  //       ? "${taskStartController.taskInProgressCardData.value.count}"
                  //       : "0",
                  //   style: greenBold16Color,
                ),
              ),
              SizedBox(
                width: 1.w,
              ),
              Text(
                AppStrings.taskToFinish,
                style: blackBold16Color,
              ),
            ],
          ),
          SizedBox(
            height: 2.h,
          ),
          const Align(
            alignment: Alignment.center,
            child: Text(
              AppStrings.toStart,
              style: logoTextStyle,
            ),
          ),
          Obx(
            () => Expanded(
              child: taskStartController.myTaskList.isEmpty
                  ? Center(child: const Text("No Data Found"))
                  : ScrollablePositionedList.separated(
                      itemScrollController: taskStartController.itemController,
                      itemCount: taskStartController.myTaskList.value.length,
                      itemBuilder: (BuildContext context, int index) {
                        var dataApi =
                            taskStartController.myTaskList.value[index];
                        return BuildMyTaskToStartCard(
                          isCompleted: false,
                          cardColor: AppColors.blue13Color,
                          textName: dataApi.userData!.firstName ?? "",
                          image: dataApi.images ?? "",
                          textDetail: dataApi.description ?? "",
                          textDetailColor: AppColors.lightBlueColor,
                          flagImage: 'assets/images/post_icon/yellow_flag.svg',
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) {
                        return SizedBox(
                          height: 2.h,
                        );
                      },
                    ),
            ),
          ),
        ]),
      ),
    );
  }
}

class BuildMyTaskToStartCard extends StatelessWidget {
  final needController = Get.put(NeedHelpController());
  String textName, image, flagImage, textDetail;
  Color cardColor, textDetailColor;
  bool isCompleted;

  BuildMyTaskToStartCard(
      {required this.textName,
      required this.image,
      required this.flagImage,
      required this.textDetail,
      required this.cardColor,
      required this.textDetailColor,
      required this.isCompleted});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 20, top: 20, right: 20),
      height: 170,
      width: 30,
      decoration: BoxDecoration(
          color: cardColor,
          borderRadius: BorderRadius.circular(20),
          boxShadow: [
            BoxShadow(
                offset: const Offset(0, 2),
                blurRadius: 4.0,
                spreadRadius: 2.0,
                color: Colors.grey.withOpacity(0.2)),
          ]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "$textName > Stage 1",
                style: black12MulishTextStyle,
              ),
              CachedNetworkImage(
                imageUrl: image ?? "",
                errorWidget: (context, url, error) => Container(
                  height: 9.h,
                  width: 9.h,
                  decoration: BoxDecoration(
                      color: Colors.grey.withOpacity(0.2),
                      shape: BoxShape.circle),
                  child: Center(
                    child: Icon(
                      Icons.person,
                      color: Colors.grey,
                      size: 5.h,
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            child: Text(
              textDetail,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                  color: textDetailColor,
                  fontSize: 16,
                  fontWeight: FontWeight.w500,
                  fontFamily: 'mulish'),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          isCompleted
              ? Container()
              : SvgPicture.asset(
                  flagImage,
                  height: 3.h,
                )
        ],
      ),
    );
  }
}
