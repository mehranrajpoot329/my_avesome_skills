import 'dart:developer';

import 'package:AwesomeSkills/local/db/local_storage.dart';
import 'package:AwesomeSkills/models/user_model.dart';
import 'package:AwesomeSkills/network/end_points.dart';
import 'package:AwesomeSkills/network/requests.dart';
import 'package:AwesomeSkills/utils/utils.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/models/api_model/personal_model/user_info_model.dart'
    as user_info_model;
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:AwesomeSkills/models/api_model/personal_model/my_task_model.dart'
    as my_task_model;
import 'package:AwesomeSkills/models/api_model/personal_model/get_need_help_model.dart'
    as need_help_card;
import 'package:AwesomeSkills/models/api_model/personal_model/get_can_help_model.dart'
    as can_help_card;
import '../../../../../network/base_response.dart';

class MyTaskToStartController extends GetxController {
  final itemController = ItemScrollController();
  RxBool isCardIndex = false.obs;

  Rx<user_info_model.Data> getUserInfo = user_info_model.Data().obs;
  Rx<my_task_model.Data> taskInProgressCardCountData = my_task_model.Data().obs;
  RxList<my_task_model.Rows> taskInProgressCardData =
      <my_task_model.Rows>[].obs;

  Rx<can_help_card.Data> getCanHelpCard = can_help_card.Data().obs;
  Rx<need_help_card.Data> getNeedHelpCard = need_help_card.Data().obs;

  RxList<need_help_card.Row> myTaskList = <need_help_card.Row>[].obs;

  RxInt itemCount = 20.obs;

  Future scrollItemUp() async {
    itemController.jumpTo(index: 0);
    isCardIndex.value = false;
  }

  Future scrollItemDown() async {
    itemController.jumpTo(index: 20);
    isCardIndex.value = true;
  }

  @override
  onInit() {
    super.onInit();
    Future.delayed(10.milliseconds, () {
      getNeedHelp();
    });
  }

  Future<void> getProgressMyTask() async {
    try {
      Utils.showProgressBar();
      BaseResponse response = await DioClient().getRequest(
        endPoint: EndPoints.myInProgressTaskUrl,
      );

      if (response.error == false) {
        my_task_model.Data data = my_task_model.Data.fromJson(response.data);
        taskInProgressCardCountData.value =
            my_task_model.Data.fromJson(response.data);
        if (data.rows != null && data.rows!.isNotEmpty) {
          for (var data in response.data) {
            myTaskList.add(data);
          }
        }
        Utils.dismissProgressBar();
      } else {
        Utils.errorFlutterToast(msg: response.message.toString());
      }
      Utils.dismissProgressBar();
    } catch (e) {
      Utils.dismissProgressBar();
      Utils.errorFlutterToast(msg: e.toString());
    }
  }

  Future<void> getCanHelp() async {
    try {
      Utils.showProgressBar();
      BaseResponse response = await DioClient().getRequest(
          endPoint: EndPoints.getCanHelpCardUrl,
          queryParams: {"page": "1", "limit": "6", "order": "DESC"});

      if (response.error == false) {
        can_help_card.Data baseResponse =
            can_help_card.Data.fromJson(response.data);
        getCanHelpCard.value = baseResponse;
      } else {
        Utils.errorFlutterToast(msg: response.message.toString());
      }
      Utils.dismissProgressBar();
    } catch (e) {
      Utils.dismissProgressBar();
      log('$e');
      Utils.errorFlutterToast(msg: e.toString());
    }
  }

  Future<void> getUserInfoData() async {
    try {
      print(HiveHelper.getAuthToken());
      Utils.showProgressBar();
      BaseResponse response = await DioClient().getRequest(
          endPoint: EndPoints.getUserInfo,
          headers: {"token": HiveHelper.getAuthToken()});

      if (response.error == false) {
        user_info_model.Data baseResponse =
            user_info_model.Data.fromJson(response.data);
        if (baseResponse != null) {
          getUserInfo.value = baseResponse;
        }
      } else {
        Utils.errorFlutterToast(msg: response.message.toString());
      }
      Utils.dismissProgressBar();
    } catch (e) {
      Utils.dismissProgressBar();
      log('$e');
      Utils.errorFlutterToast(msg: e.toString());
    }
  }

  Future<void> getNeedHelp() async {
    try {
      Utils.showProgressBar();
      BaseResponse response = await DioClient().getRequest(
          endPoint: EndPoints.getNeedHelpCardUrl,
          queryParams: {"page": "1", "limit": "6", "order": "DESC"});

      if (response.error == false) {
        need_help_card.Data data = need_help_card.Data.fromJson(response.data);
        if (data.row != null && data.row!.isNotEmpty) {
          var userData = getUserInfo.value.fUid;
          //    User userData = HiveHelper.getUser();
          for (var i = 0; i < data.row!.length; i++) {
            var remoteUser = data.row![i].userData;
            if (remoteUser != null) {
              if (userData == remoteUser.fUid) {
                myTaskList.value.add(data.row![i]);
              }
            }
          }
        }
        Utils.dismissProgressBar();
      } else {
        Utils.errorFlutterToast(msg: response.message.toString());
      }
      Utils.dismissProgressBar();
    } catch (e) {
      Utils.dismissProgressBar();
      Utils.errorFlutterToast(msg: e.toString());
    }
  }
}
