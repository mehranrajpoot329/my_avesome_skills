import 'package:AwesomeSkills/Modules/Personal/landing_screen/Need_Help/need_help_controller.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';

class MyTaskListCard extends StatelessWidget {
  String textName, image, flagImage, textDetail;
  Color cardColor, textDetailColor;
  bool isCompleted = true;

  MyTaskListCard(
      {required this.cardColor,
      required this.textName,
      required this.image,
      required this.textDetail,
      required this.textDetailColor,
      required this.flagImage,
      required this.isCompleted});

  @override
  Widget build(BuildContext context) {
    return Stack(
      clipBehavior: Clip.none,
      children: [
        Positioned(
            child: BuildMyTaskCard(
                isCompleted: isCompleted,
                textName: textName,
                image: image,
                flagImage: flagImage,
                textDetail: textDetail,
                cardColor: cardColor,
                textDetailColor: textDetailColor)),
        Positioned(
          top: 2.h,
          right: 4.w,
          child: BuildMyTaskCard(
              isCompleted: isCompleted,
              textName: textName,
              image: image,
              flagImage: flagImage,
              textDetail: textDetail,
              cardColor: cardColor,
              textDetailColor: textDetailColor),
        ),
        Positioned(
          top: 4.h,
          right: 8.w,
          child: BuildMyTaskCard(
              isCompleted: isCompleted,
              textName: textName,
              image: image,
              flagImage: flagImage,
              textDetail: textDetail,
              cardColor: cardColor,
              textDetailColor: textDetailColor),
        ),
        Positioned(
          top: 6.h,
          right: 12.w,
          child: BuildMyTaskCard(
              isCompleted: isCompleted,
              textName: textName,
              image: image,
              flagImage: flagImage,
              textDetail: textDetail,
              cardColor: cardColor,
              textDetailColor: textDetailColor),
        ),
      ],
    );
  }
}

class BuildMyTaskCard extends StatelessWidget {
  final needHelpController = Get.put(NeedHelpController());

  String textName, image, flagImage, textDetail;
  Color cardColor, textDetailColor;
  bool isCompleted;

  BuildMyTaskCard(
      {required this.textName,
      required this.image,
      required this.flagImage,
      required this.textDetail,
      required this.cardColor,
      required this.textDetailColor,
      required this.isCompleted});
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomRight,
      child: Padding(
        padding: const EdgeInsets.only(right: 5.0),
        child: Container(
          padding: const EdgeInsets.only(left: 20, top: 20, right: 20),
          height: 170,
          width: MediaQuery.of(context).size.width * 0.8,
          decoration: BoxDecoration(
              color: cardColor,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                    offset: const Offset(0, 5),
                    blurRadius: 4.0,
                    spreadRadius: 2.0,
                    color: Colors.grey.withOpacity(0.2)),
              ]),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    textName,
                    style: black12MulishTextStyle,
                  ),
                  CachedNetworkImage(
                    imageUrl: image ?? "",
                    placeholder: (context, url) => CircularProgressIndicator(),
                    errorWidget: (context, url, error) => Container(
                      height: 9.h,
                      width: 9.h,
                      decoration: BoxDecoration(
                          color: Colors.grey.withOpacity(0.2),
                          shape: BoxShape.circle),
                      child: Center(
                        child: Icon(
                          Icons.person,
                          color: Colors.grey,
                          size: 5.h,
                        ),
                      ),
                    ),
                  ),
                  // SizedBox(
                  //   height: 50,
                  //   width: 50,
                  //   child: image != null && image != ""
                  //       ? Image.network(image)
                  //       : Image.asset(
                  //           "assets/images/post_icon/profile_alex.png"),
                  // ),
                ],
              ),
              Text(
                textDetail,
                overflow: TextOverflow.ellipsis,
                maxLines: 2,
                style: TextStyle(
                    color: textDetailColor,
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    fontFamily: 'mulish'),
              ),
              const SizedBox(
                height: 20,
              ),
              isCompleted
                  ? Container()
                  : SvgPicture.asset(
                      flagImage,
                      height: 3.h,
                    )
            ],
          ),
        ),
      ),
    );
  }
}
