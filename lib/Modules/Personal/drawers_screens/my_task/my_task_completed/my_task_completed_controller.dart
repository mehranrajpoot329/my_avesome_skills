import 'package:get/get.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';

class MyTaskCompletedController extends GetxController {
  final itemController = ItemScrollController();
  RxBool isCardIndex = false.obs;

  RxInt itemCount = 20.obs;

  Future scrollItemUp() async {
    itemController.jumpTo(index: 0);
    isCardIndex.value = false;
  }

  Future scrollItemDown() async {
    itemController.jumpTo(index: 20);
    isCardIndex.value = true;
  }
}
