import 'package:flutter/material.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:sizer/sizer.dart';

class BuildVisitInfo extends StatelessWidget {
  num visits, newJobs, jobsInProgress, finishedJobs, recentReviews;

  BuildVisitInfo({
    required this.visits,
    required this.newJobs,
    required this.jobsInProgress,
    required this.finishedJobs,
    required this.recentReviews,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        buildCard(context: context, textName: AppStrings.visits, view: visits),
        buildCard(
          context: context,
          textName: AppStrings.newJobs,
          view: newJobs,
        ),
        buildCard(
          context: context,
          textName: AppStrings.jobInProgress,
          view: jobsInProgress,
        ),
        buildCard(
            context: context,
            textName: AppStrings.finishedJobs,
            view: finishedJobs),
        buildCard(
          context: context,
          textName: AppStrings.recentReviews,
          view: recentReviews,
        ),
        SizedBox(
          height: 2.h,
        )
      ],
    );
  }

  Widget buildCard(
      {required BuildContext context,
      required String textName,
      required num view}) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 3.w),
          height: 7.h,
          width: MediaQuery.of(context).size.width - 20,
          decoration: BoxDecoration(
              color: AppColors.lightBlue6Color,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                    color: Colors.grey.withOpacity(0.2),
                    spreadRadius: 2.0,
                    blurRadius: 4.0),
              ]),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                textName,
                style: blackSixHundredWeightColor,
              ),
              Text(
                "$view",
                style: blackSixHundredWeightColor,
              )
            ],
          ),
        ),
        SizedBox(
          height: 2.h,
        ),
      ],
    );
  }
}
