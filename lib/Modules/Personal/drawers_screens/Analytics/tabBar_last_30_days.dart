import 'package:AwesomeSkills/Modules/Personal/drawers_screens/Analytics/analytics_controller.dart';
import 'package:flutter/material.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/Analytics/build_visit_info.dart';
import 'package:get/get.dart';

import '../../../../constants/app_strings.dart';

class TabBarLast30Days extends StatelessWidget {
  final analyticsController = Get.put(AnalyticsController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => BuildVisitInfo(
        visits: analyticsController.getAnalyticsData.value.monthlyVisits ?? 0,
        newJobs: analyticsController.getAnalyticsData.value.monthNewJob ?? 0,
        jobsInProgress:
            analyticsController.getAnalyticsData.value.monthlyProgressJob ?? 0,
        finishedJobs:
            analyticsController.getAnalyticsData.value.monthlyCompletedJob ?? 0,
        recentReviews:
            analyticsController.getAnalyticsData.value.monthlyReview ?? 0,
      ),
    );
  }
}
