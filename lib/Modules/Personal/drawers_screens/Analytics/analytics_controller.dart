import 'dart:developer';

import 'package:AwesomeSkills/local/db/local_storage.dart';
import 'package:AwesomeSkills/network/end_points.dart';
import 'package:AwesomeSkills/network/requests.dart';
import 'package:AwesomeSkills/utils/utils.dart';
import 'package:get/get.dart';

import '../../../../network/base_response.dart';
import 'package:AwesomeSkills/models/api_model/personal_model/get_analysis_model.dart';

class AnalyticsController extends GetxController {
  RxBool isAnalyticsMessageVisible = false.obs;
  num? needHelpVisit, canHelpVisit;
  num? addAllVisit;

  Rx<Data> getAnalyticsData = Data().obs;

  void showAnalyticsMessage() {
    isAnalyticsMessageVisible.value = !isAnalyticsMessageVisible.value;
    update();
  }

  @override
  onInit() {
    super.onInit();
    Future.delayed(20.milliseconds, () {
      getAnalysis();
    });
  }

  Future<void> getAnalysis() async {
    try {
      print(HiveHelper.getAuthToken());
      Utils.showProgressBar();
      BaseResponse response = await DioClient().getRequest(
          endPoint: EndPoints.getAnalysisUrl,
          headers: {"token": HiveHelper.getAuthToken()});

      if (response.error == false) {
        Data baseResponse = Data.fromJson(response.data);
        getAnalyticsData.value = baseResponse;
        needHelpVisit = getAnalyticsData.value.needHelpAllVisits;
        canHelpVisit = getAnalyticsData.value.canHelpAllVisit;
        addAllVisit = needHelpVisit! + canHelpVisit!;
      } else {
        Utils.dismissProgressBar();
        Utils.errorFlutterToast(msg: response.message.toString());
      }
    } catch (e) {
      Utils.dismissProgressBar();
      log('$e');
      Utils.errorFlutterToast(msg: e.toString());
    }
  }
}
