import 'package:AwesomeSkills/Modules/Personal/drawers_screens/Analytics/analytics_controller.dart';
import 'package:flutter/material.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/Analytics/build_visit_info.dart';
import 'package:get/get.dart';

import '../../../../constants/app_strings.dart';

class TabBarWeekly extends StatelessWidget {
  final analyticsController = Get.put(AnalyticsController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => BuildVisitInfo(
        visits: analyticsController.getAnalyticsData.value.weeklyVisits == null
            ? 0
            : analyticsController.getAnalyticsData.value.weeklyVisits ?? 0,
        newJobs: analyticsController.getAnalyticsData.value.weeklyNewJob ?? 0,
        jobsInProgress:
            analyticsController.getAnalyticsData.value.weeklyProgressJob ?? 0,
        finishedJobs:
            analyticsController.getAnalyticsData.value.weeklyCompletedJob ?? 0,
        recentReviews:
            analyticsController.getAnalyticsData.value.weeklyReview ?? 0,
      ),
    );
  }
}
