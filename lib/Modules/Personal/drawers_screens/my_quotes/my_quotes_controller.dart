import 'package:get/get.dart';

class MyQuotesController extends GetxController {
  RxString quotes = ''.obs;
  RxString quotesDays = ''.obs;
  RxBool getQuotes = false.obs;

  changeSwitchCanNeedWith(bool value) {
    getQuotes.value = value;
  }
}
