import 'package:flutter/material.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/my_quotes/my_quotes_card_build.dart';

class INeedHelpTabBarScreenMyQuotes extends StatelessWidget {
  const INeedHelpTabBarScreenMyQuotes({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      itemCount: 3,
      itemBuilder: (context, int index) {
        return MyQuotesCardBuild(
          image: 'assets/images/profile_alex.png',
          personName: AppStrings.tomLatham,
          field: AppStrings.webDesigner,
          number: AppStrings.hashZeroOne,
          title: AppStrings.houseMoving,
          detail: AppStrings.toHelpWithMoving,
          price: AppStrings.eightHundred,
          date: AppStrings.twentyFiveJanuary,
        );
      },
    );
  }
}
