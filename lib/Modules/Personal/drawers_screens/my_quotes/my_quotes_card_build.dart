import 'package:flutter/material.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:sizer/sizer.dart';

class MyQuotesCardBuild extends StatelessWidget {
  String image, personName, field, number, title, detail, price, date;

  MyQuotesCardBuild({
    required this.image,
    required this.personName,
    required this.field,
    required this.number,
    required this.title,
    required this.detail,
    required this.price,
    required this.date,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
            height: 23.h,
            width: 90.w,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(15),
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey.withOpacity(0.2),
                      spreadRadius: 4.0,
                      blurRadius: 2.0)
                ]),
            child: Stack(children: [
              Positioned(
                  top: 2.h,
                  left: 5.w,
                  child: Column(
                    children: [
                      Container(
                        height: 10.h,
                        width: 10.h,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage(image), fit: BoxFit.cover)),
                      ),
                      SizedBox(
                        height: 1.h,
                      ),
                      Text(
                        personName,
                        style: black12MulishTextStyle,
                      ),
                      SizedBox(
                        height: 1.h,
                      ),
                      Text(
                        field,
                        style: grey10MulishTextStyle,
                      ),
                      SizedBox(
                        height: 1.h,
                      ),
                      Text(
                        number,
                        style: grey13MulishTextStyle,
                      )
                    ],
                  )),
              Positioned(
                  top: 4.h,
                  left: 32.w,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        title,
                        style: yellow2ColorTextStyle,
                      ),
                      SizedBox(
                        height: 1.h,
                      ),
                      SizedBox(
                        width: 50.w,
                        child: Text(
                          detail,
                          style: blackBoldPoppinsColor,
                        ),
                      )
                    ],
                  )),
              Positioned(
                top: 1.h,
                right: 5.w,
                child: Text(
                  price,
                  style: greenColorTextStyle,
                ),
              ),
              Positioned(
                  bottom: 1.h,
                  right: 5.w,
                  child: Text(
                    date,
                    style: blackMulishTextStyle,
                  )),
            ])),
        SizedBox(
          height: 3.h,
        ),
      ],
    );
  }
}
