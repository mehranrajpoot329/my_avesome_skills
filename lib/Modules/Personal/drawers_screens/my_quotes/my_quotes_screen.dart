import 'package:AwesomeSkills/Modules/Personal/drawers_screens/my_quotes/my_quotes_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/my_quotes/i_can_help_tabBar_screen_myquotes.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/my_quotes/i_need_help_tabBar_screen_myquotes.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';

import 'package:AwesomeSkills/widgets/custom_bottom_appBar_navi/custom_bottom_appBar_navi.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:sizer/sizer.dart';

class MyQuotesScreen extends StatelessWidget {
  final myQuotesController = Get.put(MyQuotesController());

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: NestedScrollWidget(
        floatingActionButton: WidgetMethod.floatingActionButton(context),
        body: Padding(
          padding: EdgeInsets.only(left: 3.w, right: 2.w),
          child: Column(children: [
            SizedBox(
              height: 1.h,
            ),
            Row(
              children: [
                GestureDetector(
                  onTap: () => Get.back(),
                  child: Icon(
                    Icons.arrow_back_ios_new,
                    color: Colors.black,
                    size: 3.h,
                  ),
                ),
                SizedBox(
                  width: 5.w,
                ),
                const Text(
                  AppStrings.myQuotes,
                  style: montserrat20TextStyle,
                ),
              ],
            ),
            SizedBox(
              height: 1.h,
            ),
            Container(
              height: 17.h,
              padding: EdgeInsets.only(left: 1.w, right: 1.w, top: 0.2.h),
              decoration: BoxDecoration(
                color: AppColors.lightGreen2Color,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 2.w, right: 2.w),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            AppStrings.myQuotes,
                            style: orange600ColorTextStyle,
                          ),
                          Obx(
                            () => Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Transform.scale(
                                  alignment: Alignment.bottomRight,
                                  scaleX: 0.6,
                                  scaleY: 0.4,
                                  child: CupertinoSwitch(
                                    trackColor: AppColors.greyColor,
                                    activeColor: AppColors.greenColor,
                                    value: myQuotesController.getQuotes.value,
                                    onChanged: (newValue) => myQuotesController
                                        .changeSwitchCanNeedWith(newValue),
                                  ),
                                ),
                                Text(
                                  myQuotesController.getQuotes.value
                                      ? AppStrings.quotesPaused
                                      : AppStrings.getQuotes,
                                  style: blackSixHundredWeightColor,
                                ),
                              ],
                            ),
                          ),
                        ]),
                  ),
                  TabBar(
                      isScrollable: true,
                      padding: EdgeInsets.zero,
                      indicatorSize: TabBarIndicatorSize.label,
                      indicatorColor: AppColors.lightBlueColor,
                      labelColor: Colors.black,
                      labelStyle: TextStyle(fontWeight: FontWeight.bold),
                      unselectedLabelStyle: TextStyle(
                          fontWeight: FontWeight.w400, color: Colors.black),
                      tabs: [
                        Tab(
                          text: AppStrings.needHelp,
                        ),
                        Tab(
                          text: AppStrings.canHelpWith,
                        ),
                      ]),
                ],
              ),
            ),
            Expanded(
              child: TabBarView(
                  physics: NeverScrollableScrollPhysics(),
                  children: [
                    INeedHelpTabBarScreenMyQuotes(),
                    ICanHelpTabBarScreenMyQuotes(),
                  ]),
            )
          ]),
        ),
      ),
    );
  }
}
