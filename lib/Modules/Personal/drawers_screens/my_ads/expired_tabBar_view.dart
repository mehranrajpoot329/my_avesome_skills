import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/my_ads/my_ads_controller.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/Need_Help/need_help_card.dart';
import 'package:sizer/sizer.dart';

import '../../../../constants/app_colors.dart';
import '../../../../constants/app_strings.dart';
import '../../../../constants/text_style.dart';

class ExpiredTabBarView extends StatelessWidget {
  final myAdsController = Get.put(MyAdsController());

  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      padding: EdgeInsets.zero,
      children: [
        Align(
          alignment: Alignment.center,
          child: ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: 4,
              itemBuilder: (context, int index) {
                return Obx(
                  () => Center(
                      child: Column(
                    children: [
                      GestureDetector(
                        onLongPress: () =>
                            myAdsController.showIsisExpiredVisible(),
                        child: NeedHelpCard(
                          initialValueOfStar: 0.0,
                          reviewStarValue: "(${AppStrings.zero})",
                          jobPrice: AppStrings.twoThreeNine,
                          title: AppStrings.housePaint,
                          userName: AppStrings.alex,
                          location: AppStrings.southPortQLD,
                          imageUrl: 'assets/images/profile_a_fernendez.png',
                          durationOfJob: AppStrings.asap,
                          onTap: () {},
                        ),
                      ),
                      Visibility(
                          visible: myAdsController.isExpire.value,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                FontAwesomeIcons.share,
                                size: 20.sp,
                              ),
                              SizedBox(
                                width: 4.w,
                              ),
                              Icon(
                                FontAwesomeIcons.edit,
                                size: 20.sp,
                              ),
                              SizedBox(
                                width: 4.w,
                              ),
                              Icon(
                                FontAwesomeIcons.trashAlt,
                                color: AppColors.red4Color,
                                size: 20.sp,
                              ),
                            ],
                          )),
                      SizedBox(
                        height: 3.h,
                      ),
                    ],
                  )),
                );
              }),
        ),
      ],
    );
  }
}

// class ExpiredTabBarView extends StatefulWidget {
//   @override
//   State<ExpiredTabBarView> createState() => _ExpiredTabBarViewState();
// }
//
// class _ExpiredTabBarViewState extends State<ExpiredTabBarView> {
//   final controller = Get.put(MyAdsController());
//
//   @override
//   Widget build(BuildContext context) {
//     return ListView(
//       shrinkWrap: true,
//       physics: NeverScrollableScrollPhysics(),
//       padding: EdgeInsets.zero,
//       children: [
//         Align(
//           alignment: Alignment.topRight,
//           child: Row(
//             mainAxisAlignment: MainAxisAlignment.end,
//             children: [
//               GestureDetector(
//                 onTap: () {
//                   if (controller.valueFirst == true &&
//                       controller.onCheck == true) {
//                     controller.pauseCardDialog(context);
//                   } else {}
//                 },
//                 child: const Text(
//                   AppStrings.repost,
//                   style: blue400ColorTextStyle,
//                 ),
//               ),
//               SizedBox(
//                 width: 2.w,
//               ),
//               GestureDetector(
//                 onTap: () {
//                   if (controller.valueFirst == true &&
//                       controller.onCheck == true) {
//                     controller.deleteCardDialog(context);
//                   } else {}
//                 },
//                 child: const Text(
//                   AppStrings.delete,
//                   style: blue1ColorTextStyle,
//                 ),
//               ),
//               SizedBox(
//                 width: 2.w,
//               ),
//               GestureDetector(
//                 onTap: () {},
//                 child: const Text(
//                   AppStrings.edit,
//                   style: blue400ColorTextStyle,
//                 ),
//               ),
//             ],
//           ),
//         ),
//         Align(
//           alignment: Alignment.center,
//           child: ListView.builder(
//               physics: NeverScrollableScrollPhysics(),
//               shrinkWrap: true,
//               itemCount: 3,
//               itemBuilder: (context, int index) {
//                 return GestureDetector(
//                     onLongPress: () {
//                       setState(() {
//                         if (controller.onCheck == false) {
//                           controller.onCheck = true;
//                         } else {
//                           controller.onCheck = false;
//                         }
//                       });
//                     },
//                     child: Stack(
//                       clipBehavior: Clip.none,
//                       children: [
//                         Center(child: NeedHelpCard(onTap: () {})),
//                         controller.onCheck
//                             ? Positioned(
//                                 top: -20,
//                                 right: 10,
//                                 child: Checkbox(
//                                   shape: const CircleBorder(),
//                                   side: const BorderSide(
//                                       color: AppColors.blueColor, width: 2),
//                                   checkColor: Colors.white,
//                                   activeColor: AppColors.lightBlueColor,
//                                   value: controller.valueFirst,
//                                   onChanged: (bool? value) {
//                                     setState(() {
//                                       controller.valueFirst = value!;
//                                     });
//                                   },
//                                 ),
//                               )
//                             : Container(),
//                       ],
//                     ));
//               }),
//         )
//       ],
//     );
//   }
// }
