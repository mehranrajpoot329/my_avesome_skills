import 'package:AwesomeSkills/routes/app_pages.dart';
import 'package:AwesomeSkills/utils/utils.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/bottom_sheet_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/my_ads/expired_tabBar_view.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/my_ads/live_tabBar_view.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/my_ads/my_ads_controller.dart';
import 'package:AwesomeSkills/widgets/custom_bottom_appBar_navi/custom_bottom_appBar_navi.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:AwesomeSkills/widgets/drawer/personal/drawer_profile_overview_personal_controller.dart';
import 'package:sizer/sizer.dart';

import 'paused_tabbar_view.dart';

class MyAdsScreen extends StatefulWidget {
  @override
  State<MyAdsScreen> createState() => _MyAdsScreenState();
}

class _MyAdsScreenState extends State<MyAdsScreen> {
  final controller = Get.put(MyAdsController());
  final appBarController = Get.put(ProfileOverviewControllerPersonal());

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: NestedScrollWidget(
          floatingActionButton: WidgetMethod.floatingActionButton(context),
          body: Padding(
            padding: EdgeInsets.only(left: 3.w, right: 2.w),
            child: Column(children: [
              SizedBox(
                height: 1.h,
              ),
              Row(
                children: [
                  GestureDetector(
                    onTap: () => Get.back(),
                    child: Icon(
                      Icons.arrow_back_ios_new,
                      color: Colors.black,
                      size: 3.h,
                    ),
                  ),
                  SizedBox(
                    width: 5.w,
                  ),
                  const Text(
                    AppStrings.myAds,
                    style: montserrat20TextStyle,
                  ),
                ],
              ),
              SizedBox(
                height: 3.h,
              ),
              Row(
                children: [
                  Expanded(
                    flex: 3,
                    child: Container(
                      padding: EdgeInsets.only(right: 2.w, top: 1.5.h),
                      height: 13.5.h,
                      width: 100.w,
                      decoration: BoxDecoration(
                        color: AppColors.lightGreen2Color,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(left: 3.w),
                            child: Text(
                              AppStrings.myAds,
                              style: orange600ColorTextStyle,
                            ),
                          ),
                          const TabBar(
                              isScrollable: true,
                              indicatorColor: AppColors.lightBlueColor,
                              labelColor: Colors.black,
                              tabs: [
                                Tab(
                                  text: AppStrings.live,
                                ),
                                Tab(
                                  text: AppStrings.paused,
                                ),
                                Tab(
                                  text: AppStrings.expired,
                                ),
                              ]),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 2.w,
                  ),
                  Container(
                    height: 13.5.h,
                    width: 30.w,
                    decoration: BoxDecoration(
                      color: AppColors.green9Color,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: MaterialButton(
                      onPressed: () =>
                          BottomSheetAdScreen.isLoginedInBottomSheet(
                              context: context,
                              needHelpTap: () =>
                                  Get.toNamed(AppPage.adScreenNeedHelpPersonal),
                              canHelpTap: () => Get.toNamed(
                                  AppPage.adScreenCanNeedHelpPersonal)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const [
                          FittedBox(
                            child: Text(
                              AppStrings.post,
                              style: white22TextStyle,
                            ),
                          ),
                          FittedBox(
                            child: Text(
                              AppStrings.anAd,
                              style: white22TextStyle,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 2.h,
              ),
              Expanded(
                child: TabBarView(
                    physics: NeverScrollableScrollPhysics(),
                    children: [
                      LiveTabBarView(),
                      PausedTabBarView(),
                      ExpiredTabBarView(),
                    ]),
              ),
            ]),
          )),
    );
  }
}
