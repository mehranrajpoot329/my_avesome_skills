import 'package:AwesomeSkills/network/end_points.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/my_ads/my_ads_controller.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/Need_Help/need_help_card.dart';
import 'package:sizer/sizer.dart';

import '../../../../constants/app_colors.dart';
import '../../../../constants/app_strings.dart';
import '../../../../constants/text_style.dart';

class LiveTabBarView extends StatelessWidget {
  final myAdsController = Get.put(MyAdsController());

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.zero,
      children: [
        Align(
          alignment: Alignment.center,
          child: Obx(
            () => myAdsController.myAdsList.isEmpty
                ? Center(
                    child: Text(
                    "No Data Found In My Ads",
                    style: blackSixHundredWeightColor,
                  ))
                : ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: myAdsController.myAdsList.value.length,
                    itemBuilder: (context, int index) {
                      return Center(
                        child: Column(
                          children: [
                            GestureDetector(
                              onLongPress: () =>
                                  myAdsController.showIsLiveVisible(),
                              child: NeedHelpCard(
                                initialValueOfStar: 0.0,
                                reviewStarValue: "(${AppStrings.zero})",
                                title: myAdsController.myAdsList[index].title
                                    .toString(),
                                jobPrice: AppStrings.twoThreeNine,
                                userName: myAdsController
                                        .myAdsList[index].userData!.firstName ??
                                    "",
                                location: myAdsController.myAdsList[index]
                                            .userData!.address ==
                                        null
                                    ? ""
                                    : myAdsController.myAdsList[index].userData!
                                            .address!.streetAddress ??
                                        "",
                                durationOfJob: myAdsController
                                            .myAdsList[index].startingTime ==
                                        null
                                    ? ""
                                    : myAdsController.myAdsList[index]
                                            .startingTime!.duration ??
                                        "",
                                imageUrl:
                                    "${EndPoints.baseURL}${myAdsController.myAdsList[index].userData!.photoURL}",
                                onTap: () {},
                              ),
                            ),
                            Visibility(
                                visible: myAdsController.isLive.value,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.pause_circle_outline,
                                      size: 24.sp,
                                    ),
                                    SizedBox(
                                      width: 4.w,
                                    ),
                                    Icon(
                                      FontAwesomeIcons.edit,
                                      size: 20.sp,
                                    ),
                                    SizedBox(
                                      width: 4.w,
                                    ),
                                    Icon(
                                      FontAwesomeIcons.trashAlt,
                                      color: AppColors.red4Color,
                                      size: 20.sp,
                                    ),
                                  ],
                                )),
                            SizedBox(
                              height: 3.h,
                            ),
                          ],
                        ),
                      );
                    }),
          ),
        ),
      ],
    );
  }
}
