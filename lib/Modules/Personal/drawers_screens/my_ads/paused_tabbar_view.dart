import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/my_ads/my_ads_controller.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/Need_Help/need_help_card.dart';
import 'package:sizer/sizer.dart';

import '../../../../constants/app_colors.dart';
import '../../../../constants/app_strings.dart';
import '../../../../constants/text_style.dart';

class PausedTabBarView extends StatelessWidget {
  final myAdsController = Get.put(MyAdsController());

  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      padding: EdgeInsets.zero,
      children: [
        Align(
          alignment: Alignment.center,
          child: ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: 4,
              itemBuilder: (context, int index) {
                return Obx(
                  () => Center(
                      child: Column(
                    children: [
                      GestureDetector(
                        onLongPress: () =>
                            myAdsController.showIsPausedVisible(),
                        child: NeedHelpCard(
                          initialValueOfStar: 0.0,
                          reviewStarValue: "(${AppStrings.zero})",
                          title: AppStrings.housePaint,
                          jobPrice: AppStrings.twoThreeNine,
                          userName: AppStrings.alex,
                          location: AppStrings.southPortQLD,
                          imageUrl: 'assets/images/profile_a_fernendez.png',
                          durationOfJob: AppStrings.asap,
                          onTap: () {},
                        ),
                      ),
                      Visibility(
                          visible: myAdsController.isPaused.value,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.play_circle_outline,
                                size: 24.sp,
                              ),
                              SizedBox(
                                width: 4.w,
                              ),
                              Icon(
                                FontAwesomeIcons.edit,
                                size: 20.sp,
                              ),
                              SizedBox(
                                width: 4.w,
                              ),
                              Icon(
                                FontAwesomeIcons.trashAlt,
                                color: AppColors.red4Color,
                                size: 20.sp,
                              ),
                            ],
                          )),
                      SizedBox(
                        height: 3.h,
                      ),
                    ],
                  )),
                );
              }),
        ),
      ],
    );
  }
}
