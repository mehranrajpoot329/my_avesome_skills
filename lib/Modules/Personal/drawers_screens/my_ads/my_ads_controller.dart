import 'dart:developer';

import 'package:AwesomeSkills/local/db/local_storage.dart';
import 'package:AwesomeSkills/models/user_model.dart';
import 'package:AwesomeSkills/network/base_response.dart';
import 'package:AwesomeSkills/network/requests.dart';
import 'package:AwesomeSkills/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/models/api_model/personal_model/get_need_help_model.dart'
    as need_help_card;

import '../../../../network/end_points.dart';

class MyAdsController extends GetxController {
  RxBool isLive = false.obs;
  RxBool isPaused = false.obs;
  RxBool isExpire = false.obs;

  RxList<need_help_card.Row> myAdsList = <need_help_card.Row>[].obs;

  showIsLiveVisible() {
    isLive.value = !isLive.value;
  }

  showIsPausedVisible() {
    isPaused.value = !isPaused.value;
  }

  showIsisExpiredVisible() {
    isExpire.value = !isExpire.value;
  }

  @override
  onInit() {
    Future.delayed(10.milliseconds, () {
      getNeedHelp();
    });
    super.onInit();
  }

  bool valueFirst = false;

  Future<void> getNeedHelp() async {
    try {
      Utils.showProgressBar();
      BaseResponse response = await DioClient().getRequest(
          endPoint: EndPoints.getNeedHelpCardUrl,
          queryParams: {"page": "1", "limit": "6", "order": "DESC"});

      if (response.error == false) {
        need_help_card.Data data = need_help_card.Data.fromJson(response.data);
        if (data.row != null && data.row!.isNotEmpty) {
          User userdata = HiveHelper.getUser();
          for (var i = 0; i < data.row!.length; i++) {
            var remoteUser = data.row![i].userData;
            if (remoteUser != null) {
              if (userdata.firebaseUId == remoteUser.fUid) {
                myAdsList.value.add(data.row![i]);
              }
            }
          }
        }
        Utils.dismissProgressBar();
      } else {
        Utils.errorFlutterToast(msg: response.message.toString());
      }
      Utils.dismissProgressBar();
    } catch (e) {
      Utils.dismissProgressBar();
      Utils.errorFlutterToast(msg: e.toString());
    }
  }
}
