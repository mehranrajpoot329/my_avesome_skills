import 'dart:io';

import 'package:AwesomeSkills/constants/app_fonts.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/settings/my_profile_setting/my_profile_controller.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:AwesomeSkills/widgets/button/custom_icon_button.dart';
import 'package:image_picker/image_picker.dart';
import 'package:multiple_images_picker/multiple_images_picker.dart';
import 'package:sizer/sizer.dart';

class ProfileSettingBodyExpansionTile extends StatelessWidget {
  ProfileSettingBodyExpansionTile({Key? key}) : super(key: key);

  final profileController = Get.put(MyProfileSettingController());

  Future<ImageSource?> pickImage() async {
    ImageSource? selectedSource = await Get.defaultDialog(
      title: 'Select From',
      content: Column(
        children: [
          GestureDetector(
            onTap: () {
              Get.back(result: ImageSource.gallery);
            },
            child: const ListTile(
              leading: Icon(Icons.image),
              title: Text(
                'Gallery',
                style: TextStyle(
                    color: AppColors.textBlack, fontFamily: AppFonts.inter),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              Get.back(result: ImageSource.camera);
            },
            child: const ListTile(
              leading: Icon(Icons.photo_camera),
              title: Text(
                'Camera',
                style: TextStyle(
                  color: AppColors.textBlack,
                  fontFamily: AppFonts.inter,
                ),
              ),
            ),
          ),
        ],
      ),
    );
    return selectedSource;
  }

  @override
  Widget build(BuildContext context) {
    return FormBuilder(
      key: profileController.profileFormKey,
      child: Column(
        children: [
          ExpansionTile(
            title: Row(
              children: [
                Text(
                  AppStrings.profilePicture,
                  style: lightBlue500TextStyle,
                ),
                SizedBox(
                  width: 2.w,
                ),
                const Text(
                  AppStrings.optional,
                  style: grey14Color,
                ),
              ],
            ),
            children: [
              const ListTile(
                title: Text(AppStrings.uploadOrChange),
              ),
              SizedBox(
                height: 1.h,
              ),
              GetBuilder(
                init: MyProfileSettingController(),
                builder: (_) {
                  return profileController.projectImage != null
                      ? Center(
                          child: Container(
                            height: 20.h,
                            width: 40.w,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  image: FileImage(
                                    File(profileController.projectImage!.path),
                                  ),
                                )),
                          ),
                        )
                      : Container();
                },
              ),
              SizedBox(
                height: 2.h,
              ),
              GestureDetector(
                onTap: () async {
                  ImageSource? imageSource = await pickImage();
                  if (imageSource == null) return;
                  profileController.selectImage(imageSource);
                },
                child: DottedBorder(
                  dashPattern: const [6, 2],
                  radius: const Radius.circular(10),
                  color: AppColors.indigoColor,
                  strokeWidth: 1.5,
                  child: Container(
                    height: 17.h,
                    width: 90.w,
                    decoration: const BoxDecoration(
                      color: Colors.transparent,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SvgPicture.asset(
                          'assets/images/post_icon/upload_icon.svg',
                        ),
                        SizedBox(
                          height: 0.2.h,
                        ),
                        Text(
                          AppStrings.dragAndDrop,
                          style: blackBoldPoppinsColor,
                        ),
                        SizedBox(
                          height: 0.2.h,
                        ),
                        Text(
                          AppStrings.or,
                          style: blackBoldPoppinsColor,
                        ),
                        SizedBox(
                          height: 0.2.h,
                        ),
                        const Text(
                          AppStrings.clickToBrowse,
                          style: yellowPoppins300TextStyle,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 2.h,
              )
            ],
          ),
          ExpansionTile(
            title: Row(
              children: [
                Text(
                  AppStrings.star,
                  style: red4ColorTextStyle,
                ),
                Text(
                  AppStrings.aboutMe,
                  style: lightBlue500TextStyle,
                ),
              ],
            ),
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 5.w),
                child: SizedBox(
                  height: 13.h,
                  child: TextFormField(
                    controller: profileController.aboutMeController,
                    maxLines: 5,
                    maxLength: 500,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: AppColors.lightBlue6Color,
                      border: OutlineInputBorder(
                          borderSide: BorderSide(
                              style: BorderStyle.solid,
                              color: Colors.grey.withOpacity(0.2),
                              width: 5.w)),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
            ],
          ),
          ExpansionTile(
            title: Row(
              children: [
                Text(
                  AppStrings.portfolio,
                  style: lightBlue500TextStyle,
                ),
                SizedBox(
                  width: 2.w,
                ),
                const Text(
                  AppStrings.optional,
                  style: grey14Color,
                ),
              ],
            ),
            children: [
              const ListTile(
                title: Text(
                  AppStrings.uploadPhotoForPastJob,
                  style: blackPoppins300TextStyle,
                ),
              ),
              SizedBox(
                height: 1.h,
              ),
              GestureDetector(
                onTap: () => profileController.loadAssets(),
                child: DottedBorder(
                  dashPattern: const [6, 2],
                  radius: const Radius.circular(10),
                  color: AppColors.indigoColor,
                  strokeWidth: 1.5,
                  child: Container(
                    height: 10.h,
                    width: 20.w,
                    decoration: const BoxDecoration(
                      color: Colors.transparent,
                    ),
                    child: Stack(
                      clipBehavior: Clip.none,
                      children: [
                        Positioned(
                          top: 1.h,
                          left: 5.w,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.add,
                                color: AppColors.indigoColor,
                                size: 3.h,
                              ),
                              SizedBox(
                                height: 2.h,
                              ),
                              Text(
                                AppStrings.add,
                                style: indigo14ColorTextStyle,
                              ),
                            ],
                          ),
                        ),
                        Positioned(
                          bottom: -3,
                          right: -6.0,
                          child: SvgPicture.asset(
                            'assets/images/post_icon/edit_icon.svg',
                            height: 13.sp,
                            width: 13.sp,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 1.h,
              ),
              GetBuilder(
                  init: MyProfileSettingController(),
                  builder: (_) {
                    return profileController.imagesList.isNotEmpty
                        ? SizedBox(
                            height: profileController.imagesList.length <= 3
                                ? 20.h
                                : profileController.imagesList.length <= 4
                                    ? 40.h
                                    : profileController.imagesList.length <= 6
                                        ? 60.h
                                        : 60.h,
                            width: Get.width,
                            child: GridView.count(
                              physics: const NeverScrollableScrollPhysics(),
                              crossAxisCount: 3,
                              crossAxisSpacing: 10,
                              mainAxisSpacing: 10,
                              children: List.generate(
                                  profileController.imagesList.length, (index) {
                                Asset asset =
                                    profileController.imagesList[index];
                                return AssetThumb(
                                  asset: asset,
                                  width: 300,
                                  height: 300,
                                );
                              }),
                            ))
                        : const SizedBox();
                  }),
              SizedBox(
                height: 2.h,
              )
            ],
          ),
          ExpansionTile(
            title: Row(
              children: [
                Text(
                  AppStrings.resume,
                  style: lightBlue500TextStyle,
                ),
                const SizedBox(
                  width: 10,
                ),
                const Text(
                  AppStrings.optional,
                  style: grey14Color,
                ),
              ],
            ),
            children: [
              const ListTile(
                title: Text(
                  AppStrings.uploadYourResume,
                  style: blackPoppins300TextStyle,
                ),
              ),
              SizedBox(
                height: 1.h,
              ),
              GetBuilder(
                init: MyProfileSettingController(),
                builder: (_) {
                  return GestureDetector(
                    onTap: () => profileController.resumePdfDocument(),
                    child: DottedBorder(
                      dashPattern: const [6, 2],
                      radius: const Radius.circular(10),
                      color: AppColors.indigoColor,
                      strokeWidth: 1.5,
                      child: Container(
                        height: 17.h,
                        width: 90.w,
                        decoration: const BoxDecoration(
                          color: Colors.transparent,
                        ),
                        child: profileController.resumePdf == null
                            ? Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  SvgPicture.asset(
                                    'assets/images/post_icon/upload_icon.svg',
                                  ),
                                  SizedBox(
                                    height: 0.2.h,
                                  ),
                                  Text(
                                    AppStrings.dragAndDrop,
                                    style: blackBoldPoppinsColor,
                                  ),
                                  SizedBox(
                                    height: 0.2.h,
                                  ),
                                  Text(
                                    AppStrings.or,
                                    style: blackBoldPoppinsColor,
                                  ),
                                  SizedBox(
                                    height: 0.2.h,
                                  ),
                                  const Text(
                                    AppStrings.clickToBrowse,
                                    style: yellowPoppins300TextStyle,
                                  ),
                                ],
                              )
                            : Padding(
                                padding:
                                    EdgeInsets.only(left: 10.w, right: 10.w),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.upload_file_outlined,
                                      color: AppColors.lightBlueColor,
                                      size: 5.h,
                                    ),
                                    SizedBox(
                                      height: 1.h,
                                    ),
                                    Text(
                                      profileController.resumePdf!.path
                                          .split("/")
                                          .last,
                                      style: blackSixHundredWeightColor,
                                    )
                                  ],
                                ),
                              ),
                      ),
                    ),
                  );
                },
              ),
              SizedBox(
                height: 2.h,
              )
            ],
          ),
          ExpansionTile(
            title: Row(
              children: [
                Text(
                  AppStrings.education,
                  style: lightBlue500TextStyle,
                ),
                SizedBox(
                  width: 2.w,
                ),
                const Text(
                  AppStrings.optional,
                  style: grey14Color,
                ),
              ],
            ),
            children: [
              ListTile(
                title: Text(
                  AppStrings.whatLevelOfEducation,
                  style: blackPoppins300TextStyle,
                ),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 2.h,
                    ),
                    SizedBox(
                      height: 5.h,
                      child: TextFormField(
                        controller: profileController.educationController,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: AppColors.lightBlue6Color,
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5)),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    CustomIconButton(
                        buttonName: AppStrings.addMore,
                        width: 35.w,
                        height: 6.h,
                        buttonColor: AppColors.indigoColor,
                        textStyle: customButtonTextStyle,
                        borderRadius: BorderRadius.circular(10),
                        onTap: () {}),
                  ],
                ),
              ),
              SizedBox(
                height: 1.h,
              ),
            ],
          ),
          ExpansionTile(
            title: Row(
              children: [
                Text(
                  AppStrings.awards,
                  style: lightBlue500TextStyle,
                ),
                SizedBox(
                  width: 1.w,
                ),
                const Text(
                  AppStrings.optional,
                  style: grey14Color,
                ),
              ],
            ),
            children: [
              ListTile(
                title: Text(
                  AppStrings.listYourLifeAchievement,
                  style: blackPoppins300TextStyle,
                ),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 2.h,
                    ),
                    SizedBox(
                      height: 5.h,
                      child: TextFormField(
                        controller: profileController.awardsController,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: AppColors.lightBlue6Color,
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5)),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    CustomIconButton(
                        buttonName: AppStrings.addMore,
                        width: 35.w,
                        height: 6.h,
                        buttonColor: AppColors.indigoColor,
                        textStyle: customButtonTextStyle,
                        borderRadius: BorderRadius.circular(10),
                        onTap: () {}),
                  ],
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
            ],
          ),
          ExpansionTile(
            title: Row(
              children: [
                Text(
                  AppStrings.star,
                  style: red4ColorTextStyle,
                ),
                Text(
                  AppStrings.contact,
                  style: lightBlue500TextStyle,
                ),
              ],
            ),
            children: [
              ListTile(
                title: const Text(
                  AppStrings.insertYourContact,
                  style: blackPoppins300TextStyle,
                ),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 2.h,
                    ),
                    Row(
                      children: [
                        const Expanded(
                          child: Text(
                            AppStrings.address,
                            style: black9PoppinsColor,
                          ),
                        ),
                        SizedBox(
                          width: 3.w,
                        ),
                        SizedBox(
                          height: 5.h,
                          width: 60.w,
                          child: Padding(
                            padding: EdgeInsets.only(right: 4.w),
                            child: TextFormField(
                              controller: profileController.streetController,
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: AppColors.lightBlue6Color,
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5)),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    Row(
                      children: [
                        const Expanded(
                          child: Text(
                            AppStrings.mobile,
                            style: black9PoppinsColor,
                          ),
                        ),
                        SizedBox(
                          width: 2.w,
                        ),
                        SizedBox(
                          height: 5.h,
                          width: 60.w,
                          child: Padding(
                            padding: EdgeInsets.only(right: 4.w),
                            child: TextFormField(
                              controller: profileController.phoneController,
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: AppColors.lightBlue6Color,
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5)),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    Row(
                      children: [
                        const Expanded(
                          flex: 1,
                          child: Text(
                            AppStrings.homePhone,
                            style: black9PoppinsColor,
                          ),
                        ),
                        SizedBox(
                          height: 5.h,
                          width: 60.w,
                          child: Padding(
                            padding: EdgeInsets.only(right: 4.w),
                            child: TextFormField(
                              controller: profileController.homePhoneController,
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: AppColors.lightBlue6Color,
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5)),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    Row(
                      children: [
                        const Expanded(
                          child: Text(
                            AppStrings.emailDash,
                            style: black9PoppinsColor,
                          ),
                        ),
                        SizedBox(
                          height: 5.h,
                          width: 60.w,
                          child: Padding(
                            padding: EdgeInsets.only(right: 4.w),
                            child: TextFormField(
                              controller: profileController.emailController,
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: AppColors.lightBlue6Color,
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5)),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    Row(
                      children: [
                        const Expanded(
                          child: Text(
                            AppStrings.website,
                            style: black9PoppinsColor,
                          ),
                        ),
                        SizedBox(
                          height: 5.h,
                          width: 60.w,
                          child: Padding(
                            padding: EdgeInsets.only(right: 4.w),
                            child: TextFormField(
                              controller: profileController.websiteController,
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: AppColors.lightBlue6Color,
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5)),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 3.h,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        CustomButton(
                          height: 5.h,
                          width: 22.w,
                          buttonName: AppStrings.save,
                          borderRadius: BorderRadius.circular(3),
                          buttonColor: AppColors.lightBlueColor,
                          textStyle: blackPoppins300TextStyle,
                          onTap: () => profileController.profileUpdate(),
                        ),
                        SizedBox(
                          width: 20.w,
                          child: Row(
                            children: [
                              SvgPicture.asset(
                                'assets/images/post_icon/edit_icon.svg',
                                height: 3.h,
                                fit: BoxFit.fitHeight,
                              ),
                              const Text(
                                AppStrings.edit,
                                style: indigoColorTextStyle,
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                  ],
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
