import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/settings/my_profile_setting/my_profile_controller.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/settings/my_profile_setting/profile_setting_body_expansion_tile.dart';
import 'package:AwesomeSkills/widgets/custom_bottom_appBar_navi/custom_bottom_appBar_navi.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:sizer/sizer.dart';

class MyProfileSetting extends StatelessWidget {
  MyProfileSetting({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NestedScrollWidget(
      floatingActionButton: WidgetMethod.floatingActionButton(context),
      body: Padding(
          padding: EdgeInsets.only(left: 3.w, right: 2.w),
          child: SingleChildScrollView(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                SizedBox(
                  height: 1.h,
                ),
                Row(
                  children: [
                    GestureDetector(
                      onTap: () => Get.back(),
                      child: Icon(
                        Icons.arrow_back_ios_new,
                        color: Colors.black,
                        size: 3.h,
                      ),
                    ),
                    SizedBox(
                      width: 5.w,
                    ),
                    const Text(
                      AppStrings.settings,
                      style: montserrat20TextStyle,
                    ),
                  ],
                ),
                SizedBox(
                  height: 2.h,
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 4.w, vertical: 2.h),
                  height: 10.h,
                  width: 100.w,
                  decoration: BoxDecoration(
                    color: AppColors.lightGreen2Color,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: const Text(
                    AppStrings.profileSettings,
                    style: orangeColor600TextStyle,
                  ),
                ),
                SizedBox(
                  height: 2.h,
                ),
                ProfileSettingBodyExpansionTile(),
                SizedBox(
                  height: 2.h,
                ),
              ]))),
    );
  }
}
