import 'dart:io';

import 'package:AwesomeSkills/local/db/local_storage.dart';
import 'package:AwesomeSkills/network/end_points.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:multiple_images_picker/multiple_images_picker.dart';
import '../../../../../utils/utils.dart';
import 'package:http/http.dart' as http;

class MyProfileSettingController extends GetxController {
  TextEditingController aboutMeController = TextEditingController();
  TextEditingController educationController = TextEditingController();
  TextEditingController awardsController = TextEditingController();
  TextEditingController streetController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController homePhoneController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController websiteController = TextEditingController();
  CroppedFile? projectImage;
  final profileFormKey = GlobalKey<FormBuilderState>();

  File? resumePdf;

  Future<void> selectImage(ImageSource selectedSource) async {
    projectImage = await Utils.getImage(source: selectedSource);
    update();
  }

  Future<void> resumePdfDocument() async {
    FilePickerResult? pdfFile = await FilePicker.platform.pickFiles();
    resumePdf = File(pdfFile!.files.single.path!);
    update();
  }

  final ImagePicker imagePicker = ImagePicker();
  List<Asset> imagesList = <Asset>[];

  Future<void> loadAssets() async {
    List<Asset> resultList = <Asset>[];
    String error = 'No Error Detected';

    try {
      resultList = await MultipleImagesPicker.pickImages(
        maxImages: 300,
        enableCamera: true,
        selectedAssets: imagesList,
        cupertinoOptions: const CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: const MaterialOptions(
          actionBarColor: "#abcdef",
          actionBarTitle: "Example App",
          allViewTitle: "All Photos",
          useDetailsView: false,
          selectCircleStrokeColor: "#000000",
        ),
      );
    } on Exception catch (e) {
      error = e.toString();
    }
    imagesList = resultList;
    update();
  }

  Future<void> profileUpdate() async {
    if (profileFormKey.currentState!.saveAndValidate()) {
      var headers = {'Authorization': 'Bearer ${HiveHelper.getAuthToken()}'};
      var request = http.MultipartRequest('POST',
          Uri.parse("${EndPoints.baseURL}${EndPoints.userprofilePostUrl}"));
      request.fields.addAll({
        'streetAddress': streetController.text.toString(),
        'phone': phoneController.text.toString(),
        'home_phone': homePhoneController.text.toString(),
        'email': emailController.text.toString(),
        'website': websiteController.text.toString(),
        'educationLevel': educationController.text.toString(),
        'achievements': awardsController.text.toString(),
        'description': aboutMeController.text.toString(),
      });
      request.files.add(await http.MultipartFile.fromPath(
          'profile_img', projectImage.toString()));
      // request.files.add(await http.MultipartFile.fromPath(
      //     'portfolio', "${imagesList.length}"));
      // request.files
      //     .add(await http.MultipartFile.fromPath('resume', '$resumePdf'));
      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
      } else {
        print(response.reasonPhrase);
      }
    }
  }
}
