import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:sizer/sizer.dart';

class JobAlertWidget extends StatelessWidget {
  bool isIcon;
  String textName;

  JobAlertWidget({
    required this.isIcon,
    required this.textName,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 10.h,
      width: 90.w,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
              offset: Offset(0, 3),
              blurRadius: 0.5,
              spreadRadius: 0.5,
              color: Colors.grey..withOpacity(0.1))
        ],
        border: Border.all(
            color: Colors.grey, width: 0.5, style: BorderStyle.solid),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              isIcon
                  ? Image.asset(
                      'assets/images/speaker_icon.png',
                      height: 3.h,
                    )
                  : const SizedBox(),
              const SizedBox(
                width: 10,
              ),
              Text(
                textName,
                style: blackBold16Color,
              )
            ],
          )
        ],
      ),
    );
  }
}
