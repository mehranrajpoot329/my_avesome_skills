import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/settings/alert_notification_settings/alert_notification_controller.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/settings/alert_notification_settings/email_notofication_widget.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/settings/alert_notification_settings/extra_notication_widget.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/settings/alert_notification_settings/job_alert_widget.dart';
import 'package:sizer/sizer.dart';

class NeedHelpWithTabBar extends StatelessWidget {
  final alertNotificationController = Get.put(AlertNotificationController());

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        left: 2.w,
        right: 2.w,
      ),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 1.h,
            ),
            const FittedBox(
              child: Text(
                AppStrings.emailNotificationSettings,
                style: blue22ColorTextStyle,
              ),
            ),
            FittedBox(
              child: Text(
                AppStrings.howWouldYouLike,
                style: black14Color,
              ),
            ),
            SizedBox(
              height: 1.h,
            ),
            Center(
              child: GetBuilder<AlertNotificationController>(builder: (_) {
                return EmailNotificationWidget(
                    checkBoxValue: alertNotificationController.emailCheckBox,
                    onChangedCheckBox: (value) =>
                        alertNotificationController.changeCheckBoxEmail(value),
                    switchValue: alertNotificationController.emailSwitch,
                    onChanged: (newValue) =>
                        alertNotificationController.changeSwitchEmail(newValue),
                    imagePath: 'assets/images/post_icon/email_icon.svg',
                    textName: AppStrings.emailAddress,
                    textName2: AppStrings.emailName);
              }),
            ),
            SizedBox(
              height: 4.h,
            ),
            Center(
              child: GetBuilder<AlertNotificationController>(builder: (_) {
                return EmailNotificationWidget(
                    checkBoxValue: alertNotificationController.smsCheckBox,
                    onChangedCheckBox: (value) =>
                        alertNotificationController.changeCheckBoxSms(value),
                    switchValue: alertNotificationController.smsSwitch,
                    onChanged: (newValue) =>
                        alertNotificationController.changeSwitchSMS(newValue),
                    imagePath: 'assets/images/post_icon/mobile_icon2.svg',
                    textName: AppStrings.sms,
                    textName2: AppStrings.phoneNumber);
              }),
            ),
            SizedBox(
              height: 2.h,
            ),
            const Text(
              AppStrings.jobAlert,
              style: blue22ColorTextStyle,
            ),
            FittedBox(
              child: Text(
                AppStrings.getNotified,
                style: black14Color,
              ),
            ),
            SizedBox(
              height: 1.h,
            ),
            Center(
              child: JobAlertWidget(
                isIcon: true,
                textName: AppStrings.addKeywords,
              ),
            ),
            SizedBox(
              height: 3.h,
            ),
            Center(
              child: JobAlertWidget(
                isIcon: false,
                textName: AppStrings.chooseKMSRadius,
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            const Text(
              AppStrings.extraNotification,
              style: blue22ColorTextStyle,
            ),
            FittedBox(
              child: Text(
                AppStrings.chooseExtraNotifications,
                style: black14Color,
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            GetBuilder<AlertNotificationController>(builder: (_) {
              return ExtraNotification(
                  switchValue: alertNotificationController.needHelp,
                  onChanged: (newValue) => alertNotificationController
                      .changeSwitchNeedHelp(newValue),
                  textName: AppStrings.needHelp,
                  color: AppColors.blue17Color);
            }),
            SizedBox(
              height: 2.h,
            ),
            GetBuilder<AlertNotificationController>(builder: (_) {
              return ExtraNotification(
                  switchValue: alertNotificationController.canHelp,
                  onChanged: (newValue) => alertNotificationController
                      .changeSwitchCanNeedWith(newValue),
                  textName: AppStrings.canHelpWith,
                  color: AppColors.yellow5Color);
            }),
            SizedBox(
              height: 2.h,
            ),
            GetBuilder<AlertNotificationController>(builder: (_) {
              return ExtraNotification(
                  switchValue: alertNotificationController.quotes,
                  onChanged: (newValue) =>
                      alertNotificationController.changeSwitchQuotes(newValue),
                  textName: AppStrings.quotesPlusReview,
                  color: AppColors.yellow5Color);
            }),
            SizedBox(
              height: 2.h,
            ),
            GetBuilder<AlertNotificationController>(builder: (_) {
              return ExtraNotification(
                  switchValue: alertNotificationController.forBusiness,
                  onChanged: (newValue) => alertNotificationController
                      .changeSwitchForBusiness(newValue),
                  textName: AppStrings.forBusiness,
                  color: AppColors.yellow5Color);
            }),
            SizedBox(
              height: 3.h,
            )
          ],
        ),
      ),
    );
  }
}
