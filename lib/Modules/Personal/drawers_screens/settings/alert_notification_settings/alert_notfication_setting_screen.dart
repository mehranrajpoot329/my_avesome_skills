import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/settings/alert_notification_settings/can_help_with_tabBar.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/settings/alert_notification_settings/need_help_with_tabBar.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/settings/alert_notification_settings/quote_reviews.dart';
import 'package:AwesomeSkills/widgets/custom_bottom_appBar_navi/custom_bottom_appBar_navi.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:sizer/sizer.dart';

class AlertNotificationSetting extends StatelessWidget {
  const AlertNotificationSetting({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: NestedScrollWidget(
        floatingActionButton: WidgetMethod.floatingActionButton(context),
        body: Padding(
            padding: EdgeInsets.only(left: 3.w, right: 2.w, top: 2.h),
            child: Column(children: [
              Row(
                children: [
                  GestureDetector(
                    onTap: () => Get.back(),
                    child: Icon(
                      Icons.arrow_back_ios_new,
                      color: Colors.black,
                      size: 3.h,
                    ),
                  ),
                  SizedBox(
                    width: 5.w,
                  ),
                  const Text(
                    AppStrings.settings,
                    style: montserrat20TextStyle,
                  ),
                ],
              ),
              SizedBox(
                height: 2.h,
              ),
              Container(
                height: 15.h,
                padding: EdgeInsets.only(left: 2.w, right: 2.w, top: 2.h),
                decoration: BoxDecoration(
                    color: AppColors.lightGreen2Color,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                          blurRadius: 4.0,
                          spreadRadius: 2.0,
                          color: Colors.grey.withOpacity(0.2))
                    ]),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left: 4.w),
                          child: Text(
                            AppStrings.alertNotifications,
                            style: orange600ColorTextStyle,
                          ),
                        ),
                      ],
                    ),
                    const TabBar(
                        isScrollable: true,
                        indicatorSize: TabBarIndicatorSize.label,
                        indicatorColor: AppColors.lightBlueColor,
                        labelColor: Colors.black,
                        labelStyle: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                        unselectedLabelStyle: TextStyle(
                          fontWeight: FontWeight.w400,
                          color: Colors.black,
                        ),
                        tabs: [
                          Tab(
                            text: AppStrings.needHelp,
                          ),
                          Tab(
                            text: AppStrings.canHelpWith,
                          ),
                          Tab(
                            text: AppStrings.quotesPlusReview,
                          ),
                        ]),
                  ],
                ),
              ),
              Expanded(
                child: TabBarView(
                    physics: NeverScrollableScrollPhysics(),
                    children: [
                      NeedHelpWithTabBar(),
                      CanHelpWithTabBar(),
                      QuotesReviews(),
                    ]),
              ),
            ])),
      ),
    );
  }
}
