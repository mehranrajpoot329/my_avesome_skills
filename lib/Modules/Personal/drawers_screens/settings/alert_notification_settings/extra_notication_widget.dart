import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/constants/app_colors.dart';
import 'package:sizer/sizer.dart';

class ExtraNotification extends StatelessWidget {
  String textName;
  Color color;
  bool switchValue;
  Function onChanged;

  ExtraNotification({
    required this.textName,
    required this.color,
    required this.switchValue,
    required this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 4.w, vertical: 2.h),
      height: 10.h,
      width: 90.w,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                textName,
                style: black5002Color,
              ),
              Transform.scale(
                scale: 0.8,
                child: CupertinoSwitch(
                    trackColor: Colors.grey,
                    activeColor: AppColors.indigoColor,
                    value: switchValue,
                    onChanged: (newValue) {
                      onChanged(newValue);
                    }),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
