import 'package:AwesomeSkills/Modules/Personal/drawers_screens/settings/social_media_settings/social_media_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';

class SocialMediaWidget extends StatelessWidget {
  final socialMediaController = Get.put(SocialMediaController());
  String socialMediaName, imagePath, hintText;
  TextEditingController controller;
  Function onChanged;
  Color color;

  SocialMediaWidget(
      {required this.socialMediaName,
      required this.imagePath,
      required this.controller,
      required this.hintText,
      required this.onChanged,
      required this.color});
  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Container(
        height: 8.h,
        width: 45.w,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(9),
            border:
                Border.all(color: color, width: 1, style: BorderStyle.solid),
            boxShadow: [
              BoxShadow(
                  offset: Offset(0, 5),
                  blurRadius: 1.0,
                  spreadRadius: 1.0,
                  color: Colors.grey.withOpacity(0.1))
            ]),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              child: SvgPicture.asset(
                imagePath,
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            socialMediaController.isTextField == false
                ? Expanded(
                    flex: 2,
                    child: Text(socialMediaName,
                        style: TextStyle(
                            fontFamily: 'poppins',
                            color: color,
                            fontSize: 18,
                            fontWeight: FontWeight.w400)),
                  )
                : Expanded(
                    flex: 3,
                    child: TextFormField(
                      onChanged: (val) => onChanged(val),
                      controller: controller,
                      decoration: InputDecoration(
                          contentPadding:
                              EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                          hintText: hintText,
                          hintStyle: grey18Color,
                          border: InputBorder.none),
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}
