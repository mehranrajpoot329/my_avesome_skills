import 'package:AwesomeSkills/local/db/local_storage.dart';
import 'package:AwesomeSkills/network/base_response.dart';
import 'package:AwesomeSkills/network/end_points.dart';
import 'package:AwesomeSkills/network/requests.dart';
import 'package:AwesomeSkills/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';

class SocialMediaController extends GetxController {
  TextEditingController faceBookController = TextEditingController();
  TextEditingController twitterController = TextEditingController();
  TextEditingController instagramController = TextEditingController();
  TextEditingController linkedInController = TextEditingController();
  TextEditingController googleController = TextEditingController();
  RxBool isTextField = false.obs;

  final socialMediaFormKey = GlobalKey<FormBuilderState>();

  Rx<String> faceBookTextField = ''.obs;
  Rx<String> faceBookLabel = "facebook".obs;
  Rx<String> twitterTextField = ''.obs;
  Rx<String> twitterLabel = "twitter".obs;
  Rx<String> instagramTextField = ''.obs;
  Rx<String> instagramLabel = "instagram".obs;
  Rx<String> linkedInTextField = ''.obs;
  Rx<String> linkedInLabel = "twitter".obs;
  Rx<String> googleTextField = ''.obs;
  Rx<String> googleLabel = "instagram".obs;

  void faceBookOnChanged(val) {
    faceBookTextField.value = val;
    faceBookLabel;
  }

  void twitterOnChanged(val) {
    twitterTextField.value = val;
    twitterLabel;
  }

  void instagramOnChanged(val) {
    instagramTextField.value = val;
    instagramLabel;
  }

  void linkedInOnChanged(val) {
    linkedInTextField.value = val;
    linkedInLabel;
  }

  void googleOnChanged(val) {
    googleTextField.value = val;
    googleLabel;
  }

  clickOnEditButton() {
    isTextField.value = true;
  }

  Future<void> socialMediaLinks() async {
    if (socialMediaFormKey.currentState!.saveAndValidate()) {
      var formData = ({
        "social_links": [
          {
            "label": faceBookLabel,
            'link': faceBookTextField.value.toString(),
          },
          {
            "label": twitterLabel,
            'link': twitterTextField.value.toString(),
          },
          {
            "label": instagramLabel,
            'link': instagramTextField.value.toString(),
          },
          {
            "label": linkedInLabel,
            'link': linkedInTextField.value.toString(),
          },
          {
            "label": googleLabel,
            'link': googleTextField.value.toString(),
          },
        ]
      });
      BaseResponse response = await DioClient().putRequest(
        endPoint: EndPoints.resetPassword,
        headers: {"token": HiveHelper.getAuthToken()},
        body: formData,
      );
      if (response.error == false) {
        Utils.successSnackBar(message: response.data.toString());
      }
    }
  }
}
