import 'package:AwesomeSkills/Modules/Personal/drawers_screens/settings/social_media_settings/social_media_controller.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/settings/social_media_settings/build_social_media_widget.dart';
import 'package:AwesomeSkills/widgets/custom_bottom_appBar_navi/custom_bottom_appBar_navi.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:sizer/sizer.dart';

class SocialMediaScreen extends StatelessWidget {
  final socialMediaController = Get.put(SocialMediaController());

  @override
  Widget build(BuildContext context) {
    return NestedScrollWidget(
        floatingActionButton: WidgetMethod.floatingActionButton(context),
        body: Padding(
            padding: EdgeInsets.only(left: 3.w, right: 2.w),
            child: SingleChildScrollView(
              child: FormBuilder(
                key: socialMediaController.socialMediaFormKey,
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          GestureDetector(
                            onTap: () => Get.back(),
                            child: Icon(
                              Icons.arrow_back_ios_new,
                              color: Colors.black,
                              size: 3.h,
                            ),
                          ),
                          SizedBox(
                            width: 5.w,
                          ),
                          const Text(
                            AppStrings.settings,
                            style: montserrat20TextStyle,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 1.h,
                      ),
                      Container(
                        padding:
                            EdgeInsets.only(left: 4.w, right: 2.w, top: 2.h),
                        height: 10.h,
                        width: 100.w,
                        decoration: BoxDecoration(
                          color: AppColors.lightGreen2Color,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: const Text(
                          AppStrings.socialMedia,
                          style: orangeColor600TextStyle,
                        ),
                      ),
                      SizedBox(
                        height: 2.h,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          const Text(
                            AppStrings.manageYourLinked,
                            style: blue600ColorTextStyle,
                          ),
                          GestureDetector(
                            onTap: () =>
                                socialMediaController.clickOnEditButton(),
                            child: Text(
                              AppStrings.edit,
                              style: indigo14ColorTextStyle,
                            ),
                          ),
                        ],
                      ),
                      const Text(
                        AppStrings.socialAccount,
                        style: blue600ColorTextStyle,
                      ),
                      SizedBox(
                        height: 3.h,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: SocialMediaWidget(
                              onChanged: (val) =>
                                  socialMediaController.faceBookOnChanged(val),
                              hintText: AppStrings.facebook,
                              controller:
                                  socialMediaController.faceBookController,
                              imagePath:
                                  'assets/images/post_icon/facebook_icon.svg',
                              socialMediaName: AppStrings.facebook,
                              color: AppColors.blue18Color,
                            ),
                          ),
                          SizedBox(
                            width: 5.w,
                          ),
                          Expanded(
                            child: SocialMediaWidget(
                              onChanged: (val) =>
                                  socialMediaController.twitterOnChanged(val),
                              hintText: AppStrings.twitter,
                              controller:
                                  socialMediaController.twitterController,
                              imagePath:
                                  'assets/images/post_icon/twitter_icon.svg',
                              socialMediaName: AppStrings.twitter,
                              color: AppColors.blue18Color,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 3.h,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            child: SocialMediaWidget(
                              onChanged: (val) =>
                                  socialMediaController.instagramLabel(val),
                              hintText: AppStrings.instagram,
                              controller:
                                  socialMediaController.instagramController,
                              imagePath:
                                  'assets/images/post_icon/instagram_icon.svg',
                              socialMediaName: AppStrings.instagram,
                              color: AppColors.grey4Color,
                            ),
                          ),
                          SizedBox(
                            width: 5.w,
                          ),
                          Expanded(
                            child: SocialMediaWidget(
                              onChanged: (val) =>
                                  socialMediaController.linkedInOnChanged(val),
                              hintText: AppStrings.linkedIn,
                              controller:
                                  socialMediaController.linkedInController,
                              imagePath:
                                  'assets/images/post_icon/linkedIn_icon.svg',
                              socialMediaName: AppStrings.linkedIn,
                              color: AppColors.blueColor,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 3.h,
                      ),
                      SocialMediaWidget(
                        onChanged: (val) =>
                            socialMediaController.googleOnChanged(val),
                        hintText: AppStrings.google,
                        controller: socialMediaController.googleController,
                        imagePath:
                            'assets/images/post_icon/google_plus_icon.svg',
                        socialMediaName: AppStrings.google,
                        color: AppColors.red2Color,
                      ),
                      SizedBox(
                        height: 2.h,
                      ),
                      CustomButton(
                        height: 5.h,
                        width: 22.w,
                        buttonName: AppStrings.save,
                        borderRadius: BorderRadius.circular(3),
                        buttonColor: AppColors.lightBlueColor,
                        textStyle: blackPoppins300TextStyle,
                        onTap: () => socialMediaController.socialMediaLinks(),
                      ),
                    ]),
              ),
            )));
  }
}
