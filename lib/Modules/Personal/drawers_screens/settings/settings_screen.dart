import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/settings/setting_card.dart';
import 'package:AwesomeSkills/constants/text_style.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:sizer/sizer.dart';

import '../../../../routes/app_pages.dart';

class SettingScreen extends StatelessWidget {
  const SettingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NestedScrollWidget(
      floatingActionButton: WidgetMethod.floatingActionButton(context),
      body: Padding(
          padding: EdgeInsets.only(left: 3.w, right: 2.w),
          child: SingleChildScrollView(
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              SizedBox(
                height: 2.h,
              ),
              Row(
                children: [
                  GestureDetector(
                    onTap: () => Get.back(),
                    child: Icon(
                      Icons.arrow_back_ios_new,
                      color: Colors.black,
                      size: 3.h,
                    ),
                  ),
                  SizedBox(
                    width: 5.w,
                  ),
                  const Text(
                    AppStrings.settings,
                    style: montserrat20TextStyle,
                  ),
                ],
              ),
              SizedBox(
                height: 2.h,
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 4.w, vertical: 1.5.h),
                height: 10.h,
                width: 100.w,
                decoration: BoxDecoration(
                  color: AppColors.lightGreen2Color,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: const Text(
                  AppStrings.helloAgainAstria,
                  style: orangeColor600TextStyle,
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 2.w),
                child: Container(
                  padding: EdgeInsets.only(left: 2.w, right: 2.w, top: 0.3.h),
                  height: 13.h,
                  width: 35.w,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey.withOpacity(0.2),
                          spreadRadius: 4.0,
                          blurRadius: 6.0)
                    ],
                    border: Border.all(
                        color: AppColors.indigoColor,
                        width: 1,
                        style: BorderStyle.solid),
                  ),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Container(
                            height: 7.h,
                            width: 7.w,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.transparent,
                                border: Border.all(
                                    color: AppColors.indigoColor,
                                    width: 1,
                                    style: BorderStyle.solid)),
                            child: Center(
                              child: Icon(
                                Icons.person,
                                color: AppColors.indigoColor,
                                size: 15.sp,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 2.w,
                          ),
                          Text(
                            AppStrings.twenty,
                            style: indigoColor20TextStyle,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 1.h,
                      ),
                      const FittedBox(
                        child: Text(
                          AppStrings.profileCompletion,
                          style: black400Color,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 2.w),
                child: Row(
                  children: [
                    Expanded(
                      child: SettingsCard(
                        onTap: () {
                          Get.toNamed(AppPage.myProfileSetting);
                        },
                        isPic: true,
                        isComplete: true,
                        imagePath: 'assets/images/settings_profile.png',
                        textName: AppStrings.profileSettings,
                      ),
                    ),
                    SizedBox(
                      width: 4.w,
                    ),
                    Expanded(
                      child: SettingsCard(
                        onTap: () {
                          Get.toNamed(AppPage.alertNotificationSetting);
                        },
                        isPic: false,
                        isComplete: false,
                        imagePath: 'assets/images/post_icon/alert_icon.svg',
                        textName: AppStrings.alertNotification,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 2.w),
                child: Row(
                  children: [
                    Expanded(
                      child: SettingsCard(
                        onTap: () =>
                            Get.toNamed(AppPage.securityPasswordSetting),
                        isPic: false,
                        isComplete: true,
                        imagePath: 'assets/images/post_icon/lock_icon.svg',
                        textName: AppStrings.securityPassword,
                      ),
                    ),
                    SizedBox(
                      width: 4.w,
                    ),
                    Expanded(
                      child: SettingsCard(
                        onTap: () {
                          Get.toNamed(AppPage.socialMediaSetting);
                        },
                        isPic: false,
                        isComplete: false,
                        imagePath: 'assets/images/post_icon/share_icon.svg',
                        textName: AppStrings.socialMedia,
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 2.w),
                child: Row(
                  children: [
                    Expanded(
                      child: FreeMyPlan(
                        onTap: () => Get.toNamed(AppPage.freePlanSettings),
                      ),
                    ),
                    SizedBox(
                      width: 4.w,
                    ),
                    Expanded(
                      child: PaymentMethodCard(
                        onTap: () => Get.toNamed(AppPage.paymentMethodSettings),
                      ),
                    )
                  ],
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              Center(
                  child: CustomButton(
                buttonName: AppStrings.upgradeBusiness,
                height: 50,
                width: 240,
                buttonColor: AppColors.lightGreen4Color,
                textStyle: blackBoldPoppinsColor,
                borderRadius: BorderRadius.circular(10),
                onTap: () => Get.toNamed(AppPage.listYourBusiness),
              )),
              SizedBox(
                height: 3.h,
              ),
            ]),
          )),
    );
  }
}
