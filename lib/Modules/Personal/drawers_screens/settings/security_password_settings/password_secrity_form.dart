import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/settings/security_password_settings/security_password_controller.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:sizer/sizer.dart';

class PasswordSecurityForm extends StatelessWidget {
  final controller = Get.put(SecurityPasswordController());

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(left: 6.w, right: 3.w),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.all(4.w),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      AppStrings.addMobileNumber,
                      style: indigo14ColorTextStyle,
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    SizedBox(
                      height: 7.h,
                      child: TextFormField(
                        controller: controller.phoneNumberController,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: AppColors.lightBlue6Color,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    const FittedBox(
                      child: Text(
                        AppStrings.confirmCode,
                        style: indigo14ColorTextStyle,
                      ),
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    SizedBox(
                      height: 7.h,
                      child: TextFormField(
                        controller: controller.confirmPhoneController,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: AppColors.lightBlue6Color,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    CustomButton(
                        buttonName: AppStrings.addPhone,
                        width: 40.w,
                        height: 5.h,
                        buttonColor: AppColors.lightBlueColor,
                        textStyle: white700MulishTextStyle,
                        borderRadius: BorderRadius.circular(3),
                        onTap: () {}),
                    SizedBox(
                      height: 3.h,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        GetBuilder<SecurityPasswordController>(builder: (_) {
          return buildSwitchWidget(
            isOptionalText: false,
            isTextIcon: true,
            context: context,
            imagePath: 'assets/images/post_icon/finger_print_icon.png',
            textName: AppStrings.fingerPrintSignIn,
            detail: AppStrings.useFingerPrint,
            switchValue: controller.isFingerPrint,
            onChanged: (newValue) =>
                controller.changeSwitchFingerPrint(newValue),
          );
        }),
        SizedBox(
          height: 2.h,
        ),
        GetBuilder<SecurityPasswordController>(builder: (_) {
          return buildSwitchWidget(
            isOptionalText: false,
            isTextIcon: true,
            context: context,
            imagePath: 'assets/images/post_icon/face_recognition_icon.png',
            textName: AppStrings.faceRecognition,
            detail: AppStrings.useYourFace,
            switchValue: controller.isFaceRecognition,
            onChanged: (newValue) =>
                controller.changeSwitchFaceRecognition(newValue),
          );
        }),
        SizedBox(
          height: 2.h,
        ),
        Row(
          children: [
            Image.asset(
              'assets/images/post_icon/two_factor_authentication_icon.png',
              height: 5.h,
              width: 5.h,
            ),
            SizedBox(
              width: 3.w,
            ),
            Expanded(
              child: FittedBox(
                child: Text(
                  AppStrings.twoFactorAuthentication,
                  style: lightBlue16ColorTextStyle,
                ),
              ),
            ),
            SizedBox(
              width: 2.w,
            ),
            GetBuilder<SecurityPasswordController>(
              builder: (_) {
                return Transform.scale(
                  scale: 0.8,
                  child: CupertinoSwitch(
                    trackColor: Colors.grey,
                    activeColor: AppColors.indigoColor,
                    value: controller.isTwoFactorAuthentication,
                    onChanged: (newValue) => controller
                        .changeSwitchTwoFactorAuthentication(newValue),
                  ),
                );
              },
            ),
          ],
        ),
        SizedBox(
          height: 1.h,
        ),
        GetBuilder<SecurityPasswordController>(builder: (_) {
          return buildSwitchWidget(
            isOptionalText: true,
            isTextIcon: true,
            context: context,
            imagePath: 'assets/images/post_icon/location_icon.png',
            textName: AppStrings.addressVerification,
            detail: AppStrings.verifyYourPhysical,
            switchValue: controller.isAddressVerification,
            onChanged: (newValue) =>
                controller.changeSwitchAddressVerification(newValue),
          );
        }),
        SizedBox(
          height: 2.h,
        ),
        Padding(
          padding: EdgeInsets.only(left: 12.w),
          child: CustomButton(
              width: 40.w,
              height: 5.h,
              buttonName: AppStrings.addAddress,
              buttonColor: AppColors.lightBlueColor,
              textStyle: white700MulishTextStyle,
              borderRadius: BorderRadius.circular(3),
              onTap: () {}),
        ),
        SizedBox(
          height: 2.h,
        ),
        GetBuilder<SecurityPasswordController>(
          builder: (_) {
            return Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      width: 50.w,
                      child: Row(
                        children: [
                          Image.asset(
                            'assets/images/post_icon/lock_key_icon.png',
                            height: 24.sp,
                            width: 24.sp,
                          ),
                          SizedBox(
                            width: 4.w,
                          ),
                          const Text(
                            AppStrings.resetPassword,
                            style: lightBlue16ColorTextStyle,
                          ),
                        ],
                      ),
                    ),
                    Transform.scale(
                      scale: 0.8,
                      child: CupertinoSwitch(
                        trackColor: Colors.grey,
                        activeColor: AppColors.indigoColor,
                        value: controller.isResetPassword,
                        onChanged: (newValue) =>
                            controller.changeSwitchResetPassword(newValue),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 2.h,
                ),
                Visibility(
                  visible: controller.isResetPassword,
                  child: Padding(
                    padding: EdgeInsets.only(left: 10.w, right: 3.w),
                    child: FormBuilder(
                      key: controller.resetFormKey,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: 6.h,
                            child: TextFormField(
                              controller: controller.oldPasswordController,
                              decoration: InputDecoration(
                                contentPadding: const EdgeInsets.symmetric(
                                    vertical: 5, horizontal: 10),
                                hintText: AppStrings.oldPassword,
                                filled: true,
                                fillColor: AppColors.lightBlue6Color,
                                hintStyle: grey18Color,
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 1.h,
                          ),
                          SizedBox(
                            height: 6.h,
                            child: TextFormField(
                              controller: controller.newPasswordController,
                              decoration: InputDecoration(
                                  filled: true,
                                  fillColor: AppColors.lightBlue6Color,
                                  contentPadding: const EdgeInsets.symmetric(
                                      vertical: 5, horizontal: 10),
                                  hintText: AppStrings.newPassword,
                                  hintStyle: grey18Color,
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(4),
                                  )),
                            ),
                          ),
                          SizedBox(
                            height: 1.h,
                          ),
                          SizedBox(
                            height: 6.h,
                            child: TextFormField(
                              controller:
                                  controller.confirmNewPasswordController,
                              decoration: InputDecoration(
                                  filled: true,
                                  fillColor: AppColors.lightBlue6Color,
                                  contentPadding: const EdgeInsets.symmetric(
                                      vertical: 5, horizontal: 10),
                                  hintText: AppStrings.confirmPassword,
                                  hintStyle: grey18Color,
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(4),
                                  )),
                            ),
                          ),
                          SizedBox(
                            height: 2.h,
                          ),
                          CustomButton(
                              width: 40.w,
                              height: 5.h,
                              buttonName: AppStrings.resetPassword,
                              buttonColor: AppColors.lightBlueColor,
                              textStyle: white700MulishTextStyle,
                              borderRadius: BorderRadius.circular(3),
                              onTap: () => controller.resetYourPassword()),
                          SizedBox(
                            height: 2.h,
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            );
          },
        ),
        SizedBox(
          height: 2.h,
        ),
        GetBuilder<SecurityPasswordController>(builder: (_) {
          return buildSwitchWidget(
            isOptionalText: false,
            isTextIcon: true,
            context: context,
            imagePath: 'assets/images/post_icon/question_icon.png',
            textName: AppStrings.securityQuestion,
            detail: AppStrings.byAddingSecurity,
            switchValue: controller.isSecurityQuestion,
            onChanged: (newValue) =>
                controller.changeSwitchSecurityQuestion(newValue),
          );
        }),
        SizedBox(
          height: 2.h,
        ),
        GetBuilder<SecurityPasswordController>(builder: (_) {
          return Visibility(
            visible: controller.isSecurityQuestion,
            child: Padding(
              padding: EdgeInsets.only(left: 12.w, right: 3.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    AppStrings.selectSecurityQuestion,
                    style: indigoColor12TextStyle,
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  SizedBox(
                    height: 6.h,
                    child: TextFormField(
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: AppColors.lightBlue6Color,
                        contentPadding: const EdgeInsets.symmetric(
                            vertical: 5, horizontal: 10),
                        hintText: AppStrings.typeYourQuestion,
                        hintStyle: grey18Color,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  const Text(
                    AppStrings.yourAnswer,
                    style: indigoColor12TextStyle,
                  ),
                  SizedBox(
                    height: 6.h,
                    child: TextFormField(
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: AppColors.lightBlue6Color,
                          contentPadding: const EdgeInsets.symmetric(
                              vertical: 5, horizontal: 10),
                          hintText: AppStrings.typeYourAnswer,
                          hintStyle: grey18Color,
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(4),
                          )),
                    ),
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  CustomButton(
                      buttonName: AppStrings.set,
                      width: 30.w,
                      height: 5.h,
                      buttonColor: AppColors.lightBlueColor,
                      textStyle: white700MulishTextStyle,
                      borderRadius: BorderRadius.circular(4),
                      onTap: () {})
                ],
              ),
            ),
          );
        }),
      ],
    );
  }

  Widget buildSwitchWidget({
    required BuildContext context,
    required String imagePath,
    required String textName,
    required String detail,
    required bool switchValue,
    required Function onChanged,
    required bool isTextIcon,
    required bool isOptionalText,
  }) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        isTextIcon
            ? Image.asset(
                imagePath,
                height: 5.h,
                width: 5.h,
              )
            : SizedBox(
                width: 0.1.w,
              ),
        isTextIcon
            ? SizedBox(
                width: 4.w,
              )
            : SizedBox(
                width: 10.w,
              ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  FittedBox(
                    child: Text(
                      textName,
                      style: lightBlue16ColorTextStyle,
                    ),
                  ),
                  SizedBox(
                    width: 1.w,
                  ),
                  isOptionalText
                      ? Expanded(
                          child: FittedBox(
                          child: Text(
                            AppStrings.optionalBracket,
                            style: greyTextColor,
                          ),
                        ))
                      : Container(),
                ],
              ),
              SizedBox(
                width: 75.w,
                child: Text(
                  detail,
                  style: black12PoppinsColor,
                ),
              ),
            ],
          ),
        ),
        Transform.scale(
          scale: 0.8,
          child: CupertinoSwitch(
              trackColor: Colors.grey,
              activeColor: AppColors.indigoColor,
              value: switchValue,
              onChanged: (newValue) {
                onChanged(newValue);
              }),
        ),
      ],
    );
  }
}
