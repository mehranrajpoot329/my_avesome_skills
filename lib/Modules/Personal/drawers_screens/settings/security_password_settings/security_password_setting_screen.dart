import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/settings/security_password_settings/password_secrity_form.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/settings/settings_controller.dart';
import 'package:AwesomeSkills/widgets/custom_bottom_appBar_navi/custom_bottom_appBar_navi.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:sizer/sizer.dart';

class SecurityPasswordSetting extends StatefulWidget {
  @override
  State<SecurityPasswordSetting> createState() =>
      _SecurityPasswordSettingState();
}

class _SecurityPasswordSettingState extends State<SecurityPasswordSetting> {
  final SettingsController controller = Get.put(SettingsController());

  @override
  Widget build(BuildContext context) {
    return NestedScrollWidget(
      floatingActionButton: WidgetMethod.floatingActionButton(context),
      body: Padding(
          padding: EdgeInsets.only(left: 3.w, right: 2.w),
          child: SingleChildScrollView(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                SizedBox(
                  height: 2.h,
                ),
                Row(
                  children: [
                    GestureDetector(
                      onTap: () => Get.back(),
                      child: Icon(
                        Icons.arrow_back_ios_new,
                        color: Colors.black,
                        size: 3.h,
                      ),
                    ),
                    SizedBox(
                      width: 5.w,
                    ),
                    const Text(
                      AppStrings.settings,
                      style: montserrat20TextStyle,
                    ),
                  ],
                ),
                SizedBox(
                  height: 2.h,
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 3.w, vertical: 2.h),
                  height: 10.h,
                  width: 100.w,
                  decoration: BoxDecoration(
                    color: AppColors.lightGreen2Color,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: const FittedBox(
                    child: Text(
                      AppStrings.securityPasswordLogIn,
                      style: orange500ColorTextStyle,
                    ),
                  ),
                ),
                SizedBox(
                  height: 2.h,
                ),
                Padding(
                  padding: EdgeInsets.only(left: 4.w),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          SvgPicture.asset(
                            'assets/images/post_icon/lock_orange_icon.svg',
                            color: AppColors.lightBlueColor,
                            height: 24.sp,
                            width: 24.sp,
                          ),
                          SizedBox(
                            width: 3.w,
                          ),
                          const Text(
                            AppStrings.yourSecurity,
                            style: blue600ColorTextStyle,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 1.h,
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 11.w),
                        child: Text(
                          AppStrings.secureYourAccount,
                          style: black14Color,
                        ),
                      ),
                      SizedBox(
                        height: 2.h,
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SvgPicture.asset(
                            'assets/images/post_icon/mobile_black_icon.svg',
                            color: AppColors.lightBlueColor,
                          ),
                          SizedBox(
                            width: 2.w,
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 3.w),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  AppStrings.telephoneNumber,
                                  style: lightBlue16ColorTextStyle,
                                ),
                                SizedBox(
                                  height: 0.5.h,
                                ),
                                SizedBox(
                                  width: 75.w,
                                  child: const Text(
                                    AppStrings.youHaveNotAssociated,
                                    style: black12PoppinsColor,
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                      SizedBox(
                        height: 2.h,
                      ),
                      PasswordSecurityForm()
                    ],
                  ),
                ),
                SizedBox(
                  height: 5.h,
                ),
              ]))),
    );
  }
}
