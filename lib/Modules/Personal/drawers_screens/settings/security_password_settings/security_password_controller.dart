import 'package:AwesomeSkills/local/db/local_storage.dart';
import 'package:AwesomeSkills/models/user_model.dart';
import 'package:AwesomeSkills/network/base_response.dart';
import 'package:AwesomeSkills/network/end_points.dart';
import 'package:AwesomeSkills/network/requests.dart';
import 'package:AwesomeSkills/utils/utils.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart' hide FormData;

class SecurityPasswordController extends GetxController {
  final TextEditingController phoneNumberController = TextEditingController();
  final TextEditingController confirmPhoneController = TextEditingController();
  final TextEditingController oldPasswordController = TextEditingController();
  final TextEditingController newPasswordController = TextEditingController();
  final TextEditingController confirmNewPasswordController =
      TextEditingController();

  final resetFormKey = GlobalKey<FormBuilderState>();

  bool isFingerPrint = false;
  bool isFaceRecognition = false;
  bool isTwoFactorAuthentication = false;
  bool isAddressVerification = false;
  bool isResetPassword = false;
  bool isSecurityQuestion = false;

  changeSwitchFingerPrint(bool value) {
    isFingerPrint = value;
    update();
  }

  changeSwitchFaceRecognition(bool value) {
    isFaceRecognition = value;
    update();
  }

  changeSwitchTwoFactorAuthentication(bool value) {
    isTwoFactorAuthentication = value;
    update();
  }

  changeSwitchAddressVerification(bool value) {
    isAddressVerification = value;
    update();
  }

  changeSwitchResetPassword(bool value) {
    isResetPassword = value;
    update();
  }

  changeSwitchSecurityQuestion(bool value) {
    isSecurityQuestion = value;
    update();
  }

  Future<void> resetYourPassword() async {
    if (resetFormKey.currentState!.saveAndValidate()) {
      var formData = ({
        'new_password': newPasswordController.text.toString(),
        'confirm_new_password': confirmNewPasswordController.text.toString(),
        'old_password': oldPasswordController.text.toString(),
      });
      BaseResponse response = await DioClient().putRequest(
        endPoint: EndPoints.resetPassword,
        headers: {"token": HiveHelper.getAuthToken()},
        body: formData,
      );
      if (response.error == false) {
        print(newPasswordController.text.toString());
        print(oldPasswordController.text.toString());
        User user = User.fromJson(response.data);
        HiveHelper.saveUser(user);
        Utils.successSnackBar(message: "Password Update");
      }
    }
  }
}
