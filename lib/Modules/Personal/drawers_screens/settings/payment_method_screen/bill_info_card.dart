import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:sizer/sizer.dart';

class BillInformationContainer extends StatelessWidget {
  const BillInformationContainer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 3.w, right: 3.w, top: 5.h),
      height: 80.h,
      width: 100.w,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
                offset: Offset(0, 3),
                blurRadius: 1.0,
                spreadRadius: 1.0,
                color: Colors.grey..withOpacity(0.1))
          ]),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(left: 2.w),
            child: Row(
              children: [
                SvgPicture.asset(
                  'assets/images/post_icon/payment_blue_icon.svg',
                  height: 20.sp,
                  width: 20.sp,
                  fit: BoxFit.cover,
                ),
                SizedBox(
                  width: 5.w,
                ),
                const Text(
                  AppStrings.billingInformation,
                  style: grey24Color,
                )
              ],
            ),
          ),
          SizedBox(
            height: 2.h,
          ),
          BillInfoCard(
              imagePath: 'assets/images/post_icon/master_card_icon.svg',
              textName: AppStrings.masterCardEnding,
              date: AppStrings.sevenSlashTwenty,
              editOnTap: () {},
              deleteOnTap: () {}),
          SizedBox(
            height: 2.h,
          ),
          BillInfoCard(
              imagePath: 'assets/images/post_icon/visa_icon.svg',
              textName: AppStrings.visaEnding,
              date: AppStrings.sevenSlashTwenty,
              editOnTap: () {},
              deleteOnTap: () {}),
          SizedBox(
            height: 2.h,
          ),
          BillInfoCard(
              imagePath: 'assets/images/post_icon/american_express_icon.svg',
              textName: AppStrings.americanExpressEnding,
              date: AppStrings.sevenSlashTwenty,
              editOnTap: () {},
              deleteOnTap: () {}),
        ],
      ),
    );
  }
}

class BillInfoCard extends StatelessWidget {
  String imagePath, textName, date;
  Function editOnTap, deleteOnTap;

  BillInfoCard({
    required this.imagePath,
    required this.textName,
    required this.date,
    required this.editOnTap,
    required this.deleteOnTap,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 3.w, right: 4.w, top: 2.h),
      height: 19.h,
      decoration: BoxDecoration(
        color: AppColors.grey3Color.withOpacity(0.2),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        children: [
          Row(
            children: [
              SvgPicture.asset(
                imagePath,
                height: 24.sp,
                width: 24.sp,
              ),
              SizedBox(
                width: 4.w,
              ),
              Expanded(
                child: FittedBox(
                  child: Text(
                    textName,
                    style: blackBold16Color,
                  ),
                ),
              )
            ],
          ),
          SizedBox(
            height: 7.h,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              const Text(
                AppStrings.sevenSlashTwenty,
                style: grey14Color,
              ),
              const SizedBox(
                width: 10,
              ),
              GestureDetector(
                onTap: () => editOnTap(),
                child: const Text(
                  AppStrings.edit,
                  style: indigo14ColorTextStyle,
                ),
              ),
              const SizedBox(
                width: 10,
              ),
              GestureDetector(
                onTap: () => deleteOnTap(),
                child:
                    SvgPicture.asset('assets/images/post_icon/delete_icon.svg'),
              )
            ],
          ),
        ],
      ),
    );
  }
}
