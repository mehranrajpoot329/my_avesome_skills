import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/routes/app_pages.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:sizer/sizer.dart';

class FreePlanScreen extends StatelessWidget {
  const FreePlanScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NestedScrollWidget(
        floatingActionButton: WidgetMethod.floatingActionButton(context),
        body: Padding(
            padding: EdgeInsets.only(left: 2.w, right: 2.w),
            child: SingleChildScrollView(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                  SizedBox(
                    height: 2.h,
                  ),
                  Row(
                    children: [
                      GestureDetector(
                        onTap: () => Get.back(),
                        child: Icon(
                          Icons.arrow_back_ios_new,
                          color: Colors.black,
                          size: 3.h,
                        ),
                      ),
                      SizedBox(
                        width: 5.w,
                      ),
                      const Text(
                        AppStrings.settings,
                        style: montserrat20TextStyle,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: 3.w, vertical: 2.h),
                    height: 10.h,
                    width: 100.w,
                    decoration: BoxDecoration(
                      color: AppColors.lightGreen2Color,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: const Text(
                      AppStrings.myPlan,
                      style: orangeColor600TextStyle,
                    ),
                  ),
                  SizedBox(
                    height: 3.h,
                  ),
                  Center(
                    child: Container(
                      padding: EdgeInsets.only(left: 10.w),
                      height: 40.h,
                      width: 80.w,
                      decoration: BoxDecoration(
                          color: AppColors.blue10Color,
                          borderRadius: BorderRadius.circular(8),
                          boxShadow: [
                            BoxShadow(
                                offset: const Offset(0, 5),
                                color: Colors.grey.withOpacity(0.2),
                                spreadRadius: 1.0,
                                blurRadius: 1.0)
                          ]),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text(
                            AppStrings.myPlan,
                            style: lightBlue32ColorTextStyle,
                          ),
                          SizedBox(
                            height: 0.5.h,
                          ),
                          const Text(
                            AppStrings.free,
                            style: green40ColorTextStyle,
                          ),
                          Text(
                            AppStrings.yourAreCurrently,
                            style: blackBold16Color,
                          ),
                          SizedBox(
                            height: 2.h,
                          ),
                          CustomButton(
                              buttonName: AppStrings.upgrade,
                              width: 60.w,
                              height: 7.h,
                              buttonColor: AppColors.blue19Color,
                              textStyle: white28ColorTextStyle,
                              borderRadius: BorderRadius.circular(5),
                              onTap: () {
                                Get.toNamed(AppPage.listYourBusiness);
                              }),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 5.h,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 25.w),
                    child: const Text(
                      AppStrings.invoices,
                      style: blackBold600Color,
                    ),
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 30.w),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        GestureDetector(
                            onTap: () {},
                            child: const Text(
                              AppStrings.viewAllInvoices,
                              style: blue20ColorTextStyle,
                            )),
                        SizedBox(
                          height: 0.5.h,
                        ),
                        GestureDetector(
                            onTap: () {},
                            child: const Text(
                              AppStrings.downloadInvoices,
                              style: blue20ColorTextStyle,
                            ))
                      ],
                    ),
                  ),
                ]))));
  }
}
