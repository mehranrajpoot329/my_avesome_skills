import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:sizer/sizer.dart';

class SettingsCard extends StatelessWidget {
  String imagePath, textName;
  bool isComplete;
  bool isPic;
  Function onTap;
  SettingsCard(
      {required this.imagePath,
      required this.textName,
      required this.isPic,
      required this.onTap,
      required this.isComplete});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onTap(),
      child: Container(
        height: 18.h,
        width: 18.h,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
            border: Border.all(
                color: Colors.grey, width: 0.5, style: BorderStyle.solid),
            boxShadow: [
              BoxShadow(
                  offset: Offset(0, 5),
                  color: Colors.grey.withOpacity(0.2),
                  spreadRadius: 2.0,
                  blurRadius: 2.0),
            ]),
        child: Stack(
          children: [
            Positioned(
                top: 20,
                left: 20,
                child: isPic
                    ? Container(
                        height: 6.h,
                        width: 6.h,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              image: AssetImage(imagePath),
                              fit: BoxFit.fitHeight,
                            )),
                      )
                    : SvgPicture.asset(
                        imagePath,
                        height: 23,
                        width: 26,
                      )),
            Positioned(
              top: 15,
              right: 20,
              child: isComplete
                  ? SvgPicture.asset(
                      'assets/images/post_icon/check_icon.svg',
                      height: 15,
                      width: 17,
                    )
                  : Text(
                      AppStrings.inComplete,
                      style: redColorTextStyle,
                    ),
            ),
            Positioned(
                bottom: 15,
                left: 20,
                child: Text(
                  textName,
                  style: black14Color,
                )),
          ],
        ),
      ),
    );
  }
}

class FreeMyPlan extends StatelessWidget {
  Function onTap;

  FreeMyPlan({required this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onTap(),
      child: Container(
          height: 18.h,
          width: 18.h,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
              border: Border.all(
                  color: Colors.grey, width: 0.5, style: BorderStyle.solid),
              boxShadow: [
                BoxShadow(
                    offset: Offset(0, 3),
                    color: Colors.grey.withOpacity(0.2),
                    spreadRadius: 2.0,
                    blurRadius: 2.0),
              ]),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  AppStrings.free,
                  style: greenColorArialTextStyle,
                ),
                SizedBox(
                  height: 1.h,
                ),
                Text(
                  AppStrings.myPlan,
                  style: blackBold16Color,
                )
              ])),
    );
  }
}

class PaymentMethodCard extends StatelessWidget {
  Function onTap;

  PaymentMethodCard({required this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onTap(),
      child: Container(
          height: 18.h,
          width: 18.h,
          decoration: BoxDecoration(
              color: AppColors.blue16Color,
              borderRadius: BorderRadius.circular(15),
              border: Border.all(
                  color: Colors.grey, width: 0.5, style: BorderStyle.solid),
              boxShadow: [
                BoxShadow(
                    offset: Offset(0, 3),
                    color: Colors.grey.withOpacity(0.2),
                    spreadRadius: 2.0,
                    blurRadius: 2.0),
              ]),
          child: Stack(children: [
            Positioned(
              top: 2.h,
              right: 3.w,
              child: Text(
                AppStrings.inComplete,
                style: redColorTextStyle,
              ),
            ),
            Positioned(
                top: 4.h,
                left: 5.w,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SvgPicture.asset(
                        'assets/images/post_icon/payment_icon.svg'),
                    SizedBox(
                      height: 1.h,
                    ),
                    Text(
                      AppStrings.creditPlusDebitCard,
                      style: blackMulish10TextStyle,
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    Text(
                      AppStrings.paymentMethod,
                      style: white14Color,
                    ),
                  ],
                )),
          ])),
    );
  }
}
