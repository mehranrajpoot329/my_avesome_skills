import 'dart:developer';
import 'dart:io';

import 'package:AwesomeSkills/local/db/local_storage.dart';
import 'package:AwesomeSkills/network/base.dart';
import 'package:AwesomeSkills/network/base_response.dart';
import 'package:AwesomeSkills/network/end_points.dart';
import 'package:AwesomeSkills/network/requests.dart';
import 'package:AwesomeSkills/utils/utils.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/models/api_model/personal_model/user_info_model.dart';
import 'package:hive/hive.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';

class MySkillsController extends GetxController {
  final TextEditingController titleController = TextEditingController();
  final TextEditingController nameSkillController = TextEditingController();
  final TextEditingController priceSkillController = TextEditingController();
  final TextEditingController languageController = TextEditingController();
  final TextEditingController certificateController = TextEditingController();
  final titleFormKey = GlobalKey<FormBuilderState>();
  final skillFormKey = GlobalKey<FormBuilderState>();
  final languageFormKey = GlobalKey<FormBuilderState>();
  final certificateFormKey = GlobalKey<FormBuilderState>();

  String nameOfSkill = '';
  String nameOfPrice = '';

  CroppedFile? projectImage;

  bool deleteEditVisibleShow = false;

  void showEditDelete() {
    deleteEditVisibleShow = !deleteEditVisibleShow;
    update();
  }

  bool isSkillVisible = false;
  bool isLanguageVisible = false;
  bool isCertificateVisible = false;
  bool isProjectTitleVisible = false;

  Rx<Data> getUserInfo = Data().obs;

  String? initialSkill, priceSkill;

  Future<void> selectImage(ImageSource selectedSource) async {
    projectImage = await Utils.getImage(source: selectedSource);
    update();
  }

  showAllSkillProjectDetails() {
    isProjectTitleVisible = !isProjectTitleVisible;
    update();
  }

  void showInfoSkillDetail() {
    isSkillVisible = !isSkillVisible;
    update();
  }

  void showInfoLanguageDetail() {
    isLanguageVisible = !isLanguageVisible;
    update();
  }

  void showInfoCertificateDetail() {
    isCertificateVisible = !isCertificateVisible;
    update();
  }

  @override
  onInit() {
    super.onInit();
    Future.delayed(20.milliseconds, () {
      getUserInfoData();
    });
  }

  Future<void> getUserInfoData() async {
    try {
      print(HiveHelper.getAuthToken());
      Utils.showProgressBar();
      BaseResponse response = await DioClient().getRequest(
          endPoint: EndPoints.getUserInfo,
          headers: {"token": HiveHelper.getAuthToken()});

      if (response.error == false) {
        Data baseResponse = Data.fromJson(response.data);
        if (baseResponse != null) {
          getUserInfo.value = baseResponse;
        }
      } else {
        Utils.errorFlutterToast(msg: response.message.toString());
      }
      Utils.dismissProgressBar();
    } catch (e) {
      Utils.dismissProgressBar();
      log('$e');
      Utils.errorFlutterToast(msg: e.toString());
    }
  }

  Future<void> deleteSkillUser({num? id}) async {
    try {
      print(HiveHelper.getAuthToken());
      Utils.showProgressBar();
      BaseResponse response = await DioClient().deleteRequest(
          endPoint: "${EndPoints.deleteSkillUrl}$id",
          headers: {"token": HiveHelper.getAuthToken()});

      Utils.successFlutterToast(msg: "Deleted");
      Utils.dismissProgressBar();
    } catch (e) {
      Utils.dismissProgressBar();
      log('$e');
      Utils.errorFlutterToast(msg: e.toString());
    }
  }

  Future<void> deleteLanguageUser({num? id}) async {
    try {
      print(HiveHelper.getAuthToken());
      Utils.showProgressBar();
      BaseResponse response = await DioClient().deleteRequest(
          endPoint: "${EndPoints.deleteLanguageUrl}$id",
          headers: {"token": HiveHelper.getAuthToken()});

      Utils.successFlutterToast(msg: "Deleted");
      Utils.dismissProgressBar();
    } catch (e) {
      Utils.dismissProgressBar();
      log('$e');
      Utils.errorFlutterToast(msg: e.toString());
    }
  }

  Future<void> deleteCertificateUser({num? id}) async {
    try {
      print(HiveHelper.getAuthToken());
      Utils.showProgressBar();
      BaseResponse response = await DioClient().deleteRequest(
          endPoint: "${EndPoints.deleteCertificateUrl}$id",
          headers: {"token": HiveHelper.getAuthToken()});

      Utils.successFlutterToast(msg: "Deleted");
      Utils.dismissProgressBar();
    } catch (e) {
      Utils.dismissProgressBar();
      log('$e');
      Utils.errorFlutterToast(msg: e.toString());
    }
  }

  Future<void> skillPost() async {
    try {
      if (skillFormKey.currentState!.saveAndValidate()) {
        var formData = {
          'skill': nameSkillController.text.toString(),
          'hour_rate': priceSkillController.text.toString(),
        };
        BaseResponse response = await DioClient().postRequest(
          endPoint: EndPoints.postAddSkillUrl,
          body: formData,
        );
        if (response.error == false) {
          Utils.successFlutterToast(msg: response.data);
          print(response.data);
          nameSkillController.clear();
          priceSkillController.clear();
        } else {
          Utils.errorFlutterToast(msg: response.message.toString());
        }
      }
    } catch (e) {
      Utils.errorSnackBar(message: e.toString());
    }
  }

  Future<void> skillEditPost(
      {num? id, String? initialSkill, String? priceSkill}) async {
    try {
      if (skillFormKey.currentState!.saveAndValidate()) {
        var formData = {
          'skill': nameSkillController.text.toString(),
          'hour_rate': priceSkillController.text.toString(),
        };
        BaseResponse response = await DioClient().postRequest(
          endPoint: EndPoints.postAddSkillUrl,
          queryParameters: {
            "id": id,
          },
          body: formData,
        );
        if (response.error == false) {
          Utils.successFlutterToast(msg: response.data);
          print(response.data);
          nameSkillController.clear();
          priceSkillController.clear();
        } else {
          Utils.errorFlutterToast(msg: response.message.toString());
        }
      }
    } catch (e) {
      Utils.errorSnackBar(message: e.toString());
    }
  }

  Future<void> mySkillTitlePost() async {
    try {
      if (titleFormKey.currentState!.saveAndValidate()) {
        var formData = {
          'desgination': titleController.text.toString(),
        };
        BaseResponse response = await DioClient().postRequest(
            endPoint: EndPoints.postDesignationUrl,
            body: formData,
            headers: {"Authorization": HiveHelper.getAuthToken()});
        if (response.error == false) {
          Utils.successFlutterToast(msg: response.data);
        } else {
          Utils.errorFlutterToast(msg: response.message.toString());
        }
      }
    } catch (e) {
      Utils.errorSnackBar(message: e.toString());
    }
  }

  Future<void> languagePost() async {
    try {
      if (languageFormKey.currentState!.saveAndValidate()) {
        var formData = {
          'languages': languageController.text.toString(),
        };
        BaseResponse response = await DioClient().postRequest(
            endPoint: EndPoints.postAddLanguage,
            body: formData,
            headers: {"Authorization": HiveHelper.getAuthToken()});
        if (response.error == false) {
          Utils.errorFlutterToast(msg: response.message.toString());
          languageController.clear();
          if (kDebugMode) {
            print(response.data);
          }
        } else {
          Utils.errorFlutterToast(msg: response.message.toString());
        }
      }
    } catch (e) {
      Utils.errorSnackBar(message: e.toString());
    }
  }

  Future<void> tradeCertificate() async {
    try {
      if (certificateFormKey.currentState!.saveAndValidate()) {
        var formData = {
          'certificates': certificateController.text.toString(),
        };
        BaseResponse response = await DioClient().postRequest(
            endPoint: EndPoints.postAddCertificate,
            body: formData,
            headers: {"Authorization": HiveHelper.getAuthToken()});
        if (response.error == false) {
          Utils.errorFlutterToast(msg: response.message.toString());
          if (kDebugMode) {
            print(response.data);
          }
        } else {
          Utils.errorFlutterToast(msg: response.message.toString());
        }
      }
    } catch (e) {
      Utils.errorSnackBar(message: e.toString());
    }
  }
}
