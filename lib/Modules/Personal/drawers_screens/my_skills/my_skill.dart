import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/my_skills/my_skills_controller.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:AwesomeSkills/widgets/button/custom_icon_button.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/image_slider_myskills_page_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/transport_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sizer/sizer.dart';

import '../../../../constants/app_fonts.dart';

class MySkillsScreen extends StatelessWidget {
  final controller = Get.put(MySkillsController());

  @override
  Widget build(BuildContext context) {
    return NestedScrollWidget(
      floatingActionButton: WidgetMethod.floatingActionButton(context),
      body: Padding(
          padding: EdgeInsets.only(left: 3.w, right: 2.w),
          child: SingleChildScrollView(
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              SizedBox(
                height: 2.h,
              ),
              Row(
                children: [
                  GestureDetector(
                    onTap: () => Get.back(),
                    child: Icon(
                      Icons.arrow_back_ios_new,
                      color: Colors.black,
                      size: 3.h,
                    ),
                  ),
                  SizedBox(
                    width: 5.w,
                  ),
                  const Text(
                    AppStrings.mySkills,
                    style: montserrat20TextStyle,
                  ),
                ],
              ),
              SizedBox(
                height: 2.h,
              ),
              Container(
                padding: EdgeInsets.only(left: 4.w, right: 2.w, top: 2.h),
                height: 10.h,
                width: 100.w,
                decoration: BoxDecoration(
                  color: AppColors.lightGreen2Color,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Obx(
                  () => controller.getUserInfo.value == null
                      ? const Text("Professional")
                      : Text(
                          controller.getUserInfo.value.designation ?? "",
                          style: orangeColor600TextStyle,
                        ),
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
              GetBuilder<MySkillsController>(builder: (_) {
                return Visibility(
                    visible: controller.deleteEditVisibleShow,
                    child: Padding(
                        padding: EdgeInsets.only(left: 2.2, right: 2.w),
                        child: FormBuilder(
                            key: controller.titleFormKey,
                            child: Column(children: [
                              TextFormField(
                                controller: controller.titleController,
                                decoration: InputDecoration(
                                  contentPadding:
                                      EdgeInsets.only(left: 2.w, right: 2.w),
                                  hintText: 'Professional',
                                  hintStyle: labelTextStyle,
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                ),
                              ),
                              SizedBox(
                                height: 3.h,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  CustomButton(
                                      buttonName: AppStrings.create,
                                      width: 25.w,
                                      height: 4.h,
                                      buttonColor: AppColors.indigoColor,
                                      textStyle: customButtonTextStyle,
                                      borderRadius: BorderRadius.circular(7),
                                      onTap: () {
                                        controller.mySkillTitlePost();
                                      }),
                                  SizedBox(
                                    width: 2.h,
                                  ),
                                  GetBuilder(
                                    init: MySkillsController(),
                                    builder: (_) {
                                      return CustomButton(
                                        buttonName: AppStrings.cancel,
                                        width: 25.w,
                                        height: 4.h,
                                        buttonColor: AppColors.grey4Color,
                                        textStyle: customButtonTextStyle,
                                        borderRadius: BorderRadius.circular(7),
                                        onTap: () =>
                                            controller.showInfoLanguageDetail(),
                                      );
                                    },
                                  ),
                                ],
                              ),
                            ]))));
              }),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    AppStrings.myAwesomeSkillText,
                    style: blue600ColorTextStyle,
                  ),
                  TextButton(
                    onPressed: () => controller.showEditDelete(),
                    child: const Text(
                      AppStrings.editSkill,
                      style: inter16TextStyle,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 1.h,
              ),
              const Text(
                AppStrings.top3Skills,
                style: inter14TextStyle,
              ),
              SizedBox(
                height: 2.h,
              ),
              Obx(() => controller.getUserInfo.value.skills!.length == 0
                  ? const Text("")
                  : Center(
                      child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          width: 25.w,
                          child: Text(
                            controller.getUserInfo.value.skills![0].skill ?? "",
                            overflow: TextOverflow.ellipsis,
                            style: blackBoldTextStyle,
                          ),
                        ),
                        SizedBox(
                          width: 10.w,
                        ),
                        SizedBox(
                          width: 20.w,
                          child: Text(
                            "\$ ${controller.getUserInfo.value.skills![0].hourRate} / hr" ??
                                "",
                            overflow: TextOverflow.ellipsis,
                            style: blackBoldTextStyle,
                          ),
                        ),
                        SizedBox(
                          width: 5.w,
                        ),
                        GetBuilder(
                            init: MySkillsController(),
                            builder: (_) {
                              return Visibility(
                                  visible: controller.deleteEditVisibleShow,
                                  child: Row(
                                    children: [
                                      GestureDetector(
                                        onTap: () {
                                          editFormField(
                                              context: context,
                                              createOnTap: () {},
                                              cancelOnTap: () {},
                                              initialSkill: controller
                                                      .getUserInfo
                                                      .value
                                                      .skills![0]
                                                      .skill ??
                                                  "",
                                              priceSkill: controller
                                                      .getUserInfo
                                                      .value
                                                      .skills![0]
                                                      .hourRate ??
                                                  "",
                                              userId: controller.getUserInfo
                                                  .value.skills![0].id,
                                              skillEditController: controller
                                                  .nameSkillController);
                                        },
                                        child: SvgPicture.asset(
                                          'assets/images/post_icon/edit_icon.svg',
                                          height: 3.h,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 5.w,
                                      ),
                                      GestureDetector(
                                        onTap: () => controller.deleteSkillUser(
                                            id: controller.getUserInfo.value
                                                .skills![0].id),
                                        child: SvgPicture.asset(
                                          'assets/images/post_icon/delete_icon.svg',
                                          height: 3.h,
                                        ),
                                      ),
                                    ],
                                  ));
                            })
                      ],
                    ))),
              Obx(() => controller.getUserInfo.value.skills!.length <= 1
                  ? const Text("")
                  : Center(
                      child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          width: 25.w,
                          child: Text(
                            controller.getUserInfo.value.skills![1].skill ?? "",
                            overflow: TextOverflow.ellipsis,
                            style: blackBoldTextStyle,
                          ),
                        ),
                        SizedBox(
                          width: 10.w,
                        ),
                        SizedBox(
                          width: 20.w,
                          child: Text(
                            "\$ ${controller.getUserInfo.value.skills![1].hourRate} / hr" ??
                                "",
                            overflow: TextOverflow.ellipsis,
                            style: blackBoldTextStyle,
                          ),
                        ),
                        SizedBox(
                          width: 5.w,
                        ),
                        GetBuilder(
                            init: MySkillsController(),
                            builder: (_) {
                              return Visibility(
                                  visible: controller.deleteEditVisibleShow,
                                  child: Row(
                                    children: [
                                      GestureDetector(
                                        onTap: () {
                                          // editFormField(
                                          //     context: context,
                                          //     createOnTap: () {},
                                          //     cancelOnTap: () {},
                                          //     userId: controller.getUserInfo
                                          //         .value.skills![1].id,
                                          //     skillEditController: controller
                                          //         .nameSkillController,
                                          //     initialPrice: controller
                                          //             .getUserInfo
                                          //             .value
                                          //             .skills![1]
                                          //             .hourRate ??
                                          //         "");
                                        },
                                        child: SvgPicture.asset(
                                          'assets/images/post_icon/edit_icon.svg',
                                          height: 3.h,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 5.w,
                                      ),
                                      GestureDetector(
                                        onTap: () => controller.deleteSkillUser(
                                            id: controller.getUserInfo.value
                                                .skills![1].id),
                                        child: SvgPicture.asset(
                                          'assets/images/post_icon/delete_icon.svg',
                                          height: 3.h,
                                        ),
                                      ),
                                    ],
                                  ));
                            })
                      ],
                    ))),
              Obx(() => controller.getUserInfo.value.skills!.length <= 2
                  ? Text("")
                  : Center(
                      child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          width: 25.w,
                          child: Text(
                            controller.getUserInfo.value.skills![2].skill ?? "",
                            overflow: TextOverflow.ellipsis,
                            style: blackBoldTextStyle,
                          ),
                        ),
                        SizedBox(
                          width: 10.w,
                        ),
                        SizedBox(
                          width: 20.w,
                          child: Text(
                            "\$ ${controller.getUserInfo.value.skills![2].hourRate} / hr" ??
                                "",
                            overflow: TextOverflow.ellipsis,
                            style: blackBoldTextStyle,
                          ),
                        ),
                        SizedBox(
                          width: 5.w,
                        ),
                        GetBuilder(
                            init: MySkillsController(),
                            builder: (_) {
                              return Visibility(
                                  visible: controller.deleteEditVisibleShow,
                                  child: Row(
                                    children: [
                                      GestureDetector(
                                        onTap: () {
                                          // editFormField(
                                          //     context: context,
                                          //     createOnTap: () {},
                                          //     cancelOnTap: () {},
                                          //     userId: controller.getUserInfo
                                          //         .value.skills![2].id,
                                          //     skillEditController: controller
                                          //         .nameSkillController,
                                          //     initialPrice: controller
                                          //             .getUserInfo
                                          //             .value
                                          //             .skills![2]
                                          //             .hourRate ??
                                          //         "");
                                        },
                                        child: SvgPicture.asset(
                                          'assets/images/post_icon/edit_icon.svg',
                                          height: 3.h,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 5.w,
                                      ),
                                      GestureDetector(
                                        onTap: () => controller.deleteSkillUser(
                                            id: controller.getUserInfo.value
                                                .skills![2].id),
                                        child: SvgPicture.asset(
                                          'assets/images/post_icon/delete_icon.svg',
                                          height: 3.h,
                                        ),
                                      ),
                                    ],
                                  ));
                            })
                      ],
                    ))),
              SizedBox(
                height: 2.h,
              ),
              CustomIconButton(
                  buttonName: AppStrings.addMoreSkills,
                  width: 45.w,
                  height: 6.h,
                  buttonColor: AppColors.indigoColor,
                  textStyle: customButtonTextStyle,
                  borderRadius: BorderRadius.circular(10),
                  onTap: () => controller.showInfoSkillDetail()),
              SizedBox(
                height: 2.h,
              ),
              GetBuilder<MySkillsController>(
                builder: (_) {
                  return Visibility(
                    visible: controller.isSkillVisible,
                    child: Padding(
                      padding: EdgeInsets.only(left: 2.w, right: 2.w),
                      child: FormBuilder(
                        key: controller.skillFormKey,
                        child: Column(
                          children: [
                            TextFormField(
                              controller: controller.nameSkillController,
                              decoration: InputDecoration(
                                contentPadding:
                                    EdgeInsets.only(left: 2.w, right: 2.w),
                                hintText: AppStrings.nameOfSkills,
                                hintStyle: labelTextStyle,
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10)),
                              ),
                            ),
                            SizedBox(
                              height: 2.h,
                            ),
                            TextFormField(
                              controller: controller.priceSkillController,
                              decoration: InputDecoration(
                                contentPadding:
                                    EdgeInsets.only(left: 2.w, right: 2.w),
                                hintText: AppStrings.enterYourPrice,
                                hintStyle: labelTextStyle,
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10)),
                              ),
                            ),
                            SizedBox(
                              height: 3.h,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                CustomButton(
                                    buttonName: AppStrings.create,
                                    width: 25.w,
                                    height: 4.h,
                                    buttonColor: AppColors.indigoColor,
                                    textStyle: customButtonTextStyle,
                                    borderRadius: BorderRadius.circular(7),
                                    onTap: () => controller.skillPost()),
                                SizedBox(
                                  width: 2.w,
                                ),
                                CustomButton(
                                    buttonName: AppStrings.cancel,
                                    width: 25.w,
                                    height: 4.h,
                                    buttonColor: AppColors.grey5Color,
                                    textStyle: customButtonTextStyle,
                                    borderRadius: BorderRadius.circular(7),
                                    onTap: () {}),
                              ],
                            ),
                            SizedBox(
                              height: 1.h,
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
              const Text(
                AppStrings.language,
                style: blue600ColorTextStyle,
              ),
              Obx(() => controller.getUserInfo.value.languages!.length == 0
                  ? const Text("")
                  : Center(
                      child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          width: 25.w,
                          child: Text(
                            controller.getUserInfo.value.languages![0]
                                    .languages ??
                                "",
                            overflow: TextOverflow.ellipsis,
                            style: blackBoldTextStyle,
                          ),
                        ),
                        SizedBox(width: 20.w),
                        GetBuilder(
                            init: MySkillsController(),
                            builder: (_) {
                              return Visibility(
                                  visible: controller.deleteEditVisibleShow,
                                  child: Row(
                                    children: [
                                      SvgPicture.asset(
                                        'assets/images/post_icon/edit_icon.svg',
                                        height: 3.h,
                                      ),
                                      SizedBox(
                                        width: 5.w,
                                      ),
                                      GestureDetector(
                                        onTap: () =>
                                            controller.deleteLanguageUser(
                                                id: controller.getUserInfo.value
                                                    .languages![0].id),
                                        child: SvgPicture.asset(
                                          'assets/images/post_icon/delete_icon.svg',
                                          height: 3.h,
                                        ),
                                      ),
                                    ],
                                  ));
                            })
                      ],
                    ))),
              Obx(() => controller.getUserInfo.value.languages!.length <= 1
                  ? const Text("")
                  : Center(
                      child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          width: 25.w,
                          child: Text(
                            controller.getUserInfo.value.languages![1]
                                    .languages ??
                                "",
                            overflow: TextOverflow.ellipsis,
                            style: blackBoldTextStyle,
                          ),
                        ),
                        SizedBox(width: 20.w),
                        GetBuilder(
                            init: MySkillsController(),
                            builder: (_) {
                              return Visibility(
                                  visible: controller.deleteEditVisibleShow,
                                  child: Row(
                                    children: [
                                      SvgPicture.asset(
                                        'assets/images/post_icon/edit_icon.svg',
                                        height: 3.h,
                                      ),
                                      SizedBox(
                                        width: 5.w,
                                      ),
                                      GestureDetector(
                                        onTap: () =>
                                            controller.deleteLanguageUser(
                                                id: controller.getUserInfo.value
                                                    .languages![1].id),
                                        child: SvgPicture.asset(
                                          'assets/images/post_icon/delete_icon.svg',
                                          height: 3.h,
                                        ),
                                      ),
                                    ],
                                  ));
                            })
                      ],
                    ))),
              Obx(() => controller.getUserInfo.value.languages!.length <= 2
                  ? Text("")
                  : Center(
                      child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          width: 25.w,
                          child: Text(
                            controller.getUserInfo.value.languages![2]
                                    .languages ??
                                "",
                            overflow: TextOverflow.ellipsis,
                            style: blackBoldTextStyle,
                          ),
                        ),
                        SizedBox(width: 20.w),
                        GetBuilder(
                            init: MySkillsController(),
                            builder: (_) {
                              return Visibility(
                                  visible: controller.deleteEditVisibleShow,
                                  child: Row(
                                    children: [
                                      SvgPicture.asset(
                                        'assets/images/post_icon/edit_icon.svg',
                                        height: 3.h,
                                      ),
                                      SizedBox(
                                        width: 5.w,
                                      ),
                                      GestureDetector(
                                        onTap: () =>
                                            controller.deleteLanguageUser(
                                                id: controller.getUserInfo.value
                                                    .languages![2].id),
                                        child: SvgPicture.asset(
                                          'assets/images/post_icon/delete_icon.svg',
                                          height: 3.h,
                                        ),
                                      ),
                                    ],
                                  ));
                            })
                      ],
                    ))),
              SizedBox(
                height: 1.h,
              ),
              CustomIconButton(
                buttonName: AppStrings.addWhatOtherLanguage,
                width: 55.w,
                height: 6.h,
                buttonColor: AppColors.indigoColor,
                textStyle: customButtonTextStyle,
                borderRadius: BorderRadius.circular(10),
                onTap: () => controller.showInfoLanguageDetail(),
              ),
              SizedBox(
                height: 2.h,
              ),
              GetBuilder(
                init: MySkillsController(),
                builder: (_) {
                  return Visibility(
                    visible: controller.isLanguageVisible,
                    child: Padding(
                      padding: EdgeInsets.only(left: 2.w, right: 2.w),
                      child: FormBuilder(
                        key: controller.languageFormKey,
                        child: Column(
                          children: [
                            TextFormField(
                              controller: controller.languageController,
                              decoration: InputDecoration(
                                contentPadding:
                                    EdgeInsets.only(left: 2.w, right: 2.w),
                                hintText: 'language',
                                hintStyle: labelTextStyle,
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10)),
                              ),
                            ),
                            SizedBox(
                              height: 3.h,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                CustomButton(
                                    buttonName: AppStrings.create,
                                    width: 25.w,
                                    height: 4.h,
                                    buttonColor: AppColors.indigoColor,
                                    textStyle: customButtonTextStyle,
                                    borderRadius: BorderRadius.circular(7),
                                    onTap: () {
                                      controller.languagePost();
                                      controller.certificateController.clear();
                                    }),
                                SizedBox(
                                  width: 2.h,
                                ),
                                GetBuilder(
                                  init: MySkillsController(),
                                  builder: (_) {
                                    return CustomButton(
                                      buttonName: AppStrings.cancel,
                                      width: 25.w,
                                      height: 4.h,
                                      buttonColor: AppColors.grey4Color,
                                      textStyle: customButtonTextStyle,
                                      borderRadius: BorderRadius.circular(7),
                                      onTap: () =>
                                          controller.showInfoLanguageDetail(),
                                    );
                                  },
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 1.h,
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
              SizedBox(
                height: 1.h,
              ),
              const Text(
                AppStrings.tradeCertificate,
                style: blue600ColorTextStyle,
              ),
              Obx(() => controller.getUserInfo.value.certificates!.length == 0
                  ? Text("")
                  : Center(
                      child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          width: 25.w,
                          child: Text(
                            controller.getUserInfo.value.certificates![0]
                                    .certificates ??
                                "",
                            overflow: TextOverflow.ellipsis,
                            style: blackBoldTextStyle,
                          ),
                        ),
                        SizedBox(width: 20.w),
                        GetBuilder(
                            init: MySkillsController(),
                            builder: (_) {
                              return Visibility(
                                  visible: controller.deleteEditVisibleShow,
                                  child: Row(
                                    children: [
                                      SvgPicture.asset(
                                        'assets/images/post_icon/edit_icon.svg',
                                        height: 3.h,
                                      ),
                                      SizedBox(
                                        width: 5.w,
                                      ),
                                      GestureDetector(
                                        onTap: () =>
                                            controller.deleteCertificateUser(
                                                id: controller.getUserInfo.value
                                                    .certificates![0].id),
                                        child: SvgPicture.asset(
                                          'assets/images/post_icon/delete_icon.svg',
                                          height: 3.h,
                                        ),
                                      ),
                                    ],
                                  ));
                            })
                      ],
                    ))),
              Obx(() => controller.getUserInfo.value.certificates!.length <= 1
                  ? const Text("")
                  : Center(
                      child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          width: 25.w,
                          child: Text(
                            controller.getUserInfo.value.certificates![1]
                                    .certificates ??
                                "",
                            overflow: TextOverflow.ellipsis,
                            style: blackBoldTextStyle,
                          ),
                        ),
                        SizedBox(width: 20.w),
                        GetBuilder(
                            init: MySkillsController(),
                            builder: (_) {
                              return Visibility(
                                  visible: controller.deleteEditVisibleShow,
                                  child: Row(
                                    children: [
                                      SvgPicture.asset(
                                        'assets/images/post_icon/edit_icon.svg',
                                        height: 3.h,
                                      ),
                                      SizedBox(
                                        width: 5.w,
                                      ),
                                      GestureDetector(
                                        onTap: () =>
                                            controller.deleteCertificateUser(
                                                id: controller.getUserInfo.value
                                                    .languages![1].id),
                                        child: SvgPicture.asset(
                                          'assets/images/post_icon/delete_icon.svg',
                                          height: 3.h,
                                        ),
                                      ),
                                    ],
                                  ));
                            })
                      ],
                    ))),
              Obx(() => controller.getUserInfo.value.certificates!.length <= 2
                  ? const Text("")
                  : Center(
                      child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          width: 25.w,
                          child: Text(
                            controller.getUserInfo.value.certificates![2]
                                    .certificates ??
                                "",
                            overflow: TextOverflow.ellipsis,
                            style: blackBoldTextStyle,
                          ),
                        ),
                        SizedBox(width: 20.w),
                        GetBuilder(
                            init: MySkillsController(),
                            builder: (_) {
                              return Visibility(
                                  visible: controller.deleteEditVisibleShow,
                                  child: Row(
                                    children: [
                                      SvgPicture.asset(
                                        'assets/images/post_icon/edit_icon.svg',
                                        height: 3.h,
                                      ),
                                      SizedBox(
                                        width: 5.w,
                                      ),
                                      GestureDetector(
                                        onTap: () =>
                                            controller.deleteCertificateUser(
                                                id: controller.getUserInfo.value
                                                    .languages![2].id),
                                        child: SvgPicture.asset(
                                          'assets/images/post_icon/delete_icon.svg',
                                          height: 3.h,
                                        ),
                                      ),
                                    ],
                                  ));
                            })
                      ],
                    ))),
              SizedBox(
                height: 1.h,
              ),
              CustomIconButton(
                  buttonName: AppStrings.addMore,
                  width: 40.w,
                  height: 6.h,
                  buttonColor: AppColors.indigoColor,
                  textStyle: customButtonTextStyle,
                  borderRadius: BorderRadius.circular(10),
                  onTap: () => controller.showInfoCertificateDetail()),
              SizedBox(
                height: 2.h,
              ),
              GetBuilder<MySkillsController>(
                builder: (_) {
                  return Visibility(
                    visible: controller.isCertificateVisible,
                    child: Padding(
                      padding: EdgeInsets.only(left: 2.2, right: 2.w),
                      child: FormBuilder(
                        key: controller.certificateFormKey,
                        child: Column(
                          children: [
                            TextFormField(
                              controller: controller.certificateController,
                              decoration: InputDecoration(
                                contentPadding:
                                    EdgeInsets.only(left: 2.w, right: 2.w),
                                hintText: 'certificate',
                                hintStyle: labelTextStyle,
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10)),
                              ),
                            ),
                            SizedBox(
                              height: 3.h,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                CustomButton(
                                    buttonName: AppStrings.create,
                                    width: 25.w,
                                    height: 4.h,
                                    buttonColor: AppColors.indigoColor,
                                    textStyle: customButtonTextStyle,
                                    borderRadius: BorderRadius.circular(7),
                                    onTap: () => controller.tradeCertificate()),
                                SizedBox(
                                  width: 2.h,
                                ),
                                CustomButton(
                                    buttonName: AppStrings.cancel,
                                    width: 25.w,
                                    height: 4.h,
                                    buttonColor: AppColors.grey4Color,
                                    textStyle: customButtonTextStyle,
                                    borderRadius: BorderRadius.circular(7),
                                    onTap: () {}),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
              SizedBox(
                height: 2.h,
              ),
              const Text(
                AppStrings.transport,
                style: blue600ColorTextStyle,
              ),
              SizedBox(
                height: 2.h,
              ),
              const TransportWidget(),
              SizedBox(
                height: 2.h,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    AppStrings.myWorksProject,
                    style: blue600ColorTextStyle,
                  ),
                  TextButton(
                    onPressed: () => controller.showAllSkillProjectDetails(),
                    child: const Text(
                      AppStrings.editSkill,
                      style: inter16TextStyle,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 2.h,
              ),
              GetBuilder(
                init: MySkillsController(),
                builder: (_) {
                  return Visibility(
                    visible: controller.isProjectTitleVisible,
                    child: Column(
                      children: [
                        TextFormField(
                          controller: controller.certificateController,
                          decoration: InputDecoration(
                            contentPadding:
                                EdgeInsets.only(left: 2.w, right: 2.w),
                            hintText: 'Project Title',
                            hintStyle: labelTextStyle,
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10)),
                          ),
                        ),
                        SizedBox(
                          height: 2.h,
                        ),
                        GestureDetector(
                          onTap: () async {
                            ImageSource? imageSource = await pickImage();
                            if (imageSource == null) return;
                            controller.selectImage(imageSource);
                          },
                          child: DottedBorder(
                            dashPattern: const [6, 2],
                            radius: const Radius.circular(10),
                            color: AppColors.indigoColor,
                            strokeWidth: 1.5,
                            child: Container(
                              height: 20.h,
                              width: 100.w,
                              decoration: const BoxDecoration(
                                color: Colors.transparent,
                              ),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SvgPicture.asset(
                                    'assets/images/post_icon/upload_icon.svg',
                                  ),
                                  SizedBox(
                                    height: 0.4.h,
                                  ),
                                  Text(
                                    AppStrings.dragAndDrop,
                                    style: blackBoldPoppinsColor,
                                  ),
                                  SizedBox(
                                    height: 0.4.h,
                                  ),
                                  Text(
                                    AppStrings.or,
                                    style: blackBoldPoppinsColor,
                                  ),
                                  SizedBox(
                                    height: 0.4.h,
                                  ),
                                  const Text(
                                    AppStrings.clickToBrowse,
                                    style: yellowPoppins300TextStyle,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 2.h,
                        ),
                        controller.projectImage != null
                            ? Container(
                                height: 20.h,
                                width: Get.width,
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                  image: FileImage(
                                    File(controller.projectImage!.path),
                                  ),
                                )),
                              )
                            : Container(),
                        SizedBox(
                          height: 3.h,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CustomButton(
                                buttonName: AppStrings.create,
                                width: 25.w,
                                height: 4.h,
                                buttonColor: AppColors.indigoColor,
                                textStyle: customButtonTextStyle,
                                borderRadius: BorderRadius.circular(7),
                                onTap: () => controller.tradeCertificate()),
                            SizedBox(
                              width: 2.h,
                            ),
                            CustomButton(
                                buttonName: AppStrings.cancel,
                                width: 25.w,
                                height: 4.h,
                                buttonColor: AppColors.grey4Color,
                                textStyle: customButtonTextStyle,
                                borderRadius: BorderRadius.circular(7),
                                onTap: () {}),
                          ],
                        ),
                      ],
                    ),
                  );
                },
              ),
              SizedBox(
                height: 2.h,
              ),
              ImageSliderMySkillPageWidget(),
              SizedBox(
                height: 3.h,
              ),
            ]),
          )),
    );
  }

  Future<ImageSource?> pickImage() async {
    ImageSource? selectedSource = await Get.defaultDialog(
      title: 'Select From',
      content: Column(
        children: [
          GestureDetector(
            onTap: () {
              Get.back(result: ImageSource.gallery);
            },
            child: const ListTile(
              leading: Icon(Icons.image),
              title: Text(
                'Gallery',
                style: TextStyle(
                    color: AppColors.textBlack, fontFamily: AppFonts.inter),
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              Get.back(result: ImageSource.camera);
            },
            child: const ListTile(
              leading: Icon(Icons.photo_camera),
              title: Text(
                'Camera',
                style: TextStyle(
                  color: AppColors.textBlack,
                  fontFamily: AppFonts.inter,
                ),
              ),
            ),
          ),
        ],
      ),
    );
    return selectedSource;
  }

  static editFormField({
    required BuildContext context,
    required Function createOnTap,
    required Function cancelOnTap,
    required String initialSkill,
    required String priceSkill,
    required TextEditingController skillEditController,
    required userId,
  }) {
    final skillController = Get.put(MySkillsController());
    return showModalBottomSheet(
        backgroundColor: Colors.transparent,
        context: context,
        builder: (context) => Container(
              height: 220,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "Edit",
                    style: blackBold16Color,
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Padding(
                      padding: EdgeInsets.only(left: 5.w, right: 5.w),
                      child: FormBuilder(
                          child: Column(children: [
                        TextFormField(
                          initialValue: initialSkill,
                          //   initialValue: initialSkill,
                          decoration: InputDecoration(
                            contentPadding:
                                EdgeInsets.only(left: 2.w, right: 2.w),
                            hintStyle: labelTextStyle,
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10)),
                          ),
                        ),
                        SizedBox(
                          height: 2.h,
                        ),
                        TextFormField(
                          initialValue: priceSkill,
                          decoration: InputDecoration(
                            contentPadding:
                                EdgeInsets.only(left: 2.w, right: 2.w),
                            hintStyle: labelTextStyle,
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10)),
                          ),
                        ),
                        SizedBox(
                          height: 3.h,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CustomButton(
                                buttonName: AppStrings.create,
                                width: 25.w,
                                height: 4.h,
                                buttonColor: AppColors.indigoColor,
                                textStyle: customButtonTextStyle,
                                borderRadius: BorderRadius.circular(7),
                                onTap: () => skillController.skillEditPost(
                                    id: userId,
                                    initialSkill: initialSkill,
                                    priceSkill: priceSkill)),
                            SizedBox(
                              width: 2.h,
                            ),
                            CustomButton(
                                buttonName: AppStrings.cancel,
                                width: 25.w,
                                height: 4.h,
                                buttonColor: AppColors.grey4Color,
                                textStyle: customButtonTextStyle,
                                borderRadius: BorderRadius.circular(7),
                                onTap: () {})
                          ],
                        ),
                      ])))
                ],
              ),
            ));
  }
}
