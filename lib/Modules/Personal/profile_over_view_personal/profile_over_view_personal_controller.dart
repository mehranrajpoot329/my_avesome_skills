import 'package:AwesomeSkills/Modules/Personal/ad_Screen/ad_screeb_can_help_personal/ad_screen_can_help_personal.dart';
import 'package:get/get.dart';

class ProfileOverViewPersonalController extends GetxController {
  final selectAll = CheckBoxModal(title: 'Not Selected');

  final checkBoxList = [
    CheckBoxModal(title: 'Selected'),
    CheckBoxModal(title: 'Indeterminate'),
    CheckBoxModal(title: 'Disabled'),
    CheckBoxModal(title: 'Disabled selected'),
    CheckBoxModal(title: 'Disabled Indeterminate'),
  ];

  changeCheckBoxSelectIndustries(CheckBoxModal checkBoxModal) {
    checkBoxModal.value = !checkBoxModal.value;
    update();
  }

  changeCheckBoxSelectItemIndustries(CheckBoxModal checkBoxModal) {
    checkBoxModal.value = !checkBoxModal.value;
    update();
  }
}
