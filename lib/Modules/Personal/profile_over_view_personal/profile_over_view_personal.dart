import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Modules/Personal/ad_Screen/ad_screeb_can_help_personal/ad_screen_can_help_personal.dart';
import 'package:AwesomeSkills/Modules/Personal/profile_over_view_personal/profile_over_view_personal_controller.dart';
import 'package:AwesomeSkills/constants/app_fonts.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/image_slider_myskills_page_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:sizer/sizer.dart';

import '../../../Constants/text_style.dart';
import '../../../constants/app_colors.dart';
import '../../../constants/app_strings.dart';

class ProfileOverViewPersonal extends StatelessWidget {
  ProfileOverViewPersonal({Key? key}) : super(key: key);
  final controller = Get.put(ProfileOverViewPersonalController());

  @override
  Widget build(BuildContext context) {
    return NestedScrollWidget(
        floatingActionButton: WidgetMethod.floatingActionButton(context),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                  padding: EdgeInsets.only(left: 2.w, right: 2.w),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          height: 2.h,
                        ),
                        Container(
                            padding: EdgeInsets.only(
                                left: 4.w, right: 2.w, top: 2.h),
                            height: 10.h,
                            width: 100.w,
                            decoration: BoxDecoration(
                              color: AppColors.lightGreen2Color,
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Text(
                              AppStrings.welcomeTpSpacePencil,
                              style: orange600ColorTextStyle,
                            )),
                        SizedBox(
                          height: 2.h,
                        ),
                        Row(
                          children: [
                            buildWidgetLikeJobProgress(
                                iconString:
                                    'assets/images/post_icon/thumb_icon.svg',
                                text: AppStrings.oneFortyTwoLikes),
                            SizedBox(
                              width: 5.w,
                            ),
                            buildWidgetLikeJobProgress(
                                iconString:
                                    'assets/images/post_icon/group_person_blue.svg',
                                text: AppStrings.zeroNineJobsInProgress),
                          ],
                        ),
                        SizedBox(
                          height: 2.h,
                        ),
                        buildWidgetLikeJobProgress(
                            iconString:
                                'assets/images/post_icon/group_person_blue.svg',
                            text: AppStrings.tenFinishedJob),
                        SizedBox(
                          height: 2.h,
                        ),
                        const Text(
                          AppStrings.meetTheTeam,
                          style: blue500ColorTextStyle,
                        ),
                        SizedBox(
                          height: 2.h,
                        ),
                      ])),
              Row(
                children: [
                  Expanded(
                      child: buildWidgetMeetTeam(
                          text: AppStrings.ceo,
                          isRightImage: false,
                          imagePath: 'assets/images/ceo_profile_pics.png')),
                  SizedBox(
                    width: 10.w,
                  ),
                  Expanded(
                      child: buildWidgetMeetTeam(
                          text: AppStrings.stateManager,
                          isRightImage: true,
                          imagePath:
                              'assets/images/profile_state_manager.png')),
                ],
              ),
              SizedBox(
                height: 2.h,
              ),
              Row(
                children: [
                  Expanded(
                      child: buildWidgetMeetTeam(
                          text: AppStrings.customerHelp,
                          isRightImage: false,
                          imagePath:
                              'assets/images/profile_customer_help.png')),
                  SizedBox(
                    width: 10.w,
                  ),
                  Expanded(
                      child: buildWidgetMeetTeam(
                          text: AppStrings.director,
                          isRightImage: true,
                          imagePath: 'assets/images/profile_director.png')),
                ],
              ),
              SizedBox(
                height: 2.h,
              ),
              Padding(
                padding: EdgeInsets.only(left: 2.w, right: 2.w),
                child: const Text(
                  AppStrings.companyLife,
                  style: blue500ColorTextStyle,
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
              Row(
                children: [
                  Expanded(
                      child: buildCompanyLifePicture(
                          isRightImage: false,
                          imageUrl: 'assets/images/spray_bottle.png')),
                  SizedBox(
                    width: 10.w,
                  ),
                  Expanded(
                      child: buildCompanyLifePicture(
                          isRightImage: true,
                          imageUrl: 'assets/images/spray_bottle2.png')),
                ],
              ),
              SizedBox(
                height: 2.h,
              ),
              Row(
                children: [
                  Expanded(
                      child: buildCompanyLifePicture(
                          isRightImage: false,
                          imageUrl: 'assets/images/dustbin.png')),
                  SizedBox(
                    width: 10.w,
                  ),
                  Expanded(
                      child: buildCompanyLifePicture(
                          isRightImage: true,
                          imageUrl: 'assets/images/hand_gloves.png')),
                ],
              ),
              SizedBox(
                height: 2.h,
              ),
              Padding(
                padding: EdgeInsets.only(left: 2.w, right: 2.w),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      AppStrings.aboutUs,
                      style: blue500ColorTextStyle,
                    ),
                    Text(
                      AppStrings.loremIpsumText,
                      style: blackSixHundredWeightColor,
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    const Text(
                      AppStrings.product,
                      style: blue500ColorTextStyle,
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    Center(
                      child: Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 4.w, vertical: 2.h),
                          height: 50.h,
                          width: 90.w,
                          decoration: BoxDecoration(
                              color: AppColors.lightBlue2Color,
                              borderRadius: BorderRadius.circular(20),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.1),
                                  blurRadius: 4.0,
                                  spreadRadius: 2.0,
                                )
                              ]),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                    child: Scrollbar(
                                  thickness: 5,
                                  radius: const Radius.circular(5),
                                  thumbVisibility: false,
                                  child: ListView(
                                    children: [
                                      InkWell(
                                        onTap: () => controller
                                            .changeCheckBoxSelectIndustries(
                                                controller.selectAll),
                                        child: Row(
                                          children: [
                                            GetBuilder<
                                                ProfileOverViewPersonalController>(
                                              builder: (_) {
                                                return Checkbox(
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5)),
                                                  side: const BorderSide(
                                                      color: AppColors
                                                          .lightBlueColor,
                                                      width: 0.5),
                                                  checkColor: Colors.white,
                                                  activeColor:
                                                      AppColors.lightBlueColor,
                                                  value: controller
                                                      .selectAll.value,
                                                  onChanged: (value) => controller
                                                      .changeCheckBoxSelectIndustries(
                                                          controller.selectAll),
                                                );
                                              },
                                            ),
                                            SizedBox(
                                              width: 3.w,
                                            ),
                                            Text(
                                              controller.selectAll.title,
                                              style: blackBold16Color,
                                            ),
                                          ],
                                        ),
                                      ),
                                      ...controller.checkBoxList.map(
                                        (item) => InkWell(
                                          onTap: () => controller
                                              .changeCheckBoxSelectItemIndustries(
                                                  item),
                                          child: Row(
                                            children: [
                                              GetBuilder<
                                                  ProfileOverViewPersonalController>(
                                                builder: (_) {
                                                  return Checkbox(
                                                    shape:
                                                        RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        5)),
                                                    side: const BorderSide(
                                                        color: AppColors
                                                            .lightBlueColor,
                                                        width: 0.5),
                                                    checkColor: Colors.white,
                                                    activeColor: AppColors
                                                        .lightBlueColor,
                                                    value: item.value,
                                                    onChanged: (value) => controller
                                                        .changeCheckBoxSelectItemIndustries(
                                                            item),
                                                  );
                                                },
                                              ),
                                              SizedBox(
                                                width: 3.w,
                                              ),
                                              Text(
                                                item.title,
                                                style: blackBold16Color,
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ))
                              ])),
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    const Text(
                      AppStrings.services,
                      style: blue500ColorTextStyle,
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    Padding(
                      padding: EdgeInsets.all(2.w),
                      child: Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 4.w, vertical: 2.h),
                          height: 50.h,
                          width: 90.w,
                          decoration: BoxDecoration(
                              color: AppColors.lightBlue2Color,
                              borderRadius: BorderRadius.circular(20),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.1),
                                  blurRadius: 4.0,
                                  spreadRadius: 2.0,
                                )
                              ]),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                    child: Scrollbar(
                                  thickness: 5,
                                  radius: const Radius.circular(5),
                                  thumbVisibility: false,
                                  child: ListView(
                                    children: [
                                      InkWell(
                                        onTap: () => controller
                                            .changeCheckBoxSelectIndustries(
                                                controller.selectAll),
                                        child: Row(
                                          children: [
                                            GetBuilder<
                                                ProfileOverViewPersonalController>(
                                              builder: (_) {
                                                return Checkbox(
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5)),
                                                  side: const BorderSide(
                                                      color: AppColors
                                                          .lightBlueColor,
                                                      width: 0.5),
                                                  checkColor: Colors.white,
                                                  activeColor:
                                                      AppColors.lightBlueColor,
                                                  value: controller
                                                      .selectAll.value,
                                                  onChanged: (value) => controller
                                                      .changeCheckBoxSelectIndustries(
                                                          controller.selectAll),
                                                );
                                              },
                                            ),
                                            SizedBox(
                                              width: 3.w,
                                            ),
                                            Text(
                                              controller.selectAll.title,
                                              style: blackBold16Color,
                                            ),
                                          ],
                                        ),
                                      ),
                                      ...controller.checkBoxList.map(
                                        (item) => InkWell(
                                          onTap: () => controller
                                              .changeCheckBoxSelectItemIndustries(
                                                  item),
                                          child: Row(
                                            children: [
                                              GetBuilder<
                                                  ProfileOverViewPersonalController>(
                                                builder: (_) {
                                                  return Checkbox(
                                                    shape:
                                                        RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        5)),
                                                    side: const BorderSide(
                                                        color: AppColors
                                                            .lightBlueColor,
                                                        width: 0.5),
                                                    checkColor: Colors.white,
                                                    activeColor: AppColors
                                                        .lightBlueColor,
                                                    value: item.value,
                                                    onChanged: (value) => controller
                                                        .changeCheckBoxSelectItemIndustries(
                                                            item),
                                                  );
                                                },
                                              ),
                                              SizedBox(
                                                width: 3.w,
                                              ),
                                              Text(
                                                item.title,
                                                style: blackBold16Color,
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ))
                              ])),
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    const Text(
                      AppStrings.projects,
                      style: blue500ColorTextStyle,
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    ImageSliderMySkillPageWidget(),
                  ],
                ),
              ),
              SizedBox(
                height: 3.h,
              ),
            ],
          ),
        ));
  }

  Widget buildWidgetLikeJobProgress(
      {required String iconString, required String text}) {
    return Container(
      padding: EdgeInsets.only(left: 7.w, right: 7.w),
      height: 15.h,
      width: 45.w,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
                offset: Offset(0, 2),
                blurRadius: 1.0,
                spreadRadius: 1.0,
                color: Colors.grey..withOpacity(0.1))
          ]),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SvgPicture.asset(
            iconString,
            height: 3.h,
            width: 3.h,
          ),
          SizedBox(
            height: 1.h,
          ),
          FittedBox(child: Text(text, style: indigo14ColorTextStyle))
        ],
      ),
    );
  }

  Widget buildWidgetMeetTeam(
      {required String imagePath,
      required bool isRightImage,
      required String text}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          height: 17.h,
          width: double.infinity,
          decoration: BoxDecoration(
            borderRadius: isRightImage
                ? const BorderRadius.only(
                    topLeft: Radius.circular(10),
                    bottomLeft: Radius.circular(10),
                  )
                : const BorderRadius.only(
                    topRight: Radius.circular(10),
                    bottomRight: Radius.circular(10),
                  ),
            image: DecorationImage(
                image: AssetImage(imagePath), fit: BoxFit.cover),
          ),
        ),
        SizedBox(
          height: 1.h,
        ),
        Align(
          alignment: Alignment.center,
          child: Text(
            text,
            style: black14Mulish1TextStyle,
          ),
        ),
      ],
    );
  }

  Widget buildCompanyLifePicture(
      {required String imageUrl, required bool isRightImage}) {
    return Container(
      height: 17.h,
      width: 30.w,
      decoration: BoxDecoration(
          borderRadius: isRightImage
              ? const BorderRadius.only(
                  topLeft: Radius.circular(10),
                  bottomLeft: Radius.circular(10),
                )
              : const BorderRadius.only(
                  topRight: Radius.circular(10),
                  bottomRight: Radius.circular(10),
                ),
          image:
              DecorationImage(image: AssetImage(imageUrl), fit: BoxFit.cover)),
    );
  }
}
