import 'dart:developer';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Modules/Personal/ad_Screen/custom_ad_info_text_form_field.dart';
import 'package:AwesomeSkills/Modules/Personal/personal_info_for_post_ad/personal_info_for_post_controller.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:AwesomeSkills/widgets/custom_textfield/custom_textfield.dart';
import 'package:AwesomeSkills/widgets/drop_down.dart';
import 'package:sizer/sizer.dart';

import '../../../Constants/app_colors.dart';
import '../../../Constants/text_style.dart';
import '../../../constants/app_strings.dart';

class PersonalInfoExpansionTile extends StatelessWidget {
  PersonalInfoExpansionTile({Key? key}) : super(key: key);
  final controller = Get.put(PersonalInfoForPostController());

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 1.h,
        ),
        ExpansionTile(
          title: Text(
            AppStrings.applicantDetails,
            style: lightBlue500TextStyle,
          ),
          children: [
            ListTile(
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomAdInfoTextFormField(
                      isOptionalText: false,
                      titleTextStyle: lightBlue500TextStyle,
                      height: 7.h,
                      width: 100.w,
                      title: AppStrings.fullName,
                      hintText: AppStrings.fullName,
                      controller: controller.industryController),
                  SizedBox(
                    height: 1.h,
                  ),
                  CustomAdInfoTextFormField(
                      isOptionalText: false,
                      titleTextStyle: lightBlue500TextStyle,
                      height: 7.h,
                      width: 100.w,
                      title: AppStrings.profileUserName,
                      hintText: AppStrings.ownerDirectorCeo,
                      controller: controller.industryController),
                  SizedBox(
                    height: 2.h,
                  ),
                  ExpansionTile(
                      tilePadding: EdgeInsets.zero,
                      title: Text(
                        AppStrings.createPassword,
                        style: red4ColorTextStyle,
                      ),
                      children: [
                        CustomTextFormField(
                            height: 7.h,
                            width: 100.w,
                            borderRadius: BorderRadius.circular(10),
                            hintTextStyle: black12Color,
                            hintText: AppStrings.newPassword,
                            name: 'new_password',
                            color: AppColors.lightBlue6Color,
                            validator: (String? value) {},
                            controller: controller.administratorController),
                        SizedBox(
                          height: 2.h,
                        ),
                        CustomTextFormField(
                            height: 7.h,
                            width: 100.w,
                            borderRadius: BorderRadius.circular(10),
                            hintTextStyle: black12Color,
                            hintText: AppStrings.confirmPassword,
                            name: 'confirm_password',
                            color: AppColors.lightBlue6Color,
                            validator: (String? value) {},
                            controller: controller.administratorController),
                        SizedBox(
                          height: 3.h,
                        ),
                        CustomButton(
                            buttonName: AppStrings.change,
                            width: 30.w,
                            height: 5.h,
                            buttonColor: AppColors.lightGreen3Color,
                            textStyle: black14Color,
                            borderRadius: BorderRadius.circular(7),
                            onTap: () {}),
                        SizedBox(
                          height: 2.h,
                        ),
                      ]),
                  SizedBox(
                    height: 2.h,
                  ),
                  CustomAdInfoTextFormField(
                      isOptionalText: false,
                      titleTextStyle: lightBlue500TextStyle,
                      height: 7.h,
                      width: 100.w,
                      title: AppStrings.dateOfBirth,
                      hintText: '',
                      controller: controller.industryController),
                  SizedBox(
                    height: 2.h,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        AppStrings.profilePicture,
                        style: lightBlue500TextStyle,
                      ),
                      DottedBorder(
                        dashPattern: const [6, 2],
                        radius: const Radius.circular(10),
                        color: AppColors.indigoColor,
                        strokeWidth: 1.5,
                        child: Container(
                          padding:
                              EdgeInsets.only(top: 2.h, left: 1.w, right: 1.w),
                          height: 15.h,
                          width: 40.w,
                          decoration: const BoxDecoration(
                            color: Colors.transparent,
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SvgPicture.asset(
                                'assets/images/post_icon/upload_icon.svg',
                              ),
                              const SizedBox(
                                height: 2,
                              ),
                              const FittedBox(
                                child: Text(
                                  AppStrings.dragAndDrop,
                                  style: black9PoppinsColor,
                                ),
                              ),
                              SizedBox(
                                height: 0.3.h,
                              ),
                              const Text(
                                AppStrings.or,
                                style: black9PoppinsColor,
                              ),
                              const SizedBox(
                                height: 2,
                              ),
                              const Text(
                                AppStrings.clickToBrowse,
                                style: yellowPoppins300TextStyle,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 2.h,
            )
          ],
        ),
        ExpansionTile(
          title: Text(
            AppStrings.tradingName,
            style: lightBlue500TextStyle,
          ),
          children: [
            Padding(
              padding: EdgeInsets.only(left: 2.w, right: 2.w),
              child: CustomTextFormField(
                  height: 7.h,
                  width: 100.w,
                  borderRadius: BorderRadius.circular(10),
                  hintTextStyle: black12Color,
                  hintText: AppStrings.thisIsTheNameTo,
                  name: 'trading_name',
                  color: AppColors.lightBlue6Color,
                  validator: (String? value) {},
                  controller: controller.administratorController),
            ),
            SizedBox(
              height: 2.h,
            ),
          ],
        ),
        ExpansionTile(
          title: Text(
            AppStrings.subrub,
            style: lightBlue500TextStyle,
          ),
          children: [
            SizedBox(
              height: 2.h,
            ),
            Padding(
              padding: EdgeInsets.only(left: 2.w, right: 2.w),
              child: CustomDropDown(
                height: 7.h,
                width: 100.w,
                hint: AppStrings.category,
                items: const <String>[
                  'House Cleaning',
                  'End of Lease Cleaning',
                  'Carpet Steam Cleaning',
                  'Window Cleaning',
                ],
                selectedValue: (value) {
                  controller.subrub.value = value!;
                  log(value.toString());
                },
              ),
            ),
            SizedBox(
              height: 2.h,
            )
          ],
        ),
        ExpansionTile(
          title: Text(
            AppStrings.address,
            style: lightBlue500TextStyle,
          ),
          children: [
            Padding(
              padding: EdgeInsets.only(left: 2.w, right: 2.w),
              child: CustomTextFormField(
                  height: 7.h,
                  width: 100.w,
                  borderRadius: BorderRadius.circular(10),
                  hintTextStyle: black12Color,
                  hintText: AppStrings.noPoBoxAccepted,
                  name: 'address',
                  color: AppColors.lightBlue6Color,
                  validator: (String? value) {},
                  controller: controller.administratorController),
            ),
            SizedBox(
              height: 2.h,
            )
          ],
        ),
        ExpansionTile(
          title: Text(
            AppStrings.state,
            style: lightBlue500TextStyle,
          ),
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: Padding(
                padding: EdgeInsets.only(left: 2.w, right: 2.w),
                child: CustomTextFormField(
                    height: 7.h,
                    width: 100.w,
                    borderRadius: BorderRadius.circular(10),
                    hintTextStyle: black12Color,
                    hintText: AppStrings.QLD,
                    name: 'state',

                    color: AppColors.lightBlue6Color,

                    validator: (String? value) {},
                    controller: controller.administratorController),
              ),
            ),
            SizedBox(
              height: 2.h,
            )
          ],
        ),
        ExpansionTile(
          title: Text(
            AppStrings.postCode,
            style: lightBlue500TextStyle,
          ),
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: Padding(
                padding: EdgeInsets.only(left: 2.w, right: 2.w),
                child: CustomTextFormField(
                    height: 7.h,
                    width: 100.w,
                    borderRadius: BorderRadius.circular(10),
                    hintTextStyle: black12Color,
                    hintText: '',
                    name: 'post_code',

                    color: AppColors.lightBlue6Color,

                    validator: (String? value) {},
                    controller: controller.administratorController),
              ),
            ),
            SizedBox(
              height: 2.h,
            )
          ],
        ),
        ExpansionTile(
          title: Text(
            AppStrings.mobile,
            style: lightBlue500TextStyle,
          ),
          children: [
            Align(
                alignment: Alignment.topLeft,
                child: Padding(
                  padding: EdgeInsets.only(left: 2.w, right: 2.w),
                  child: CustomTextFormField(
                      height: 7.h,
                      width: 100.w,

                      borderRadius: BorderRadius.circular(10),
                      hintTextStyle: black12Color,
                      hintText: '',
                      name: 'mobile',
                      color: AppColors.lightBlue6Color,
                      validator: (String? value) {},
                      controller: controller.administratorController),
                )),
            SizedBox(
              height: 2.h,
            )
          ],
        ),
        ExpansionTile(
          title: Text(
            AppStrings.email,
            style: lightBlue500TextStyle,
          ),
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: Padding(
                padding: EdgeInsets.only(left: 2.w, right: 2.w),
                child: CustomTextFormField(
                    height: 7.h,
                    width: 100.w,
                    borderRadius: BorderRadius.circular(10),
                    hintTextStyle: black12Color,
                    hintText: '',
                    name: 'email',
                    color: AppColors.lightBlue6Color,
                    validator: (String? value) {},
                    controller: controller.administratorController),
              ),
            ),
            SizedBox(
              height: 2.h,
            )
          ],
        ),
        ExpansionTile(
          title: Text(
            AppStrings.industry,
            style: lightBlue500TextStyle,
          ),
          children: [
            Padding(
              padding: EdgeInsets.only(left: 2.w, right: 2.w),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomDropDown(
                    height: 7.h,
                    width: 100.w,
                    hint: AppStrings.category,
                    items: const <String>[
                      'House Cleaning',
                      'End of Lease Cleaning',
                      'Carpet Steam Cleaning',
                      'Window Cleaning',
                    ],
                    selectedValue: (value) {
                      controller.subrub.value = value!;
                      log(value.toString());
                    },
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Center(
                    child: Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: 4.w, vertical: 2.h),
                        height: 40.h,
                        width: 95.w,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.1),
                                blurRadius: 4.0,
                                spreadRadius: 2.0,
                              )
                            ]),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                AppStrings.selectIndustries,
                                style: lightBlue500TextStyle,
                              ),
                              SizedBox(
                                height: 0.5.h,
                              ),
                              const Divider(
                                color: AppColors.lightBlueColor,
                              ),
                              Expanded(
                                  child: Scrollbar(
                                thickness: 5,
                                radius: const Radius.circular(5),
                                thumbVisibility: false,
                                child: ListView(
                                  children: [
                                    InkWell(
                                      onTap: () => controller
                                          .changeCheckBoxSelectIndustries(
                                              controller.selectCategory),
                                      child: Row(
                                        children: [
                                          GetBuilder<
                                              PersonalInfoForPostController>(
                                            builder: (_) {
                                              return Checkbox(
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            5)),
                                                side: const BorderSide(
                                                    color: AppColors
                                                        .lightBlueColor,
                                                    width: 0.5),
                                                checkColor: Colors.white,
                                                activeColor:
                                                    AppColors.lightBlueColor,
                                                value:
                                                    controller.selectAll.value,
                                                onChanged: (value) => controller
                                                    .changeCheckBoxSelectIndustries(
                                                        controller
                                                            .selectCategory),
                                              );
                                            },
                                          ),
                                          SizedBox(
                                            width: 3.w,
                                          ),
                                          Text(
                                            controller.selectAll.title,
                                            style: blackBold16Color,
                                          ),
                                        ],
                                      ),
                                    ),
                                    ...controller.checkBoxList.map(
                                      (item) => InkWell(
                                        onTap: () => controller
                                            .changeCheckBoxSelectItemIndustries(
                                                item),
                                        child: Row(
                                          children: [
                                            GetBuilder<
                                                PersonalInfoForPostController>(
                                              builder: (_) {
                                                return Checkbox(
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5)),
                                                  side: const BorderSide(
                                                      color: AppColors
                                                          .lightBlueColor,
                                                      width: 0.5),
                                                  checkColor: Colors.white,
                                                  activeColor:
                                                      AppColors.lightBlueColor,
                                                  value: item.value,
                                                  onChanged: (value) => controller
                                                      .changeCheckBoxSelectItemIndustries(
                                                          item),
                                                );
                                              },
                                            ),
                                            SizedBox(
                                              width: 3.w,
                                            ),
                                            Text(
                                              item.title,
                                              style: blackBold16Color,
                                            ),
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ))
                            ])),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 2.h,
            )
          ],
        ),
        SizedBox(
          height: 2.h,
        ),
      ],
    );
  }
}
