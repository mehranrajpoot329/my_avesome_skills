import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Modules/Personal/ad_Screen/ad_screeb_can_help_personal/ad_screen_can_help_personal.dart';

class PersonalInfoForPostController extends GetxController {
  TextEditingController industryController = TextEditingController();
  TextEditingController administratorController = TextEditingController();
  RxString subrub = ''.obs;
  int travelingSlider = 3;
  RxString category = ''.obs;
  final selectAll = CheckBoxModal(title: 'Select All');

  final selectCategory = CheckBoxModal(
    title: 'title',
  );
  final checkBoxList = [
    CheckBoxModal(title: 'House Cleaning'),
    CheckBoxModal(title: 'End of Lease Cleaning'),
    CheckBoxModal(title: 'Carpet Steam Cleaning'),
    CheckBoxModal(title: 'Window Cleaning'),
    CheckBoxModal(title: 'Outdoor Cleaning'),
    CheckBoxModal(title: 'Office Cleaning'),
    CheckBoxModal(title: 'High Pressure Cleaning'),
    CheckBoxModal(title: 'Upholstery Cleaning'),
  ];

  void travelingSliderFunc(double newValue) {
    travelingSlider = newValue.round();
    update();
  }

  changeCheckBoxSelectIndustries(CheckBoxModal checkBoxModal) {
    checkBoxModal.value = !checkBoxModal.value;
    update();
  }

  changeCheckBoxSelectItemIndustries(CheckBoxModal checkBoxModal) {
    checkBoxModal.value = !checkBoxModal.value;
    update();
  }
}
