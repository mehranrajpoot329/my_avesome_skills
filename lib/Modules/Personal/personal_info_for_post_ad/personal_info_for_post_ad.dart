import 'package:AwesomeSkills/constants/app_colors.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../../../Constants/text_style.dart';
import '../../../constants/app_strings.dart';
import '../../../widgets/button/custom_button.dart';
import 'personal_info_expansion_tile.dart';

class PersonalInfoForPostAd extends StatelessWidget {
  const PersonalInfoForPostAd({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NestedScrollWidget(
      floatingActionButton: WidgetMethod.floatingActionButton(context),
      body: Padding(
          padding: EdgeInsets.only(left: 1.w, right: 1.w),
          child: ListView(children: [
            Container(
              padding: EdgeInsets.symmetric(horizontal: 2.w),
              height: 8.h,
              decoration: BoxDecoration(
                  color: AppColors.lightGreen2Color,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                        blurRadius: 4.0,
                        spreadRadius: 2.0,
                        color: Colors.grey.withOpacity(0.2))
                  ]),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    AppStrings.createPersonalAccount,
                    style: orange600ColorTextStyle,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            PersonalInfoExpansionTile(),
            SizedBox(
              height: 2.h,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CustomButton(
                    buttonName: AppStrings.saveForLater,
                    width: 30.w,
                    height: 5.h,
                    buttonColor: AppColors.lightBlue2Color,
                    textStyle: black14Color,
                    borderRadius: BorderRadius.circular(6),
                    onTap: () {}),
                SizedBox(
                  width: 3.w,
                ),
                CustomButton(
                    buttonName: AppStrings.finish,
                    width: 30.w,
                    height: 5.h,
                    buttonColor: AppColors.green5Color,
                    textStyle: black14Color,
                    borderRadius: BorderRadius.circular(6),
                    onTap: () {}),
              ],
            ),
            SizedBox(
              height: 4.h,
            )
          ])),
    );
  }
}
