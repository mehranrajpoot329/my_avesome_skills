import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:AwesomeSkills/local/db/local_storage.dart';
import 'package:AwesomeSkills/models/api_model/personal_model/ad_need_help_personal_post_model.dart';
import 'package:AwesomeSkills/models/api_model/personal_model/get_category_apis_model.dart'
    as api_model;
import 'package:dio/dio.dart';
import 'package:AwesomeSkills/network/base_response.dart';
import 'package:AwesomeSkills/network/end_points.dart';
import 'package:AwesomeSkills/network/requests.dart';
import 'package:AwesomeSkills/routes/app_pages.dart';
import 'package:AwesomeSkills/utils/utils.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart' hide FormData, MultipartFile;
import 'package:image_picker/image_picker.dart';
import 'package:AwesomeSkills/models/api_model/personal_model/get_tags_ad_need_help.dart'
    as get_tags;
import 'package:AwesomeSkills/models/api_model/personal_model/working_duration_model.dart'
    as get_working_duration;
import 'package:AwesomeSkills/models/api_model/personal_model/get_job_price.dart'
    as get_job_price;
import 'package:AwesomeSkills/models/api_model/personal_model/get_repair_installation.dart'
    as repair_installation;
import 'package:AwesomeSkills/models/api_model/personal_model/get_choose_labour_model.dart'
    as choose_labour;
import 'package:AwesomeSkills/models/api_model/personal_model/work_place_model.dart'
    as work_place;
import 'package:multiple_images_picker/multiple_images_picker.dart';

import 'package:http/http.dart' as http;

class AdScreenNeedHelpPersonalController extends GetxController {
  TextEditingController jobTitleController = TextEditingController();
  TextEditingController jobDescriptionController = TextEditingController();
  TextEditingController extraInstructionController = TextEditingController();
  TextEditingController subRubController = TextEditingController();
  TextEditingController youtubeController = TextEditingController();
  TextEditingController jobLocatedController = TextEditingController();
  TextEditingController provideAreaSizeJobController = TextEditingController();
  TextEditingController priceInHourController =
      TextEditingController(text: "\$");
  TextEditingController hourlyRateController = TextEditingController();

  File? profileImage;

  Future<void> selectImage(ImageSource selectedSource) async {
    profileImage = await Utils.getImage(source: selectedSource);
    update();
  }

  final ImagePicker imagePicker = ImagePicker();
  List<Asset> imagesList = <Asset>[];

  Future<void> loadAssets() async {
    List<Asset> resultList = <Asset>[];
    String error = 'No Error Detected';

    try {
      resultList = await MultipleImagesPicker.pickImages(
        maxImages: 300,
        enableCamera: true,
        selectedAssets: imagesList,
        cupertinoOptions: const CupertinoOptions(takePhotoIcon: "chat"),
        materialOptions: const MaterialOptions(
          actionBarColor: "#abcdef",
          actionBarTitle: "Example App",
          allViewTitle: "All Photos",
          useDetailsView: false,
          selectCircleStrokeColor: "#000000",
        ),
      );
    } on Exception catch (e) {
      error = e.toString();
    }
    imagesList = resultList;
    update();
  }

  Dio dio = Dio(
    BaseOptions(
      connectTimeout: 10 * 1000, // 60 seconds
      receiveTimeout: 10 * 1000,
    ),
  );

  int selectedItem = 0;
  int selectedItemTags = 0;

  final adNeedHelpFormKey = GlobalKey<FormBuilderState>();
  RxInt groupValueWorkplace = 0.obs;

  bool isPriceFixed = false;
  RxBool isCompletedInPerson = false.obs;
  RxBool isCompletedInOnline = false.obs;

  changeRadioValue(value) {
    groupValueWorkplace.value = value;
  }

  RxBool completedPerson = false.obs;
  RxBool completedOnline = false.obs;
  RxString category = ''.obs;
  RxString selectTradesman = ''.obs;
  List tagsList = [];

  Rx<work_place.Data> getWorkPlace = work_place.Data().obs;
  Rx<get_tags.Data> getTagsCard = get_tags.Data().obs;
  Rx<get_working_duration.Data> getWorkingDuration =
      get_working_duration.Data().obs;

  Rx<get_job_price.Data> getJobPrice = get_job_price.Data().obs;

  RxList<api_model.Row> categoryName = RxList();
  Rx<api_model.Row>? selectedCategory;
  var categoryModel = api_model.Data();

  RxList<api_model.Services> serviceName = RxList();
  Rx<api_model.Services>? selectedService;
  var serviceModel = api_model.Row();

  RxList<repair_installation.Result> getRepairInstallationName = RxList();
  Rx<repair_installation.Result>? selectedRepairInstallation;
  var repairInstallationModel = repair_installation.Data();

  RxList<choose_labour.Result> getChooseLabourName = RxList();
  Rx<choose_labour.Result>? selectedChooseLabour;
  var chooseLabourModel = choose_labour.Data();

  selectedStartWork(accountName) {
    selectedItem = accountName;
    update();
  }

  selectItemTags(accountName) {
    if (tagsList.contains(accountName)) {
      tagsList.remove(accountName);
    } else {
      tagsList.add(accountName);
    }
    update();
  }

  @override
  onInit() {
    super.onInit();
    Future.delayed(30.milliseconds, () {
      getCategory();
//      getServices();
      getCardTags();
      getDurationWork();
      getRepairInstallationData();
      getChooseLabourData();
      getWorkPlaceData();
      getJobPriceData();
      adsNeedHelpPost();
    });
  }

  selectedDropDownCategory(value) {
    selectedCategory?.value = value!;
  }

  selectedDropDownService(value) {
    selectedService?.value = value!;
  }

  selectedDropDownRepairInstallation(value) {
    selectedRepairInstallation?.value = value!;
  }

  selectedDropDownChooseLabour(value) {
    selectedChooseLabour?.value = value!;
  }

  changeSwitchFixedPrice(bool value) {
    isPriceFixed = value;
    update();
  }

  Future<void> getRepairInstallationData() async {
    try {
      Utils.showProgressBar();
      BaseResponse response = await DioClient().getRequest(
        endPoint: EndPoints.getJobTypeRepairUrl,
      );
      if (response.error == false) {
        repairInstallationModel =
            repair_installation.Data.fromJson(response.data);
        getRepairInstallationName.value = repairInstallationModel.result ?? [];
        if (getRepairInstallationName.isNotEmpty) {
          selectedRepairInstallation = repair_installation.Result().obs;
          selectedRepairInstallation?.value = getRepairInstallationName.first;
        }
      }
      Utils.dismissProgressBar();
    } catch (e) {
      Utils.dismissProgressBar();
      print(e.toString());
    }
  }

  Future<void> getChooseLabourData() async {
    try {
      Utils.showProgressBar();
      BaseResponse response = await DioClient().getRequest(
        endPoint: EndPoints.getChooseLabourUrl,
      );
      if (response.error == false) {
        chooseLabourModel = choose_labour.Data.fromJson(response.data);
        getChooseLabourName.value = chooseLabourModel.result ?? [];
        if (getChooseLabourName.isNotEmpty) {
          selectedChooseLabour = choose_labour.Result().obs;
          selectedChooseLabour?.value = getChooseLabourName.first;
        }
      }
      Utils.dismissProgressBar();
    } catch (e) {
      Utils.dismissProgressBar();
      print(e.toString());
    }
  }

  Future<void> getCategory() async {
    try {
      Utils.showProgressBar();
      BaseResponse response = await DioClient().getRequest(
        endPoint: EndPoints.postsGetCategoryUrl,
      );
      if (response.error == false) {
        categoryModel = api_model.Data.fromJson(response.data);
        categoryName.value = categoryModel.row ?? [];
        if (categoryName.isNotEmpty) {
          selectedCategory = api_model.Row().obs;
          selectedCategory?.value = categoryName.first;
        }
      }
      Utils.dismissProgressBar();
    } catch (e) {
      Utils.dismissProgressBar();
      if (kDebugMode) {
        print(e.toString());
      }
    }
  }

  Future<void> getServices(Rx<api_model.Row>? selectedCategory) async {
    var id = selectedCategory!.value;
    serviceName.value = id.services ?? [];

    try {
      Utils.showProgressBar();
      BaseResponse response = await DioClient().getRequest(
        endPoint: EndPoints.postsGetCategoryUrl,
      );
      if (response.error == false) {
        serviceModel = api_model.Data.fromJson(response.data) as api_model.Row;

        //  serviceName.value = serviceModel.services ?? [];
        if (serviceName.isNotEmpty) {
          selectedService!.value = serviceName.first;
        }
      }
      Utils.dismissProgressBar();
    } catch (e) {
      Utils.dismissProgressBar();
      Utils.errorSnackBar(message: e.toString());
    }
  }

  Future<void> getDurationWork() async {
    try {
      Utils.showProgressBar();
      BaseResponse response = await DioClient()
          .getRequest(endPoint: EndPoints.getWorkingDurationUrl);

      if (response.error == false) {
        getWorkingDuration.value =
            get_working_duration.Data.fromJson(response.data);
      }
    } catch (e) {
      Utils.errorSnackBar(message: e.toString());
    }
  }

  Future<void> getJobPriceData() async {
    try {
      Utils.showProgressBar();
      BaseResponse response =
          await DioClient().getRequest(endPoint: EndPoints.getJobPriceUrl);

      if (response.error == false) {
        getJobPrice.value = get_job_price.Data.fromJson(response.data);
      }
    } catch (e) {
      Utils.errorSnackBar(message: e.toString());
    }
  }

  Future<void> getCardTags() async {
    try {
      Utils.showProgressBar();
      BaseResponse response = await DioClient().getRequest(
        endPoint: EndPoints.getTagsAdNeedHelpUrl,
      );

      if (response.error == false) {
        getTagsCard.value = get_tags.Data.fromJson(response.data);
        Utils.dismissProgressBar();
      } else {
        Utils.errorFlutterToast(msg: response.message.toString());
      }
    } catch (e) {
      Utils.dismissProgressBar();
      log('$e');
      Utils.errorFlutterToast(msg: e.toString());
    }
  }

  Future<void> getWorkPlaceData() async {
    try {
      Utils.showProgressBar();
      BaseResponse response = await DioClient().getRequest(
        endPoint: EndPoints.getWorkplaceUrl,
      );

      if (response.error == false) {
        getWorkPlace.value = work_place.Data.fromJson(response.data);
        Utils.dismissProgressBar();
      } else {
        Utils.errorFlutterToast(msg: response.message.toString());
      }
    } catch (e) {
      Utils.dismissProgressBar();
      log('$e');
      Utils.errorFlutterToast(msg: e.toString());
    }
  }

  Future uploadImageToServer() async {
    try {
      var headers = {'Authorization': 'Bearer ${HiveHelper.getAuthToken()}'};

      var request = http.MultipartRequest(
          'POST', Uri.parse(EndPoints.baseURL + EndPoints.postAdNeedHelpUrl));
      request.fields.addAll({
        'job_title': jobTitleController.text.toString(),
        'job_description': jobDescriptionController.text.toString(),
        'instruction': extraInstructionController.text.toString(),
        'category': selectedCategory!.value.id.toString(),
        'tags': tagsList.toString(),
        'starting_time': getWorkingDuration.value.result![0].id.toString(),
        'job_price': "20",
        'resource': selectedChooseLabour!.value.id.toString(),
        'area': provideAreaSizeJobController.text.toString(),
        'job_type': selectedRepairInstallation!.value.id.toString(),
        'isTradeMan_visit': "0".toString(),
        //   selectTradesman.value.toString(),
        'workplace': groupValueWorkplace.value.toString(),
        'fixed_price': isPriceFixed.toString(),
        'featured': "1".toString(),
      });

      request.files.add(await http.MultipartFile.fromPath(
          'images', 'assets/images/profile_julian.png'));
      request.headers.addAll(headers);

      http.StreamedResponse response = await request.send();

      if (response.statusCode == 200) {
        print(await response.stream.bytesToString());
      } else {
        print(response.reasonPhrase);
      }
    } catch (e) {
      print(e.toString());
    }
  }

  Future<void> adsNeedHelpPost() async {
    if (adNeedHelpFormKey.currentState!.saveAndValidate()) {
      print(HiveHelper.getAuthToken());
      var formData = ({
        'job_title': jobTitleController.text.toString(),
        'job_description': jobDescriptionController.text.toString(),
        'instruction': extraInstructionController.text.toString(),
        'category': selectedCategory!.value.id.toString(),
        'tags': tagsList.toString(),
        'starting_time': getWorkingDuration.value.result![0].id.toString(),
        'job_price': "20",
        'resource': selectedChooseLabour!.value.id.toString(),
        'area': provideAreaSizeJobController.text.toString(),
        'job_type': selectedRepairInstallation!.value.id.toString(),
        'isTradeMan_visit': "0".toString(),
        //   selectTradesman.value.toString(),
        'workplace': groupValueWorkplace.value.toString(),
        'fixed_price': isPriceFixed,
        'featured': "1".toString(),
      });

      BaseResponse response = await DioClient().postRequest(
        headers: {
          "token": HiveHelper.getAuthToken(),
        },
        endPoint: EndPoints.postAdNeedHelpUrl,
        body: formData,
      );

      if (response.error == false) {
        AdNeedHelpPersonalPostModel adPostPersonalNeedHelp =
            AdNeedHelpPersonalPostModel.fromJson(response.data);
        print(HiveHelper.getAuthToken());
        Utils.successSnackBar(message: response.data.toString());
        Get.toNamed(AppPage.personalInfoForPostAd);
        //  UserModel user = UserModel.fromJson(response.data);
        log(response.data);
        Utils.successSnackBar(message: "User Created");
      } else {
        Utils.errorFlutterToast(msg: response.message.toString());
      }
    }
  }
}
