import 'package:AwesomeSkills/Modules/Personal/ad_Screen/ad_personal_need_help_personal/ad_screen_need_help_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';

import '../../../../constants/app_colors.dart';
import 'package:AwesomeSkills/models/api_model/personal_model/get_repair_installation.dart'
    as RepairInstallation;

class CustomDropDownRepairInstallation extends StatelessWidget {
  final adNeedHelpController = Get.put(AdScreenNeedHelpPersonalController());

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 7.h,
        width: 100.w,
        padding: EdgeInsets.symmetric(horizontal: 4.w),
        decoration: BoxDecoration(
          color: AppColors.lightBlue6Color,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Center(
          child: DropdownButtonHideUnderline(
            child: Obx(
              () => adNeedHelpController.getRepairInstallationName.isEmpty
                  ? CircularProgressIndicator()
                  : DropdownButton<RepairInstallation.Result>(
                      value: adNeedHelpController
                          .selectedRepairInstallation?.value,
                      hint: const Text('Selected Sub Service'),
                      isExpanded: true,
                      icon: const Icon(Icons.arrow_drop_down),
                      elevation: 16,
                      style: Theme.of(context).textTheme.subtitle1,
                      onChanged: (RepairInstallation.Result? newValue) =>
                          adNeedHelpController
                              .selectedDropDownRepairInstallation(newValue),
                      items: adNeedHelpController.getRepairInstallationName
                          .map<DropdownMenuItem<RepairInstallation.Result>>(
                              (RepairInstallation.Result value) {
                        return DropdownMenuItem<RepairInstallation.Result>(
                            value: value,
                            child: Text(
                              value.jobType!.capitalizeFirst.toString(),
                            ));
                      }).toList(),
                    ),
            ),
          ),
        ));
  }
}
