import 'package:AwesomeSkills/Modules/Personal/ad_Screen/ad_personal_need_help_personal/ad_screen_need_help_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';

import '../../../../constants/app_colors.dart';
import 'package:AwesomeSkills/models/api_model/personal_model/get_choose_labour_model.dart'
    as choose_labour;

class CustomDropDownGetChooseLabour extends StatelessWidget {
  final adNeedHelpController = Get.put(AdScreenNeedHelpPersonalController());

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 7.h,
        width: 100.w,
        padding: EdgeInsets.symmetric(horizontal: 4.w),
        decoration: BoxDecoration(
          color: AppColors.lightBlue6Color,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Center(
          child: DropdownButtonHideUnderline(
            child: Obx(
              () => adNeedHelpController.getChooseLabourName.isEmpty
                  ? CircularProgressIndicator()
                  : DropdownButton<choose_labour.Result>(
                      value: adNeedHelpController.selectedChooseLabour?.value,
                      hint: const Text('Selected Sub Service'),
                      isExpanded: true,
                      icon: const Icon(Icons.arrow_drop_down),
                      elevation: 16,
                      style: Theme.of(context).textTheme.subtitle1,
                      onChanged: (choose_labour.Result? newValue) =>
                          adNeedHelpController
                              .selectedDropDownChooseLabour(newValue),
                      items: adNeedHelpController.getChooseLabourName
                          .map<DropdownMenuItem<choose_labour.Result>>(
                              (choose_labour.Result value) {
                        return DropdownMenuItem<choose_labour.Result>(
                            value: value,
                            child: Text(
                              value.resource!.capitalizeFirst ?? "",
                            ));
                      }).toList(),
                    ),
            ),
          ),
        ));
  }
}
