import 'dart:developer';
import 'dart:io';
import 'package:AwesomeSkills/Modules/Personal/ad_Screen/ad_personal_need_help_personal/custom_drop_down_category.dart';
import 'package:AwesomeSkills/Modules/Personal/ad_Screen/ad_personal_need_help_personal/custom_drop_down_choose_labour.dart';
import 'package:AwesomeSkills/Modules/Personal/ad_Screen/ad_personal_need_help_personal/custom_drop_down_repair_installation.dart';
import 'package:AwesomeSkills/Modules/Personal/ad_Screen/ad_personal_need_help_personal/custom_drop_down_service.dart';
import 'package:AwesomeSkills/constants/app_fonts.dart';
import 'package:AwesomeSkills/widgets/check_box.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Personal/ad_Screen/ad_personal_need_help_personal/ad_screen_need_help_controller.dart';
import 'package:AwesomeSkills/Modules/Personal/ad_Screen/custom_ad_info_text_form_field.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/Need_Help/need_help_card.dart';
import 'package:AwesomeSkills/routes/app_pages.dart';
import 'package:AwesomeSkills/widgets/button/save_preview_post_ad_button.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_textfield/custom_textfield.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:AwesomeSkills/widgets/drop_down.dart';
import 'package:multiple_images_picker/multiple_images_picker.dart';
import 'package:shimmer/shimmer.dart';
import 'package:sizer/sizer.dart';

class AdScreenNeedHelpPerssonal extends StatelessWidget {
  final controller = Get.put(AdScreenNeedHelpPersonalController());

  @override
  Widget build(BuildContext context) {
    return NestedScrollWidget(
        floatingActionButton: WidgetMethod.floatingActionButton(context),
        body: FormBuilder(
          key: controller.adNeedHelpFormKey,
          child: Padding(
              padding: EdgeInsets.only(left: 2.w, right: 2.w),
              child: SingleChildScrollView(
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                    SizedBox(
                      height: 1.h,
                    ),
                    Row(
                      children: [
                        InkWell(
                          onTap: () => Get.back(),
                          child: Icon(
                            Icons.arrow_back_ios_new,
                            color: Colors.black,
                            size: 15.sp,
                          ),
                        ),
                        SizedBox(
                          width: 5.w,
                        ),
                        const Text(
                          AppStrings.ad,
                          style: montserrat20TextStyle,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    Container(
                      padding: EdgeInsets.only(left: 3.w, top: 0.5.h),
                      height: 12.5.h,
                      width: 100.w,
                      decoration: BoxDecoration(
                        color: AppColors.lightGreen2Color,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            AppStrings.getNoticed,
                            style: orange600ColorTextStyle,
                          ),
                          Row(
                            children: [
                              Text(
                                AppStrings.standardAd,
                                style: blackBold16Color,
                              ),
                              SizedBox(
                                width: 1.w,
                              ),
                              const Text(
                                AppStrings.free,
                                style: orange16ColorTextStyle,
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Text(
                                AppStrings.featuredAd,
                                style: blackBold16Color,
                              ),
                              SizedBox(
                                width: 1.w,
                              ),
                              const Text(
                                AppStrings.tenDay,
                                style: orange16ColorTextStyle,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    const Text(
                      AppStrings.needHelp,
                      style: blue500ColorTextStyle,
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    CustomAdInfoTextFormField(
                      isOptionalText: false,
                      height: 7.h,
                      width: 110.w,
                      title: AppStrings.jobTitle,
                      hintText: AppStrings.useTitleAndFair,
                      controller: controller.jobTitleController,
                      titleTextStyle: blackFourteenSpTextStyle,
                    ),
                    SizedBox(
                      height: 0.5.h,
                    ),
                    const Align(
                      alignment: Alignment.bottomRight,
                      child: Text(
                        AppStrings.eightyCharacter,
                        style: black12PoppinsColor,
                      ),
                    ),
                    CustomAdInfoTextFormField(
                        titleTextStyle: blackFourteenSpTextStyle,
                        isOptionalText: false,
                        height: 10.h,
                        width: 110.w,
                        title: AppStrings.jobDescription,
                        hintText: AppStrings.beAsDescriptive,
                        controller: controller.jobDescriptionController),
                    SizedBox(
                      height: 1.h,
                    ),
                    Row(
                      children: [
                        Text(
                          AppStrings.extraInstruction,
                          style: blackFourteenSpTextStyle,
                        ),
                        SizedBox(
                          width: 2.w,
                        ),
                        const Text(
                          AppStrings.optionalBracket,
                          style: grey14Color,
                        )
                      ],
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(
                          vertical: 0.2.h, horizontal: 2.w),
                      height: 10.h,
                      width: 110.w,
                      decoration: BoxDecoration(
                        color: AppColors.lightBlue6Color,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: TextFormField(
                        controller: controller.extraInstructionController,
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: "",
                            hintStyle: black12Color),
                      ),
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    Text(
                      AppStrings.whichCategoryIsThe,
                      style: blackFourteenSpTextStyle,
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    Row(
                      children: [
                        Expanded(child: CustomDropDownCategory()),
                        SizedBox(
                          width: 8.w,
                        ),
                        Expanded(child: CustomDropDownService()),
                      ],
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    Text(
                      AppStrings.tags,
                      style: blackFourteenSpTextStyle,
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    GetBuilder<AdScreenNeedHelpPersonalController>(
                        builder: (_) {
                      return controller.getTagsCard.value.result!.isEmpty
                          ? Shimmer.fromColors(
                              baseColor: Colors.grey[300]!,
                              highlightColor: Colors.grey[100]!,
                              child: SizedBox(
                                height: 10.h,
                                child: ListView.builder(
                                    itemCount: 6,
                                    itemBuilder: (context, index) {
                                      return Container(
                                        height: 3.h,
                                        width: 10.w,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                      );
                                    }),
                              ))
                          : SizedBox(
                              height: 6.h,
                              child: ListView.separated(
                                scrollDirection: Axis.horizontal,
                                itemCount:
                                    controller.getTagsCard.value.result!.length,
                                itemBuilder: (context, index) {
                                  return FilterChip(
                                      backgroundColor:
                                          AppColors.lightBlue6Color,
                                      selectedColor: AppColors.blueAccent3Color,
                                      labelStyle: black14Color,
                                      label: Text(
                                        controller.getTagsCard.value
                                                .result![index].title ??
                                            "",
                                      ),
                                      // chipName: ,
                                      selected:
                                          controller.tagsList.contains(index),
                                      onSelected: (value) =>
                                          controller.selectItemTags(index));
                                },
                                separatorBuilder:
                                    (BuildContext context, int index) {
                                  return SizedBox(
                                    width: 2.w,
                                  );
                                },
                              ));
                    }),
                    SizedBox(
                      height: 2.h,
                    ),
                    Text(
                      AppStrings.whichSubRub,
                      style: blackFourteenSpTextStyle,
                    ),
                    SizedBox(
                      height: 0.5.h,
                    ),
                    CustomDropDownRepairInstallation(),
                    SizedBox(
                      height: 2.h,
                    ),
                    Text(
                      AppStrings.whenDoYouNeed,
                      style: blackFourteenSpTextStyle,
                    ),
                    GetBuilder<AdScreenNeedHelpPersonalController>(
                        builder: (_) {
                      return controller.getWorkingDuration.value.result!.isEmpty
                          ? Shimmer.fromColors(
                              baseColor: Colors.grey[300]!,
                              highlightColor: Colors.grey[100]!,
                              child: SizedBox(
                                height: 10.h,
                                child: ListView.builder(
                                    itemCount: 6,
                                    itemBuilder: (context, index) {
                                      return Container(
                                        height: 3.h,
                                        width: 10.w,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                      );
                                    }),
                              ))
                          : SizedBox(
                              height: 6.h,
                              child: ListView.separated(
                                scrollDirection: Axis.horizontal,
                                itemCount: controller
                                    .getWorkingDuration.value.result!.length,
                                itemBuilder: (context, index) {
                                  return ChoiceChip(
                                      selectedColor: AppColors.blueAccent3Color,
                                      labelStyle: black14Color,
                                      backgroundColor:
                                          AppColors.lightBlue6Color,
                                      label: Text(controller.getWorkingDuration
                                              .value.result![index].duration ??
                                          ""),
                                      selected:
                                          controller.selectedItem == index,
                                      onSelected: (value) =>
                                          controller.selectedStartWork(index));
                                },
                                separatorBuilder:
                                    (BuildContext context, int index) {
                                  return SizedBox(
                                    width: 2.w,
                                  );
                                },
                              ));
                    }),
                    SizedBox(
                      height: 1.h,
                    ),
                    Text(
                      AppStrings.jobPrice,
                      style: blackFourteenSpTextStyle,
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    GetBuilder(
                      init: AdScreenNeedHelpPersonalController(),
                      builder: (_) {
                        return Container(
                            height: 40,
                            width: 170,
                            padding: EdgeInsets.only(left: 2.w, right: 2.w),
                            decoration: BoxDecoration(
                              color: AppColors.lightBlue6Color,
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: TextFormField(
                              readOnly: controller.isPriceFixed == true
                                  ? false
                                  : true,
                              controller: controller.priceInHourController,
                              decoration: const InputDecoration(
                                border: InputBorder.none,
                                hintText: AppStrings.totalPriceDollar,
                              ),
                            ));
                      },
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                          width: 50.w,
                          child: Row(
                            children: [
                              Text(
                                AppStrings.fixedPrice,
                                style: blackFourteenSpTextStyle,
                              ),
                              SizedBox(
                                width: 3.w,
                              ),
                              GetBuilder<AdScreenNeedHelpPersonalController>(
                                  builder: (_) => Transform.scale(
                                        scaleY: 0.5,
                                        scaleX: 0.6,
                                        child: CupertinoSwitch(
                                            thumbColor:
                                                AppColors.lightBlueColor,
                                            trackColor:
                                                Colors.grey.withOpacity(0.3),
                                            activeColor:
                                                AppColors.lightBlue4Color,
                                            value: controller.isPriceFixed,
                                            onChanged: (newValue) {
                                              controller.changeSwitchFixedPrice(
                                                  newValue);
                                            }),
                                      )),
                            ],
                          ),
                        ),
                        GetBuilder(
                          init: AdScreenNeedHelpPersonalController(),
                          builder: (_) {
                            return Container(
                                height: 40,
                                width: 170,
                                padding: EdgeInsets.only(left: 2.w, right: 2.w),
                                decoration: BoxDecoration(
                                  color: AppColors.lightBlue6Color,
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                child: TextFormField(
                                  readOnly: controller.isPriceFixed == true
                                      ? true
                                      : false,
                                  controller: controller.hourlyRateController,
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: AppStrings.hourlyRate,
                                  ),
                                ));
                          },
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    Text(
                      AppStrings.isThisRepairOrInstallation,
                      style: blackFourteenSpTextStyle,
                    ),
                    SizedBox(
                      height: 0.5.h,
                    ),
                    CustomDropDownRepairInstallation(),
                    SizedBox(
                      height: 2.h,
                    ),
                    Text(
                      AppStrings.quotesToIncludeMaterial,
                      style: blackFourteenSpTextStyle,
                    ),
                    SizedBox(
                      height: 0.5.h,
                    ),
                    CustomDropDownGetChooseLabour(),
                    SizedBox(
                      height: 2.h,
                    ),
                    CustomAdInfoTextFormField(
                        titleTextStyle: blackFourteenSpTextStyle,
                        isOptionalText: false,
                        height: 50,
                        width: MediaQuery.of(context).size.width - 20,
                        title: AppStrings.pleaseProvideSizeArea,
                        hintText: AppStrings.enterANumber,
                        controller: controller.provideAreaSizeJobController),
                    SizedBox(
                      height: 2.h,
                    ),
                    Text(
                      AppStrings.canTradesManVisitTheSite,
                      style: blackFourteenSpTextStyle,
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    CustomDropDown(
                      height: 7.h,
                      width: 100.w,
                      hint: AppStrings.chooseCategory,
                      items: const <String>[
                        'Yes',
                        'No',
                      ],
                      selectedValue: (value) {
                        controller.selectTradesman.value = value!;
                        log(value.toString());
                      },
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    Text(
                      AppStrings.workPlace,
                      style: blackFourteenSpTextStyle,
                    ),
                    Obx(
                      () => LabeledCheckbox(
                        labelTextStyle: blackBold16Color,
                        contentPadding: EdgeInsets.zero,
                        activeColor: AppColors.black,
                        label: controller.getWorkPlace.value.result![1]
                                .description!.capitalizeFirst ??
                            "",
                        onTap: (value) {
                          controller.isCompletedInPerson.value =
                              value ?? !controller.isCompletedInPerson.value;
                        },
                        value: controller.isCompletedInPerson.value,
                        gap: 0,
                      ),
                    ),
                    Obx(
                      () => LabeledCheckbox(
                        labelTextStyle: blackBold16Color,
                        contentPadding: EdgeInsets.zero,
                        activeColor: AppColors.black,
                        label: controller.getWorkPlace.value.result![0]
                                .description!.capitalizeFirst ??
                            "",
                        onTap: (value) {
                          controller.isCompletedInOnline.value =
                              value ?? !controller.isCompletedInOnline.value;
                        },
                        value: controller.isCompletedInOnline.value,
                        gap: 0,
                      ),
                    ),
                    const SizedBox(
                      height: 15.0,
                    ),
                    Text(
                      AppStrings.uploadImageOptional,
                      style: blackFourteenSpTextStyle,
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    GestureDetector(
                      onTap: () => controller.loadAssets(),
                      child: DottedBorder(
                        dashPattern: const [6, 2],
                        radius: const Radius.circular(10),
                        color: AppColors.indigoColor,
                        strokeWidth: 1.5,
                        child: Container(
                          height: 20.h,
                          width: 100.w,
                          decoration: const BoxDecoration(
                            color: Colors.transparent,
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SvgPicture.asset(
                                'assets/images/post_icon/upload_icon.svg',
                              ),
                              SizedBox(
                                height: 0.4.h,
                              ),
                              Text(
                                AppStrings.dragAndDrop,
                                style: blackBoldPoppinsColor,
                              ),
                              SizedBox(
                                height: 0.4.h,
                              ),
                              Text(
                                AppStrings.or,
                                style: blackBoldPoppinsColor,
                              ),
                              SizedBox(
                                height: 0.4.h,
                              ),
                              const Text(
                                AppStrings.clickToBrowse,
                                style: yellowPoppins300TextStyle,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    GetBuilder(
                        init: AdScreenNeedHelpPersonalController(),
                        builder: (_) {
                          return controller.imagesList.isNotEmpty
                              ? SizedBox(
                                  height: controller.imagesList.length <= 3
                                      ? 20.h
                                      : controller.imagesList.length <= 4
                                          ? 40.h
                                          : controller.imagesList.length <= 6
                                              ? 60.h
                                              : 60.h,
                                  width: Get.width,
                                  child: GridView.count(
                                    physics:
                                        const NeverScrollableScrollPhysics(),
                                    crossAxisCount: 3,
                                    crossAxisSpacing: 10,
                                    mainAxisSpacing: 10,
                                    children: List.generate(
                                        controller.imagesList.length, (index) {
                                      Asset asset =
                                          controller.imagesList[index];
                                      return AssetThumb(
                                        asset: asset,
                                        width: 300,
                                        height: 300,
                                      );
                                    }),
                                  ))
                              : const SizedBox();
                        }),
                    SizedBox(
                      height: 2.h,
                    ),
                    Row(
                      children: [
                        Text(
                          AppStrings.youtubeVideoLinks,
                          style: blackFourteenSpTextStyle,
                        ),
                        SizedBox(
                          width: 2.w,
                        ),
                        const Text(
                          AppStrings.optional,
                          style: grey14Color,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    CustomTextFormField(
                        hintText: '',
                        name: 'youtube_link',
                        height: 7.h,
                        width: 100.w,
                        borderRadius: BorderRadius.circular(10),
                        hintTextStyle: black12Color,
                        color: AppColors.lightBlue6Color,
                        validator: (String? value) {},
                        controller: controller.youtubeController),
                    SizedBox(
                      height: 2.h,
                    ),
                    GetBuilder(
                      init: AdScreenNeedHelpPersonalController(),
                      builder: (_) {
                        return Center(
                            child: NeedHelpCard(
                                initialValueOfStar: 0.0,
                                reviewStarValue: "(${AppStrings.zero})",
                                title: controller.jobTitleController.text
                                    .toString(),
                                jobPrice: AppStrings.twoThreeNine,
                                userName: AppStrings.alex,
                                location: AppStrings.southPortQLD,
                                imageUrl: '',
                                durationOfJob: AppStrings.asap,
                                onTap: () {}));
                      },
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Column(
                          children: [
                            Row(
                              children: [
                                Text(
                                  AppStrings.featured,
                                  style: blackBold16Color,
                                ),
                                SizedBox(
                                  width: 2.w,
                                ),
                                const Icon(
                                  Icons.help_outline,
                                  color: AppColors.green10Color,
                                )
                              ],
                            ),
                            Text(
                              AppStrings.tenTenDay,
                              style: blackBold16Color,
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 10.w,
                        ),
                        Container(
                          height: 5.h,
                          width: 20.w,
                          decoration: BoxDecoration(
                              color: Colors.transparent,
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                  color: AppColors.green10Color,
                                  width: 2.0,
                                  style: BorderStyle.solid)),
                          child: const Center(
                            child: Text(
                              AppStrings.add,
                              style: greenColorTextStyle,
                            ),
                          ),
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 15.0,
                    ),
                    SavePreviewPostButton(
                      saveOnTap: () {},
                      previewAdOnTap: () {},
                      postAdOnTap: () => controller.adsNeedHelpPost(),
                    ),
                    SizedBox(
                      height: 10.h,
                    ),
                  ]))),
        ));
  }
}
