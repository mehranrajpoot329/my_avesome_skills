import 'package:flutter/material.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:sizer/sizer.dart';

class CustomAdInfoTextFormField extends StatelessWidget {
  String title, hintText;
  TextStyle titleTextStyle;
  TextEditingController controller;
  double height, width;
  bool isOptionalText;

  CustomAdInfoTextFormField(
      {required this.isOptionalText,
      required this.height,
      required this.width,
      required this.title,
      required this.titleTextStyle,
      required this.hintText,
      required this.controller});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Expanded(
              child: Text(
                title,
                style: titleTextStyle,
              ),
            ),
            isOptionalText
                ? SizedBox(
                    width: 1.w,
                  )
                : Container(),
            isOptionalText
                ? Text(
                    AppStrings.optionalBracket,
                    style: grey14Color,
                  )
                : Container(),
          ],
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 3.w),
          height: height,
          width: width,
          decoration: BoxDecoration(
            color: AppColors.lightBlue6Color,
            borderRadius: BorderRadius.circular(10),
          ),
          child: TextFormField(
            controller: controller,
            decoration: InputDecoration(
                border: InputBorder.none,
                hintText: hintText,
                hintStyle: black12Color),
          ),
        ),
      ],
    );
  }
}
