import 'package:flutter/material.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:sizer/sizer.dart';

class BuildTagsNeedWorkWidget extends StatelessWidget {
  String text1, text2, text3, text4;
  bool isNextRow;

  BuildTagsNeedWorkWidget(
      {required this.isNextRow,
      required this.text1,
      required this.text2,
      required this.text3,
      required this.text4});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(child: buildTagsWidget(nameTags: text1)),
            SizedBox(
              width: 8.w,
            ),
            Expanded(
              child: buildTagsWidget(nameTags: text2),
            ),
          ],
        ),
        const SizedBox(
          height: 15.0,
        ),
        isNextRow
            ? Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(child: buildTagsWidget(nameTags: text3)),
                  SizedBox(
                    width: 8.w,
                  ),
                  Expanded(
                    child: buildTagsWidget(nameTags: text4),
                  )
                ],
              )
            : Container(),
      ],
    );
  }

  Widget buildTagsWidget({required String nameTags}) {
    return Container(
      height: 40,
      width: 178,
      decoration: BoxDecoration(
        color: AppColors.lightBlue6Color,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Center(
        child: Text(
          nameTags,
          style: black14Color,
        ),
      ),
    );
  }
}
