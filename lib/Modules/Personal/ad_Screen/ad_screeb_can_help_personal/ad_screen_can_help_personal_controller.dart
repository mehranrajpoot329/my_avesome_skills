import 'package:AwesomeSkills/network/end_points.dart';
import 'package:AwesomeSkills/network/requests.dart';
import 'package:AwesomeSkills/utils/utils.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Modules/Personal/ad_Screen/ad_screeb_can_help_personal/ad_screen_can_help_personal.dart';
import 'package:AwesomeSkills/models/api_model/personal_model/get_category_apis_model.dart'
    as api_model;

import '../../../../local/db/local_storage.dart';
import '../../../../network/base_response.dart';

class AdScreenCanHelpPersonalController extends GetxController {
  TextEditingController skillControllerOne = TextEditingController();
  TextEditingController priceControllerOne = TextEditingController();
  TextEditingController skillControllerTwo = TextEditingController();
  TextEditingController priceControllerTwo = TextEditingController();
  TextEditingController skillControllerThree = TextEditingController();
  TextEditingController priceControllerThree = TextEditingController();
  TextEditingController forAnExtraControllerOne = TextEditingController();
  TextEditingController forAnExtraControllerTwo = TextEditingController();
  TextEditingController forAnExtraControllerOneValue = TextEditingController();
  TextEditingController forAnExtraControllerTwoValue = TextEditingController();
  TextEditingController personForJobController = TextEditingController();
  TextEditingController mySkillController = TextEditingController();
  TextEditingController myEducationController = TextEditingController();
  TextEditingController postJobsController = TextEditingController();

  final adCanHelpFormKey = GlobalKey<FormBuilderState>();
  RxString category = ''.obs;
  bool completedOnline = false;
  bool extra = false;
  bool extraFor = false;
  RxBool isVisibleExtraField = false.obs;
  RxBool isVisibleExtraFieldTwo = false.obs;

  double traveling = 4.0;

  isShowVisibleTextField() {
    isVisibleExtraField.value = !isVisibleExtraField.value;
  }

  isShowVisibleTextFieldTwo() {
    isVisibleExtraFieldTwo.value = !isVisibleExtraFieldTwo.value;
  }

  final selectAll = CheckBoxModal(
    title: 'title',
  );
  final checkBoxList = [
    CheckBoxModal(title: 'House Cleaning'),
    CheckBoxModal(title: 'End of Lease Cleaning'),
    CheckBoxModal(title: 'Carpet Steam Cleaning'),
    CheckBoxModal(title: 'Window Cleaning'),
    CheckBoxModal(title: 'Outdoor Cleaning'),
    CheckBoxModal(title: 'Office Cleaning'),
    CheckBoxModal(title: 'High Pressure Cleaning'),
    CheckBoxModal(title: 'Upholstery Cleaning'),
  ];

  RxList<api_model.Row> categoryName = RxList();
  Rx<api_model.Row>? selectedCategory;
  var categoryModel = api_model.Data();

  RxList<api_model.Services> serviceName = RxList();
  Rx<api_model.Services>? selectedService;
  var serviceModel = api_model.Row();

  final selectIndustriesList = <String>[
    'Select All',
    'House Cleaning',
    'End of Lease Cleaning',
    'Carpet Steam Cleaning',
    'Window Cleaning',
    'Select All',
    'House Cleaning',
    'End of Lease Cleaning',
    'Carpet Steam Cleaning',
    'Window Cleaning',
  ];

  final selectedService12 = api_model.Services(
    serviceName: "selectedService.value",
  );

  changeCheckBoxForAnExtra(bool value) {
    extraFor = value;
    update();
  }

  changeCheckBoxSelectIndustries111(service) {
    service = !service;
    update();
  }

  changeCheckBoxSelectIndustries(CheckBoxModal checkBoxModal) {
    checkBoxModal.value = !checkBoxModal.value;
    update();
  }

  changeCheckBoxSelectIndustries222(service) {
    service = !service;
    update();
  }

  changeCheckBoxSelectItemIndustries(CheckBoxModal checkBoxModal) {
    checkBoxModal.value = !checkBoxModal.value;
    update();
  }

  changeTraveling(value) {
    traveling = value;
    update();
  }

  changeCheckBoxForExtra(bool value) {
    extra = value;
    update();
  }

  selectedDropDownCategory(value) {
    selectedCategory?.value = value!;
  }

  selectedDropDownService(value) {
    selectedService?.value = value!;
  }

  @override
  onInit() {
    super.onInit();
    Future.delayed(30.milliseconds, () {
      getCategory();
    });
  }

  Future<void> getCategory() async {
    try {
      Utils.showProgressBar();
      BaseResponse response = await DioClient().getRequest(
        endPoint: EndPoints.postsGetCategoryUrl,
      );
      if (response.error == false) {
        categoryModel = api_model.Data.fromJson(response.data);
        categoryName.value = categoryModel.row ?? [];
        if (categoryName.isNotEmpty) {
          selectedCategory = api_model.Row().obs;
          selectedCategory?.value = categoryName.first;
        }
      }
      Utils.dismissProgressBar();
    } catch (e) {
      Utils.dismissProgressBar();
      if (kDebugMode) {
        print(e.toString());
      }
    }
  }

  Future<void> getServices(Rx<api_model.Row>? selectedCategory) async {
    var id = selectedCategory!.value;
    serviceName.value = id.services ?? [];

    try {
      Utils.showProgressBar();
      BaseResponse response = await DioClient().getRequest(
        endPoint: EndPoints.postsGetCategoryUrl,
      );
      if (response.error == false) {
        serviceModel = api_model.Data.fromJson(response.data) as api_model.Row;

        //  serviceName.value = serviceModel.services ?? [];
        if (serviceName.isNotEmpty) {
          selectedService!.value = serviceName.first;
        }
      }
      Utils.dismissProgressBar();
    } catch (e) {
      Utils.dismissProgressBar();
      Utils.errorSnackBar(message: e.toString());
    }
  }

  // Future<void> adsCanHelpPost() async {
  //   if (adCanHelpFormKey.currentState!.saveAndValidate()) {
  //     var formData = {
  //       'category': selectedCategory!.value.id.toString(),
  //       'job_price': "20",
  //       'featured': "1".toString(),
  //     };
  //
  //     BaseResponse response = await DioClient().postRequest(
  //       headers: {
  //         "token": HiveHelper.getAuthToken(),
  //       },
  //       endPoint: EndPoints.postAdCanHelpUrl,
  //       body: formData,
  //     );
  //
  //     if (response.error == false) {
  //       AdNeedHelpPersonalPostModel adPostPersonalNeedHelp =
  //           AdNeedHelpPersonalPostModel.fromJson(response.data);
  //       print(HiveHelper.getAuthToken());
  //       Utils.successSnackBar(message: response.data.toString());
  //       Get.toNamed(AppPage.personalInfoForPostAd);
  //       //  UserModel user = UserModel.fromJson(response.data);
  //       log(response.data);
  //       Utils.successSnackBar(message: "User Created");
  //     } else {
  //       BaseResponse baseResponse = BaseResponse.fromJson(response.data);
  //       Utils.flutterToast(msg: baseResponse.message.toString());
  //     }
  //   }
  // }
}
