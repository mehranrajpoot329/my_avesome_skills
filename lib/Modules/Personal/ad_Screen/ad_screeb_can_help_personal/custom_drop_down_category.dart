import 'package:AwesomeSkills/Modules/Personal/ad_Screen/ad_screeb_can_help_personal/ad_screen_can_help_personal_controller.dart';
import 'package:AwesomeSkills/constants/app_colors.dart';
import 'package:AwesomeSkills/models/api_model/personal_model/get_category_apis_model.dart'
    as api_model;
import 'package:AwesomeSkills/network/end_points.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';

class CustomDropDownCategory extends StatelessWidget {
  final controller = Get.put(AdScreenCanHelpPersonalController());

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 7.h,
        width: 100.w,
        padding: EdgeInsets.symmetric(horizontal: 4.w),
        decoration: BoxDecoration(
          color: AppColors.lightBlue6Color,
          borderRadius: BorderRadius.circular(10),
        ),
        child: Center(
          child: DropdownButtonHideUnderline(
            child: Obx(
              () => controller.categoryName.isEmpty
                  ? CircularProgressIndicator()
                  : DropdownButton<api_model.Row>(
                      value: controller.selectedCategory?.value,
                      isExpanded: true,
                      icon: const Icon(Icons.arrow_drop_down),
                      elevation: 16,
                      style: Theme.of(context).textTheme.subtitle1,
                      onChanged: (api_model.Row? newValue) {
                        controller.selectedDropDownCategory(newValue);
                        controller.getServices(controller.selectedCategory);
                      },
                      items: controller.categoryName
                          .map<DropdownMenuItem<api_model.Row>>(
                              (api_model.Row value) {
                        return DropdownMenuItem<api_model.Row>(
                          value: value,
                          child: Text(value.category!.capitalizeFirst ?? ""),
                        );
                      }).toList(),
                    ),
            ),
          ),
        ));
  }
}
