import 'package:AwesomeSkills/Modules/Personal/ad_Screen/ad_screeb_can_help_personal/ad_screen_can_help_personal_controller.dart';
import 'package:AwesomeSkills/constants/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';
import 'package:AwesomeSkills/models/api_model/personal_model/get_category_apis_model.dart'
    as api_model;

class CustomDropDownService extends StatelessWidget {
  final adCanHelpController = Get.put(AdScreenCanHelpPersonalController());

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 7.h,
      width: 100.w,
      padding: EdgeInsets.symmetric(horizontal: 4.w),
      decoration: BoxDecoration(
        color: AppColors.lightBlue6Color,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Obx(
        () => Center(
          child: adCanHelpController.serviceName.isEmpty
              ? const CircularProgressIndicator()
              : DropdownButtonHideUnderline(
                  child: DropdownButton<api_model.Services>(
                    value: adCanHelpController.selectedService?.value,
                    hint: const Text('Selected Service'),
                    isExpanded: true,
                    icon: const Icon(Icons.arrow_drop_down),
                    elevation: 16,
                    style: Theme.of(context).textTheme.subtitle1,
                    onChanged: (api_model.Services? newValue) =>
                        adCanHelpController.selectedDropDownService(newValue),
                    items: adCanHelpController.serviceName
                        .map<DropdownMenuItem<api_model.Services>>(
                            (api_model.Services value) {
                      num? selectedCategoryId =
                          adCanHelpController.selectedCategory!.value.id;
                      num? selectedServiceId =
                          adCanHelpController.selectedService!.value.id;

                      var id = int.parse(
                          "${adCanHelpController.selectedCategory!.value.id}");
                      return DropdownMenuItem<api_model.Services>(
                          value: value,
                          child: Text(
                            (selectedCategoryId == selectedServiceId)
                                ? value.serviceName ?? ""
                                : "",
                            style: TextStyle(fontSize: 10),
                          ));
                    }).toList(),
                  ),
                ),
        ),
      ),
    );
  }
}
