import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:sizer/sizer.dart';

import '../../../constants/text_style.dart';

class NotificationMenu extends StatelessWidget {
  const NotificationMenu({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(15.0))),
      child: Badge(
          position: const BadgePosition(end: -4, bottom: 13),
          badgeContent: const Text(
            '1',
            style: TextStyle(fontSize: 8, color: Colors.white),
          ),
          child: SvgPicture.asset(
            "assets/images/post_icon/notification.svg",
          )),
      itemBuilder: (context) => [
        PopupMenuItem(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Text(
              AppStrings.notifications,
              style: interBlack2TextStyle,
            ),
            BuildWidgetDrawer(
              imageUrl: 'assets/images/profile_julian.png',
              title: AppStrings.yourOrderIsPlaced,
              details: AppStrings.dummyText,
              orderTiming: AppStrings.threeHourAgo,
            ),
            BuildWidgetDrawer(
              imageUrl: 'assets/images/profile_julian.png',
              title: AppStrings.yourOrderIsPlaced,
              details: AppStrings.dummyText,
              orderTiming: AppStrings.threeHourAgo,
            ),
            BuildWidgetDrawer(
              imageUrl: 'assets/images/profile_julian.png',
              title: AppStrings.yourOrderIsPlaced,
              details: AppStrings.dummyText,
              orderTiming: AppStrings.threeHourAgo,
            ),
            SizedBox(
              height: 1.h,
            ),
            const Divider(),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SvgPicture.asset(
                    'assets/images/post_icon/blue_forward_arrow.svg'),
                SizedBox(
                  width: 3.w,
                ),
                Text(
                  AppStrings.viewMoreCapital,
                  style: interLightBlueTextStyle,
                )
              ],
            )
          ],
        ))
      ],
    );
  }
}

class BuildWidgetDrawer extends StatelessWidget {
  String imageUrl, title, details, orderTiming;

  BuildWidgetDrawer({
    required this.imageUrl,
    required this.title,
    required this.details,
    required this.orderTiming,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 1.h),
      child: Row(
        children: [
          Container(
            height: 15.h,
            width: 15.w,
            decoration: BoxDecoration(
                image: DecorationImage(image: AssetImage(imageUrl))),
          ),
          SizedBox(
            width: 1.w,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                title,
                style: blackPoppins300TextStyle,
              ),
              Container(
                width: 40.w,
                child: Text(
                  details,
                  style: grey9Color,
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
              Row(
                children: [
                  SvgPicture.asset('assets/images/post_icon/clock_icon.svg'),
                  SizedBox(
                    width: 2.w,
                  ),
                  Text(
                    orderTiming,
                    style: grey9Color,
                  )
                ],
              )
            ],
          )
        ],
      ),
    );
  }
}
