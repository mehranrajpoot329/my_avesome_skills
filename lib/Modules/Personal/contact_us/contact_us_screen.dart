import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Modules/Business/custom_scroll_appBar_business/nested_business_scroll_widget.dart';
import 'package:AwesomeSkills/Modules/Personal/contact_us/contact_us_controller.dart';
import 'package:AwesomeSkills/Modules/login_signup/signup_controller.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';

import '../../../constants/app_strings.dart';
import '../../../constants/text_style.dart';

class ContactUsScreen extends StatelessWidget {
  final contactUsController = Get.put(ContactUsController());
  static final signUpController = Get.put(SignUpController());
  var personalAccount =
      signUpController.selectedItem == AppStrings.personalAccount;

  @override
  Widget build(BuildContext context) {
    return personalAccount
        ? NestedScrollWidget(
            floatingActionButton: WidgetMethod.floatingActionButton(context),
            body: Padding(
              padding: EdgeInsets.only(left: 2.w, right: 2.w),
              child: ListView(
                padding: EdgeInsets.zero,
                children: [
                  Row(
                    children: [
                      GestureDetector(
                        onTap: () => Get.back(),
                        child: Icon(
                          Icons.arrow_back_ios_new,
                          color: Colors.black,
                          size: 3.h,
                        ),
                      ),
                      SizedBox(
                        width: 5.w,
                      ),
                      const Text(
                        AppStrings.contactUs,
                        style: montserrat20TextStyle,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Container(
                    height: 60.h,
                    width: Get.width,
                    padding: EdgeInsets.only(left: 3.w, right: 3.w, top: 3.h),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.transparent,
                        border: Border.all(
                            color: AppColors.lightBlueColor,
                            width: 1,
                            style: BorderStyle.solid)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        buildContactUSFormWidget(
                            imagePath:
                                "assets/images/post_icon/mobile_icon.svg",
                            textName: AppStrings.mobileNumber),
                        SizedBox(
                          height: 2.h,
                        ),
                        buildContactUSFormWidget(
                            imagePath: "assets/images/post_icon/email_icon.svg",
                            textName: AppStrings.emailSpam),
                        SizedBox(
                          height: 2.h,
                        ),
                        buildContactUSFormWidget(
                            imagePath:
                                "assets/images/post_icon/location_indigo_icon.svg",
                            textName: AppStrings.southPortQLD),
                        SizedBox(
                          height: 5.h,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: SizedBox(
                                height: 7.h,
                                child: TextFormField(
                                  controller:
                                      contactUsController.nameController,
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.only(
                                        left: 2.w,
                                        right: 2.w,
                                        top: 1.h,
                                        bottom: 1.h),
                                    hintText: AppStrings.nameText,
                                    fillColor: AppColors.lightBlue6Color,
                                    filled: true,
                                    border: OutlineInputBorder(),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 2.w,
                            ),
                            Expanded(
                              child: SizedBox(
                                height: 7.h,
                                child: TextFormField(
                                  controller:
                                      contactUsController.emailController,
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.only(
                                        left: 2.w,
                                        right: 2.w,
                                        top: 1.h,
                                        bottom: 1.h),
                                    hintText: AppStrings.email,
                                    fillColor: AppColors.lightBlue6Color,
                                    filled: true,
                                    border: OutlineInputBorder(),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 2.h,
                        ),
                        TextFormField(
                          maxLines: 5,
                          controller: contactUsController.emailController,
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(
                                left: 2.w, right: 2.w, top: 1.h, bottom: 1.h),
                            hintText: AppStrings.message,
                            fillColor: AppColors.lightBlue6Color,
                            filled: true,
                            border: OutlineInputBorder(),
                          ),
                        ),
                        SizedBox(
                          height: 3.h,
                        ),
                        CustomButton(
                          height: 54,
                          width: MediaQuery.of(context).size.width - 40,
                          borderRadius: BorderRadius.circular(8),
                          buttonName: AppStrings.sendMessage,
                          textStyle: buttonBlueTextStyle,
                          buttonColor: AppColors.blueColor,
                          onTap: () {
                            print("Hello");
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ))
        : NestedBusinessScroll(
            floatingActionButton: WidgetMethod.floatingActionButton(context),
            body: Padding(
              padding: EdgeInsets.only(left: 2.w, right: 2.w),
              child: ListView(
                padding: EdgeInsets.zero,
                children: [
                  Row(
                    children: [
                      GestureDetector(
                        onTap: () => Get.back(),
                        child: Icon(
                          Icons.arrow_back_ios_new,
                          color: Colors.black,
                          size: 3.h,
                        ),
                      ),
                      SizedBox(
                        width: 5.w,
                      ),
                      const Text(
                        AppStrings.contactUs,
                        style: montserrat20TextStyle,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Container(
                    height: 60.h,
                    width: Get.width,
                    padding: EdgeInsets.only(left: 3.w, right: 3.w, top: 3.h),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.transparent,
                        border: Border.all(
                            color: AppColors.lightBlueColor,
                            width: 1,
                            style: BorderStyle.solid)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        buildContactUSFormWidget(
                            imagePath:
                                "assets/images/post_icon/mobile_icon.svg",
                            textName: AppStrings.mobileNumber),
                        SizedBox(
                          height: 2.h,
                        ),
                        buildContactUSFormWidget(
                            imagePath: "assets/images/post_icon/email_icon.svg",
                            textName: AppStrings.emailSpam),
                        SizedBox(
                          height: 2.h,
                        ),
                        buildContactUSFormWidget(
                            imagePath:
                                "assets/images/post_icon/location_indigo_icon.svg",
                            textName: AppStrings.southPortQLD),
                        SizedBox(
                          height: 5.h,
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: SizedBox(
                                height: 7.h,
                                child: TextFormField(
                                  controller:
                                      contactUsController.nameController,
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.only(
                                        left: 2.w,
                                        right: 2.w,
                                        top: 1.h,
                                        bottom: 1.h),
                                    hintText: AppStrings.nameText,
                                    fillColor: AppColors.lightBlue6Color,
                                    filled: true,
                                    border: OutlineInputBorder(),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 2.w,
                            ),
                            Expanded(
                              child: SizedBox(
                                height: 7.h,
                                child: TextFormField(
                                  controller:
                                      contactUsController.emailController,
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.only(
                                        left: 2.w,
                                        right: 2.w,
                                        top: 1.h,
                                        bottom: 1.h),
                                    hintText: AppStrings.email,
                                    fillColor: AppColors.lightBlue6Color,
                                    filled: true,
                                    border: OutlineInputBorder(),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 2.h,
                        ),
                        TextFormField(
                          maxLines: 5,
                          controller: contactUsController.emailController,
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(
                                left: 2.w, right: 2.w, top: 1.h, bottom: 1.h),
                            hintText: AppStrings.message,
                            fillColor: AppColors.lightBlue6Color,
                            filled: true,
                            border: OutlineInputBorder(),
                          ),
                        ),
                        SizedBox(
                          height: 3.h,
                        ),
                        CustomButton(
                          height: 54,
                          width: MediaQuery.of(context).size.width - 40,
                          borderRadius: BorderRadius.circular(8),
                          buttonName: AppStrings.sendMessage,
                          textStyle: buttonBlueTextStyle,
                          buttonColor: AppColors.blueColor,
                          onTap: () {
                            print("Hello");
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ));
  }

  Widget buildContactUSFormWidget(
      {required String imagePath, required String textName}) {
    return SizedBox(
      width: 60.w,
      child: Row(
        children: [
          Expanded(
            child: SvgPicture.asset(
              imagePath,
              height: 3.h,
            ),
          ),
          SizedBox(
            width: 7.w,
          ),
          Expanded(
              flex: 4,
              child: Text(
                textName,
                style: blackSixHundredWeightColor,
              )),
        ],
      ),
    );
  }
}
