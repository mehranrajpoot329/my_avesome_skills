import 'dart:convert';
import 'dart:developer';

import 'package:AwesomeSkills/Modules/Personal/drawers_screens/my_task/my_task_to_start/my_task_to_start_controller.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/Need_Help/need_help_controller.dart';
import 'package:AwesomeSkills/models/api_model/personal_model/get_can_help_model.dart'
    as can_help_card;
import 'package:AwesomeSkills/models/api_model/personal_model/get_need_help_model.dart'
    as need_help_card;
import 'package:AwesomeSkills/network/base_response.dart';
import 'package:AwesomeSkills/network/end_points.dart';
import 'package:AwesomeSkills/network/requests.dart';
import 'package:AwesomeSkills/utils/utils.dart';
import 'package:get/get.dart';

class LandingScreenController extends GetxController {
  final needHelpController = Get.put(NeedHelpController());
  final myTaskToStartController = Get.put(MyTaskToStartController());
  Rx<can_help_card.Data> getCanHelpCard = can_help_card.Data().obs;
  Rx<need_help_card.Data> getNeedHelpCard = need_help_card.Data().obs;

  @override
  onInit() {
    super.onInit();
    Future.delayed(30.milliseconds, () {
      myTaskToStartController.getUserInfoData();
      getCanHelp();
      getNeedHelp();
    });
  }

  Future<void> getCanHelp() async {
    try {
      Utils.showProgressBar();
      BaseResponse response = await DioClient().getRequest(
          endPoint: EndPoints.getCanHelpCardUrl,
          queryParams: {"page": "1", "limit": "6", "order": "DESC"});

      if (response.error == false) {
        can_help_card.Data baseResponse =
            can_help_card.Data.fromJson(response.data);
        getCanHelpCard.value = baseResponse;
      } else {
        Utils.errorFlutterToast(msg: response.message.toString());
      }
      Utils.dismissProgressBar();
    } catch (e) {
      Utils.dismissProgressBar();
      log('$e');
      Utils.errorFlutterToast(msg: e.toString());
    }
  }

  Future<void> getNeedHelp() async {
    try {
      Utils.showProgressBar();
      BaseResponse response = await DioClient().getRequest(
          endPoint: EndPoints.getNeedHelpCardUrl,
          queryParams: {"page": "1", "limit": "6", "order": "DESC"});

      if (response.error == false) {
        getNeedHelpCard.value = need_help_card.Data.fromJson(response.data);
        Utils.dismissProgressBar();
      } else {
        Utils.errorFlutterToast(msg: response.message.toString());
      }
      Utils.dismissProgressBar();
    } catch (e) {
      Utils.dismissProgressBar();
      Utils.errorFlutterToast(msg: e.toString());
    }
  }
}
