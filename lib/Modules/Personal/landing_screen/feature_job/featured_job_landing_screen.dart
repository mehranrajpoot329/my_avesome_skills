import 'dart:ffi';

import 'package:AwesomeSkills/Modules/Personal/landing_screen/Need_Help/need_help_controller.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/feature_job/feature_job_card_controller.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/landing_screen_controller.dart';
import 'package:AwesomeSkills/network/end_points.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/feature_job/featured_job_card.dart';
import 'package:shimmer/shimmer.dart';
import 'package:sizer/sizer.dart';

import '../../../../Constants/app_strings.dart';
import '../../../../Constants/text_style.dart';

class FeaturedJobLandingScreen extends StatelessWidget {
  final featureJobController = Get.put(FeatureJobsCardController());
  final needHelpController = Get.put(LandingScreenController());

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 42.h,
      width: Get.width,
      padding: EdgeInsets.only(top: 2.h),
      decoration:
          BoxDecoration(color: const Color(0xFFD5E5F5).withOpacity(0.7)),
      child: Padding(
        padding: EdgeInsets.only(
          left: 2.w,
        ),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(right: 2.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: 65.w,
                    child: Row(
                      children: [
                        Text(
                          AppStrings.featured,
                          style: blackBoldFourteenTextStyle,
                        ),
                        SizedBox(width: 1.w),
                        Text(
                          AppStrings.jobs,
                          style: TextStyle(
                              color: AppColors.blueColor,
                              fontSize: 14.sp,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'poppins'),
                        ),
                        SizedBox(width: 1.w),
                        Text(
                          AppStrings.ofTheDay,
                          style: blackBoldFourteenTextStyle,
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 4.h,
                    width: 25.w,
                    decoration: BoxDecoration(
                        color: AppColors.lightBlueColor,
                        borderRadius: BorderRadius.circular(30)),
                    child: Center(
                      child: Text(
                        AppStrings.explore,
                        style: customButtonWhiteTextStyle,
                      ),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            Padding(
              padding: EdgeInsets.only(left: 3.w),
              child: SizedBox(
                height: 32.h,
                child: Align(
                  alignment: Alignment.center,
                  child: Obx(
                    () => needHelpController.getNeedHelpCard.value.row == null
                        ? Shimmer.fromColors(
                            baseColor: Colors.grey[300]!,
                            highlightColor: Colors.grey[100]!,
                            child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: 6,
                              itemBuilder: (context, index) {
                                return Container(
                                  height: 30.h,
                                  width: 85.w,
                                  decoration: BoxDecoration(
                                    boxShadow: const [
                                      BoxShadow(
                                          offset: Offset(0, 5),
                                          blurRadius: 4.0,
                                          spreadRadius: 2.0,
                                          color: AppColors.blueAccent2Color)
                                    ],
                                    borderRadius: BorderRadius.circular(20),
                                    color: Colors.white,
                                  ),
                                );
                              },
                            ),
                          )
                        : ListView.builder(
                            itemCount: needHelpController
                                .getNeedHelpCard.value.row!.length,
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (context, index) {
                              var featuredData = needHelpController
                                  .getNeedHelpCard.value.row![index].isFeatured;
                              bool isFeatured = featuredData! == "true";
                              String featuredSkillOne = needHelpController
                                  .getNeedHelpCard.value.row![index].images!;
                              var listOfData = featuredSkillOne.split(",");

                              var featuredCardData = needHelpController
                                  .getNeedHelpCard.value.row![index];

                              return needHelpController
                                      .getNeedHelpCard.value.row!.isEmpty
                                  ? CircularProgressIndicator()
                                  : isFeatured
                                      ? FeatureJobCard(
                                          jobtitle:
                                              featuredCardData.title ?? "",
                                          durationOfJob:
                                              featuredCardData.startingTime ==
                                                      null
                                                  ? ""
                                                  : featuredCardData
                                                          .startingTime!
                                                          .duration ??
                                                      "",
                                          location: featuredCardData
                                                      .userData!.address ==
                                                  null
                                              ? "No Location found"
                                              : featuredCardData.userData!
                                                      .address!.streetAddress ??
                                                  "",
                                          images:
                                              "${EndPoints.baseURL}${featuredCardData.userData!.photoURL}",
                                          jobDescription:
                                              featuredCardData.description ??
                                                  "",
                                          jobPrice:
                                              featuredCardData.jobPrice ?? "",
                                          viewPost: AppStrings.onePointFiveText,
                                        )
                                      : Container();
                            },
                          ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
