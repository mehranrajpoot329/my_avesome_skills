import 'dart:convert';
import 'dart:developer';

import 'package:AwesomeSkills/network/base_response.dart';
import 'package:AwesomeSkills/network/end_points.dart';
import 'package:AwesomeSkills/network/requests.dart';
import 'package:AwesomeSkills/utils/utils.dart';
import 'package:get/get.dart';

import 'package:AwesomeSkills/models/api_model/personal_model/get_feature_job_card_model.dart';

class FeatureJobsCardController extends GetxController {
  Rx<Data> getFeaturedData = Data().obs;

  @override
  onInit() {
    super.onInit();
    Future.delayed(30.milliseconds, () {
      getFeatureJob();
    });
  }

  Future<void> getFeatureJob() async {
    try {
      Utils.showProgressBar();
      BaseResponse response = await DioClient().getRequest(
          endPoint: EndPoints.getFeatureJobUrl, queryParams: {"limit": "5"});

      if (response.error == false) {
        getFeaturedData.value = Data.fromJson(response.data);
      }
      Utils.dismissProgressBar();
    } catch (e) {
      Utils.dismissProgressBar();
      log('$e');
      Utils.errorFlutterToast(msg: e.toString());
    }
    update();
  }
}
