import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:sizer/sizer.dart';

class FeatureJobCard extends StatelessWidget {
  String jobtitle,
      location,
      images,
      jobDescription,
      viewPost,
      jobPrice,
      durationOfJob;

  FeatureJobCard({
    required this.jobtitle,
    required this.location,
    required this.images,
    required this.jobDescription,
    required this.jobPrice,
    required this.viewPost,
    required this.durationOfJob,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          height: 30.h,
          width: 85.w,
          decoration: BoxDecoration(
            boxShadow: const [
              BoxShadow(
                  offset: Offset(0, 5),
                  blurRadius: 4.0,
                  spreadRadius: 2.0,
                  color: AppColors.blueAccent2Color)
            ],
            borderRadius: BorderRadius.circular(20),
            color: Colors.white,
          ),
          child: Stack(
            children: [
              Positioned(
                top: 0.0,
                left: 0.0,
                child: Container(
                  padding: EdgeInsets.only(
                    left: 3.w,
                  ),
                  height: 40,
                  width: 85.w,
                  decoration: const BoxDecoration(
                      color: AppColors.blueAccentColor,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20),
                          topRight: Radius.circular(20))),
                  child: Row(
                    children: [
                      Expanded(
                        child: Row(
                          children: [
                            const Icon(
                              Icons.favorite_border,
                              color: Colors.white,
                            ),
                            SizedBox(
                              width: 1.w,
                            ),
                            SizedBox(
                              width: 29.w,
                              child: Text(
                                jobtitle,
                                overflow: TextOverflow.ellipsis,
                                style: customButtonWhiteTextStyle,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 10.w,
                      ),
                      Expanded(
                        child: Row(
                          children: [
                            CircleAvatar(
                                radius: 15,
                                backgroundColor: Colors.white,
                                child: Icon(
                                  Icons.location_on,
                                  color: Colors.black,
                                  size: 3.h,
                                )),
                            SizedBox(
                              width: 1.w,
                            ),
                            SizedBox(
                              width: 26.w,
                              child: Text(
                                location,
                                overflow: TextOverflow.ellipsis,
                                style: customButtonWhiteTextStyle,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Positioned(
                top: 8.h,
                left: 25,
                child: Row(
                  children: [
                    CachedNetworkImage(
                      imageUrl: images ?? "",
                      placeholder: (context, url) =>
                          CircularProgressIndicator(),
                      errorWidget: (context, url, error) => Container(
                        height: 13.h,
                        width: 13.h,
                        decoration: BoxDecoration(
                            color: Colors.grey.withOpacity(0.2),
                            shape: BoxShape.circle),
                        child: Center(
                          child: Icon(
                            Icons.person,
                            color: Colors.grey,
                            size: 10.h,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 3.w,
                    ),
                    SizedBox(
                      width: 50.w,
                      child: Text(
                        "$jobDescription",
                        maxLines: 4,
                        overflow: TextOverflow.ellipsis,
                        style: interBlackFiveHundredTextStyle,
                      ),
                    ),
                  ],
                ),
              ),
              Positioned(
                top: 19.5.h,
                right: 10.w,
                child: Text(
                  '\$$jobPrice',
                  style: indigoSixteenColorTextStyle,
                ),
              ),
              Positioned(
                  top: 22.h,
                  right: 0.0,
                  left: 0.0,
                  child: const Divider(color: AppColors.indigoColor)),
              Positioned(
                bottom: 10,
                left: 5.w,
                right: 5.w,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: 15.w,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Image.asset(
                            'assets/images/post_icon/calendar.png',
                            height: 20,
                          ),
                          SizedBox(
                            width: 10.w,
                            child: Text(
                              durationOfJob,
                              overflow: TextOverflow.ellipsis,
                              style: interBlackTextStyle,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 20.w,
                      child: Row(
                        children: [
                          Image.asset(
                            'assets/images/post_icon/visible_icon.png',
                            height: 20,
                          ),
                          Text(
                            viewPost,
                            style: interBlackTextStyle,
                          ),
                        ],
                      ),
                    ),
                    CustomButton(
                        buttonName: AppStrings.getPaid,
                        width: 28.w,
                        borderRadius: BorderRadius.circular(5),
                        height: 4.h,
                        buttonColor: AppColors.greenColor,
                        textStyle: interWhiteTextStyle,
                        onTap: () {}),
                  ],
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          width: 2.w,
        ),
      ],
    );
  }
}
