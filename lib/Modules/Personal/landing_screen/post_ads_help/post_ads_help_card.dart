import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:sizer/sizer.dart';

class PostAdsHelpCard extends StatelessWidget {
  String textName, textDetails, buttonName, image;
  Function onTap;

  PostAdsHelpCard({
    required this.image,
    required this.textName,
    required this.textDetails,
    required this.buttonName,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          padding: EdgeInsets.only(top: 2.h, left: 2.w, right: 2.w),
          height: 33.h,
          width: 45.w,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.transparent,
              border: Border.all(
                  color: Colors.grey, width: 1.0, style: BorderStyle.solid)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SvgPicture.asset(image),
              SizedBox(
                height: 0.5.h,
              ),
              FittedBox(
                fit: BoxFit.contain,
                child: Text(
                  textName,
                  style: blackSixHundredWeightColor,
                ),
              ),
              SizedBox(height: 1.h),
              Text(
                textDetails,
                textAlign: TextAlign.center,
                style: greyTextStyle,
              ),
              SizedBox(height: 2.h),
              CustomButton(
                  height: 5.h,
                  buttonName: buttonName,
                  width: 34.w,
                  borderRadius: BorderRadius.circular(8),
                  buttonColor: AppColors.blueAccentColor.withOpacity(0.2),
                  textStyle: TextStyle(
                      fontSize: 9.sp, color: AppColors.blueAccentColor),
                  onTap: onTap),
            ],
          ),
        ),
        SizedBox(
          width: 4.w,
        ),
      ],
    );
  }
}
