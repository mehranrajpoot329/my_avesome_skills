import 'package:AwesomeSkills/Modules/Personal/landing_screen/post_ads_help/post_ads_help_card.dart';
import 'package:flutter/material.dart';

import '../../../../Constants/app_strings.dart';
import '../../../../constants/text_style.dart';

class PostAdsHelpLandingScreen extends StatelessWidget {
  const PostAdsHelpLandingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(left: 15.0),
        child: ListView(scrollDirection: Axis.horizontal, children: [
          PostAdsHelpCard(
              image: 'assets/images/post_icon/need_help.svg',
              textName: AppStrings.needHelp,
              textDetails: AppStrings.fromItsMedievalOrigins,
              buttonName: AppStrings.iNeedHelp,
              onTap: () {}),
          PostAdsHelpCard(
              image: 'assets/images/post_icon/i_can_help.svg',
              textName: AppStrings.canHelpWith,
              textDetails: AppStrings.fromItsMedievalOrigins,
              buttonName: AppStrings.iCanHelp,
              onTap: () {}),
          PostAdsHelpCard(
              image: 'assets/images/post_icon/post_add.svg',
              textName: AppStrings.postAndAd,
              textDetails: AppStrings.fromItsMedievalOrigins,
              buttonName: AppStrings.postAndAd,
              onTap: () {}),
          PostAdsHelpCard(
              image: 'assets/images/post_icon/quotes.svg',
              textName: AppStrings.pricing,
              textDetails: AppStrings.fromItsMedievalOrigins,
              buttonName: AppStrings.ourFee,
              onTap: () {}),
          PostAdsHelpCard(
              image: 'assets/images/post_icon/list_your_business.svg',
              textName: AppStrings.listYourBusiness,
              textDetails: AppStrings.fromItsMedievalOrigins,
              buttonName: AppStrings.listYourBusinessSmall,
              onTap: () {}),
        ]));
  }
}
