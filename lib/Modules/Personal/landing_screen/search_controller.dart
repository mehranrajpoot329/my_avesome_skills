import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SearchController extends GetxController {
  TextEditingController searchController = TextEditingController();
  double distance = 4.0;
  RangeValues priceRange = RangeValues(5, 10);

  RxBool isFilter = false.obs;
  RxBool needHelp = false.obs;
  RxBool canNeedHelp = false.obs;
  RxBool useCurrentLocation = false.obs;

  showSearchFilter() {
    isFilter.value = !isFilter.value;
  }

  changeDistance(value) {
    distance = value;
    update();
  }

  changePriceRange(value) {
    priceRange = value;
    update();
  }
}
