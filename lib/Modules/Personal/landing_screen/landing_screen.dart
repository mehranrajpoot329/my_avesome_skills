import 'package:AwesomeSkills/Modules/Personal/landing_screen/Can_Help/can_help_with_landing_screen.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/feature_job/featured_job_landing_screen.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/get_help_today_landing_screen/get_help_today_landing_screen.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/landing_screen_controller.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/post_ads_help/post_ads_help_landing_screen.dart';
import 'package:AwesomeSkills/local/db/local_storage.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/custom_app_bar_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';

import '../../../Constants/text_style.dart';
import '../../../constants/app_colors.dart';
import '../../../constants/app_strings.dart';
import 'Need_Help/need_help_landing_screen.dart';

class LandingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CustomAppBarController>(
      builder: (_) {
        return ListView(
          padding: EdgeInsets.zero,
          children: [
            SizedBox(
              height: 1.h,
            ),
            HiveHelper.isLoggedIn()
                ? Container()
                : Column(
                    children: [
                      const Align(
                        alignment: Alignment.center,
                        child: Text(
                          AppStrings.howItWork,
                          style: blue600ColorTextStyle,
                        ),
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: Container(
                          height: 0.5.h,
                          width: 50.w,
                          color: AppColors.lightBlueColor,
                        ),
                      )
                    ],
                  ),
            HiveHelper.isLoggedIn()
                ? const SizedBox(
                    height: 1,
                  )
                : SizedBox(
                    height: 36.h,
                    child: const PostAdsHelpLandingScreen(),
                  ),
            HiveHelper.isLoggedIn()
                ? SizedBox(
                    height: 1.h,
                  )
                : SizedBox(
                    height: 1.h,
                  ),
            FeaturedJobLandingScreen(),

            // AnimatedContainer(
            //   duration: Duration(seconds: 3),
            //   child:
            //   ),
            // ),
            SizedBox(
              height: 1.h,
            ),
            Padding(
              padding: EdgeInsets.only(left: 3.w, right: 2.w),
              child: NeedHelpLandingScreen(),
            ),
            GetHelpTodayLandingScreen(),
            SizedBox(
              height: 2.h,
            ),
            Padding(
              padding: EdgeInsets.only(left: 3.w, right: 2.w),
              child: CanHelpWithLandingScreen(),
            ),
          ],
        );
      },
    );
  }
}
