import 'package:AwesomeSkills/Modules/Personal/landing_screen/landing_screen_controller.dart';
import 'package:AwesomeSkills/local/db/local_storage.dart';
import 'package:AwesomeSkills/network/end_points.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/Can_Help/can_help_card.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/Can_Help/can_help_controller.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/custom_app_bar_controller.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/bottom_sheet_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/need_row_with_map_view.dart';
import 'package:sizer/sizer.dart';

import '../../../../Constants/app_colors.dart';
import '../../../../constants/app_strings.dart';
import '../../../../constants/text_style.dart';
import '../../../../routes/app_pages.dart';
import '../../../../widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import '../../../../widgets/custom_widget_app/widget_method.dart';

class CanHelpScreenWithOutBottomNav extends StatelessWidget {
  final customAppBarController = Get.put(CustomAppBarController());
  final canHelpScreenController = Get.put(CanHelpController());
  final landingScreenController = Get.put(LandingScreenController());

  @override
  Widget build(BuildContext context) {
    return NestedScrollWidget(
      floatingActionButton: WidgetMethod.floatingActionButton(context),
      body: Padding(
        padding: EdgeInsets.only(left: 3.w, right: 2.w),
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: 1.h,
              ),
              Row(
                children: [
                  GestureDetector(
                    onTap: () => Get.back(),
                    child: Icon(
                      Icons.arrow_back_ios_new,
                      color: Colors.black,
                      size: 3.h,
                    ),
                  ),
                  SizedBox(
                    width: 5.w,
                  ),
                  const Text(
                    AppStrings.iCanHelp,
                    style: montserrat20TextStyle,
                  ),
                ],
              ),
              SizedBox(
                height: 2.h,
              ),
              NeedHelpRowWithMap(
                isText: true,
                textName: AppStrings.canHelpWith,
              ),
              Obx(
                () => ListView.builder(
                  physics: const ScrollPhysics(),
                  shrinkWrap: true,
                  itemCount:
                      landingScreenController.getCanHelpCard.value.row!.length >
                                  5 &&
                              !canHelpScreenController.showAllItems.value
                          ? 5
                          : landingScreenController
                              .getCanHelpCard.value.row!.length,
                  itemBuilder: (BuildContext context, int index) {
                    String featuredSkillOne = landingScreenController
                        .getCanHelpCard.value.row![index].featuredSkill!;

                    var listOfData = featuredSkillOne.split(RegExp(r"[:,]"));
                    return Center(
                      child: CanHelpCard(
                        title: landingScreenController
                            .getCanHelpCard.value.row![index].title
                            .toString(),
                        userName: landingScreenController.getCanHelpCard.value
                            .row![index].userData!.firstName
                            .toString(),
                        featureSkillsOne: listOfData[0],
                        featureSkillSpeedOne: listOfData[1],
                        featureSkillsTwo: listOfData[2],
                        featureSkillSpeedTwo: listOfData[3],
                        featureSkillsThree: listOfData[4],
                        featureSkillSpeedThree: listOfData[5],
                        imageUrl:
                            "${EndPoints.baseURL}${landingScreenController.getCanHelpCard.value.row![index].userData!.photoURL}",
                        location: landingScreenController.getCanHelpCard.value
                                    .row![0].userData!.address ==
                                null
                            ? ""
                            : landingScreenController.getCanHelpCard.value
                                .row![0].userData!.address!.streetAddress
                                .toString(),
                        onTap: () {
                          HiveHelper.isLoggedIn()
                              ? Get.toNamed(AppPage.canHelpCardDetailScreen)
                              : BottomSheetAdScreen.isLoginedOutBottomSheet(
                                  context);
                        },
                      ),
                    );
                  },
                ),
              ),
              Obx(() => canHelpScreenController.showAllItems.value
                  ? Container()
                  : Align(
                      alignment: Alignment.bottomRight,
                      child: CustomButton(
                        buttonName: AppStrings.viewAll,
                        width: 22.w,
                        height: 3.h,
                        buttonColor: AppColors.lightBlue2Color,
                        textStyle: customButtonLightTextStyle,
                        borderRadius: BorderRadius.circular(20),
                        onTap: () =>
                            canHelpScreenController.isShowCanHelpCard(),
                      ),
                    )),
              SizedBox(
                height: 6.h,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
