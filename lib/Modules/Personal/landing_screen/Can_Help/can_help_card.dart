import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/rating_star.dart';
import 'package:sizer/sizer.dart';

class CanHelpCard extends StatelessWidget {
  Function onTap;
  String title,
      userName,
      featureSkillsOne,
      featureSkillSpeedOne,
      featureSkillsTwo,
      featureSkillSpeedTwo,
      featureSkillsThree,
      featureSkillSpeedThree,
      imageUrl,
      location;

  CanHelpCard({
    required this.onTap,
    required this.title,
    required this.userName,
    required this.featureSkillsOne,
    required this.featureSkillSpeedOne,
    required this.featureSkillsTwo,
    required this.featureSkillSpeedTwo,
    required this.featureSkillsThree,
    required this.featureSkillSpeedThree,
    required this.imageUrl,
    required this.location,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onTap(),
      child: Column(
        children: [
          Container(
            height: 30.h,
            width: 90.w,
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                    offset: Offset(0, 3),
                    blurRadius: 4.0,
                    spreadRadius: 2.0,
                    color: Colors.grey.withOpacity(0.4))
              ],
              borderRadius: BorderRadius.circular(20),
              color: Colors.white,
            ),
            child: Stack(
              children: [
                Container(
                  padding: EdgeInsets.only(left: 9.w, right: 8.w),
                  height: 6.h,
                  width: 90.w,
                  decoration: const BoxDecoration(
                      color: AppColors.blueAccent3Color,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20),
                          topRight: Radius.circular(20))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        width: 50.w,
                        child: Text(
                          title,
                          overflow: TextOverflow.ellipsis,
                          style: whiteBoldTwelveColor,
                        ),
                      ),
                      CircleAvatar(
                          radius: 12.sp,
                          backgroundColor: AppColors.lightBlueColor,
                          child: Icon(
                            Icons.favorite,
                            color: Colors.white,
                            size: 3.h,
                          )),
                    ],
                  ),
                ),
                Positioned(
                    top: 7.5.h,
                    left: 30.w,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Row(
                          children: [
                            SizedBox(
                              width: 27.w,
                              child: Text(
                                overflow: TextOverflow.ellipsis,
                                userName,
                                style: blue18MulishTextStyle,
                              ),
                            ),
                            RatingStar(
                              textStyle: greyTextColor,
                              isNumberRating: true,
                              ratingNumber: AppStrings.sixtyFour,
                            ),
                          ],
                        ),
                        Text(
                          AppStrings.jobSuccessPercentage,
                          style: greenMulishTextStyle,
                        )
                      ],
                    )),

                /// for practice
                Positioned(
                    top: 14.h,
                    left: 30.w,
                    child: Text(
                      AppStrings.featuredSkill,
                      style: blue12MulishTextStyle,
                    )),
                Positioned(
                    top: 17.h,
                    left: 30.w,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              featureSkillsOne,
                              style: blackMulishTextStyle,
                            ),
                            Text(
                              featureSkillsTwo,
                              style: blackMulishTextStyle,
                            ),
                            Text(
                              featureSkillsThree,
                              style: blackMulishTextStyle,
                            ),
                          ],
                        ),
                        SizedBox(
                          width: 30.w,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "\$ $featureSkillSpeedOne / hr",
                              style: blue12MulishTextStyle,
                            ),
                            Text(
                              "\$ $featureSkillSpeedTwo / hr",
                              style: blue12MulishTextStyle,
                            ),
                            Text(
                              "\$ $featureSkillSpeedThree / hr",
                              style: blue12MulishTextStyle,
                            ),
                          ],
                        ),
                      ],
                    )),
                Positioned(
                  top: 9.h,
                  left: 3.w,
                  child: CachedNetworkImage(
                    imageUrl: imageUrl ?? "",
                    placeholder: (context, url) => CircularProgressIndicator(),
                    errorWidget: (context, url, error) => Container(
                      height: 13.h,
                      width: 13.h,
                      decoration: BoxDecoration(
                          color: Colors.grey.withOpacity(0.2),
                          shape: BoxShape.circle),
                      child: Center(
                        child: Icon(
                          Icons.person,
                          color: Colors.grey,
                          size: 10.h,
                        ),
                      ),
                    ),
                  ),
                ),
                Positioned(
                  bottom: 0.h,
                  left: 0.w,
                  right: 0.w,
                  child: Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: 9.w, vertical: 0.7.h),
                    height: 4.h,
                    width: Get.width,
                    decoration: const BoxDecoration(
                        color: AppColors.blue5Color,
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20))),
                    child: Text(
                      location,
                      style: interBlackTextStyle,
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 2.h,
          )
        ],
      ),
    );
  }
}
