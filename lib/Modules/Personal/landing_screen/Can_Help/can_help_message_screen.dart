import 'package:AwesomeSkills/Modules/Personal/landing_screen/Can_Help/can_help_controller.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';

import '../../../../Constants/app_strings.dart';
import '../../../../constants/app_colors.dart';
import '../../../../constants/text_style.dart';
import '../../../../widgets/custom_widget_app/rating_star.dart';

class CanHelpMessagingScreen extends StatelessWidget {
  final canHelpController = Get.put(CanHelpController());

  @override
  Widget build(BuildContext context) {
    return NestedScrollWidget(
        floatingActionButton: WidgetMethod.floatingActionButton(context),
        body: Padding(
          padding: EdgeInsets.only(left: 3.w, right: 2.w),
          child: ListView(padding: EdgeInsets.zero, children: [
            SizedBox(
              height: 1.h,
            ),
            Row(
              children: [
                GestureDetector(
                  onTap: () => Get.back(),
                  child: Icon(
                    Icons.arrow_back_ios_new,
                    color: Colors.black,
                    size: 3.h,
                  ),
                ),
                SizedBox(
                  width: 5.w,
                ),
                const Text(
                  AppStrings.message,
                  style: montserrat20TextStyle,
                ),
              ],
            ),
            SizedBox(
              height: 2.h,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(
                  width: 35.w,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        height: 10.h,
                        width: 10.h,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            border: Border.all(
                                color: AppColors.lightBlueColor,
                                width: 1,
                                style: BorderStyle.solid),
                            image: DecorationImage(
                                image: AssetImage(
                                    'assets/images/profile_a_fernendez.png'),
                                fit: BoxFit.cover)),
                      ),
                      SizedBox(
                        width: 2.w,
                      ),
                      Text(
                        AppStrings.alex,
                        style: blue18MulishTextStyle,
                      ),
                    ],
                  ),
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    RatingStar(
                      textStyle: greyTextColor,
                      isNumberRating: true,
                      ratingNumber: AppStrings.sixtyFour,
                    ),
                    Text(
                      AppStrings.jobSuccessPercentage,
                      style: greenMulishTextStyle,
                    )
                  ],
                ),
              ],
            ),
            SizedBox(
              height: 2.h,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  AppStrings.housePaint,
                  style: blue600ColorTextStyle,
                ),
                Icon(
                  Icons.favorite,
                  color: Colors.red,
                  size: 24.sp,
                ),
              ],
            ),
            SizedBox(
              height: 2.h,
            ),
            Row(
              children: [
                SvgPicture.asset(
                  'assets/images/post_icon/calender_tick_icon.svg',
                  height: 3.h,
                ),
                SizedBox(
                  width: 1.w,
                ),
                const Text(
                  AppStrings.asap,
                  style: black14Mulish1TextStyle,
                ),
                SizedBox(
                  width: 2.w,
                ),
                SvgPicture.asset(
                  'assets/images/post_icon/visible_grey.svg',
                  height: 2.h,
                ),
                SizedBox(
                  width: 2.w,
                ),
                const Text(
                  AppStrings.five,
                  style: black14Mulish1TextStyle,
                ),
              ],
            ),
            SizedBox(
              height: 2.h,
            ),
            const Text(
              AppStrings.address,
              style: blue700MulishTextStyle,
            ),
            SizedBox(
              height: 1.h,
            ),
            const Text(
              AppStrings.southPort,
              style: black400MulishTextStyle,
            ),
            SizedBox(
              height: 2.h,
            ),
            const Text(
              AppStrings.jobValueSmall,
              style: blue700MulishTextStyle,
            ),
            SizedBox(
              height: 1.h,
            ),
            Text(
              AppStrings.eightHundred,
              style: greenColor24TextStyle,
            ),
            SizedBox(
              height: 1.h,
            ),
            const Text(
              AppStrings.jobDetail,
              style: black18MulishTextStyle,
            ),
            SizedBox(
              height: 1.h,
            ),
            Text(
              AppStrings.iNeedPaimter,
              style: blackMulish300TextStyle,
            ),
            SizedBox(
              height: 1.h,
            ),
            Divider(),
            SizedBox(
              height: 1.h,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  flex: 2,
                  child: TextFormField(
                    controller: canHelpController.messageController,
                    decoration: const InputDecoration(
                        border: InputBorder.none,
                        hintText: AppStrings.typeYourMessage,
                        hintStyle: grey12MulishTextStyle),
                  ),
                ),
                Expanded(
                    child: CustomButton(
                        buttonName: AppStrings.reply,
                        width: 30.w,
                        height: 3.h,
                        buttonColor: AppColors.lightBlueColor,
                        textStyle: white12MulishTextStyle,
                        borderRadius: BorderRadius.circular(4),
                        onTap: () {})),
              ],
            ),
            SizedBox(
              height: 3.h,
            ),
            Row(
              children: [
                SvgPicture.asset(
                  'assets/images/post_icon/gallery_icon.svg',
                  height: 3.h,
                ),
                SizedBox(
                  width: 1.w,
                ),
                SvgPicture.asset(
                  'assets/images/post_icon/video_icon.svg',
                  height: 3.h,
                ),
                SizedBox(
                  width: 1.w,
                ),
                SvgPicture.asset(
                  'assets/images/post_icon/pdf_icon.svg',
                  height: 3.h,
                ),
              ],
            ),
            SizedBox(
              height: 6.h,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CustomButton(
                    buttonName: AppStrings.rejectedJob,
                    width: 30.w,
                    height: 4.h,
                    buttonColor: AppColors.red2Color,
                    textStyle: blackMulishTextStyle,
                    borderRadius: BorderRadius.circular(5),
                    onTap: () {}),
                const SizedBox(
                  width: 10,
                ),
                CustomButton(
                    buttonName: AppStrings.acceptJob,
                    width: 30.w,
                    height: 4.h,
                    buttonColor: AppColors.green12Color,
                    textStyle: blackMulishTextStyle,
                    borderRadius: BorderRadius.circular(5),
                    onTap: () {}),
              ],
            ),
          ]),
        ));
  }
}
