import 'package:AwesomeSkills/Modules/Personal/landing_screen/landing_screen_controller.dart';
import 'package:AwesomeSkills/network/end_points.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/need_row_with_map_view.dart';
import 'package:shimmer/shimmer.dart';
import 'package:sizer/sizer.dart';

import '../../../../Constants/app_colors.dart';
import '../../../../Constants/app_strings.dart';
import '../../../../Constants/text_style.dart';
import '../../../../routes/app_pages.dart';
import 'can_help_card.dart';

class CanHelpWithLandingScreen extends StatelessWidget {
  final landingScreenController = Get.put(LandingScreenController());

  @override
  Widget build(BuildContext context) {
    String featuredSkillOne =
        landingScreenController.getCanHelpCard.value.row![0].featuredSkill!;

    var listOfData = featuredSkillOne.split(RegExp(r"[:,]"));
    var getCanHelpApi = landingScreenController.getCanHelpCard.value.row![0];

    return Column(
      children: [
        NeedHelpRowWithMap(
          isText: false,
          textName: AppStrings.canHelpWith,
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 2.w),
          child: Obx(
            () => CanHelpCard(
              title: landingScreenController.getCanHelpCard.value.row![0].title
                  .toString(),
              userName: landingScreenController
                  .getCanHelpCard.value.row![0].userData!.firstName
                  .toString(),
              featureSkillsOne: listOfData[0],
              featureSkillSpeedOne: listOfData[1],
              featureSkillsTwo: listOfData[2],
              featureSkillSpeedTwo: listOfData[3],
              featureSkillsThree: listOfData[4],
              featureSkillSpeedThree: listOfData[5],
              imageUrl:
                  "${EndPoints.baseURL}${landingScreenController.getCanHelpCard.value.row![0].userData!.photoURL}",
              location: landingScreenController
                          .getCanHelpCard.value.row![0].userData!.address ==
                      null
                  ? ""
                  : landingScreenController.getCanHelpCard.value.row![0]
                      .userData!.address!.streetAddress
                      .toString(),
              onTap: () {
                Get.toNamed(AppPage.canHelpScreenWithOutBottomNav);
              },
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(right: 3.w),
          child: Align(
            alignment: Alignment.bottomRight,
            child: CustomButton(
              buttonName: AppStrings.viewAll,
              width: 22.w,
              height: 3.h,
              buttonColor: AppColors.lightBlue2Color,
              textStyle: customButtonLightTextStyle,
              borderRadius: BorderRadius.circular(20),
              onTap: () => Get.toNamed(AppPage.canHelpScreenWithOutBottomNav),
            ),
          ),
        ),
        SizedBox(
          height: 6.h,
        ),
      ],
    );
  }
}
