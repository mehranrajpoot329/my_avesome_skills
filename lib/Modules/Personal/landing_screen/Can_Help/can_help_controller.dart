import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class CanHelpController extends GetxController {
  TextEditingController messageController = TextEditingController();
  RxInt itemCount = 10.obs;
  RxBool showAllItems = false.obs;

  isShowCanHelpCard() {
    showAllItems.value = true;
  }
}
