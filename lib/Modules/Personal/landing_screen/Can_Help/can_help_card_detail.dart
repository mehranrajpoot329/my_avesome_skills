import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/rating_star.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:sizer/sizer.dart';

import '../../../../routes/app_pages.dart';

class CanHelpCardDetail extends StatelessWidget {
  var getCanHelpCard = Get.arguments;

  @override
  Widget build(BuildContext context) {
    print("------------ $getCanHelpCard ------------");
    return NestedScrollWidget(
        floatingActionButton: Padding(
            padding: EdgeInsets.only(
              left: 10.w,
            ),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Row(
                    children: [
                      Expanded(
                        child: CustomButton(
                            buttonName: AppStrings.message,
                            width: 30.w,
                            height: 5.h,
                            buttonColor: AppColors.blue9Color,
                            textStyle: interWhite14TextStyle,
                            borderRadius: BorderRadius.circular(10),
                            onTap: () =>
                                Get.toNamed(AppPage.canHelpMessageScreen)),
                      ),
                      SizedBox(
                        width: 10.w,
                      ),
                      Expanded(
                        child: CustomButton(
                            buttonName: AppStrings.call,
                            width: 30.w,
                            height: 5.h,
                            buttonColor: AppColors.green5Color,
                            textStyle: black14Color,
                            borderRadius: BorderRadius.circular(10),
                            onTap: () {}),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  GestureDetector(
                    onTap: () => WidgetMethod.bottomSheet(context),
                    child: FittedBox(
                      child: SvgPicture.asset(
                        'assets/images/micMan.svg',
                        height: 23.sp,
                        width: 23.sp,
                      ),
                    ),
                  )
                ])),
        body: Padding(
          padding: EdgeInsets.only(
            left: 2.w,
            right: 2.w,
          ),
          child: Container(
              height: Get.height,
              width: Get.width,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: const BorderRadius.only(
                    topRight: Radius.circular(20),
                    topLeft: Radius.circular(20),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.2),
                      blurRadius: 4.0,
                      spreadRadius: 2.0,
                    )
                  ]),
              child: ListView(
                  padding: EdgeInsets.zero,
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 5.w, right: 5.w),
                      height: 8.h,
                      width: 80.w,
                      decoration: const BoxDecoration(
                          color: AppColors.blueAccent3Color,
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(20),
                              topRight: Radius.circular(20))),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "",
                            style: whiteBoldTwelveColor,
                          ),
                          CircleAvatar(
                              radius: 15,
                              backgroundColor: AppColors.lightBlueColor,
                              child: Icon(
                                Icons.favorite,
                                color: Colors.red,
                                size: 15.sp,
                              )),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 5.w, right: 2.w),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                height: 100,
                                width: 100,
                                decoration: const BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                        image: AssetImage(
                                            'assets/images/profile_alex.png'),
                                        fit: BoxFit.fitHeight)),
                              ),
                              SizedBox(
                                width: 5.w,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    width: 55.w,
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: Text(
                                            AppStrings.alex,
                                            style: blue18MulishTextStyle,
                                          ),
                                        ),
                                        Column(
                                          children: [
                                            RatingStar(
                                                textStyle: greyTextColor,
                                                isNumberRating: true,
                                                ratingNumber: '64'),
                                            SizedBox(
                                              height: 1.h,
                                            ),
                                            Text(
                                              AppStrings.jobSuccessPercentage,
                                              textAlign: TextAlign.center,
                                              style: greenMulishTextStyle,
                                            )
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    height: 2.h,
                                  ),
                                  Text(
                                    AppStrings.featuredSkill,
                                    style: blue12MulishTextStyle,
                                  ),
                                  SizedBox(
                                    height: 1.h,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        AppStrings.helpMoving,
                                        style: blackMulishTextStyle,
                                      ),
                                      SizedBox(width: 16.w),
                                      Text(
                                        AppStrings.fortyOne,
                                        style: blue12MulishTextStyle,
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 0.5.h,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        AppStrings.homeRepairs,
                                        style: blackMulishTextStyle,
                                      ),
                                      SizedBox(width: 18.w),
                                      Text(
                                        AppStrings.thirtySeven,
                                        style: blue12MulishTextStyle,
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 0.5.h,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        AppStrings.furnitureAssembly,
                                        style: blackMulishTextStyle,
                                      ),
                                      SizedBox(width: 11.w),
                                      Text(
                                        AppStrings.thirtyFive,
                                        style: blue12MulishTextStyle,
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 1.h,
                          ),
                          SizedBox(
                            width: 60.w,
                            child: Row(
                              children: [
                                Text(
                                  AppStrings.responseRate,
                                  style: blackTwelveColor,
                                ),
                                SizedBox(
                                  width: 1.w,
                                ),
                                Text(
                                  ':',
                                  style: blackTwelveColor,
                                ),
                                SizedBox(
                                  width: 1.w,
                                ),
                                Text(
                                  AppStrings.ninetyEight,
                                  style: blackTwelveColor,
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 0.5.h,
                          ),
                          SizedBox(
                            width: 70.w,
                            child: Row(
                              children: [
                                Text(
                                  AppStrings.responseTime,
                                  style: blackTwelveColor,
                                ),
                                SizedBox(
                                  width: 1.w,
                                ),
                                Text(
                                  ':',
                                  style: blackTwelveColor,
                                ),
                                SizedBox(
                                  width: 1.w,
                                ),
                                Text(
                                  AppStrings.withInHalfHour,
                                  style: blackTwelveColor,
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 0.5.h,
                          ),
                          SizedBox(
                            width: 44.w,
                            child: Row(
                              children: [
                                Text(
                                  AppStrings.jobSuccess,
                                  style: blackTwelveColor,
                                ),
                                SizedBox(
                                  width: 1.w,
                                ),
                                Text(
                                  ':',
                                  style: blackTwelveColor,
                                ),
                                SizedBox(
                                  width: 1.w,
                                ),
                                Text(
                                  AppStrings.ninetyNine,
                                  style: blackTwelveColor,
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 1.h,
                          ),
                          Row(
                            children: [
                              SvgPicture.asset(
                                'assets/images/post_icon/thumb_icon.svg',
                                height: 14.sp,
                                width: 14.sp,
                              ),
                              SizedBox(
                                width: 1.w,
                              ),
                              Text(
                                AppStrings.oneFortyTwo,
                                style: blackBoldPoppinsColor,
                              ),
                              SizedBox(
                                width: 3.w,
                              ),
                              SvgPicture.asset(
                                'assets/images/post_icon/visible_blue.svg',
                                height: 14.sp,
                                width: 14.sp,
                              ),
                              const SizedBox(
                                width: 5,
                              ),
                              Text(
                                AppStrings.oneFortyTwo,
                                style: blackBoldPoppinsColor,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 2.h,
                          ),
                          Text(
                            AppStrings.rightPersonJob,
                            style: blueMulish1TextStyle,
                          ),
                          SizedBox(
                            height: 1.h,
                          ),
                          Text(
                            AppStrings.suppliesAndResource,
                            style: blackSixHundredWeightColor,
                          ),
                          SizedBox(
                            height: 2.h,
                          ),
                          const Text(
                            AppStrings.abn,
                            style: blue16ColorTextStyle,
                          ),
                          SizedBox(
                            height: 2.h,
                          ),
                          Row(
                            children: [
                              SvgPicture.asset(
                                  'assets/images/post_icon/location_green.svg'),
                              SizedBox(
                                width: 1.h,
                              ),
                              const Text(
                                AppStrings.southPort,
                                style: greenBoldPoppinsColor,
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 2.h,
                          ),
                          buildInfo('assets/images/post_icon/mobile_icon.svg',
                              AppStrings.mobileNumber),
                          SizedBox(
                            height: 2.h,
                          ),
                          buildInfo('assets/images/post_icon/email_icon.svg',
                              AppStrings.emailSpam),
                          SizedBox(
                            height: 2.h,
                          ),
                          buildInfo('assets/images/post_icon/web_icon.svg',
                              AppStrings.webMyClean),
                          SizedBox(
                            height: 3.h,
                          ),
                          Align(
                            alignment: Alignment.center,
                            child: CustomButton(
                                buttonName: AppStrings.acceptJob,
                                width: 35.w,
                                height: 5.h,
                                buttonColor: AppColors.blue10Color,
                                textStyle: black14Color,
                                borderRadius: BorderRadius.circular(10),
                                onTap: () {}),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 15.h,
                    ),
                  ])),
        ));
  }

  Widget buildInfo(String image, String text) {
    return Row(
      children: [
        SvgPicture.asset(
          image,
          width: 5.w,
          fit: BoxFit.fitWidth,
        ),
        SizedBox(
          width: 4.w,
        ),
        Text(
          text,
          style: blackBoldPoppinsColor,
        )
      ],
    );
  }
}
