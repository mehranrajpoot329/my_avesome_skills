import 'package:AwesomeSkills/Modules/Personal/drawers_screens/messages/message_controller.dart';
import 'package:AwesomeSkills/models/firebase_model/message_model.dart';
import 'package:AwesomeSkills/models/firebase_model/message_text_show.dart';
import 'package:AwesomeSkills/network/base_response.dart';
import 'package:AwesomeSkills/network/end_points.dart';
import 'package:AwesomeSkills/network/requests.dart';
import 'package:AwesomeSkills/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import '../../../../models/api_model/personal_model/get_need_help_model.dart';

class NeedHelpController extends GetxController {
  TextEditingController messageController = TextEditingController();
  final messageDataController = Get.put(MessageController());
  RxInt itemCount = 10.obs;
  RxBool showAllItems = false.obs;

  isShowCanHelpCard() {
    showAllItems.value = true;
  }

  @override
  onInit() {
    messageDataController.getFirebaseUserData();

    //  retrieveData();
    super.onInit();
  }

  TextEditingController messageTextEditingController = TextEditingController();

  RxList<MessageModel> messageList = <MessageModel>[].obs;
  RxList<MessageTextShowModel> messageTextShowList =
      <MessageTextShowModel>[].obs;

  bool isScrollable = false;

  CroppedFile? messagePicUpload;

  Future<void> selectImage(ImageSource selectedSource) async {
    messagePicUpload = await Utils.getImage(source: selectedSource);
    update();
  }
}
