import 'package:AwesomeSkills/Modules/Personal/landing_screen/Need_Help/need_help_controller.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/landing_screen_controller.dart';
import 'package:AwesomeSkills/network/end_points.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/Need_Help/need_help_card.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/need_row_with_map_view.dart';
import 'package:shimmer/shimmer.dart';
import 'package:sizer/sizer.dart';
import '../../../../Constants/app_colors.dart';
import '../../../../Constants/app_strings.dart';
import '../../../../Constants/text_style.dart';
import '../../../../routes/app_pages.dart';
import '../../../../widgets/button/custom_button.dart';

class NeedHelpLandingScreen extends StatelessWidget {
  final needHelpController = Get.put(LandingScreenController());

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        NeedHelpRowWithMap(textName: AppStrings.needHelp, isText: true),
        SizedBox(height: 2.h),
        Padding(
          padding: EdgeInsets.only(left: 3.w, right: 3.w),
          child: Obx(
            () => NeedHelpCard(
              initialValueOfStar: 0.0,
              reviewStarValue: "(${AppStrings.zero})",
              title:
                  needHelpController.getNeedHelpCard.value.row![0].title ?? "",
              jobPrice: needHelpController
                          .getNeedHelpCard.value.row![0].jobPrice ==
                      ""
                  ? "\$0"
                  : "\$${needHelpController.getNeedHelpCard.value.row![0].jobPrice}",
              userName: needHelpController
                      .getNeedHelpCard.value.row![0].userData!.firstName ??
                  "",
              location: needHelpController
                          .getNeedHelpCard.value.row![0].userData!.address ==
                      null
                  ? ""
                  : needHelpController.getNeedHelpCard.value.row![0].userData!
                          .address!.streetAddress ??
                      "",
              durationOfJob: needHelpController
                          .getNeedHelpCard.value.row![0].startingTime ==
                      null
                  ? ""
                  : needHelpController.getNeedHelpCard.value.row![0]
                          .startingTime!.duration ??
                      "",
              imageUrl:
                  "${EndPoints.baseURL}${needHelpController.getNeedHelpCard.value.row![0].userData!.photoURL}",
              onTap: () {
                Get.toNamed(AppPage.needHelpScreenWithOutBottomNav);
              },
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(right: 3.w),
          child: Align(
            alignment: Alignment.bottomRight,
            child: CustomButton(
              buttonName: AppStrings.viewAll,
              width: 22.w,
              height: 3.h,
              buttonColor: AppColors.lightBlue2Color,
              textStyle: customButtonLightTextStyle,
              borderRadius: BorderRadius.circular(20),
              onTap: () => Get.toNamed(AppPage.needHelpScreenWithOutBottomNav),
            ),
          ),
        ),
        SizedBox(
          height: 2.h,
        ),
      ],
    );
  }
}
