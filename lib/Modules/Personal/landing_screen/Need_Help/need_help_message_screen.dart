import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/messages/message_controller.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/Need_Help/need_help_controller.dart';
import 'package:AwesomeSkills/constants/app_strings.dart';
import 'package:AwesomeSkills/firebase/firebase.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/rating_star.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';

class NeedHelpMessagingScreen extends StatelessWidget {
  final needHelpController = Get.put(NeedHelpController());
  final messageController = Get.put(MessageController());
  var data = Get.arguments;

  @override
  Widget build(BuildContext context) {
    return NestedScrollWidget(
        floatingActionButton: WidgetMethod.floatingActionButton(context),
        body: Padding(
          padding: EdgeInsets.only(left: 3.w, right: 2.w),
          child: ListView(
              physics: NeverScrollableScrollPhysics(),
              padding: EdgeInsets.zero,
              children: [
                SizedBox(
                  height: 1.h,
                ),
                Row(
                  children: [
                    GestureDetector(
                      onTap: () => Get.back(),
                      child: Icon(
                        Icons.arrow_back_ios_new,
                        color: Colors.black,
                        size: 3.h,
                      ),
                    ),
                    SizedBox(
                      width: 5.w,
                    ),
                    const Text(
                      AppStrings.message,
                      style: montserrat20TextStyle,
                    ),
                  ],
                ),
                SizedBox(
                  height: 2.h,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      width: 35.w,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            height: 10.h,
                            width: 10.h,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(
                                    color: AppColors.lightBlueColor,
                                    width: 1,
                                    style: BorderStyle.solid),
                                image: DecorationImage(
                                    image: AssetImage(
                                        'assets/images/profile_a_fernendez.png'),
                                    fit: BoxFit.cover)),
                          ),
                          SizedBox(
                            width: 2.w,
                          ),
                          Text(
                            data ?? "",
                            style: blue18MulishTextStyle,
                          ),
                        ],
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        RatingStar(
                          textStyle: greyTextColor,
                          isNumberRating: true,
                          ratingNumber: AppStrings.sixtyFour,
                        ),
                        Text(
                          AppStrings.jobSuccessPercentage,
                          style: greenMulishTextStyle,
                        )
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  height: 2.h,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      AppStrings.housePaint,
                      style: blue600ColorTextStyle,
                    ),
                    Icon(
                      Icons.favorite,
                      color: Colors.red,
                      size: 24.sp,
                    ),
                  ],
                ),
                SizedBox(
                  height: 2.h,
                ),
                Row(
                  children: [
                    SvgPicture.asset(
                      'assets/images/post_icon/calender_tick_icon.svg',
                      height: 3.h,
                    ),
                    SizedBox(
                      width: 1.w,
                    ),
                    const Text(
                      AppStrings.asap,
                      style: black14Mulish1TextStyle,
                    ),
                    SizedBox(
                      width: 2.w,
                    ),
                    SvgPicture.asset(
                      'assets/images/post_icon/visible_grey.svg',
                      height: 2.h,
                    ),
                    SizedBox(
                      width: 2.w,
                    ),
                    const Text(
                      AppStrings.five,
                      style: black14Mulish1TextStyle,
                    ),
                  ],
                ),
                SizedBox(
                  height: 2.h,
                ),
                const Text(
                  AppStrings.address,
                  style: blue700MulishTextStyle,
                ),
                SizedBox(
                  height: 1.h,
                ),
                const Text(
                  AppStrings.southPort,
                  style: black400MulishTextStyle,
                ),
                SizedBox(
                  height: 2.h,
                ),
                const Text(
                  AppStrings.jobValueSmall,
                  style: blue700MulishTextStyle,
                ),
                SizedBox(
                  height: 1.h,
                ),
                Text(
                  AppStrings.eightHundred,
                  style: greenColor24TextStyle,
                ),
                SizedBox(
                  height: 1.h,
                ),
                const Text(
                  AppStrings.jobDetail,
                  style: black18MulishTextStyle,
                ),
                SizedBox(
                  height: 1.h,
                ),
                Text(
                  AppStrings.iNeedPaimter,
                  style: blackMulish300TextStyle,
                ),
                SizedBox(
                  height: 1.h,
                ),
                Divider(),
                SizedBox(
                  height: 1.h,
                ),
                Obx(() => SizedBox(
                    height: 50.h,
                    child: ListView.builder(
                        itemCount:
                            needHelpController.messageTextShowList.length,
                        itemBuilder: (BuildContext context, int index) {
                          // DateTime d = controller
                          //     .messageTextShowList[index].date!
                          //     .toDate();
                          return Column(
                            children: [
                              FirebaseAuthorization
                                          .firebaseAuth.currentUser!.uid !=
                                      needHelpController
                                          .messageTextShowList[index].uid
                                  ? Column(
                                      children: [
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: [
                                            Container(
                                              height: 5.h,
                                              width: 5.h,
                                              decoration: const BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  image: DecorationImage(
                                                      image: AssetImage(
                                                        'assets/images/profile_messager2.png',
                                                      ),
                                                      fit: BoxFit.cover)),
                                            ),
                                            SizedBox(
                                              width: 1.w,
                                            ),
                                            const Text(
                                              AppStrings.tenTwenty,
                                              style: grey12MulishTextStyle,
                                            )
                                          ],
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(left: 10.w),
                                          child: Container(
                                            padding: EdgeInsets.only(
                                                left: 3.w, top: 1.h),
                                            height: 5.h,
                                            width: 70.w,
                                            decoration: const BoxDecoration(
                                              color: AppColors.whiteGreyColor,
                                              borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(10),
                                                  topLeft: Radius.circular(10),
                                                  bottomLeft:
                                                      Radius.circular(10)),
                                            ),
                                            child: Text(
                                              needHelpController
                                                      .messageTextShowList[
                                                          index]
                                                      .message ??
                                                  "",
                                              style: blackMulish300TextStyle,
                                            ),
                                          ),
                                        ),
                                      ],
                                    )
                                  : Column(
                                      children: [
                                        Padding(
                                          padding: EdgeInsets.only(left: 10.w),
                                          child: Container(
                                            padding: EdgeInsets.only(
                                                left: 2.w, top: 1.h),
                                            height: 5.h,
                                            width: 70.w,
                                            decoration: const BoxDecoration(
                                              color: AppColors.blue15Color,
                                              borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(10),
                                                  bottomRight:
                                                      Radius.circular(10),
                                                  bottomLeft:
                                                      Radius.circular(10)),
                                            ),
                                            child: Text(
                                              needHelpController
                                                      .messageTextShowList[
                                                          index]
                                                      .message ??
                                                  "",
                                              style: blackMulish300TextStyle,
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 0.5.h,
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.end,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: [
                                            const Text(
                                              AppStrings.tenTwenty,
                                              style: grey12MulishTextStyle,
                                            ),
                                            SizedBox(
                                              width: 1.w,
                                            ),
                                            Container(
                                              height: 5.h,
                                              width: 5.h,
                                              decoration: const BoxDecoration(
                                                shape: BoxShape.circle,
                                                image: DecorationImage(
                                                    image: AssetImage(
                                                        'assets/images/profile_messager3.png'),
                                                    fit: BoxFit.cover),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                              SizedBox(
                                height: 2.h,
                              ),
                              Row(
                                children: [
                                  const Expanded(flex: 1, child: Divider()),
                                  SizedBox(
                                    width: 2.w,
                                  ),
                                  const Expanded(
                                    flex: 0,
                                    child: Text(
                                      AppStrings.yesterday,
                                      style: grey12MulishTextStyle,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 2.w,
                                  ),
                                  Expanded(flex: 1, child: Divider()),
                                ],
                              ),
                              SizedBox(
                                height: 1.h,
                              ),
                            ],
                          );
                        }))),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      flex: 2,
                      child: TextFormField(
                        focusNode: FocusNode(skipTraversal: true),
                        controller: needHelpController.messageController,
                        decoration: const InputDecoration(
                            border: InputBorder.none,
                            hintText: AppStrings.typeYourMessage,
                            hintStyle: grey12MulishTextStyle),
                      ),
                    ),
                    Expanded(
                        child: CustomButton(
                            buttonName: AppStrings.reply,
                            width: 30.w,
                            height: 3.h,
                            buttonColor: AppColors.lightBlueColor,
                            textStyle: white12MulishTextStyle,
                            borderRadius: BorderRadius.circular(4),
                            onTap: () => messageController.postMessage())),
                  ],
                ),
                SizedBox(
                  height: 3.h,
                ),
                Row(
                  children: [
                    SvgPicture.asset(
                      'assets/images/post_icon/gallery_icon.svg',
                      height: 3.h,
                    ),
                    SizedBox(
                      width: 1.w,
                    ),
                    SvgPicture.asset(
                      'assets/images/post_icon/video_icon.svg',
                      height: 3.h,
                    ),
                    SizedBox(
                      width: 1.w,
                    ),
                    SvgPicture.asset(
                      'assets/images/post_icon/pdf_icon.svg',
                      height: 3.h,
                    ),
                  ],
                ),
                SizedBox(
                  height: 6.h,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    CustomButton(
                        buttonName: AppStrings.rejectedJob,
                        width: 30.w,
                        height: 4.h,
                        buttonColor: AppColors.red2Color,
                        textStyle: blackMulishTextStyle,
                        borderRadius: BorderRadius.circular(5),
                        onTap: () {}),
                    const SizedBox(
                      width: 10,
                    ),
                    CustomButton(
                        buttonName: AppStrings.acceptJob,
                        width: 30.w,
                        height: 4.h,
                        buttonColor: AppColors.green12Color,
                        textStyle: blackMulishTextStyle,
                        borderRadius: BorderRadius.circular(5),
                        onTap: () {}),
                  ],
                ),
              ]),
        ));
  }
}
