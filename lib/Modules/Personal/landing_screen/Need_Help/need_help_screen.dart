import 'package:AwesomeSkills/Modules/Personal/drawers_screens/messages/message_controller.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/Need_Help/need_help_card_detail.dart';
import 'package:AwesomeSkills/local/db/local_storage.dart';
import 'package:AwesomeSkills/models/need_card_show_detail_model.dart';
import 'package:AwesomeSkills/network/end_points.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/Need_Help/need_help_card.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/Need_Help/need_help_controller.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/landing_screen_controller.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/bottom_sheet_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/need_row_with_map_view.dart';
import 'package:shimmer/shimmer.dart';
import 'package:sizer/sizer.dart';

import '../../../../Constants/app_colors.dart';
import '../../../../routes/app_pages.dart';

class NeedHelpScreen extends StatelessWidget {
  final landingScreenController = Get.put(LandingScreenController());
  final needHelpController = Get.put(NeedHelpController());
  final messageController = Get.put(MessageController());

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 3.w, right: 2.w),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 1.h,
            ),
            Row(
              children: [
                GestureDetector(
                  onTap: () => Get.back(),
                  child: Icon(
                    Icons.arrow_back_ios_new,
                    color: Colors.black,
                    size: 3.h,
                  ),
                ),
                SizedBox(
                  width: 5.w,
                ),
                const Text(
                  AppStrings.iNeedHelp,
                  style: montserrat20TextStyle,
                ),
              ],
            ),
            Padding(
              padding: EdgeInsets.only(
                left: 1.w,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 2.h,
                  ),
                  NeedHelpRowWithMap(
                    isText: true,
                    textName: AppStrings.needHelp,
                  ),
                ],
              ),
            ),
            Obx(
              () => ListView.builder(
                physics: const ScrollPhysics(),
                shrinkWrap: true,
                itemCount: landingScreenController
                                .getNeedHelpCard.value.row!.length >
                            5 &&
                        !needHelpController.showAllItems.value
                    ? 5
                    : landingScreenController.getNeedHelpCard.value.row!.length,
                itemBuilder: (BuildContext context, int index) {
                  var dataApi =
                      landingScreenController.getNeedHelpCard.value.row![index];
                  return landingScreenController.getNeedHelpCard.value.row ==
                          null
                      ? Shimmer.fromColors(
                          baseColor: Colors.grey[300]!,
                          highlightColor: Colors.grey[100]!,
                          child: ListView.builder(
                            itemCount: 6,
                            itemBuilder: (context, index) {
                              return Card(
                                elevation: 1.0,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(16),
                                ),
                                child: const SizedBox(height: 80),
                              );
                            },
                          ),
                        )
                      : Center(
                          child: NeedHelpCard(
                            initialValueOfStar: 0.0,
                            reviewStarValue: "(${AppStrings.zero})",
                            title: dataApi.title ?? "",
                            jobPrice: dataApi.jobPrice == ""
                                ? "\$0"
                                : "\$${dataApi.jobPrice}",
                            userName: landingScreenController.getNeedHelpCard
                                    .value.row![index].userData!.firstName ??
                                "",
                            location: landingScreenController.getNeedHelpCard
                                        .value.row![index].userData!.address ==
                                    null
                                ? ""
                                : landingScreenController
                                        .getNeedHelpCard
                                        .value
                                        .row![index]
                                        .userData!
                                        .address!
                                        .streetAddress ??
                                    "",
                            durationOfJob: landingScreenController
                                        .getNeedHelpCard
                                        .value
                                        .row![index]
                                        .startingTime ==
                                    null
                                ? ""
                                : landingScreenController.getNeedHelpCard.value
                                        .row![index].startingTime!.duration ??
                                    "",
                            imageUrl:
                                "${EndPoints.baseURL}${landingScreenController.getNeedHelpCard.value.row![index].userData!.photoURL}",
                            onTap: () {
                              HiveHelper.isLoggedIn()
                                  ? Get.to(() => NeedHelpCardDetailScreen(),
                                      arguments: landingScreenController
                                          .getNeedHelpCard.value.row![index].id)

                                  // Get.toNamed(
                                  //         AppPage.needHelpCardDetailScreen)
                                  : BottomSheetAdScreen.isLoginedOutBottomSheet(
                                      context);
                            },
                          ),
                        );
                },
              ),
            ),
            Obx(() => needHelpController.showAllItems.value
                ? Container()
                : Align(
                    alignment: Alignment.bottomRight,
                    child: CustomButton(
                      buttonName: AppStrings.viewAll,
                      width: 22.w,
                      height: 3.h,
                      buttonColor: AppColors.lightBlue2Color,
                      textStyle: customButtonLightTextStyle,
                      borderRadius: BorderRadius.circular(20),
                      onTap: () => needHelpController.isShowCanHelpCard(),
                    ),
                  )),
            SizedBox(
              height: 6.h,
            ),
          ],
        ),
      ),
    );
  }
}
