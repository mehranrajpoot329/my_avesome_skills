import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/messages/message_controller.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/Need_Help/need_help_controller.dart';
import 'package:AwesomeSkills/constants/app_colors.dart';
import 'package:AwesomeSkills/constants/app_strings.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/rating_star.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';

class NeedHelpCard extends StatelessWidget {
  final needHelpLandingController = Get.put(NeedHelpController());

  String title,
      userName,
      imageUrl,
      location,
      durationOfJob,
      jobPrice,
      reviewStarValue;
  double initialValueOfStar;
  Function onTap;

  NeedHelpCard({
    required this.onTap,
    required this.title,
    required this.userName,
    required this.imageUrl,
    required this.location,
    required this.durationOfJob,
    required this.jobPrice,
    required this.reviewStarValue,
    required this.initialValueOfStar,
  });
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onTap(),
      child: Column(
        children: [
          Container(
            height: 30.h,
            width: 90.w,
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                    offset: Offset(0, 3),
                    blurRadius: 4.0,
                    spreadRadius: 2.0,
                    color: Colors.grey.withOpacity(0.4))
              ],
              borderRadius: BorderRadius.circular(20),
              color: Colors.white,
            ),
            child: Stack(
              children: [
                Container(
                  padding: EdgeInsets.only(left: 9.w, right: 8.w),
                  height: 6.h,
                  width: 90.w,
                  decoration: const BoxDecoration(
                      color: AppColors.purpleColor,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20),
                          topRight: Radius.circular(20))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        title,
                        style: whiteBoldTwelveColor,
                      ),
                      CircleAvatar(
                          radius: 12.sp,
                          backgroundColor: AppColors.blueAccent2Color,
                          child: Icon(
                            Icons.favorite,
                            color: Colors.white,
                            size: 3.h,
                          )),
                    ],
                  ),
                ),
                Positioned(
                  top: 7.5.h,
                  left: 10.w,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        userName,
                        style: interBlack2TextStyle,
                      ),
                      SizedBox(
                        height: 1.h,
                      ),
                      CachedNetworkImage(
                        imageUrl: imageUrl ?? "",
                        placeholder: (context, url) =>
                            CircularProgressIndicator(),
                        errorWidget: (context, url, error) => Container(
                          height: 13.h,
                          width: 13.h,
                          decoration: BoxDecoration(
                              color: Colors.grey.withOpacity(0.2),
                              shape: BoxShape.circle),
                          child: Center(
                            child: Icon(
                              Icons.person,
                              color: Colors.grey,
                              size: 10.h,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  top: 8.h,
                  left: 50.w,
                  child: Column(
                    children: [
                      Row(
                        children: [
                          RatingBar.builder(
                            itemSize: 20,
                            initialRating: initialValueOfStar,
                            minRating: 0,
                            direction: Axis.horizontal,
                            allowHalfRating: true,
                            itemCount: 5,
                            itemBuilder: (context, _) => Icon(
                              Icons.star,
                              color: Colors.amber,
                            ),
                            onRatingUpdate: (rating) {
                              print(rating);
                            },
                          ),
                          Text(
                            reviewStarValue,
                            style: greyTextColor,
                          )
                        ],
                      ),
                      SizedBox(
                        height: 1.5.h,
                      ),
                      Text(
                        jobPrice,
                        style: interGreenTextStyle,
                      ),
                      SizedBox(
                        height: 1.h,
                      ),
                      CustomButton(
                        buttonName: AppStrings.jobValue,
                        width: 25.w,
                        height: 3.h,
                        buttonColor: AppColors.lightGreenColor,
                        textStyle: blackMulishTextStyle,
                        borderRadius: BorderRadius.circular(3),
                        onTap: () {},
                      ),
                      SizedBox(
                        height: 1.h,
                      ),
                      Row(
                        children: [
                          Image.asset(
                            'assets/images/post_icon/calendar.png',
                            height: 10.sp,
                          ),
                          SizedBox(width: 3.w),
                          Text(
                            durationOfJob,
                            style: interBlackTextStyle,
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                Positioned(
                  bottom: 0,
                  right: 0,
                  left: 0,
                  child: Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: 9.w, vertical: 0.7.h),
                    height: 4.h,
                    width: Get.width,
                    decoration: const BoxDecoration(
                        color: AppColors.blueAccent2Color,
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20))),
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        location,
                        style: interBlackTextStyle,
                        textAlign: TextAlign.start,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 2.h,
          )
        ],
      ),
    );
  }
}
