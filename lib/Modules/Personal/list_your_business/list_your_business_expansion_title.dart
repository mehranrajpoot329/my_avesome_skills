import 'dart:developer';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Modules/Personal/ad_Screen/custom_ad_info_text_form_field.dart';
import 'package:AwesomeSkills/Modules/Personal/list_your_business/free_plan_card.dart';
import 'package:AwesomeSkills/Modules/Personal/list_your_business/list_your_business_controller.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:AwesomeSkills/widgets/custom_textfield/custom_textfield.dart';
import 'package:AwesomeSkills/widgets/drop_down.dart';
import 'package:sizer/sizer.dart';
import '../../../Constants/app_strings.dart';
import '../../../Constants/text_style.dart';
import '../../../constants/app_colors.dart';
import '../drawers_screens/settings/social_media_settings/build_social_media_widget.dart';

class ListYourBusinessExpansionTile extends StatelessWidget {
  ListYourBusinessExpansionTile({Key? key}) : super(key: key);

  final controller = Get.put(ListYourBusinessController());

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: 1.h,
        ),
        ExpansionTile(
          title: Text(
            AppStrings.applicantDetails,
            style: lightBlue500TextStyle,
          ),
          children: [
            ListTile(
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomAdInfoTextFormField(
                      isOptionalText: false,
                      titleTextStyle: lightBlue500TextStyle,
                      height: 7.h,
                      width: 100.w,
                      title: AppStrings.industry,
                      hintText: AppStrings.fullName,
                      controller: controller.industryController),
                  SizedBox(
                    height: 1.h,
                  ),
                  CustomAdInfoTextFormField(
                      isOptionalText: false,
                      titleTextStyle: lightBlue500TextStyle,
                      height: 7.h,
                      width: 100.w,
                      title: AppStrings.positionInTheCompany,
                      hintText: AppStrings.ownerDirectorCeo,
                      controller: controller.industryController),
                  SizedBox(
                    height: 2.h,
                  ),
                  ExpansionTile(
                      tilePadding: EdgeInsets.zero,
                      title: Text(
                        AppStrings.createPassword,
                        style: red4ColorTextStyle,
                      ),
                      children: [
                        CustomTextFormField(
                            height: 7.h,
                            width: 100.w,
                            borderRadius: BorderRadius.circular(10),
                            hintTextStyle: black12Color,
                            hintText: AppStrings.newPassword,
                            name: 'new_password',
                            color: AppColors.lightBlue6Color,
                            validator: (String? value) {},
                            controller: controller.administratorController),
                        SizedBox(
                          height: 2.h,
                        ),
                        CustomTextFormField(
                            height: 7.h,
                            width: 100.w,
                            borderRadius: BorderRadius.circular(10),
                            hintTextStyle: black12Color,
                            hintText: AppStrings.confirmPassword,
                            name: 'confirm_password',
                            color: AppColors.lightBlue6Color,
                            validator: (String? value) {},
                            controller: controller.administratorController),
                        SizedBox(
                          height: 3.h,
                        ),
                        CustomButton(
                            buttonName: AppStrings.change,
                            width: 30.w,
                            height: 5.h,
                            buttonColor: AppColors.lightGreen3Color,
                            textStyle: black14Color,
                            borderRadius: BorderRadius.circular(7),
                            onTap: () {}),
                        SizedBox(
                          height: 2.h,
                        ),
                      ]),
                  SizedBox(
                    height: 2.h,
                  ),
                  CustomAdInfoTextFormField(
                      isOptionalText: false,
                      titleTextStyle: lightBlue500TextStyle,
                      height: 7.h,
                      width: 100.w,
                      title: AppStrings.userNameYouWillNeed,
                      hintText: '',
                      controller: controller.industryController),
                  SizedBox(
                    height: 1.h,
                  ),
                  CustomAdInfoTextFormField(
                      isOptionalText: false,
                      titleTextStyle: lightBlue500TextStyle,
                      height: 7.h,
                      width: 100.w,
                      title: AppStrings.passwordYouWillNeed,
                      hintText: '',
                      controller: controller.industryController),
                  SizedBox(
                    height: 2.h,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              AppStrings.profilePicture,
                              style: lightBlue500TextStyle,
                            ),
                            SizedBox(
                              height: 1.h,
                            ),
                            DottedBorder(
                              dashPattern: const [6, 2],
                              radius: const Radius.circular(10),
                              color: AppColors.indigoColor,
                              strokeWidth: 1.5,
                              child: Container(
                                height: 15.h,
                                width: 47.w,
                                decoration: const BoxDecoration(
                                  color: Colors.transparent,
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    SvgPicture.asset(
                                      'assets/images/post_icon/upload_icon.svg',
                                    ),
                                    const SizedBox(
                                      height: 2,
                                    ),
                                    const FittedBox(
                                      child: Text(
                                        AppStrings.dragAndDrop,
                                        style: black9PoppinsColor,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 0.3.h,
                                    ),
                                    const Text(
                                      AppStrings.or,
                                      style: black9PoppinsColor,
                                    ),
                                    const SizedBox(
                                      height: 2,
                                    ),
                                    const Text(
                                      AppStrings.clickToBrowse,
                                      style: yellowPoppins300TextStyle,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 2.w,
                      ),
                      Text(
                        AppStrings.OR,
                        style: blackBoldPoppinsColor,
                      ),
                      SizedBox(
                        width: 2.w,
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              AppStrings.logoText,
                              style: lightBlue500TextStyle,
                            ),
                            SizedBox(
                              height: 1.h,
                            ),
                            DottedBorder(
                              dashPattern: const [6, 2],
                              radius: const Radius.circular(10),
                              color: AppColors.indigoColor,
                              strokeWidth: 1.5,
                              child: Container(
                                height: 15.h,
                                width: 47.w,
                                decoration: const BoxDecoration(
                                  color: Colors.transparent,
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    SvgPicture.asset(
                                      'assets/images/post_icon/upload_icon.svg',
                                    ),
                                    const SizedBox(
                                      height: 2,
                                    ),
                                    const FittedBox(
                                      child: Text(
                                        AppStrings.dragAndDrop,
                                        style: black9PoppinsColor,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 0.3.h,
                                    ),
                                    const Text(
                                      AppStrings.or,
                                      style: black9PoppinsColor,
                                    ),
                                    const SizedBox(
                                      height: 2,
                                    ),
                                    const Text(
                                      AppStrings.clickToBrowse,
                                      style: yellowPoppins300TextStyle,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 2.h,
            )
          ],
        ),
        ExpansionTile(
          title: Row(
            children: [
              Text(
                AppStrings.administrator,
                style: lightBlue500TextStyle,
              ),
              SizedBox(
                width: 1.w,
              ),
              Expanded(
                child: Text(
                  AppStrings.createManage,
                  style: black8Color,
                ),
              ),
            ],
          ),
          children: [
            ListTile(
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomTextFormField(
                      height: 7.h,
                      width: 100.w,
                      borderRadius: BorderRadius.circular(10),
                      hintTextStyle: black12Color,
                      hintText: AppStrings.fullName,
                      name: 'full_name',
                      color: AppColors.lightBlue6Color,
                      validator: (String? value) {},
                      controller: controller.administratorController),
                  SizedBox(
                    height: 2.h,
                  )
                ],
              ),
            )
          ],
        ),
        ExpansionTile(
          title: Text(
            AppStrings.meetTheTeam,
            style: lightBlue500TextStyle,
          ),
          children: [
            ListTile(
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  DottedBorder(
                    dashPattern: const [6, 2],
                    radius: const Radius.circular(10),
                    color: AppColors.indigoColor,
                    strokeWidth: 1.5,
                    child: Container(
                      height: 10.h,
                      width: 20.w,
                      decoration: const BoxDecoration(
                        color: Colors.transparent,
                      ),
                      child: Stack(
                        clipBehavior: Clip.none,
                        children: [
                          Positioned(
                            top: 1.h,
                            left: 5.w,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Icon(
                                  Icons.add,
                                  color: AppColors.indigoColor,
                                  size: 3.h,
                                ),
                                SizedBox(
                                  height: 2.h,
                                ),
                                Text(
                                  AppStrings.add,
                                  style: indigo14ColorTextStyle,
                                ),
                              ],
                            ),
                          ),
                          Positioned(
                            bottom: -3,
                            right: -6.0,
                            child: SvgPicture.asset(
                              'assets/images/post_icon/edit_icon.svg',
                              height: 13.sp,
                              width: 13.sp,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                ],
              ),
            ),
          ],
        ),
        ExpansionTile(
          title: Text(
            AppStrings.companyBusinessDetail,
            style: lightBlue500TextStyle,
          ),
          children: [
            ListTile(
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomTextFormField(
                      height: 7.h,
                      width: 100.w,
                      borderRadius: BorderRadius.circular(10),
                      hintTextStyle: black12Color,
                      hintText: AppStrings.searchByBusiness,
                      name: 'full_name',
                      color: AppColors.lightBlue6Color,
                      validator: (String? value) {},
                      controller: controller.administratorController),
                  SizedBox(
                    height: 1.h,
                  ),
                  CustomAdInfoTextFormField(
                      isOptionalText: false,
                      titleTextStyle: lightBlue500TextStyle,
                      height: 7.h,
                      width: 100.w,
                      title: AppStrings.abnText,
                      hintText: '',
                      controller: controller.industryController),
                  SizedBox(
                    height: 1.h,
                  ),
                  CustomAdInfoTextFormField(
                      isOptionalText: false,
                      titleTextStyle: lightBlue500TextStyle,
                      height: 7.h,
                      width: 100.w,
                      title: AppStrings.tradingName,
                      hintText: AppStrings.thisIsTheNameTo,
                      controller: controller.industryController),
                  SizedBox(
                    height: 1.h,
                  ),
                  CustomAdInfoTextFormField(
                      isOptionalText: false,
                      titleTextStyle: lightBlue500TextStyle,
                      height: 7.h,
                      width: 100.w,
                      title: AppStrings.subrub,
                      hintText: AppStrings.ashmore,
                      controller: controller.industryController),
                  SizedBox(
                    height: 1.h,
                  ),
                  CustomAdInfoTextFormField(
                      isOptionalText: false,
                      titleTextStyle: lightBlue500TextStyle,
                      height: 7.h,
                      width: 100.w,
                      title: AppStrings.address,
                      hintText: AppStrings.noPoBoxAccepted,
                      controller: controller.industryController),
                  SizedBox(
                    height: 1.h,
                  ),
                  CustomAdInfoTextFormField(
                      isOptionalText: false,
                      titleTextStyle: lightBlue500TextStyle,
                      height: 7.h,
                      width: 100.w,
                      title: AppStrings.state,
                      hintText: AppStrings.QLD,
                      controller: controller.industryController),
                  SizedBox(
                    height: 1.h,
                  ),
                  CustomAdInfoTextFormField(
                      isOptionalText: false,
                      titleTextStyle: lightBlue500TextStyle,
                      height: 7.h,
                      width: 100.w,
                      title: AppStrings.postCode,
                      hintText: '',
                      controller: controller.industryController),
                  SizedBox(
                    height: 1.h,
                  ),
                  CustomAdInfoTextFormField(
                      isOptionalText: false,
                      titleTextStyle: lightBlue500TextStyle,
                      height: 7.h,
                      width: 100.w,
                      title: AppStrings.mobile,
                      hintText: '',
                      controller: controller.industryController),
                  SizedBox(
                    height: 1.h,
                  ),
                  CustomAdInfoTextFormField(
                      isOptionalText: false,
                      titleTextStyle: lightBlue500TextStyle,
                      height: 7.h,
                      width: 100.w,
                      title: AppStrings.landLine,
                      hintText: '',
                      controller: controller.industryController),
                  SizedBox(
                    height: 1.h,
                  ),
                  CustomAdInfoTextFormField(
                      isOptionalText: false,
                      titleTextStyle: lightBlue500TextStyle,
                      height: 7.h,
                      width: 100.w,
                      title: AppStrings.email,
                      hintText: '',
                      controller: controller.industryController),
                  SizedBox(
                    height: 2.h,
                  ),
                ],
              ),
            ),
          ],
        ),
        ExpansionTile(
          title: Text(
            AppStrings.doYouHaveOrRequire,
            style: lightBlue500TextStyle,
          ),
          children: [
            ListTile(
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      CustomButton(
                          buttonName: AppStrings.yes,
                          width: 25.w,
                          height: 4.h,
                          buttonColor: AppColors.lightGreen3Color,
                          textStyle: black500Color,
                          borderRadius: BorderRadius.circular(20),
                          onTap: () {}),
                      SizedBox(
                        width: 3.w,
                      ),
                      CustomButton(
                          buttonName: AppStrings.no,
                          width: 25.w,
                          height: 4.h,
                          buttonColor: AppColors.redAccent2Color,
                          textStyle: black500Color,
                          borderRadius: BorderRadius.circular(20),
                          onTap: () {}),
                    ],
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              AppStrings.license,
                              style: lightBlue500TextStyle,
                            ),
                            SizedBox(
                              height: 1.h,
                            ),
                            DottedBorder(
                              dashPattern: const [6, 2],
                              radius: const Radius.circular(10),
                              color: AppColors.indigoColor,
                              strokeWidth: 1.5,
                              child: Container(
                                height: 15.h,
                                width: 47.w,
                                decoration: const BoxDecoration(
                                  color: Colors.transparent,
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    SvgPicture.asset(
                                      'assets/images/post_icon/upload_icon.svg',
                                    ),
                                    const SizedBox(
                                      height: 2,
                                    ),
                                    const FittedBox(
                                      child: Text(
                                        AppStrings.dragAndDrop,
                                        style: black9PoppinsColor,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 0.3.h,
                                    ),
                                    const Text(
                                      AppStrings.or,
                                      style: black9PoppinsColor,
                                    ),
                                    const SizedBox(
                                      height: 2,
                                    ),
                                    const Text(
                                      AppStrings.clickToBrowse,
                                      style: yellowPoppins300TextStyle,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 10.w,
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              AppStrings.certificate,
                              style: lightBlue500TextStyle,
                            ),
                            SizedBox(
                              height: 1.h,
                            ),
                            DottedBorder(
                              dashPattern: const [6, 2],
                              radius: const Radius.circular(10),
                              color: AppColors.indigoColor,
                              strokeWidth: 1.5,
                              child: Container(
                                height: 15.h,
                                width: 47.w,
                                decoration: const BoxDecoration(
                                  color: Colors.transparent,
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    SvgPicture.asset(
                                      'assets/images/post_icon/upload_icon.svg',
                                    ),
                                    const SizedBox(
                                      height: 2,
                                    ),
                                    const FittedBox(
                                      child: Text(
                                        AppStrings.dragAndDrop,
                                        style: black9PoppinsColor,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 0.3.h,
                                    ),
                                    const Text(
                                      AppStrings.or,
                                      style: black9PoppinsColor,
                                    ),
                                    const SizedBox(
                                      height: 2,
                                    ),
                                    const Text(
                                      AppStrings.clickToBrowse,
                                      style: yellowPoppins300TextStyle,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 2.h,
                  )
                ],
              ),
            )
          ],
        ),
        ExpansionTile(
          title: Text(
            AppStrings.website,
            style: lightBlue500TextStyle,
          ),
          children: [
            ListTile(
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomTextFormField(
                      height: 7.h,
                      width: 100.w,
                      borderRadius: BorderRadius.circular(10),
                      hintTextStyle: black12Color,
                      hintText: AppStrings.link,
                      name: 'link',
                      color: AppColors.lightBlue6Color,
                      validator: (String? value) {},
                      controller: controller.administratorController),
                  SizedBox(
                    height: 1.h,
                  )
                ],
              ),
            )
          ],
        ),
        ExpansionTile(
          title: Text(
            AppStrings.tagLine,
            style: lightBlue500TextStyle,
          ),
          children: [
            ListTile(
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomTextFormField(
                      height: 7.h,
                      width: 100.w,
                      borderRadius: BorderRadius.circular(10),
                      hintTextStyle: black12Color,
                      hintText: AppStrings.brieflyDescribeYourCompany,
                      name: 'tag_line',
                      color: AppColors.lightBlue6Color,
                      validator: (String? value) {},
                      controller: controller.administratorController),
                  SizedBox(
                    height: 1.h,
                  )
                ],
              ),
            )
          ],
        ),
        ExpansionTile(
          title: Text(
            AppStrings.video,
            style: lightBlue500TextStyle,
          ),
          children: [
            ListTile(
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomTextFormField(
                      height: 7.h,
                      width: 100.w,
                      borderRadius: BorderRadius.circular(10),
                      hintTextStyle: black12Color,
                      hintText: AppStrings.aLinkToAVideoOf,
                      name: 'tag_line',
                      color: AppColors.lightBlue6Color,
                      validator: (String? value) {},
                      controller: controller.administratorController),
                  SizedBox(
                    height: 1.h,
                  )
                ],
              ),
            )
          ],
        ),
        ExpansionTile(
          title: Text(
            AppStrings.aboutUs,
            style: lightBlue500TextStyle,
          ),
          children: [
            ListTile(
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomTextFormField(
                      height: 20.h,
                      width: 100.w,
                      borderRadius: BorderRadius.circular(10),
                      hintTextStyle: black12Color,
                      hintText: AppStrings.brieflyDescribeWhatYourCompanyDoes,
                      name: 'tag_line',
                      color: AppColors.lightBlue6Color,
                      validator: (String? value) {},
                      controller: controller.administratorController),
                  SizedBox(
                    height: 1.h,
                  )
                ],
              ),
            )
          ],
        ),
        ExpansionTile(
          title: Text(
            AppStrings.companyLife,
            style: lightBlue500TextStyle,
          ),
          children: [
            ListTile(
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  DottedBorder(
                    dashPattern: const [6, 2],
                    radius: const Radius.circular(10),
                    color: AppColors.indigoColor,
                    strokeWidth: 1.5,
                    child: Container(
                      height: 8.h,
                      width: 20.w,
                      decoration: const BoxDecoration(
                        color: Colors.transparent,
                      ),
                      child: Stack(
                        clipBehavior: Clip.none,
                        children: [
                          Positioned(
                            top: 5,
                            left: 25,
                            child: Icon(
                              Icons.add,
                              color: AppColors.indigoColor,
                              size: 3.h,
                            ),
                          ),
                          const Positioned(
                            top: 30,
                            left: 23,
                            child: Text(
                              AppStrings.add,
                              style: indigo14ColorTextStyle,
                            ),
                          ),
                          Positioned(
                            bottom: -3,
                            right: -6.0,
                            child: SvgPicture.asset(
                              'assets/images/post_icon/edit_icon.svg',
                              height: 13.sp,
                              width: 13.sp,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                ],
              ),
            )
          ],
        ),
        ExpansionTile(
          title: Text(
            AppStrings.traveling,
            style: lightBlue500TextStyle,
          ),
          children: [
            GetBuilder<ListYourBusinessController>(
              builder: (_) {
                return ListTile(
                  subtitle: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          AppStrings.howManyKM,
                          style: blackSixHundredWeightColor,
                        ),
                        Row(
                          children: [
                            SliderTheme(
                              data: const SliderThemeData(
                                trackHeight: 4,
                              ),
                              child: Expanded(
                                flex: 1,
                                child: Slider(
                                    value:
                                        controller.travelingSlider.toDouble(),
                                    min: 1.0,
                                    max: 100.0,
                                    activeColor: AppColors.lightBlueColor,
                                    inactiveColor: Colors.blue.withOpacity(0.2),
                                    label: 'Set volume value',
                                    onChanged: (double newValue) => controller
                                        .travelingSliderFunc(newValue)),
                              ),
                            ),
                            Container(
                              height: 4.h,
                              width: 20.w,
                              decoration: BoxDecoration(
                                color: AppColors.lightBlue6Color,
                                borderRadius: BorderRadius.circular(5),
                              ),
                              child: Center(
                                child: Text(
                                  "${controller.travelingSlider.round().toString()} Km",
                                  style: blackSixHundredWeightColor,
                                ),
                              ),
                            )
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 3.w, right: 25.w),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                AppStrings.zeroKilometer,
                                style: blackSixHundredWeightColor,
                              ),
                              Text(
                                AppStrings.hundredKilometer,
                                style: blackSixHundredWeightColor,
                              )
                            ],
                          ),
                        )
                      ]),
                );
              },
            )
          ],
        ),
        ExpansionTile(
          title: Text(
            AppStrings.industry,
            style: lightBlue500TextStyle,
          ),
          children: [
            ListTile(
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomDropDown(
                    height: 7.h,
                    width: 90.w,
                    hint: AppStrings.category,
                    items: const <String>[
                      'Category',
                      'Category 1',
                      'Category 2',
                      'Category 3'
                    ],
                    selectedValue: (value) {
                      controller.category.value = value!;
                      log(value.toString());
                    },
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Center(
                    child: Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: 4.w, vertical: 2.h),
                        height: 40.h,
                        width: 95.w,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.grey.withOpacity(0.1),
                                blurRadius: 4.0,
                                spreadRadius: 2.0,
                              )
                            ]),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                AppStrings.selectIndustries,
                                style: lightBlue500TextStyle,
                              ),
                              const Divider(
                                color: AppColors.lightBlueColor,
                              ),
                              Expanded(
                                  child: Scrollbar(
                                thickness: 5,
                                radius: const Radius.circular(5),
                                thumbVisibility: false,
                                child: ListView(
                                  padding: EdgeInsets.zero,
                                  children: [
                                    InkWell(
                                      onTap: () => controller
                                          .changeCheckBoxSelectIndustries(
                                              controller.selectAll),
                                      child: Row(
                                        children: [
                                          GetBuilder<
                                              ListYourBusinessController>(
                                            builder: (_) {
                                              return Checkbox(
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            5)),
                                                side: const BorderSide(
                                                    color: AppColors
                                                        .lightBlueColor,
                                                    width: 0.5),
                                                checkColor: Colors.white,
                                                activeColor:
                                                    AppColors.lightBlueColor,
                                                value:
                                                    controller.selectAll.value,
                                                onChanged: (value) => controller
                                                    .changeCheckBoxSelectIndustries(
                                                        controller.selectAll),
                                              );
                                            },
                                          ),
                                          SizedBox(
                                            width: 3.w,
                                          ),
                                          Text(
                                            controller.selectAll.title,
                                            style: blackBold16Color,
                                          ),
                                        ],
                                      ),
                                    ),
                                    ...controller.checkBoxList.map(
                                      (item) => InkWell(
                                        onTap: () => controller
                                            .changeCheckBoxSelectItemIndustries(
                                                item),
                                        child: Row(
                                          children: [
                                            GetBuilder<
                                                ListYourBusinessController>(
                                              builder: (_) {
                                                return Checkbox(
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5)),
                                                  side: const BorderSide(
                                                      color: AppColors
                                                          .lightBlueColor,
                                                      width: 0.5),
                                                  checkColor: Colors.white,
                                                  activeColor:
                                                      AppColors.lightBlueColor,
                                                  value: item.value,
                                                  onChanged: (value) => controller
                                                      .changeCheckBoxSelectItemIndustries(
                                                          item),
                                                );
                                              },
                                            ),
                                            SizedBox(
                                              width: 3.w,
                                            ),
                                            Text(
                                              item.title,
                                              style: blackBold16Color,
                                            ),
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ))
                            ])),
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                ],
              ),
            ),
          ],
        ),
        ExpansionTile(
          title: Text(
            AppStrings.reviews,
            style: lightBlue500TextStyle,
          ),
          children: [
            ListTile(
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    AppStrings.youCanHaveTwoSources,
                    style: blackSixHundredWeightColor,
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                          child: CustomContainer(textName: AppStrings.ebay)),
                      SizedBox(
                        width: 2.w,
                      ),
                      Expanded(
                        child: CustomContainer(textName: AppStrings.facebook),
                      ),
                      SizedBox(
                        width: 2.w,
                      ),
                      Expanded(
                        child: CustomContainer(textName: AppStrings.google),
                      ),
                      SizedBox(
                        width: 2.w,
                      ),
                      Expanded(
                        child: CustomContainer(textName: AppStrings.other),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 1.h,
                  )
                ],
              ),
            ),
          ],
        ),
        ExpansionTile(
          title: Row(
            children: [
              Text(
                AppStrings.pastJob,
                style: lightBlue500TextStyle,
              ),
              SizedBox(
                width: 2.w,
              ),
              Text(
                AppStrings.optional,
                style: black8Color,
              ),
            ],
          ),
          children: [
            ListTile(
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomTextFormField(
                      height: 15.h,
                      width: 100.w,
                      borderRadius: BorderRadius.circular(10),
                      hintTextStyle: black12Color,
                      hintText: '',
                      name: 'tag_line',
                      color: AppColors.lightBlue6Color,
                      validator: (String? value) {},
                      controller: controller.administratorController),
                  SizedBox(
                    height: 1.h,
                  )
                ],
              ),
            )
          ],
        ),
        ExpansionTile(
          title: Text(
            AppStrings.manageYourLinkedSocialAccount,
            style: lightBlue500TextStyle,
          ),
          children: [
            ListTile(
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: SocialMediaWidget(
                          onChanged: (val) {},
                          hintText: "",
                          controller: controller.faceBookController,
                          imagePath:
                              'assets/images/post_icon/facebook_icon.svg',
                          socialMediaName: AppStrings.facebook,
                          color: AppColors.blue18Color,
                        ),
                      ),
                      SizedBox(
                        width: 4.w,
                      ),
                      Expanded(
                        child: SocialMediaWidget(
                          onChanged: (val) {},
                          hintText: "",
                          controller: controller.twitterController,
                          imagePath: 'assets/images/post_icon/twitter_icon.svg',
                          socialMediaName: AppStrings.twitter,
                          color: AppColors.blue18Color,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: SocialMediaWidget(
                          onChanged: (val) {},
                          hintText: "",
                          controller: controller.instagramController,
                          imagePath:
                              'assets/images/post_icon/instagram_icon.svg',
                          socialMediaName: AppStrings.instagram,
                          color: AppColors.grey4Color,
                        ),
                      ),
                      SizedBox(
                        width: 4.w,
                      ),
                      Expanded(
                        child: SocialMediaWidget(
                          hintText: "",
                          onChanged: (val) {},
                          controller: controller.linkedInController,
                          imagePath:
                              'assets/images/post_icon/linkedIn_icon.svg',
                          socialMediaName: AppStrings.linkedIn,
                          color: AppColors.blueColor,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Align(
                    alignment: Alignment.topCenter,
                    child: SocialMediaWidget(
                      onChanged: (val) {},
                      hintText: "",
                      controller: controller.googleController,
                      imagePath: 'assets/images/post_icon/google_plus_icon.svg',
                      socialMediaName: AppStrings.google,
                      color: AppColors.red2Color,
                    ),
                  ),
                  SizedBox(
                    height: 2.h,
                  )
                ],
              ),
            )
          ],
        ),
        SizedBox(
          height: 2.h,
        ),
        Padding(
          padding: EdgeInsets.only(left: 4.w),
          child: Column(
            children: [
              Row(
                children: [
                  Text(
                    AppStrings.yourAreOnThe,
                    style: greenColor15TextStyle,
                  ),
                  SizedBox(width: 1.w),
                  Text(
                    AppStrings.freePlan,
                    style: greenColor800TextStyle,
                  ),
                ],
              ),
              SizedBox(
                height: 2.h,
              ),
              FreePlanCard(),
            ],
          ),
        ),
        SizedBox(
          height: 1.h,
        ),
        ExpansionTile(
          title: Text(
            AppStrings.paymentMethod,
            style: lightBlue500TextStyle,
          ),
          children: [
            ListTile(
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  InkWell(
                    onTap: () {},
                    child: Row(
                      children: [
                        SvgPicture.asset(
                          'assets/images/post_icon/payment_add_icon.svg',
                          height: 5.h,
                          width: 5.w,
                        ),
                        SizedBox(
                          width: 4.w,
                        ),
                        Text(
                          AppStrings.addPaymentMethod,
                          style: blackSixHundredWeightColor,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 1.h,
                  )
                ],
              ),
            )
          ],
        ),
      ],
    );
  }

  Widget CustomContainer({required String textName}) {
    return Container(
      height: 4.h,
      width: 20.w,
      decoration: BoxDecoration(
        color: AppColors.lightBlue6Color,
        borderRadius: BorderRadius.circular(5),
      ),
      child: Center(
        child: Text(
          textName,
          style: blackSixHundredWeightColor,
        ),
      ),
    );
  }
}
