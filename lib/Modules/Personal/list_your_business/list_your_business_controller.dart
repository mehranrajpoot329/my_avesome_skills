import 'package:AwesomeSkills/Modules/Personal/ad_Screen/ad_screeb_can_help_personal/ad_screen_can_help_personal.dart';
import 'package:AwesomeSkills/constants/app_colors.dart';
import 'package:AwesomeSkills/constants/app_strings.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ListYourBusinessController extends GetxController {
  TextEditingController industryController = TextEditingController();
  TextEditingController administratorController = TextEditingController();
  TextEditingController faceBookController = TextEditingController();
  TextEditingController twitterController = TextEditingController();
  TextEditingController instagramController = TextEditingController();
  TextEditingController linkedInController = TextEditingController();
  TextEditingController googleController = TextEditingController();
  int travelingSlider = 3;
  RxString category = ''.obs;

  final selectAll = CheckBoxModal(
    title: 'title',
  );
  final checkBoxList = [
    CheckBoxModal(title: 'House Cleaning'),
    CheckBoxModal(title: 'End of Lease Cleaning'),
    CheckBoxModal(title: 'Carpet Steam Cleaning'),
    CheckBoxModal(title: 'Window Cleaning'),
    CheckBoxModal(title: 'Outdoor Cleaning'),
    CheckBoxModal(title: 'Office Cleaning'),
    CheckBoxModal(title: 'High Pressure Cleaning'),
    CheckBoxModal(title: 'Upholstery Cleaning'),
  ];

  var isSelected = AppStrings.personalAccount;
  var isSelectedBusiness = AppStrings.personalAccount;
  var isSelectedCasual = AppStrings.personalAccount;

  void travelingSliderFunc(double newValue) {
    travelingSlider = newValue.round();
    update();
  }

  changeCheckBoxSelectIndustries(CheckBoxModal checkBoxModal) {
    checkBoxModal.value = !checkBoxModal.value;
    update();
  }

  changeCheckBoxSelectItemIndustries(CheckBoxModal checkBoxModal) {
    checkBoxModal.value = !checkBoxModal.value;
    update();
  }

  selectedAccount(accountName) {
    isSelected = accountName;
    update();
  }

  switchHighlight(accountName) {
    if (accountName == isSelected) {
      return AppColors.blueColor;
    } else {
      return AppColors.redAccent2Color;
    }
  }

  switchStrings(accountName) {
    if (accountName == isSelected) {
      return AppStrings.selected;
    } else {
      return AppStrings.upgradeCapital;
    }
  }

  selectedCasualAccount(accountName) {
    isSelectedCasual = accountName;
    update();
  }

  switchCasualHighlight(accountName) {
    if (accountName == isSelectedCasual) {
      return AppColors.blueColor;
    } else {
      return AppColors.redAccent2Color;
    }
  }

  switchCasualCardColors(accountName) {
    if (accountName == isSelectedCasual) {
      return AppColors.lightGreenColor;
    } else {
      return Colors.white;
    }
  }

  selectedAccountBusiness(accountName) {
    isSelectedBusiness = accountName;
    update();
  }

  switchHighlightBusiness(accountName) {
    if (accountName == isSelectedBusiness) {
      return Colors.white;
    } else {
      return AppColors.redAccent2Color;
    }
  }

  switchBorderColorBusiness(accountName) {
    if (accountName == isSelectedBusiness) {
      return Colors.grey;
    } else {
      return AppColors.redAccent2Color;
    }
  }

  switchTextColorBusiness(accountName) {
    if (accountName == isSelectedBusiness) {
      return AppColors.redAccent2Color;
    } else {
      return Colors.white;
    }
  }

  switchBusinessStrings(accountName) {
    if (accountName == isSelectedBusiness) {
      return AppStrings.selected;
    } else {
      return AppStrings.upgradeCapital;
    }
  }
}
