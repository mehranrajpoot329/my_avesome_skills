import 'package:flip_card/flip_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Personal/list_your_business/list_your_business_controller.dart';
import 'package:AwesomeSkills/constants/app_colors.dart';
import 'package:AwesomeSkills/constants/app_fonts.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:sizer/sizer.dart';

class FreePlanCard extends StatelessWidget {
  final listYourBusinessController = Get.put(ListYourBusinessController());

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ListYourBusinessController>(
      builder: (_) {
        return Row(children: [
          Expanded(
            child: FlipCard(
              front: Container(
                padding: EdgeInsets.only(left: 2.w, right: 2.w),
                height: 30.h,
                width: 30.w,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                        color: AppColors.blueColor,
                        width: 2,
                        style: BorderStyle.solid)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 3.h,
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: Container(
                        height: 2.5.h,
                        width: 15.w,
                        decoration: BoxDecoration(
                          color: AppColors.blueColor,
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: const Center(
                          child: Text(
                            AppStrings.freeCapital,
                            style: white10TextStyle,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    const Align(
                      alignment: Alignment.center,
                      child: Text(
                        AppStrings.saver,
                        style: blueColorBoldTextStyle,
                      ),
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    buildTickWidget(
                        textName: AppStrings.freeToPost,
                        textStyle: blackFiveColor),
                    SizedBox(
                      height: 0.5.h,
                    ),
                    buildTickWidget(
                        textName: AppStrings.contactAnyRegistered,
                        textStyle: blackFiveColor),
                    SizedBox(
                      height: 0.5.h,
                    ),
                    buildTickWidget(
                        textName: AppStrings.canBrowseAll,
                        textStyle: blackFiveColor),
                    SizedBox(
                      height: 0.5.h,
                    ),
                    buildTickWidget(
                        textName: AppStrings.thisPlanHasNo,
                        textStyle: blackFiveColor),
                  ],
                ),
              ),
              back: Container(
                padding: EdgeInsets.only(left: 2.w, right: 2.w),
                height: 30.h,
                width: 30.w,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                        color: AppColors.blueColor,
                        width: 2,
                        style: BorderStyle.solid)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 3.h,
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: Container(
                        height: 2.5.h,
                        width: 15.w,
                        decoration: BoxDecoration(
                          color: AppColors.blueColor,
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: const Center(
                          child: Text(
                            AppStrings.freeCapital,
                            style: white10TextStyle,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    const Align(
                      alignment: Alignment.center,
                      child: Text(
                        AppStrings.saver,
                        style: blueColorBoldTextStyle,
                      ),
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    buildTickWidget(
                        textName: AppStrings.freeToPost,
                        textStyle: blackFiveColor),
                    SizedBox(
                      height: 0.5.h,
                    ),
                    buildTickWidget(
                        textName: AppStrings.contactAnyRegistered,
                        textStyle: blackFiveColor),
                    SizedBox(
                      height: 0.5.h,
                    ),
                    buildTickWidget(
                        textName: AppStrings.canBrowseAll,
                        textStyle: blackFiveColor),
                    SizedBox(
                      height: 0.5.h,
                    ),
                    buildTickWidget(
                        textName: AppStrings.thisPlanHasNo,
                        textStyle: blackFiveColor),
                  ],
                ),
              ),
            ),
          ),
          Expanded(
            child: FlipCard(
              front: Container(
                padding: EdgeInsets.only(left: 2.w, right: 2.w),
                height: 35.h,
                width: 30.w,
                decoration: BoxDecoration(
                    color: listYourBusinessController
                        .switchCasualCardColors(AppStrings.selected),
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                        color: AppColors.blueColor,
                        width: 2,
                        style: BorderStyle.solid)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 1.h,
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: Container(
                        height: 8.h,
                        width: 8.h,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.circle,
                          border: Border.all(
                              color: AppColors.redAccent2Color,
                              width: 2,
                              style: BorderStyle.solid),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text('\$', style: blackTwelveColor),
                                Text(
                                  AppStrings.nine,
                                  style: blackSixHundredWeightColor,
                                ),
                              ],
                            ),
                            const Text(
                              AppStrings.perMonth,
                              style: blackFiveColor,
                            ),
                          ],
                        ),
                      ),
                    ),
                    const Align(
                      alignment: Alignment.center,
                      child: Text(
                        AppStrings.casual,
                        style: blueColorBoldTextStyle,
                      ),
                    ),
                    SizedBox(
                      height: 0.5.h,
                    ),
                    buildTickWidget(
                        textName: AppStrings.yourAllWill,
                        textStyle: blackFiveColor),
                    SizedBox(
                      height: 0.5.h,
                    ),
                    buildTickWidget(
                        textName: AppStrings.contactAnyRegistered,
                        textStyle: blackFiveColor),
                    SizedBox(
                      height: 0.5.h,
                    ),
                    buildTickWidget(
                        textName: AppStrings.canBrowseAll,
                        textStyle: blackFiveColor),
                    SizedBox(
                      height: 0.5.h,
                    ),
                    buildTickWidget(
                        textName: AppStrings.receiveUnlimitedQuotes,
                        textStyle: blackFiveColor),
                    SizedBox(
                      height: 3.h,
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: CustomButton(
                          buttonName: AppStrings.selected,
                          width: 20.w,
                          height: 3.h,
                          buttonColor: listYourBusinessController
                              .switchCasualHighlight(AppStrings.selected),
                          textStyle: customButtonSixTextStyle,
                          borderRadius: BorderRadius.circular(15),
                          onTap: () {
                            listYourBusinessController
                                .selectedCasualAccount(AppStrings.selected);
                          }),
                    ),
                  ],
                ),
              ),
              back: Container(
                padding: EdgeInsets.only(left: 2.w, right: 2.w),
                height: 35.h,
                width: 30.w,
                decoration: BoxDecoration(
                    color: listYourBusinessController
                        .switchCasualCardColors(AppStrings.selected),
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                        color: AppColors.blueColor,
                        width: 2,
                        style: BorderStyle.solid)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 1.h,
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: Container(
                        height: 8.h,
                        width: 8.h,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.circle,
                          border: Border.all(
                              color: AppColors.redAccent2Color,
                              width: 2,
                              style: BorderStyle.solid),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text('\$', style: blackTwelveColor),
                                Text(
                                  AppStrings.nine,
                                  style: blackSixHundredWeightColor,
                                ),
                              ],
                            ),
                            const Text(
                              AppStrings.perMonth,
                              style: blackFiveColor,
                            ),
                          ],
                        ),
                      ),
                    ),
                    const Align(
                      alignment: Alignment.center,
                      child: Text(
                        AppStrings.casual,
                        style: blueColorBoldTextStyle,
                      ),
                    ),
                    SizedBox(
                      height: 0.5.h,
                    ),
                    buildTickWidget(
                        textName: AppStrings.yourAllWill,
                        textStyle: blackFiveColor),
                    SizedBox(
                      height: 0.5.h,
                    ),
                    buildTickWidget(
                        textName: AppStrings.contactAnyRegistered,
                        textStyle: blackFiveColor),
                    SizedBox(
                      height: 0.5.h,
                    ),
                    buildTickWidget(
                        textName: AppStrings.canBrowseAll,
                        textStyle: blackFiveColor),
                    SizedBox(
                      height: 0.5.h,
                    ),
                    buildTickWidget(
                        textName: AppStrings.receiveUnlimitedQuotes,
                        textStyle: blackFiveColor),
                    SizedBox(
                      height: 3.h,
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: CustomButton(
                          buttonName: AppStrings.selected,
                          width: 20.w,
                          height: 3.h,
                          buttonColor: listYourBusinessController
                              .switchCasualHighlight(AppStrings.selected),
                          textStyle: customButtonSixTextStyle,
                          borderRadius: BorderRadius.circular(15),
                          onTap: () {
                            listYourBusinessController
                                .selectedCasualAccount(AppStrings.selected);
                          }),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Expanded(
            child: FlipCard(
              front: Container(
                padding: EdgeInsets.only(left: 2.w, right: 2.w),
                height: 43.h,
                width: 30.w,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                        color: AppColors.blueColor,
                        width: 2,
                        style: BorderStyle.solid)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 1.h,
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: Container(
                        height: 8.h,
                        width: 8.h,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(
                              color: AppColors.redAccent2Color,
                              width: 2,
                              style: BorderStyle.solid),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text('\$', style: blackTwelveColor),
                                Text(
                                  AppStrings.nineteen,
                                  style: blackSixHundredWeightColor,
                                ),
                              ],
                            ),
                            const Text(
                              AppStrings.perMonth,
                              style: blackFiveColor,
                            ),
                          ],
                        ),
                      ),
                    ),
                    const Align(
                      alignment: Alignment.center,
                      child: Text(
                        AppStrings.trader,
                        style: blueColorBoldTextStyle,
                      ),
                    ),
                    SizedBox(
                      height: 0.5.h,
                    ),
                    buildTickWidget(
                        textName: AppStrings.freeToPost,
                        textStyle: blackFiveColor),
                    SizedBox(
                      height: 0.5.h,
                    ),
                    buildTickWidget(
                        textName: AppStrings.contactAnyRegistered,
                        textStyle: blackFiveColor),
                    SizedBox(
                      height: 0.5.h,
                    ),
                    buildTickWidget(
                        textName: AppStrings.canBrowseAll,
                        textStyle: blackFiveColor),
                    SizedBox(
                      height: 0.5.h,
                    ),
                    buildTickWidget(
                        textName: AppStrings.giveUnlimitedQuotes,
                        textStyle: blackFiveColor),
                    SizedBox(
                      height: 0.5.h,
                    ),
                    buildTickWidget(
                        textName: AppStrings.receiveUnlimitedQuotes,
                        textStyle: blackFiveColor),
                    SizedBox(
                      height: 0.5.h,
                    ),
                    buildTickWidget(
                        textName: AppStrings.freeToPostUpTo,
                        textStyle: blackFiveColor),
                    SizedBox(
                      height: 3.h,
                    ),
                    GetBuilder<ListYourBusinessController>(
                      builder: (_) {
                        return Align(
                          alignment: Alignment.center,
                          child: CustomButton(
                              buttonName: listYourBusinessController
                                  .switchStrings(AppStrings.upgradeCapital),
                              width: 23.w,
                              height: 3.h,
                              buttonColor: listYourBusinessController
                                  .switchHighlight(AppStrings.upgradeCapital),
                              textStyle: customButtonSixTextStyle,
                              borderRadius: BorderRadius.circular(15),
                              onTap: () {
                                listYourBusinessController
                                    .selectedAccount(AppStrings.upgradeCapital);
                              }),
                        );
                      },
                    ),
                  ],
                ),
              ),
              back: Container(
                padding: EdgeInsets.only(left: 2.w, right: 2.w),
                height: 43.h,
                width: 30.w,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                        color: AppColors.blueColor,
                        width: 2,
                        style: BorderStyle.solid)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 1.h,
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: Container(
                        height: 8.h,
                        width: 8.h,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(
                              color: AppColors.redAccent2Color,
                              width: 2,
                              style: BorderStyle.solid),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text('\$', style: blackTwelveColor),
                                Text(
                                  AppStrings.nineteen,
                                  style: blackSixHundredWeightColor,
                                ),
                              ],
                            ),
                            const Text(
                              AppStrings.perMonth,
                              style: blackFiveColor,
                            ),
                          ],
                        ),
                      ),
                    ),
                    const Align(
                      alignment: Alignment.center,
                      child: Text(
                        AppStrings.trader,
                        style: blueColorBoldTextStyle,
                      ),
                    ),
                    SizedBox(
                      height: 0.5.h,
                    ),
                    buildTickWidget(
                        textName: AppStrings.freeToPost,
                        textStyle: blackFiveColor),
                    SizedBox(
                      height: 0.5.h,
                    ),
                    buildTickWidget(
                        textName: AppStrings.contactAnyRegistered,
                        textStyle: blackFiveColor),
                    SizedBox(
                      height: 0.5.h,
                    ),
                    buildTickWidget(
                        textName: AppStrings.canBrowseAll,
                        textStyle: blackFiveColor),
                    SizedBox(
                      height: 0.5.h,
                    ),
                    buildTickWidget(
                        textName: AppStrings.giveUnlimitedQuotes,
                        textStyle: blackFiveColor),
                    SizedBox(
                      height: 0.5.h,
                    ),
                    buildTickWidget(
                        textName: AppStrings.receiveUnlimitedQuotes,
                        textStyle: blackFiveColor),
                    SizedBox(
                      height: 0.5.h,
                    ),
                    buildTickWidget(
                        textName: AppStrings.freeToPostUpTo,
                        textStyle: blackFiveColor),
                    SizedBox(
                      height: 3.h,
                    ),
                    GetBuilder<ListYourBusinessController>(
                      builder: (_) {
                        return Align(
                          alignment: Alignment.center,
                          child: CustomButton(
                              buttonName: listYourBusinessController
                                  .switchStrings(AppStrings.upgradeCapital),
                              width: 23.w,
                              height: 3.h,
                              buttonColor: listYourBusinessController
                                  .switchHighlight(AppStrings.upgradeCapital),
                              textStyle: customButtonSixTextStyle,
                              borderRadius: BorderRadius.circular(15),
                              onTap: () {
                                listYourBusinessController
                                    .selectedAccount(AppStrings.upgradeCapital);
                              }),
                        );
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: FlipCard(
              front: Container(
                  height: 47.h,
                  width: 30.w,
                  decoration: BoxDecoration(
                    color: AppColors.blueColor,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          height: 3.h,
                          width: 31.w,
                          decoration: const BoxDecoration(
                            color: AppColors.redAccent2Color,
                            borderRadius: BorderRadius.only(
                              topRight: Radius.circular(10),
                              topLeft: Radius.circular(10),
                            ),
                          ),
                          child: Center(
                            child: Text(
                              AppStrings.recommended,
                              style: white14Color,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 1.h,
                        ),
                        Align(
                          alignment: Alignment.center,
                          child: Container(
                            height: 8.h,
                            width: 8.h,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              shape: BoxShape.circle,
                              border: Border.all(
                                  color: AppColors.redAccent2Color,
                                  width: 2,
                                  style: BorderStyle.solid),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text('\$', style: blackTwelveColor),
                                    Text(
                                      AppStrings.twentyNine,
                                      style: blackSixHundredWeightColor,
                                    ),
                                  ],
                                ),
                                const Text(
                                  AppStrings.perMonth,
                                  style: blackFiveColor,
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 1.h,
                        ),
                        const Align(
                          alignment: Alignment.center,
                          child: Text(
                            AppStrings.business,
                            style: whiteFourteenColorTextStyle,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 2.w, right: 2.w),
                          child: Column(
                            children: [
                              buildTickWidget(
                                  textName: AppStrings.freeToPost,
                                  textStyle: whiteFiveColor),
                              SizedBox(
                                height: 0.5.h,
                              ),
                              buildTickWidget(
                                  textName: AppStrings.contactAnyRegistered,
                                  textStyle: whiteFiveColor),
                              SizedBox(
                                height: 0.5.h,
                              ),
                              buildTickWidget(
                                  textName: AppStrings.canBrowseAll,
                                  textStyle: whiteFiveColor),
                              SizedBox(
                                height: 0.5.h,
                              ),
                              buildTickWidget(
                                  textName: AppStrings.giveUnlimitedQuotes,
                                  textStyle: whiteFiveColor),
                              SizedBox(
                                height: 0.5.h,
                              ),
                              buildTickWidget(
                                  textName: AppStrings.receiveUnlimitedQuotes,
                                  textStyle: whiteFiveColor),
                              SizedBox(
                                height: 0.5.h,
                              ),
                              buildTickWidget(
                                  textName: AppStrings.freeToPostUpTo,
                                  textStyle: whiteFiveColor),
                              SizedBox(
                                height: 0.5.h,
                              ),
                              buildTickWidget(
                                  textName: AppStrings.fullBusinessProfilePage,
                                  textStyle: whiteFiveColor),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 2.h,
                        ),
                        Align(
                          alignment: Alignment.center,
                          child: GetBuilder<ListYourBusinessController>(
                            builder: (_) {
                              return Align(
                                alignment: Alignment.center,
                                child: GestureDetector(
                                  onTap: () => listYourBusinessController
                                      .selectedAccountBusiness(
                                          AppStrings.upgradeCapital),
                                  child: Container(
                                    width: 20.w,
                                    height: 3.h,
                                    decoration: BoxDecoration(
                                      color: listYourBusinessController
                                          .switchHighlightBusiness(
                                              AppStrings.upgradeCapital),
                                      borderRadius: BorderRadius.circular(15),
                                      border: Border.all(
                                          color: listYourBusinessController
                                              .switchBorderColorBusiness(
                                                  AppStrings.upgradeCapital),
                                          width: 2,
                                          style: BorderStyle.solid),
                                    ),
                                    child: Center(
                                      child: Text(
                                        listYourBusinessController
                                            .switchBusinessStrings(
                                                AppStrings.upgradeCapital),
                                        style: TextStyle(
                                          fontFamily: AppFonts.poppins,
                                          fontSize: 6.sp,
                                          color: listYourBusinessController
                                              .switchTextColorBusiness(
                                                  AppStrings.upgradeCapital),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      ])),
              back: Container(
                  height: 47.h,
                  width: 30.w,
                  decoration: BoxDecoration(
                    color: AppColors.blueColor,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          height: 3.h,
                          width: 31.w,
                          decoration: const BoxDecoration(
                            color: AppColors.redAccent2Color,
                            borderRadius: BorderRadius.only(
                              topRight: Radius.circular(10),
                              topLeft: Radius.circular(10),
                            ),
                          ),
                          child: Center(
                            child: Text(
                              AppStrings.recommended,
                              style: white14Color,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 1.h,
                        ),
                        Align(
                          alignment: Alignment.center,
                          child: Container(
                            height: 8.h,
                            width: 8.h,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              shape: BoxShape.circle,
                              border: Border.all(
                                  color: AppColors.redAccent2Color,
                                  width: 2,
                                  style: BorderStyle.solid),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text('\$', style: blackTwelveColor),
                                    Text(
                                      AppStrings.twentyNine,
                                      style: blackSixHundredWeightColor,
                                    ),
                                  ],
                                ),
                                const Text(
                                  AppStrings.perMonth,
                                  style: blackFiveColor,
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 1.h,
                        ),
                        const Align(
                          alignment: Alignment.center,
                          child: Text(
                            AppStrings.business,
                            style: whiteFourteenColorTextStyle,
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 2.w, right: 2.w),
                          child: Column(
                            children: [
                              buildTickWidget(
                                  textName: AppStrings.freeToPost,
                                  textStyle: whiteFiveColor),
                              SizedBox(
                                height: 0.5.h,
                              ),
                              buildTickWidget(
                                  textName: AppStrings.contactAnyRegistered,
                                  textStyle: whiteFiveColor),
                              SizedBox(
                                height: 0.5.h,
                              ),
                              buildTickWidget(
                                  textName: AppStrings.canBrowseAll,
                                  textStyle: whiteFiveColor),
                              SizedBox(
                                height: 0.5.h,
                              ),
                              buildTickWidget(
                                  textName: AppStrings.giveUnlimitedQuotes,
                                  textStyle: whiteFiveColor),
                              SizedBox(
                                height: 0.5.h,
                              ),
                              buildTickWidget(
                                  textName: AppStrings.receiveUnlimitedQuotes,
                                  textStyle: whiteFiveColor),
                              SizedBox(
                                height: 0.5.h,
                              ),
                              buildTickWidget(
                                  textName: AppStrings.freeToPostUpTo,
                                  textStyle: whiteFiveColor),
                              SizedBox(
                                height: 0.5.h,
                              ),
                              buildTickWidget(
                                  textName: AppStrings.fullBusinessProfilePage,
                                  textStyle: whiteFiveColor),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 2.h,
                        ),
                        Align(
                          alignment: Alignment.center,
                          child: GetBuilder<ListYourBusinessController>(
                            builder: (_) {
                              return Align(
                                alignment: Alignment.center,
                                child: GestureDetector(
                                  onTap: () => listYourBusinessController
                                      .selectedAccountBusiness(
                                          AppStrings.upgradeCapital),
                                  child: Container(
                                    width: 20.w,
                                    height: 3.h,
                                    decoration: BoxDecoration(
                                      color: listYourBusinessController
                                          .switchHighlightBusiness(
                                              AppStrings.upgradeCapital),
                                      borderRadius: BorderRadius.circular(15),
                                      border: Border.all(
                                          color: listYourBusinessController
                                              .switchBorderColorBusiness(
                                                  AppStrings.upgradeCapital),
                                          width: 2,
                                          style: BorderStyle.solid),
                                    ),
                                    child: Center(
                                      child: Text(
                                        listYourBusinessController
                                            .switchBusinessStrings(
                                                AppStrings.upgradeCapital),
                                        style: TextStyle(
                                          fontFamily: AppFonts.poppins,
                                          fontSize: 6.sp,
                                          color: listYourBusinessController
                                              .switchTextColorBusiness(
                                                  AppStrings.upgradeCapital),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      ])),
            ),
          )
        ]);
      },
    );
  }

  Widget buildTickWidget(
      {required String textName, required TextStyle textStyle}) {
    return Row(
      children: [
        SvgPicture.asset(
          'assets/images/post_icon/check_icon.svg',
          height: 5.sp,
          width: 5.sp,
        ),
        SizedBox(
          width: 2.w,
        ),
        Expanded(
          child: Text(
            textName,
            style: textStyle,
          ),
        )
      ],
    );
  }
}
