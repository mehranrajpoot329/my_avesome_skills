import 'package:AwesomeSkills/Modules/Personal/list_your_business/list_your_business_expansion_title.dart';
import 'package:AwesomeSkills/constants/app_colors.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/custom_app_bar_controller.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';

import '../../../Constants/app_strings.dart';
import '../../../Constants/text_style.dart';

class ListYourBusiness extends StatelessWidget {
  final customAppBar = Get.put(CustomAppBarController());

  @override
  Widget build(BuildContext context) {
    return NestedScrollWidget(
      floatingActionButton: WidgetMethod.floatingActionButton(context),
      body: Padding(
          padding: EdgeInsets.only(left: 3.w, right: 2.w),
          child: ListView(children: [
            Container(
              padding: EdgeInsets.only(left: 4.w, right: 2.w, top: 1.h),
              height: 10.h,
              decoration: BoxDecoration(
                color: AppColors.lightGreen2Color,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    AppStrings.createBusinessAccount,
                    style: orange600ColorTextStyle,
                  ),
                ],
              ),
            ),
            ListYourBusinessExpansionTile(),
            SizedBox(
              height: 2.h,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CustomButton(
                    buttonName: AppStrings.saveForLater,
                    width: 30.w,
                    height: 5.h,
                    buttonColor: AppColors.lightBlue2Color,
                    textStyle: black14Color,
                    borderRadius: BorderRadius.circular(6),
                    onTap: () {}),
                SizedBox(
                  width: 3.w,
                ),
                CustomButton(
                    buttonName: AppStrings.submit,
                    width: 30.w,
                    height: 5.h,
                    buttonColor: AppColors.green5Color,
                    textStyle: black14Color,
                    borderRadius: BorderRadius.circular(6),
                    onTap: () {}),
              ],
            ),
            SizedBox(
              height: 2.h,
            )
          ])),
    );
  }
}
