import 'package:AwesomeSkills/Modules/Personal/landing_screen/landing_screen_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';

import '../../../Constants/app_colors.dart';
import '../../../Constants/app_strings.dart';
import '../../../Constants/text_style.dart';
import '../../../routes/app_pages.dart';
import '../../../widgets/button/custom_button.dart';
import '../../../widgets/custom_scroll_appBar/custom_app_bar_controller.dart';
import '../../../widgets/custom_textfield/custom_textfield.dart';
import 'login_controller.dart';

class LoginForm extends StatelessWidget {
  final controller = Get.put(LoginController());
  final customAppBarController = Get.put(CustomAppBarController());
  final landingScreenController = Get.put(LandingScreenController());

  Widget buildTextWidget(String name) {
    return Text(
      name,
      style: labelTextStyle,
    );
  }

  @override
  Widget build(BuildContext context) {
    return FormBuilder(
        key: controller.formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 25.0),
            buildTextWidget(AppStrings.emailAddress),
            const SizedBox(height: 5.0),
            CustomTextFormField(
                height: 7.h,
                width: 100.w,
                borderRadius: BorderRadius.circular(6),
                hintTextStyle: labelTextStyle,
                name: 'email',
                validator: (String? value) => (value?.isEmpty ?? true)
                    ? 'Email is Required'
                    // : (controller.emailRegExp.hasMatch(value!)
                    // ?
                    : null,
                color: AppColors.lightBlue6Color,
                controller: controller.emailController,
                hintText: AppStrings.emailTextAddress),
            const SizedBox(height: 25.0),
            buildTextWidget(AppStrings.password),
            const SizedBox(height: 5.0),
            CustomTextFormFieldSuffix(
                onTap: () => controller.showPassword(),
                obscureText: controller.isVisiblePassword,
                height: 7.h,
                width: 100.w,
                borderRadius: BorderRadius.circular(6),
                hintTextStyle: labelTextStyle,
                name: 'password',
                validator: (String? value) => (value?.isEmpty ?? true)
                    ? 'password is Required'
                    : (controller.passwordController.text.length >= 8
                        ? null
                        : 'Enter your password up to 8 character'),
                color: AppColors.lightBlue6Color,
                controller: controller.passwordController,
                hintText: AppStrings.textPassword),
            const SizedBox(height: 5),
            const Align(
              alignment: Alignment.bottomRight,
              child: Text(
                AppStrings.forgotPassword,
                style: blueTextStyle,
              ),
            ),
            const SizedBox(height: 20),
            CustomButton(
              height: 54,
              width: MediaQuery.of(context).size.width - 40,
              borderRadius: BorderRadius.circular(8),
              buttonName: AppStrings.logIn,
              textStyle: buttonBlueTextStyle,
              buttonColor: AppColors.blueColor,
              onTap: () => controller.login(),
              // {
              //   if (customAppBarController.isLogined = true) {
              //     landingScreenController.isShowLogined();
              //     Get.toNamed(AppPage.landingScreen);
              //
              //   }
            ),
            const SizedBox(height: 15.0),
            const Align(
              alignment: Alignment.center,
              child: Text(AppStrings.orLoginWith, style: blue16TextStyle),
            ),
            const SizedBox(height: 25.0),
            CustomGFAButton(
              textName: AppStrings.google,
              imgPath: 'assets/images/post_icon/google.png',
            ),
            const SizedBox(height: 15.0),
            CustomGFAButton(
              textName: AppStrings.facebook,
              imgPath: 'assets/images/post_icon/facebook.png',
            ),
            const SizedBox(height: 15.0),
            GestureDetector(
              onTap: () => Get.toNamed(AppPage.myMessage),
              child: CustomGFAButton(
                textName: AppStrings.apple,
                imgPath: 'assets/images/post_icon/apple.png',
              ),
            ),
            const SizedBox(
              height: 50,
            )
          ],
        ));
  }
}

class CustomGFAButton extends StatelessWidget {
  String textName;
  String imgPath;

  CustomGFAButton({Key? key, required this.textName, required this.imgPath})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 48,
      width: MediaQuery.of(context).size.width - 20,
      decoration: BoxDecoration(
        border: Border.all(
            color: AppColors.gFAButtonColor,
            width: 1,
            style: BorderStyle.solid),
        borderRadius: BorderRadius.circular(8.0),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(imgPath),
          const SizedBox(width: 10),
          Text(
            textName,
            style: gFATextStyle,
          )
        ],
      ),
    );
  }
}
