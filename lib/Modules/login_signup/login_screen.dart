import 'package:AwesomeSkills/Modules/login_signup/login_form.dart';
import 'package:AwesomeSkills/local/db/local_storage.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/custom_app_bar_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/landing_screen_controller.dart';
import 'package:AwesomeSkills/routes/app_pages.dart';

class LoginScreen extends StatelessWidget {
  final landingScreenController = Get.put(LandingScreenController());
  final customAppBarController = Get.put(CustomAppBarController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 30),
              Align(
                alignment: Alignment.bottomRight,
                child: TextButton(
                  onPressed: () {
                    HiveHelper.isLoggedOut();
                    HiveHelper.saveUserLogout(isLoggedOut: false);
                    Get.toNamed(AppPage.landingScreen);
                  },
                  child: const Text(
                    AppStrings.skip,
                    style: customLightBlue3TextStyle,
                  ),
                ),
              ),
              const Align(
                alignment: Alignment.center,
                child: Text(AppStrings.myAwesomeSkill,
                    style: projectNameTextStyle),
              ),
              const Align(
                alignment: Alignment.center,
                child: Text(AppStrings.createFreeAccount,
                    style: createAccountTextStyle),
              ),
              const SizedBox(height: 10),
              LoginForm(),
            ],
          ),
        ),
      ),
    );
  }
}
