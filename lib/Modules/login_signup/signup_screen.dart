import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/routes/app_pages.dart';

import 'package:AwesomeSkills/widgets/button/personal_bussiness_button.dart';

import 'signup_form.dart';

class SignUpScreen extends StatelessWidget {
  const SignUpScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 30),
              Align(
                alignment: Alignment.bottomRight,
                child: TextButton(
                  onPressed: () {
                    Get.toNamed(AppPage.landingScreen);
                  },
                  child: Text(
                    AppStrings.skip,
                    style: customLightBlue3TextStyle,
                  ),
                ),
              ),
              const Align(
                alignment: Alignment.center,
                child: Text(AppStrings.myAwesomeSkill,
                    style: projectNameTextStyle),
              ),
              const Align(
                alignment: Alignment.center,
                child: Text(AppStrings.createFreeAccount,
                    style: createAccountTextStyle),
              ),
              const SizedBox(height: 20),
              const SizedBox(height: 10),
              SignUpForm(),
            ],
          ),
        ),
      ),
    );
  }
}
