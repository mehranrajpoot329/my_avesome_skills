import 'dart:math';

import 'package:AwesomeSkills/firebase/firebase.dart';
import 'package:AwesomeSkills/local/db/local_storage.dart';
import 'package:AwesomeSkills/models/user_model.dart';
import 'package:AwesomeSkills/network/base_response.dart';
import 'package:AwesomeSkills/network/end_points.dart';
import 'package:AwesomeSkills/network/requests.dart';
import 'package:AwesomeSkills/routes/app_pages.dart';
import 'package:AwesomeSkills/utils/utils.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart' hide FormData, MultipartFile;
import '../../../constants/app_colors.dart';
import '../../../constants/app_strings.dart';

class SignUpController extends GetxController {
  final pageViewController = PageController(initialPage: 0);

  @override
  void dispose() {
    pageViewController.dispose();
    super.dispose();
  }

  signUpTextFieldClear() {
    emailController.clear();
    fullNameController.clear();
    passwordController.clear();
    confirmController.clear();
  }

  signupBusinessTextFieldClear() {
    businessController.clear();
    emailBusinessController.clear();
    fullNameBusinessController.clear();
    passwordBusinessController.clear();
    confirmBusinessController.clear();
  }

  RxBool isChecked = false.obs;
  final personalFormKey = GlobalKey<FormBuilderState>();
  final businessFormKey = GlobalKey<FormBuilderState>();
  bool valueFirst = false;
  String selectedItem = AppStrings.personalAccount;
  RxBool isVisiblePassword = true.obs;
  bool isVisibleConfirmPassword = true;
  TextEditingController fullNameController = TextEditingController();
  TextEditingController fullNameBusinessController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController emailBusinessController = TextEditingController();
  TextEditingController businessController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController passwordBusinessController = TextEditingController();
  TextEditingController confirmController = TextEditingController();
  TextEditingController confirmBusinessController = TextEditingController();
  RxBool isBusinessAccount = false.obs;

  final RegExp nameRegExp = RegExp('[a-zA-Z]');
  final RegExp emailRegExp =
      RegExp("r'^.+@[a-zA-Z]+\.{1}[a-zA-Z]+(\.{0,1}[a-zA-Z]+)\$");

  checkBoxTerm(bool value) {
    valueFirst = value;
    update();
  }

  isBusinessAccountSelection() {
    isBusinessAccount.value = !isBusinessAccount.value;
  }

  showPassword() {
    isVisiblePassword.value = !isVisiblePassword.value;
  }

  selectedAccount(accountName) {
    selectedItem = accountName;
    update();
  }

  switchHighlight(accountName) {
    if (accountName == selectedItem) {
      return AppColors.buttonBlueColor;
    } else {
      return AppColors.buttonGreyColor;
    }
  }

  switchHighlightColor(accountName) {
    if (accountName == selectedItem) {
      return Colors.white;
    } else {
      return AppColors.blueColor;
    }
  }

  Color colorPassword1() {
    if (passwordController.text.isEmpty) {
      return AppColors.greyColor;
    } else if (passwordController.text.length >= 7) {
      return AppColors.greenColor;
    } else {
      return AppColors.redAccent2Color;
    }
  }

  Color colorPassword2() {
    if (passwordController.text.length <= 3) {
      return AppColors.greyColor;
    } else if (passwordController.text.length >= 7) {
      return AppColors.greenColor;
    } else {
      return AppColors.redAccent2Color;
    }
  }

  Color colorPassword3() {
    if (passwordController.text.length <= 6) {
      return AppColors.greyColor;
    } else if (passwordController.text.length >= 7) {
      return AppColors.greenColor;
    } else {
      return AppColors.redAccent2Color;
    }
  }

  Color colorPassword4() {
    if (passwordController.text.length >= 8) {
      return AppColors.greenColor;
    } else {
      return AppColors.greyColor;
    }
  }

  String textName() {
    if (passwordController.text.length <= 5) {
      return 'Weak';
    } else {
      return 'Strong';
    }
  }

  Color textColor() {
    if (passwordController.text.length <= 5) {
      return AppColors.redAccentColor;
    } else {
      return AppColors.greenColor;
    }
  }

  Future<void> signUpPersonal() async {
    if (personalFormKey.currentState!.saveAndValidate()) {
      var formData = ({
        'type': 'personal',
        'first_name': fullNameController.text.toString(),
        'email': emailController.text.toString(),
        'password': passwordController.text.toString(),
        'confirm_password': confirmController.text.toString(),
      });
      BaseResponse response = await DioClient().postRequest(
        endPoint: EndPoints.signUpUrl,
        body: formData,
      );
      if (response.error == false) {
        User user = User.fromJson(response.data);
        Utils.successFlutterToast(msg: "User Created");
        await HiveHelper.saveUser(user);
        await HiveHelper.saveIsNew(isNew: false);
        await HiveHelper.saveLoggIn(isLoggedIn: true);
        Get.toNamed(AppPage.landingScreen);
        signUpTextFieldClear();
      } else {
        BaseResponse baseResponse = BaseResponse.fromJson(response.data);
        Utils.errorFlutterToast(msg: baseResponse.message.toString());
      }
    }
  }

  Future<void> signUpBusiness() async {
    try {
      if (businessFormKey.currentState!.saveAndValidate()) {
        if (isChecked.isFalse) {
          Get.showSnackbar(
            Utils.errorSnackBar(message: 'Accept Terms & Conditions'),
          );
        } else {
          var formData = ({
            'type': AppStrings.businessAccount,
            'first_name': fullNameBusinessController.text.toString(),
            'email': emailBusinessController.text.toString(),
            'password': passwordBusinessController.text.toString(),
            'confirm_password': confirmBusinessController.text.toString(),
          });
          BaseResponse response = await DioClient().postRequest(
            endPoint: EndPoints.signUpUrl,
            body: formData,
          );
          if (response.error == false) {
            User user = User.fromJson(response.data);
            signupBusinessTextFieldClear();
            Utils.successSnackBar(message: "User Created");
            await HiveHelper.saveUser(user);
            await HiveHelper.saveIsNew(isNew: false);
            await HiveHelper.saveLoggIn(isLoggedIn: true);
            Get.toNamed(AppPage.landingScreenBusiness);
          } else {
            Utils.errorFlutterToast(msg: response.message.toString());
          }
        }
      }
    } catch (e) {
      Get.showSnackbar(
        Utils.errorSnackBar(message: e.toString()),
      );
    }
  }
}
