import 'package:AwesomeSkills/firebase/firebase.dart';

import 'package:AwesomeSkills/Modules/Personal/landing_screen/landing_screen_controller.dart';
import 'package:AwesomeSkills/local/db/local_storage.dart';
import 'package:AwesomeSkills/models/user_model.dart';
import 'package:AwesomeSkills/network/base_response.dart';
import 'package:AwesomeSkills/network/end_points.dart';
import 'package:AwesomeSkills/network/requests.dart';
import 'package:AwesomeSkills/routes/app_pages.dart';
import 'package:AwesomeSkills/utils/utils.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/custom_app_bar_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart' hide FormData, Response;

class LoginController extends GetxController {
  final formKey = GlobalKey<FormBuilderState>();
  RxBool isVisiblePassword = true.obs;
  final customController = CustomAppBarController();

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  final landingScreenController = Get.put(LandingScreenController());

  showPassword() {
    isVisiblePassword.value = !isVisiblePassword.value;
  }

  loginTextFieldClear() {
    emailController.clear();
    passwordController.clear();
  }

  Future<void> login() async {
    if (formKey.currentState!.saveAndValidate()) {
      await FirebaseAuthorization.firebaseAuth.signInWithEmailAndPassword(
          email: emailController.text.toString(),
          password: passwordController.text.toString());

      var formData = {
        'email': emailController.text.toString(),
        'password': passwordController.text.toString(),
      };

      BaseResponse response = await DioClient().postRequest(
        endPoint: EndPoints.signInUrl,
        body: formData,
      );
      if (response.error == false) {
        print("------ ${response.data} --------- ");
        User user = User.fromJson(response.data);
        await HiveHelper.saveUser(user);
        await HiveHelper.saveIsNew(isNew: false);
        await HiveHelper.setAuthToken(user.token!);
        await HiveHelper.saveUserLogin(isLoggedIn: true);
        print(user.token);
        customController.moveToNext();
        Get.back();
        Get.toNamed(AppPage.landingScreen);
        loginTextFieldClear();
      } else {
        Utils.errorFlutterToast(msg: response.message.toString());
      }
    }
  }
}
