import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Modules/Business/custom_scroll_appBar_business/custom_app_bar_business_controller.dart';
import 'package:AwesomeSkills/Modules/Business/landing_screen_business/landing_screen__business_controller.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/landing_screen_controller.dart';
import 'package:AwesomeSkills/Modules/login_signup/signup_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:get/get.dart';
import 'package:sizer/sizer.dart';

import '../../../Constants/app_strings.dart';
import '../../../Constants/text_style.dart';
import '../../../routes/app_pages.dart';
import '../../../widgets/button/custom_button.dart';
import '../../../widgets/button/personal_bussiness_button.dart';
import '../../../widgets/check_box.dart';
import '../../../widgets/custom_scroll_appBar/custom_app_bar_controller.dart';
import '../../../widgets/custom_textfield/custom_textfield.dart';

class SignUpForm extends StatelessWidget {
  final controller = Get.put(SignUpController());
  final customAppBarController = Get.put(CustomAppBarController());
  final customAppBarBusinessController =
      Get.put(CustomAppBarBusinessController());
  final landingScreenController = Get.put(LandingScreenController());
  final businessLandingScreenController =
      Get.put(LandingScreenBusinessController());

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: Get.height,
      child: PageView(
        physics: NeverScrollableScrollPhysics(),
        controller: controller.pageViewController,
        children: [
          personalForm(context),
          businessForm(context),
        ],
      ),
    );
  }

  passwordWeakStatus() {
    return GetBuilder<SignUpController>(
      builder: (_) {
        return Row(
          children: [
            reUseContainer(controller.colorPassword1()),
            const SizedBox(width: 3),
            reUseContainer(controller.colorPassword2()),
            const SizedBox(width: 3),
            reUseContainer(controller.colorPassword3()),
            const SizedBox(width: 3),
            reUseContainer(controller.colorPassword4()),
            const SizedBox(
              width: 5,
            ),
            Text(
              controller.textName(),
              style: TextStyle(
                  color: controller.textColor(),
                  fontSize: 14,
                  fontFamily: 'poppins',
                  fontWeight: FontWeight.w500),
            ),
          ],
        );
      },
    );
  }

  reUseContainer(Color color) {
    return Container(
      height: 8,
      width: 55,
      decoration: BoxDecoration(
        color: color,
      ),
    );
  }

  Widget _buildTextWidget(String name) {
    return Text(
      name,
      style: labelTextStyle,
    );
  }

  Widget personalForm(BuildContext context) {
    return FormBuilder(
      key: controller.personalFormKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          PersonalBusinessButton(),
          SizedBox(
            height: 2.h,
          ),
          _buildTextWidget(AppStrings.fullName),
          SizedBox(height: 0.5.h),
          CustomTextFormField(
              height: 6.h,
              width: 100.w,
              borderRadius: BorderRadius.circular(6),
              hintTextStyle: labelTextStyle,
              name: 'first_name',
              validator: (String? value) => (value?.isEmpty ?? true)
                  ? 'Full name is Required'
                  : (controller.nameRegExp.hasMatch(value!)
                      ? null
                      : 'Enter a Valid Name'),
              color: AppColors.lightBlue6Color,
              controller: controller.fullNameController,
              hintText: AppStrings.nameText),
          SizedBox(
            height: 2.h,
          ),
          _buildTextWidget(AppStrings.email),
          SizedBox(height: 0.5.h),
          CustomTextFormField(
              height: 6.h,
              width: 100.w,
              borderRadius: BorderRadius.circular(6),
              hintTextStyle: labelTextStyle,
              name: 'email',
              validator: (String? value) => (value?.isEmpty ?? true)
                  ? 'Email is Required'
                  // : (controller.emailRegExp.hasMatch(value!)
                  // ?
                  : null,
              // : 'Enter Valid Email'),
              color: AppColors.lightBlue6Color,
              controller: controller.emailController,
              hintText: AppStrings.emailTextAddress),
          SizedBox(
            height: 2.h,
          ),
          _buildTextWidget(AppStrings.password),
          SizedBox(height: 0.5.h),
          CustomTextFormFieldSuffix(
              height: 6.h,
              width: 100.w,
              obscureText: controller.isVisiblePassword,
              onTap: () => controller.showPassword(),
              borderRadius: BorderRadius.circular(6),
              hintTextStyle: labelTextStyle,
              name: 'password',
              validator: (String? value) => (value?.isEmpty ?? true)
                  ? 'password is Required'
                  : (controller.passwordController.text.length >= 8
                      ? null
                      : 'Enter your password up to 8 character'),
              color: AppColors.lightBlue6Color,
              controller: controller.passwordController,
              hintText: AppStrings.textPassword),
          SizedBox(height: 1.h),
          passwordWeakStatus(),
          SizedBox(height: 1.h),
          const Text(
            AppStrings.moreCharacterUse,
            style: customButtonBlueTextStyle,
          ),
          const SizedBox(height: 4),
          const Text(
            AppStrings.numberAndSymbol,
            style: customButtonBlueTextStyle,
          ),
          SizedBox(height: 2.h),
          _buildTextWidget(AppStrings.confirmPassword),
          SizedBox(height: 0.5.h),
          CustomTextFormFieldSuffix(
            height: 6.h,
            width: 100.w,
            borderRadius: BorderRadius.circular(6),
            hintTextStyle: labelTextStyle,
            name: 'confirm_password',
            validator: (String? value) => (value?.isEmpty ?? true)
                ? 'Confirm Password is Required'
                : (value == controller.passwordController.text
                    ? null
                    : 'Password do not match'),
            color: AppColors.lightBlue6Color,
            controller: controller.confirmController,
            hintText: AppStrings.textPassword,
            obscureText: controller.isVisiblePassword,
            onTap: () => controller.showPassword(),
          ),
          SizedBox(height: 1.h),
          Obx(
            () => LabeledCheckbox(
              contentPadding: EdgeInsets.zero,
              labelTextStyle: const TextStyle(
                fontFamily: 'poppins',
                fontSize: 10,
              ),
              activeColor: AppColors.black,
              label: AppStrings.acceptTermCondition,
              onTap: (value) {
                controller.isChecked.value =
                    value ?? !controller.isChecked.value;
              },
              value: controller.isChecked.value,
              gap: 0,
            ),
          ),
          SizedBox(height: 3.h),
          CustomButton(
            height: 54,
            width: MediaQuery.of(context).size.width - 40,
            borderRadius: BorderRadius.circular(8),
            buttonName: AppStrings.signUp,
            textStyle: buttonBlueTextStyle,
            buttonColor: AppColors.blueColor,
            onTap: () => controller.signUpPersonal(),
          ),
          SizedBox(
            height: 2.h,
          )
        ],
      ),
    );
  }

  Widget businessForm(BuildContext context) {
    return FormBuilder(
      key: controller.businessFormKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          PersonalBusinessButton(),
          SizedBox(
            height: 2.h,
          ),
          _buildTextWidget(AppStrings.fullName),
          SizedBox(height: 0.5.h),
          CustomTextFormField(
              height: 6.h,
              width: 100.w,
              borderRadius: BorderRadius.circular(6),
              hintTextStyle: labelTextStyle,
              name: 'full_name',
              validator: (String? value) => (value?.isEmpty ?? true)
                  ? 'Full name is Required'
                  : (controller.nameRegExp.hasMatch(value!)
                      ? null
                      : 'Enter a Valid Name'),
              color: AppColors.lightBlue6Color,
              controller: controller.fullNameBusinessController,
              hintText: AppStrings.nameText),
          SizedBox(
            height: 2.h,
          ),
          _buildTextWidget(AppStrings.email),
          SizedBox(height: 0.5.h),
          CustomTextFormField(
              height: 6.h,
              width: 100.w,
              borderRadius: BorderRadius.circular(6),
              hintTextStyle: labelTextStyle,
              name: 'email',
              validator: (String? value) => (value?.isEmpty ?? true)
                  ? 'Email is Required'
                  // : (controller.emailRegExp.hasMatch(value!)
                  // ?
                  : null,
              // : 'Enter Valid Email'),
              color: AppColors.lightBlue6Color,
              controller: controller.emailBusinessController,
              hintText: AppStrings.emailTextAddress),
          SizedBox(
            height: 2.h,
          ),
          _buildTextWidget(AppStrings.businessUrl),
          SizedBox(height: 0.5.h),
          CustomTextFormField(
              height: 6.h,
              width: 100.w,
              borderRadius: BorderRadius.circular(6),
              hintTextStyle: labelTextStyle,
              name: 'business_url',
              validator: (String? value) =>
                  (value?.isEmpty ?? true) ? 'Business URL is Required' : null,
              color: AppColors.lightBlue6Color,
              controller: controller.businessController,
              hintText: AppStrings.businessHintText),
          SizedBox(height: 2.h),
          _buildTextWidget(AppStrings.password),
          SizedBox(height: 0.5.h),
          CustomTextFormField(
              height: 6.h,
              width: 100.w,
              borderRadius: BorderRadius.circular(6),
              hintTextStyle: labelTextStyle,
              name: 'password',
              validator: (String? value) => (value?.isEmpty ?? true)
                  ? 'password is Required'
                  : (controller.passwordBusinessController.text.length >= 8
                      ? null
                      : 'Enter your password up to 8 character'),
              color: AppColors.lightBlue6Color,
              controller: controller.passwordBusinessController,
              hintText: AppStrings.textPassword),
          SizedBox(height: 1.h),
          passwordWeakStatus(),
          SizedBox(height: 1.h),
          const Text(
            AppStrings.moreCharacterUse,
            style: customButtonBlueTextStyle,
          ),
          const SizedBox(height: 4),
          const Text(
            AppStrings.numberAndSymbol,
            style: customButtonBlueTextStyle,
          ),
          SizedBox(height: 2.h),
          _buildTextWidget(AppStrings.confirmPassword),
          SizedBox(height: 0.5.h),
          CustomTextFormField(
              height: 6.h,
              width: 100.w,
              borderRadius: BorderRadius.circular(6),
              hintTextStyle: labelTextStyle,
              name: 'confirm_password',
              validator: (String? value) => (value?.isEmpty ?? true)
                  ? 'Confirm Password is Required'
                  : (value == controller.passwordBusinessController.text
                      ? null
                      : 'Password do not match'),
              color: AppColors.lightBlue6Color,
              controller: controller.confirmBusinessController,
              hintText: AppStrings.textPassword),
          SizedBox(height: 1.h),
          Obx(
            () => LabeledCheckbox(
              contentPadding: EdgeInsets.zero,
              activeColor: AppColors.black,
              labelTextStyle: const TextStyle(
                fontFamily: 'poppins',
                fontSize: 12,
              ),
              label: AppStrings.acceptTermCondition,
              onTap: (value) {
                controller.isChecked.value =
                    value ?? !controller.isChecked.value;
              },
              value: controller.isChecked.value,
              gap: 0,
            ),
          ),
          SizedBox(height: 3.h),
          CustomButton(
            height: 54,
            width: MediaQuery.of(context).size.width - 40,
            borderRadius: BorderRadius.circular(8),
            buttonName: AppStrings.signUp,
            textStyle: buttonBlueTextStyle,
            buttonColor: AppColors.blueColor,
            onTap: () => controller.signUpBusiness(),
          ),
          SizedBox(
            height: 2.h,
          )
        ],
      ),
    );
  }
}
