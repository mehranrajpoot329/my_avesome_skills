import 'package:flutter/material.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Modules/Business/drawers_screens_business/favourites_business/favourite_card_business.dart';

class CardScreenListBusiness extends StatelessWidget {
  const CardScreenListBusiness({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemCount: 4,
        itemBuilder: (context, int index) {
          return FavouriteCardBusiness(
            image: 'assets/images/profile_alex.png',
            personName: AppStrings.tomLatham,
            field: AppStrings.webDesigner,
            detail: AppStrings.easyToWork,
            date: AppStrings.twoMarch2022,
          );
        });
  }
}
