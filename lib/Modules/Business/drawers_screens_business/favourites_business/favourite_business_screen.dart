import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Business/custom_scroll_appBar_business/nested_business_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_bottom_appBar_navi/custom_bottom_appBar_navi.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:sizer/sizer.dart';

import '../../../Personal/drawers_screens/favourites/card_screen_list.dart';

class FavouriteScreenBusiness extends StatelessWidget {
  const FavouriteScreenBusiness({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NestedBusinessScroll(
        floatingActionButton: WidgetMethod.floatingActionButton(context),
        body: Padding(
            padding: EdgeInsets.only(left: 2.w, right: 2.w),
            child: SingleChildScrollView(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 1.h,
                    ),
                    Row(
                      children: [
                        GestureDetector(
                          onTap: () => Get.back(),
                          child: Icon(
                            Icons.arrow_back_ios_new,
                            color: Colors.black,
                            size: 3.h,
                          ),
                        ),
                        SizedBox(
                          width: 5.w,
                        ),
                        const Text(
                          AppStrings.favourite,
                          style: montserrat20TextStyle,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    FittedBox(
                      child: Text(
                        AppStrings.helloAgainAstria,
                        style: poppins36ColorTextStyle,
                      ),
                    ),
                    SizedBox(
                      height: 0.2.h,
                    ),
                    FittedBox(
                      child: Row(
                        children: [
                          Text(
                            AppStrings.youHave,
                            style: blackBold16Color,
                          ),
                          const Text(
                            AppStrings.five,
                            style: greenBold16Color,
                          ),
                          SizedBox(
                            width: 1.w,
                          ),
                          Text(
                            AppStrings.savedFavourite,
                            style: blackBold16Color,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    const Text(
                      AppStrings.favourite,
                      style: blue500ColorTextStyle,
                    ),
                    const CardScreenList()
                  ]),
            )));
  }
}
