import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:AwesomeSkills/widgets/custom_bottom_appBar_navi/custom_bottom_appBar_navi.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:sizer/sizer.dart';

import '../../../custom_scroll_appBar_business/nested_business_scroll_widget.dart';

class AboutUsBusiness extends StatelessWidget {
  const AboutUsBusiness({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NestedBusinessScroll(
      floatingActionButton: WidgetMethod.floatingActionButton(context),
      body: Padding(
          padding: EdgeInsets.only(left: 3.w, right: 2.w),
          child: ListView(padding: EdgeInsets.zero, children: [
            Row(
              children: [
                GestureDetector(
                  onTap: () => Get.back(),
                  child: Icon(
                    Icons.arrow_back_ios_new,
                    color: Colors.black,
                    size: 3.h,
                  ),
                ),
                SizedBox(
                  width: 5.w,
                ),
                const Text(
                  AppStrings.settings,
                  style: montserrat20TextStyle,
                ),
              ],
            ),
            SizedBox(
              height: 2.h,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 4.w, vertical: 0.5.h),
              height: 14.5.h,
              width: 100.w,
              decoration: BoxDecoration(
                color: AppColors.lightGreen2Color,
                borderRadius: BorderRadius.circular(15.0),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    AppStrings.inSeasonPTYLTD,
                    style: orangeColor600TextStyle,
                  ),
                  Text(
                    AppStrings.joinedIn2007,
                    style: blackBold16Color,
                  ),
                  Text(
                    AppStrings.adminFlorinBelbe,
                    style: blackBold16Color,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            Text(
              AppStrings.aboutUs,
              style: lightBlue32ColorTextStyle,
            ),
            SizedBox(
              height: 2.h,
            ),
            TextFormField(
              maxLength: 500,
              maxLines: 10,
              decoration: InputDecoration(
                  filled: true,
                  fillColor: AppColors.lightBlue6Color,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  )),
            ),
            SizedBox(
              height: 2.h,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                CustomButton(
                    buttonName: AppStrings.save,
                    height: 5.h,
                    width: 20.w,
                    buttonColor: AppColors.lightBlueColor,
                    textStyle: blackSixHundredWeightColor,
                    borderRadius: BorderRadius.circular(3),
                    onTap: () {}),
                SizedBox(
                  width: 15.w,
                  child: Row(
                    children: [
                      SvgPicture.asset(
                        'assets/images/post_icon/edit_icon.svg',
                        height: 3.h,
                        fit: BoxFit.fitHeight,
                      ),
                      Text(
                        AppStrings.edit,
                        style: indigo14ColorTextStyle,
                      )
                    ],
                  ),
                )
              ],
            ),
          ])),
    );
  }
}
