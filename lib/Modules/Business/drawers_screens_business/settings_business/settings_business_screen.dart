import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/business_settings_card.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:sizer/sizer.dart';
import '../../../../routes/app_pages.dart';
import '../../custom_scroll_appBar_business/nested_business_scroll_widget.dart';

class SettingScreenBusiness extends StatelessWidget {
  const SettingScreenBusiness({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NestedBusinessScroll(
      floatingActionButton: WidgetMethod.floatingActionButton(context),
      body: Padding(
          padding: EdgeInsets.only(left: 2.w, right: 2.w),
          child: SingleChildScrollView(
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              SizedBox(
                height: 2.h,
              ),
              Row(
                children: [
                  GestureDetector(
                    onTap: () => Get.back(),
                    child: Icon(
                      Icons.arrow_back_ios_new,
                      color: Colors.black,
                      size: 3.h,
                    ),
                  ),
                  SizedBox(
                    width: 5.w,
                  ),
                  const Text(
                    AppStrings.settings,
                    style: montserrat20TextStyle,
                  ),
                ],
              ),
              SizedBox(
                height: 2.h,
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 4.w, vertical: 0.5.h),
                height: 14.5.h,
                width: 100.w,
                decoration: BoxDecoration(
                  color: AppColors.lightGreen2Color,
                  borderRadius: BorderRadius.circular(15.0),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text(
                      AppStrings.inSeasonPTYLTD,
                      style: orangeColor600TextStyle,
                    ),
                    Text(
                      AppStrings.joinedIn2007,
                      style: blackBold16Color,
                    ),
                    Text(
                      AppStrings.adminFlorinBelbe,
                      style: blackBold16Color,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: buildCompanyDetailsWidget(
                        name: AppStrings.ceo,
                        imageUrl: 'assets/images/ceo_profile_pics.png'),
                  ),
                  SizedBox(
                    width: 2.w,
                  ),
                  Expanded(
                    child: buildCompanyDetailsWidget(
                        name: AppStrings.stateManager,
                        imageUrl: 'assets/images/profile_state_manager.png'),
                  ),
                  SizedBox(
                    width: 2.w,
                  ),
                  Expanded(
                      child: buildCompanyDetailsWidget(
                          name: AppStrings.customerHelp,
                          imageUrl: 'assets/images/profile_customer_help.png')),
                ],
              ),
              SizedBox(
                height: 2.h,
              ),
              Center(
                child: DottedBorder(
                  dashPattern: const [6, 2],
                  radius: const Radius.circular(10),
                  color: AppColors.indigoColor,
                  strokeWidth: 1.5,
                  child: Container(
                    height: 10.h,
                    width: 20.w,
                    decoration: const BoxDecoration(
                      color: Colors.transparent,
                    ),
                    child: Stack(
                      clipBehavior: Clip.none,
                      children: [
                        Positioned(
                          top: 1.h,
                          left: 5.w,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.add,
                                color: AppColors.indigoColor,
                                size: 3.h,
                              ),
                              SizedBox(
                                height: 2.h,
                              ),
                              Text(
                                AppStrings.add,
                                style: indigo14ColorTextStyle,
                              ),
                            ],
                          ),
                        ),
                        Positioned(
                          bottom: -3,
                          right: -6.0,
                          child: SvgPicture.asset(
                            'assets/images/post_icon/edit_icon.svg',
                            height: 13.sp,
                            width: 13.sp,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 2.w),
                child: Container(
                  padding: EdgeInsets.only(left: 2.w, right: 2.w, top: 0.3.h),
                  height: 13.h,
                  width: 35.w,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(15),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.grey.withOpacity(0.2),
                          spreadRadius: 4.0,
                          blurRadius: 6.0)
                    ],
                    border: Border.all(
                        color: AppColors.indigoColor,
                        width: 1,
                        style: BorderStyle.solid),
                  ),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Container(
                            height: 7.h,
                            width: 7.w,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.transparent,
                                border: Border.all(
                                    color: AppColors.indigoColor,
                                    width: 1,
                                    style: BorderStyle.solid)),
                            child: Center(
                              child: Icon(
                                Icons.person,
                                color: AppColors.indigoColor,
                                size: 15.sp,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 2.w,
                          ),
                          Text(
                            AppStrings.twenty,
                            style: indigoColor20TextStyle,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 1.h,
                      ),
                      FittedBox(
                        child: Text(
                          AppStrings.profileCompletion,
                          style: blackSixHundredWeightColor,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 2.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: BusinessSettingsCompanyDetailsCard(
                        onTap: () {
                          Get.toNamed(AppPage.companyDetailsSettingBusiness);
                        },
                        isComplete: true,
                        imagePath: 'assets/images/post_icon/company_life.png',
                        textName: AppStrings.companyDetails,
                      ),
                    ),
                    SizedBox(
                      width: 4.w,
                    ),
                    Expanded(
                      child: BusinessSettingsCard(
                        onTap: () {
                          Get.toNamed(AppPage.aboutUsBusiness);
                        },
                        isComplete: false,
                        imagePath: 'assets/images/post_icon/group_person.svg',
                        textName: AppStrings.aboutUs,
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 2.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: BusinessSettingsCard(
                        onTap: () {
                          Get.toNamed(AppPage.serviceBusiness);
                        },
                        isComplete: true,
                        imagePath: 'assets/images/post_icon/service_icon.svg',
                        textName: AppStrings.services,
                      ),
                    ),
                    SizedBox(
                      width: 4.w,
                    ),
                    Expanded(
                      child: BusinessSettingsCard(
                        onTap: () {
                          Get.toNamed(AppPage.companyLifeBusiness);
                        },
                        isComplete: false,
                        imagePath:
                            'assets/images/post_icon/company_life_icon.svg',
                        textName: AppStrings.companyLife,
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 2.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: BusinessSettingsCard(
                        onTap: () {
                          Get.toNamed(AppPage.alertNotificationBusiness);
                        },
                        isComplete: true,
                        imagePath: 'assets/images/post_icon/alert_icon.svg',
                        textName: AppStrings.alertNotifications,
                      ),
                    ),
                    SizedBox(
                      width: 7.w,
                    ),
                    Expanded(
                      child: BusinessSettingsCard(
                        onTap: () {
                          Get.toNamed(AppPage.securityPasswordSettingBusiness);
                        },
                        isComplete: true,
                        imagePath: 'assets/images/post_icon/lock_icon.svg',
                        textName: AppStrings.security,
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 2.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: BusinessSettingsCompanyDetailsCard(
                        onTap: () {
                          Get.toNamed(AppPage.receiveLeadingSettingBusiness);
                        },
                        isComplete: false,
                        imagePath:
                            'assets/images/post_icon/receiving_leads.png',
                        textName: AppStrings.receivingLeads,
                      ),
                    ),
                    SizedBox(
                      width: 4.w,
                    ),
                    Expanded(
                        child: SocialMediaBusinessCard(
                      isComplete: true,
                      onTap: () {
                        Get.toNamed(AppPage.socialMediaSettingBusiness);
                      },
                    ))
                  ],
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 2.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: FreeMyPlanBusiness(
                        onTap: () {
                          Get.toNamed(AppPage.freePlanSettingsBusiness);
                        },
                      ),
                    ),
                    SizedBox(
                      width: 4.w,
                    ),
                    Expanded(child: PaymentMethodCardBusiness(
                      onTap: () {
                        Get.toNamed(AppPage.paymentMethodSettingsBusiness);
                      },
                    ))
                  ],
                ),
              ),
              SizedBox(
                height: 3.h,
              ),
            ]),
          )),
    );
  }

  Widget buildCompanyDetailsWidget(
      {required String name, required String imageUrl}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          height: 15.h,
          width: 30.w,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            image:
                DecorationImage(image: AssetImage(imageUrl), fit: BoxFit.cover),
          ),
        ),
        SizedBox(
          height: 1.h,
        ),
        FittedBox(
          child: Text(
            name,
            style: blackBold16Color,
          ),
        )
      ],
    );
  }
}
