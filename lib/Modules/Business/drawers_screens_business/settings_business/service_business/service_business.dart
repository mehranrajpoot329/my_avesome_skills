import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:AwesomeSkills/widgets/button/custom_icon_button.dart';
import 'package:AwesomeSkills/widgets/custom_bottom_appBar_navi/custom_bottom_appBar_navi.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:sizer/sizer.dart';

import '../../../custom_scroll_appBar_business/nested_business_scroll_widget.dart';

class ServiceBusiness extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return NestedBusinessScroll(
      floatingActionButton: WidgetMethod.floatingActionButton(context),
      body: Padding(
          padding: const EdgeInsets.only(left: 10.0, right: 10),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    GestureDetector(
                      onTap: () => Get.back(),
                      child: Icon(
                        Icons.arrow_back_ios_new,
                        color: Colors.black,
                        size: 3.h,
                      ),
                    ),
                    SizedBox(
                      width: 5.w,
                    ),
                    const Text(
                      AppStrings.settings,
                      style: montserrat20TextStyle,
                    ),
                  ],
                ),
                SizedBox(
                  height: 2.h,
                ),
                Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: 4.w, vertical: 0.5.h),
                  height: 14.5.h,
                  width: 100.w,
                  decoration: BoxDecoration(
                    color: AppColors.lightGreen2Color,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        AppStrings.inSeasonPTYLTD,
                        style: orangeColor600TextStyle,
                      ),
                      Text(
                        AppStrings.joinedIn2007,
                        style: blackBold16Color,
                      ),
                      Text(
                        AppStrings.adminFlorinBelbe,
                        style: blackBold16Color,
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 2.h,
                ),
                const Text(
                  AppStrings.services,
                  style: blue600ColorTextStyle,
                ),
                SizedBox(
                  height: 0.5.h,
                ),
                Row(
                  children: [
                    const Text(
                      AppStrings.writeDownYour,
                      style: black18Color,
                    ),
                    SizedBox(
                      width: 1.5.w,
                    ),
                    Text(
                      AppStrings.services,
                      style: indigoColor20TextStyle,
                    ),
                  ],
                ),
                const Text(
                  AppStrings.thatYouIntend,
                  style: black18Color,
                ),
                SizedBox(
                  height: 2.h,
                ),
                SizedBox(
                  height: 8.h,
                  child: TextFormField(
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                    )),
                  ),
                ),
                SizedBox(
                  height: 2.h,
                ),
                CustomIconButton(
                    buttonName: AppStrings.addMore,
                    width: 30.w,
                    height: 6.h,
                    buttonColor: AppColors.indigoColor,
                    textStyle: customButtonTextStyle,
                    borderRadius: BorderRadius.circular(10),
                    onTap: () {}),
                SizedBox(
                  height: 4.h,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CustomButton(
                      height: 5.h,
                      width: 20.w,
                      buttonName: AppStrings.save,
                      borderRadius: BorderRadius.circular(3),
                      buttonColor: AppColors.lightBlueColor,
                      textStyle: blackSixHundredWeightColor,
                      onTap: () {},
                    ),
                    SizedBox(
                      width: 15.w,
                      child: Row(
                        children: [
                          SvgPicture.asset(
                            'assets/images/post_icon/edit_icon.svg',
                            height: 3.h,
                            fit: BoxFit.fitHeight,
                          ),
                          const Text(
                            AppStrings.edit,
                            style: indigoColorTextStyle,
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ],
            ),
          )),
    );
  }
}
