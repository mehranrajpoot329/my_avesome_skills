import 'package:get/get.dart';

class AlertNotificationBusinessController extends GetxController {
  bool needHelp = false;
  bool canHelp = false;
  bool quotes = false;
  bool forBusiness = false;
  bool emailCheckBox = false;
  bool smsCheckBox = false;
  bool emailSwitch = false;
  bool smsSwitch = false;

  changeCheckBoxEmail(bool? value) {
    emailCheckBox = value!;
    update();
  }

  changeCheckBoxSms(bool? value) {
    smsCheckBox = value!;
    update();
  }

  changeSwitchNeedHelp(bool value) {
    needHelp = value;
    update();
  }

  changeSwitchCanNeedWith(bool value) {
    canHelp = value;
    update();
  }

  changeSwitchQuotes(bool value) {
    quotes = value;
    update();
  }

  changeSwitchForBusiness(bool value) {
    forBusiness = value;
    update();
  }

  changeSwitchEmail(bool value) {
    emailSwitch = value;
    update();
  }

  changeSwitchSMS(bool value) {
    smsSwitch = value;
    update();
  }
}
