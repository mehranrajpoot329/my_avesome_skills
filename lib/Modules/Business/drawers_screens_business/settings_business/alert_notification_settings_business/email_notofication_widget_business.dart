import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Business/drawers_screens_business/settings_business/alert_notification_settings_business/alert_notification_business_controller.dart';
import 'package:sizer/sizer.dart';

class EmailNotificationWidgetBusiness extends StatelessWidget {
  String textName, textName2, imagePath;
  bool switchValue, checkBoxValue;
  Function onChanged, onChangedCheckBox;

  EmailNotificationWidgetBusiness({
    required this.imagePath,
    required this.textName,
    required this.textName2,
    required this.switchValue,
    required this.checkBoxValue,
    required this.onChangedCheckBox,
    required this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 5.w, right: 5.w, top: 3.h),
      height: 19.h,
      width: 90.w,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
                offset: Offset(0, 3),
                blurRadius: 1.0,
                spreadRadius: 1.0,
                color: Colors.grey..withOpacity(0.1))
          ]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.only(left: 5.w),
            child: SvgPicture.asset(
              imagePath,
            ),
          ),
          SizedBox(
            height: 1.h,
          ),
          SizedBox(
            height: 2.7.h,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 5.w),
                  child: Text(
                    textName,
                    style: blackBold16Color,
                  ),
                ),
                Transform.scale(
                  scale: 0.8,
                  child: CupertinoSwitch(
                      trackColor: Colors.grey,
                      activeColor: AppColors.indigoColor,
                      value: switchValue,
                      onChanged: (newValue) => onChanged(newValue)),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 2.w),
            child: Row(
              children: [
                GetBuilder<AlertNotificationBusinessController>(builder: (_) {
                  return Checkbox(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      side: const BorderSide(
                          color: AppColors.indigoColor, width: 1),
                      checkColor: Colors.white,
                      activeColor: AppColors.indigoColor,
                      value: checkBoxValue,
                      onChanged: (bool? value) => onChangedCheckBox(value));
                }),
                Text(
                  textName2,
                  style: blue12ColorTextStyle,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
