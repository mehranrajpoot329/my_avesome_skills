import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Business/drawers_screens_business/settings_business/alert_notification_settings_business/alert_notification_business_controller.dart';
import 'package:AwesomeSkills/Modules/Business/drawers_screens_business/settings_business/alert_notification_settings_business/email_notofication_widget_business.dart';
import 'package:AwesomeSkills/Modules/Business/drawers_screens_business/settings_business/alert_notification_settings_business/extra_notication_widget_business.dart';
import 'package:AwesomeSkills/Modules/Business/drawers_screens_business/settings_business/alert_notification_settings_business/job_alert_widget_business.dart';
import 'package:sizer/sizer.dart';

class NeedHelpWithTabBarBusiness extends StatelessWidget {
  final alertNotificationBusinessController =
      Get.put(AlertNotificationBusinessController());

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        left: 2.w,
        right: 2.w,
      ),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 1.h,
            ),
            const FittedBox(
              child: Text(
                AppStrings.emailNotificationSettings,
                style: blue22ColorTextStyle,
              ),
            ),
            FittedBox(
              child: Text(
                AppStrings.howWouldYouLike,
                style: black14Color,
              ),
            ),
            SizedBox(
              height: 1.h,
            ),
            Center(
              child:
                  GetBuilder<AlertNotificationBusinessController>(builder: (_) {
                return EmailNotificationWidgetBusiness(
                    checkBoxValue:
                        alertNotificationBusinessController.emailCheckBox,
                    onChangedCheckBox: (value) =>
                        alertNotificationBusinessController
                            .changeCheckBoxEmail(value),
                    switchValue:
                        alertNotificationBusinessController.emailSwitch,
                    onChanged: (newValue) => alertNotificationBusinessController
                        .changeSwitchEmail(newValue),
                    imagePath: 'assets/images/post_icon/email_icon.svg',
                    textName: AppStrings.emailAddress,
                    textName2: AppStrings.emailName);
              }),
            ),
            SizedBox(
              height: 4.h,
            ),
            Center(
              child:
                  GetBuilder<AlertNotificationBusinessController>(builder: (_) {
                return EmailNotificationWidgetBusiness(
                    checkBoxValue:
                        alertNotificationBusinessController.smsCheckBox,
                    onChangedCheckBox: (value) =>
                        alertNotificationBusinessController
                            .changeCheckBoxSms(value),
                    switchValue: alertNotificationBusinessController.smsSwitch,
                    onChanged: (newValue) => alertNotificationBusinessController
                        .changeSwitchSMS(newValue),
                    imagePath: 'assets/images/post_icon/mobile_icon2.svg',
                    textName: AppStrings.sms,
                    textName2: AppStrings.phoneNumber);
              }),
            ),
            SizedBox(
              height: 2.h,
            ),
            const Text(
              AppStrings.jobAlert,
              style: blue22ColorTextStyle,
            ),
            FittedBox(
              child: Text(
                AppStrings.getNotified,
                style: black14Color,
              ),
            ),
            SizedBox(
              height: 1.h,
            ),
            Center(
              child: JobAlertWidgetBusiness(
                isIcon: true,
                textName: AppStrings.addKeywords,
              ),
            ),
            SizedBox(
              height: 3.h,
            ),
            Center(
              child: JobAlertWidgetBusiness(
                isIcon: false,
                textName: AppStrings.chooseKMSRadius,
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            const Text(
              AppStrings.extraNotification,
              style: blue22ColorTextStyle,
            ),
            FittedBox(
              child: Text(
                AppStrings.chooseExtraNotifications,
                style: black14Color,
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            GetBuilder<AlertNotificationBusinessController>(builder: (_) {
              return ExtraNotificationBusiness(
                  switchValue: alertNotificationBusinessController.needHelp,
                  onChanged: (newValue) => alertNotificationBusinessController
                      .changeSwitchNeedHelp(newValue),
                  textName: AppStrings.needHelp,
                  color: AppColors.blue17Color);
            }),
            SizedBox(
              height: 2.h,
            ),
            GetBuilder<AlertNotificationBusinessController>(builder: (_) {
              return ExtraNotificationBusiness(
                  switchValue: alertNotificationBusinessController.canHelp,
                  onChanged: (newValue) => alertNotificationBusinessController
                      .changeSwitchCanNeedWith(newValue),
                  textName: AppStrings.canHelpWith,
                  color: AppColors.yellow5Color);
            }),
            SizedBox(
              height: 2.h,
            ),
            GetBuilder<AlertNotificationBusinessController>(builder: (_) {
              return ExtraNotificationBusiness(
                  switchValue: alertNotificationBusinessController.quotes,
                  onChanged: (newValue) => alertNotificationBusinessController
                      .changeSwitchQuotes(newValue),
                  textName: AppStrings.quotesPlusReview,
                  color: AppColors.yellow5Color);
            }),
            SizedBox(
              height: 2.h,
            ),
            GetBuilder<AlertNotificationBusinessController>(builder: (_) {
              return ExtraNotificationBusiness(
                  switchValue: alertNotificationBusinessController.forBusiness,
                  onChanged: (newValue) => alertNotificationBusinessController
                      .changeSwitchForBusiness(newValue),
                  textName: AppStrings.forBusiness,
                  color: AppColors.yellow5Color);
            }),
            SizedBox(
              height: 3.h,
            )
          ],
        ),
      ),
    );
  }
}
