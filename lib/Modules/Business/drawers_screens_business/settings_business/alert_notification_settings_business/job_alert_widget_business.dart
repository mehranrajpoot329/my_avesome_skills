import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';

class JobAlertWidgetBusiness extends StatelessWidget {
  bool isIcon;
  String textName;

  JobAlertWidgetBusiness({
    required this.isIcon,
    required this.textName,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
      height: 60,
      width: MediaQuery.of(context).size.width - 20,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(15),
          border: Border.all(
              color: Colors.grey, width: 0.5, style: BorderStyle.solid),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 2.0,
              blurRadius: 4.0,
            )
          ]),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              isIcon
                  ? SvgPicture.asset(
                      'assets/images/post_icon/speaker.svg',
                      height: 20,
                    )
                  : const SizedBox(),
              const SizedBox(
                width: 10,
              ),
              Text(
                textName,
                style: blackBold16Color,
              )
            ],
          )
        ],
      ),
    );
  }
}
