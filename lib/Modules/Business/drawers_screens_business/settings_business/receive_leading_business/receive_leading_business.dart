import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:AwesomeSkills/widgets/drop_down.dart';
import 'package:sizer/sizer.dart';

import '../../../custom_scroll_appBar_business/nested_business_scroll_widget.dart';
import 'receive_leading_business_controller.dart';

class ReceiveLeadingBusiness extends StatelessWidget {
  final controller = Get.put(ReceiveLeadingBusinessController());

  @override
  Widget build(BuildContext context) {
    return NestedBusinessScroll(
        floatingActionButton: WidgetMethod.floatingActionButton(context),
        body: Padding(
          padding: EdgeInsets.only(left: 3.w, right: 2.w),
          child: ListView(padding: EdgeInsets.zero, children: [
            Row(
              children: [
                GestureDetector(
                  onTap: () => Get.back(),
                  child: Icon(
                    Icons.arrow_back_ios_new,
                    color: Colors.black,
                    size: 3.h,
                  ),
                ),
                SizedBox(
                  width: 5.w,
                ),
                const Text(
                  AppStrings.settings,
                  style: montserrat20TextStyle,
                ),
              ],
            ),
            SizedBox(
              height: 2.h,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 4.w, vertical: 0.5.h),
              height: 14.5.h,
              width: 100.w,
              decoration: BoxDecoration(
                color: AppColors.lightGreen2Color,
                borderRadius: BorderRadius.circular(15.0),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    AppStrings.inSeasonPTYLTD,
                    style: orangeColor600TextStyle,
                  ),
                  Text(
                    AppStrings.joinedIn2007,
                    style: blackBold16Color,
                  ),
                  Text(
                    AppStrings.adminFlorinBelbe,
                    style: blackBold16Color,
                  ),
                ],
              ),
            ),
            SizedBox(height: 1.h),
            const Text(
              AppStrings.industry,
              style: blue500ColorTextStyle,
            ),
            CustomDropDown(
              height: 7.h,
              width: 100.w,
              hint: AppStrings.category,
              items: const <String>[
                'UI/UX',
                'Design',
                'Website  ',
                'App',
              ],
              selectedValue: (value) {
                controller.category.value = value!;
                log(value.toString());
              },
            ),
            SizedBox(height: 2.h),
            Center(
              child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 4.w, vertical: 2.h),
                  height: 40.h,
                  width: 95.w,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                          offset: Offset(0, 5),
                          color: Colors.grey.withOpacity(0.1),
                          blurRadius: 4.0,
                          spreadRadius: 2.0,
                        )
                      ]),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          AppStrings.selectIndustries,
                          style: lightBlue500TextStyle,
                        ),
                        const Divider(
                          color: AppColors.lightBlueColor,
                        ),
                        Expanded(
                            child: Scrollbar(
                          thickness: 5,
                          radius: const Radius.circular(5),
                          thumbVisibility: false,
                          child: ListView(
                            padding: EdgeInsets.zero,
                            children: [
                              InkWell(
                                onTap: () =>
                                    controller.changeCheckBoxSelectIndustries(
                                        controller.selectAll),
                                child: Row(
                                  children: [
                                    GetBuilder<
                                        ReceiveLeadingBusinessController>(
                                      builder: (_) {
                                        return Checkbox(
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(5)),
                                          side: const BorderSide(
                                              color: AppColors.lightBlueColor,
                                              width: 0.5),
                                          checkColor: Colors.white,
                                          activeColor: AppColors.lightBlueColor,
                                          value: controller.selectAll.value,
                                          onChanged: (value) => controller
                                              .changeCheckBoxSelectIndustries(
                                                  controller.selectAll),
                                        );
                                      },
                                    ),
                                    SizedBox(
                                      width: 3.w,
                                    ),
                                    Text(
                                      controller.selectAll.title,
                                      style: blackBold16Color,
                                    ),
                                  ],
                                ),
                              ),
                              ...controller.checkBoxList.map(
                                (item) => InkWell(
                                  onTap: () => controller
                                      .changeCheckBoxSelectItemIndustries(item),
                                  child: Row(
                                    children: [
                                      GetBuilder<
                                          ReceiveLeadingBusinessController>(
                                        builder: (_) {
                                          return Checkbox(
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(5)),
                                            side: const BorderSide(
                                                color: AppColors.lightBlueColor,
                                                width: 0.5),
                                            checkColor: Colors.white,
                                            activeColor:
                                                AppColors.lightBlueColor,
                                            value: item.value,
                                            onChanged: (value) => controller
                                                .changeCheckBoxSelectItemIndustries(
                                                    item),
                                          );
                                        },
                                      ),
                                      SizedBox(
                                        width: 3.w,
                                      ),
                                      Text(
                                        item.title,
                                        style: blackBold16Color,
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        ))
                      ])),
            ),
            SizedBox(
              height: 3.h,
            ),
          ]),
        ));
  }
}
