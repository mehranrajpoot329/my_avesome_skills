import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Modules/Personal/ad_Screen/ad_screeb_can_help_personal/ad_screen_can_help_personal.dart';

class ReceiveLeadingBusinessController extends GetxController {
  final selectAll = CheckBoxModal(
    title: 'title',
  );
  final checkBoxList = [
    CheckBoxModal(title: 'House Cleaning'),
    CheckBoxModal(title: 'End of Lease Cleaning'),
    CheckBoxModal(title: 'Carpet Steam Cleaning'),
    CheckBoxModal(title: 'Window Cleaning'),
    CheckBoxModal(title: 'Outdoor Cleaning'),
    CheckBoxModal(title: 'Office Cleaning'),
    CheckBoxModal(title: 'High Pressure Cleaning'),
    CheckBoxModal(title: 'Upholstery Cleaning'),
  ];

  var industries = 'Select Industries';
  RxString category = ''.obs;

  List categories = <String>[
    'UI/UX',
    'Design',
    'Website  ',
    'App',
  ];

  List selectIndustriesList = [
    'Select All',
    'House Cleaning',
    'End of Lease Cleaning',
    'Carpet Steam Cleaning',
    'Window Cleaning',
    'Select All',
    'House Cleaning',
    'End of Lease Cleaning',
    'Carpet Steam Cleaning',
    'Window Cleaning',
  ];

  final selected = "some book type".obs;
  void setSelected(String value) {
    selected.value = value;
  }

  // void setSelected(String value) {
  //   industries.value = value;
  // }

  changeCheckBoxSelectIndustries(CheckBoxModal checkBoxModal) {
    checkBoxModal.value = !checkBoxModal.value;
    update();
  }

  changeCheckBoxSelectItemIndustries(CheckBoxModal checkBoxModal) {
    checkBoxModal.value = !checkBoxModal.value;
    update();
  }
}
