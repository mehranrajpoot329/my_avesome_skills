import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:sizer/sizer.dart';

class SocialMediaWidgetBusiness extends StatelessWidget {
  String socialMediaName, imagePath;
  Color color;

  SocialMediaWidgetBusiness(
      {required this.socialMediaName,
      required this.imagePath,
      required this.color});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 8.h,
      width: 45.w,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(9),
          border: Border.all(color: color, width: 1, style: BorderStyle.solid),
          boxShadow: [
            BoxShadow(
                offset: Offset(0, 5),
                blurRadius: 1.0,
                spreadRadius: 1.0,
                color: Colors.grey.withOpacity(0.1))
          ]),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SvgPicture.asset(
            imagePath,
          ),
          const SizedBox(
            width: 10,
          ),
          Text(socialMediaName,
              style: TextStyle(
                  fontFamily: 'poppins',
                  color: color,
                  fontSize: 18,
                  fontWeight: FontWeight.w400))
        ],
      ),
    );
  }
}
