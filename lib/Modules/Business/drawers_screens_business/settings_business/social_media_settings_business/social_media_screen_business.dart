import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Business/drawers_screens_business/settings_business/social_media_settings_business/build_social_media_widget_business.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:sizer/sizer.dart';

import '../../../custom_scroll_appBar_business/nested_business_scroll_widget.dart';

class SocialMediaScreenBusiness extends StatelessWidget {
  const SocialMediaScreenBusiness({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NestedBusinessScroll(
        floatingActionButton: WidgetMethod.floatingActionButton(context),
        body: Padding(
            padding: EdgeInsets.only(left: 2.w, right: 2.w),
            child: SingleChildScrollView(
                physics: NeverScrollableScrollPhysics(),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 2.h,
                      ),
                      Row(
                        children: [
                          GestureDetector(
                            onTap: () => Get.back(),
                            child: Icon(
                              Icons.arrow_back_ios_new,
                              color: Colors.black,
                              size: 3.h,
                            ),
                          ),
                          SizedBox(
                            width: 5.w,
                          ),
                          const Text(
                            AppStrings.settings,
                            style: montserrat20TextStyle,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 2.h,
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: 3.w, vertical: 2.h),
                        height: 10.h,
                        width: 100.w,
                        decoration: BoxDecoration(
                          color: AppColors.lightGreen2Color,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: const Text(
                          AppStrings.socialMedia,
                          style: orangeColor600TextStyle,
                        ),
                      ),
                      SizedBox(
                        height: 2.h,
                      ),
                      const Text(
                        AppStrings.manageYourLinked,
                        style: blue600ColorTextStyle,
                      ),
                      const Text(
                        AppStrings.socialAccount,
                        style: blue600ColorTextStyle,
                      ),
                      SizedBox(
                        height: 3.h,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SocialMediaWidgetBusiness(
                            imagePath:
                                'assets/images/post_icon/facebook_icon.svg',
                            socialMediaName: AppStrings.facebook,
                            color: AppColors.blue18Color,
                          ),
                          SocialMediaWidgetBusiness(
                            imagePath:
                                'assets/images/post_icon/twitter_icon.svg',
                            socialMediaName: AppStrings.twitter,
                            color: AppColors.blue18Color,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 3.h,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SocialMediaWidgetBusiness(
                            imagePath:
                                'assets/images/post_icon/instagram_icon.svg',
                            socialMediaName: AppStrings.instagram,
                            color: AppColors.grey4Color,
                          ),
                          SocialMediaWidgetBusiness(
                            imagePath:
                                'assets/images/post_icon/linkedIn_icon.svg',
                            socialMediaName: AppStrings.linkedIn,
                            color: AppColors.blueColor,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 3.h,
                      ),
                      SocialMediaWidgetBusiness(
                        imagePath:
                            'assets/images/post_icon/google_plus_icon.svg',
                        socialMediaName: AppStrings.google,
                        color: AppColors.red2Color,
                      ),
                    ]))));
  }
}
