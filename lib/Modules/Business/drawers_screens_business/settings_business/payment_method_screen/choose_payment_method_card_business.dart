import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:sizer/sizer.dart';

class ChoosePaymentMethodCardBusiness extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 4.w, vertical: 2.h),
      height: 26.h,
      width: 240,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
                offset: Offset(0, 3),
                blurRadius: 1.0,
                spreadRadius: 1.0,
                color: Colors.grey..withOpacity(0.1))
          ]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              CircleAvatar(
                radius: 4,
                backgroundColor: AppColors.grey3Color,
              ),
              SizedBox(
                width: 1.w,
              ),
              CircleAvatar(
                radius: 4,
                backgroundColor: AppColors.grey3Color,
              ),
              SizedBox(
                width: 1.w,
              ),
              CircleAvatar(
                radius: 4,
                backgroundColor: AppColors.grey3Color,
              )
            ],
          ),
          SizedBox(
            height: 2.h,
          ),
          SvgPicture.asset(
            'assets/images/post_icon/master_card_icon.svg',
            height: 24.sp,
            width: 24.sp,
          ),
          SizedBox(
            height: 2.h,
          ),
          Text(
            AppStrings.masterCardNumber,
            style: blackBold16Color,
          ),
          SizedBox(
            height: 1.h,
          ),
          const Text(
            AppStrings.addOnTen,
            style: grey14Color,
          ),
          SizedBox(
            height: 1.h,
          ),
          const Text(
            AppStrings.addOnTen,
            style: grey14Color,
          ),
        ],
      ),
    );
  }
}
