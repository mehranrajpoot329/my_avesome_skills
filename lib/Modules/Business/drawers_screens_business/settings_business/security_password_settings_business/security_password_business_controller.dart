import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SecurityPasswordBusinessController extends GetxController {
  final TextEditingController phoneNumberController = TextEditingController();
  final TextEditingController confirmPhoneController = TextEditingController();

  bool isFingerPrint = false;
  bool isFaceRecognition = false;
  bool isTwoFactorAuthentication = false;
  bool isAddressVerification = false;
  bool isResetPassword = false;
  bool isSecurityQuestion = false;

  changeSwitchFingerPrint(bool value) {
    isFingerPrint = value;
    update();
  }

  changeSwitchFaceRecognition(bool value) {
    isFaceRecognition = value;
    update();
  }

  changeSwitchTwoFactorAuthentication(bool value) {
    isTwoFactorAuthentication = value;
    update();
  }

  changeSwitchAddressVerification(bool value) {
    isAddressVerification = value;
    update();
  }

  changeSwitchResetPassword(bool value) {
    isResetPassword = value;
    update();
  }

  changeSwitchSecurityQuestion(bool value) {
    isSecurityQuestion = value;
    update();
  }
}
