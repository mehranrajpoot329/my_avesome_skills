import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Business/drawers_screens_business/settings_business/company_details_business/company_detail_card.dart';
import 'package:AwesomeSkills/widgets/custom_bottom_appBar_navi/custom_bottom_appBar_navi.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:sizer/sizer.dart';

import '../../../custom_scroll_appBar_business/nested_business_scroll_widget.dart';

class CompanyDetailsScreenBusiness extends StatelessWidget {
  const CompanyDetailsScreenBusiness({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NestedBusinessScroll(
      floatingActionButton: WidgetMethod.floatingActionButton(context),
      body: Padding(
          padding: EdgeInsets.only(left: 3.w, right: 2.w),
          child: ListView(padding: EdgeInsets.zero, children: [
            Row(
              children: [
                GestureDetector(
                  onTap: () => Get.back(),
                  child: Icon(
                    Icons.arrow_back_ios_new,
                    color: Colors.black,
                    size: 3.h,
                  ),
                ),
                SizedBox(
                  width: 5.w,
                ),
                const Text(
                  AppStrings.settings,
                  style: montserrat20TextStyle,
                ),
              ],
            ),
            SizedBox(
              height: 2.h,
            ),
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: 4.w,
              ),
              height: 14.5.h,
              width: 100.w,
              decoration: BoxDecoration(
                color: AppColors.lightGreen2Color,
                borderRadius: BorderRadius.circular(15.0),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    AppStrings.inSeasonPTYLTD,
                    style: orangeColor600TextStyle,
                  ),
                  Text(
                    AppStrings.joinedIn2007,
                    style: blackBold16Color,
                  ),
                  Text(
                    AppStrings.adminFlorinBelbe,
                    style: blackBold16Color,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            Padding(
                padding: EdgeInsets.only(left: 2.w, right: 2.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: CompanyDetailsAdministratorCard(
                        imagePath: 'assets/images/administrator_icon.svg',
                        textName: AppStrings.administrator,
                        isComplete: true,
                        onTap: () {},
                      ),
                    ),
                    SizedBox(
                      width: 4.w,
                    ),
                    Expanded(
                      child: CompanyDetailsAddressCard(
                        imagePath: 'assets/images/home_icon_indigo.svg',
                        textName: AppStrings.address,
                        companyAddress: AppStrings.companyAddress,
                        isComplete: true,
                        onTap: () {},
                      ),
                    )
                  ],
                )),
            SizedBox(
              height: 3.h,
            ),
            Padding(
                padding: EdgeInsets.only(left: 2.w, right: 2.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: CompanyDetailsEmailCard(
                        imagePath: 'assets/images/email_red_icon.svg',
                        textName: AppStrings.email,
                        emailAddress: AppStrings.fbelbeSpaflo,
                        isComplete: true,
                        onTap: () {},
                      ),
                    ),
                    SizedBox(
                      width: 4.w,
                    ),
                    Expanded(
                      child: CompanyDetailsPhoneNumberCard(
                        imagePath: 'assets/images/yellow_telephone.svg',
                        textName: AppStrings.phoneNumberText,
                        homeNumber: '(07) 5577 2962',
                        mobileNumber: '0433 871 731',
                        isComplete: true,
                        onTap: () {},
                      ),
                    )
                  ],
                )),
            SizedBox(
              height: 3.h,
            ),
            Padding(
                padding: EdgeInsets.only(left: 2.w, right: 2.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: CompanyDetailsABNCertificatesCard(
                        imagePath: 'assets/images/post_icon/abn_icon.svg',
                        textName: 'ABN',
                        number: '125 987 566 167',
                        isComplete: true,
                        onTap: () {},
                      ),
                    ),
                    SizedBox(
                      width: 4.w,
                    ),
                    Expanded(
                      child: CompanyDetailsABNCertificatesCard(
                        imagePath: 'assets/images/certificate_license.svg',
                        textName: AppStrings.certificateAndLicense,
                        number: 'QBCC 856598',
                        isComplete: true,
                        onTap: () {},
                      ),
                    )
                  ],
                )),
            SizedBox(
              height: 2.h,
            ),
          ])),
    );
  }
}
