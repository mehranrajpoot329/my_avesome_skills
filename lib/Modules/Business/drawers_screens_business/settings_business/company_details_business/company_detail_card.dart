import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:sizer/sizer.dart';

class CompanyDetailsAdministratorCard extends StatelessWidget {
  String imagePath, textName;
  bool isComplete;
  Function onTap;

  CompanyDetailsAdministratorCard(
      {required this.imagePath,
      required this.textName,
      required this.onTap,
      required this.isComplete});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onTap(),
      child: Container(
        padding: EdgeInsets.only(top: 3.h, left: 2.w, right: 2.w),
        height: 20.h,
        width: 20.h,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
            boxShadow: [
              BoxShadow(
                  offset: Offset(0, 5),
                  color: Colors.grey.withOpacity(0.3),
                  spreadRadius: 1.0,
                  blurRadius: 1.0),
            ]),
        child: Stack(
          children: [
            Positioned(
              right: 10,
              child: isComplete
                  ? SvgPicture.asset(
                      'assets/images/post_icon/check_icon.svg',
                      height: 13.sp,
                      width: 13.sp,
                    )
                  : Text(
                      AppStrings.inComplete,
                      style: redColorTextStyle,
                    ),
            ),
            SizedBox(
              height: 2.h,
            ),
            Positioned(
              top: 20.0,
              left: 10,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SvgPicture.asset(
                    imagePath,
                    height: 24.sp,
                    width: 24.sp,
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Text(
                    textName,
                    style: blackBold16Color,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class CompanyDetailsAddressCard extends StatelessWidget {
  String imagePath, textName, companyAddress;
  bool isComplete;
  Function onTap;

  CompanyDetailsAddressCard(
      {required this.imagePath,
      required this.textName,
      required this.companyAddress,
      required this.onTap,
      required this.isComplete});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onTap(),
      child: Container(
        padding: EdgeInsets.only(top: 2.h, left: 2.w, right: 2.w),
        height: 20.h,
        width: 20.h,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
            boxShadow: [
              BoxShadow(
                  offset: Offset(0, 5),
                  color: Colors.grey.withOpacity(0.3),
                  spreadRadius: 1.0,
                  blurRadius: 1.0),
            ]),
        child: Stack(
          children: [
            Positioned(
              right: 10,
              child: isComplete
                  ? SvgPicture.asset(
                      'assets/images/post_icon/check_icon.svg',
                      height: 13.sp,
                      width: 13.sp,
                    )
                  : Text(
                      AppStrings.inComplete,
                      style: redColorTextStyle,
                    ),
            ),
            SizedBox(
              height: 2.h,
            ),
            Positioned(
              top: 5.0,
              left: 10,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SvgPicture.asset(
                    imagePath,
                    height: 20.sp,
                    width: 20.sp,
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  Text(
                    textName,
                    style: blackBold16Color,
                  ),
                  SizedBox(
                    height: 0.2.h,
                  ),
                  SizedBox(
                    width: 35.w,
                    child: Text(
                      companyAddress,
                      style: blackEightColor,
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class CompanyDetailsEmailCard extends StatelessWidget {
  String imagePath, textName, emailAddress;
  bool isComplete;
  Function onTap;

  CompanyDetailsEmailCard(
      {required this.imagePath,
      required this.textName,
      required this.emailAddress,
      required this.onTap,
      required this.isComplete});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onTap(),
      child: Container(
        padding: EdgeInsets.only(
          top: 2.h,
          left: 4.w,
          right: 5.w,
        ),
        height: 20.h,
        width: 20.h,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
            boxShadow: [
              BoxShadow(
                  offset: Offset(0, 5),
                  color: Colors.grey.withOpacity(0.3),
                  spreadRadius: 1.0,
                  blurRadius: 1.0),
            ]),
        child: Column(
          children: [
            Align(
              alignment: Alignment.bottomRight,
              child: isComplete
                  ? SvgPicture.asset(
                      'assets/images/post_icon/check_icon.svg',
                      height: 13.sp,
                      width: 13.sp,
                    )
                  : const Text(
                      AppStrings.inComplete,
                      style: redColorTextStyle,
                    ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SvgPicture.asset(
                  imagePath,
                  height: 24.sp,
                  width: 24.sp,
                ),
                SizedBox(
                  height: 1.h,
                ),
                Text(
                  textName,
                  style: blackBold16Color,
                ),
                SizedBox(
                  height: 0.5.h,
                ),
                Text(
                  emailAddress,
                  style: blackEightColor,
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class CompanyDetailsPhoneNumberCard extends StatelessWidget {
  String imagePath, textName, homeNumber, mobileNumber;
  bool isComplete;
  Function onTap;

  CompanyDetailsPhoneNumberCard(
      {required this.imagePath,
      required this.textName,
      required this.homeNumber,
      required this.mobileNumber,
      required this.onTap,
      required this.isComplete});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onTap(),
      child: Container(
        padding: EdgeInsets.only(top: 2.h, left: 2.w, right: 5.w),
        height: 20.h,
        width: 20.h,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
            boxShadow: [
              BoxShadow(
                  offset: Offset(0, 5),
                  color: Colors.grey.withOpacity(0.3),
                  spreadRadius: 1.0,
                  blurRadius: 1.0),
            ]),
        child: Column(
          children: [
            Align(
              alignment: Alignment.bottomRight,
              child: isComplete
                  ? SvgPicture.asset(
                      'assets/images/post_icon/check_icon.svg',
                      height: 13.sp,
                      width: 13.sp,
                    )
                  : Text(
                      AppStrings.inComplete,
                      style: redColorTextStyle,
                    ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SvgPicture.asset(
                  imagePath,
                  height: 20.sp,
                  width: 20.sp,
                ),
                SizedBox(
                  height: 1.h,
                ),
                Text(
                  textName,
                  style: blackBold16Color,
                ),
                Text(
                  'Home : $homeNumber',
                  style: blackEightColor,
                ),
                Text(
                  'Mobile : $mobileNumber',
                  style: blackEightColor,
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class CompanyDetailsABNCertificatesCard extends StatelessWidget {
  String imagePath, textName, number;
  bool isComplete;
  Function onTap;

  CompanyDetailsABNCertificatesCard(
      {required this.imagePath,
      required this.textName,
      required this.number,
      required this.onTap,
      required this.isComplete});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onTap(),
      child: Container(
        padding: EdgeInsets.only(top: 2.h, left: 4.w, right: 5.w),
        height: 20.h,
        width: 20.h,
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
            boxShadow: [
              BoxShadow(
                  offset: Offset(0, 5),
                  color: Colors.grey.withOpacity(0.3),
                  spreadRadius: 1.0,
                  blurRadius: 1.0),
            ]),
        child: Column(
          children: [
            Align(
              alignment: Alignment.bottomRight,
              child: isComplete
                  ? SvgPicture.asset(
                      'assets/images/post_icon/check_icon.svg',
                      height: 13.sp,
                      width: 13.sp,
                    )
                  : const Text(
                      AppStrings.inComplete,
                      style: redColorTextStyle,
                    ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SvgPicture.asset(
                    imagePath,
                    height: 30.sp,
                    width: 30.sp,
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  FittedBox(
                    child: Text(
                      textName,
                      style: blackBold16Color,
                    ),
                  ),
                  SizedBox(
                    height: 0.5.h,
                  ),
                  Text(
                    number,
                    style: blackEightColor,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
