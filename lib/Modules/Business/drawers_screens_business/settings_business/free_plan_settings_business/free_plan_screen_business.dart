import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:AwesomeSkills/widgets/custom_bottom_appBar_navi/custom_bottom_appBar_navi.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:sizer/sizer.dart';

import '../../../custom_scroll_appBar_business/nested_business_scroll_widget.dart';

class FreePlanScreenBusiness extends StatelessWidget {
  const FreePlanScreenBusiness({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NestedBusinessScroll(
        floatingActionButton: WidgetMethod.floatingActionButton(context),
        body: Padding(
            padding: EdgeInsets.only(left: 2.w, right: 2.w),
            child: SingleChildScrollView(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                  SizedBox(
                    height: 2.h,
                  ),
                  Row(
                    children: [
                      GestureDetector(
                        onTap: () => Get.back(),
                        child: Icon(
                          Icons.arrow_back_ios_new,
                          color: Colors.black,
                          size: 3.h,
                        ),
                      ),
                      SizedBox(
                        width: 5.w,
                      ),
                      const Text(
                        AppStrings.settings,
                        style: montserrat20TextStyle,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: 3.w, vertical: 2.h),
                    height: 10.h,
                    width: 100.w,
                    decoration: BoxDecoration(
                      color: AppColors.lightGreen2Color,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Text(
                          AppStrings.myPlan,
                          style: orangeColor600TextStyle,
                        ),
                        Text(
                          AppStrings.postedThreeOfFourAds,
                          style: blackBoldPoppinsColor,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 3.h,
                  ),
                  Center(
                    child: Container(
                      padding: EdgeInsets.only(left: 10.w),
                      height: 40.h,
                      width: 80.w,
                      decoration: BoxDecoration(
                          color: AppColors.blue10Color,
                          borderRadius: BorderRadius.circular(8),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey.withOpacity(0.2),
                                spreadRadius: 2.0,
                                blurRadius: 4.0)
                          ]),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text(
                            AppStrings.myPlan,
                            style: lightBlue32ColorTextStyle,
                          ),
                          SizedBox(
                            height: 0.5.h,
                          ),
                          Text(
                            AppStrings.yourAreCurrently,
                            style: blackBold16Color,
                          ),
                          Text(
                            AppStrings.business,
                            style: green40ColorTextStyle,
                          ),
                          SizedBox(
                            height: 2.h,
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 5.h,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 25.w),
                    child: Text(
                      AppStrings.invoices,
                      style: blackBold600Color,
                    ),
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 30.w),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        GestureDetector(
                            onTap: () {},
                            child: const Text(
                              AppStrings.viewAllInvoices,
                              style: blue20ColorTextStyle,
                            )),
                        SizedBox(
                          height: 0.5.h,
                        ),
                        GestureDetector(
                            onTap: () {},
                            child: const Text(
                              AppStrings.downloadInvoices,
                              style: blue20ColorTextStyle,
                            ))
                      ],
                    ),
                  ),
                ]))));
  }
}
