import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Business/drawers_screens_business/settings_business/my_profile_setting_business/profile_setting_body_expansion_tile_business.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/settings/my_profile_setting/profile_setting_body_expansion_tile.dart';
import 'package:AwesomeSkills/widgets/custom_bottom_appBar_navi/custom_bottom_appBar_navi.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';

import '../../../custom_scroll_appBar_business/nested_business_scroll_widget.dart';

class MyProfileSettingBusiness extends StatelessWidget {
  const MyProfileSettingBusiness({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NestedBusinessScroll(
      floatingActionButton: WidgetMethod.floatingActionButton(context),
      body: Padding(
          padding: const EdgeInsets.only(left: 10.0, right: 10),
          child: SingleChildScrollView(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    GestureDetector(
                      onTap: () => Get.back(),
                      child: const Icon(
                        Icons.arrow_back_ios_new,
                        color: Colors.black,
                        size: 25,
                      ),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    const Text(
                      AppStrings.settings,
                      style: montserrat20TextStyle,
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 17),
                  height: 80,
                  width: MediaQuery.of(context).size.width - 20,
                  decoration: BoxDecoration(
                      color: AppColors.lightGreen2Color,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                            blurRadius: 4.0,
                            spreadRadius: 2.0,
                            color: Colors.grey.withOpacity(0.2))
                      ]),
                  child: const Text(
                    AppStrings.profileSettings,
                    style: orangeColor600TextStyle,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                ProfileSettingBodyExpansionTileBusiness(),
                const SizedBox(
                  height: 60,
                ),
              ]))),
    );
  }
}
