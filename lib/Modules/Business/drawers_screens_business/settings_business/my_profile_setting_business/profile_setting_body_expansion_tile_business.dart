import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:AwesomeSkills/widgets/button/custom_icon_button.dart';

class ProfileSettingBodyExpansionTileBusiness extends StatelessWidget {
  const ProfileSettingBodyExpansionTileBusiness({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ExpansionTile(
          title: Row(
            children: [
              Text(
                AppStrings.profilePicture,
                style: lightBlue500TextStyle,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                AppStrings.optional,
                style: grey14Color,
              ),
            ],
          ),
          children: [
            const Padding(
              padding: EdgeInsets.only(right: 100.0),
              child: Text(
                AppStrings.uploadOrChange,
                style: blackPoppins300TextStyle,
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            DottedBorder(
              dashPattern: const [6, 2],
              radius: const Radius.circular(10),
              color: AppColors.indigoColor,
              strokeWidth: 1.5,
              child: Container(
                padding: const EdgeInsets.symmetric(vertical: 20),
                height: 124,
                width: MediaQuery.of(context).size.width - 50,
                decoration: const BoxDecoration(
                  color: Colors.transparent,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SvgPicture.asset(
                      'assets/images/post_icon/upload_icon.svg',
                    ),
                    const SizedBox(
                      height: 2,
                    ),
                    Text(
                      AppStrings.dragAndDrop,
                      style: blackBoldPoppinsColor,
                    ),
                    const SizedBox(
                      height: 2,
                    ),
                    Text(
                      AppStrings.or,
                      style: blackBoldPoppinsColor,
                    ),
                    const SizedBox(
                      height: 2,
                    ),
                    const Text(
                      AppStrings.clickToBrowse,
                      style: yellowPoppins300TextStyle,
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            )
          ],
        ),
        ExpansionTile(
          title: Row(
            children: [
              Text(
                AppStrings.star,
                style: red4ColorTextStyle,
              ),
              Text(
                AppStrings.aboutMe,
                style: lightBlue500TextStyle,
              ),
            ],
          ),
          children: [
            TextFormField(
              maxLines: 5,
              maxLength: 500,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
          ],
        ),
        ExpansionTile(
          title: Row(
            children: [
              Text(
                AppStrings.portfolio,
                style: lightBlue500TextStyle,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                AppStrings.optional,
                style: grey14Color,
              ),
            ],
          ),
          children: [
            const Padding(
              padding: EdgeInsets.only(right: 100.0),
              child: Text(
                AppStrings.uploadOrChange,
                style: blackPoppins300TextStyle,
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            DottedBorder(
              dashPattern: const [6, 2],
              radius: const Radius.circular(10),
              color: AppColors.indigoColor,
              strokeWidth: 1.5,
              child: Container(
                padding: const EdgeInsets.symmetric(vertical: 20),
                height: 124,
                width: MediaQuery.of(context).size.width - 50,
                decoration: const BoxDecoration(
                  color: Colors.transparent,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SvgPicture.asset(
                      'assets/images/post_icon/upload_icon.svg',
                    ),
                    const SizedBox(
                      height: 2,
                    ),
                    Text(
                      AppStrings.dragAndDrop,
                      style: blackBoldPoppinsColor,
                    ),
                    const SizedBox(
                      height: 2,
                    ),
                    Text(
                      AppStrings.or,
                      style: blackBoldPoppinsColor,
                    ),
                    const SizedBox(
                      height: 2,
                    ),
                    const Text(
                      AppStrings.clickToBrowse,
                      style: yellowPoppins300TextStyle,
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            )
          ],
        ),
        ExpansionTile(
          title: Row(
            children: [
              Text(
                AppStrings.resume,
                style: lightBlue500TextStyle,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                AppStrings.optional,
                style: grey14Color,
              ),
            ],
          ),
          children: [
            const Padding(
              padding: EdgeInsets.only(right: 100.0),
              child: Text(
                AppStrings.uploadYourResume,
                style: blackPoppins300TextStyle,
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            DottedBorder(
              dashPattern: const [6, 2],
              radius: const Radius.circular(10),
              color: AppColors.indigoColor,
              strokeWidth: 1.5,
              child: Container(
                padding: const EdgeInsets.symmetric(vertical: 20),
                height: 124,
                width: MediaQuery.of(context).size.width - 50,
                decoration: const BoxDecoration(
                  color: Colors.transparent,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    SvgPicture.asset(
                      'assets/images/post_icon/upload_icon.svg',
                    ),
                    const SizedBox(
                      height: 2,
                    ),
                    Text(
                      AppStrings.dragAndDrop,
                      style: blackBoldPoppinsColor,
                    ),
                    const SizedBox(
                      height: 2,
                    ),
                    Text(
                      AppStrings.or,
                      style: blackBoldPoppinsColor,
                    ),
                    const SizedBox(
                      height: 2,
                    ),
                    const Text(
                      AppStrings.clickToBrowse,
                      style: yellowPoppins300TextStyle,
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            )
          ],
        ),
        ExpansionTile(
          title: Row(
            children: [
              Text(
                AppStrings.education,
                style: lightBlue500TextStyle,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                AppStrings.optional,
                style: grey14Color,
              ),
            ],
          ),
          children: [
            const Padding(
              padding: EdgeInsets.only(right: 65.0),
              child: Text(
                AppStrings.whatLevelOfEducation,
                style: blackPoppins300TextStyle,
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              height: 19,
              width: MediaQuery.of(context).size.width - 30,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.transparent,
                  border: Border.all(
                      color: Colors.grey,
                      width: 1.5,
                      style: BorderStyle.solid)),
            ),
            const SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.only(right: 230),
              child: CustomIconButton(
                buttonName: AppStrings.addMore,
                width: 120,
                height: 30,
                buttonColor: AppColors.indigoColor,
                textStyle: white10TextStyle,
                borderRadius: BorderRadius.circular(10),
                onTap: () {},
              ),
            ),
            const SizedBox(
              height: 10,
            ),
          ],
        ),
        ExpansionTile(
          title: Row(
            children: [
              Text(
                AppStrings.awards,
                style: lightBlue500TextStyle,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                AppStrings.optional,
                style: grey14Color,
              ),
            ],
          ),
          children: [
            const Padding(
              padding: EdgeInsets.only(right: 170.0),
              child: Text(
                AppStrings.listYourLifeAchievement,
                style: blackPoppins300TextStyle,
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              height: 19,
              width: MediaQuery.of(context).size.width - 30,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.transparent,
                  border: Border.all(
                      color: Colors.grey,
                      width: 1.5,
                      style: BorderStyle.solid)),
            ),
            const SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.only(right: 230),
              child: CustomIconButton(
                buttonName: AppStrings.addMore,
                width: 120,
                height: 30,
                buttonColor: AppColors.indigoColor,
                textStyle: white10TextStyle,
                borderRadius: BorderRadius.circular(10),
                onTap: () {},
              ),
            ),
            const SizedBox(
              height: 10,
            ),
          ],
        ),
        ExpansionTile(
          title: Row(
            children: [
              Text(
                AppStrings.star,
                style: red4ColorTextStyle,
              ),
              Text(
                AppStrings.contact,
                style: lightBlue500TextStyle,
              ),
            ],
          ),
          children: [
            const Padding(
              padding: EdgeInsets.only(right: 170.0),
              child: Text(
                AppStrings.insertYourContact,
                style: blackPoppins300TextStyle,
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              children: [
                const Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(left: 20),
                    child: Text(
                      AppStrings.address,
                      style: black9PoppinsColor,
                    ),
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
                SizedBox(
                  height: 30,
                  width: MediaQuery.of(context).size.width * 0.7,
                  child: Padding(
                    padding: const EdgeInsets.only(right: 20.0),
                    child: TextFormField(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              children: [
                const Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(left: 20),
                    child: Text(
                      AppStrings.mobile,
                      style: black9PoppinsColor,
                    ),
                  ),
                ),
                const SizedBox(
                  width: 20,
                ),
                SizedBox(
                  height: 30,
                  width: MediaQuery.of(context).size.width * 0.7,
                  child: Padding(
                    padding: const EdgeInsets.only(right: 20.0),
                    child: TextFormField(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              children: [
                const Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(left: 20),
                    child: Text(
                      AppStrings.homePhone,
                      style: black9PoppinsColor,
                    ),
                  ),
                ),
                SizedBox(
                  height: 30,
                  width: MediaQuery.of(context).size.width * 0.7,
                  child: Padding(
                    padding: const EdgeInsets.only(right: 20.0),
                    child: TextFormField(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              children: [
                const Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(left: 20),
                    child: Text(
                      AppStrings.emailDash,
                      style: black9PoppinsColor,
                    ),
                  ),
                ),
                SizedBox(
                  height: 30,
                  width: MediaQuery.of(context).size.width * 0.7,
                  child: Padding(
                    padding: const EdgeInsets.only(right: 20.0),
                    child: TextFormField(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Row(
              children: [
                const Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(left: 20),
                    child: Text(
                      AppStrings.website,
                      style: black9PoppinsColor,
                    ),
                  ),
                ),
                SizedBox(
                  height: 30,
                  width: MediaQuery.of(context).size.width * 0.7,
                  child: Padding(
                    padding: const EdgeInsets.only(right: 20.0),
                    child: TextFormField(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.only(
                left: 20.0,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  CustomButton(
                    height: 40,
                    width: 74,
                    buttonName: AppStrings.save,
                    borderRadius: BorderRadius.circular(3),
                    buttonColor: AppColors.lightBlueColor,
                    textStyle: blackPoppins300TextStyle,
                    onTap: () {},
                  ),
                  SizedBox(
                    width: 130,
                    child: Row(
                      children: [
                        SvgPicture.asset(
                          'assets/images/post_icon/edit_icon.svg',
                          height: 14,
                          width: 16,
                        ),
                        const Text(
                          AppStrings.edit,
                          style: indigoColorTextStyle,
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            const SizedBox(
              height: 15,
            ),
          ],
        ),
      ],
    );
  }
}
