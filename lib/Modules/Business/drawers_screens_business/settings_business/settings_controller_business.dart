import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SettingsControllerBusiness extends GetxController {
  final TextEditingController phoneNumberController = TextEditingController();
  final TextEditingController confirmPhoneController = TextEditingController();
  bool isFingerPrint = false;
  bool isFaceRecognition = false;
  bool isTwoFactorAuthentication = false;
  bool isAddressVerification = false;
  bool isSecurityQuestion = false;

  changeSwitchFingerPrint(bool value) {
    isFingerPrint = value;
    update();
  }

  changeSwitchFaceRecognition(bool value) {
    isFingerPrint = value;
    update();
  }

  changeSwitchTwoFactorAuthentication(bool value) {
    isTwoFactorAuthentication = value;
    update();
  }

  changeSwitchAddressVerification(bool value) {
    isAddressVerification = value;
    update();
  }

  changeSwitchSecurityQuestion(bool value) {
    isSecurityQuestion = value;
    update();
  }
}
