import 'package:get/get.dart';

class MyQuotesBusinessController extends GetxController {
  RxBool getQuotes = false.obs;
  RxString quotes = ''.obs;
  RxString quotesDays = ''.obs;

  changeSwitchCanNeedWith(bool value) {
    getQuotes.value = value;
  }
}
