import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Modules/Business/drawers_screens_business/my_quotes_business/my_quotes_business_controller.dart';
import 'package:AwesomeSkills/Modules/Business/drawers_screens_business/my_quotes_business/my_quotes_card_build_business.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/my_quotes/my_quotes_card_build.dart';
import 'package:AwesomeSkills/widgets/drop_down.dart';
import 'package:sizer/sizer.dart';

class ICanHelpTabBarScreenMyQuotesBusiness extends StatelessWidget {
  final myQuotesController = Get.put(MyQuotesBusinessController());

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: 100,
      itemBuilder: (context, int index) {
        return MyQuotesCardBuildBusiness(
          image: 'assets/images/profile_alex.png',
          personName: AppStrings.tomLatham,
          field: AppStrings.webDesigner,
          number: AppStrings.hashZeroOne,
          title: AppStrings.houseMoving,
          detail: AppStrings.toHelpWithMoving,
          price: AppStrings.eightHundred,
          date: AppStrings.twentyFiveJanuary,
        );
      },
    );
  }
}
