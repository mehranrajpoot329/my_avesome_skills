import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';

class BuildCardWidgetBusiness extends StatelessWidget {
  String imagePath, textName, postTimingAgo;

  BuildCardWidgetBusiness(
      {required this.imagePath,
      required this.textName,
      required this.postTimingAgo});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 30,
        ),
        Container(
          height: 100,
          width: 230,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                    color: Colors.grey.withOpacity(0.2),
                    spreadRadius: 2.0,
                    blurRadius: 4.0)
              ]),
          child: Stack(
            clipBehavior: Clip.none,
            children: [
              Positioned(
                top: 5,
                left: 10,
                child: Container(
                  padding: const EdgeInsets.only(top: 20),
                  height: 90,
                  width: 80,
                  decoration: BoxDecoration(
                      color: AppColors.brownColor,
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                          image: AssetImage(
                            imagePath,
                          ),
                          fit: BoxFit.contain)),
                ),
              ),
              Positioned(
                  left: -9.5,
                  top: -9.5,
                  child: Container(
                    height: 19,
                    width: 19,
                    decoration: const BoxDecoration(
                        color: AppColors.green10Color, shape: BoxShape.circle),
                    child: const Center(
                      child: Text(
                        AppStrings.seven,
                        style: whiteMulishTextStyle,
                      ),
                    ),
                  )),
              Positioned(
                top: 15,
                left: 100,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      textName,
                      style: blackMulish1TextStyle,
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Text(
                      postTimingAgo,
                      style: grey13MulishTextStyle,
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    SizedBox(
                      width: 50,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SvgPicture.asset(
                            'assets/images/post_icon/visible_grey.svg',
                            height: 15,
                          ),
                          SvgPicture.asset(
                            'assets/images/post_icon/calender_add_icon.svg',
                            height: 15,
                          )
                        ],
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
        const SizedBox(
          height: 20,
        )
      ],
    );
  }
}
