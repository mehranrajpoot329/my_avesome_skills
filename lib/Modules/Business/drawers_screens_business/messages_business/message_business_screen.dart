import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Business/custom_scroll_appBar_business/nested_business_scroll_widget.dart';
import 'package:AwesomeSkills/Modules/Business/drawers_screens_business/messages_business/message_tabBar_widget_screen_business.dart';
import 'package:AwesomeSkills/Modules/Business/drawers_screens_business/messages_business/messgae_typing_screen_business.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:scroll_loop_auto_scroll/scroll_loop_auto_scroll.dart';
import 'package:sizer/sizer.dart';

class MessageScreenBusiness extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: NestedBusinessScroll(
          floatingActionButton: WidgetMethod.floatingActionButton(context),
          body: SingleChildScrollView(
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              SizedBox(
                height: 1.h,
              ),
              Padding(
                padding: EdgeInsets.only(left: 2.w, right: 2.w),
                child: Row(
                  children: [
                    GestureDetector(
                      onTap: () => Get.back(),
                      child: Icon(
                        Icons.arrow_back_ios_new,
                        color: Colors.black,
                        size: 3.h,
                      ),
                    ),
                    SizedBox(
                      width: 5.w,
                    ),
                    Text(
                      AppStrings.message,
                      style: montserrat20TextStyle,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
              Container(
                height: 13.h,
                width: double.infinity,
                decoration: const BoxDecoration(
                  color: AppColors.lightGreen2Color,
                ),
                child: Center(
                  child: Container(
                    padding: EdgeInsets.only(left: 2.w, right: 4.w),
                    height: 10.h,
                    width: 95.w,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: ScrollLoopAutoScroll(
                      scrollDirection: Axis.horizontal, //required
                      delay: Duration(seconds: 1),
                      duration: Duration(seconds: 50),
                      gap: 25,
                      reverseScroll: false,
                      duplicateChild: 5,
                      enableScrollInput: true,
                      delayAfterScrollInput: Duration(seconds: 1),
                      child: Row(
                        children: [
                          Container(
                            child: Container(
                              height: 15.h,
                              width: 15.w,
                              decoration: BoxDecoration(
                                  color: AppColors.lightGreenColor,
                                  shape: BoxShape.circle,
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.grey.withOpacity(0.2),
                                        spreadRadius: 2.0,
                                        blurRadius: 4.0),
                                  ]),
                              child: Center(
                                child: SvgPicture.asset(
                                  'assets/images/post_icon/message_icon.svg',
                                  height: 18.sp,
                                  width: 18.sp,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 2.w,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                AppStrings.overAllMessage,
                                style: blackBoldPoppinsColor,
                              ),
                              Row(
                                children: [
                                  Text(
                                    AppStrings.twoThreeDoubleNine,
                                    style: blackBoldPoppinsColor,
                                  ),
                                  SizedBox(
                                    width: 1.w,
                                  ),
                                  Text(
                                    AppStrings.thisMonth,
                                    style: greyTextColor,
                                  ),
                                ],
                              ),
                            ],
                          ),
                          SizedBox(
                            width: 3.w,
                          ),
                          Container(
                            height: 40,
                            width: 0.5,
                            color: AppColors.lightBlueColor.withOpacity(0.6),
                          ),
                          SizedBox(
                            width: 3.w,
                          ),
                          Container(
                            height: 15.h,
                            width: 15.w,
                            decoration: BoxDecoration(
                                color: AppColors.lightGreen3Color,
                                shape: BoxShape.circle,
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.grey.withOpacity(0.2),
                                      spreadRadius: 2.0,
                                      blurRadius: 4.0)
                                ]),
                            child: Center(
                              child: SvgPicture.asset(
                                'assets/images/post_icon/send_icon.svg',
                                height: 18.sp,
                                width: 18.sp,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 2.w,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                AppStrings.sent,
                                style: blackBoldPoppinsColor,
                              ),
                              Row(
                                children: [
                                  Text(
                                    AppStrings.twoThreeNine,
                                    style: blackBoldPoppinsColor,
                                  ),
                                  SizedBox(
                                    width: 1.w,
                                  ),
                                  Text(
                                    AppStrings.thisMonth,
                                    style: greyTextColor,
                                  ),
                                ],
                              ),
                            ],
                          ),
                          SizedBox(
                            width: 3.w,
                          ),
                          Container(
                            child: Container(
                              height: 40,
                              width: 0.5,
                              color: AppColors.lightBlueColor.withOpacity(0.6),
                            ),
                          ),
                          SizedBox(
                            width: 3.w,
                          ),
                          Container(
                            height: 15.h,
                            width: 15.w,
                            decoration: BoxDecoration(
                                color: AppColors.lightGreen3Color,
                                shape: BoxShape.circle,
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.grey.withOpacity(0.2),
                                      spreadRadius: 2.0,
                                      blurRadius: 4.0)
                                ]),
                            child: Center(
                              child: SvgPicture.asset(
                                'assets/images/post_icon/received_icon.svg',
                                height: 18.sp,
                                width: 18.sp,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 2.w,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                AppStrings.receivedMessage,
                                style: blackBoldPoppinsColor,
                              ),
                              Row(
                                children: [
                                  Text(
                                    AppStrings.twoThreeNine,
                                    style: blackBoldPoppinsColor,
                                  ),
                                  SizedBox(
                                    width: 1.w,
                                  ),
                                  Text(
                                    AppStrings.thisMonth,
                                    style: greyTextColor,
                                  ),
                                ],
                              ),
                            ],
                          ),
                          SizedBox(
                            width: 3.w,
                          ),
                          Container(
                            height: 40,
                            width: 0.5,
                            color: AppColors.lightBlueColor.withOpacity(0.6),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 3.h,
              ),
              const MessageTabBarWidgetScreenBusiness(),
              const SizedBox(
                height: 20,
              ),
              SizedBox(
                height: 2.h,
              ),
            ]),
          ),
        ));
  }
}
