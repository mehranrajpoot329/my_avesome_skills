import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Business/drawers_screens_business/messages_business/build_card_widget_business.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/messages/build_card_widget.dart';
import 'package:AwesomeSkills/routes/app_pages.dart';

class ActiveTabBarScreenBusiness extends StatelessWidget {
  const ActiveTabBarScreenBusiness({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15.0),
      child: Scrollbar(
        trackVisibility: true,
        thickness: 10,
        radius: const Radius.circular(10),
        child: ListView.builder(
          scrollDirection: Axis.vertical,
          padding: EdgeInsets.only(right: 20),
          itemCount: 4,
          itemBuilder: (context, int index) {
            return GestureDetector(
              onTap: () => Get.toNamed(AppPage.myMessageBusinessTyping),
              child: BuildCardWidgetBusiness(
                  imagePath: 'assets/images/profile_messeger.png',
                  textName: AppStrings.rupertLandstroom,
                  postTimingAgo: AppStrings.threeHourAgo),
            );
          },
        ),
      ),
    );
  }
}
