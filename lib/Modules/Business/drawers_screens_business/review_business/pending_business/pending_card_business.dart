import 'package:flutter/material.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/rating_star.dart';
import 'package:sizer/sizer.dart';

class PendingCardBusiness extends StatelessWidget {
  String image, personName, field, date, waiting;

  PendingCardBusiness({
    required this.image,
    required this.personName,
    required this.field,
    required this.waiting,
    required this.date,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
            height: 23.h,
            width: 90.w,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(15),
                boxShadow: [
                  BoxShadow(
                      offset: Offset(0, 5),
                      color: Colors.grey.withOpacity(0.2),
                      spreadRadius: 4.0,
                      blurRadius: 2.0)
                ]),
            child: Stack(children: [
              Positioned(
                  top: 2.h,
                  left: 5.w,
                  child: Column(
                    children: [
                      Container(
                        height: 10.h,
                        width: 10.h,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage(image), fit: BoxFit.cover)),
                      ),
                      SizedBox(
                        height: 2.h,
                      ),
                      Text(
                        personName,
                        style: black12MulishTextStyle,
                      ),
                      SizedBox(
                        height: 1.h,
                      ),
                      Text(
                        field,
                        style: grey10MulishTextStyle,
                      )
                    ],
                  )),
              Positioned(
                  top: 6.h,
                  left: 28.w,
                  child: Text(
                    waiting,
                    style: logoTextStyle,
                  )),
              Positioned(
                  bottom: 2.h,
                  right: 3.w,
                  child: Text(
                    date,
                    style: blackBoldPoppinsColor,
                  )),
            ])),
        SizedBox(
          height: 2.h,
        ),
      ],
    );
  }
}
