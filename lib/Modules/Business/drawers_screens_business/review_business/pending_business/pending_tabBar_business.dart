import 'package:flutter/material.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Modules/Business/drawers_screens_business/review_business/pending_business/pending_card_business.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/review/pending/pending_card.dart';

class PendingTabBarBusiness extends StatelessWidget {
  const PendingTabBarBusiness({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: 10,
        itemBuilder: (context, int index) {
          return PendingCardBusiness(
            image: 'assets/images/profile_alex.png',
            personName: AppStrings.tomLatham,
            field: AppStrings.webDesigner,
            waiting: AppStrings.waitingOnReview,
            date: AppStrings.twoMarch2022,
          );
        });
  }
}
