import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ReviewControllerBusiness extends GetxController {
  TextEditingController commentTextEditingController = TextEditingController();
  TextEditingController codeTypingTextEditingController =
      TextEditingController();
  RxBool isReviewMessageVisible = false.obs;
  bool valueFirst = false;
  int honestValue = 3;
  int workValue = 3;
  int technicalSkillValue = 3;
  int punctualValue = 3;
  int communicationValue = 3;

  void showReviewMessage() {
    isReviewMessageVisible.value = !isReviewMessageVisible.value;
    update();
  }
}
