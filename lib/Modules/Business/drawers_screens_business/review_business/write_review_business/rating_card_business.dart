import 'package:AwesomeSkills/Modules/Business/drawers_screens_business/review_business/write_review_business/write_review_business_controller.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Business/drawers_screens_business/review_business/review_controller_business.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/review/review_controller.dart';
import 'package:rating_bar_flutter/rating_bar_flutter.dart';
import 'package:sizer/sizer.dart';

class RatingCommentCardBusiness extends StatelessWidget {
  final writeReviewBusinessController =
      Get.put(WriteReviewBusinessController());

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
          padding: EdgeInsets.symmetric(horizontal: 2.w, vertical: 2.h),
          height: 35.h,
          width: 90.w,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15.0),
              boxShadow: [
                BoxShadow(
                    offset: Offset(0, 5),
                    color: Colors.grey.withOpacity(0.2),
                    spreadRadius: 2.0,
                    blurRadius: 4.0)
              ]),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  AppStrings.yourRatings,
                  style: blackBold16Color,
                ),
                GetBuilder<WriteReviewBusinessController>(
                  builder: (_) {
                    return RatingBarFlutter(
                      onRatingChanged: (rating) =>
                          writeReviewBusinessController.ratingFunction(rating),
                      filledIcon: Icons.star,
                      emptyIcon: Icons.star,
                      halfFilledIcon: Icons.star_half,
                      isHalfAllowed: true,
                      aligns: Alignment.centerLeft,
                      filledColor: Colors.amberAccent,
                      emptyColor: Colors.grey,
                      halfFilledColor: Colors.amberAccent,
                      size: 12.sp,
                    );
                  },
                )
              ],
            ),
            SizedBox(
              height: 1.h,
            ),
            Text(
              AppStrings.yourComment,
              style: blackBold16Color,
            ),
            SizedBox(
              height: 1.h,
            ),
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: 1.w,
              ),
              height: 15.h,
              width: 85.w,
              decoration: const BoxDecoration(
                color: AppColors.whiteGrey2Color,
              ),
              child: TextFormField(
                controller:
                    writeReviewBusinessController.commentTextEditingController,
                decoration: const InputDecoration(
                  border: InputBorder.none,
                ),
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            Text(
              AppStrings.maximumLength,
              style: blackBold16Color,
            )
          ])),
    );
  }
}
