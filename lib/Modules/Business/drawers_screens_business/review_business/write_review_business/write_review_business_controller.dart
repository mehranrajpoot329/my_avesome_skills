import 'package:flutter/material.dart';
import 'package:get/get.dart';

class WriteReviewBusinessController extends GetxController {
  TextEditingController commentTextEditingController = TextEditingController();
  double _ratingStar = 0;

  ratingFunction(rating) {
    _ratingStar = rating;
    update();
  }
}
