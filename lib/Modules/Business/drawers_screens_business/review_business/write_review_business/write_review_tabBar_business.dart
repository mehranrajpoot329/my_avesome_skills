import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Business/drawers_screens_business/review_business/review_controller_business.dart';
import 'package:AwesomeSkills/Modules/Business/drawers_screens_business/review_business/write_review_business/rating_card_business.dart';
import 'package:AwesomeSkills/Modules/Business/drawers_screens_business/review_business/write_review_business/slider_info_card_business.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/review/write_review/rating_card.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/review/review_controller.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/review/write_review/slider_info_card.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:sizer/sizer.dart';

class WriteReviewTaBarBusiness extends StatefulWidget {
  @override
  State<WriteReviewTaBarBusiness> createState() =>
      _WriteReviewTaBarBusinessState();
}

class _WriteReviewTaBarBusinessState extends State<WriteReviewTaBarBusiness> {
  final controller = Get.put(ReviewControllerBusiness());

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 2.w),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 3.h,
            ),
            const Text(
              AppStrings.writeAReview,
              style: blue600ColorTextStyle,
            ),
            SizedBox(
              height: 2.h,
            ),
            RatingCommentCardBusiness(),
            SizedBox(
              height: 2.h,
            ),
            SliderInfoCardBusiness(),
            SizedBox(
              height: 2.h,
            ),
            const FittedBox(
              child: Text(
                AppStrings.typeCharacterYouSee,
                style: lightBlueTextStyle,
              ),
            ),
            SizedBox(
              height: 1.h,
            ),
            Row(
              children: [
                Container(
                  height: 43,
                  width: 124,
                  decoration: BoxDecoration(
                    color: AppColors.lightBlueColor.withOpacity(0.1),
                    borderRadius: BorderRadius.circular(2),
                  ),
                  child: const Center(
                    child: Text(
                      AppStrings.tripleSevenTripleNine,
                      style: lightBlueTextStyle,
                    ),
                  ),
                ),
                SizedBox(
                  width: 2.w,
                ),
                Container(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  height: 43,
                  width: 124,
                  decoration: BoxDecoration(
                    color: AppColors.lightBlueColor.withOpacity(0.1),
                    borderRadius: BorderRadius.circular(2),
                  ),
                  child: TextFormField(
                    controller: controller.codeTypingTextEditingController,
                    decoration: const InputDecoration(border: InputBorder.none),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 2.h,
            ),
            SizedBox(
              width: 100.w,
              child: Row(
                children: [
                  Checkbox(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(3)),
                    side:
                        const BorderSide(color: AppColors.blueColor, width: 2),
                    checkColor: Colors.white,
                    activeColor: AppColors.blueColor,
                    value: controller.valueFirst,
                    onChanged: (bool? value) {
                      setState(() {
                        controller.valueFirst = value!;
                      });
                    },
                  ),
                  const Expanded(
                    child: Text(
                      AppStrings.iAccept,
                      style: TextStyle(
                        fontFamily: 'poppins',
                        fontSize: 12,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            CustomButton(
                buttonName: AppStrings.postYourReview,
                width: 50.w,
                height: 5.h,
                buttonColor: AppColors.lightBlueColor,
                textStyle: customButtonWhiteTextStyle,
                borderRadius: BorderRadius.circular(2),
                onTap: () {}),
            SizedBox(
              height: 2.h,
            )
          ],
        ),
      ),
    );
  }
}
