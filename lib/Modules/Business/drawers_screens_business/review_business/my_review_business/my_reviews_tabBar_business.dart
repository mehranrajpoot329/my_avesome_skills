import 'package:flutter/material.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Modules/Business/drawers_screens_business/review_business/my_review_business/my_review_card_business.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/review/my_review/my_review_card.dart';

class MyReviewTabBarBusiness extends StatelessWidget {
  const MyReviewTabBarBusiness({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: 10,
        itemBuilder: (context, int index) {
          return ReviewCardBusiness(
            image: 'assets/images/profile_alex.png',
            personName: AppStrings.tomLatham,
            field: AppStrings.webDesigner,
            detail: AppStrings.easyToWork,
            date: AppStrings.twoMarch2022,
          );
        });
  }
}
