import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Business/drawers_screens_business/review_business/my_review_business/my_reviews_tabBar_business.dart';
import 'package:AwesomeSkills/Modules/Business/drawers_screens_business/review_business/review_controller_business.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:sizer/sizer.dart';

import '../../custom_scroll_appBar_business/nested_business_scroll_widget.dart';
import 'pending_business/pending_tabBar_business.dart';
import 'write_review_business/write_review_tabBar_business.dart';

class ReviewScreenBusiness extends StatelessWidget {
  final reviewBusinessController = Get.put(ReviewControllerBusiness());

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: NestedBusinessScroll(
        floatingActionButton: WidgetMethod.floatingActionButton(context),
        body: Padding(
            padding: EdgeInsets.only(left: 2.w, right: 2.w, top: 2.h),
            child: Column(children: [
              Row(
                children: [
                  GestureDetector(
                    onTap: () => Get.back(),
                    child: Icon(
                      Icons.arrow_back_ios_new,
                      color: Colors.black,
                      size: 3.h,
                    ),
                  ),
                  SizedBox(
                    width: 5.w,
                  ),
                  const Text(
                    AppStrings.reviews,
                    style: montserrat20TextStyle,
                  ),
                ],
              ),
              Obx(
                () => Visibility(
                  visible:
                      reviewBusinessController.isReviewMessageVisible.value,
                  child: Padding(
                    padding: EdgeInsets.only(left: 10.w),
                    child: const Text(
                      AppStrings.leaveFeedback,
                      style: grey14Color,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
              Container(
                height: 16.h,
                padding: EdgeInsets.only(left: 2.w, right: 2.w, top: 0.5.h),
                decoration: BoxDecoration(
                  color: AppColors.lightGreen2Color,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left: 3.w),
                          child: Text(
                            AppStrings.reviews,
                            style: orange600ColorTextStyle,
                          ),
                        ),
                        IconButton(
                          onPressed: () =>
                              reviewBusinessController.showReviewMessage(),
                          icon: Icon(
                            Icons.info_rounded,
                            size: 4.h,
                          ),
                          color: AppColors.lightBlueColor,
                        )
                      ],
                    ),
                    const TabBar(
                        isScrollable: true,
                        indicatorSize: TabBarIndicatorSize.label,
                        indicatorColor: AppColors.lightBlueColor,
                        labelColor: Colors.black,
                        labelStyle: TextStyle(fontWeight: FontWeight.bold),
                        unselectedLabelStyle: TextStyle(
                            fontWeight: FontWeight.w400, color: Colors.black),
                        tabs: [
                          Tab(
                            text: AppStrings.myReview,
                          ),
                          Tab(
                            text: AppStrings.pending,
                          ),
                          Tab(
                            text: AppStrings.writeReview,
                          ),
                        ]),
                  ],
                ),
              ),
              Expanded(
                child: TabBarView(
                    physics: NeverScrollableScrollPhysics(),
                    children: [
                      const MyReviewTabBarBusiness(),
                      const PendingTabBarBusiness(),
                      WriteReviewTaBarBusiness(),
                    ]),
              ),
            ])),
      ),
    );
  }
}
