import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/routes/app_pages.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:sizer/sizer.dart';

import '../../custom_scroll_appBar_business/nested_business_scroll_widget.dart';

class MyTaskScreenBusiness extends StatelessWidget {
  const MyTaskScreenBusiness({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NestedBusinessScroll(
      floatingActionButton: WidgetMethod.floatingActionButton(context),
      body: Padding(
        padding: EdgeInsets.only(left: 3.w, right: 2.w),
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            Row(
              children: [
                GestureDetector(
                  onTap: () => Get.back(),
                  child: Icon(
                    Icons.arrow_back_ios_new,
                    color: Colors.black,
                    size: 3.h,
                  ),
                ),
                SizedBox(
                  width: 5.w,
                ),
                const Text(
                  AppStrings.myTask,
                  style: montserrat20TextStyle,
                ),
              ],
            ),
            const FittedBox(
              child: Text(
                AppStrings.helloAgain,
                style: poppins36ColorTextStyle,
              ),
            ),
            Row(
              children: [
                Text(
                  AppStrings.youHave,
                  style: blackBold16Color,
                ),
                const Text(
                  AppStrings.five,
                  style: greenBold16Color,
                ),
                SizedBox(
                  width: 1.w,
                ),
                Text(
                  AppStrings.taskToFinish,
                  style: blackBold16Color,
                ),
              ],
            ),
            SizedBox(
              height: 2.h,
            ),
            const Align(
              alignment: Alignment.center,
              child: Text(
                AppStrings.toStart,
                style: logoTextStyle,
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            GestureDetector(
              onTap: () => Get.toNamed(AppPage.myTaskToStartBusiness),
              child: MyTaskListBusiness(
                isCompleted: false,
                cardColor: AppColors.blue13Color,
                textName: AppStrings.releaseProject,
                image: 'assets/images/profile_task.png',
                textDetail: AppStrings.updateContractorAgreement,
                textDetailColor: AppColors.lightBlueColor,
                flagImage: 'assets/images/post_icon/yellow_flag.svg',
              ),
            ),
            SizedBox(
              height: 9.h,
            ),
            const Align(
              alignment: Alignment.center,
              child: Text(
                AppStrings.inProgress,
                style: orangeColor18TextStyle,
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            GestureDetector(
              onTap: () => Get.toNamed(AppPage.myTaskInProgressBusiness),
              child: MyTaskListBusiness(
                  isCompleted: false,
                  cardColor: AppColors.lightYellowColor,
                  textName: AppStrings.releaseProject,
                  image: 'assets/images/profile_task_progress.png',
                  textDetail: AppStrings.updateContractorAgreement,
                  textDetailColor: AppColors.redColor,
                  flagImage: 'assets/images/post_icon/red_flag.svg'),
            ),
            SizedBox(
              height: 9.h,
            ),
            const Align(
              alignment: Alignment.center,
              child: Text(
                AppStrings.completed,
                style: greenColor18TextStyle,
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            GestureDetector(
              onTap: () => Get.toNamed(AppPage.myTaskCompletedBusiness),
              child: MyTaskListBusiness(
                  isCompleted: true,
                  cardColor: AppColors.green8Color,
                  textName: AppStrings.releaseProject,
                  image: 'assets/images/profile_task_complete.png',
                  textDetail: AppStrings.updateContractorAgreement,
                  textDetailColor: AppColors.green7Color,
                  flagImage: 'assets/images/post_icon/red_flag.svg'),
            ),
            SizedBox(
              height: 10.h,
            ),
          ],
        ),
      ),
    );
  }
}

class MyTaskListBusiness extends StatelessWidget {
  String textName, image, flagImage, textDetail;
  Color cardColor, textDetailColor;
  bool isCompleted = true;

  MyTaskListBusiness(
      {required this.cardColor,
      required this.textName,
      required this.image,
      required this.textDetail,
      required this.textDetailColor,
      required this.flagImage,
      required this.isCompleted});

  @override
  Widget build(BuildContext context) {
    return Stack(
      clipBehavior: Clip.none,
      children: [
        Positioned(
            child: buildMyTaskCard(
                isCompleted: isCompleted,
                context: context,
                textName: textName,
                image: image,
                flagImage: flagImage,
                textDetail: textDetail,
                cardColor: cardColor,
                textDetailColor: textDetailColor)),
        Positioned(
          top: 2.h,
          right: 4.w,
          child: buildMyTaskCard(
              isCompleted: isCompleted,
              context: context,
              textName: textName,
              image: image,
              flagImage: flagImage,
              textDetail: textDetail,
              cardColor: cardColor,
              textDetailColor: textDetailColor),
        ),
        Positioned(
          top: 4.h,
          right: 8.w,
          child: buildMyTaskCard(
              isCompleted: isCompleted,
              context: context,
              textName: textName,
              image: image,
              flagImage: flagImage,
              textDetail: textDetail,
              cardColor: cardColor,
              textDetailColor: textDetailColor),
        ),
        Positioned(
          top: 6.h,
          right: 12.w,
          child: buildMyTaskCard(
              isCompleted: isCompleted,
              context: context,
              textName: textName,
              image: image,
              flagImage: flagImage,
              textDetail: textDetail,
              cardColor: cardColor,
              textDetailColor: textDetailColor),
        ),
      ],
    );
  }

  Widget buildMyTaskCard(
      {required BuildContext context,
      required String textName,
      required String image,
      required String flagImage,
      required String textDetail,
      required Color cardColor,
      required Color textDetailColor,
      required isCompleted}) {
    return Align(
      alignment: Alignment.bottomRight,
      child: Padding(
        padding: const EdgeInsets.only(right: 5.0),
        child: Container(
          padding: const EdgeInsets.only(left: 20, top: 20, right: 20),
          height: 170,
          width: MediaQuery.of(context).size.width * 0.8,
          decoration: BoxDecoration(
              color: cardColor,
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                    offset: Offset(0, 5),
                    blurRadius: 4.0,
                    spreadRadius: 2.0,
                    color: Colors.grey.withOpacity(0.2)),
              ]),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    textName,
                    style: black12MulishTextStyle,
                  ),
                  Container(
                    height: 50,
                    width: 50,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            image: AssetImage(image), fit: BoxFit.cover)),
                  ),
                ],
              ),
              Text(
                textDetail,
                style: TextStyle(
                    color: textDetailColor,
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    fontFamily: 'mulish'),
              ),
              const SizedBox(
                height: 20,
              ),
              isCompleted
                  ? Container()
                  : SvgPicture.asset(
                      flagImage,
                      height: 3.h,
                    )
            ],
          ),
        ),
      ),
    );
  }
}
