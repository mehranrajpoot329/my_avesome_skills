import 'package:AwesomeSkills/Modules/Personal/drawers_screens/my_task/my_task_completed/my_task_completed_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Modules/Business/custom_scroll_appBar_business/nested_business_scroll_widget.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/my_task/my_task_to_start/my_task_to_start.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:sizer/sizer.dart';

import '../../../../../constants/app_colors.dart';
import '../../../../../constants/app_strings.dart';
import '../../../../../constants/text_style.dart';

class MyTaskCompletedBusiness extends StatelessWidget {
  final taskCompletedController = Get.put(MyTaskCompletedController());

  @override
  Widget build(BuildContext context) {
    return NestedBusinessScroll(
      floatingActionButton: Stack(
        clipBehavior: Clip.none,
        children: [
          Positioned(
            right: 0.w,
            bottom: -2.h,
            child: Obx(
              () => Row(
                children: [
                  taskCompletedController.isCardIndex.value
                      ? GestureDetector(
                          onTap: () => taskCompletedController.scrollItemUp(),
                          child: CircleAvatar(
                            radius: 20,
                            backgroundColor: AppColors.iconVisibleColor,
                            child: Icon(
                              Icons.arrow_upward_rounded,
                              color: Colors.white,
                            ),
                          ))
                      : GestureDetector(
                          onTap: () => taskCompletedController.scrollItemDown(),
                          child: CircleAvatar(
                            radius: 20,
                            backgroundColor: AppColors.iconVisibleColor,
                            child: Icon(
                              Icons.arrow_downward_rounded,
                              color: Colors.white,
                            ),
                          )),
                  SizedBox(
                    width: 70.w,
                  ),
                  GestureDetector(
                    onTap: () => WidgetMethod.bottomSheet(context),
                    child: FittedBox(
                      child: SvgPicture.asset(
                        'assets/images/micMan.svg',
                        height: 23.sp,
                        width: 23.sp,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
      body: Padding(
        padding: EdgeInsets.only(left: 3.w, right: 2.w),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          SizedBox(
            height: 1.h,
          ),
          Row(
            children: [
              GestureDetector(
                onTap: () => Get.back(),
                child: Icon(
                  Icons.arrow_back_ios_new,
                  color: Colors.black,
                  size: 3.h,
                ),
              ),
              SizedBox(
                width: 5.w,
              ),
              const Text(
                AppStrings.myTask,
                style: montserrat20TextStyle,
              ),
            ],
          ),
          const FittedBox(
            child: Text(
              AppStrings.helloAgain,
              style: poppins36ColorTextStyle,
            ),
          ),
          Row(
            children: [
              Text(
                AppStrings.youHave,
                style: blackBold16Color,
              ),
              const Text(
                AppStrings.five,
                style: greenBold16Color,
              ),
              SizedBox(
                width: 1.w,
              ),
              Text(
                AppStrings.taskToFinish,
                style: blackBold16Color,
              ),
            ],
          ),
          SizedBox(
            height: 2.h,
          ),
          const Align(
            alignment: Alignment.center,
            child: Text(
              AppStrings.completed,
              style: greenColor18TextStyle,
            ),
          ),
          Expanded(
            child: ScrollablePositionedList.separated(
              itemScrollController: taskCompletedController.itemController,
              itemCount: taskCompletedController.itemCount.value,
              itemBuilder: (BuildContext context, int index) {
                return BuildMyTaskToStartCard(
                    isCompleted: true,
                    cardColor: AppColors.green8Color,
                    textName: AppStrings.releaseProject,
                    image: 'assets/images/profile_task_complete.png',
                    textDetail: AppStrings.updateContractorAgreement,
                    textDetailColor: AppColors.green7Color,
                    flagImage: 'assets/images/post_icon/red_flag.svg');
              },
              separatorBuilder: (BuildContext context, int index) {
                return SizedBox(
                  height: 2.h,
                );
              },
            ),
          ),
        ]),
      ),
    );
  }
}
