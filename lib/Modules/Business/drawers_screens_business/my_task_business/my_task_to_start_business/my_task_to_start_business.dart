import 'package:AwesomeSkills/Modules/Personal/drawers_screens/my_task/my_task_to_start/my_task_to_start_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Modules/Personal/drawers_screens/my_task/my_task_list_card.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:scrollable_positioned_list/scrollable_positioned_list.dart';
import 'package:sizer/sizer.dart';

import '../../../../../Constants/app_strings.dart';
import '../../../../../Constants/text_style.dart';
import '../../../custom_scroll_appBar_business/nested_business_scroll_widget.dart';

class MyTaskToStartBusiness extends StatelessWidget {
  final taskStartController = Get.put(MyTaskToStartController());

  @override
  Widget build(BuildContext context) {
    return NestedBusinessScroll(
      floatingActionButton: Stack(
        clipBehavior: Clip.none,
        children: [
          Positioned(
            right: 0.w,
            bottom: -2.h,
            child: Obx(
              () => Row(
                children: [
                  taskStartController.isCardIndex.value
                      ? GestureDetector(
                          onTap: () => taskStartController.scrollItemUp(),
                          child: CircleAvatar(
                            radius: 20,
                            backgroundColor: AppColors.iconVisibleColor,
                            child: Icon(
                              Icons.arrow_upward_rounded,
                              color: Colors.white,
                            ),
                          ))
                      : GestureDetector(
                          onTap: () => taskStartController.scrollItemDown(),
                          child: CircleAvatar(
                            radius: 20,
                            backgroundColor: AppColors.iconVisibleColor,
                            child: Icon(
                              Icons.arrow_downward_rounded,
                              color: Colors.white,
                            ),
                          )),
                  SizedBox(
                    width: 70.w,
                  ),
                  GestureDetector(
                    onTap: () => WidgetMethod.bottomSheet(context),
                    child: FittedBox(
                      child: SvgPicture.asset(
                        'assets/images/micMan.svg',
                        height: 23.sp,
                        width: 23.sp,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
      body: Padding(
        padding: EdgeInsets.only(left: 3.w, right: 2.w),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          SizedBox(
            height: 1.h,
          ),
          Row(
            children: [
              GestureDetector(
                onTap: () => Get.back(),
                child: Icon(
                  Icons.arrow_back_ios_new,
                  color: Colors.black,
                  size: 3.h,
                ),
              ),
              SizedBox(
                width: 5.w,
              ),
              const Text(
                AppStrings.myTask,
                style: montserrat20TextStyle,
              ),
            ],
          ),
          const FittedBox(
            child: Text(
              AppStrings.helloAgain,
              style: poppins36ColorTextStyle,
            ),
          ),
          Row(
            children: [
              Text(
                AppStrings.youHave,
                style: blackBold16Color,
              ),
              const Text(
                AppStrings.five,
                style: greenBold16Color,
              ),
              SizedBox(
                width: 1.w,
              ),
              Text(
                AppStrings.taskToFinish,
                style: blackBold16Color,
              ),
            ],
          ),
          SizedBox(
            height: 2.h,
          ),
          const Align(
            alignment: Alignment.center,
            child: Text(
              AppStrings.toStart,
              style: logoTextStyle,
            ),
          ),
          Expanded(
            child: ScrollablePositionedList.separated(
              itemScrollController: taskStartController.itemController,
              itemCount: taskStartController.itemCount.value,
              itemBuilder: (BuildContext context, int index) {
                return BuildMyTaskToStartCard(
                  isCompleted: false,
                  cardColor: AppColors.blue13Color,
                  textName: AppStrings.releaseProject,
                  image: 'assets/images/profile_task.png',
                  textDetail: AppStrings.updateContractorAgreement,
                  textDetailColor: AppColors.lightBlueColor,
                  flagImage: 'assets/images/post_icon/yellow_flag.svg',
                );
              },
              separatorBuilder: (BuildContext context, int index) {
                return SizedBox(
                  height: 3.h,
                );
              },
            ),
          ),
        ]),
      ),
    );
  }
}

class BuildMyTaskToStartCard extends StatelessWidget {
  String textName, image, flagImage, textDetail;
  Color cardColor, textDetailColor;
  bool isCompleted;

  BuildMyTaskToStartCard(
      {required this.textName,
      required this.image,
      required this.flagImage,
      required this.textDetail,
      required this.cardColor,
      required this.textDetailColor,
      required this.isCompleted});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 20, top: 20, right: 20),
      height: 170,
      width: MediaQuery.of(context).size.width * 0.8,
      decoration: BoxDecoration(
          color: cardColor,
          borderRadius: BorderRadius.circular(20),
          boxShadow: [
            BoxShadow(
                offset: const Offset(0, 5),
                blurRadius: 4.0,
                spreadRadius: 2.0,
                color: Colors.grey.withOpacity(0.2)),
          ]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                textName,
                style: black12MulishTextStyle,
              ),
              Container(
                height: 50,
                width: 50,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: AssetImage(image), fit: BoxFit.cover)),
              ),
            ],
          ),
          Text(
            textDetail,
            style: TextStyle(
                color: textDetailColor,
                fontSize: 16,
                fontWeight: FontWeight.w500,
                fontFamily: 'mulish'),
          ),
          const SizedBox(
            height: 20,
          ),
          isCompleted
              ? Container()
              : SvgPicture.asset(
                  flagImage,
                  height: 3.h,
                )
        ],
      ),
    );
  }
}
