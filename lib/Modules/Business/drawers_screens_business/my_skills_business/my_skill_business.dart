import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Business/custom_scroll_appBar_business/nested_business_scroll_widget.dart';
import 'package:AwesomeSkills/Modules/Business/drawers_screens_business/my_skills_business/my_skills_business_controller.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:AwesomeSkills/widgets/button/custom_icon_button.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/image_slider_myskills_page_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/transport_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:AwesomeSkills/widgets/drop_down.dart';
import 'package:sizer/sizer.dart';

class MySkillsScreenBusiness extends StatelessWidget {
  final controller = Get.put(MySkillsBusinessController());

  @override
  Widget build(BuildContext context) {
    return NestedBusinessScroll(
        floatingActionButton: WidgetMethod.floatingActionButton(context),
        body: Padding(
            padding: EdgeInsets.only(left: 2.w, right: 2.w),
            child: SingleChildScrollView(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                  SizedBox(
                    height: 2.h,
                  ),
                  Row(
                    children: [
                      GestureDetector(
                        onTap: () => Get.back(),
                        child: Icon(
                          Icons.arrow_back_ios_new,
                          color: Colors.black,
                          size: 3.h,
                        ),
                      ),
                      SizedBox(
                        width: 5.w,
                      ),
                      const Text(
                        AppStrings.mySkills,
                        style: montserrat20TextStyle,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 4.w, right: 2.w, top: 0.5.h),
                    height: 14.5.h,
                    width: 100.w,
                    decoration: BoxDecoration(
                      color: AppColors.lightGreen2Color,
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Text(
                          AppStrings.inSeasonPTYLTD,
                          style: orangeColor600TextStyle,
                        ),
                        Text(
                          AppStrings.joinedIn2007,
                          style: blackBold16Color,
                        ),
                        Text(
                          AppStrings.adminFlorinBelbe,
                          style: blackBold16Color,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 3.h,
                  ),
                  Text(
                    AppStrings.industry,
                    style: black500TextStyle,
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  CustomDropDown(
                    height: 7.h,
                    width: 100.w,
                    hint: AppStrings.asap,
                    items: const <String>[
                      'ASAP',
                      '1 Month',
                      '2 Month',
                      '3 Month',
                    ],
                    selectedValue: (value) {
                      controller.industry.value = value!;
                      log(value.toString());
                    },
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Center(
                    child: Container(
                        padding: EdgeInsets.symmetric(
                            horizontal: 4.w, vertical: 2.h),
                        height: 40.h,
                        width: 95.w,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [
                              BoxShadow(
                                offset: Offset(0, 5),
                                color: Colors.grey.withOpacity(0.1),
                                blurRadius: 1.0,
                                spreadRadius: 1.0,
                              )
                            ]),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const FittedBox(
                                child: Text(
                                  AppStrings.selectIndustries,
                                  style: blue500ColorTextStyle,
                                ),
                              ),
                              const Divider(
                                color: AppColors.lightBlueColor,
                              ),
                              Expanded(
                                  child: Scrollbar(
                                thickness: 5,
                                radius: const Radius.circular(5),
                                thumbVisibility: false,
                                child: ListView(
                                  padding: EdgeInsets.zero,
                                  children: [
                                    InkWell(
                                      onTap: () => controller
                                          .changeCheckBoxSelectIndustries(
                                              controller.selectAll),
                                      child: Row(
                                        children: [
                                          GetBuilder<
                                              MySkillsBusinessController>(
                                            builder: (_) {
                                              return Checkbox(
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            5)),
                                                side: const BorderSide(
                                                    color: AppColors
                                                        .lightBlueColor,
                                                    width: 0.5),
                                                checkColor: Colors.white,
                                                activeColor:
                                                    AppColors.lightBlueColor,
                                                value:
                                                    controller.selectAll.value,
                                                onChanged: (value) => controller
                                                    .changeCheckBoxSelectIndustries(
                                                        controller.selectAll),
                                              );
                                            },
                                          ),
                                          SizedBox(
                                            width: 3.w,
                                          ),
                                          Text(
                                            controller.selectAll.title,
                                            style: blackBold16Color,
                                          ),
                                        ],
                                      ),
                                    ),
                                    ...controller.checkBoxList.map(
                                      (item) => InkWell(
                                        onTap: () => controller
                                            .changeCheckBoxSelectItemIndustries(
                                                item),
                                        child: Row(
                                          children: [
                                            GetBuilder<
                                                MySkillsBusinessController>(
                                              builder: (_) {
                                                return Checkbox(
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5)),
                                                  side: const BorderSide(
                                                      color: AppColors
                                                          .lightBlueColor,
                                                      width: 0.5),
                                                  checkColor: Colors.white,
                                                  activeColor:
                                                      AppColors.lightBlueColor,
                                                  value: item.value,
                                                  onChanged: (value) => controller
                                                      .changeCheckBoxSelectItemIndustries(
                                                          item),
                                                );
                                              },
                                            ),
                                            SizedBox(
                                              width: 3.w,
                                            ),
                                            Text(
                                              item.title,
                                              style: blackBold16Color,
                                            ),
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ))
                            ])),
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: const [
                      Text(
                        AppStrings.myAwesomeSkillText,
                        style: blue600ColorTextStyle,
                      ),
                      Text(
                        AppStrings.editSkill,
                        style: inter16TextStyle,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  const Text(
                    AppStrings.top3Skills,
                    style: inter14TextStyle,
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 20.w),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Expanded(
                          child: FittedBox(
                            child: Text(
                              AppStrings.webDesign,
                              style: black600TextStyle,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 5.w,
                        ),
                        Expanded(
                          child: Text(
                            AppStrings.twentyFive,
                            maxLines: 1,
                            softWrap: false,
                            overflow: TextOverflow.fade,
                            style: black500TextStyle,
                          ),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 20.w),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Expanded(
                          child: Text(
                            AppStrings.python,
                            style: black600TextStyle,
                          ),
                        ),
                        SizedBox(
                          width: 5.w,
                        ),
                        Expanded(
                          child: Text(
                            AppStrings.forty,
                            maxLines: 1,
                            softWrap: false,
                            overflow: TextOverflow.fade,
                            style: black500TextStyle,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 20.w),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const Expanded(
                          child: Text(
                            AppStrings.uIDesign,
                            style: black600TextStyle,
                          ),
                        ),
                        SizedBox(
                          width: 5.w,
                        ),
                        Expanded(
                          child: Text(
                            AppStrings.thirty,
                            maxLines: 1,
                            softWrap: false,
                            overflow: TextOverflow.fade,
                            style: black500TextStyle,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  CustomIconButton(
                      buttonName: AppStrings.addMoreSkills,
                      width: 45.w,
                      height: 6.h,
                      buttonColor: AppColors.indigoColor,
                      textStyle: customButtonTextStyle,
                      borderRadius: BorderRadius.circular(10),
                      onTap: () => controller.showInfoSkillDetail()),
                  SizedBox(
                    height: 2.h,
                  ),
                  GetBuilder<MySkillsBusinessController>(
                    builder: (_) {
                      return Visibility(
                        visible: controller.isSkillVisible,
                        child: Padding(
                          padding: EdgeInsets.only(left: 2.w, right: 2.w),
                          child: Column(
                            children: [
                              TextFormField(
                                controller: controller.nameSkillController,
                                decoration: InputDecoration(
                                  contentPadding:
                                      EdgeInsets.only(left: 2.w, right: 2.w),
                                  hintText: AppStrings.nameOfSkills,
                                  hintStyle: labelTextStyle,
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                ),
                              ),
                              SizedBox(
                                height: 2.h,
                              ),
                              TextFormField(
                                controller: controller.priceSkillController,
                                decoration: InputDecoration(
                                  contentPadding:
                                      EdgeInsets.only(left: 2.w, right: 2.w),
                                  hintText: AppStrings.enterYourPrice,
                                  hintStyle: labelTextStyle,
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                ),
                              ),
                              SizedBox(
                                height: 3.h,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  CustomButton(
                                      buttonName: AppStrings.create,
                                      width: 25.w,
                                      height: 4.h,
                                      buttonColor: AppColors.indigoColor,
                                      textStyle: customButtonTextStyle,
                                      borderRadius: BorderRadius.circular(7),
                                      onTap: () {}),
                                  SizedBox(
                                    width: 2.w,
                                  ),
                                  CustomButton(
                                      buttonName: AppStrings.cancel,
                                      width: 25.w,
                                      height: 4.h,
                                      buttonColor: AppColors.grey5Color,
                                      textStyle: customButtonTextStyle,
                                      borderRadius: BorderRadius.circular(7),
                                      onTap: () {}),
                                ],
                              ),
                              SizedBox(
                                height: 1.h,
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  const Text(
                    AppStrings.language,
                    style: blue600ColorTextStyle,
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  CustomIconButton(
                    buttonName: AppStrings.addWhatOtherLanguage,
                    width: 50.w,
                    height: 6.h,
                    buttonColor: AppColors.indigoColor,
                    textStyle: customButtonTextStyle,
                    borderRadius: BorderRadius.circular(10),
                    onTap: () => controller.showInfoLanguageDetail(),
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  GetBuilder<MySkillsBusinessController>(
                    builder: (_) {
                      return Visibility(
                        visible: controller.isLanguageVisible,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 20.0, right: 20),
                          child: Column(
                            children: [
                              TextFormField(
                                controller: controller.languageController,
                                decoration: InputDecoration(
                                  contentPadding:
                                      EdgeInsets.only(left: 2.w, right: 2.w),
                                  hintText: 'language',
                                  hintStyle: labelTextStyle,
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                ),
                              ),
                              SizedBox(
                                height: 3.h,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  CustomButton(
                                      buttonName: AppStrings.create,
                                      width: 25.w,
                                      height: 4.h,
                                      buttonColor: AppColors.indigoColor,
                                      textStyle: customButtonTextStyle,
                                      borderRadius: BorderRadius.circular(7),
                                      onTap: () {}),
                                  SizedBox(
                                    width: 2.h,
                                  ),
                                  CustomButton(
                                      buttonName: AppStrings.cancel,
                                      width: 25.w,
                                      height: 4.h,
                                      buttonColor: AppColors.grey4Color,
                                      textStyle: customButtonTextStyle,
                                      borderRadius: BorderRadius.circular(7),
                                      onTap: () {}),
                                ],
                              ),
                              SizedBox(
                                height: 1.h,
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  const Text(
                    AppStrings.tradeCertificate,
                    style: blue600ColorTextStyle,
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  CustomIconButton(
                      buttonName: AppStrings.addMore,
                      width: 45.w,
                      height: 6.h,
                      buttonColor: AppColors.indigoColor,
                      textStyle: customButtonTextStyle,
                      borderRadius: BorderRadius.circular(10),
                      onTap: () => controller.showInfoCertificateDetail()),
                  SizedBox(
                    height: 1.h,
                  ),
                  GetBuilder<MySkillsBusinessController>(
                    builder: (_) {
                      return Visibility(
                        visible: controller.isCertificateVisible,
                        child: Padding(
                          padding: EdgeInsets.only(left: 2.2, right: 2.w),
                          child: Column(
                            children: [
                              TextFormField(
                                controller: controller.certificateController,
                                decoration: InputDecoration(
                                  contentPadding:
                                      EdgeInsets.only(left: 2.w, right: 2.w),
                                  hintText: 'certificate',
                                  hintStyle: labelTextStyle,
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                ),
                              ),
                              SizedBox(
                                height: 3.h,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  CustomButton(
                                      buttonName: AppStrings.create,
                                      width: 25.w,
                                      height: 4.h,
                                      buttonColor: AppColors.indigoColor,
                                      textStyle: customButtonTextStyle,
                                      borderRadius: BorderRadius.circular(7),
                                      onTap: () {}),
                                  SizedBox(
                                    width: 2.h,
                                  ),
                                  CustomButton(
                                      buttonName: AppStrings.cancel,
                                      width: 25.w,
                                      height: 4.h,
                                      buttonColor: AppColors.grey4Color,
                                      textStyle: customButtonTextStyle,
                                      borderRadius: BorderRadius.circular(7),
                                      onTap: () {}),
                                ],
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  const Text(
                    AppStrings.transport,
                    style: blue600ColorTextStyle,
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  const TransportWidget(),
                  SizedBox(
                    height: 2.h,
                  ),
                  const Text(
                    AppStrings.myWorksProject,
                    style: blue600ColorTextStyle,
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  ImageSliderMySkillPageWidget(),
                  SizedBox(
                    height: 3.h,
                  ),
                ]))));
  }
}
