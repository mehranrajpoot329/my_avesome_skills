import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Modules/Personal/ad_Screen/ad_screeb_can_help_personal/ad_screen_can_help_personal.dart';

class MySkillsBusinessController extends GetxController {
  final TextEditingController nameSkillController = TextEditingController();
  final TextEditingController priceSkillController = TextEditingController();
  final TextEditingController languageController = TextEditingController();
  final TextEditingController certificateController = TextEditingController();
  bool isSkillVisible = false;
  bool isLanguageVisible = false;
  bool isCertificateVisible = false;

  void showInfoSkillDetail() {
    isSkillVisible = !isSkillVisible;
    update();
  }

  void showInfoLanguageDetail() {
    isLanguageVisible = !isLanguageVisible;
    update();
  }

  void showInfoCertificateDetail() {
    isCertificateVisible = !isCertificateVisible;
    update();
  }

  final selectAll = CheckBoxModal(
    title: 'title',
  );
  RxString industry = ''.obs;

  List selectIndustriesList = [
    'Select All',
    'House Cleaning',
    'End of Lease Cleaning',
    'Carpet Steam Cleaning',
    'Window Cleaning',
    'Select All',
    'House Cleaning',
    'End of Lease Cleaning',
    'Carpet Steam Cleaning',
    'Window Cleaning',
  ];

  final checkBoxList = [
    CheckBoxModal(title: 'House Cleaning'),
    CheckBoxModal(title: 'End of Lease Cleaning'),
    CheckBoxModal(title: 'Carpet Steam Cleaning'),
    CheckBoxModal(title: 'Window Cleaning'),
    CheckBoxModal(title: 'Outdoor Cleaning'),
    CheckBoxModal(title: 'Office Cleaning'),
    CheckBoxModal(title: 'High Pressure Cleaning'),
    CheckBoxModal(title: 'Upholstery Cleaning'),
  ];

  changeCheckBoxSelectIndustries(CheckBoxModal checkBoxModal) {
    checkBoxModal.value = !checkBoxModal.value;
    update();
  }

  changeCheckBoxSelectItemIndustries(CheckBoxModal checkBoxModal) {
    checkBoxModal.value = !checkBoxModal.value;
    update();
  }
}
