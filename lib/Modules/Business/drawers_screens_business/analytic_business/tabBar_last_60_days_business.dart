import 'package:flutter/material.dart';

import 'build_visited_info_card_business.dart';

class TabBarLast60DaysBusiness extends StatelessWidget {
  const TabBarLast60DaysBusiness({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BuildVisitedInfoCardBusiness();
  }
}
