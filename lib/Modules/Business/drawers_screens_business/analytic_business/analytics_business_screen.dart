import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Business/custom_scroll_appBar_business/nested_business_scroll_widget.dart';
import 'package:AwesomeSkills/Modules/Business/drawers_screens_business/analytic_business/analytics_bussiness_controller.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:sizer/sizer.dart';
import 'tabBar_last_30_days_business.dart';
import 'tabBar_last_60_days_business.dart';
import 'tabBar_weekly_business.dart';

class AnalyticsScreenBusiness extends StatelessWidget {
  final analyticsBusinessController = Get.put(AnalyticsBusinessController());

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: NestedBusinessScroll(
        floatingActionButton: WidgetMethod.floatingActionButton(context),
        body: Padding(
            padding: EdgeInsets.only(left: 2.w, right: 2.w),
            child: ListView(padding: EdgeInsets.zero, children: [
              Row(
                children: [
                  GestureDetector(
                    onTap: () => Get.back(),
                    child: Icon(
                      Icons.arrow_back_ios_new,
                      color: Colors.black,
                      size: 3.h,
                    ),
                  ),
                  SizedBox(
                    width: 5.w,
                  ),
                  const Text(
                    AppStrings.analytics,
                    style: montserrat20TextStyle,
                  ),
                ],
              ),
              SizedBox(
                height: 2.h,
              ),
              Obx(
                () => Visibility(
                  visible: analyticsBusinessController
                      .isAnalyticsMessageVisible.value,
                  child: Padding(
                    padding: EdgeInsets.only(left: 10.w),
                    child: const Text(
                      AppStrings.leaveFeedback,
                      style: grey14Color,
                    ),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 2.w),
                height: 10.h,
                width: 100.w,
                decoration: BoxDecoration(
                  color: AppColors.lightGreen2Color,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      AppStrings.analytics,
                      style: orange600ColorTextStyle,
                    ),
                    IconButton(
                        onPressed: () =>
                            analyticsBusinessController.showAnalyticsMessage(),
                        icon: Icon(
                          Icons.info_rounded,
                          color: AppColors.lightBlueColor,
                          size: 4.h,
                        )),
                  ],
                ),
              ),
              SizedBox(
                height: 2.h,
              ),
              buildAnalyticsInfo(
                iconString: 'assets/images/post_icon/visible_blue.svg',
                textName: AppStrings.totalVisit,
                percentage: AppStrings.plusTwentyFive,
              ),
              SizedBox(
                height: 2.h,
              ),
              buildAnalyticsInfo(
                iconString: 'assets/images/post_icon/thumb_icon.svg',
                textName: AppStrings.newLike,
                percentage: AppStrings.plusTwentyFive,
              ),
              SizedBox(
                height: 2.h,
              ),
              const Text(
                AppStrings.statistics,
                style: blue600ColorTextStyle,
              ),
              const TabBar(
                  isScrollable: true,
                  indicatorSize: TabBarIndicatorSize.tab,
                  indicatorColor: AppColors.lightBlueColor,
                  labelColor: Colors.black,
                  labelStyle: TextStyle(fontWeight: FontWeight.bold),
                  unselectedLabelStyle: TextStyle(
                      fontWeight: FontWeight.w400, color: Colors.black),
                  tabs: [
                    Tab(
                      text: AppStrings.weekly,
                    ),
                    Tab(
                      text: AppStrings.last30Day,
                    ),
                    Tab(
                      text: AppStrings.last60Day,
                    )
                  ]),
              SizedBox(
                height: 2.h,
              ),
              SizedBox(
                height: 50.h,
                child: const TabBarView(
                    physics: NeverScrollableScrollPhysics(),
                    children: [
                      TabBarWeeklyBusiness(),
                      TabBarLast30DaysBusiness(),
                      TabBarLast60DaysBusiness(),
                    ]),
              ),
            ])),
      ),
    );
  }

  buildAnalyticsInfo({
    required String iconString,
    required String textName,
    required String percentage,
  }) {
    return Container(
      padding: EdgeInsets.only(left: 4.w, right: 2.w),
      height: 15.h,
      width: 100.w,
      decoration: BoxDecoration(
          color: Colors.transparent,
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
              color: AppColors.lightBlueColor,
              width: 1,
              style: BorderStyle.solid)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(
            height: 1.2.h,
          ),
          SvgPicture.asset(
            iconString,
            height: 20,
          ),
          SizedBox(
            height: 1.h,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                textName,
                style: blackBold600Color,
              ),
              const Text(
                AppStrings.plusTwentyFive,
                style: greenColor16TextStyle,
              ),
            ],
          ),
          SizedBox(
            height: 1.h,
          ),
          LinearPercentIndicator(
            padding: EdgeInsets.zero,
            barRadius: Radius.circular(10),
            animation: true,
            linearStrokeCap: LinearStrokeCap.roundAll,
            width: 85.w,
            lineHeight: 10.0,
            percent: 0.5,
            progressColor: AppColors.indigoColor,
            backgroundColor: AppColors.indigoColor.withOpacity(0.4),
          )
        ],
      ),
    );
  }
}
