import 'package:flutter/material.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';

import 'build_visited_info_card_business.dart';

class TabBarWeeklyBusiness extends StatelessWidget {
  const TabBarWeeklyBusiness({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BuildVisitedInfoCardBusiness();
  }
}
