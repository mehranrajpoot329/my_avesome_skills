import 'package:get/get.dart';

class AnalyticsBusinessController extends GetxController {
  RxBool isAnalyticsMessageVisible = false.obs;

  void showAnalyticsMessage() {
    isAnalyticsMessageVisible.value = !isAnalyticsMessageVisible.value;
    update();
  }
}
