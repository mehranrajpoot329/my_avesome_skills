import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';

enum ConfirmAction { Cancel, Accept }

class MyAdsControllerBusiness extends GetxController {
  bool onCheck = false;
  bool valueFirst = false;

  RxBool isLive = false.obs;
  RxBool isPaused = false.obs;
  RxBool isExpire = false.obs;

  showIsLiveVisible() {
    isLive.value = !isLive.value;
  }

  showIsPausedVisible() {
    isPaused.value = !isPaused.value;
  }

  showIsisExpiredVisible() {
    isExpire.value = !isExpire.value;
  }
}
