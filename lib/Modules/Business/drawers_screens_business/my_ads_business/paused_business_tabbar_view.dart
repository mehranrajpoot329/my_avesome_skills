import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Modules/Business/drawers_screens_business/my_ads_business/my_ads_controller_business.dart';
import 'package:AwesomeSkills/Modules/Business/landing_screen_business/Need_Help_business/need_help_card_business.dart';
import 'package:sizer/sizer.dart';

import '../../../../Constants/app_colors.dart';
import '../../../../Constants/app_strings.dart';
import '../../../../Constants/text_style.dart';
import '../../../Personal/landing_screen/Need_Help/need_help_card.dart';

class PausedBusinessTabBarView extends StatefulWidget {
  @override
  State<PausedBusinessTabBarView> createState() =>
      _PausedBusinessTabBarViewState();
}

class _PausedBusinessTabBarViewState extends State<PausedBusinessTabBarView> {
  final myAdsBusinessController = Get.put(MyAdsControllerBusiness());

  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      padding: EdgeInsets.zero,
      children: [
        Align(
          alignment: Alignment.center,
          child: ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: 4,
              itemBuilder: (context, int index) {
                return Obx(
                  () => Center(
                      child: Column(
                    children: [
                      GestureDetector(
                        onLongPress: () =>
                            myAdsBusinessController.showIsPausedVisible(),
                        child: NeedHelpBusinessCard(
                          onTap: () {},
                        ),
                      ),
                      Visibility(
                          visible: myAdsBusinessController.isPaused.value,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.play_circle_outline,
                                size: 24.sp,
                              ),
                              SizedBox(
                                width: 4.w,
                              ),
                              Icon(
                                FontAwesomeIcons.edit,
                                size: 20.sp,
                              ),
                              SizedBox(
                                width: 4.w,
                              ),
                              Icon(
                                FontAwesomeIcons.trashAlt,
                                color: AppColors.red4Color,
                                size: 20.sp,
                              ),
                            ],
                          )),
                      SizedBox(
                        height: 3.h,
                      ),
                    ],
                  )),
                );
              }),
        ),
      ],
    );
  }
}
