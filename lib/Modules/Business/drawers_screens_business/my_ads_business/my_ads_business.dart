import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Business/custom_scroll_appBar_business/nested_business_scroll_widget.dart';
import 'package:AwesomeSkills/Modules/Business/drawers_screens_business/my_ads_business/expired_business_tabbar_view.dart';
import 'package:AwesomeSkills/Modules/Business/drawers_screens_business/my_ads_business/live_business_tabbar_view.dart';
import 'package:AwesomeSkills/Modules/Business/drawers_screens_business/my_ads_business/my_ads_controller_business.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/Need_Help/need_help_card.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:sizer/sizer.dart';

import 'paused_business_tabbar_view.dart';

class MyAdsScreenBusiness extends StatefulWidget {
  @override
  State<MyAdsScreenBusiness> createState() => _MyAdsScreenBusinessState();
}

class _MyAdsScreenBusinessState extends State<MyAdsScreenBusiness> {
  final controller = Get.put(MyAdsControllerBusiness());

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: NestedBusinessScroll(
          floatingActionButton: WidgetMethod.floatingActionButton(context),
          body: Padding(
            padding: EdgeInsets.only(left: 3.w, right: 2.w),
            child: SingleChildScrollView(
              physics: NeverScrollableScrollPhysics(),
              child: Column(children: [
                SizedBox(
                  height: 1.h,
                ),
                Row(
                  children: [
                    GestureDetector(
                      onTap: () => Get.back(),
                      child: Icon(
                        Icons.arrow_back_ios_new,
                        color: Colors.black,
                        size: 3.h,
                      ),
                    ),
                    SizedBox(
                      width: 5.w,
                    ),
                    const Text(
                      AppStrings.myAds,
                      style: montserrat20TextStyle,
                    ),
                  ],
                ),
                SizedBox(
                  height: 3.h,
                ),
                Row(
                  children: [
                    Expanded(
                      flex: 3,
                      child: Container(
                        padding: EdgeInsets.only(right: 2.w, top: 1.5.h),
                        height: 13.5.h,
                        width: 100.w,
                        decoration: BoxDecoration(
                          color: AppColors.lightGreen2Color,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(left: 3.w),
                              child: Text(
                                AppStrings.myAds,
                                style: orange600ColorTextStyle,
                              ),
                            ),
                            const TabBar(
                                isScrollable: true,
                                indicatorColor: AppColors.lightBlueColor,
                                labelColor: Colors.black,
                                tabs: [
                                  Tab(
                                    text: AppStrings.live,
                                  ),
                                  Tab(
                                    text: AppStrings.paused,
                                  ),
                                  Tab(
                                    text: AppStrings.expired,
                                  ),
                                ]),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 2.w,
                    ),
                    Container(
                      height: 13.5.h,
                      width: 30.w,
                      decoration: BoxDecoration(
                        color: AppColors.green9Color,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: MaterialButton(
                        onPressed: () {},
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: const [
                            FittedBox(
                              child: Text(
                                AppStrings.post,
                                style: white22TextStyle,
                              ),
                            ),
                            FittedBox(
                              child: Text(
                                AppStrings.anAd,
                                style: white22TextStyle,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 2.h,
                ),
                SizedBox(
                  height: 110.h,
                  child: TabBarView(
                      physics: NeverScrollableScrollPhysics(),
                      children: [
                        LiveBusinessTabBarView(),
                        PausedBusinessTabBarView(),
                        ExpiredBusinessTabBarView(),
                      ]),
                ),
              ]),
            ),
          )),
    );
  }
}
