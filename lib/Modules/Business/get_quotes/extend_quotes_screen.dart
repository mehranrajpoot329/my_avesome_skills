import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'package:AwesomeSkills/Constants/app_colors.dart';

import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Business/custom_scroll_appBar_business/nested_business_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/rating_star.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:sizer/sizer.dart';

class ExtendQuotesScreen extends StatelessWidget {
  bool isLogined = true;

  @override
  Widget build(BuildContext context) {
    return NestedBusinessScroll(
        floatingActionButton: WidgetMethod.floatingActionButton(context),
        body: Padding(
            padding: EdgeInsets.only(left: 2.w, right: 2.w),
            child: SingleChildScrollView(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                  SizedBox(
                    height: 2.h,
                  ),
                  Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: 3.w, vertical: 2.5.h),
                    height: 10.h,
                    width: 100.w,
                    decoration: BoxDecoration(
                      color: AppColors.lightGreen2Color,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Text(
                      AppStrings.staceyPetCare,
                      style: orange600ColorTextStyle,
                    ),
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Center(
                    child: Container(
                      height: 200,
                      width: 100.w,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          image: const DecorationImage(
                              image: AssetImage('assets/images/cat_pic.png'),
                              fit: BoxFit.cover)),
                    ),
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 2.w),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          AppStrings.jobQuotesDollar,
                          style: blue600ColorTextStyle,
                        ),
                        SizedBox(
                            width: 33.w,
                            child: RatingStar(
                                textStyle: greyTextColor,
                                isNumberRating: true,
                                ratingNumber: '23')),
                        SizedBox(
                          height: 2.h,
                        ),
                        SizedBox(
                          width: 60.w,
                          child: Row(
                            children: [
                              Text(
                                AppStrings.responseRate,
                                style: blackTwelveColor,
                              ),
                              SizedBox(
                                width: 1.w,
                              ),
                              Text(
                                ':',
                                style: blackTwelveColor,
                              ),
                              SizedBox(
                                width: 1.w,
                              ),
                              Text(
                                AppStrings.ninetyEight,
                                style: blackTwelveColor,
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 1.h,
                        ),
                        SizedBox(
                          width: 70.w,
                          child: Row(
                            children: [
                              Text(
                                AppStrings.responseTime,
                                style: blackTwelveColor,
                              ),
                              SizedBox(
                                width: 1.w,
                              ),
                              Text(
                                ':',
                                style: blackTwelveColor,
                              ),
                              SizedBox(
                                width: 1.w,
                              ),
                              Text(
                                AppStrings.withInHalfHour,
                                style: blackTwelveColor,
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 1.h,
                        ),
                        SizedBox(
                          width: 44.w,
                          child: Row(
                            children: [
                              Text(
                                AppStrings.jobSuccess,
                                style: blackTwelveColor,
                              ),
                              SizedBox(
                                width: 1.w,
                              ),
                              Text(
                                ':',
                                style: blackTwelveColor,
                              ),
                              SizedBox(
                                width: 1.w,
                              ),
                              Text(
                                AppStrings.ninetyNine,
                                style: blackTwelveColor,
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 1.h,
                        ),
                        Row(
                          children: [
                            SvgPicture.asset(
                              'assets/images/post_icon/thumb_icon.svg',
                              height: 15.sp,
                              width: 15.sp,
                            ),
                            SizedBox(
                              width: 1.w,
                            ),
                            Text(
                              AppStrings.oneFortyTwo,
                              style: blackBoldPoppinsColor,
                            ),
                            SizedBox(
                              width: 5.w,
                            ),
                            SvgPicture.asset(
                              'assets/images/post_icon/visible_blue.svg',
                              height: 15.sp,
                              width: 15.sp,
                            ),
                            SizedBox(
                              width: 1.w,
                            ),
                            Text(
                              AppStrings.oneFortyTwo,
                              style: blackBoldPoppinsColor,
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 1.h,
                        ),
                        const Text(
                          AppStrings.hereIsYourQuotes,
                          style: blue500ColorTextStyle,
                        ),
                        SizedBox(
                          height: 1.h,
                        ),
                        Text(
                          AppStrings.petGrooming,
                          style: redColor32TextStyle,
                        ),
                        SizedBox(
                          height: 1.h,
                        ),
                        const Text(
                          AppStrings.hashZeroOne,
                          style: customLightBlue3TextStyle,
                        ),
                        SizedBox(
                          height: 1.h,
                        ),
                        Text(
                          AppStrings.hiDiane,
                          style: blackSixHundredWeightColor,
                        ),
                        Text(
                          AppStrings.tOComeOutAnd,
                          style: blackSixHundredWeightColor,
                        ),
                        SizedBox(
                          height: 1.h,
                        ),
                        const Text(
                          AppStrings.abn,
                          style: blue16ColorTextStyle,
                        ),
                        SizedBox(
                          height: 2.h,
                        ),
                        buildInfo(
                            'assets/images/post_icon/location_indigo_icon.svg',
                            AppStrings.bundallQLD),
                        SizedBox(
                          height: 2.h,
                        ),
                        buildInfo('assets/images/post_icon/mobile_icon.svg',
                            AppStrings.mobileNumber),
                        SizedBox(
                          height: 2.h,
                        ),
                        buildInfo('assets/images/post_icon/email_icon.svg',
                            AppStrings.emailSpam),
                        SizedBox(
                          height: 2.h,
                        ),
                        buildInfo('assets/images/post_icon/web_icon.svg',
                            AppStrings.webMyClean),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 3.h,
                  ),
                ]))));
  }

  Widget buildInfo(String image, String text) {
    return Row(
      children: [
        SvgPicture.asset(
          image,
          width: 6.w,
          fit: BoxFit.fitWidth,
        ),
        SizedBox(
          width: 2.w,
        ),
        Text(
          text,
          style: blackBoldPoppinsColor,
        )
      ],
    );
  }
}
