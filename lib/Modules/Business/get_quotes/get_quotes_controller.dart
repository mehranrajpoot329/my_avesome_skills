import 'package:flutter/material.dart';
import 'package:get/get.dart';

class GetQuotesController extends GetxController {
  TextEditingController jobLocatedController = TextEditingController();
  TextEditingController provideAreaSizeJobController = TextEditingController();
  TextEditingController yourNameController = TextEditingController();
  TextEditingController emailAddressController = TextEditingController();
  TextEditingController bestNumberToContact = TextEditingController();
  TextEditingController description = TextEditingController();

  RxString industry = ''.obs;

  List imageSlider = [
    'assets/images/spray_bottle.png',
    'assets/images/spray_bottle2.png',
    'assets/images/spray_bottle.png',
    'assets/images/spray_bottle2.png',
  ];

  List<Widget> generateSliderImage() {
    return imageSlider
        .map(
          (e) => Container(
            height: 100,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                image: DecorationImage(
                    image: AssetImage(e), fit: BoxFit.fitHeight)),
          ),
        )
        .toList();
  }
}
