import 'dart:developer';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Business/custom_scroll_appBar_business/nested_business_scroll_widget.dart';
import 'package:AwesomeSkills/Modules/Business/get_quotes/get_quotes_controller.dart';
import 'package:AwesomeSkills/Modules/Personal/ad_Screen/custom_ad_info_text_form_field.dart';
import 'package:AwesomeSkills/routes/app_pages.dart';

import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:AwesomeSkills/widgets/button/save_preview_post_ad_button.dart';
import 'package:AwesomeSkills/widgets/custom_bottom_appBar_navi/custom_bottom_appBar_navi.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_textfield/custom_textfield.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:AwesomeSkills/widgets/drop_down.dart';
import 'package:sizer/sizer.dart';

class GetQuotesScreen extends StatelessWidget {
  final controller = Get.put(GetQuotesController());

  @override
  Widget build(BuildContext context) {
    return NestedBusinessScroll(
      floatingActionButton: WidgetMethod.floatingActionButton(context),
      body: Padding(
          padding: EdgeInsets.only(left: 2.w, right: 2.w),
          child: SingleChildScrollView(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                SizedBox(
                  height: 2.h,
                ),
                Row(
                  children: [
                    Icon(
                      Icons.arrow_back_ios_new,
                      color: Colors.black,
                      size: 15.sp,
                    ),
                    SizedBox(
                      width: 5.w,
                    ),
                    Text(
                      AppStrings.getQuotesSmall,
                      style: montserrat20TextStyle,
                    ),
                  ],
                ),
                SizedBox(
                  height: 1.h,
                ),
                const Text(
                  AppStrings.getQuotes,
                  style: lightBlue32ColorTextStyle,
                ),
                SizedBox(
                  height: 1.h,
                ),
                Text(
                  AppStrings.industry,
                  style: blackFourteenSpTextStyle,
                ),
                SizedBox(
                  height: 1.h,
                ),
                CustomDropDown(
                  height: 7.h,
                  width: 100.w,
                  hint: AppStrings.category,
                  items: const <String>[
                    'House Cleaning',
                    'End of Lease Cleaning',
                    'Carpet Steam Cleaning',
                    'Window Cleaning',
                  ],
                  selectedValue: (value) {
                    controller.industry.value = value!;
                    log(value.toString());
                  },
                ),
                SizedBox(
                  height: 2.h,
                ),
                Text(
                  AppStrings.title,
                  style: blackFourteenSpTextStyle,
                ),
                SizedBox(
                  height: 1.h,
                ),
                CustomDropDown(
                  height: 7.h,
                  width: 100.w,
                  hint: AppStrings.category,
                  items: const <String>[
                    'House Cleaning',
                    'End of Lease Cleaning',
                    'Carpet Steam Cleaning',
                    'Window Cleaning',
                  ],
                  selectedValue: (value) {
                    controller.industry.value = value!;
                    log(value.toString());
                  },
                ),
                SizedBox(
                  height: 1.h,
                ),
                Text(
                  AppStrings.description,
                  style: blackFourteenSpTextStyle,
                ),
                SizedBox(
                  height: 1.h,
                ),
                FormBuilderTextField(
                  decoration: const InputDecoration(
                    filled: true,
                    fillColor: AppColors.lightBlue6Color,
                    border: OutlineInputBorder(),
                    hintStyle: hintTextStyle,
                  ),
                  controller: controller.description,
                  name: 'description',
                  maxLength: 200,
                  validator: (String) {},
                ),
                SizedBox(
                  height: 2.h,
                ),
                const FittedBox(
                  child: Text(
                    AppStrings.tellUsMoreAboutThe,
                    style: blue500ColorTextStyle,
                  ),
                ),
                SizedBox(
                  height: 1.h,
                ),
                CustomAdInfoTextFormField(
                    titleTextStyle: blackFourteenSpTextStyle,
                    isOptionalText: false,
                    height: 8.h,
                    width: 100.w,
                    title: AppStrings.whereIsTheJob,
                    hintText: AppStrings.enterPostCodeOrSubRub,
                    controller: controller.jobLocatedController),
                SizedBox(
                  height: 2.h,
                ),
                Text(
                  AppStrings.whenDoYouNeeTheWork,
                  style: blackFourteenSpTextStyle,
                ),
                SizedBox(
                  height: 1.h,
                ),
                CustomDropDown(
                  height: 7.h,
                  width: 100.w,
                  hint: AppStrings.asap,
                  items: const <String>[
                    'ASAP',
                    '1 Month',
                    '2 Month',
                    '3 Month',
                  ],
                  selectedValue: (value) {
                    controller.industry.value = value!;
                    log(value.toString());
                  },
                ),
                SizedBox(
                  height: 2.h,
                ),
                Text(
                  AppStrings.whatTypeOfJObs,
                  style: blackFourteenSpTextStyle,
                ),
                SizedBox(
                  height: 1.h,
                ),
                CustomDropDown(
                  height: 7.h,
                  width: 100.w,
                  hint: AppStrings.chooseCategory,
                  items: const <String>[
                    'Cleaning',
                    'Design',
                    'Website  ',
                    'App',
                  ],
                  selectedValue: (value) {
                    controller.industry.value = value!;
                    log(value.toString());
                  },
                ),
                SizedBox(
                  height: 2.h,
                ),
                Text(
                  AppStrings.isThisRepairOrInstallation,
                  style: blackFourteenSpTextStyle,
                ),
                SizedBox(
                  height: 1.h,
                ),
                CustomDropDown(
                  height: 7.h,
                  width: 100.w,
                  hint: AppStrings.chooseCategory,
                  items: const <String>[
                    'Cleaning',
                    'Design',
                    'Website  ',
                    'App',
                  ],
                  selectedValue: (value) {
                    controller.industry.value = value!;
                    log(value.toString());
                  },
                ),
                SizedBox(
                  height: 2.h,
                ),
                Text(
                  AppStrings.quotesToIncludeMaterial,
                  style: blackFourteenSpTextStyle,
                ),
                SizedBox(
                  height: 1.h,
                ),
                CustomDropDown(
                  height: 7.h,
                  width: 100.w,
                  hint: AppStrings.chooseCategory,
                  items: const <String>[
                    'Cleaning',
                    'Design',
                    'Website  ',
                    'App',
                  ],
                  selectedValue: (value) {
                    controller.industry.value = value!;
                    log(value.toString());
                  },
                ),
                SizedBox(
                  height: 2.h,
                ),
                CustomAdInfoTextFormField(
                    titleTextStyle: blackFourteenSpTextStyle,
                    isOptionalText: false,
                    height: 50,
                    width: MediaQuery.of(context).size.width - 20,
                    title: AppStrings.pleaseProvideSizeArea,
                    hintText: AppStrings.enterANumber,
                    controller: controller.provideAreaSizeJobController),
                SizedBox(
                  height: 2.h,
                ),
                Text(
                  AppStrings.canTradesManVisitTheSite,
                  style: blackFourteenSpTextStyle,
                ),
                SizedBox(
                  height: 1.h,
                ),
                CustomDropDown(
                  height: 7.h,
                  width: 100.w,
                  hint: AppStrings.chooseCategory,
                  items: const <String>[
                    'Cleaning',
                    'Design',
                    'Website  ',
                    'App',
                  ],
                  selectedValue: (value) {
                    controller.industry.value = value!;
                    log(value.toString());
                  },
                ),
                SizedBox(
                  height: 2.h,
                ),
                Text(
                  AppStrings.uploadImageOptional,
                  style: blackFourteenSpTextStyle,
                ),
                SizedBox(
                  height: 1.h,
                ),
                DottedBorder(
                  dashPattern: const [6, 2],
                  radius: const Radius.circular(10),
                  color: AppColors.indigoColor,
                  strokeWidth: 1.5,
                  child: Container(
                    height: 20.h,
                    width: 100.w,
                    decoration: const BoxDecoration(
                      color: Colors.transparent,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SvgPicture.asset(
                          'assets/images/post_icon/upload_icon.svg',
                        ),
                        const SizedBox(
                          height: 2,
                        ),
                        Text(
                          AppStrings.dragAndDrop,
                          style: blackBoldPoppinsColor,
                        ),
                        const SizedBox(
                          height: 2,
                        ),
                        Text(
                          AppStrings.or,
                          style: blackBoldPoppinsColor,
                        ),
                        const SizedBox(
                          height: 2,
                        ),
                        const Text(
                          AppStrings.clickToBrowse,
                          style: yellowPoppins300TextStyle,
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  height: 15.0,
                ),
                CarouselSlider(
                  items: controller.generateSliderImage(),
                  options: CarouselOptions(
                      enlargeCenterPage: true,
                      autoPlayCurve: Curves.fastOutSlowIn,
                      autoPlayAnimationDuration:
                          const Duration(milliseconds: 800),
                      viewportFraction: 0.8),
                ),
                SizedBox(
                  height: 2.h,
                ),
                CustomAdInfoTextFormField(
                    titleTextStyle: blackFourteenSpTextStyle,
                    isOptionalText: false,
                    height: 8.h,
                    width: 100.w,
                    title: AppStrings.yourName,
                    hintText: '',
                    controller: controller.yourNameController),
                SizedBox(
                  height: 2.h,
                ),
                CustomAdInfoTextFormField(
                    titleTextStyle: blackFourteenSpTextStyle,
                    isOptionalText: false,
                    height: 8.h,
                    width: 100.w,
                    title: AppStrings.emailAddress,
                    hintText: '',
                    controller: controller.emailAddressController),
                SizedBox(
                  height: 2.h,
                ),
                CustomAdInfoTextFormField(
                    isOptionalText: false,
                    titleTextStyle: blackFourteenSpTextStyle,
                    height: 8.h,
                    width: 100.w,
                    title: AppStrings.bestNumberToContactYou,
                    hintText: '',
                    controller: controller.bestNumberToContact),
                SizedBox(
                  height: 5.h,
                ),
                SavePreviewPostButton(
                  saveOnTap: () {},
                  previewAdOnTap: () => Get.toNamed(AppPage.extendQuotesScreen),
                  postAdOnTap: () => Get.toNamed(AppPage.listYourBusiness),
                ),
                SizedBox(
                  height: 5.h,
                ),
              ]))),
    );
  }
}
