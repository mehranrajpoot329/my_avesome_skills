import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Modules/Personal/ad_Screen/ad_screeb_can_help_personal/ad_screen_can_help_personal.dart';

class AdScreenCanHelpBusinessController extends GetxController {
  TextEditingController skillControllerOne = TextEditingController();
  TextEditingController priceControllerOne = TextEditingController();
  TextEditingController skillControllerTwo = TextEditingController();
  TextEditingController priceControllerTwo = TextEditingController();
  TextEditingController skillControllerThree = TextEditingController();
  TextEditingController priceControllerThree = TextEditingController();
  TextEditingController forAnExtraControllerOne = TextEditingController();
  TextEditingController forAnExtraControllerTwo = TextEditingController();
  TextEditingController forAnExtraControllerOneValue = TextEditingController();
  TextEditingController forAnExtraControllerTwoValue = TextEditingController();
  TextEditingController personForJobController = TextEditingController();
  TextEditingController mySkillController = TextEditingController();
  TextEditingController myEducationController = TextEditingController();
  TextEditingController postJobsController = TextEditingController();
  RxString category = ''.obs;
  bool completedOnline = false;
  bool extra = false;
  bool extraFor = false;
  RxBool isVisibleExtraField = false.obs;
  RxBool isVisibleExtraFieldTwo = false.obs;

  double traveling = 4.0;

  isShowVisibleTextField() {
    isVisibleExtraField.value = !isVisibleExtraField.value;
  }

  isShowVisibleTextFieldTwo() {
    isVisibleExtraFieldTwo.value = !isVisibleExtraFieldTwo.value;
  }

  final selectAll = CheckBoxModal(
    title: 'title',
  );
  final checkBoxList = [
    CheckBoxModal(title: 'House Cleaning'),
    CheckBoxModal(title: 'End of Lease Cleaning'),
    CheckBoxModal(title: 'Carpet Steam Cleaning'),
    CheckBoxModal(title: 'Window Cleaning'),
    CheckBoxModal(title: 'Outdoor Cleaning'),
    CheckBoxModal(title: 'Office Cleaning'),
    CheckBoxModal(title: 'High Pressure Cleaning'),
    CheckBoxModal(title: 'Upholstery Cleaning'),
  ];

  final selectIndustriesList = <String>[
    'Select All',
    'House Cleaning',
    'End of Lease Cleaning',
    'Carpet Steam Cleaning',
    'Window Cleaning',
    'Select All',
    'House Cleaning',
    'End of Lease Cleaning',
    'Carpet Steam Cleaning',
    'Window Cleaning',
  ];

  changeCheckBoxForAnExtra(bool value) {
    extraFor = value;
    update();
  }

  changeCheckBoxSelectIndustries(CheckBoxModal checkBoxModal) {
    checkBoxModal.value = !checkBoxModal.value;
    update();
  }

  changeCheckBoxSelectItemIndustries(CheckBoxModal checkBoxModal) {
    checkBoxModal.value = !checkBoxModal.value;
    update();
  }

  changeTraveling(value) {
    traveling = value;
    update();
  }

  changeCheckBoxForExtra(bool value) {
    extra = value;
    update();
  }
}
