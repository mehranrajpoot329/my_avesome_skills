import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Modules/Business/ad_Screen_business/ad_screen_can_help_business_controller.dart';
import 'package:AwesomeSkills/Modules/Business/ad_Screen_business/custom_ad_info_text_form_field_business.dart';
import 'package:AwesomeSkills/Modules/Business/custom_scroll_appBar_business/nested_business_scroll_widget.dart';
import 'package:AwesomeSkills/Modules/Personal/ad_Screen/ad_screeb_can_help_personal/ad_screen_can_help_personal.dart';
import 'package:AwesomeSkills/Modules/Personal/ad_Screen/ad_screeb_can_help_personal/ad_screen_can_help_personal_controller.dart';
import 'package:AwesomeSkills/routes/app_pages.dart';
import 'package:sizer/sizer.dart';
import '../../../Constants/app_colors.dart';
import '../../../Constants/app_strings.dart';
import '../../../Constants/text_style.dart';
import '../../../widgets/button/custom_button.dart';
import '../../../widgets/button/save_preview_post_ad_button.dart';
import '../../../widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import '../../../widgets/custom_widget_app/widget_method.dart';
import '../../../widgets/drop_down.dart';

class AdScreenCanHelpBusinessWithOutBottomNav extends StatelessWidget {
  final controller = Get.put(AdScreenCanHelpBusinessController());
  final selectAll = CheckBoxModal(
    title: 'title',
  );
  final checkBoxList = [
    CheckBoxModal(title: 'House Cleaning'),
    CheckBoxModal(title: 'End of Lease Cleaning'),
    CheckBoxModal(title: 'Carpet Steam Cleaning'),
    CheckBoxModal(title: 'Window Cleaning'),
    CheckBoxModal(title: 'Outdoor Cleaning'),
    CheckBoxModal(title: 'Office Cleaning'),
    CheckBoxModal(title: 'High Pressure Cleaning'),
    CheckBoxModal(title: 'Upholstery Cleaning'),
  ];

  @override
  Widget build(BuildContext context) {
    return NestedBusinessScroll(
        floatingActionButton: WidgetMethod.floatingActionButton(context),
        body: Padding(
            padding: EdgeInsets.only(left: 2.w, right: 2.w),
            child: SingleChildScrollView(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 1.h,
                    ),
                    Row(
                      children: [
                        InkWell(
                          onTap: () => Get.back(),
                          child: Icon(
                            Icons.arrow_back_ios_new,
                            color: Colors.black,
                            size: 15.sp,
                          ),
                        ),
                        SizedBox(
                          width: 5.w,
                        ),
                        const Text(
                          AppStrings.ad,
                          style: montserrat20TextStyle,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 3.w, vertical: 2.h),
                      height: 16.h,
                      width: 100.w,
                      decoration: BoxDecoration(
                          color: AppColors.lightGreen2Color,
                          borderRadius: BorderRadius.circular(10),
                          boxShadow: [
                            BoxShadow(
                                blurRadius: 4.0,
                                spreadRadius: 2.0,
                                color: Colors.grey.withOpacity(0.2))
                          ]),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            AppStrings.getNoticed,
                            style: orange600ColorTextStyle,
                          ),
                          Row(
                            children: [
                              Text(
                                AppStrings.standardAd,
                                style: blackBold16Color,
                              ),
                              SizedBox(
                                width: 1.w,
                              ),
                              const Text(
                                AppStrings.free,
                                style: orange16ColorTextStyle,
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Text(
                                AppStrings.featuredAd,
                                style: blackBold16Color,
                              ),
                              SizedBox(
                                width: 1.w,
                              ),
                              const Text(
                                AppStrings.tenDay,
                                style: orange16ColorTextStyle,
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    const Text(
                      AppStrings.canHelpWith,
                      style: blue500ColorTextStyle,
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    Text(
                      AppStrings.industry,
                      style: black500TextStyle,
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    CustomDropDown(
                      height: 7.h,
                      width: 100.w,
                      hint: AppStrings.category,
                      items: const <String>[
                        'Cleaning',
                        'Design',
                        'Website  ',
                        'App',
                      ],
                      selectedValue: (value) {
                        controller.category.value = value!;
                        log(value.toString());
                      },
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    Text(
                      AppStrings.title,
                      style: black500TextStyle,
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    CustomDropDown(
                      height: 7.h,
                      width: 100.w,
                      hint: AppStrings.category,
                      items: const <String>[
                        'House Cleaning',
                        'Design',
                        'Website  ',
                        'App',
                      ],
                      selectedValue: (value) {
                        controller.category.value = value!;
                        log(value.toString());
                      },
                    ),
                    SizedBox(
                      height: 3.h,
                    ),
                    Center(
                      child: Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: 4.w, vertical: 2.h),
                          height: 50.h,
                          width: 95.w,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.1),
                                  blurRadius: 4.0,
                                  spreadRadius: 2.0,
                                )
                              ]),
                          child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                FittedBox(
                                  child: Text(
                                    AppStrings.selectIndustries,
                                    style: blue500ColorTextStyle,
                                  ),
                                ),
                                const Divider(
                                  color: AppColors.lightBlueColor,
                                ),
                                Expanded(
                                    child: Scrollbar(
                                  thickness: 5,
                                  radius: const Radius.circular(5),
                                  thumbVisibility: false,
                                  child: ListView(
                                    padding: EdgeInsets.zero,
                                    children: [
                                      InkWell(
                                        onTap: () => controller
                                            .changeCheckBoxSelectIndustries(
                                                selectAll),
                                        child: Row(
                                          children: [
                                            GetBuilder<
                                                AdScreenCanHelpPersonalController>(
                                              builder: (_) {
                                                return Checkbox(
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5)),
                                                  side: const BorderSide(
                                                      color: AppColors
                                                          .lightBlueColor,
                                                      width: 0.5),
                                                  checkColor: Colors.white,
                                                  activeColor:
                                                      AppColors.lightBlueColor,
                                                  value: controller
                                                      .selectAll.value,
                                                  onChanged: (value) => controller
                                                      .changeCheckBoxSelectIndustries(
                                                          selectAll),
                                                );
                                              },
                                            ),
                                            SizedBox(
                                              width: 3.w,
                                            ),
                                            Text(
                                              controller.selectAll.title,
                                              style: blackBold16Color,
                                            ),
                                          ],
                                        ),
                                      ),
                                      ...checkBoxList.map(
                                        (item) => InkWell(
                                          onTap: () => controller
                                              .changeCheckBoxSelectItemIndustries(
                                                  item),
                                          child: Row(
                                            children: [
                                              GetBuilder<
                                                  AdScreenCanHelpPersonalController>(
                                                builder: (_) {
                                                  return Checkbox(
                                                    shape:
                                                        RoundedRectangleBorder(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        5)),
                                                    side: const BorderSide(
                                                        color: AppColors
                                                            .lightBlueColor,
                                                        width: 0.5),
                                                    checkColor: Colors.white,
                                                    activeColor: AppColors
                                                        .lightBlueColor,
                                                    value: item.value,
                                                    onChanged: (value) => controller
                                                        .changeCheckBoxSelectItemIndustries(
                                                            item),
                                                  );
                                                },
                                              ),
                                              SizedBox(
                                                width: 3.w,
                                              ),
                                              Text(
                                                item.title,
                                                style: blackBold16Color,
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ))
                              ])),
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    Text(
                      AppStrings.featuredSkillsHourly,
                      style: black500TextStyle,
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    buildFeature(
                      skillController: controller.skillControllerOne,
                      priceController: controller.priceControllerOne,
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    buildFeature(
                      skillController: controller.skillControllerTwo,
                      priceController: controller.priceControllerTwo,
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    buildFeature(
                      skillController: controller.skillControllerThree,
                      priceController: controller.priceControllerThree,
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                        left: 1.w,
                      ),
                      child: Row(
                        children: [
                          SizedBox(
                            width: 50.w,
                            child: Row(
                              children: [
                                Expanded(
                                  flex: 2,
                                  child: Container(
                                    padding: EdgeInsets.only(
                                        top: 1.7.h, right: 2.w, left: 2.w),
                                    height: 30,
                                    width: 70,
                                    decoration: BoxDecoration(
                                      color: AppColors.lightBlue6Color,
                                      borderRadius: BorderRadius.circular(5),
                                    ),
                                    child: TextFormField(
                                        controller:
                                            controller.forAnExtraControllerOne,
                                        decoration: InputDecoration(
                                          border: InputBorder.none,
                                          hintStyle: greyTextColor,
                                          hintText: AppStrings.forAnExtra,
                                        )),
                                  ),
                                ),
                                SizedBox(
                                  width: 3.w,
                                ),
                                Expanded(
                                    child: Container(
                                  padding: EdgeInsets.only(top: 13, left: 2.w),
                                  height: 4.h,
                                  width: 20.w,
                                  decoration: BoxDecoration(
                                    color: AppColors.lightBlue6Color,
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  child: TextFormField(
                                      controller: controller
                                          .forAnExtraControllerOneValue,
                                      decoration: InputDecoration(
                                        border: InputBorder.none,
                                        hintStyle: greyTextColor,
                                        hintText: AppStrings.dollarFortyFive,
                                      )),
                                )),
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 10.w,
                          ),
                          SizedBox(
                            width: 20.w,
                            child: Row(
                              children: [
                                Expanded(
                                  flex: 2,
                                  child: GetBuilder<
                                      AdScreenCanHelpPersonalController>(
                                    builder: (_) {
                                      return Checkbox(
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(5)),
                                          side: const BorderSide(
                                              color: AppColors.lightBlueColor,
                                              width: 0.5),
                                          checkColor: Colors.white,
                                          activeColor: AppColors.lightBlueColor,
                                          value: controller.extra,
                                          onChanged: (bool? value) => controller
                                              .changeCheckBoxForExtra(value!));
                                    },
                                  ),
                                ),
                                const Text(
                                  AppStrings.iWill,
                                  style: black12PoppinsColor,
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                        left: 1.w,
                      ),
                      child: Row(
                        children: [
                          SizedBox(
                            width: 50.w,
                            child: Row(
                              children: [
                                Expanded(
                                  flex: 2,
                                  child: Container(
                                    padding: EdgeInsets.only(
                                        top: 1.7.h, right: 2.w, left: 2.w),
                                    height: 30,
                                    width: 70,
                                    decoration: BoxDecoration(
                                      color: AppColors.lightBlue6Color,
                                      borderRadius: BorderRadius.circular(5),
                                    ),
                                    child: TextFormField(
                                        controller:
                                            controller.forAnExtraControllerOne,
                                        decoration: InputDecoration(
                                          border: InputBorder.none,
                                          hintStyle: greyTextColor,
                                          hintText: AppStrings.forAnExtra,
                                        )),
                                  ),
                                ),
                                SizedBox(
                                  width: 3.w,
                                ),
                                Expanded(
                                    child: Container(
                                  padding: EdgeInsets.only(top: 13, left: 2.w),
                                  height: 4.h,
                                  width: 20.w,
                                  decoration: BoxDecoration(
                                    color: AppColors.lightBlue6Color,
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  child: TextFormField(
                                      controller: controller
                                          .forAnExtraControllerTwoValue,
                                      decoration: InputDecoration(
                                        border: InputBorder.none,
                                        hintStyle: greyTextColor,
                                        hintText: AppStrings.dollarFortyFive,
                                      )),
                                )),
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 10.w,
                          ),
                          SizedBox(
                            width: 20.w,
                            child: Row(
                              children: [
                                Expanded(
                                  flex: 2,
                                  child: GetBuilder<
                                      AdScreenCanHelpPersonalController>(
                                    builder: (_) {
                                      return Checkbox(
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(5)),
                                          side: const BorderSide(
                                              color: AppColors.lightBlueColor,
                                              width: 0.5),
                                          checkColor: Colors.white,
                                          activeColor: AppColors.lightBlueColor,
                                          value: controller.extraFor,
                                          onChanged: (bool? value) => controller
                                              .changeCheckBoxForAnExtra(
                                                  value!));
                                    },
                                  ),
                                ),
                                const Text(
                                  AppStrings.iWill,
                                  style: black12PoppinsColor,
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 2.h,
                    ),
                    Text(
                      AppStrings.traveling,
                      style: black500TextStyle,
                    ),
                    SizedBox(
                      height: 1.h,
                    ),
                    const Text(
                      AppStrings.howManyKMYouAre,
                      style: black12PoppinsColor,
                    ),
                    GetBuilder<AdScreenCanHelpPersonalController>(
                      builder: (_) {
                        return Slider(
                            max: 100,
                            min: 0,
                            divisions: 20,
                            value: controller.traveling,
                            label:
                                '${controller.traveling.round().toString()} Km',
                            onChanged: (newValue) =>
                                controller.changeTraveling(newValue));
                      },
                    ),
                    CustomAdInfoTextFormFieldBusiness(
                        titleTextStyle: black500TextStyle,
                        isOptionalText: false,
                        height: 8.h,
                        width: 100.w,
                        title: AppStrings.iamTheRightPerson,
                        hintText: '',
                        controller: controller.personForJobController),
                    SizedBox(
                      height: 3.h,
                    ),
                    customTextFieldForm(
                        title: AppStrings.mySkill,
                        controller: controller.mySkillController),
                    SizedBox(
                      height: 2.h,
                    ),
                    customTextFieldForm(
                        title: AppStrings.myEducation,
                        controller: controller.myEducationController),
                    SizedBox(
                      height: 2.h,
                    ),
                    customTextFieldForm(
                        title: AppStrings.postJobs,
                        controller: controller.postJobsController),
                    SizedBox(
                      height: 2.h,
                    ),
                    CustomButton(
                      height: 5.h,
                      width: 50.w,
                      buttonName: AppStrings.uploadImages,
                      buttonColor: Colors.grey.withOpacity(0.2),
                      textStyle: blackPoppins300TextStyle,
                      borderRadius: BorderRadius.circular(5),
                      onTap: () {},
                    ),
                    SizedBox(
                      height: 4.h,
                    ),
                    SavePreviewPostButton(
                      saveOnTap: () {},
                      previewAdOnTap: () {},
                      postAdOnTap: () => Get.toNamed(AppPage.listYourBusiness),
                    ),
                    SizedBox(
                      height: 12.h,
                    ),
                  ]),
            )));
  }

  Widget buildFeature(
      {required TextEditingController priceController,
      required TextEditingController skillController}) {
    return Padding(
        padding: EdgeInsets.only(left: 1.w, right: 1.w),
        child: Row(
          children: [
            SizedBox(
              width: 50.w,
              child: Row(
                children: [
                  Expanded(
                    flex: 2,
                    child: Container(
                      padding:
                          EdgeInsets.only(top: 1.7.h, right: 2.w, left: 2.w),
                      height: 30,
                      width: 70,
                      decoration: BoxDecoration(
                        color: AppColors.lightBlue6Color,
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: TextFormField(
                          controller: skillController,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            hintStyle: greyTextColor,
                            hintText: AppStrings.typeYourSkills,
                          )),
                    ),
                  ),
                  SizedBox(
                    width: 3.w,
                  ),
                  Expanded(
                      child: Container(
                    padding: EdgeInsets.only(top: 13, left: 2.w),
                    height: 30,
                    width: 70,
                    decoration: BoxDecoration(
                      color: AppColors.lightBlue6Color,
                      borderRadius: BorderRadius.circular(5),
                    ),
                    child: TextFormField(
                        controller: priceController,
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          hintStyle: greyTextColor,
                          hintText: AppStrings.dollarFortyFive,
                        )),
                  )),
                ],
              ),
            )
          ],
        ));
  }

  Widget customTextFieldForm(
      {required String title, required TextEditingController controller}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text(
              title,
              style: black500TextStyle,
            ),
            const Text(
              AppStrings.optionalBracket,
              style: grey14Color,
            )
          ],
        ),
        SizedBox(
          height: 1.h,
        ),
        Container(
          padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
          height: 8.h,
          width: 100.w,
          decoration: BoxDecoration(
            color: AppColors.lightBlue6Color,
            borderRadius: BorderRadius.circular(10),
          ),
          child: TextFormField(
            controller: controller,
            decoration: InputDecoration(
                border: InputBorder.none,
                hintText: '',
                hintStyle: black12Color),
          ),
        ),
      ],
    );
  }
}
