import 'package:flutter/material.dart';
import 'package:get/get.dart';

class AdScreenNeedHelpBusinessController extends GetxController {
  TextEditingController jobTitleController = TextEditingController();
  TextEditingController jobDescriptionController = TextEditingController();
  TextEditingController extraInstructionController = TextEditingController();
  TextEditingController subRubController = TextEditingController();
  TextEditingController youtubeController = TextEditingController();
  TextEditingController jobLocatedController = TextEditingController();
  TextEditingController provideAreaSizeJobController = TextEditingController();
  String selectedValue = 'UI/UX';
  bool isPriceFixed = false;
  bool completedPerson = false;
  bool completedOnline = false;
  RxString category = ''.obs;
  RxString industry = ''.obs;

  // List of items in our dropdown menu
  List categories = <String>[
    'UI/UX',
    'Design',
    'Website  ',
    'App',
  ];

  List imageSlider = [
    'assets/images/spray_bottle.png',
    'assets/images/spray_bottle2.png',
    'assets/images/spray_bottle.png',
    'assets/images/spray_bottle2.png',
  ];

  List<Widget> generateSliderImage() {
    return imageSlider
        .map(
          (e) => Container(
            height: 140,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                image:
                    DecorationImage(image: AssetImage(e), fit: BoxFit.cover)),
          ),
        )
        .toList();
  }

  changeSwitchFixedPrice(bool value) {
    isPriceFixed = value;
    update();
  }

  changeCheckBoxCompletedPerson(bool value) {
    completedPerson = value;
    update();
  }

  changeCheckBoxCompletedOnline(bool value) {
    completedOnline = value;
    update();
  }
}
