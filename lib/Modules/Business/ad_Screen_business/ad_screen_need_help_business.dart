import 'dart:developer';
import 'package:AwesomeSkills/Modules/Business/landing_screen_business/Need_Help_business/need_help_card_business.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Business/ad_Screen_business/ad_screen_need_help_business_controller.dart';
import 'package:AwesomeSkills/Modules/Business/ad_Screen_business/build_tags_need_work_widget_business.dart';
import 'package:AwesomeSkills/Modules/Business/ad_Screen_business/custom_ad_info_text_form_field_business.dart';
import 'package:AwesomeSkills/Modules/Business/custom_scroll_appBar_business/nested_business_scroll_widget.dart';
import 'package:AwesomeSkills/Modules/Personal/ad_Screen/build_tags_need_work_widget.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/Need_Help/need_help_card.dart';
import 'package:AwesomeSkills/routes/app_pages.dart';
import 'package:AwesomeSkills/widgets/button/save_preview_post_ad_button.dart';
import 'package:AwesomeSkills/widgets/custom_textfield/custom_textfield.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:AwesomeSkills/widgets/drop_down.dart';
import 'package:sizer/sizer.dart';

class AdScreenNeedHelpBusiness extends StatelessWidget {
  final controller = Get.put(AdScreenNeedHelpBusinessController());

  @override
  Widget build(BuildContext context) {
    return NestedBusinessScroll(
        floatingActionButton: WidgetMethod.floatingActionButton(context),
        body: Padding(
            padding: EdgeInsets.only(left: 2.w, right: 2.w),
            child: SingleChildScrollView(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                  SizedBox(
                    height: 1.h,
                  ),
                  Row(
                    children: [
                      InkWell(
                        onTap: () => Get.back(),
                        child: Icon(
                          Icons.arrow_back_ios_new,
                          color: Colors.black,
                          size: 15.sp,
                        ),
                      ),
                      SizedBox(
                        width: 5.w,
                      ),
                      const Text(
                        AppStrings.ad,
                        style: montserrat20TextStyle,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 3.w, top: 0.5.h),
                    height: 12.5.h,
                    width: 100.w,
                    decoration: BoxDecoration(
                      color: AppColors.lightGreen2Color,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          AppStrings.getNoticed,
                          style: orange600ColorTextStyle,
                        ),
                        Row(
                          children: [
                            Text(
                              AppStrings.standardAd,
                              style: blackBold16Color,
                            ),
                            SizedBox(
                              width: 1.w,
                            ),
                            const Text(
                              AppStrings.free,
                              style: orange16ColorTextStyle,
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Text(
                              AppStrings.featuredAd,
                              style: blackBold16Color,
                            ),
                            SizedBox(
                              width: 1.w,
                            ),
                            const Text(
                              AppStrings.tenDay,
                              style: orange16ColorTextStyle,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  const Text(
                    AppStrings.needHelp,
                    style: blue500ColorTextStyle,
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  CustomAdInfoTextFormFieldBusiness(
                    isOptionalText: false,
                    height: 7.h,
                    width: 110.w,
                    title: AppStrings.jobTitle,
                    hintText: AppStrings.useTitleAndFair,
                    controller: controller.jobTitleController,
                    titleTextStyle: blackFourteenSpTextStyle,
                  ),
                  SizedBox(
                    height: 0.5.h,
                  ),
                  const Align(
                    alignment: Alignment.bottomRight,
                    child: Text(
                      AppStrings.eightyCharacter,
                      style: black12PoppinsColor,
                    ),
                  ),
                  CustomAdInfoTextFormFieldBusiness(
                      titleTextStyle: blackFourteenSpTextStyle,
                      isOptionalText: false,
                      height: 10.h,
                      width: 110.w,
                      title: AppStrings.jobDescription,
                      hintText: AppStrings.beAsDescriptive,
                      controller: controller.jobDescriptionController),
                  SizedBox(
                    height: 1.h,
                  ),
                  Row(
                    children: [
                      Text(
                        AppStrings.extraInstruction,
                        style: blackFourteenSpTextStyle,
                      ),
                      SizedBox(
                        width: 2.w,
                      ),
                      const Text(
                        AppStrings.optionalBracket,
                        style: grey14Color,
                      )
                    ],
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  Container(
                    padding:
                        EdgeInsets.symmetric(vertical: 0.2.h, horizontal: 2.w),
                    height: 10.h,
                    width: 110.w,
                    decoration: BoxDecoration(
                      color: AppColors.lightBlue6Color,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: TextFormField(
                      controller: controller.extraInstructionController,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: "",
                          hintStyle: black12Color),
                    ),
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Text(
                    AppStrings.whichCategoryIsThe,
                    style: blackFourteenSpTextStyle,
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: CustomDropDown(
                          height: 7.h,
                          width: 100.w,
                          hint: AppStrings.category,
                          items: const <String>[
                            'UI/UX',
                            'Design',
                            'Website  ',
                            'App',
                          ],
                          selectedValue: (value) {
                            controller.category.value = value!;
                            log(value.toString());
                          },
                        ),
                      ),
                      Expanded(
                        child: CustomDropDown(
                          height: 7.h,
                          width: 100.w,
                          hint: AppStrings.subCategory,
                          items: const <String>[
                            'UI/UX',
                            'Design',
                            'Website  ',
                            'App',
                          ],
                          selectedValue: (value) {
                            controller.category.value = value!;
                            log(value.toString());
                          },
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Text(
                    AppStrings.tags,
                    style: blackFourteenSpTextStyle,
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  BuildTagsNeedWorkWidgetBusiness(
                    isNextRow: true,
                    text1: AppStrings.uiUX,
                    text2: AppStrings.design,
                    text3: AppStrings.website,
                    text4: AppStrings.app,
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Text(
                    AppStrings.whichSubRub,
                    style: blackFourteenSpTextStyle,
                  ),
                  SizedBox(
                    height: 0.5.h,
                  ),
                  CustomDropDown(
                    height: 7.h,
                    width: 100.w,
                    hint: AppStrings.category,
                    items: const <String>[
                      'UI/UX',
                      'Design',
                      'Website  ',
                      'App',
                    ],
                    selectedValue: (value) {
                      controller.category.value = value!;
                      log(value.toString());
                    },
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Text(
                    AppStrings.whenDoYouNeed,
                    style: blackFourteenSpTextStyle,
                  ),
                  BuildTagsNeedWorkWidget(
                    isNextRow: true,
                    text1: AppStrings.asap,
                    text2: AppStrings.nextFewDays,
                    text3: AppStrings.week,
                    text4: AppStrings.iAmFlexible,
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  Text(
                    AppStrings.jobPrice,
                    style: blackFourteenSpTextStyle,
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  Container(
                    height: 40,
                    width: 178,
                    decoration: BoxDecoration(
                      color: AppColors.lightBlue6Color,
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Center(
                      child: Text(
                        AppStrings.hourlyRate,
                        style: black14Color,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        width: 50.w,
                        child: Row(
                          children: [
                            Text(
                              AppStrings.fixedPrice,
                              style: blackFourteenSpTextStyle,
                            ),
                            SizedBox(
                              width: 3.w,
                            ),
                            GetBuilder<AdScreenNeedHelpBusinessController>(
                                builder: (_) => Transform.scale(
                                      scaleY: 0.5,
                                      scaleX: 0.6,
                                      child: CupertinoSwitch(
                                          thumbColor: AppColors.lightBlueColor,
                                          trackColor:
                                              Colors.grey.withOpacity(0.3),
                                          activeColor:
                                              AppColors.lightBlue4Color,
                                          value: controller.isPriceFixed,
                                          onChanged: (newValue) {
                                            controller.changeSwitchFixedPrice(
                                                newValue);
                                          }),
                                    )),
                          ],
                        ),
                      ),
                      Container(
                        height: 40,
                        width: 178,
                        decoration: BoxDecoration(
                          color: AppColors.lightBlue6Color,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Center(
                          child: Text(
                            AppStrings.totalPriceDollar,
                            style: black14Color,
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Text(
                    AppStrings.isThisRepairOrInstallation,
                    style: blackFourteenSpTextStyle,
                  ),
                  SizedBox(
                    height: 0.5.h,
                  ),
                  CustomDropDown(
                    height: 7.h,
                    width: 100.w,
                    hint: AppStrings.chooseCategory,
                    items: const <String>[
                      'Cleaning',
                      'Design',
                      'Website  ',
                      'App',
                    ],
                    selectedValue: (value) {
                      controller.industry.value = value!;
                      log(value.toString());
                    },
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Text(
                    AppStrings.quotesToIncludeMaterial,
                    style: blackFourteenSpTextStyle,
                  ),
                  SizedBox(
                    height: 0.5.h,
                  ),
                  CustomDropDown(
                    height: 7.h,
                    width: 100.w,
                    hint: AppStrings.chooseCategory,
                    items: const <String>[
                      'Cleaning',
                      'Design',
                      'Website  ',
                      'App',
                    ],
                    selectedValue: (value) {
                      controller.industry.value = value!;
                      log(value.toString());
                    },
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  CustomAdInfoTextFormFieldBusiness(
                      titleTextStyle: blackFourteenSpTextStyle,
                      isOptionalText: false,
                      height: 50,
                      width: MediaQuery.of(context).size.width - 20,
                      title: AppStrings.pleaseProvideSizeArea,
                      hintText: AppStrings.enterANumber,
                      controller: controller.provideAreaSizeJobController),
                  SizedBox(
                    height: 2.h,
                  ),
                  Text(
                    AppStrings.canTradesManVisitTheSite,
                    style: blackFourteenSpTextStyle,
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  CustomDropDown(
                    height: 7.h,
                    width: 100.w,
                    hint: AppStrings.chooseCategory,
                    items: const <String>[
                      'Cleaning',
                      'Design',
                      'Website  ',
                      'App',
                    ],
                    selectedValue: (value) {
                      controller.industry.value = value!;
                      log(value.toString());
                    },
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  Text(
                    AppStrings.workPlace,
                    style: blackFourteenSpTextStyle,
                  ),
                  Row(
                    children: [
                      GetBuilder<AdScreenNeedHelpBusinessController>(
                        builder: (_) {
                          return Checkbox(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5)),
                              side: const BorderSide(
                                  color: AppColors.lightBlueColor, width: 0.5),
                              checkColor: Colors.white,
                              activeColor: AppColors.lightBlueColor,
                              value: controller.completedPerson,
                              onChanged: (bool? value) => controller
                                  .changeCheckBoxCompletedPerson(value!));
                        },
                      ),
                      Text(
                        AppStrings.toBeCompleted,
                        style: blackBold16Color,
                      )
                    ],
                  ),
                  Row(
                    children: [
                      GetBuilder<AdScreenNeedHelpBusinessController>(
                        builder: (_) {
                          return Checkbox(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5)),
                              side: const BorderSide(
                                  color: AppColors.lightBlueColor, width: 0.5),
                              checkColor: Colors.white,
                              activeColor: AppColors.lightBlueColor,
                              value: controller.completedOnline,
                              onChanged: (bool? value) => controller
                                  .changeCheckBoxCompletedOnline(value!));
                        },
                      ),
                      Text(
                        AppStrings.canBeCompleted,
                        style: blackBold16Color,
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 15.0,
                  ),
                  Text(
                    AppStrings.uploadImageOptional,
                    style: blackFourteenSpTextStyle,
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  DottedBorder(
                    dashPattern: const [6, 2],
                    radius: const Radius.circular(10),
                    color: AppColors.indigoColor,
                    strokeWidth: 1.5,
                    child: Container(
                      height: 20.h,
                      width: 100.w,
                      decoration: const BoxDecoration(
                        color: Colors.transparent,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SvgPicture.asset(
                            'assets/images/post_icon/upload_icon.svg',
                          ),
                          SizedBox(
                            height: 0.4.h,
                          ),
                          Text(
                            AppStrings.dragAndDrop,
                            style: blackBoldPoppinsColor,
                          ),
                          SizedBox(
                            height: 0.4.h,
                          ),
                          Text(
                            AppStrings.or,
                            style: blackBoldPoppinsColor,
                          ),
                          SizedBox(
                            height: 0.4.h,
                          ),
                          const Text(
                            AppStrings.clickToBrowse,
                            style: yellowPoppins300TextStyle,
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  CarouselSlider(
                    items: controller.generateSliderImage(),
                    options: CarouselOptions(
                        enlargeCenterPage: true,
                        autoPlayCurve: Curves.fastOutSlowIn,
                        autoPlayAnimationDuration:
                            const Duration(milliseconds: 800),
                        viewportFraction: 0.7),
                  ),
                  SizedBox(
                    height: 2.h,
                  ),
                  Row(
                    children: [
                      Text(
                        AppStrings.youtubeVideoLinks,
                        style: blackFourteenSpTextStyle,
                      ),
                      SizedBox(
                        width: 2.w,
                      ),
                      Text(
                        AppStrings.optional,
                        style: grey14Color,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 1.h,
                  ),
                  CustomTextFormField(
                      hintText: '',
                      name: 'youtube_link',
                      height: 7.h,
                      width: 100.w,
                      borderRadius: BorderRadius.circular(10),
                      hintTextStyle: black12Color,
                      color: AppColors.lightBlue6Color,
                      validator: (String? value) {},
                      controller: controller.youtubeController),
                  SizedBox(
                    height: 2.h,
                  ),
                  Center(child: NeedHelpBusinessCard(onTap: () {})),
                  SizedBox(
                    height: 1.h,
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Column(
                        children: [
                          Row(
                            children: [
                              Text(
                                AppStrings.featured,
                                style: blackBold16Color,
                              ),
                              SizedBox(
                                width: 2.w,
                              ),
                              Icon(
                                Icons.help_outline,
                                color: AppColors.green10Color,
                              )
                            ],
                          ),
                          Text(
                            AppStrings.tenTenDay,
                            style: blackBold16Color,
                          ),
                        ],
                      ),
                      SizedBox(
                        width: 10.w,
                      ),
                      Container(
                        height: 5.h,
                        width: 20.w,
                        decoration: BoxDecoration(
                            color: Colors.transparent,
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(
                                color: AppColors.green10Color,
                                width: 2.0,
                                style: BorderStyle.solid)),
                        child: const Center(
                          child: Text(
                            AppStrings.add,
                            style: greenColorTextStyle,
                          ),
                        ),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 15.0,
                  ),
                  SavePreviewPostButton(
                    saveOnTap: () {},
                    previewAdOnTap: () {},
                    postAdOnTap: () => Get.toNamed(AppPage.listYourBusiness),
                  ),
                  SizedBox(
                    height: 10.h,
                  ),
                ]))));
  }
}
