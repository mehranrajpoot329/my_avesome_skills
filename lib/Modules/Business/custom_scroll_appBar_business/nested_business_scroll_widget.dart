import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Modules/Business/custom_bottom_appBar_navi_business/custom_bottom_appBar_navi_business.dart';
import 'package:AwesomeSkills/Modules/Business/custom_scroll_appBar_business/custom_app_bar_business_controller.dart';
import 'package:AwesomeSkills/Modules/Personal/notifications_screen/notification_screen.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:AwesomeSkills/widgets/custom_bottom_appBar_navi/custom_bottom_appBar_navi.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/custom_app_bar_controller.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget_controller.dart';
import 'package:AwesomeSkills/widgets/drawer/business/drawer_profile_over_view_business_controller.dart';
import 'package:AwesomeSkills/widgets/drawer/menu_drawer.dart';
import 'package:AwesomeSkills/widgets/drawer/personal/drawer_profile_overview_personal_controller.dart';
import 'package:sizer/sizer.dart';

import '../../../Constants/app_strings.dart';
import '../../../Constants/text_style.dart';

PreferredSize _appBar() {
  return PreferredSize(
    preferredSize: Size.fromHeight(21.h),
    child: Container(
      decoration: BoxDecoration(color: Colors.white70, boxShadow: [
        BoxShadow(
            color: Colors.grey.withOpacity(0.2),
            spreadRadius: 2.0,
            blurRadius: 2.0)
      ]),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              MenuDrawer(),
              Container(
                height: 10.h,
                width: 40.w,
                decoration: const BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/images/logo/logo.png'),
                        fit: BoxFit.fitWidth)),
              ),
              Padding(
                  padding: const EdgeInsets.only(right: 5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const NotificationMenu(),
                      const SizedBox(width: 10),
                      Badge(
                        position: const BadgePosition(
                          end: 7,
                          bottom: 7,
                        ),
                        badgeContent: const Center(
                          child: Text(
                            '1',
                            style: TextStyle(fontSize: 8, color: Colors.white),
                          ),
                        ),
                        child: const Icon(
                          FontAwesomeIcons.comment,
                          size: 30,
                          color: Colors.black,
                        ),
                      ),
                      Builder(
                        builder: (context) => IconButton(
                          icon: Icon(
                            Icons.person_add_alt_1,
                            color: Colors.black,
                            size: 20.sp,
                          ),
                          onPressed: () => Scaffold.of(context).openDrawer(),
                        ),
                      )
                    ],
                  )),
            ],
          ),
          Container(
            height: 10.h,
            width: Get.width,
            decoration: BoxDecoration(color: AppColors.blue7Color),
            child: Padding(
              padding: EdgeInsets.only(left: 2.w, right: 2.w),
              child: Row(
                children: [
                  Expanded(
                      child: Container(
                    padding: EdgeInsets.only(left: 4.w),
                    height: 6.h,
                    width: Get.width,
                    decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                            color: Colors.grey.withOpacity(0.2),
                            spreadRadius: 2.0,
                            blurRadius: 2.0)
                      ],
                      color: AppColors.darkLightColor.withOpacity(0.7),
                      borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(6),
                          bottomLeft: Radius.circular(6)),
                    ),
                    child: TextFormField(
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          hintText: AppStrings.whatOnYourToDo,
                          hintStyle: labelTextStyle,
                          suffixIcon: SearchMenu()),
                    ),
                  )),
                  CustomButton(
                    buttonName: AppStrings.search,
                    width: 22.w,
                    height: 6.h,
                    buttonColor: AppColors.lightBlueColor,
                    textStyle: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w400,
                      fontFamily: 'poppins',
                      fontSize: 10.sp,
                    ),
                    borderRadius: const BorderRadius.only(
                        topRight: Radius.circular(5),
                        bottomRight: Radius.circular(5)),
                    onTap: () {},
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    ),
  );
}

class NestedBusinessScroll extends StatelessWidget {
  final appBarController = Get.put(DrawerProfileOverViewBusinessController());
  Widget body, floatingActionButton;

  NestedBusinessScroll({
    required this.body,
    required this.floatingActionButton,
  });
  final customController = Get.put(CustomAppBarBusinessController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawerEdgeDragWidth: 0.0,
      floatingActionButton: floatingActionButton,
      bottomNavigationBar: CustomNavBarBusinessWidget(),
      drawer: Obx(() => appBarController.switchDrawer()),
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxScrolled) {
          return <Widget>[
            customController.createSilverAppBar1(context),
            customController.isLogined
                ? customController.createSilverAppBar4(context)
                : customController.createSilverAppBar2(),
            customController.createSilverAppBar3(context),
            customController.isLogined
                ? customController.createSilverAppBar5(context)
                : customController.createSilverAppBar4(context)
          ];
        },
        body: body,
      ),
    );
  }
}

class CustomAppbar extends StatelessWidget implements PreferredSize {
  @override
  Size get preferredSize => Size.fromHeight(19.h);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        decoration: BoxDecoration(color: Colors.white70, boxShadow: [
          BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 2.0,
              blurRadius: 2.0)
        ]),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                MenuDrawer(),
                SizedBox(
                  width: 3.w,
                ),
                Expanded(
                  child: Container(
                    height: 5.h,
                    width: 40.w,
                    decoration: const BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('assets/images/logo/logo.png'),
                            fit: BoxFit.fitWidth)),
                  ),
                ),
                SizedBox(
                  width: 3.w,
                ),
                Padding(
                    padding: const EdgeInsets.only(right: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const NotificationMenu(),
                        const SizedBox(width: 10),
                        Badge(
                          position: const BadgePosition(
                            end: 7,
                            bottom: 7,
                          ),
                          badgeContent: const Center(
                            child: Text(
                              '1',
                              style:
                                  TextStyle(fontSize: 8, color: Colors.white),
                            ),
                          ),
                          child: const Icon(
                            FontAwesomeIcons.comment,
                            size: 30,
                            color: Colors.black,
                          ),
                        ),
                        Builder(
                          builder: (context) => IconButton(
                            icon: Icon(
                              Icons.person_add_alt_1,
                              color: Colors.black,
                              size: 20.sp,
                            ),
                            onPressed: () => Scaffold.of(context).openDrawer(),
                          ),
                        )
                      ],
                    )),
              ],
            ),
            Container(
              height: 10.h,
              width: Get.width,
              decoration: BoxDecoration(color: AppColors.blue7Color),
              child: Padding(
                padding: EdgeInsets.only(left: 2.w, right: 2.w),
                child: Row(
                  children: [
                    Expanded(
                        child: Container(
                      padding: EdgeInsets.only(left: 4.w),
                      height: 6.h,
                      width: Get.width,
                      decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                              color: Colors.grey.withOpacity(0.2),
                              spreadRadius: 2.0,
                              blurRadius: 2.0)
                        ],
                        color: AppColors.darkLightColor.withOpacity(0.7),
                        borderRadius: const BorderRadius.only(
                            topLeft: Radius.circular(6),
                            bottomLeft: Radius.circular(6)),
                      ),
                      child: TextFormField(
                        decoration: InputDecoration(
                            border: InputBorder.none,
                            hintText: AppStrings.whatOnYourToDo,
                            hintStyle: labelTextStyle,
                            suffixIcon: SearchMenu()),
                      ),
                    )),
                    CustomButton(
                      buttonName: AppStrings.search,
                      width: 22.w,
                      height: 6.h,
                      buttonColor: AppColors.lightBlueColor,
                      textStyle: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.w400,
                        fontFamily: 'poppins',
                        fontSize: 10.sp,
                      ),
                      borderRadius: const BorderRadius.only(
                          topRight: Radius.circular(5),
                          bottomRight: Radius.circular(5)),
                      onTap: () {},
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  // TODO: implement child
  Widget get child => throw UnimplementedError();
}
