import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Modules/Business/custom_scroll_appBar_business/custom_app_bar_business_controller.dart';
import 'package:AwesomeSkills/routes/app_pages.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/bottom_sheet_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:AwesomeSkills/widgets/drawer/business/drawer_profile_over_view_business_controller.dart';
import 'package:sizer/sizer.dart';

class CustomAppBarBusinessScreen extends StatefulWidget {
  @override
  State<CustomAppBarBusinessScreen> createState() =>
      _CustomAppBarBusinessScreenState();
}

class _CustomAppBarBusinessScreenState
    extends State<CustomAppBarBusinessScreen> {
  final appBarController = Get.put(DrawerProfileOverViewBusinessController());
  final customAppBarBusinessController =
      Get.put(CustomAppBarBusinessController());
  int _currentIndex = 0;
  late PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawerEdgeDragWidth: 0.0,
      drawer: customAppBarBusinessController.isLogined
          ? Obx(() => appBarController.switchDrawer())
          : null,
      floatingActionButton: Stack(
        clipBehavior: Clip.none,
        children: [
          Positioned(
              right: 0.w,
              bottom: -2.h,
              child: WidgetMethod.parentWidgetFloating(context))
        ],
      ),
      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(8.0),
        child: BottomNavyBar(
          curve: Curves.bounceInOut,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          containerHeight: 8.h,
          backgroundColor: AppColors.lightBlueColor,
          selectedIndex: _currentIndex,
          showElevation: true, // use this to remove appBar's elevation
          onItemSelected: (index) => setState(() {
            _currentIndex = index;
          }),
          items: customAppBarBusinessController.bottomNavyBarItem(context),
        ),
      ),
      body: GetBuilder<CustomAppBarBusinessController>(
        builder: (_) {
          return NestedScrollView(
            headerSliverBuilder: (BuildContext context, bool innerBoxScrolled) {
              return <Widget>[
                customAppBarBusinessController.createSilverAppBar1(context),
                customAppBarBusinessController.isLogined
                    ? customAppBarBusinessController
                        .createSilverAppBar4(context)
                    : customAppBarBusinessController.createSilverAppBar2(),
                customAppBarBusinessController.createSilverAppBar3(context),
                customAppBarBusinessController.isLogined
                    ? customAppBarBusinessController
                        .createSilverAppBar5(context)
                    : customAppBarBusinessController
                        .createSilverAppBar4(context)
              ];
            },
            body: customAppBarBusinessController.screens[_currentIndex],
          );
        },
      ),
    );
  }
}
