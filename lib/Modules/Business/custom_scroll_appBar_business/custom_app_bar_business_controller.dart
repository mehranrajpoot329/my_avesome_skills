import 'package:badges/badges.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Business/ad_Screen_business/ad_screen_need_help_business.dart';
import 'package:AwesomeSkills/Modules/Business/landing_screen_business/Can_Help_business/can_help_screen_business.dart';
import 'package:AwesomeSkills/Modules/Business/landing_screen_business/Need_Help_business/need_help_screen_business.dart';
import 'package:AwesomeSkills/Modules/Business/landing_screen_business/landing_screen_business.dart';
import 'package:AwesomeSkills/Modules/Business/landing_screen_business/search_business_controller.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/search_controller.dart';
import 'package:AwesomeSkills/Modules/Personal/notifications_screen/notification_screen.dart';
import 'package:AwesomeSkills/constants/app_colors.dart';
import 'package:AwesomeSkills/constants/app_fonts.dart';
import 'package:AwesomeSkills/constants/app_strings.dart';
import 'package:AwesomeSkills/routes/app_pages.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/bottom_sheet_widget.dart';
import 'package:AwesomeSkills/widgets/drawer/menu_drawer.dart';
import 'package:sizer/sizer.dart';

class CustomAppBarBusinessController extends GetxController {
  final controller = Get.put(SearchBusinessController());
  RxBool selectItem = false.obs;
  int currentIndex = 0;
  bool isLogined = false;
  RxBool isSearchFilterVisible = false.obs;

  selectIndex(index) {
    currentIndex = index;
    update();
  }

  isLoginedOutUser() {
    isLogined = false;
    update();
  }

  isLoginedUser() {
    isLogined = true;
    update();
  }

  List<Widget> screens = [
    LandingScreenBusiness(),
    CanHelpBusinessScreen(),
    NeedHelpBusinessScreen(),
    Container()
  ];

  List<BottomNavyBarItem> bottomNavyBarItem(BuildContext context) {
    return [
      BottomNavyBarItem(
        icon: SvgPicture.asset(
          'assets/images/post_icon/home_icon.svg',
          height: 18.sp,
          width: 18.sp,
        ),
        title: Text(
          AppStrings.home,
          style: TextStyle(
              color: Colors.white,
              fontSize: 10.sp,
              fontFamily: AppFonts.poppins,
              fontWeight: FontWeight.w200),
        ),
        activeColor: Colors.white,
      ),
      BottomNavyBarItem(
        icon: SvgPicture.asset(
          'assets/images/post_icon/hand_shake.svg',
          height: 18.sp,
          width: 18.sp,
        ),
        title: const Text(
          AppStrings.canHelp,
          style: TextStyle(
              color: Colors.white,
              fontFamily: AppFonts.poppins,
              fontWeight: FontWeight.w200),
        ),
        activeColor: Colors.white,
      ),
      BottomNavyBarItem(
        icon: SvgPicture.asset(
          'assets/images/post_icon/question_icon.svg',
          height: 18.sp,
          width: 18.sp,
        ),
        title: Text(
          AppStrings.needHelp,
          style: TextStyle(
              color: Colors.white,
              fontSize: 10.sp,
              fontFamily: AppFonts.poppins,
              fontWeight: FontWeight.w200),
        ),
        activeColor: Colors.white,
      ),
      BottomNavyBarItem(
        icon: GestureDetector(
          onTap: () => isLogined
              ? BottomSheetAdScreen.isLoginedInBottomSheet(
                  context: context,
                  needHelpTap: () =>
                      Get.toNamed(AppPage.adScreenNeedHelpBusiness),
                  canHelpTap: () =>
                      Get.toNamed(AppPage.adScreenCanNeedHelpBusiness))
              : BottomSheetAdScreen.isLoginedOutBottomSheet(context),
          child: SvgPicture.asset(
            'assets/images/post_icon/post_icon.svg',
            height: 18.sp,
            width: 18.sp,
          ),
        ),
        title: Text(
          AppStrings.postAd,
          style: TextStyle(
              fontSize: 10.sp,
              color: Colors.white,
              fontFamily: AppFonts.poppins,
              fontWeight: FontWeight.w200),
        ),
        activeColor: Colors.white,
      ),
      // PersistentBottomNavBarItem(
      //     title: AppStrings.getQuotes,
      //     textStyle: const TextStyle(
      //         color: Colors.white,
      //         fontFamily: AppFonts.poppins,
      //         fontWeight: FontWeight.w200),
      //     activeColorPrimary: Colors.white,
      //     icon: SvgPicture.asset(
      //       'assets/images/post_icon/quote_icon.svg',
      //       height: 16.sp,
      //       width: 16.sp,
      //     )),
    ];
  }

  SliverAppBar createSilverAppBar1(BuildContext context) {
    return SliverAppBar(
      titleSpacing: 3.w,
      leading: MenuDrawer(),
      title: Row(
        children: [
          Expanded(
            child: Container(
              height: 10.h,
              width: 40.w,
              decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/images/logo/logo.png'),
                      fit: BoxFit.fitWidth)),
            ),
          )
        ],
      ),
      actions: [
        isLogined
            ? Padding(
                padding: EdgeInsets.only(right: 3.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const NotificationMenu(),
                    const SizedBox(width: 10),
                    Badge(
                        position: const BadgePosition(
                          end: 4,
                          bottom: 5,
                        ),
                        badgeContent: const Center(
                          child: Text(
                            '1',
                            style: TextStyle(fontSize: 8, color: Colors.white),
                          ),
                        ),
                        child: SvgPicture.asset(
                            "assets/images/post_icon/icon_message.svg")),
                    SizedBox(
                      width: 2.w,
                    ),
                    Builder(
                      builder: (context) => GestureDetector(
                        onTap: () => Scaffold.of(context).openDrawer(),
                        child: SvgPicture.asset(
                          "assets/images/post_icon/person_icon.svg",
                          height: 15.sp,
                        ),
                      ),
                    ),
                  ],
                ))
            : Padding(
                padding: const EdgeInsets.only(top: 5, bottom: 5, right: 10),
                child: Row(
                  children: [
                    TextButton(
                      onPressed: () {
                        Get.toNamed(AppPage.loginScreen);
                      },
                      child: const Text(
                        AppStrings.logInLandingScreen,
                        style: lightBlueTextStyle,
                      ),
                    ),
                    const SizedBox(width: 4),
                    InkWell(
                      onTap: () {
                        Get.toNamed(AppPage.signUpScreen);
                      },
                      child: Container(
                        height: 5.h,
                        width: 20.w,
                        decoration: BoxDecoration(
                          color: AppColors.lightBlueColor,
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: const Center(
                          child: Text(
                            AppStrings.signUpLandingScreen,
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              )
      ],
      elevation: 0.0,
      backgroundColor: Colors.white,
      pinned: true,
      bottom: PreferredSize(
        // Add this code
        preferredSize: const Size.fromHeight(-8), // Add this code
        child: Container(), // Add this code
      ),
    );
  }

  SliverAppBar createSilverAppBar2() {
    return SliverAppBar(
      leading: Container(),
      title: Container(),
      backgroundColor: Colors.white,
      expandedHeight: 175,
      floating: false,
      elevation: 0,
      flexibleSpace: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
        return FlexibleSpaceBar(
          collapseMode: CollapseMode.parallax,
          background: Container(
            color: Colors.white,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 1,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(left: 3.w),
                              child: Row(
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      const Text(
                                        AppStrings.findLocal,
                                        style: lightBlue20TextStyle,
                                      ),
                                      const Text(
                                        AppStrings.professionalForText,
                                        style: lightBlue20TextStyle,
                                      ),
                                      const Text(
                                        AppStrings.prettyMuchText,
                                        style: lightBlue20TextStyle,
                                      ),
                                      SizedBox(
                                        height: 1.h,
                                      ),
                                    ],
                                  ),
                                  Expanded(
                                      child: CircleAvatar(
                                    radius: 40.sp,
                                    backgroundColor:
                                        AppColors.circleLightBlueColor,
                                    child: Stack(
                                      clipBehavior: Clip.none,
                                      children: [
                                        Image.asset(
                                            'assets/images/find_plumber.png'),
                                      ],
                                    ),
                                  )),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 1.h,
                            ),
                            Row(
                              children: [
                                SvgPicture.asset(
                                  'assets/images/post_icon/check_icon.svg',
                                  height: 10.sp,
                                ),
                                SizedBox(
                                  width: 2.w,
                                ),
                                Expanded(
                                  child: Text(
                                    AppStrings.allAdsUpTo,
                                    style: blackMulishTextStyle,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 1.h,
                            ),
                            Row(
                              children: [
                                SvgPicture.asset(
                                  'assets/images/post_icon/check_icon.svg',
                                  height: 10.sp,
                                ),
                                SizedBox(
                                  width: 2.w,
                                ),
                                Expanded(
                                  child: Text(
                                    AppStrings.overDollarTwoFiftyItIsOnly,
                                    style: blackMulishTextStyle,
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 1.h,
                            ),
                            Row(
                              children: [
                                SvgPicture.asset(
                                  'assets/images/post_icon/check_icon.svg',
                                  height: 10.sp,
                                ),
                                SizedBox(
                                  width: 2.w,
                                ),
                                Expanded(
                                  child: Text(
                                    AppStrings.monthlyPlansStartingFrom,
                                    style: blackMulishTextStyle,
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 2.w,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      }),
    );
  }

  SliverAppBar createSilverAppBar3(BuildContext context) {
    return SliverAppBar(
      leading: Container(),
      elevation: isLogined ? 0.0 : 10,
      bottom: const PreferredSize(
        // Add this code
        preferredSize: Size.fromHeight(-25), // Add this code
        child: Text(''), // Add this code
      ),
      backgroundColor: isLogined ? AppColors.blue7Color : Colors.white,
      pinned: true,
      flexibleSpace: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: FlexibleSpaceBar(
            background: Stack(
              clipBehavior: Clip.none,
              children: [
                Padding(
                  padding: EdgeInsets.only(top: 1.h),
                  child: Row(
                    children: [
                      Expanded(
                          child: Container(
                        padding: EdgeInsets.only(left: 4.w),
                        height: 6.h,
                        width: Get.width,
                        decoration: BoxDecoration(
                          color: isLogined
                              ? AppColors.darkLightColor.withOpacity(0.7)
                              : AppColors.buttonGreyColor,
                          borderRadius: const BorderRadius.only(
                              topLeft: Radius.circular(6),
                              bottomLeft: Radius.circular(6)),
                        ),
                        child: TextFormField(
                          controller: controller.searchController,
                          decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: AppStrings.whatOnYourToDo,
                              hintStyle: labelTextStyle,
                              suffixIcon: SearchMenu()),
                        ),
                      )),
                      CustomButton(
                        buttonName: AppStrings.search,
                        width: 22.w,
                        height: 6.h,
                        buttonColor: AppColors.lightBlueColor,
                        textStyle: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.w400,
                          fontFamily: 'poppins',
                          fontSize: 10.sp,
                        ),
                        borderRadius: const BorderRadius.only(
                            topRight: Radius.circular(5),
                            bottomRight: Radius.circular(5)),
                        onTap: () {},
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )),
    );
  }

  SliverAppBar createSilverAppBar4(BuildContext context) {
    return SliverAppBar(
      leading: Container(),
      elevation: 0.0,
      backgroundColor: Colors.white,
      pinned: true,
      bottom: PreferredSize(
        // Add this code
        preferredSize: const Size.fromHeight(-80.0), // Add this code
        child: Container(), // Add this code
      ),
    );
  }

  SliverAppBar createSilverAppBar5(BuildContext context) {
    return SliverAppBar(
      leading: Container(),
      elevation: 3.0,
      backgroundColor: Colors.white,
      pinned: true,
      bottom: PreferredSize(
        // Add this code
        preferredSize: Size.fromHeight(-10.h), // Add this code
        child: Container(), // Add this code
      ),
    );
  }
}
