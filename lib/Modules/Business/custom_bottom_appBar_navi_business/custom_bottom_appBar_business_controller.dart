import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/constants/app_strings.dart';

import '../../../Constants/app_colors.dart';

class CustomBottomAppBarBusinessController extends GetxController {
  int _currentIndex = 0;
  late PageController _pageController;

  @override
  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  var selectedIcon = AppStrings.home;

  selectedIconAction(selectedIcon) {
    selectedIcon = selectedIcon;
    update();
  }

  switchHighlight(selectedIcon) {
    if (selectedIcon == selectedIcon) {
      return AppColors.buttonBlueColor;
    } else {
      return AppColors.buttonGreyColor;
    }
  }

  switchHighlightColor(selectedIcon) {
    if (selectedIcon == selectedIcon) {
      return Colors.white;
    } else {
      return AppColors.blueColor;
    }
  }
}
