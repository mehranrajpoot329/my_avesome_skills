import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CanHelpBusinessController extends GetxController {
  TextEditingController messageController = TextEditingController();
  RxInt itemCount = 10.obs;
  RxBool showAllItems = false.obs;

  isShowCanHelpCard() {
    showAllItems.value = true;
  }
}
