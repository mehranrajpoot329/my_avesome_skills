import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Modules/Business/landing_screen_business/Can_Help_business/can_help_card_business.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/need_row_with_map_view.dart';
import 'package:sizer/sizer.dart';

import '../../../../Constants/app_colors.dart';
import '../../../../Constants/app_strings.dart';
import '../../../../Constants/text_style.dart';
import '../../../../routes/app_pages.dart';

class CanHelpBusinessWithLandingScreen extends StatelessWidget {
  const CanHelpBusinessWithLandingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        NeedHelpRowWithMap(
          isText: false,
          textName: AppStrings.canHelpWith,
        ),
        Padding(
            padding: EdgeInsets.symmetric(horizontal: 2.w),
            child: CanHelpBusinessCard(
              onTap: () {
                Get.toNamed(AppPage.canHelpScreenBusinessWithOutBottomNav);
              },
            )),
        Padding(
          padding: EdgeInsets.only(right: 3.w),
          child: Align(
            alignment: Alignment.bottomRight,
            child: CustomButton(
              buttonName: AppStrings.viewAll,
              width: 22.w,
              height: 3.h,
              buttonColor: AppColors.lightBlue2Color,
              textStyle: customButtonLightTextStyle,
              borderRadius: BorderRadius.circular(20),
              onTap: () =>
                  Get.toNamed(AppPage.canHelpScreenBusinessWithOutBottomNav),
            ),
          ),
        ),
        SizedBox(
          height: 6.h,
        ),
      ],
    );
  }
}
