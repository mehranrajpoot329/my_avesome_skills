import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/rating_star.dart';
import 'package:sizer/sizer.dart';

class CanHelpBusinessCard extends StatelessWidget {
  Function onTap;

  CanHelpBusinessCard({required this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onTap(),
      child: Column(
        children: [
          Container(
            height: 30.h,
            width: 90.w,
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                    offset: Offset(0, 3),
                    blurRadius: 4.0,
                    spreadRadius: 2.0,
                    color: Colors.grey.withOpacity(0.4))
              ],
              borderRadius: BorderRadius.circular(20),
              color: Colors.white,
            ),
            child: Stack(
              children: [
                Container(
                  padding: EdgeInsets.only(left: 9.w, right: 8.w),
                  height: 6.h,
                  width: 90.w,
                  decoration: const BoxDecoration(
                      color: AppColors.blueAccent3Color,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20),
                          topRight: Radius.circular(20))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        AppStrings.housePaint,
                        style: whiteBoldTwelveColor,
                      ),
                      CircleAvatar(
                          radius: 12.sp,
                          backgroundColor: AppColors.lightBlueColor,
                          child: Icon(
                            Icons.favorite,
                            color: Colors.white,
                            size: 3.h,
                          )),
                    ],
                  ),
                ),
                Positioned(
                    top: 7.5.h,
                    left: 34.w,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Row(
                          children: [
                            Text(
                              AppStrings.alex,
                              style: blue18MulishTextStyle,
                            ),
                            SizedBox(width: 9.w),
                            RatingStar(
                              textStyle: greyTextColor,
                              isNumberRating: true,
                              ratingNumber: AppStrings.sixtyFour,
                            ),
                          ],
                        ),
                        Text(
                          AppStrings.jobSuccessPercentage,
                          style: greenMulishTextStyle,
                        )
                      ],
                    )),
                Positioned(
                    top: 14.h,
                    left: 34.w,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          AppStrings.featuredSkill,
                          style: blue12MulishTextStyle,
                        ),
                        SizedBox(
                          height: 0.5.h,
                        ),
                        Row(
                          children: [
                            Text(
                              AppStrings.helpMoving,
                              style: blackMulishTextStyle,
                            ),
                            SizedBox(width: 12.5.w),
                            Text(
                              AppStrings.fortyOne,
                              style: blue12MulishTextStyle,
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 0.3.h,
                        ),
                        Row(
                          children: [
                            Text(
                              AppStrings.homeRepairs,
                              style: blackMulishTextStyle,
                            ),
                            SizedBox(width: 14.5.w),
                            Text(
                              AppStrings.thirtySeven,
                              style: blue12MulishTextStyle,
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 0.5.h,
                        ),
                        Row(
                          children: [
                            Text(
                              AppStrings.furnitureAssembly,
                              style: blackMulishTextStyle,
                            ),
                            SizedBox(width: 7.w),
                            Text(
                              AppStrings.thirtyFive,
                              style: blue12MulishTextStyle,
                            ),
                          ],
                        )
                      ],
                    )),
                Positioned(
                  top: 9.h,
                  left: 3.w,
                  child: Container(
                    height: 14.h,
                    width: 14.h,
                    decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            image: AssetImage('assets/images/profile_alex.png'),
                            fit: BoxFit.cover)),
                  ),
                ),
                Positioned(
                  bottom: 0.h,
                  left: 0.w,
                  right: 0.w,
                  child: Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: 9.w, vertical: 0.7.h),
                    height: 4.h,
                    width: Get.width,
                    decoration: const BoxDecoration(
                        color: AppColors.blue5Color,
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20))),
                    child: Text(
                      AppStrings.southPortQLD,
                      style: interBlackTextStyle,
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 2.h,
          )
        ],
      ),
    );
  }
}
