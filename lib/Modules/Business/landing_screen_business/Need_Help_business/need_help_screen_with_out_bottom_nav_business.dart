import 'package:AwesomeSkills/Modules/Business/landing_screen_business/Need_Help_business/need_help_card_business.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Modules/Business/custom_scroll_appBar_business/nested_business_scroll_widget.dart';
import 'package:AwesomeSkills/Modules/Business/landing_screen_business/Need_Help_business/need_help_business_controller.dart';
import 'package:AwesomeSkills/Modules/Business/landing_screen_business/landing_screen__business_controller.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/Need_Help/need_help_card.dart';
import 'package:AwesomeSkills/routes/app_pages.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/nested_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/bottom_sheet_widget.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/need_row_with_map_view.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:sizer/sizer.dart';

import '../../../../Constants/app_colors.dart';
import '../../../../constants/app_strings.dart';
import '../../../../constants/text_style.dart';
import '../../../../widgets/button/custom_button.dart';

class NeedHelpScreenBusinessWithOutBottomNav extends StatelessWidget {
  final landingBusinessController = Get.put(LandingScreenBusinessController());
  final needHelpBusinessController = Get.put(NeedHelpBusinessController());

  @override
  Widget build(BuildContext context) {
    return NestedBusinessScroll(
        floatingActionButton: WidgetMethod.floatingActionButton(context),
        body: Padding(
          padding: EdgeInsets.only(left: 3.w, right: 2.w),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  height: 1.h,
                ),
                Row(
                  children: [
                    GestureDetector(
                      onTap: () => Get.back(),
                      child: Icon(
                        Icons.arrow_back_ios_new,
                        color: Colors.black,
                        size: 3.h,
                      ),
                    ),
                    SizedBox(
                      width: 5.w,
                    ),
                    const Text(
                      AppStrings.iNeedHelp,
                      style: montserrat20TextStyle,
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(
                    left: 1.w,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 2.h,
                      ),
                      NeedHelpRowWithMap(
                        isText: true,
                        textName: AppStrings.needHelp,
                      ),
                    ],
                  ),
                ),
                Obx(
                  () => ListView.builder(
                    physics: const ScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: needHelpBusinessController.itemCount.value > 5 &&
                            !needHelpBusinessController.showAllItems.value
                        ? 5
                        : needHelpBusinessController.itemCount.value,
                    itemBuilder: (BuildContext context, int index) {
                      return Center(
                        child: NeedHelpBusinessCard(
                          onTap: () {
                            landingBusinessController.isLogined
                                ? Get.toNamed(
                                    AppPage.needHelpCardBusinessDetailScreen)
                                : BottomSheetAdScreen.isLoginedOutBottomSheet(
                                    context);
                          },
                        ),
                      );
                    },
                  ),
                ),
                Obx(() => needHelpBusinessController.showAllItems.value
                    ? Container()
                    : Align(
                        alignment: Alignment.bottomRight,
                        child: CustomButton(
                          buttonName: AppStrings.viewAll,
                          width: 22.w,
                          height: 3.h,
                          buttonColor: AppColors.lightBlue2Color,
                          textStyle: customButtonLightTextStyle,
                          borderRadius: BorderRadius.circular(20),
                          onTap: () =>
                              needHelpBusinessController.isShowCanHelpCard(),
                        ),
                      )),
                SizedBox(
                  height: 6.h,
                )
              ],
            ),
          ),
        ));
  }
}
