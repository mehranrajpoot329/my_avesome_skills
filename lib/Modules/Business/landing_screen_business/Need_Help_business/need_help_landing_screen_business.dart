import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Modules/Business/landing_screen_business/Need_Help_business/need_help_card_business.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/Need_Help/need_help_card.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/need_row_with_map_view.dart';
import 'package:sizer/sizer.dart';

import '../../../../Constants/app_colors.dart';
import '../../../../Constants/app_strings.dart';
import '../../../../Constants/text_style.dart';
import '../../../../routes/app_pages.dart';
import '../../../../widgets/button/custom_button.dart';

class NeedHelpLandingBusinessScreen extends StatelessWidget {
  const NeedHelpLandingBusinessScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        NeedHelpRowWithMap(textName: AppStrings.needHelp, isText: true),
        SizedBox(height: 2.h),
        Padding(
            padding: EdgeInsets.only(left: 3.w, right: 3.w),
            child: NeedHelpBusinessCard(
              onTap: () {
                print("Hello");
                Get.toNamed(AppPage.needHelpScreenBusinessWithOutBottomNav);
              },
            )),
        Padding(
          padding: EdgeInsets.only(right: 3.w),
          child: Align(
            alignment: Alignment.bottomRight,
            child: CustomButton(
              buttonName: AppStrings.viewAll,
              width: 22.w,
              height: 3.h,
              buttonColor: AppColors.lightBlue2Color,
              textStyle: customButtonLightTextStyle,
              borderRadius: BorderRadius.circular(20),
              onTap: () =>
                  Get.toNamed(AppPage.needHelpScreenBusinessWithOutBottomNav),
            ),
          ),
        ),
        SizedBox(
          height: 2.h,
        ),
      ],
    );
  }
}
