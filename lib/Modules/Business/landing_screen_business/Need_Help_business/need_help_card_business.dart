import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/rating_star.dart';
import 'package:sizer/sizer.dart';

class NeedHelpBusinessCard extends StatelessWidget {
  Function onTap;

  NeedHelpBusinessCard({required this.onTap});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onTap(),
      child: Column(
        children: [
          Container(
            height: 30.h,
            width: 90.w,
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                    offset: Offset(0, 3),
                    blurRadius: 4.0,
                    spreadRadius: 2.0,
                    color: Colors.grey.withOpacity(0.4))
              ],
              borderRadius: BorderRadius.circular(20),
              color: Colors.white,
            ),
            child: Stack(
              children: [
                Container(
                  padding: EdgeInsets.only(left: 9.w, right: 8.w),
                  height: 6.h,
                  width: 90.w,
                  decoration: const BoxDecoration(
                      color: AppColors.purpleColor,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20),
                          topRight: Radius.circular(20))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        AppStrings.housePaint,
                        style: whiteBoldTwelveColor,
                      ),
                      CircleAvatar(
                          radius: 12.sp,
                          backgroundColor: AppColors.blueAccent2Color,
                          child: Icon(
                            Icons.favorite,
                            color: Colors.white,
                            size: 3.h,
                          )),
                    ],
                  ),
                ),
                Positioned(
                  top: 7.5.h,
                  left: 10.w,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const Text(
                        AppStrings.aFernandez,
                        style: interBlack2TextStyle,
                      ),
                      SizedBox(
                        height: 1.h,
                      ),
                      Container(
                        height: 14.h,
                        width: 14.h,
                        decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                image: AssetImage(
                                    'assets/images/profile_a_fernendez.png'),
                                fit: BoxFit.cover)),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  top: 8.h,
                  left: 50.w,
                  child: Column(
                    children: [
                      RatingStar(
                        textStyle: greyTextColor,
                        isNumberRating: true,
                        ratingNumber: AppStrings.zero,
                      ),
                      SizedBox(
                        height: 1.5.h,
                      ),
                      Text(
                        AppStrings.$300,
                        style: interGreenTextStyle,
                      ),
                      SizedBox(
                        height: 1.h,
                      ),
                      CustomButton(
                        buttonName: AppStrings.jobValue,
                        width: 25.w,
                        height: 3.h,
                        buttonColor: AppColors.lightGreenColor,
                        textStyle: blackMulishTextStyle,
                        borderRadius: BorderRadius.circular(3),
                        onTap: () {},
                      ),
                      SizedBox(
                        height: 1.h,
                      ),
                      Row(
                        children: [
                          Image.asset(
                            'assets/images/post_icon/calendar.png',
                            height: 10.sp,
                          ),
                          SizedBox(width: 3.w),
                          Text(
                            AppStrings.asap,
                            style: interBlackTextStyle,
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                Positioned(
                  bottom: 0,
                  right: 0,
                  left: 0,
                  child: Container(
                    padding:
                        EdgeInsets.symmetric(horizontal: 9.w, vertical: 0.7.h),
                    height: 4.h,
                    width: Get.width,
                    decoration: const BoxDecoration(
                        color: AppColors.blueAccent2Color,
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20))),
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        AppStrings.southPortQLD,
                        style: interBlackTextStyle,
                        textAlign: TextAlign.start,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 2.h,
          )
        ],
      ),
    );
  }
}
