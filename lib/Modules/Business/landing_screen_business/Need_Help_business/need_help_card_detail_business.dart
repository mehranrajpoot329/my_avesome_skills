import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/Modules/Business/custom_scroll_appBar_business/nested_business_scroll_widget.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/rating_star.dart';
import 'package:AwesomeSkills/widgets/custom_widget_app/widget_method.dart';
import 'package:sizer/sizer.dart';

import '../../../../routes/app_pages.dart';

class NeedHelpCardDetailBusinessScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return NestedBusinessScroll(
      floatingActionButton: Padding(
          padding: EdgeInsets.only(left: 7.w, bottom: 0.h),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Row(
                  children: [
                    Expanded(
                      child: CustomButton(
                          buttonName: AppStrings.message,
                          width: 30.w,
                          height: 5.h,
                          buttonColor: AppColors.blue9Color,
                          textStyle: interWhite14TextStyle,
                          borderRadius: BorderRadius.circular(10),
                          onTap: () => Get.toNamed(
                              AppPage.needHelpMessageScreenBusiness)),
                    ),
                    SizedBox(
                      width: 10.w,
                    ),
                    Expanded(
                      child: CustomButton(
                          buttonName: AppStrings.call,
                          width: 30.w,
                          height: 5.h,
                          buttonColor: AppColors.green5Color,
                          textStyle: black14Color,
                          borderRadius: BorderRadius.circular(10),
                          onTap: () {}),
                    ),
                  ],
                ),
                SizedBox(
                  height: 1.h,
                ),
                GestureDetector(
                  onTap: () => WidgetMethod.bottomSheet(context),
                  child: FittedBox(
                    child: SvgPicture.asset(
                      'assets/images/micMan.svg',
                      height: 23.sp,
                      width: 23.sp,
                    ),
                  ),
                )
              ])),
      body: Padding(
          padding: EdgeInsets.only(
            left: 2.w,
            right: 2.w,
          ),
          child: Container(
            height: Get.height,
            width: Get.width,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: const BorderRadius.only(
                  topRight: Radius.circular(20),
                  topLeft: Radius.circular(20),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.2),
                    blurRadius: 4.0,
                    spreadRadius: 2.0,
                  )
                ]),
            child: ListView(
              padding: EdgeInsets.zero,
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              children: [
                Container(
                  padding: EdgeInsets.only(left: 5.w, right: 5.w),
                  height: 8.h,
                  width: 80.w,
                  decoration: const BoxDecoration(
                      color: AppColors.purpleColor,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20),
                          topRight: Radius.circular(20))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        AppStrings.housePaint,
                        style: whiteBoldTwelveColor,
                      ),
                      CircleAvatar(
                          radius: 15,
                          backgroundColor: AppColors.lightBlueColor,
                          child: Icon(
                            Icons.favorite,
                            color: Colors.red,
                            size: 15.sp,
                          )),
                    ],
                  ),
                ),
                SizedBox(
                  height: 2.h,
                ),
                Padding(
                  padding: EdgeInsets.only(left: 5.w, right: 5.w),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            AppStrings.aFernandez,
                            style: interBlack16TextStyle,
                          ),
                          Row(
                            children: [
                              Icon(
                                Icons.location_on,
                                color: AppColors.green3Color,
                                size: 18.sp,
                              ),
                              Text(
                                AppStrings.southPort,
                                style: interGreen15TextStyle,
                              )
                            ],
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 1.h,
                      ),
                      SizedBox(
                          width: 30.w,
                          child: RatingStar(
                              textStyle: greyTextColor,
                              isNumberRating: true,
                              ratingNumber: '64')),
                      SizedBox(
                        height: 2.h,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            height: 100,
                            width: 100,
                            decoration: const BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    image: AssetImage(
                                        'assets/images/profile_a_fernendez.png'),
                                    fit: BoxFit.cover)),
                          ),
                          Column(
                            children: [
                              Text(
                                AppStrings.$300,
                                style: interGreenTextStyle,
                              ),
                              SizedBox(
                                height: 1.h,
                              ),
                              CustomButton(
                                buttonName: AppStrings.jobValue,
                                width: 30.w,
                                height: 4.h,
                                buttonColor: AppColors.lightGreenColor,
                                textStyle: blackMulishTextStyle,
                                borderRadius: BorderRadius.circular(5),
                                onTap: () {},
                              ),
                              SizedBox(
                                height: 1.5.h,
                              ),
                              Row(
                                children: [
                                  Image.asset(
                                      'assets/images/post_icon/calendar.png'),
                                  SizedBox(
                                    width: 2.w,
                                  ),
                                  Text(
                                    AppStrings.asap,
                                    style: blackSixHundredWeightColor,
                                  )
                                ],
                              ),
                            ],
                          )
                        ],
                      ),
                      SizedBox(
                        height: 2.h,
                      ),
                      Text(
                        AppStrings.jobDescription,
                        style: blueMulish1TextStyle,
                      ),
                      SizedBox(
                        height: 1.h,
                      ),
                      Text(
                        AppStrings.jobId,
                        style: blackMulish300TextStyle,
                      ),
                      SizedBox(
                        height: 1.5.h,
                      ),
                      Text(
                        AppStrings.suppliesAndResource,
                        style: blackSixHundredWeightColor,
                      ),
                      SizedBox(
                        height: 2.h,
                      ),
                      Text(
                        AppStrings.needToBeDone,
                        style: blueMulish1TextStyle,
                      ),
                      SizedBox(
                        height: 2.h,
                      ),
                    ],
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    buildNeedDoneWidget('assets/images/spray_bottle.png'),
                    buildNeedDoneWidget('assets/images/spray_bottle2.png'),
                    buildNeedDoneWidget('assets/images/dustbin.png'),
                  ],
                ),
                SizedBox(
                  height: 3.h,
                ),
                Align(
                  alignment: Alignment.center,
                  child: CustomButton(
                      buttonName: AppStrings.iAmInterested,
                      width: 35.w,
                      height: 5.h,
                      buttonColor: AppColors.blue10Color,
                      textStyle: black14Color,
                      borderRadius: BorderRadius.circular(10),
                      onTap: () {}),
                ),
                SizedBox(
                  height: 15.h,
                ),
              ],
            ),
          )),
    );
  }

  Widget buildNeedDoneWidget(String image) {
    return Container(
      height: 13.h,
      width: 30.w,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        image: DecorationImage(image: AssetImage(image), fit: BoxFit.fill),
        boxShadow: [
          BoxShadow(
              blurRadius: 4.0,
              spreadRadius: 2.0,
              color: Colors.grey.withOpacity(0.2)),
        ],
      ),
    );
  }
}
