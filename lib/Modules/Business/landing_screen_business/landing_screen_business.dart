import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Modules/Business/landing_screen_business/Can_Help_business/can_help_with_landing_screen_business.dart';
import 'package:AwesomeSkills/Modules/Business/landing_screen_business/Need_Help_business/need_help_landing_screen_business.dart';
import 'package:AwesomeSkills/Modules/Business/landing_screen_business/feature_job_business/featured_job_landing_screen_business.dart';
import 'package:AwesomeSkills/Modules/Business/landing_screen_business/get_help_today_landing_screen_business/get_help_today_landing_screen_business.dart';
import 'package:AwesomeSkills/Modules/Business/landing_screen_business/landing_screen__business_controller.dart';
import 'package:AwesomeSkills/Modules/Business/landing_screen_business/post_ads_help_business/post_ads_help_landing_screen_business.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/Can_Help/can_help_with_landing_screen.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/Need_Help/need_help_landing_screen.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/get_help_today_landing_screen/get_help_today_landing_screen.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/landing_screen_controller.dart';

import 'package:sizer/sizer.dart';

class LandingScreenBusiness extends StatelessWidget {
  final landingScreenBusinessController =
      Get.put(LandingScreenBusinessController());
  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.zero,
      children: [
        landingScreenBusinessController.isLogined
            ? const SizedBox(
                height: 1,
              )
            : SizedBox(
                height: 36.h,
                child: const PostAdsHelpBusinessLandingScreen(),
              ),
        landingScreenBusinessController.isLogined
            ? SizedBox(
                height: 1.h,
              )
            : SizedBox(
                height: 1.h,
              ),
        const FeaturedJobBusinessLandingScreen(),
        SizedBox(
          height: 1.h,
        ),
        Padding(
          padding: EdgeInsets.only(left: 3.w, right: 2.w),
          child: NeedHelpLandingBusinessScreen(),
        ),
        GetHelpTodayBusinessLandingScreen(),
        SizedBox(
          height: 2.h,
        ),
        Padding(
          padding: EdgeInsets.only(left: 3.w, right: 2.w),
          child: CanHelpBusinessWithLandingScreen(),
        ),
      ],
    );
  }
}
