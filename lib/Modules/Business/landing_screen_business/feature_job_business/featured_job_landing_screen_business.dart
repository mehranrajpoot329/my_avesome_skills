import 'package:AwesomeSkills/Modules/Business/landing_screen_business/feature_job_business/featured_job_card_business.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Modules/Personal/landing_screen/feature_job/featured_job_card.dart';
import 'package:sizer/sizer.dart';

import '../../../../Constants/app_strings.dart';
import '../../../../Constants/text_style.dart';

class FeaturedJobBusinessLandingScreen extends StatelessWidget {
  const FeaturedJobBusinessLandingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 42.h,
      width: Get.width,
      padding: EdgeInsets.only(top: 2.h),
      decoration:
          BoxDecoration(color: const Color(0xFFD5E5F5).withOpacity(0.7)),
      child: Padding(
        padding: EdgeInsets.only(
          left: 2.w,
        ),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(right: 2.w),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SizedBox(
                    width: 65.w,
                    child: Row(
                      children: [
                        Text(
                          AppStrings.featured,
                          style: blackBoldFourteenTextStyle,
                        ),
                        SizedBox(width: 1.w),
                        Text(
                          AppStrings.jobs,
                          style: TextStyle(
                              color: AppColors.blueColor,
                              fontSize: 14.sp,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'poppins'),
                        ),
                        SizedBox(width: 1.w),
                        Text(
                          AppStrings.ofTheDay,
                          style: blackBoldFourteenTextStyle,
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 4.h,
                    width: 25.w,
                    decoration: BoxDecoration(
                        color: AppColors.lightBlueColor,
                        borderRadius: BorderRadius.circular(30)),
                    child: Center(
                      child: Text(
                        AppStrings.explore,
                        style: customButtonWhiteTextStyle,
                      ),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 2.h,
            ),
            Padding(
                padding: EdgeInsets.only(left: 3.w),
                child: SizedBox(
                  height: 32.h,
                  child: Align(
                    alignment: Alignment.center,
                    child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) {
                        return FeatureJobBusinessCard(
                          aboutWork: AppStrings.housePaint,
                          location: AppStrings.southPort,
                          images: 'assets/images/profile_pic.png',
                          textDetails: AppStrings.loremIpsumText,
                          price: AppStrings.thirtyDollar,
                          viewPost: AppStrings.onePointFiveText,
                        );
                      },
                    ),
                  ),
                )),
          ],
        ),
      ),
    );
  }
}
