import 'package:flutter/material.dart';
import 'package:AwesomeSkills/Constants/app_colors.dart';
import 'package:AwesomeSkills/Constants/app_strings.dart';
import 'package:AwesomeSkills/Constants/text_style.dart';
import 'package:AwesomeSkills/widgets/button/custom_button.dart';
import 'package:sizer/sizer.dart';

class FeatureJobBusinessCard extends StatelessWidget {
  String aboutWork, location, images, textDetails, viewPost;
  int price;

  FeatureJobBusinessCard({
    required this.aboutWork,
    required this.location,
    required this.images,
    required this.textDetails,
    required this.price,
    required this.viewPost,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          height: 30.h,
          width: 85.w,
          decoration: BoxDecoration(
            boxShadow: const [
              BoxShadow(
                  offset: Offset(0, 5),
                  blurRadius: 4.0,
                  spreadRadius: 2.0,
                  color: AppColors.blueAccent2Color)
            ],
            borderRadius: BorderRadius.circular(20),
            color: Colors.white,
          ),
          child: Stack(
            children: [
              Positioned(
                top: 0.0,
                left: 0.0,
                child: Container(
                  padding: EdgeInsets.only(
                    left: 3.w,
                  ),
                  height: 40,
                  width: 85.w,
                  decoration: const BoxDecoration(
                      color: AppColors.blueAccentColor,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(20),
                          topRight: Radius.circular(20))),
                  child: Row(
                    children: [
                      SizedBox(
                        width: 40.w,
                        child: Row(
                          children: [
                            const Icon(
                              Icons.favorite_border,
                              color: Colors.white,
                            ),
                            SizedBox(
                              width: 1.w,
                            ),
                            Text(
                              aboutWork,
                              style: customButtonWhiteTextStyle,
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 10.w,
                      ),
                      Expanded(
                        child: Row(
                          children: [
                            CircleAvatar(
                                radius: 15,
                                backgroundColor: Colors.white,
                                child: Icon(
                                  Icons.location_on,
                                  color: Colors.black,
                                  size: 3.h,
                                )),
                            SizedBox(
                              width: 1.w,
                            ),
                            Text(
                              location,
                              style: customButtonWhiteTextStyle,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Positioned(
                top: 9.h,
                left: 25,
                child: Row(
                  children: [
                    Container(
                      height: 70,
                      width: 70,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              image: AssetImage(
                                images,
                              ),
                              fit: BoxFit.cover)),
                    ),
                    SizedBox(
                      width: 3.w,
                    ),
                    SizedBox(
                      width: 55.w,
                      child: Text(
                        AppStrings.loremIpsumText,
                        style: interBlackFiveHundredTextStyle,
                      ),
                    ),
                  ],
                ),
              ),
              Positioned(
                top: 20.h,
                right: 15.w,
                child: Text(
                  '\$$price',
                  style: interBlueTextStyle,
                ),
              ),
              Positioned(
                  top: 22.h,
                  right: 0.0,
                  left: 0.0,
                  child: const Divider(color: AppColors.indigoColor)),
              Positioned(
                bottom: 1.h,
                left: 5.w,
                right: 5.w,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    SizedBox(
                      width: 15.w,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Image.asset(
                            'assets/images/post_icon/calendar.png',
                            height: 20,
                          ),
                          Text(
                            AppStrings.asap,
                            style: interBlackTextStyle,
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 20.w,
                      child: Row(
                        children: [
                          Image.asset(
                            'assets/images/post_icon/visible_icon.png',
                            height: 20,
                          ),
                          Text(
                            viewPost,
                            style: interBlackTextStyle,
                          ),
                        ],
                      ),
                    ),
                    CustomButton(
                        buttonName: AppStrings.getPaid,
                        width: 28.w,
                        borderRadius: BorderRadius.circular(5),
                        height: 4.h,
                        buttonColor: AppColors.greenColor,
                        textStyle: interWhiteTextStyle,
                        onTap: () {}),
                  ],
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          width: 2.w,
        ),
      ],
    );
  }
}
