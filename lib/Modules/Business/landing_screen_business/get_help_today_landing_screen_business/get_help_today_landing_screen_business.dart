import 'package:AwesomeSkills/Modules/Business/custom_scroll_appBar_business/custom_app_bar_business_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/custom_app_bar_controller.dart';
import 'package:sizer/sizer.dart';

import '../../../../Constants/app_colors.dart';
import '../../../../Constants/app_strings.dart';
import '../../../../Constants/text_style.dart';
import '../../../../widgets/button/custom_button.dart';

class GetHelpTodayBusinessLandingScreen extends StatelessWidget {
  final customAppBarController = Get.put(CustomAppBarBusinessController());

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 4.w, right: 4.w, top: 2.h),
      height: customAppBarController.isLogined ? 22.h : 17.h,
      width: MediaQuery.of(context).size.width,
      decoration: const BoxDecoration(
        color: AppColors.lightBlue2Color,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Text(
                AppStrings.getHelpToday,
                style: orangeColorTextStyle,
              ),
              CustomButton(
                buttonName: AppStrings.viewMore,
                width: 24.w,
                height: 3.h,
                buttonColor: AppColors.lightBlueColor,
                textStyle: customButtonTextStyle,
                borderRadius: BorderRadius.circular(20),
                onTap: () {},
              ),
            ],
          ),
          SizedBox(
            height: 1.h,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              buildGetHelpToday(
                name: AppStrings.handyman,
              ),
              buildGetHelpToday(
                name: AppStrings.minorHome,
              ),
            ],
          ),
          SizedBox(
            height: 2.h,
          ),
          customAppBarController.isLogined
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    buildGetHelpToday(
                      name: AppStrings.hireAPainter,
                    ),
                    buildGetHelpToday(
                      name: AppStrings.lightInstallation,
                    ),
                  ],
                )
              : Container(),
        ],
      ),
    );
  }

  Widget buildGetHelpToday({required String name}) {
    return Container(
      height: 4.h,
      width: 40.w,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Center(
        child: FittedBox(
          child: Text(
            name,
            style: customButtonLightTextStyle,
          ),
        ),
      ),
    );
  }
}
