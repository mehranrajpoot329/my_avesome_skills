import 'dart:async';

import 'package:AwesomeSkills/local/db/local_storage.dart';
import 'package:AwesomeSkills/utils/utils.dart';
import 'package:AwesomeSkills/widgets/custom_scroll_appBar/custom_app_bar_controller.dart';
import 'package:avatar_glow/avatar_glow.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:AwesomeSkills/routes/app_pages.dart';

class SplashScreen extends StatelessWidget {
  final customAppBar = Get.put(CustomAppBarController());

  static void moveToNext() {
    bool isNew = HiveHelper.isNew();
    bool isAlreadyLoggedIn = HiveHelper.isLoggedIn();
    if (isNew) {
      Future.delayed(3.seconds, () => Get.offAllNamed(AppPage.landingScreen));
      return;
    } else if (isAlreadyLoggedIn) {
      Utils.showProgressBar();
      Future.delayed(3.seconds, () => Get.offAllNamed(AppPage.landingScreen));
      return;
    } else if (HiveHelper.isLoggedOut() || !isAlreadyLoggedIn) {
      Future.delayed(3.seconds, () => Get.offAllNamed(AppPage.loginScreen));
    }
  }

  @override
  Widget build(BuildContext context) {
    moveToNext();
    return Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Center(
              child: AvatarGlow(
                endRadius: 90,
                duration: const Duration(seconds: 2),
                glowColor: Colors.white24,
                repeat: true,
                repeatPauseDuration: const Duration(seconds: 2),
                startDelay: const Duration(seconds: 1),
                child: Material(
                    elevation: 8.0,
                    shape: const CircleBorder(),
                    child: CircleAvatar(
                      backgroundColor: Colors.grey[100],
                      radius: 50.0,
                      child: Image.asset(
                          "assets/images/logo/my_awesome_skills_icon.png"),
                    )),
              ),
            ),
          ],
        ));
  }
}
