import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:AwesomeSkills/constants/app_colors.dart';
import 'package:AwesomeSkills/constants/app_fonts.dart';

class Utils {
  static GetSnackBar successSnackBar({
    String title = 'Success',
    required String message,
    SnackPosition snackPosition = SnackPosition.BOTTOM,
  }) {
    Get.log("[$title] $message");
    return GetSnackBar(
      titleText: Text(
        title,
        style: Get.textTheme.headline6?.merge(
          const TextStyle(color: AppColors.textBlack),
        ),
      ),
      messageText: Text(
        message,
        style: Get.textTheme.caption?.merge(
          const TextStyle(
              color: AppColors.textBlack, fontFamily: AppFonts.poppins),
        ),
      ),
      snackPosition: snackPosition,
      margin: const EdgeInsets.all(20),
      backgroundColor: AppColors.blueColor,
      icon: const Icon(
        Icons.check_circle_outline,
        size: 32,
        color: AppColors.black,
      ),
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 18),
      borderRadius: 8,
      boxShadows: [
        BoxShadow(
            color: AppColors.textBlack.withOpacity(0.5),
            offset: const Offset(3, 5),
            blurRadius: 15),
      ],
      dismissDirection: DismissDirection.horizontal,
      duration: const Duration(seconds: 5),
    );
  }

  static GetSnackBar errorSnackBar(
      {String title = 'Error', required String message}) {
    Get.log("[$title] $message", isError: true);
    String filteredMessage = (message.toLowerCase().contains('timeout') ||
            message.toLowerCase().contains('socket') ||
            message.toLowerCase().contains('connection') ||
            message.toLowerCase().contains('Connecting') ||
            message.toLowerCase().contains('timed out') ||
            message.toLowerCase().contains('closed'))
        ? 'Please check your internet Connection'
        : message;

    return GetSnackBar(
      titleText: Text(
        title.tr,
        style: Get.textTheme.headline6?.merge(
          const TextStyle(
            color: AppColors.textBlack,
            fontFamily: AppFonts.poppins,
          ),
        ),
      ),
      messageText: Text(
        filteredMessage,
        style: Get.textTheme.caption?.merge(
          const TextStyle(
            color: AppColors.textBlack,
            fontFamily: AppFonts.poppins,
          ),
        ),
      ),
      snackPosition: SnackPosition.BOTTOM,
      margin: const EdgeInsets.all(10),
      backgroundColor: Colors.redAccent,
      icon: const Icon(Icons.remove_circle_outline,
          size: 32, color: AppColors.black),
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 18),
      borderRadius: 8,
      duration: const Duration(seconds: 5),
    );
  }

  static void hideKeyboard(BuildContext context) {
    FocusScope.of(context).unfocus();
  }

  // static final _picker = ImagePicker();
  //
  // static Future getImage({
  //   required ImageSource source,
  //   CropAspectRatioPreset? cropAspectRatioPreset,
  // }) async {
  //   XFile? image = await _picker.pickImage(source: source, imageQuality: 100);
  //   if (image != null) {
  //     File? croppedFile = await ImageCropper().cropImage(
  //         sourcePath: image.path,
  //         //compressFormat: ImageCompressFormat.png,
  //         //compressQuality: 100,
  //         maxHeight: 500,
  //         maxWidth: 500,
  //         aspectRatio: const CropAspectRatio(ratioX: 1.0, ratioY: 1.0),
  //         //aspectRatioPresets: [CropAspectRatioPreset.ratio16x9],
  //         androidUiSettings: AndroidUiSettings(
  //           toolbarTitle: 'Cropper',
  //           toolbarColor: Colors.white,
  //           toolbarWidgetColor: AppColors.yellow,
  //           initAspectRatio:
  //               cropAspectRatioPreset ?? CropAspectRatioPreset.ratio16x9,
  //           lockAspectRatio: true,
  //         ),
  //         iosUiSettings: const IOSUiSettings(
  //           minimumAspectRatio: 1.0,
  //         ));
  //     return croppedFile;
  //   }
  // }

  static showProgressBar() {
    if (Get.isDialogOpen ?? false) {
      return;
    }
    Get.dialog(
      _progressBar(),
      barrierDismissible: (kDebugMode) ? true : false,
      transitionDuration: 100.milliseconds,
      barrierColor: Colors.white.withOpacity(0.2),
      useSafeArea: true,
    );
  }

  static dismissProgressBar() {
    if (Get.isDialogOpen ?? false) {
      Get.back();
    }
  }

  static final _picker = ImagePicker();

  static Future getImage({
    required ImageSource source,
    CropAspectRatioPreset? cropAspectRatioPreset,
  }) async {
    XFile? pickedFile;
    CroppedFile? croppedFile;
    pickedFile = await _picker.pickImage(source: source, imageQuality: 100);
    if (pickedFile != null || croppedFile != null) {
      croppedFile = await ImageCropper().cropImage(
          sourcePath: pickedFile!.path,
          //compressFormat: ImageCompressFormat.png,
          //compressQuality: 100,
          maxHeight: 500,
          maxWidth: 500,
          aspectRatio: const CropAspectRatio(ratioX: 1.0, ratioY: 1.0),
          //aspectRatioPresets: [CropAspectRatioPreset.ratio16x9],
          uiSettings: [
            AndroidUiSettings(
              toolbarTitle: 'Cropper',
              toolbarColor: Colors.white,
              toolbarWidgetColor: AppColors.lightBlue2Color,
              initAspectRatio:
                  cropAspectRatioPreset ?? CropAspectRatioPreset.ratio16x9,
              lockAspectRatio: true,
            ),
            IOSUiSettings(
              minimumAspectRatio: 1.0,
            )
          ]);
      return croppedFile;
    }
  }

  static Widget _progressBar() {
    return Center(
      child: Container(
        width: Get.width / 2.3,
        height: 70,
        decoration: BoxDecoration(
          color: Colors.white,
          backgroundBlendMode: BlendMode.colorDodge,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              blurRadius: 10,
              offset: const Offset(5, 5),
              spreadRadius: 5,
            ),
          ],
        ),
        child: const Center(
          child: CircularProgressIndicator.adaptive(
            backgroundColor: AppColors.lightBlueColor,
          ),
        ),
      ),
    );
  }

  // static DataTime toDateTime(TimeStamp value) {
  //   if (value == null) {
  //     return null;
  //   } else {
  //     return value.toDate();
  //   }
  // }

  static Future<bool?> errorFlutterToast({required String msg}) {
    return Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: AppColors.red2Color,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  static Future<bool?> successFlutterToast({required String msg}) {
    return Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: AppColors.black,
        textColor: Colors.white,
        fontSize: 16.0);
  }

  static void log({
    String identifier = '@@@@\n---------------------------------------->',
    required String message,
  }) {
    if (kDebugMode) {
      print('$identifier\n$message\n<----------------------------------------');
    }
  }

  // static void openURL(String url) async {
  //   if (await canLaunchUrl(Uri.parse(url))) {
  //     await launchUrl(
  //       Uri.parse(url),
  //     );
  //   }
  // }

  // static Future<void> open1to1Chat(
  //   String currentUser,
  //   String targetUser,
  //   BuildContext context,
  // ) async {
  //   showProgressBar();
  //   await ThreadsController.fetchOrCreatePrivate1to1Thread(
  //     currentUser,
  //     targetUser,
  //   ).then((value) {
  //     dismissProgressBar();
  //     if (value.isError) {
  //       Get.showSnackbar(Utils.errorSnackBar(message: value.message));
  //       return;
  //     }
  //     Navigator.of(context).push(
  //       MaterialPageRoute(
  //         maintainState: false,
  //         builder: (context) => ChatScreen(
  //           currentUserId: currentUser,
  //           thread: value,
  //           position: -1,
  //         ),
  //       ),
  //     );
  //   });
  // }

  static void copyDataToClipboard({required String data}) {
    Clipboard.setData(ClipboardData(text: data));
  }

  /// Returns [TextDirection.rtl] if the current locale has RTL support.
  // static TextDirection getTextDirectionRTL(String languageCode) {
  //   if (intl.Bidi.isRtlLanguage(languageCode)) {
  //     return TextDirection.rtl;
  //   }
  //   return TextDirection.ltr;
  // }

  // static Future<String> findAddress({required LatLng latLng}) async {
  //   List<Placemark> placemarks =
  //       await placemarkFromCoordinates(latLng.latitude, latLng.longitude);
  //   Placemark first = placemarks.first;
  //   String address =
  //       '${first.locality} ${first.administrativeArea},${first.subLocality}, ${first.subAdministrativeArea},${first.street}, ${first.name},${first.thoroughfare}, ${first.subThoroughfare}';
  //   return address;
  // }

  // October 18, 2019
  static String formatDateYMMMMD(String date) {
    DateTime inputDate = DateTime.parse(date);
    var outputFormat = DateFormat('yMMMMd');
    var outputDate = outputFormat.format(inputDate);
    return outputDate;
  }
}
